﻿const app = new Vue({
    el: '#App',
    data: {
        GetMonth: new Date().getMonth() + 1,
        ApiUrl: 'http://bridge.mommyhappy.com/',
        Request: {
            url: '',
            data: '',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            method: 'post'
        },
        BranchSelectData: [],
        BranchSelected: 0,
        YearSelectData: [],
        YearSelected: 0,
        ReportSelectData: [],
        ReportSelected: 0,
        StayReportData: [],
        StayReportShow: false,
        PromotionReportData: [],
        PromotionReportShow: false,
        HousekeeperReportData: [],
        HousekeeperReportShow: false
    },
    mounted: function () {
        this.GetBranchData();
        this.YearSelectData = this.GetYearData();
        this.YearSelected = this.GetYear();
        this.ReportSelectData = this.GetReportData();
    },
    methods: {
        GetBranchData: function () {
            this.Request.url = this.ApiUrl + 'hr/100/';
            this.Request.method = 'get';
            let Selt = this;
            $.ajax(this.Request).done(function (Response) {
                if (typeof Response !== undefined) {
                    Selt.BranchSelectData = Response.Data;
                }
            }).fail(function (Error) {
                console.log(Error);
            });
        },
        GetYear: function() {
            let date = new Date();
            let year = date.getFullYear();
            return year;
        },
        GetYearData: function () {
            let year = this.GetYear();
            let spacing = 3;
            let data = [];
            for (let i = year - spacing; i < year; i++) {
                data.push({
                    ID: i, Name: i
                });
            }
            for (let i = year; i <= year + spacing; i++) {
                data.push({
                    ID: i, Name: i
                });
            }
            return data;
        },
        GetReportData: function () {
            return [
                { ID: 0, Name: '留任率報表' },
                { ID: 1, Name: '未晉升統計表' },
                { ID: 2, Name: '管家績效表' }
            ];
        },
        ReportClickEvent: function () {
            switch (this.ReportSelected) {
                case 0:
                    this.StayReportEvent();
                    this.PromotionReportShow = false;
                    this.HousekeeperReportShow = false;
                    break;
                case 1:
                    this.PromotionReportEvent();
                    this.StayReportShow = false;
                    this.HousekeeperReportShow = false;
                    break;
                case 2:
                    this.HousekeeperReportEvent();
                    this.StayReportShow = false;
                    this.PromotionReportShow = false;
                    break;
                default:
                    break;
            }
        },
        StayReportEvent: function () {
            this.Request.url = this.ApiUrl + 'hr/101/';
            this.Request.data = JSON.stringify({ "Branch_ID": this.BranchSelected, "Year": this.YearSelected });
            this.Request.method = 'post';
            let Self = this;
            $.ajax(this.Request).done(function (Response) {
                if (typeof Response !== undefined) {
                    if (Response.Data.length > 0) {
                        let Data = Response.Data;
                        for (let i = 0; i < Data.length; i++) {
                            let LeftStaffYear = (Data[i].leftStaff / Self.GetMonth) * 12;
                            let NewComerTotalYear = Data[i].initTotal + (Data[i].newComer / Self.GetMonth) * 12;
                            let StayResult = 1 - LeftStaffYear / NewComerTotalYear;
                            let StayProportion = new Number(StayResult * 100).toFixed(1);
                            Data[i].StayProportion = isNaN(StayProportion) ? 0 : StayProportion;
                            let oldleftTotal = Data[i].oldleft / Data[i].initTotal * 100;
                            let oldleftProportion = new Number(100 - oldleftTotal).toFixed(1);
                            Data[i].oldleftProportion = isNaN(oldleftProportion) ? 0 : oldleftProportion;
                            let newleftTotal = Data[i].newleft / Data[i].newComer * 100;
                            let newleftProportion = new Number(100 - newleftTotal).toFixed(1);
                            Data[i].newleftProportion = isNaN(newleftProportion) ? 0 : newleftProportion;
                        }
                        Self.StayReportData = Data;
                        Self.StayReportShow = true;
                    }
                }
            }).fail(function (Error) {
                console.log(Error);
            });
        },
        PromotionReportEvent: function () {
            this.Request.url = this.ApiUrl + 'hr/102/';
            this.Request.data = JSON.stringify({ "Branch_ID": this.BranchSelected, "Year": this.YearSelected });
            this.Request.method = 'post';
            let Self = this;
            $.ajax(this.Request).done(function (Response) {
                if (typeof Response !== undefined) {
                    if (Response.Data.length > 0) {
                        Self.PromotionReportData = Response.Data;
                        Self.PromotionReportShow = true;
                    }
                }
            }).fail(function (Error) {
                console.log(Error);
            });
        },
        HousekeeperReportEvent: function () {
            this.Request.url = this.ApiUrl + 'hr/104/';
            this.Request.data = JSON.stringify({ "Branch_ID": this.BranchSelected, "Year": this.YearSelected });
            this.Request.method = 'post';
            let Self = this;
            $.ajax(this.Request).done(function (Response) {
                if (typeof Response !== undefined) {
                    if (Response.Data.length > 0) {
                        let Data = Response.Data;
                        for (let i = 0; i < Data.length; i++) {
                            let LeftStaffYear = (Data[i].leftStaff / Self.GetMonth) * 12;
                            let NewComerTotalYear = Data[i].initTotal + (Data[i].newComer / Self.GetMonth) * 12;
                            let StayResult = 1 - LeftStaffYear / NewComerTotalYear;
                            let StayProportion = new Number(StayResult * 100).toFixed(1);
                            Data[i].StayProportion = isNaN(StayProportion) ? 0 : StayProportion;
                            let oldleftTotal = Data[i].oldleft / Data[i].initTotal * 100;
                            let oldleftProportion = new Number(100 - oldleftTotal).toFixed(1);
                            Data[i].oldleftProportion = isNaN(oldleftProportion) ? 0 : oldleftProportion;
                            let newleftTotal = Data[i].newleft / Data[i].newComer * 100;
                            let newleftProportion = new Number(100 - newleftTotal).toFixed(1);
                            Data[i].newleftProportion = isNaN(newleftProportion) ? 0 : newleftProportion;
                        }
                        Self.HousekeeperReportData = Response.Data;
                        Self.HousekeeperReportShow = true;
                    }
                }
            }).fail(function (Error) {
                console.log(Error);
            });
        },
        PromotionReportUpdateEvent: function (e) {
            let Staff_No = e.path[2].children[3].textContent;
            let Des = e.path[2].children[5].children[0].value;
            this.Request.url = this.ApiUrl + 'hr/103/';
            this.Request.data = JSON.stringify({ "Staff_No": Staff_No, "Des": Des });
            this.Request.method = 'post';
            let Self = this;
            $.ajax(this.Request).done(function (Response) {
                if (typeof Response !== undefined) {
                    if (Response.Status) {
                        Self.PromotionReportEvent();
                        alert('更新成功');
                    } else {
                        alert('更新失敗');
                    }
                }
            }).fail(function (Error) {
                console.log(Error);
            });
        }
    }
});