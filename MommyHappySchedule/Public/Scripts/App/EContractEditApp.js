﻿"use strict";
/**
 * 載入模組 
 */
$(function () {
    Window.AjaxApp = new AjaxApp();
    Window.EContractEditApp = new EContractEditApp();
    Window.ContractDataApp = new ContractDataApp();
    Window.JobScheduleApp = new JobScheduleApp();
    Window.ContractPayApp = new ContractPayApp();
    Window.ContractPayDetailApp = new ContractPayDetailApp();
});

/**
 * 電子合約編輯建構子
 */
function EContractEditApp() {
    /**
     * API URL
     * @type {string} API
     */
    this.API = '/Api/EContractEdit.asmx/';

    /**
     * API 請求
     * @type {object.<string, string>} Request
     */
    this.Request = {
        url: '',
        data: '',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        method: 'post'
    };

    this.Init();
}

/**
 * 電子合約編輯初始化
 */
EContractEditApp.prototype.Init = function () {
    this.ScrollFeaturesMenu();
    this.AlertMessageClose();
};

/**
 * 滾動功能選單
 */
EContractEditApp.prototype.ScrollFeaturesMenu = function () {
    let Features = $('.features');

    $(window).scroll(ScrollEvent);

    function ScrollEvent() {
        let Top = $(this).scrollTop();
        if (Top > 100) {
            Features.addClass('scroll');
        } else {
            Features.removeClass('scroll');
        }
    }
};

/**
 * 提示訊息顯示
 * @param {object} AlertDom DOM物件
 * @param {object.<string, string>} Result 訊息
 */
EContractEditApp.prototype.AlertMessageShow = function (AlertDom, Result) {
    if (AlertDom.length > 0) {
        let Status = Result.Status !== undefined ? Result.Status : '';
        let Message = Result.Message !== undefined ? Result.Message : '';
        if (Status) {
            AlertDom.addClass('alert-success active').text(Message);
        } else {
            AlertDom.addClass('alert-danger active').text(Message);
        }
    }
};

/**
 * 提示訊息關閉
 */
EContractEditApp.prototype.AlertMessageClose = function () {
    let Self = this;
    setInterval(function () {
        $('body').find('.alert').removeClass('active');
        Self.AlertMessageClose();
    }, 15000);
};

/**
 * 建立下拉選單字串
 * @param {object.<string, object>} Data 資料陣列
 * @param {string} Key 編號
 * @returns {string} Str
 */
EContractEditApp.prototype.NewOpetion = function (Data, Key) {
    let Str = '';
    for (let Item in Data) {
        let Selected = Item === Key ? ' selected="selected"' : '';
        Str += '<option value="' + Item + '"' + Selected + '>' + Data[Item] + '</option>';
    }
    return Str;
};

/**
 * 建立下拉選單字串
 * @param {object.<string, object>} Data 資料物件
 * @param {string} Key 編號
 * @returns {string} Str
 */
EContractEditApp.prototype.NewOptionObject = function (Data, Key) {
    let Str = '';
    for (let Item in Data) {
        let Selected = Data[Item].ID === Key ? ' selected="selected"' : '';
        Str += '<option value="' + Data[Item].ID + '"' + Selected + '>' + Data[Item].Name + '</option>';
    }
    return Str;
};

/**
 * 合約資料建構子
 */
function ContractDataApp() {
    this.Request = Window.EContractEditApp.Request;

    this.UpdateBtn = $('#UpdateBtn');
    this.DeleteBtn = $('#DeleteBtn');

    this.ContractDataFieldset = $('#ContractDataFieldset');
    this.Contract_ID = this.ContractDataFieldset.children('input[name="Contract_ID"]');
    this.Contract_Nums = this.ContractDataFieldset.find('input[name="Contract_Nums"]');
    this.Branch_ID = this.ContractDataFieldset.find('select[name="Branch_ID"]');
    this.Charge_Staff_No = this.ContractDataFieldset.find('input[name="Charge_Staff_No"]');
    this.Charge_Staff_Name = this.ContractDataFieldset.find('input[name="Charge_Staff_Name"]');
    this.Cust_No = this.ContractDataFieldset.find('input[name="Cust_No"]');
    this.Contract_S_Date = this.ContractDataFieldset.find('input[name="Contract_S_Date"]');
    this.Contract_E_Date = this.ContractDataFieldset.find('input[name="Contract_E_Date"]');
    this.Contract_R_Date = this.ContractDataFieldset.find('input[name="Contract_R_Date"]');
    this.Contract_Times = this.ContractDataFieldset.find('input[name="Contract_Times"]');
    this.Contract_Status = this.ContractDataFieldset.find('select[name="Contract_Status"]');
    this.Contract_U_Date = this.ContractDataFieldset.find('input[name="Contract_U_Date"]');
    this.Service_Cycle = this.ContractDataFieldset.find('input[name="Service_Cycle"]');
    this.Contract_Years = this.ContractDataFieldset.find('input[name="Contract_Years"]');
    this.HK_Nums = this.ContractDataFieldset.find('input[name="HK_Nums"]');
    this.Note = this.ContractDataFieldset.find('textarea[name="Note"]');

    this.Init();
}

/**
 * 合約資料初始化
 */
ContractDataApp.prototype.Init = function () {
    this.UpdateClick();
    this.DeleteClick();
};

/**
 * 刪除點擊
 */
ContractDataApp.prototype.DeleteClick = function () {
    let Self = this;
    this.DeleteBtn.on('click', function () {
        if (confirm('確定刪除此筆合約，並清除此筆合約相關資料(含打卡與督導資料)?')) {
            Self.DeleteEvent(Self.Contract_ID.val());
        }
    });
};

/**
 * 刪除事件
 * @param {number} Contract_ID 合約編號
 */
ContractDataApp.prototype.DeleteEvent = function (Contract_ID) {
    this.Request.url = Window.EContractEditApp.API + 'Delete';
    this.Request.method = 'POST';
    this.Request.data = JSON.stringify({ 'Did': Contract_ID });
    Window.AjaxApp(this.Request, function (Response) {
        if (Response !== null) {
            let Result = JSON.parse(Response.d);
            location.href = '/EContractList.aspx?Status=' + Result.Status + '&Code=' + Result.Code + '&Message=' + Result.Message;
        }
    });
};

/**
 * 更新點擊
 */
ContractDataApp.prototype.UpdateClick = function () {
    let Self = this;
    this.UpdateBtn.on('click', function () {
        if (confirm('確定儲存此筆合約，我要送出囉?')) {
            let CustContractData = Self.GetContractFormData();
            let CustJobSchedule = Self.GetJobScheduleFormData();
            let CustContractPay = Self.GetContractPayFormData();
            let CustContractPayDetail = Self.GetContractPayDetailFormData();
            let Data = { 'CustContractData': CustContractData, 'CustJobSchedule': CustJobSchedule, 'CustContractPay': CustContractPay, 'CustContractPayDetail': CustContractPayDetail };
            Self.UpdateEvent(Data);
        }
    });
};

/**
 * 更新事件
 * @param {object.<string, object>} Data 物件資料
 */
ContractDataApp.prototype.UpdateEvent = function (Data) {
    let Self = this;
    this.Request.url = Window.EContractEditApp.API + 'Update';
    this.Request.method = 'POST';
    this.Request.data = JSON.stringify({ 'Input': Data });
    Window.AjaxApp(this.Request, function (Response) {
        if (Response !== null) {
            let Result = JSON.parse(Response.d);
            location.href = 'EContractList.aspx?Status=' + Result.Status + '&Code=' + Result.Code + '&Message=' + Result.Message;
        }
    });
};

/**
 * 取得合約表單資料
 * @returns {object.<string, object>} Data 資料
 */
ContractDataApp.prototype.GetContractFormData = function () {
    let Contract_ID = this.Contract_ID.val();
    let Contract_Nums = this.Contract_Nums.val();
    let Branch_ID = this.Branch_ID.val();
    let Branch_Name = this.Branch_ID.find('option:selected').text();
    let Charge_Staff_No = this.Charge_Staff_No.val();
    let Charge_Staff_Name = this.Charge_Staff_Name.val();
    let Cust_No = this.Cust_No.val();
    let Contract_S_Date = this.Contract_S_Date.val();
    let Contract_E_Date = this.Contract_E_Date.val();
    let Contract_R_Date = this.Contract_R_Date.val();
    let Contract_Times = this.Contract_Times.val();
    let Contract_Status = this.Contract_Status.val();
    let Contract_U_Date = this.Contract_U_Date.val();
    let Service_Cycle = this.Service_Cycle.val();
    let Contract_Years = this.Contract_Years.val();
    let HK_Nums = this.HK_Nums.val();
    let Note = this.Note.val();
    return { 'Contract_ID': Contract_ID, 'Contract_Nums': Contract_Nums, 'Branch_ID': Branch_ID, 'Branch_Name': Branch_Name, 'Charge_Staff_No': Charge_Staff_No, 'Charge_Staff_Name': Charge_Staff_Name, 'Cust_No': Cust_No, 'Contract_S_Date': Contract_S_Date, 'Contract_E_Date': Contract_E_Date, 'Contract_R_Date': Contract_R_Date, 'Contract_Times': Contract_Times, 'Contract_Status': Contract_Status, 'Contract_U_Date': Contract_U_Date, 'Service_Cycle': Service_Cycle, 'Contract_Years': Contract_Years, 'HK_Nums': HK_Nums, 'Note': Note };
};

/**
 * 取得工作時間表表單資料
 * @returns {object.<string, object>} Data 資料
 */
ContractDataApp.prototype.GetJobScheduleFormData = function () {
    let TbodyTr = Window.JobScheduleApp.Table.children('tbody').children('tr');
    let Data = [];
    for (let i = 0; i < TbodyTr.length; i++) {
        let TbodyTrTd = TbodyTr.eq(i).children('td');
        let Schedule_ID = TbodyTrTd.eq(0).children('input[name="Schedule_ID"]').val();
        let Cust_No = TbodyTrTd.eq(0).children('input[name="Cust_No"]').val();
        let Contract_ID = TbodyTrTd.eq(0).children('input[name="Contract_ID"]').val();
        let Job_Date = TbodyTrTd.eq(1).children('input[name="Job_Date"]').val();
        let Job_Time = TbodyTrTd.eq(2).children('select[name="Job_Time"]').val();
        let Staff_No = TbodyTrTd.eq(3).children('input[name="Staff_No"]').val();
        let Job_Result_ID = TbodyTrTd.eq(4).children('select[name="Job_Result"]').val();
        let Job_Result = TbodyTrTd.eq(4).children('select[name="Job_Result"]').find('option:selected').text();
        let Job_Price = TbodyTrTd.eq(5).children('input[name="Job_Price"]').val();
        let Job_Flag = TbodyTrTd.children('select[name="Job_Flag"]').val();
        let Note = TbodyTrTd.children('input[name="Note"]').val();
        let ObjData = { 'Schedule_ID': Schedule_ID, 'Cust_No': Cust_No, 'Contract_ID': Contract_ID, 'Job_Date': Job_Date, 'Job_Time': Job_Time, 'Staff_No': Staff_No, 'Job_Result_ID': Job_Result_ID, 'Job_Result': Job_Result, 'Job_Price': Job_Price, 'Job_Flag': Job_Flag, 'Note': Note };
        Data.push(ObjData);
    }
    return Data;
};

/**
 * 取得付款資訊表單資料
 * @returns {object.<string, object>} Data 資料
 */
ContractDataApp.prototype.GetContractPayFormData = function () {
    let ContractPayFieldset = Window.ContractPayApp.ContractPayFieldset;
    let ContractPay_ID = ContractPayFieldset.find('input[name="ContractPay_ID"]').val();
    let Contract_ID = ContractPayFieldset.find('input[name="Contract_ID"]').val();
    let Cust_No = ContractPayFieldset.find('input[name="Cust_No"]').val();
    let C_Pay_Numbers = ContractPayFieldset.find('input[name="C_Pay_Numbers"]').val();
    let Payment_Type = ContractPayFieldset.find('select[name="Payment_Type"]').val();
    let Out_Bank = ContractPayFieldset.find('input[name="Out_Bank"]').val();
    let Out_Acct_No = ContractPayFieldset.find('input[name="Out_Acct_No"]').val();
    let C_Pay_Date = ContractPayFieldset.find('input[name="C_Pay_Date"]').val();
    let C_Pay_Amt = ContractPayFieldset.find('input[name="C_Pay_Amt"]').val();
    let C_Pay_Tax = ContractPayFieldset.find('input[name="C_Pay_Tax"]').val();
    let C_Tax_Flag = ContractPayFieldset.find('select[name="C_Tax_Flag"]').val();
    let Contract_Type = ContractPayFieldset.find('select[name="Contract_Type"]').val();
    let Payment_Memo = ContractPayFieldset.find('input[name="Payment_Memo"]').val();
    return { 'ContractPay_ID': ContractPay_ID, 'Contract_ID': Contract_ID, 'Cust_No': Cust_No, 'C_Pay_Numbers': C_Pay_Numbers, 'Payment_Type': Payment_Type, 'Out_Bank': Out_Bank, 'Out_Acct_No': Out_Acct_No, 'C_Pay_Date': C_Pay_Date, 'C_Pay_Amt': C_Pay_Amt, 'C_Pay_Tax': C_Pay_Tax, 'C_Tax_Flag': C_Tax_Flag, 'Contract_Type': Contract_Type, 'Payment_Memo': Payment_Memo };
};

/**
 * 取得付款明細資料
 * @returns {object.<string, object>} Data 資料
 */
ContractDataApp.prototype.GetContractPayDetailFormData = function () {
    let TbodyTr = Window.ContractPayDetailApp.Table.children('tbody').children('tr');
    let Data = [];
    for (let i = 0; i < TbodyTr.length; i++) {
        let TbodyTrTd = TbodyTr.eq(i).children('td');
        let ContractPayDetail_ID = TbodyTrTd.eq(0).children('input[name="ContractPayDetail_ID"]').val();
        let Pay_ID = TbodyTrTd.eq(0).children('input[name="Pay_ID"]').val();
        let Pay_Number = TbodyTrTd.eq(1).children('input[name="Pay_Number"]').val();
        let Payment_Type = TbodyTrTd.eq(2).children('select[name="Payment_Type"]').val();
        let Out_Bank = TbodyTrTd.eq(3).children('input[name="Out_Bank"]').val();
        let Out_Acct_No = TbodyTrTd.eq(4).children('input[name="Out_Acct_No"]').val();
        let Amount = TbodyTrTd.eq(5).children('input[name="Amount"]').val();
        let Amt = TbodyTrTd.eq(6).children('input[name="Amt"]').val();
        let B_Pay_Date = TbodyTrTd.eq(7).children('input[name="B_Pay_Date"]').val();
        let Fulfill = TbodyTrTd.eq(8).children('select[name="Fulfill"]').val();
        let R_Pay_Date = TbodyTrTd.eq(9).children('input[name="R_Pay_Date"]').val();
        let Tax = TbodyTrTd.eq(10).children('input[name="Tax"]').val();
        let Sales_Memo = TbodyTrTd.eq(11).children('input[name="Sales_Memo"]').val();
        let ObjData = { 'ContractPayDetail_ID': ContractPayDetail_ID, 'Pay_ID': Pay_ID, 'Pay_Number': Pay_Number, 'Payment_Type': Payment_Type, 'Out_Bank': Out_Bank, 'Out_Acct_No': Out_Acct_No, 'Amount': Amount, 'Amt': Amt, 'B_Pay_Date': B_Pay_Date, 'Fulfill': Fulfill, 'R_Pay_Date': R_Pay_Date, 'Tax': Tax, 'Sales_Memo': Sales_Memo };
        Data.push(ObjData);
    }
    return Data;
};

/**
 * 工作時間表建構子
 */
function JobScheduleApp() {
    this.Request = Window.EContractEditApp.Request;

    this.JobTimeData = null;
    this.JobResultData = null;
    this.JobFlagData = null;

    this.JobScheduleFieldset = $('#JobScheduleFieldset');
    this.InsertJobScheduleBtn = $('#InsertJobScheduleBtn');

    this.Table = this.JobScheduleFieldset.children('table');
    this.InsertBlock = this.Table.find('tr.thead-insert');
    this.Cust_No = this.InsertBlock.children('td').eq(0).children('input[name="Cust_No"]');
    this.Contract_ID = this.InsertBlock.children('td').eq(0).children('input[name="Contract_ID"]');
    this.Job_Date = this.InsertBlock.children('td').eq(1).children('input[name="Job_Date"]');
    this.Job_Time = this.InsertBlock.children('td').eq(2).children('select[name="Job_Time"]');
    this.Staff_No = this.InsertBlock.children('td').eq(3).children('input[name="Staff_No"]');
    this.Job_Result = this.InsertBlock.children('td').eq(4).children('select[name="Job_Result"]');
    this.Job_Price = this.InsertBlock.children('td').eq(5).children('input[name="Job_Price"]');
    this.Job_Flag = this.InsertBlock.children('td').eq(6).children('select[name="Job_Flag"]');
    this.Note = this.InsertBlock.children('td').eq(7).children('input[name="Note"]');

    this.Init();
}

/**
 * 工作時間表初始化
 */
JobScheduleApp.prototype.Init = function () {
    this.GetJobTimeData();
    this.GetJobResultData();
    this.GetJobFlagData();

    this.InsertClick();
    this.UpdateClick();
    this.DeleteClick();
};

/**
 * 新增點擊
 */
JobScheduleApp.prototype.InsertClick = function () {
    let Self = this;
    this.InsertJobScheduleBtn.on('click', function () {
        if (confirm('確定新增合約工作時間表，我要送出囉?')) {
            let Data = {
                'Job_Date': Self.Job_Date.val(),
                'Job_Time': Self.Job_Time.val(),
                'Staff_No': Self.Staff_No.val(),
                'Cust_No': Self.Cust_No.val(),
                'Job_Result_ID': Self.Job_Result.val(),
                'Job_Result': Self.Job_Result.find('option:selected').text(),
                'Job_Price': Self.Job_Price.val(),
                'Job_Flag': Self.Job_Flag.val(),
                'Contract_ID': Self.Contract_ID.val(),
                'Note': Self.Note.val()
            };
            Self.InsertEvent(Data);
        }
    });
};

/**
 * 新增事件
 * @param {object.<string, number|string>} Data 資料
 */
JobScheduleApp.prototype.InsertEvent = function (Data) {
    let Self = this;
    this.Request.url = Window.EContractEditApp.API + 'InsertJobSchedule';
    this.Request.method = 'POST';
    this.Request.data = JSON.stringify({ Input: Data });
    Window.AjaxApp(this.Request, function (Response) {
        if (typeof Response === 'object') {
            let Result = JSON.parse(Response.d);
            if (Result !== '') {
                if (Result.Status) {
                    Self.GetJobScheduleData();
                    Self.InsertFormClear();
                }
                let AlertDom = Self.JobScheduleFieldset.children('.alert');
                Window.EContractEditApp.AlertMessageShow(AlertDom, Result);
            }
        }
    });
};

/**
 * 新增欄位清除值
 */
JobScheduleApp.prototype.InsertFormClear = function () {
    this.Job_Date.val('');
    this.Job_Time.children('option').eq(0).attr('selected', true);
    this.Staff_No.val('');
    this.Job_Result.children('option').eq(0).attr('selected', true);
    this.Job_Price.val(0);
    this.Job_Flag.children('option').eq(0).attr('selected', true);
    this.Note.val('');
};

/**
 * 更新點擊
 */
JobScheduleApp.prototype.UpdateClick = function () {
    let Self = this;
    let TbodyTr = this.Table.children('tbody').children('tr');
    for (let i = 0; i < TbodyTr.length; i++) {
        let TableBtn = TbodyTr.eq(i).children('td.table-btn');
        TableBtn.on('click', 'input[name="Update"]', function () {
            if (confirm('確定更新此筆資料?')) {
                let UpdateBlock = $(this).closest('tr');
                let Schedule_ID = UpdateBlock.children('td').eq(0).children('input[name="Schedule_ID"]').val();
                let Cust_No = UpdateBlock.children('td').eq(0).children('input[name="Cust_No"]').val();
                let Contract_ID = UpdateBlock.children('td').eq(0).children('input[name="Contract_ID"]').val();
                let Job_Date = UpdateBlock.children('td').eq(1).children('input[name="Job_Date"]').val();
                let Job_Time = UpdateBlock.children('td').eq(2).children('select[name="Job_Time"]').val();
                let Staff_No = UpdateBlock.children('td').eq(3).children('input[name="Staff_No"]').val();
                let Job_Result_ID = UpdateBlock.children('td').eq(4).children('select[name="Job_Result"]').val();
                let Job_Result = UpdateBlock.children('td').eq(4).children('select[name="Job_Result"]').find('option:selected').text();
                let Job_Price = UpdateBlock.children('td').eq(5).children('input[name="Job_Price"]').val();
                let Job_Flag = UpdateBlock.children('td').eq(6).children('select[name="Job_Flag"]').val();
                let Note = UpdateBlock.children('td').eq(7).children('input[name="Note"]').val();
                let Data = {
                    'Schedule_ID': Schedule_ID,
                    'Cust_No': Cust_No,
                    'Contract_ID': Contract_ID,
                    'Job_Date': Job_Date,
                    'Job_Time': Job_Time,
                    'Staff_No': Staff_No,
                    'Job_Result_ID': Job_Result_ID,
                    'Job_Result': Job_Result,
                    'Job_Price': Job_Price,
                    'Job_Flag': Job_Flag,
                    'Note': Note
                };
                Self.UpdateEvent(Data);
            }
        });
    }
};

/**
 * 更新事件
 * @param {object.<string, object>} Data 資料物件
 */
JobScheduleApp.prototype.UpdateEvent = function (Data) {
    let Self = this;
    this.Request.url = Window.EContractEditApp.API + 'UpdateJobSchedule';
    this.Request.method = 'POST';
    this.Request.data = JSON.stringify({ Input: Data });

    Window.AjaxApp(this.Request, function (Response) {
        if (typeof Response === 'object') {
            let Result = JSON.parse(Response.d);
            if (Result !== '') {
                if (Result.Status) {
                    Self.GetJobScheduleData();
                }
                let AlertDom = Self.JobScheduleFieldset.children('.alert');
                Window.EContractEditApp.AlertMessageShow(AlertDom, Result);
            }
        }
    });
};

/**
 * 刪除點擊
 */
JobScheduleApp.prototype.DeleteClick = function () {
    let Self = this;
    let TbodyTr = this.Table.children('tbody').children('tr');
    for (let i = 0; i < TbodyTr.length; i++) {
        let TableBtn = TbodyTr.eq(i).children('td.table-btn');
        TableBtn.on('click', 'input[name="Delete"]', function () {
            if (confirm('確定刪除此筆資料?')) {
                Self.DeleteEvent($(this));
            }
        });
    }
};

/**
 * 刪除事件
 * @param {object} Dom 刪除按鈕
 */
JobScheduleApp.prototype.DeleteEvent = function (Dom) {
    let Self = this;
    let Schedule_ID = Dom.siblings('input[name="Schedule_ID"]').val();
    this.Request.url = Window.EContractEditApp.API + 'DeleteJobSchedule';
    this.Request.method = 'POST';
    this.Request.data = JSON.stringify({ 'Did': Schedule_ID });
    Window.AjaxApp(this.Request, function (Response) {
        if (typeof Response === 'object') {
            let Result = JSON.parse(Response.d);
            if (Result !== '') {
                if (Result.Status) {
                    Dom.closest('tr').remove();
                }
                let AlertDom = Self.JobScheduleFieldset.children('.alert');
                Window.EContractEditApp.AlertMessageShow(AlertDom, Result);
            }
        }
    });
};

/**
 * 表格載入
 * @param {object.<string, object>} Data 資料陣列
 */
JobScheduleApp.prototype.TableLoad = function (Data) {
    if (Data !== null) {
        let Table = this.JobScheduleFieldset.children('table');
        let Tbody = Table.children('tbody');
        let TrStr = '';
        Tbody.empty();
        for (let Item in Data) {
            let JobTimeOptiion = Window.EContractEditApp.NewOpetion(this.JobTimeData, Data[Item].Job_Time);
            let JobResultOptiion = Window.EContractEditApp.NewOptionObject(this.JobResultData, Data[Item].Job_Result_ID);
            let JobFlagOption = Window.EContractEditApp.NewOpetion(this.JobFlagData, Data[Item].Job_Flag);
            TrStr += '<tr><td class="table-btn">' +
                '<input type="hidden" name="Schedule_ID" value="' + Data[Item].ID + '" />' +
                '<input type="hidden" name="Cust_No" value="' + Data[Item].Cust_No + '" />' +
                '<input type="hidden" name="Contract_ID" value="' + Data[Item].Contract_ID + '" />' +
                '<input class="btn btn-outline-danger" name="Delete" type="button" value="刪除" />' +
                '<input class="btn btn-outline-info" name="Update" type="button" value="儲存" />' +
            '</td>' +
            '<td class="t-w-180"><input class="form-control" type="date" name="Job_Date" value="' + Data[Item].Job_Date + '" /></td>' +
            '<td><select class="form-control" name="Job_Time">' + JobTimeOptiion + '</select></td>' +
            '<td><input class="form-control" type="text" name="Staff_No" placeholder="未派管家" value="' + Data[Item].Staff_No + '" /></td>' +
            '<td><select class="form-control" name="Job_Result">' + JobResultOptiion + '</select></td>' +
            '<td><input class="form-control" type="number" name="Job_Price" min="0" value="' + Data[Item].Job_Price + '" /></td>' +
            '<td><select class="form-control" name="Job_Flag">' + JobFlagOption + '</select></td>' +
            '<td><input class="form-control" type="text" name="Note" placeholder="單次備註" value="' + Data[Item].Note + '" /></td></tr>';
        }
        Tbody.append(TrStr);
    }
    this.UpdateClick();
    this.DeleteClick();
};

/**
 * 取得工作時間表資料
 */
JobScheduleApp.prototype.GetJobScheduleData = function () {
    let Self = this;
    this.Request.url = Window.EContractEditApp.API + 'GetJobScheduleData';
    this.Request.method = 'POST';
    this.Request.data = JSON.stringify({ 'Contract_ID': this.Contract_ID.val() });
    Window.AjaxApp(this.Request, function (Response) {
        if (typeof Response === 'object') {
            let Result = JSON.parse(Response.d);
            if (Result !== '') {
                if (Result.Status) {
                    Self.TableLoad(Result.Data);
                }
            }
        }
    });
};

/**
 * 取得服務時段資料
 */
JobScheduleApp.prototype.GetJobTimeData = function () {
    let Self = this;
    this.Request.url = Window.EContractEditApp.API + 'GetJobTimeData';
    this.Request.method = 'GET';
    Window.AjaxApp(this.Request, function (Response) {
        if (typeof Response === 'object') {
            let Result = JSON.parse(Response.d);
            if (Result !== '') {
                if (Result.Status) {
                    Self.JobTimeData = Result.Data;
                }
            }
        }
    });
};

/**
 * 取得派班項目資料
 */
JobScheduleApp.prototype.GetJobResultData = function () {
    let Self = this;
    this.Request.url = Window.EContractEditApp.API + 'GetJobResultData';
    this.Request.method = 'GET';
    Window.AjaxApp(this.Request, function (Response) {
        if (typeof Response === 'object') {
            let Result = JSON.parse(Response.d);
            if (Result !== '') {
                if (Result.Status) {
                    Self.JobResultData = Result.Data;
                }
            }
        }
    });
};

/**
 * 取得異動狀態資料
 */
JobScheduleApp.prototype.GetJobFlagData = function () {
    let Self = this;
    this.Request.url = Window.EContractEditApp.API + 'GetJobFlagData';
    this.Request.method = 'GET';
    Window.AjaxApp(this.Request, function (Response) {
        if (typeof Response === 'object') {
            let Result = JSON.parse(Response.d);
            if (Result !== '') {
                if (Result.Status) {
                    Self.JobFlagData = Result.Data;
                }
            }
        }
    });
};

/**
 * 客戶合約付款資料
 */
function ContractPayApp() {
    this.ContractPayFieldset = $('#ContractPayFieldset');
}

/**
 * 付款明細建構子
 */
function ContractPayDetailApp() {
    this.Request = Window.EContractEditApp.Request;
    this.Contract_ID = Window.ContractDataApp.Contract_ID.val();

    this.PaymentTypeData = null;
    this.FulfillData = null;

    this.ContractPayDetailFieldset = $('#ContractPayDetailFieldset');
    this.InsertContractPayDetailBtn = $('#InsertContractPayDetailBtn');

    this.Table = this.ContractPayDetailFieldset.children('table');
    this.InsertBlock = this.Table.find('tr.thead-insert');
    this.Pay_ID = this.InsertBlock.children('td').eq(0).children('input[name="Pay_ID"]');
    this.Pay_Number = this.InsertBlock.children('td').eq(1).children('input[name="Pay_Number"]');
    this.Payment_Type = this.InsertBlock.children('td').eq(2).children('select[name="Payment_Type"]');
    this.Out_Bank = this.InsertBlock.children('td').eq(3).children('input[name="Out_Bank"]');
    this.Out_Acct_No = this.InsertBlock.children('td').eq(4).children('input[name="Out_Acct_No"]');
    this.Amount = this.InsertBlock.children('td').eq(5).children('input[name="Amount"]');
    this.Amt = this.InsertBlock.children('td').eq(6).children('input[name="Amt"]');
    this.B_Pay_Date = this.InsertBlock.children('td').eq(7).children('input[name="B_Pay_Date"]');
    this.Fulfill = this.InsertBlock.children('td').eq(8).children('select[name="Fulfill"]');
    this.R_Pay_Date = this.InsertBlock.children('td').eq(9).children('input[name="R_Pay_Date"]');
    this.Tax = this.InsertBlock.children('td').eq(10).children('input[name="Tax"]');
    this.Sales_Memo = this.InsertBlock.children('td').eq(11).children('input[name="Sales_Memo"]');

    this.Init();
}

/**
 * 付款明細建初始化
 */
ContractPayDetailApp.prototype.Init = function () {
    this.GetPaymentTypeData();
    this.GetFulfillData();

    this.InsertClick();
    this.UpdateClick();
    this.DeleteClick();
};

/**
 * 新增付款明細點擊
 */
ContractPayDetailApp.prototype.InsertClick = function () {
    let Self = this;
    this.InsertContractPayDetailBtn.on('click', function () {
        if (confirm('確定新增付款明細，我要送出囉?')) {
            let Data = {
                'Pay_ID': Self.Pay_ID.val(),
                'Pay_Number': Self.Pay_Number.val(),
                'Payment_Type': Self.Payment_Type.val(),
                'Out_Bank': Self.Out_Bank.val(),
                'Out_Acct_No': Self.Out_Acct_No.val(),
                'Amount': Self.Amount.val(),
                'Amt': Self.Amt.val(),
                'B_Pay_Date': Self.B_Pay_Date.val(),
                'Fulfill': Self.Fulfill.val(),
                'R_Pay_Date': Self.R_Pay_Date.val(), 
                'Tax': Self.Tax.val(),
                'Sales_Memo': Self.Sales_Memo.val()
            };
            Self.InsertEvent(Data);
        }
    });
};

/**
 * 新增付款明細事件
 * @param {object.<string, number|string>} Data 資料
 */
ContractPayDetailApp.prototype.InsertEvent = function (Data) {
    let Self = this;
    this.Request.url = Window.EContractEditApp.API + 'InsertContractPayDetail';
    this.Request.method = 'POST';
    this.Request.data = JSON.stringify({ Input: Data });
    Window.AjaxApp(this.Request, function (Response) {
        if (typeof Response === 'object') {
            let Result = JSON.parse(Response.d);
            if (Result !== '') {
                if (Result.Status) {
                    Self.GetContractPayDetailData();
                    Self.InsertFormClear();
                    Self.Pay_Number.val(parseInt(Self.Pay_Number.val()) + 1);
                }
                let AlertDom = Self.ContractPayDetailFieldset.children('.alert');
                Window.EContractEditApp.AlertMessageShow(AlertDom, Result);
            }
        }
    });
};

/**
 * 新增欄位清除值
 */
ContractPayDetailApp.prototype.InsertFormClear = function () {
    this.Payment_Type.children('option').eq(0).attr('selected', true);
    this.Out_Bank.val();
    this.Out_Acct_No.val();
    this.Amount.val(1);
    this.Amt.val();
    this.B_Pay_Date.val();
    this.Fulfill.children('option').eq(0).attr('selected', true);
    this.R_Pay_Date.val();
    this.Tax.val();
    this.Sales_Memo.val();
};

/**
 * 更新點擊
 */
ContractPayDetailApp.prototype.UpdateClick = function () {
    let Self = this;
    let TbodyTr = this.Table.children('tbody').children('tr');
    for (let i = 0; i < TbodyTr.length; i++) {
        let TableBtn = TbodyTr.eq(i).children('td.table-btn');
        TableBtn.on('click', 'input[name="Update"]', function () {
            if (confirm('確定更新此筆資料?')) {
                let UpdateBlock = $(this).closest('tr');
                let ContractPayDetail_ID = UpdateBlock.children('td').eq(0).children('input[name="ContractPayDetail_ID"]').val();
                let Pay_ID = UpdateBlock.children('td').eq(0).children('input[name="Pay_ID"]').val();
                let Pay_Number = UpdateBlock.children('td').eq(1).children('input[name="Pay_Number"]').val();
                let Payment_Type = UpdateBlock.children('td').eq(2).children('select[name="Payment_Type"]').val();
                let Out_Bank = UpdateBlock.children('td').eq(3).children('input[name="Out_Bank"]').val();
                let Out_Acct_No = UpdateBlock.children('td').eq(4).children('input[name="Out_Acct_No"]').val();
                let Amount = UpdateBlock.children('td').eq(5).children('input[name="Amount"]').val();
                let Amt = UpdateBlock.children('td').eq(6).children('input[name="Amt"]').val();
                let B_Pay_Date = UpdateBlock.children('td').eq(7).children('input[name="B_Pay_Date"]').val();
                let Fulfill = UpdateBlock.children('td').eq(8).children('select[name="Fulfill"]').val();
                let R_Pay_Date = UpdateBlock.children('td').eq(9).children('input[name="R_Pay_Date"]').val();
                let Tax = UpdateBlock.children('td').eq(10).children('input[name="Tax"]').val();
                let Sales_Memo = UpdateBlock.children('td').eq(11).children('input[name="Sales_Memo"]').val();
                let Data = {
                    'ContractPayDetail_ID': ContractPayDetail_ID,
                    'Pay_ID': Pay_ID,
                    'Pay_Number': Pay_Number,
                    'Payment_Type': Payment_Type,
                    'Out_Bank': Out_Bank,
                    'Out_Acct_No': Out_Acct_No,
                    'Amount': Amount,
                    'Amt': Amt,
                    'B_Pay_Date': B_Pay_Date,
                    'Fulfill': Fulfill,
                    'R_Pay_Date': R_Pay_Date,
                    'Tax': Tax,
                    'Sales_Memo': Sales_Memo
                };
                Self.UpdateEvent(Data);
            }
        });
    }
};

/**
 * 更新事件
 * @param {object.<string, object>} Data 資料物件
 */
ContractPayDetailApp.prototype.UpdateEvent = function (Data) {
    let Self = this;
    this.Request.url = Window.EContractEditApp.API + 'UpdateContractPayDetail';
    this.Request.method = 'POST';
    this.Request.data = JSON.stringify({ Input: Data });
    Window.AjaxApp(this.Request, function (Response) {
        if (typeof Response === 'object') {
            let Result = JSON.parse(Response.d);
            if (Result !== '') {
                if (Result.Status) {
                    Self.GetContractPayDetailData();
                }
                let AlertDom = Self.ContractPayDetailFieldset.children('.alert');
                Window.EContractEditApp.AlertMessageShow(AlertDom, Result);
            }
        }
    });
};

/**
 * 刪除點擊
 */
ContractPayDetailApp.prototype.DeleteClick = function () {
    let Self = this;
    let TbodyTr = this.Table.children('tbody').children('tr');
    for (let i = 0; i < TbodyTr.length; i++) {
        let TableBtn = TbodyTr.eq(i).children('td.table-btn');
        TableBtn.on('click', 'input[name="Delete"]', function () {
            if (confirm('確定刪除此筆資料?')) {
                Self.DeleteEvent($(this));
            }
        });
    }
};

/**
 * 刪除事件
 * @param {object} Dom 刪除按鈕
 */
ContractPayDetailApp.prototype.DeleteEvent = function (Dom) {
    let Self = this;
    let ContractPayDetail_ID = Dom.siblings('input[name="ContractPayDetail_ID"]').val();
    this.Request.url = Window.EContractEditApp.API + 'DeleteContractPayDetail';
    this.Request.method = 'POST';
    this.Request.data = JSON.stringify({ 'Did': ContractPayDetail_ID });
    Window.AjaxApp(this.Request, function (Response) {
        if (typeof Response === 'object') {
            let Result = JSON.parse(Response.d);
            if (Result !== '') {
                if (Result.Status) {
                    Dom.closest('tr').remove();
                    Self.Pay_Number.val(parseInt(Self.Pay_Number.val()) - 1);
                }
                let AlertDom = Self.ContractPayDetailFieldset.children('.alert');
                Window.EContractEditApp.AlertMessageShow(AlertDom, Result);
            }
        }
    });
};

/**
 * 表格載入
 * @param {object.<string, object>} Data 資料陣列
 */
ContractPayDetailApp.prototype.TableLoad = function (Data) {
    if (Data !== null) {
        let Table = this.ContractPayDetailFieldset.children('table');
        let Tbody = Table.children('tbody');
        let TrStr = '';
        Tbody.empty();
        for (let Item in Data) {
            let PaymentTypeOptiion = Window.EContractEditApp.NewOptionObject(this.PaymentTypeData, Data[Item].Payment_Type);
            let FulfillOption = Window.EContractEditApp.NewOpetion(this.FulfillData, Data[Item].Fulfill);
            TrStr += '<tr><td class="table-btn">' +
            '<input type="hidden" name="ContractPayDetail_ID" value="' + Data[Item].ID + '" />' +
            '<input type="hidden" name="Pay_ID" value="' + Data[Item].Pay_ID + '" />' +
            '<input class="btn btn-outline-danger" name="Delete" type="button" value="刪除" />' +
            '<input class="btn btn-outline-info" name="Update" type="button" value="儲存" />' +
            '</td>' +
            '<td class="t-w-100"><input class="form-control" type="number" min="0" name="Pay_Number" value="' + Data[Item].Pay_Number + '" /></td>' +
            '<td><select class="form-control" name="Payment_Type">' + PaymentTypeOptiion + '</select></td>' +
            '<td><input class="form-control" type="text" name="Out_Bank" placeholder="例：00813" maxlength="5" value="' + Data[Item].Out_Bank + '" /></td>' +
            '<td class="t-w-180"><input class="form-control" type="text" name="Out_Acct_No" maxlength="16" placeholder="例；0000123456965456" value="' + Data[Item].Out_Acct_No + '" /></td>' +
            '<td class="t-w-100"><input class="form-control" type="number" name="Amount" min="0" value="' + Data[Item].Amount + '" /></td>' +
            '<td><input class="form-control" type="number" name="Amt" min="0" value="' + Data[Item].Amt + '" /></td>' +
            '<td class="t-w-180"><input class="form-control" type="date" name="B_Pay_Date" value="' + Data[Item].B_Pay_Date + '" /></td>' +
            '<td><select class="form-control" name="Fulfill">' + FulfillOption + '</select></td>' +
            '<td class="t-w-180"><input class="form-control" type="date" name="R_Pay_Date" value="' + Data[Item].R_Pay_Date + '" /></td>' +
            '<td><input class="form-control" type="number" name="Tax" min="0" value="' + Data[Item].Tax + '" /></td>' +
            '<td><input class="form-control" type="text" name="Sales_Memo" placeholder="備註" value="' + Data[Item].Sales_Memo + '" /></td></tr>';
        }
        Tbody.append(TrStr);
    }
    this.UpdateClick();
    this.DeleteClick();
};

/**
 * 取得付款明細資料
 */
ContractPayDetailApp.prototype.GetContractPayDetailData = function () {
    let Self = this;
    this.Request.url = Window.EContractEditApp.API + 'GetContractPayDetailData';
    this.Request.method = 'POST';
    this.Request.data = JSON.stringify({ 'Contract_ID': this.Contract_ID });
    Window.AjaxApp(this.Request, function (Response) {
        if (typeof Response === 'object') {
            let Result = JSON.parse(Response.d);
            if (Result !== '') {
                if (Result.Status) {
                    Self.TableLoad(Result.Data);
                }
            }
        }
    });
};

/**
 * 取得付款方式資料
 */
ContractPayDetailApp.prototype.GetPaymentTypeData = function () {
    let Self = this;
    this.Request.url = Window.EContractEditApp.API + 'GetPaymentTypeData';
    this.Request.method = 'GET';
    Window.AjaxApp(this.Request, function (Response) {
        if (typeof Response === 'object') {
            let Result = JSON.parse(Response.d);
            if (Result !== '') {
                if (Result.Status) {
                    Self.PaymentTypeData = Result.Data;
                }
            }
        }
    });
};

/**
 * 取得是否付款資料
 */
ContractPayDetailApp.prototype.GetFulfillData = function () {
    let Data = [
        { ID: 0, Name: '否' },
        { ID: 1, Name: '是' }
    ];
    this.FulfillData = Data;
    //let Self = this;
    //this.Request.url = Window.EContractEditApp.API + 'GetFulfillData';
    //this.Request.method = 'GET';
    //Window.AjaxApp(this.Request, function (Response) {
    //    if (typeof Response === 'object') {
    //        let Result = JSON.parse(Response.d);
    //        if (Result !== '') {
    //            if (Result.Status) {
    //                Self.FulfillData = Result.Data;
    //            }
    //        }
    //    }
    //});
};