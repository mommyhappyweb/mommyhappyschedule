﻿"use strict";
$(function () {
    Window.LoadingApp = new LoadingApp();
});

/**
 * 建構子
 */
function LoadingApp()
{
    if ($('#Loading').length < 0) return;  

    /*
     * 載入動畫DOM
     */
    this.Loading = $('#Loading').length > 0 ? $("#Loading") : $('.loading');

    this.Init();
}

/**
 * 初始化
 */
LoadingApp.prototype.Init = function () {
    this.LoadingAnimation();
};

/**
    * 載入動畫
    */
LoadingApp.prototype.LoadingAnimation = function () {
    $(document).ajaxStart(function () {
        Window.LoadingApp.LoadingShow();
    });

    $(document).ajaxStop(function () {
        Window.LoadingApp.LoadingRemove();
    });
};

/**
 * 顯示動畫
 */
LoadingApp.prototype.LoadingShow = function () {
    this.Loading.addClass('action');
};

/**
 * 移除動畫
 */
LoadingApp.prototype.LoadingRemove = function () {
    this.Loading.removeClass('action');
};
