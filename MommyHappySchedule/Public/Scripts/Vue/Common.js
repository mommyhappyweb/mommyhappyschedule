﻿var apiUrlCommon = 'http://bridge.mommyhappy.com/mommy';

function Test() {
    alert(1);
}


function getCust() {
    return this.GetJson('/getCustomerList', {
        "PageNum": 1,
        "KeySelect": 1,
        "keyword": ""

    });

}

function getHouseType() {
    return this.GetJson('/getHouseType');
}


function getSource() {
    return this.GetJson('/getSource');
}


function GetJson(url, data) {
    return AjaxCommonToJson(apiUrlCommon + url, "get", data);
}


function PostJson(url, data) {
    return AjaxCommonToJson(apiUrlCommon + url, "post", data);
}



function AjaxCommonToJson(url, sType, data = {}, async = false) {
    var result = '';
    $.ajax({
        url: url,
        type: sType,
        async: async,
        data: data,
        success: function (obj) {
            result = obj;
        },
        error: function (err) {
            result = err;
        }
    });
    return result;
}
