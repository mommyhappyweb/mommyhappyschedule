﻿using System.Web;
using System.ComponentModel;
using System.Collections.Generic;
using System.Web.Services;
using System.Web.Script.Services;
using Newtonsoft.Json;
using Models.List;
using Models.Method.Cust;
using Models.Library;

namespace MommyHappySchedule.Api
{
    /// <summary>
    ///Supervision 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
    [ScriptService]
    public class Supervision : WebService
    {
        /// <summary>
        /// 客戶合約督導方法
        /// </summary>
        private readonly CustJobScheduleSupervisionMethod CustJobScheduleSupervisionMethod = new CustJobScheduleSupervisionMethod();

        /// <summary>
        /// 型態轉換
        /// </summary>
        private readonly ConvertType ConvertType = new ConvertType();

        /// <summary>
        /// 取得打卡合約資料
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetCustJobScheduleData()
        {
            HttpContext HttpContext = HttpContext.Current;
            int Schedule_ID = ConvertType.Int(HttpContext.Session["Schedule_ID"]);
            string Staff_No = ConvertType.String(HttpContext.Session["Staff_No"]) ?? "";
            string Job_Date = ConvertType.String(HttpContext.Session["Job_Date"]) ?? "";
            byte Job_Result_ID = ConvertType.Byte(HttpContext.Session["Job_Result_ID"]);
            CustJobSchedule Input = new CustJobSchedule()
            {
                Schedule_ID = Schedule_ID,
                Staff_No = Staff_No,
                Job_Date = Job_Date,
                Job_Result_ID = Job_Result_ID
            };
            List<CustJobScheduleSupervision> Data = CustJobScheduleSupervisionMethod.GetData(Input);
            string Json = "";
            if (Data.Count > 0)
            {
                Json = JsonConvert.SerializeObject(Data);
            }
            else
            {
                Json = JsonConvert.SerializeObject(Input);
            }
            return Json;
        }

        /// <summary>
        /// 搜尋督導時間
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Search(string KeySelect = "", string Keyword = "")
        {
            List<CustJobScheduleSupervision> Data = CustJobScheduleSupervisionMethod.GetSearchData(KeySelect, Keyword);
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 新增督導時間
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Insert (List<CustJobScheduleSupervision> Input)
        {
            List<ResponseJsonData> Data = CustJobScheduleSupervisionMethod.Insert(Input);
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 刪除督導時間
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Delete(CustJobScheduleSupervision Input)
        {
            ResponseJsonData Data = CustJobScheduleSupervisionMethod.Delete(Input);
            return JsonConvert.SerializeObject(Data);
        }
    }
}
