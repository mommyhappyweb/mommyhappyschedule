﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Script.Services;
using System.Web.Services;
using Models.List;
using Models.Method.Cust;
using Models.Method.Public;
using Newtonsoft.Json;

namespace MommyHappySchedule.Api
{
    /// <summary>
    /// 合約編輯
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
    [ScriptService]
    public class EContractEdit : WebService
    {
        /// <summary>
        /// 客戶合約資料方法
        /// </summary>
        private readonly CustContractDataMethod CustContractDataMethod = new CustContractDataMethod();

        /// <summary>
        /// 客戶工作時間表
        /// </summary>
        private readonly CustJobScheduleMethod CustJobScheduleMethod = new CustJobScheduleMethod();

        /// <summary>
        /// 客戶付款明細方法
        /// </summary>
        private readonly CustContractPayDetailMethod CustContractPayDetailMethod = new CustContractPayDetailMethod();

        /// <summary>
        /// 服務狀態方法
        /// </summary>
        private readonly JobResultMethod JobResultMethod = new JobResultMethod();

        /// <summary>
        /// 付款方式方法
        /// </summary>
        private readonly PaymentTypeMethod PaymentTypeMethod = new PaymentTypeMethod();

        /// <summary>
        /// 更新全部合約關聯資料
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Update(CustContractDataList Input)
        {
            ResponseJsonData Data = CustContractDataMethod.Update(Input);
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 刪除全部合約關聯資料
        /// </summary>
        /// <param name="Did"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Delete(int Did)
        {
            ResponseJsonData Data = CustContractDataMethod.Delete(Did);
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 取得工作時間表資料
        /// </summary>
        /// <param name="Contract_ID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetJobScheduleData(string Contract_ID)
        {
            List<Dictionary<string, object>> GetData = CustJobScheduleMethod.GetDetailed(Contract_ID);
            ResponseJsonData Data = new ResponseJsonData()
            {
                Status = true,
                Data = GetData
            };
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 新增工作時間表
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string InsertJobSchedule(CustJobSchedule Input)
        {
            ResponseJsonData Data = CustJobScheduleMethod.Insert(Input);
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 更新工作時間表
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string UpdateJobSchedule(CustJobSchedule Input)
        {
            ResponseJsonData Data = CustJobScheduleMethod.Update(Input);
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 刪除工作時間表
        /// </summary>
        /// <param name="Did"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string DeleteJobSchedule(int Did)
        {
            ResponseJsonData Data = CustJobScheduleMethod.Delete(Did);
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 取得服務時段
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public string GetJobTimeData()
        {
            Dictionary<string, string> GetData = CustJobScheduleMethod.GetJobTimeData();
            ResponseJsonData Data = new ResponseJsonData()
            {
                Status = true,
                Data = GetData
            };
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 取得派班項目
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public string GetJobResultData()
        {
            List<Dictionary<string, object>> GetData = JobResultMethod.GetData();
            ResponseJsonData Data = new ResponseJsonData()
            {
                Status = true,
                Data = GetData
            };
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 取得異動狀態
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public string GetJobFlagData()
        {
            Dictionary<string, string> GetData = CustJobScheduleMethod.GetJobFlagData();
            ResponseJsonData Data = new ResponseJsonData()
            {
                Status = true,
                Data = GetData
            };
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 取得付款明細
        /// </summary>
        /// <param name="Contract_ID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetContractPayDetailData(string Contract_ID)
        {
            List<Dictionary<string, object>> GetData = CustContractPayDetailMethod.GetDetailed(Contract_ID);
            ResponseJsonData Data = new ResponseJsonData()
            {
                Status = true,
                Data = GetData
            };
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 新增付款明細
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string InsertContractPayDetail(CustContractPayDetail Input)
        {
            ResponseJsonData Data = CustContractPayDetailMethod.Insert(Input);
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 更新付款明細
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string UpdateContractPayDetail(CustContractPayDetail Input)
        {
            ResponseJsonData Data = CustContractPayDetailMethod.Update(Input);
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 刪除付款明細
        /// </summary>
        /// <param name="Did"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string DeleteContractPayDetail(int Did)
        {
            ResponseJsonData Data = CustContractPayDetailMethod.Delete(Did);
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 取得付款方式資料
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public string GetPaymentTypeData()
        {
            List<Dictionary<string, object>> GetData = PaymentTypeMethod.GetData();
            ResponseJsonData Data = new ResponseJsonData()
            {
                Status = true,
                Data = GetData
            };
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 取得是否付款資料
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public string GetFulfillData()
        {
            Dictionary<string, string> GetData = CustContractDataMethod.GetYesNoData();
            ResponseJsonData Data = new ResponseJsonData()
            {
                Status = true,
                Data = GetData
            };
            return JsonConvert.SerializeObject(Data);
        }
    }
}
