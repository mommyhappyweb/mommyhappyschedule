﻿using System.Collections.Generic;
using System.Web.Script.Services;
using System.Web.Services;
using Models.List;
using Models.Method.Staff;
using Models.Method.Public;
using Models.Method.Cust;
using Newtonsoft.Json;

namespace MommyHappySchedule.Api
{
    /// <summary>
    ///EContract 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
    [ScriptService]
    public class EContract : WebService
    {
        /// <summary>
        /// 取得部門資料
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public string GetBranchData()
        {
            BranchMethod BranchMethod = new BranchMethod();
            List<Dictionary<string, object>> GetData = BranchMethod.GetData();
            ResponseJsonData Data = new ResponseJsonData()
            {
                Status = true,
                Data = GetData
            };
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 取得部門服務金額
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public string GetBranchChargeData()
        {
            BranchChargeDataMethod BranchChargeDataMethod = new BranchChargeDataMethod();
            List<Dictionary<string, object>> GetData = BranchChargeDataMethod.GetData();
            ResponseJsonData Data = new ResponseJsonData()
            {
                Status = true,
                Data = GetData
            };
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 取得部門員工資料
        /// </summary>
        /// <param name="Branch_ID"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetBranchStaffData(string Branch_ID)
        {
            StaffJobDataMethod StaffJobDataMethod = new StaffJobDataMethod();
            List<Dictionary<string, object>> GetData = StaffJobDataMethod.GetBranchStaffData(Branch_ID);
            ResponseJsonData Data = new ResponseJsonData()
            {
                Status = true,
                Data = GetData
            };
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 取得城市資料
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public string GetCityData()
        {
            AddressMethod AddressMethod = new AddressMethod();
            List<Dictionary<string, object>> GetData = AddressMethod.GetCityData();
            ResponseJsonData Data = new ResponseJsonData()
            {
                Status = true,
                Data = GetData
            };
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 取得行政區資料
        /// </summary>
        /// <param name="City_Name"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetAreaData(string City_Name)
        {
            AddressMethod AddressMethod = new AddressMethod();
            List<Dictionary<string, object>> GetData = AddressMethod.GetAreaData(City_Name);
            ResponseJsonData Data = new ResponseJsonData()
            {
                Status = true,
                Data = GetData
            };
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 取得節日資料
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public string GetHolidayData()
        {
            HolidayDateMethod HolidayDateMethod = new HolidayDateMethod();
            List<Dictionary<string, object>> GetData = HolidayDateMethod.GetData();
            ResponseJsonData Data = new ResponseJsonData()
            {
                Status = true,
                Data = GetData
            };
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 取得付款方式資料
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public string GetPaymentTypeData()
        {
            PaymentTypeMethod PaymentTypeMethod = new PaymentTypeMethod();
            List<Dictionary<string, object>> GetData = PaymentTypeMethod.GetData();
            ResponseJsonData Data = new ResponseJsonData()
            {
                Status = true,
                Data = GetData
            };
            return JsonConvert.SerializeObject(Data);
        }
        /// <summary>
        /// 取得派班狀態資料
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public string GetJobResultData()
        {
            JobResultMethod JobResultMethod = new JobResultMethod();
            List<Dictionary<string, object>> GetData = JobResultMethod.GetData();
            ResponseJsonData Data = new ResponseJsonData()
            {
                Status = true,
                Data = GetData
            };
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 取得銀行資料
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
        public string GetBankData()
        {
            BankDataMethod BankDataMethod = new BankDataMethod();
            List<Dictionary<string, object>> GetData = BankDataMethod.GetData();
            ResponseJsonData Data = new ResponseJsonData()
            {
                Status = true,
                Data = GetData
            };
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 新增合約關聯資料
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string InsertContractData(CustContractDataList Input)
        {
            CustContractDataMethod CustContractDataMethod = new CustContractDataMethod();
            ResponseJsonData Data = CustContractDataMethod.Insert(Input);
            return JsonConvert.SerializeObject(Data);
        }

        /// <summary>
        /// 取得單次合約資料
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetSingleContractScheduleData(CustContractData Input)
        {
            CustContractDataMethod CustContractDataMethod = new CustContractDataMethod();
            List<Dictionary<string, object>> GetData = CustContractDataMethod.GetSingleContractSchedule(Input);
            ResponseJsonData Data = new ResponseJsonData()
            {
                Status = true,
                Data = GetData
            };
            return JsonConvert.SerializeObject(Data);
        }
    }
}
