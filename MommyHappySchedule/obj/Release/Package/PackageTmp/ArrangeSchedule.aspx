﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ArrangeSchedule.aspx.cs" Inherits="MommyHappySchedule.ArrangeSchedule" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>電子班表</title>
    <style type="text/css">
        .auto-style1 {
            height: 112px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <center>
                <table>
                    <tr>
                        <td align="center">
                            <table>

                                <tr>

                                    <td colspan="3" class="auto-style1">
                                        <table border="1" cellpadding="1" cellspacing="0">
                                            <tr style="font-size: 10pt; font-family: Arial;">
                                                <td>駐點：</td>
                                                <td>
                                                    <asp:DropDownList ID="Branch" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>年份</td>
                                                <td>
                                                    <asp:DropDownList ID="Check_Year" runat="server">
                                                        <asp:ListItem>2016</asp:ListItem>
                                                        <asp:ListItem>2017</asp:ListItem>
                                                        <asp:ListItem Selected="True">2018</asp:ListItem>
                                                        <asp:ListItem>2019</asp:ListItem>
                                                        <asp:ListItem>2020</asp:ListItem>
                                                        <asp:ListItem>2021</asp:ListItem>
                                                    </asp:DropDownList></td>
                                                <td>月份</td>
                                                <td>
                                                    <asp:DropDownList ID="Check_Month" runat="server">
                                                        <asp:ListItem Selected="True">1</asp:ListItem>
                                                        <asp:ListItem>2</asp:ListItem>
                                                        <asp:ListItem>3</asp:ListItem>
                                                        <asp:ListItem>4</asp:ListItem>
                                                        <asp:ListItem>5</asp:ListItem>
                                                        <asp:ListItem>6</asp:ListItem>
                                                        <asp:ListItem>7</asp:ListItem>
                                                        <asp:ListItem>8</asp:ListItem>
                                                        <asp:ListItem>9</asp:ListItem>
                                                        <asp:ListItem>10</asp:ListItem>
                                                        <asp:ListItem>11</asp:ListItem>
                                                        <asp:ListItem>12</asp:ListItem>
                                                    </asp:DropDownList></td>
                                                <td>
                                                    <asp:Button ID="Check_Report" runat="server" Text="產生報表" OnClick="Check_Report_Click" /></td>
                                                <td>
                                                    <asp:Label ID="Label2" runat="server" Text="" Visible ="false"></asp:Label>
                                                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="Index.aspx">返回主選單</asp:HyperLink>
                                                </td>
                                                <td colspan="2">
                                                    <asp:TextBox ID="TextBox1" runat="server" Width="80px" Visible="false"></asp:TextBox>
                                                    <asp:Label ID="Label5" runat="server" Text="0.00" Visible ="false"></asp:Label>
                                                    編號/姓名：<asp:TextBox ID="SearchText" runat="server" Width="80px" ></asp:TextBox>
                                                    <asp:Button ID="Search" runat="server" Text="搜尋" OnClick="Search_Click" />
                                                    <asp:Button ID="Left_Screen" runat="server" Text="<<" OnClick="Change_Screen" /><asp:Button ID="Right_Screen" runat="server" Text=">>" OnClick="Change_Screen" />

                                                </td>

                                            </tr>
                                            <tr style="font-size: 10pt; font-family: Arial;">
                                                <td colspan="4">
                                                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" Visible="False" AutoPostBack="true" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged">
                                                        <asp:ListItem>換客戶(單)</asp:ListItem>
                                                        <asp:ListItem>換管家(單)</asp:ListItem>
                                                        <asp:ListItem>換管家(整)</asp:ListItem>
                                                        <asp:ListItem>輸入單次備註</asp:ListItem>
                                                        <asp:ListItem>換客戶(整)</asp:ListItem>
                                                        <asp:ListItem>設定顏色</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td colspan="4">
                                                    <asp:DropDownList ID="SelectMonth" runat="server" Visible="False" AutoPostBack="true" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="DropDownList1" runat="server" Visible="False" AutoPostBack="true" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                                                        <asp:ListItem>1</asp:ListItem>
                                                        <asp:ListItem>2</asp:ListItem>
                                                        <asp:ListItem>3</asp:ListItem>
                                                        <asp:ListItem>4</asp:ListItem>
                                                        <asp:ListItem>5</asp:ListItem>
                                                        <asp:ListItem>6</asp:ListItem>
                                                        <asp:ListItem>7</asp:ListItem>
                                                        <asp:ListItem>8</asp:ListItem>
                                                        <asp:ListItem>9</asp:ListItem>
                                                        <asp:ListItem>10</asp:ListItem>
                                                        <asp:ListItem>11</asp:ListItem>
                                                        <asp:ListItem>12</asp:ListItem>
                                                        <asp:ListItem>13</asp:ListItem>
                                                        <asp:ListItem>14</asp:ListItem>
                                                        <asp:ListItem>15</asp:ListItem>
                                                        <asp:ListItem>16</asp:ListItem>
                                                        <asp:ListItem>17</asp:ListItem>
                                                        <asp:ListItem>18</asp:ListItem>
                                                        <asp:ListItem>19</asp:ListItem>
                                                        <asp:ListItem>20</asp:ListItem>
                                                        <asp:ListItem>21</asp:ListItem>
                                                        <asp:ListItem>22</asp:ListItem>
                                                        <asp:ListItem>23</asp:ListItem>
                                                        <asp:ListItem>24</asp:ListItem>
                                                        <asp:ListItem>25</asp:ListItem>
                                                        <asp:ListItem>26</asp:ListItem>
                                                        <asp:ListItem>27</asp:ListItem>
                                                        <asp:ListItem>28</asp:ListItem>
                                                        <asp:ListItem>29</asp:ListItem>
                                                        <asp:ListItem>30</asp:ListItem>
                                                        <asp:ListItem>31</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="DropDownList4" runat="server" Visible="False" AutoPostBack="true" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="DropDownList2" runat="server" Visible="False">
                                                    </asp:DropDownList>
                                                    <asp:HyperLink ID="HyperLink2" runat="server" Visible="false" Target="_blank"></asp:HyperLink>
                                                    <asp:CheckBox ID="CheckBox1" runat="server" Text="是否扣次" Visible="False" />
                                                    <asp:CheckBox ID="CheckBox2" runat="server" Text="是否順延" Visible="False" />
                                                    <asp:CheckBox ID="CheckBox4" runat="server" Visible="False" Text="上午"  AutoPostBack="true" OnCheckedChanged="DropDownList1_SelectedIndexChanged" />
                                                     <asp:CheckBox ID="CheckBox5" runat="server" Visible="False" Text="隔周換"  />
                                                   <asp:DropDownList ID="DropDownList3" runat="server" RepeatDirection="Horizontal" Visible="false">
                                                        <asp:ListItem>先不排</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:TextBox ID="JobNote" runat="server" Visible="false" MaxLength="50" TextMode="MultiLine"></asp:TextBox>
                                                    <asp:DropDownList ID="DropDownList5" Visible ="false"  runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownList5_SelectedIndexChanged">
                                                        <asp:ListItem >請選顏色</asp:ListItem>
                                                        <asp:ListItem Value="Orange">橙色</asp:ListItem>
                                                        <asp:ListItem Value="PaleGreen">淺綠</asp:ListItem>
                                                        <asp:ListItem Value="BlanchedAlmond">淺黃</asp:ListItem>
                                                        <asp:ListItem Value="LightPink">淺粉紅</asp:ListItem>
                                                        <asp:ListItem Value="LightSkyBlue">淺天藍</asp:ListItem>
                                                        <asp:ListItem Value="Red">紅色</asp:ListItem>
                                                        <asp:ListItem Value="Chocolate">深橙色</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>

                                                <td style="text-align: center" colspan="2">
                                                    <asp:CheckBox ID="CheckBox3" runat="server" Text="若有重覆記錄則覆蓋" Visible="False" />
                                                    <asp:Button ID="Button5" runat="server" Text="確定修改" Visible="False"  OnClick="Button5_Click" />
                                                    <asp:Button ID="Button6" runat="server" Text="取消" Visible="False" OnClick ="Button6_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:DataGrid ID="DataGrid3" runat="server" Font-Names="Arial"
                                            BorderWidth="1" Font-Size="Smaller" CellPadding="2" AutoGenerateColumns="False"
                                            OnItemCommand="Show_Job_Detail" OnItemDataBound="Show_Sc_Type" OnPageIndexChanged="DataGrid_PageIndexChanged"
                                            ForeColor="#333333" GridLines="None" AllowCustomPaging="False" AllowPaging="False" PagerStyle-NextPageText="下一頁" PagerStyle-PrevPageText="前一頁" PagerStyle-Mode="NumericPages" PageSize="30" >
                                            <SelectedItemStyle Font-Bold="True" ForeColor="#333333" BackColor="#C5BBAF"></SelectedItemStyle>
                                            <ItemStyle BackColor="#E3EAEB"></ItemStyle>
                                            <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#1C5E55"></HeaderStyle>
                                            <FooterStyle ForeColor="White" BackColor="#1C5E55" Font-Bold="True"></FooterStyle>
                                            <Columns>
                                                <asp:BoundColumn DataField="ID" HeaderText="職稱">
                                                    <ItemStyle Width="30px" />
                                                </asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="管家" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="HK_Name" runat="server" Text='<%# Bind("HK_Name") %>' ToolTip='<%# Bind("HK_Data") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                               <%-- <asp:BoundColumn DataField="HK_Name" HeaderText="管家" HeaderStyle-HorizontalAlign="Center" >
                                                    <ItemStyle Width="100px"></ItemStyle>
                                                </asp:BoundColumn>--%>
                                                <asp:BoundColumn DataField="Day_ID" HeaderText="" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle Width="20px"></ItemStyle>
                                                </asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="1" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B1" runat="server" OnClick="Check_Sc" Text='<%# Bind("B1") %>' ToolTip='<%# Eval("B1_1") + "\n" + Eval("B1_2") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="2" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B2" runat="server" OnClick="Check_Sc" Text='<%# Bind("B2") %>' ToolTip='<%# Eval("B2_1") + "\n" + Eval("B2_2") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="3" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B3" runat="server" OnClick="Check_Sc" Text='<%# Bind("B3") %>' ToolTip='<%# Eval("B3_1") + "\n" + Eval("B3_2") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="4" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B4" runat="server" OnClick="Check_Sc" Text='<%# Bind("B4") %>' ToolTip='<%# Eval("B4_1") + "\n" + Eval("B4_2") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="5" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B5" runat="server" OnClick="Check_Sc" Text='<%# Bind("B5") %>' ToolTip='<%# Eval("B5_1") + "\n" + Eval("B5_2") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="6" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B6" runat="server" OnClick="Check_Sc" Text='<%# Bind("B6") %>' ToolTip='<%# Eval("B6_1") + "\n" + Eval("B6_2") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="7" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B7" runat="server" OnClick="Check_Sc" Text='<%# Bind("B7") %>' ToolTip='<%# Eval("B7_1") + "\n" + Eval("B7_2") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="8" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B8" runat="server" OnClick="Check_Sc" Text='<%# Bind("B8") %>' ToolTip='<%# Eval("B8_1") + "\n" + Eval("B8_2") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="9" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B9" runat="server" OnClick="Check_Sc" Text='<%# Bind("B9") %>' ToolTip='<%# Eval("B9_1") + "\n" + Eval("B9_2") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="10" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B10" runat="server" OnClick="Check_Sc" Text='<%# Bind("B10") %>' ToolTip='<%# Eval("B10_1") + "\n" + Eval("B10_2") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="11" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B11" runat="server" OnClick="Check_Sc" Text='<%# Bind("B11") %>' ToolTip='<%# Eval("B11_1") + "\n" + Eval("B11_2") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="12" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B12" runat="server" OnClick="Check_Sc" Text='<%# Bind("B12") %>' ToolTip='<%# Eval("B12_1") + "\n" + Eval("B12_2") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="13" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B13" runat="server" OnClick="Check_Sc" Text='<%# Bind("B13") %>' ToolTip='<%# Eval("B13_1") + "\n" + Eval("B13_2") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="14" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B14" runat="server" OnClick="Check_Sc" Text='<%# Bind("B14") %>' ToolTip='<%# Eval("B14_1") + "\n" + Eval("B14_2") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>

                                            <EditItemStyle BackColor="#7C6F57" />
                                            <AlternatingItemStyle BackColor="White" />
                                        </asp:DataGrid>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:GridView ID="GridView1" runat="server" AllowCustomPaging="true"></asp:GridView>
                            <asp:TextBox ID="TextBox5" runat="server" Width="100%"></asp:TextBox></td>
                    </tr>
                </table>
            </center>
        </div>
    </form>
</body>
</html>

