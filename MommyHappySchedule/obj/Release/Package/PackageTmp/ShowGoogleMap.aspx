﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowGoogleMap.aspx.cs" Inherits="MommyHappySchedule.ShowGoogleMap" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <!--<script type="text/javascript" src="Scripts/jquery-3.3.1.min.js"></script>-->
    <!--要玩Google Map 就一定要引用此js-->
    <script async="async" defer="defer"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCMyvr47tuN5E0bwEgemLMaT-fvbztedV4&callback=initMap">
    </script>
    <script type="text/javascript">
        
        //網頁上所有的DOM都載入後
        function initMap() {
            <%--，這裡可以抓Code-Behind的 protected string zip_no;來替代--%> 
            var zip_no = QueryString("ID");

            //alert(zip_no);
            //取得標記點資料
            getMapData(zip_no);
        }

        function QueryString(name) {
            var AllVars = window.location.search.substring(1);
            var Vars = AllVars.split("&");
            for (i = 0; i < Vars.length; i++) {
                var Var = Vars[i].split("=");
                if (Var[0] == name) return Var[1];
            }
            return "";
        }

        //取得標記點資料
        function getMapData(zip_no) {
            //$.ajax(
            //    {
            //        url: 'getSpot.ashx',
            //        type: 'post',
            //        async: false,
            //        data: { zip_no: zip_no },
            //        dataType: 'json',
            //        success: addMarker,//在某一區加入多個標記點
            //        error: function (e) {
            //            alert("Google地圖發生錯誤，無法載入");
            //        }
            //    });//End jQuery Ajax
            var url = 'getSpot.ashx?zip_no=' + zip_no;
            var xhr = new XMLHttpRequest();
            xhr.open('get', url , true);
            xhr.send(null);
            xhr.onload = function () {
                var str_json = JSON.parse(xhr.responseText);
                addMarker(str_json);
            }
        } //End function getMapData(zip_no)
       
        //在某一區加入多個標記點
        function addMarker(str_json) {
            //是否為第一次執行迴圈
            var map;
            var first = true;

            for (var index in str_json) {

                //建立緯經度座標物件
                var latlng = new google.maps.LatLng(str_json[index].lat, str_json[index].lng);

                if (first) {
                    map = new google.maps.Map(document.getElementById('Map'), {
                        center: latlng,   // 22.6687945, 120.3058869 中高雄位置
                        zoom: 13
                    });
                    var marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        color: '#0000FF',
                        title: str_json[index].name
                    });
                    first = false;
                }
                else {
                    //alert(str_json[index].lat);
                    //加入一個Marker到map中
                    if (str_json[index].Type == 0)
                    {
                        var marker = new google.maps.Marker({
                            position: latlng,
                            map: map,
                            icon: {
                                path: google.maps.SymbolPath.CIRCLE,
                                strokeColor: '#FF0000',
                                fillColor: '#FF0000',
                                fillOpacity: 0.35,
                                strokeOpacity: 1.0,
                                strokeWeight: 3,
                                scale: 2
                            },
                            title: str_json[index].name
                        });
                    }
                    else
                    {
                    var marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        icon: {
                            path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                            strokeColor: '#0000FF',
                            fillColor: '#0000FF',
                            fillOpacity: 0.35,
                            strokeOpacity: 1.0,
                            strokeWeight: 3,
                            scale: 2
                        },
                        title: str_json[index].name
                        });
                    }
                }
            } //End for (var index in str_json) 
        }//End function addMarker()
    </script>
</head>
<body>
    <form id="form1" runat="server">

    <!--顯示Google Map的區塊-->
    <div id="Map" style="width:100%; height:560px;">
    </div>
    </form>
   
</body>
</html>