﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReNewReport.aspx.cs" Inherits="MommyHappySchedule.ReNewReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>專員合約成長報表</title>
    <style type="text/css">
        .auto-style1 {
            border-style: solid;
            border-width: 1px;
            padding: 1px 4px;
            text-align: center;
        }

        .auto-style2 {
            border-style: solid;
            border-width: 1px;
            padding: 1px 4px;
            text-align: center;
        }

        .auto-style3 {
            border-style: solid;
            border-width: 1px;
            padding: 1px 4px;
            text-align: center;
            width: 50px;
        }
    </style>
</head>
<body>

    <form id="form1" runat="server">


        <h3>&nbsp;</h3>


        <table class="auto-style1">
            <tr>
                <td>駐點：</td>
                <td>
                    <asp:DropDownList ID="Branch" runat="server">
                    </asp:DropDownList>
                </td>
                <td>年份</td>
                <td>
                    <asp:DropDownList ID="Check_Year" runat="server">
                        <asp:ListItem>2016</asp:ListItem>
                        <asp:ListItem Selected="True">2017</asp:ListItem>
                        <asp:ListItem>2018</asp:ListItem>
                        <asp:ListItem>2019</asp:ListItem>
                        <asp:ListItem>2020</asp:ListItem>
                        <asp:ListItem>2021</asp:ListItem>
                    </asp:DropDownList></td>
                <td>月份</td>
                <td>
                    <asp:DropDownList ID="Check_Month" runat="server"  Visible ="false"  AutoPostBack="true" OnSelectedIndexChanged ="Check_Month_SelectedIndexChanged">
                        <asp:ListItem Selected="True">1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                    </asp:DropDownList></td>
                <td>
                    <asp:Button ID="Check_Report" runat="server" Text="產生報表" OnClick="Check_Report_Click" /></td>
                <td colspan="6">
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="Index.aspx">返回主選單</asp:HyperLink></td>
            </tr>
            <tr>
                <td colspan="5">

                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                        <asp:ListItem Selected="True">統計表</asp:ListItem>
                        <asp:ListItem>專員績效表</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td bgcolor="#CCCCCC">報表負責人：</td>
                <td >
                    <asp:ImageMap ID="ImageMap1" runat="server"></asp:ImageMap></td>
                <td>
                    <asp:Label ID="Label44" runat="server" Text=""></asp:Label></td>
            </tr>
        </table>
        <table class="auto-style1">
           
               
                
                    <tr>
                        <td colspan="13">
                            <table>
                                 
                                <tr>
                                    <td class="auto-style1">單位</td>
                                    <td class="auto-style1" bgcolor="#FFFF99">專員</td>
                                    <td class="auto-style1" bgcolor="#CCFFFF" colspan="15">全年度</td>
                                    <asp:Panel ID="ViewT01" runat="server"><td class="auto-style1" bgcolor="#66CCFF" colspan="15">一月</td></asp:Panel> 
                                    <asp:Panel ID="ViewT02" runat="server"><td class="auto-style1" bgcolor="#FFCC99" colspan="15">二月</td></asp:Panel>
                                    <asp:Panel ID="ViewT03" runat="server"><td class="auto-style1" bgcolor="#CCFF99" colspan="15">三月</td></asp:Panel>
                                    <asp:Panel ID="ViewT04" runat="server"><td class="auto-style1" bgcolor="#FFFF99" colspan="15">四月</td></asp:Panel>
                                    <asp:Panel ID="ViewT05" runat="server"><td class="auto-style1" bgcolor="#66FF33" colspan="15">五月</td></asp:Panel>
                                    <asp:Panel ID="ViewT06" runat="server"><td class="auto-style1" bgcolor="#CCFFFF" colspan="15">六月</td></asp:Panel>
                                    <asp:Panel ID="ViewT07" runat="server"><td class="auto-style1" bgcolor="#66CCFF" colspan="15">七月</td></asp:Panel>
                                    <asp:Panel ID="ViewT08" runat="server"><td class="auto-style1" bgcolor="#FFCC99" colspan="15">八月</td></asp:Panel>
                                    <asp:Panel ID="ViewT09" runat="server"><td class="auto-style1" bgcolor="#CCFF99" colspan="15">九月</td></asp:Panel>
                                    <asp:Panel ID="ViewT10" runat="server"><td class="auto-style1" bgcolor="#FFFF99" colspan="15">十月</td></asp:Panel>
                                    <asp:Panel ID="ViewT11" runat="server"><td class="auto-style1" bgcolor="#66FF33" colspan="15">十一月</td></asp:Panel>
                                    <asp:Panel ID="ViewT12" runat="server"><td class="auto-style1" bgcolor="#CCFFFF" colspan="15">十二月</td></asp:Panel>

                                </tr>
                                <asp:Repeater ID="Repeater2" runat="server" >
                                    <HeaderTemplate>
                                        <tr>
                                            <td class="auto-style1">單位</td>
                                            <td class="auto-style1" bgcolor="#FFFF99">專員</td>

                                            <td class="auto-style1" bgcolor="#CCFFFF">負責客數</td>
                                            <td class="auto-style1" bgcolor="#CCFFFF">負責次數</td>
                                            <td class="auto-style1" bgcolor="#CCFFFF">新約次數</td>
                                            <td class="auto-style1" bgcolor="#CCFFFF">解約次數</td>
                                            <td class="auto-style1" bgcolor="#CCFFFF">成長次數</td>
                                            <td class="auto-style1" bgcolor="#CCFFFF">到期客數</td>
                                            <td class="auto-style1" bgcolor="#CCFFFF">到期次數</td>
                                            <td class="auto-style1" bgcolor="#CCFFFF">續約筆數</td>
                                            <td class="auto-style1" bgcolor="#CCFFFF">解約筆數</td>
                                            <td class="auto-style1" bgcolor="#CCFFFF">中途解約</td>
                                            <td class="auto-style1" bgcolor="#CCFFFF">合約異動</td>
                                            <td class="auto-style1" bgcolor="#CCFFFF">二年約數</td>
                                            <td class="auto-style1" bgcolor="#CCFFFF">解約次率</td>
                                            <td class="auto-style1" bgcolor="#CCFFFF">解約客率</td>
                                            <td class="auto-style1" bgcolor="#CCFFFF">續約進度</td>

                                            <td class="auto-style1" bgcolor="#66CCFF">負責客數</td>
                                            <td class="auto-style1" bgcolor="#66CCFF">負責次數</td>
                                            <td class="auto-style1" bgcolor="#66CCFF">新約次數</td>
                                            <td class="auto-style1" bgcolor="#66CCFF">解約次數</td>
                                            <td class="auto-style1" bgcolor="#66CCFF">成長次數</td>
                                            <td class="auto-style1" bgcolor="#66CCFF">到期客數</td>
                                            <td class="auto-style1" bgcolor="#66CCFF">到期次數</td>
                                            <td class="auto-style1" bgcolor="#66CCFF">續約筆數</td>
                                            <td class="auto-style1" bgcolor="#66CCFF">解約筆數</td>
                                            <td class="auto-style1" bgcolor="#66CCFF">中途解約</td>
                                            <td class="auto-style1" bgcolor="#66CCFF">合約異動</td>
                                            <td class="auto-style1" bgcolor="#66CCFF">二年約數</td>
                                            <td class="auto-style1" bgcolor="#66CCFF">解約次率</td>
                                            <td class="auto-style1" bgcolor="#66CCFF">解約客率</td>
                                            <td class="auto-style1" bgcolor="#66CCFF">續約進度</td>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td class="auto-style1">
                                                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ID") %>' Width="80px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Ex_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EX_Name") %>' Width="60px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cu_T") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label3" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cn_T") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label4" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ne_Cn_T") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label5" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cn_T") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label6" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Gr_Cn_T") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label7" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cu_T") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label8" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cn_T") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label9" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Re_Cu_T") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label10" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cu_T") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label11" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ct_Cu_T") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label12" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NF_Cu_T") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label13" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Tw_Cn_T") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label14" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cu_T") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label15" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cn_T") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label16" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rn_Cu_T") %>' Width="40px"></asp:Label>
                                            </td>

                                            <asp:Panel ID="ViewB01" runat="server"><td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label17" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cu_1") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label18" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cn_1") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label19" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ne_Cn_1") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label20" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cn_1") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label21" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Gr_Cn_1") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label22" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cu_1") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label23" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cn_1") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label24" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Re_Cu_1") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label25" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cu_1") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label26" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ct_Cu_1") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label27" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NF_Cu_1") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label28" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Tw_Cn_1") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label29" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cu_1") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label30" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cn_1") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label31" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rn_Cu_1") %>' Width="40px"></asp:Label>
                                            </td></asp:Panel> 

                                            <asp:Panel ID="ViewB02" runat="server"><td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label32" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cu_2") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label33" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cn_2") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label34" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ne_Cn_2") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label35" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cn_2") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label36" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Gr_Cn_2") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label37" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cu_2") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label38" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cn_2") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label39" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Re_Cu_2") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label40" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cu_2") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label41" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ct_Cu_2") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label42" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NF_Cu_2") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label43" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Tw_Cn_2") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label45" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cu_2") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label46" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cn_2") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label47" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rn_Cu_2") %>' Width="40px"></asp:Label>
                                            </td></asp:Panel> 

                                            <asp:Panel ID="ViewB03" runat="server"><td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label48" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cu_3") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label49" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cn_3") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label50" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ne_Cn_3") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label51" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cn_3") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label52" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Gr_Cn_3") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label53" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cu_3") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label54" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cn_3") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label55" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Re_Cu_3") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label56" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cu_3") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label57" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ct_Cu_3") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label58" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NF_Cu_3") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label59" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Tw_Cn_3") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label60" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cu_3") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label61" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cn_3") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label62" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rn_Cu_3") %>' Width="40px"></asp:Label>
                                            </td></asp:Panel> 

                                            <asp:Panel ID="ViewB04" runat="server"><td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label63" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cu_4") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label64" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cn_4") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label65" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ne_Cn_4") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label66" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cn_4") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label67" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Gr_Cn_4") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label68" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cu_4") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label69" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cn_4") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label70" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Re_Cu_4") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label71" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cu_4") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label72" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ct_Cu_4") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label73" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NF_Cu_4") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label74" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Tw_Cn_4") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label75" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cu_4") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label76" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cn_4") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label77" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rn_Cu_4") %>' Width="40px"></asp:Label>
                                            </td></asp:Panel> 

                                            <asp:Panel ID="ViewB05" runat="server"><td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label78" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cu_5") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label79" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cn_5") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label80" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ne_Cn_5") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label81" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cn_5") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label82" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Gr_Cn_5") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label83" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cu_5") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label84" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cn_5") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label85" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Re_Cu_5") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label86" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cu_5") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label87" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ct_Cu_5") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label88" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NF_Cu_5") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label89" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Tw_Cn_5") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label90" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cu_5") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label91" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cn_5") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label92" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rn_Cu_5") %>' Width="40px"></asp:Label>
                                            </td></asp:Panel> 

                                            <asp:Panel ID="ViewB06" runat="server"><td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label93" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cu_6") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label94" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cn_6") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label95" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ne_Cn_6") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label96" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cn_6") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label97" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Gr_Cn_6") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label98" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cu_6") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label99" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cn_6") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label100" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Re_Cu_6") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label101" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cu_6") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label102" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ct_Cu_6") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label103" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NF_Cu_6") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label104" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Tw_Cn_6") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label105" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cu_6") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label106" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cn_6") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label107" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rn_Cu_6") %>' Width="40px"></asp:Label>
                                            </td></asp:Panel> 

                                            <asp:Panel ID="ViewB07" runat="server"><td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label108" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cu_7") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label109" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cn_7") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label110" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ne_Cn_7") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label111" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cn_7") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label112" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Gr_Cn_7") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label113" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cu_7") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label114" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cn_7") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label115" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Re_Cu_7") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label116" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cu_7") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label117" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ct_Cu_7") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label118" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NF_Cu_7") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label119" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Tw_Cn_7") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label120" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cu_7") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label121" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cn_7") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label122" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rn_Cu_7") %>' Width="40px"></asp:Label>
                                            </td></asp:Panel> 

                                            <asp:Panel ID="ViewB08" runat="server"><td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label123" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cu_8") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label124" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cn_8") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label125" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ne_Cn_8") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label126" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cn_8") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label127" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Gr_Cn_8") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label128" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cu_8") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label129" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cn_8") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label130" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Re_Cu_8") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label131" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cu_8") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label132" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ct_Cu_8") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label133" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NF_Cu_8") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label134" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Tw_Cn_8") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label135" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cu_8") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label136" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cn_8") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label137" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rn_Cu_8") %>' Width="40px"></asp:Label>
                                            </td></asp:Panel> 

                                            <asp:Panel ID="ViewB09" runat="server"><td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label138" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cu_9") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label139" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cn_9") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label140" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ne_Cn_9") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label141" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cn_9") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label142" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Gr_Cn_9") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label143" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cu_9") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label144" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cn_9") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label145" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Re_Cu_9") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label146" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cu_9") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label147" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ct_Cu_9") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label148" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NF_Cu_9") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label149" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Tw_Cn_9") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label150" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cu_9") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label151" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cn_9") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label152" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rn_Cu_9") %>' Width="40px"></asp:Label>
                                            </td></asp:Panel> 

                                            <asp:Panel ID="ViewB10" runat="server"><td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label153" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cu_A") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label154" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cn_A") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label155" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ne_Cn_A") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label156" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cn_A") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label157" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Gr_Cn_A") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label158" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cu_A") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label159" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cn_A") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label160" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Re_Cu_A") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label161" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cu_A") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label162" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ct_Cu_A") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label163" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NF_Cu_A") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label164" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Tw_Cn_A") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label165" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cu_A") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label166" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cn_A") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label167" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rn_Cu_A") %>' Width="40px"></asp:Label>
                                            </td></asp:Panel> 

                                            <asp:Panel ID="ViewB11" runat="server"><td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label168" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cu_B") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label169" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cn_B") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label170" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ne_Cn_B") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label171" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cn_B") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label172" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Gr_Cn_B") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label173" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cu_B") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label174" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cn_B") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label175" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Re_Cu_B") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label176" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cu_B") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label177" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ct_Cu_B") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label178" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NF_Cu_B") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label179" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Tw_Cn_B") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label180" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cu_B") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label181" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cn_B") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label182" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rn_Cu_B") %>' Width="40px"></asp:Label>
                                            </td></asp:Panel> 

                                             <asp:Panel ID="ViewB12" runat="server"><td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label183" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cu_C") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label184" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cr_Cn_C") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label185" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ne_Cn_C") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label186" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cn_C") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label187" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Gr_Cn_C") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label188" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cu_C") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label189" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ex_Cn_C") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label190" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Re_Cu_C") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label191" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Cl_Cu_C") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label192" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Ct_Cu_C") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label193" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "NF_Cu_C") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label194" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Tw_Cn_C") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label195" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cu_C") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label196" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rc_Cn_C") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label197" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Rn_Cu_C") %>' Width="40px"></asp:Label>
                                            </td></asp:Panel> 
                                        </tr>

                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </td>
                    </tr>
             

        </table>

    </form>
</body>
</html>

