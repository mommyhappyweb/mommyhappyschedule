﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="MommyHappySchedule.Index" MasterPageFile="~/Template/Layout.Master" %>
<asp:Content ID="Style" ContentPlaceHolderID="Style" runat="server"></asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="Content" runat="server">
    <div class="container-fluid">
        <article class="alert alert-warning mt-5" role="alert">
            <header>
                <h1 class="alert-heading">系統更新</h1>
            </header>
            <hr />
            <section>
                <h3><time>2019-03-26</time>預計更新</h3>
                <h4>新增功能</h4>
                <p>1.新版新增合約</p>
                <p>包含金額稅率計算，上、下午班制，派班彈性</p>
                <p>新約、續約、加次、單次轉合約、年期計算</p>
                <p>2.新增駐點回覆連結</p>
                <hr />
                <h4>介面調整</h4>
                <p>1.管家留任率新版介面UI修正</p>
                <p>2.新增合約UI</p>
                <hr />
                <h4>系統除錯</h4>
                <p>1.修正留任率報表</p>
                <p>2.修正非強制輸入住家或公司電話</p>
                <p>3.修正非強制合約編號</p>
                <p>4.加次改為輸入合約ID(查詢方式合約編輯列表那邊可顯示此合約ID)與客戶編號</p>
            </section>
            <section>
                <h3><time>2019-03-01</time>預計更新</h3>
                <h4>新增功能</h4>
                <p>1.新增專員電子排班資料</p>
                <p>2.新增專員公督訓</p>
                <hr />
                <h4>系統除錯</h4>
                <p>1.修正一些系統錯誤</p>
                <p>2.修正合約招募分析數據顯示問題</p>
                <p>3.修正電子班表排序問題</p>
                <p>4.修正留任率報表</p>
            </section>
            <section>
                <h3><time>2019-02-22</time>預計更新</h3>
                <h4>新增功能</h4>
                <p>1.新增合約派班狀態資料統計</p>
                <p>2.新增服務周次項目</p>
                <p>3.新增合約部門初始載入預設值</p>
                <hr />
                <h4>系統除錯</h4>
                <p>1.修正合約資料，部門人事顯示資料錯誤</p>
                <p>2.修正管家留任率資料統計錯誤</p>
                <p>3.修正專員績效報表</p>
                <p>4.修正招募分析表</p>
                <p>5.修正管家留任率資料統計錯誤</p>
                <p>6.修正大量人事資料</p>
            </section>
            <section>
                <h3><time>2019-02-15</time>預計更新</h3>
                <h4>介面調整</h4>
                <p>1.電子班表UI修正</p>
                <hr />
                <h4>系統除錯</h4>
                <p>1.派班工作批次確認調整可還原預設值</p>
                <p>2.派班項目未能正確顯示</p>
            </section>
            <section>
                <h3><time>2019-01-31</time>預計更新</h3>
                <h4>系統除錯</h4>
                <p>1.公督訓錯誤</p>
                <p>2.派班工作狀態誤植</p>
                <p>3.人事資料比對完整性</p>
                <p>4.人事資料匯檔修正</p>
                <p>5.移除管家順序、狀態</p>
                <p>6.修正電子班表回駐點顯示正常</p>
                <hr />
                <h4>介面調整</h4>
                <p>1.修改管家打卡紀錄UI修正</p>
            </section>
            <section>
                <h3><time>2019-01-25</time>預計更新</h3>
                <h4>新增功能</h4>
                <p>1.新增刪除合約相關資料並清空班表及付款資料、打卡紀錄與督導紀錄</p>
                <p>2.新增<a href="/EContractList.aspx" class="alert-link">合約內容列表</a></p>
                <p>3.新增合約內容編輯</p>
                <p>4.新增合約時間表新增、修改、刪除</p>
                <p>5.新增合約付款人資訊修改</p>
                <p>6.新增合約付款明細新增、修改、刪除</p>
                <hr />
                <h4>介面調整</h4>
                <p>1.調整UI介面</p>
                <p>2.確認班表：移除到別的地方並說明，讓USER 不會誤按</p>
                <hr />
                <h4>系統除錯</h4>
                <p>1.修正合約新增失敗調整</p>
                <p>2.修正電子派班狀態</p>
                <p>3.修正當系統產生合約時，中途斷線還沒產生出相關日期而導致有合約無班別</p>
                <p>4.修正登入後錯誤訊息</p>
            </section>
        </article>
    </div>
</asp:Content>
<asp:Content ID="Script" ContentPlaceHolderID="Script" runat="server"></asp:Content>