﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustJobScheduleConfirm.aspx.cs" Inherits="MommyHappySchedule.CustJobScheduleConfirm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>確認班表</title>
    <style type="text/css">
        .auto-style1 {
            height: 112px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <center>
                <table>
                    <tr>
                        <td align="center">
                            <table>

                                <tr>

                                    <td colspan="3" class="auto-style1">
                                        <table border="1" cellpadding="1" cellspacing="0">
                                            <tr style="font-size: 10pt; font-family: Arial;">
                                                <td>駐點：</td>
                                                <td>
                                                    <asp:DropDownList ID="Branch" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>員工名稱</td>
                                                <td>
                                                    <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                                                </td>
                                                <td>客戶名稱</td>
                                                <td>
                                                    <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Button ID="Check_Report" runat="server" Text="產生報表" OnClick="Check_Report_Click" />
                                                    <asp:Button ID="Button6" runat="server" Text="取消" OnClick="Button6_Click" />
                                                </td>
                                                <td>
                                                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="Index.aspx">返回主選單</asp:HyperLink>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                                                    <asp:Label ID="Label1" runat="server" Text="0" Visible="false"></asp:Label>
                                                    <asp:Label ID="Label4" runat="server" Text="0.00"></asp:Label>
                                                    <asp:Label ID="Label5" runat="server" Text="0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr style="font-size: 10pt; font-family: Arial;">
                                                <td colspan="4">顯示：
                                                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged" RepeatLayout="Flow">
                                                        <asp:ListItem>全部</asp:ListItem>
                                                        <asp:ListItem>已確認</asp:ListItem>
                                                        <asp:ListItem>未確認</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td colspan="3">

                                                    <asp:Button ID="Left_Screen" runat="server" Text="前一天" OnClick="Change_Screen" />
                                                    <asp:TextBox ID="Show_Date" runat="server"></asp:TextBox>
                                                    <asp:Button ID="Right_Screen" runat="server" Text="後一天" OnClick="Change_Screen" />
                                                </td>
                                                <td colspan="3">
                                                    <asp:RadioButtonList ID="RadioButtonList2" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="true"  OnSelectedIndexChanged="RadioButtonList2_SelectedIndexChanged" >
                                                        <asp:ListItem>全部確定</asp:ListItem>
                                                        <asp:ListItem>全部取消</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:Button ID="Button7" runat="server" Text="存檔" OnClick="Button7_Click" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" BackColor="White" OnItemDataBound="Show_Sc_Type"
                                            BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" Font-Names="Arial" Font-Size="Smaller" BorderStyle="None">
                                            <FooterStyle BackColor="White" ForeColor="#000066" />
                                            <SelectedItemStyle BackColor="#669999" ForeColor="White" Font-Bold="True" />
                                            <Columns>

                                                <asp:BoundColumn DataField="ID" HeaderText="Type" >
                                                    <ItemStyle Width="10px" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Branch_Name" HeaderText="駐點">
                                                    <ItemStyle Width="60px" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Staff_No" HeaderText="管家編號">
                                                    <ItemStyle Width="60px" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Staff_Name" HeaderText="管家姓名">
                                                    <ItemStyle Width="60px" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Start_Time" HeaderText="起始時間">
                                                    <ItemStyle Width="40px" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="End_Time" HeaderText="終止時間">
                                                    <ItemStyle Width="40px" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Cust_No" HeaderText="客戶編號">
                                                    <ItemStyle Width="70px" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Cust_Name" HeaderText="客戶姓名">
                                                    <ItemStyle Width="70px" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Cust_Addr" HeaderText="客戶地址">
                                                    <ItemStyle Width="250px" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Cust_Geometry" HeaderText="地址經緯度">
                                                    <ItemStyle Width="100px" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Note" HeaderText="單次需求">
                                                    <ItemStyle Width="50px" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Cust_Sp_Request" HeaderText="客戶需求">
                                                    <ItemStyle Width="200px" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Job_Result" HeaderText="狀態">
                                                    <ItemStyle Width="10px" />
                                                </asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="確認">
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label3" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="Job_Flag" Visible="false">
                                                    <ItemStyle Width="120px" />
                                                </asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="確認">
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:RadioButtonList ID="RadioButtonList3" runat="server" RepeatDirection="Horizontal" >
                                                        <asp:ListItem>是</asp:ListItem>
                                                        <asp:ListItem>否</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle HorizontalAlign="Left" Mode="NumericPages" BackColor="White" ForeColor="#000066" />
                                            <ItemStyle ForeColor="#000066" />
                                        </asp:DataGrid>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:TextBox ID="TextBox5" runat="server" Width="100%"></asp:TextBox></td>
                    </tr>
                </table>
            </center>
        </div>
    </form>
</body>
</html>


