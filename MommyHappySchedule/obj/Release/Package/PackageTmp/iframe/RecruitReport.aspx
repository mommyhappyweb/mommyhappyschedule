﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Template/IframeLayout.master" AutoEventWireup="true" CodeBehind="RecruitReport.aspx.cs" Inherits="MommyHappySchedule.iframe.RecruitReport" %>

<asp:Content ID="Style" ContentPlaceHolderID="Style" runat="server">
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="Content" runat="server">
    <div class="container-fluid">
       
        <fieldset class="col-sm-10 offset-sm-1">
            <legend>資料搜尋</legend>
            <div class="form-group row">
                <div class="col-sm-2">
                    <label for="Branch">駐點</label>
                    <asp:DropDownList ID="Branch" CssClass="form-control" runat="server"></asp:DropDownList>
                </div>
                <div class="col-sm-2">
                    <label for="Check_Year">年份</label>
                    <asp:DropDownList ID="Check_Year" CssClass="form-control" runat="server">
                        <asp:ListItem>2016</asp:ListItem>
                        <asp:ListItem Selected="True">2017</asp:ListItem>
                        <asp:ListItem>2018</asp:ListItem>
                        <asp:ListItem>2019</asp:ListItem>
                        <asp:ListItem>2020</asp:ListItem>
                        <asp:ListItem>2021</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-sm-2">
                    <label for="Check_Month">月份</label>
                    <asp:DropDownList ID="Check_Month" CssClass="form-control" runat="server">
                        <asp:ListItem Selected="True">1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-sm-2">
                    <label for="RadioButtonList1">統計表</label>
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True" RepeatDirection="Horizontal" OnSelectedIndexChanged ="RadioButtonList1_SelectedIndexChanged">
                        <asp:ListItem Selected="True">招募統計表</asp:ListItem>
                        <asp:ListItem>金牌績效表</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div class="col-sm-2">報表負責人：沛彤
                     <asp:Label ID="Label44" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-sm-2">
                    <asp:Button ID="Check_Report" CssClass="btn btn-primary" runat="server" Text="產生報表" OnClick="Check_Report_Click" />
                </div>
            </div>
        </fieldset>
        <table class="table">
            <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                <asp:View ID="View1" runat="server">
                    <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemDataBound">
                        <HeaderTemplate>
                            <tr>
                                <td class="auto-style1">單位<br />
                                    月份</td>
                                <td class="auto-style1" bgcolor="#FFFF99">應徵人數</td>
                                <td class="auto-style1" bgcolor="#FFFF99">錄取人數</td>
                                <td class="auto-style1" bgcolor="#CCFFFF">繳資料數</td>
                                <td class="auto-style1" bgcolor="#66CCFF">說明會數</td>
                                <td class="auto-style1" bgcolor="#FFCC99">體驗人數</td>
                                <td class="auto-style1" bgcolor="#CCFF99">入職人數</td>
                                <td class="auto-style1" bgcolor="#FFFF99">錄取率</td>
                                <td class="auto-style1" bgcolor="#CCFFFF">錄取繳交率</td>
                                <td class="auto-style1" bgcolor="#66CCFF">繳交說明率</td>
                                <td class="auto-style1" bgcolor="#FFCC99">說明會體驗率</td>
                                <td class="auto-style1" bgcolor="#CCFF99">體驗入職率</td>
                                <td class="auto-style1" bgcolor="#66FF33">應徵入職率</td>
                            </tr>
                                <tr>
                                <td class="auto-style1">應達成指標</td>
                                <td class="auto-style1">每月16人</td>
                                <td class="auto-style1">每月8人</td>
                                <td class="auto-style1">每月5人</td>
                                <td class="auto-style1"></td>
                                <td class="auto-style1">每月4人</td>
                                <td class="auto-style1">每月2人</td>
                                <td class="auto-style1">50.00%</td>
                                <td class="auto-style1">60.00%</td>
                                <td class="auto-style1"></td>
                                <td class="auto-style1"></td>
                                <td class="auto-style1">70.00%</td>
                                <td class="auto-style1">15.50%</td>
                            </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td class="auto-style1">
                                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ID") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#FFFF99">
                                    <asp:Label ID="Recruit_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Recruit_Number") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#FFFF99">
                                    <asp:Label ID="Hire_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Hire_Number") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#CCFFFF">
                                    <asp:Label ID="Paper_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Paper_Number") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#66CCFF">
                                    <asp:Label ID="Oritation_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Oritation_Number") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#FFCC99">
                                    <asp:Label ID="Experience_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Experience_Number") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#CCFF99">
                                    <asp:Label ID="Final_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Final_Number") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#FFFF99">
                                    <asp:Label ID="R_H_P" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "R_H_P") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#CCFFFF">
                                    <asp:Label ID="H_P_P" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "H_P_P") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#66CCFF">
                                    <asp:Label ID="P_O_P" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "P_O_P") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#FFCC99">
                                    <asp:Label ID="O_E_P" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "O_E_P") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#CCFF99">
                                    <asp:Label ID="E_W_P" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "E_W_P") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#66FF33">
                                    <asp:Label ID="R_W_P" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "R_W_P") %>'> </asp:Label>
                                </td>
                            </tr>

                        </ItemTemplate>
                    </asp:Repeater>
                    <tr>
                        <td colspan="13">招募現況追踨</td>
                    </tr>
                    <tr>
                        <td colspan="13">
                            <asp:DataGrid ID="DataGrid5" runat="server" AutoGenerateColumns="False" BackColor="White"
                                BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" Font-Names="Arial" Font-Size="Smaller"
                                OnItemCommand="Show_Job_Detail" OnItemDataBound="Show_Sc_Type" BorderStyle="None">
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <SelectedItemStyle BackColor="#669999" ForeColor="White" Font-Bold="True" />
                                <Columns>
                                    <asp:BoundColumn DataField="ID" HeaderText="Type" Visible="False">
                                        <ItemStyle Width="10px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="部門" HeaderText="部門">
                                        <ItemStyle Width="70px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="本月入職人數" HeaderText="本月入職人數">
                                        <ItemStyle Width="80px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="近二周入職人數" HeaderText="近二周入職人數">
                                        <ItemStyle Width="80px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="本月應徵人數" HeaderText="本月面試人數">
                                        <ItemStyle Width="80px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="本周應徵人數" HeaderText="本周面試人數">
                                        <ItemStyle Width="80px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="本日應徵人數" HeaderText="本日面試人數">
                                        <ItemStyle Width="80px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="近二周說明會人數" HeaderText="近二周說明會人數">
                                        <ItemStyle Width="80px" />
                                    </asp:BoundColumn>

                                </Columns>
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                <PagerStyle HorizontalAlign="Left" Mode="NumericPages" BackColor="White" ForeColor="#000066" />
                                <ItemStyle ForeColor="#000066" />
                            </asp:DataGrid>
                        </td>
                    </tr>
                    <asp:Panel ID="Panel1" runat="server">
                        <tr>
                            <td colspan="13">繳交資料有問題
                                <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
                                筆</td>
                        </tr>
                        <tr>
                            <td colspan="13">
                                <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" BackColor="White"
                                    BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" Font-Names="Arial" Font-Size="Smaller"
                                    OnItemCommand="Show_Job_Detail" OnItemDataBound="Show_Sc_Type" BorderStyle="None">
                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                    <SelectedItemStyle BackColor="#669999" ForeColor="White" Font-Bold="True" />
                                    <Columns>
                                        <asp:BoundColumn DataField="ID" HeaderText="Type" Visible="False">
                                            <ItemStyle Width="10px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Branch_Name" HeaderText="Branch_Name">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Name" HeaderText="Name">
                                            <ItemStyle Width="65px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Birth" DataFormatString="{0:yyyy/MM/dd}" HeaderText="Birth">
                                            <ItemStyle Width="80px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Cell_Phone" HeaderText="電話">
                                            <ItemStyle Width="90px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Interview_Date" DataFormatString="{0:yyyy/MM/dd}" HeaderText="面試日期">
                                            <ItemStyle Width="80px" />
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="是否錄取">
                                            <ItemTemplate>
                                                <asp:Label ID="Hire_FLag" runat="server" Width="80px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="Interview_Result" Visible="False">
                                            <ItemStyle Width="20px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Hout_Info_Date" HeaderText="繳資料日" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Train_Date" HeaderText="Train_Date" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Date_1" HeaderText="Experi_Date_1" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Staff_1" HeaderText="帶訓管家_1">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Date_2" HeaderText="Experi_Date_2" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Staff_2" HeaderText="帶訓管家_2">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Date_3" HeaderText="Experi_Date_3" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Staff_3" HeaderText="帶訓管家_3">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Date_4" HeaderText="Experi_Date_4" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Staff_4" HeaderText="帶訓管家_4">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Sign_Contr_Date" HeaderText="Sign_Contr_Date" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="No_Sign_Reason" HeaderText="No_Sign_Reason">
                                            <ItemStyle Width="120px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="No_Info_Reason" HeaderText="No_Info_Reason" Visible="false">
                                            <ItemStyle Width="120px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="No_Train_Reason" HeaderText="No_Train_Reason," Visible="false">
                                            <ItemStyle Width="120px" />
                                        </asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle HorizontalAlign="Left" Mode="NumericPages" BackColor="White" ForeColor="#000066" />
                                    <ItemStyle ForeColor="#000066" />
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="Panel2" runat="server">
                        <tr>
                            <td colspan="13">新訓資料有問題有
                                <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
                                筆</td>
                        </tr>
                        <tr>
                            <td colspan="13">
                                <asp:DataGrid ID="DataGrid2" runat="server" AutoGenerateColumns="False" BackColor="White"
                                    BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" Font-Names="Arial" Font-Size="Smaller"
                                    OnItemCommand="Show_Job_Detail" OnItemDataBound="Show_Sc_Type" BorderStyle="None">
                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                    <SelectedItemStyle BackColor="#669999" ForeColor="White" Font-Bold="True" />
                                    <Columns>
                                        <asp:BoundColumn DataField="ID" HeaderText="Type" Visible="False">
                                            <ItemStyle Width="10px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Branch_Name" HeaderText="Branch_Name">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Name" HeaderText="Name">
                                            <ItemStyle Width="65px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Birth" DataFormatString="{0:yyyy/MM/dd}" HeaderText="Birth">
                                            <ItemStyle Width="80px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Cell_Phone" HeaderText="電話">
                                            <ItemStyle Width="90px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Interview_Date" DataFormatString="{0:yyyy/MM/dd}" HeaderText="面試日期">
                                            <ItemStyle Width="80px" />
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="是否錄取">
                                            <ItemTemplate>
                                                <asp:Label ID="Hire_FLag" runat="server" Width="80px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="Interview_Result" Visible="False">
                                            <ItemStyle Width="20px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Hout_Info_Date" HeaderText="繳資料日" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Train_Date" HeaderText="Train_Date" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Date_1" HeaderText="Experi_Date_1" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Staff_1" HeaderText="帶訓管家_1">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Date_2" HeaderText="Experi_Date_2" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Staff_2" HeaderText="帶訓管家_2">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Date_3" HeaderText="Experi_Date_3" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Staff_3" HeaderText="帶訓管家_3">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Date_4" HeaderText="Experi_Date_4" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Staff_4" HeaderText="帶訓管家_4">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Sign_Contr_Date" HeaderText="Sign_Contr_Date" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="No_Sign_Reason" HeaderText="No_Sign_Reason">
                                            <ItemStyle Width="120px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="No_Info_Reason" HeaderText="No_Info_Reason" Visible="false">
                                            <ItemStyle Width="120px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="No_Train_Reason" HeaderText="No_Train_Reason," Visible="false">
                                            <ItemStyle Width="120px" />
                                        </asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle HorizontalAlign="Left" Mode="NumericPages" BackColor="White" ForeColor="#000066" />
                                    <ItemStyle ForeColor="#000066" />
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="Panel3" runat="server">
                        <tr>
                            <td colspan="13">未安排體驗有 
                                <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label>
                                筆</td>
                        </tr>
                        <tr>
                            <td colspan="13">
                                <asp:DataGrid ID="DataGrid3" runat="server" AutoGenerateColumns="False" BackColor="White"
                                    BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" Font-Names="Arial" Font-Size="Smaller"
                                    OnItemCommand="Show_Job_Detail" OnItemDataBound="Show_Sc_Type" BorderStyle="None">
                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                    <SelectedItemStyle BackColor="#669999" ForeColor="White" Font-Bold="True" />
                                    <Columns>
                                        <asp:BoundColumn DataField="ID" HeaderText="Type" Visible="False">
                                            <ItemStyle Width="10px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Branch_Name" HeaderText="Branch_Name">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Name" HeaderText="Name">
                                            <ItemStyle Width="65px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Birth" DataFormatString="{0:yyyy/MM/dd}" HeaderText="Birth">
                                            <ItemStyle Width="80px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Cell_Phone" HeaderText="電話">
                                            <ItemStyle Width="90px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Interview_Date" DataFormatString="{0:yyyy/MM/dd}" HeaderText="面試日期">
                                            <ItemStyle Width="80px" />
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="是否錄取">
                                            <ItemTemplate>
                                                <asp:Label ID="Hire_FLag" runat="server" Width="80px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="Interview_Result" Visible="False">
                                            <ItemStyle Width="20px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Hout_Info_Date" HeaderText="繳資料日" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Train_Date" HeaderText="Train_Date" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Date_1" HeaderText="Experi_Date_1" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Staff_1" HeaderText="帶訓管家_1">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Date_2" HeaderText="Experi_Date_2" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Staff_2" HeaderText="帶訓管家_2">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Date_3" HeaderText="Experi_Date_3" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Staff_3" HeaderText="帶訓管家_3">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Date_4" HeaderText="Experi_Date_4" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Staff_4" HeaderText="帶訓管家_4">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Sign_Contr_Date" HeaderText="Sign_Contr_Date" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="No_Sign_Reason" HeaderText="No_Sign_Reason">
                                            <ItemStyle Width="120px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="No_Info_Reason" HeaderText="No_Info_Reason" Visible="false">
                                            <ItemStyle Width="120px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="No_Train_Reason" HeaderText="No_Train_Reason," Visible="false">
                                            <ItemStyle Width="120px" />
                                        </asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle HorizontalAlign="Left" Mode="NumericPages" BackColor="White" ForeColor="#000066" />
                                    <ItemStyle ForeColor="#000066" />
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </asp:Panel>
                    <asp:Panel ID="Panel4" runat="server">
                        <tr>
                            <td colspan="13">未入職有
                                <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label>
                                筆</td>
                        </tr>
                        <tr>
                            <td colspan="13">
                                <asp:DataGrid ID="DataGrid4" runat="server" AutoGenerateColumns="False" BackColor="White"
                                    BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" Font-Names="Arial" Font-Size="Smaller"
                                    OnItemCommand="Show_Job_Detail" OnItemDataBound="Show_Sc_Type" BorderStyle="None">
                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                    <SelectedItemStyle BackColor="#669999" ForeColor="White" Font-Bold="True" />
                                    <Columns>
                                        <asp:BoundColumn DataField="ID" HeaderText="Type" Visible="False">
                                            <ItemStyle Width="10px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Branch_Name" HeaderText="Branch_Name">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Name" HeaderText="Name">
                                            <ItemStyle Width="65px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Birth" DataFormatString="{0:yyyy/MM/dd}" HeaderText="Birth">
                                            <ItemStyle Width="80px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Cell_Phone" HeaderText="電話">
                                            <ItemStyle Width="90px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Interview_Date" DataFormatString="{0:yyyy/MM/dd}" HeaderText="面試日期">
                                            <ItemStyle Width="80px" />
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="是否錄取">
                                            <ItemTemplate>
                                                <asp:Label ID="Hire_FLag" runat="server" Width="80px"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="Interview_Result" Visible="False">
                                            <ItemStyle Width="20px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Hout_Info_Date" HeaderText="繳資料日" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Train_Date" HeaderText="Train_Date" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Date_1" HeaderText="Experi_Date_1" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Staff_1" HeaderText="帶訓管家_1">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Date_2" HeaderText="Experi_Date_2" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Staff_2" HeaderText="帶訓管家_2">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Date_3" HeaderText="Experi_Date_3" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Staff_3" HeaderText="帶訓管家_3">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Date_4" HeaderText="Experi_Date_4" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Experi_Staff_4" HeaderText="帶訓管家_4">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Sign_Contr_Date" HeaderText="Sign_Contr_Date" DataFormatString="{0:yyyy/MM/dd}">
                                            <ItemStyle Width="70px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="No_Sign_Reason" HeaderText="No_Sign_Reason">
                                            <ItemStyle Width="120px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="No_Info_Reason" HeaderText="No_Info_Reason" Visible="false">
                                            <ItemStyle Width="120px" />
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="No_Train_Reason" HeaderText="No_Train_Reason," Visible="false">
                                            <ItemStyle Width="120px" />
                                        </asp:BoundColumn>
                                    </Columns>
                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle HorizontalAlign="Left" Mode="NumericPages" BackColor="White" ForeColor="#000066" />
                                    <ItemStyle ForeColor="#000066" />
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </asp:Panel>
                </asp:View>
                <asp:View ID="View2" runat="server">
                    <tr>
                        <td colspan="13">
                            <table>
                                <tr>
                                    <td class="auto-style1">單位</td>
                                    <td class="auto-style1" bgcolor="#FFFF99">金牌管家</td>
                                    <td class="auto-style1" bgcolor="#CCFFFF" colspan="3">全年度</td>
                                    <td class="auto-style1" bgcolor="#66CCFF" colspan="3">一月</td>
                                    <td class="auto-style1" bgcolor="#FFCC99" colspan="3">二月</td>
                                    <td class="auto-style1" bgcolor="#CCFF99" colspan="3">三月</td>
                                    <td class="auto-style1" bgcolor="#FFFF99" colspan="3">四月</td>
                                    <td class="auto-style1" bgcolor="#66FF33" colspan="3">五月</td>
                                    <td class="auto-style1" bgcolor="#CCFFFF" colspan="3">六月</td>
                                    <td class="auto-style1" bgcolor="#66CCFF" colspan="3">七月</td>
                                    <td class="auto-style1" bgcolor="#FFCC99" colspan="3">八月</td>
                                    <td class="auto-style1" bgcolor="#CCFF99" colspan="3">九月</td>
                                    <td class="auto-style1" bgcolor="#FFFF99" colspan="3">十月</td>
                                    <td class="auto-style1" bgcolor="#66FF33" colspan="3">十一月</td>
                                    <td class="auto-style1" bgcolor="#CCFFFF" colspan="3">十二月</td>

                                </tr>
                                <asp:Repeater ID="Repeater2" runat="server" OnItemDataBound="Repeater1_ItemDataBound">
                                    <HeaderTemplate>
                                        <tr>
                                            <td class="auto-style1">單位</td>
                                            <td class="auto-style1" bgcolor="#FFFF99">金牌管家</td>
                                            <td class="auto-style1" bgcolor="#CCFFFF">輔導人數</td>
                                            <td class="auto-style1" bgcolor="#CCFFFF">入職人數</td>
                                            <td class="auto-style1" bgcolor="#CCFFFF">入職比率</td>
                                            <td class="auto-style1" bgcolor="#66CCFF">輔導人數</td>
                                            <td class="auto-style1" bgcolor="#66CCFF">入職人數</td>
                                            <td class="auto-style1" bgcolor="#66CCFF">入職比率</td>
                                            <td class="auto-style1" bgcolor="#FFCC99">輔導人數</td>
                                            <td class="auto-style1" bgcolor="#FFCC99">入職人數</td>
                                            <td class="auto-style1" bgcolor="#FFCC99">入職比率</td>
                                            <td class="auto-style1" bgcolor="#CCFF99">輔導人數</td>
                                            <td class="auto-style1" bgcolor="#CCFF99">入職人數</td>
                                            <td class="auto-style1" bgcolor="#CCFF99">入職比率</td>
                                            <td class="auto-style1" bgcolor="#FFFF99">輔導人數</td>
                                            <td class="auto-style1" bgcolor="#FFFF99">入職人數</td>
                                            <td class="auto-style1" bgcolor="#FFFF99">入職比率</td>
                                            <td class="auto-style1" bgcolor="#66FF33">輔導人數</td>
                                            <td class="auto-style1" bgcolor="#66FF33">入職人數</td>
                                            <td class="auto-style1" bgcolor="#66FF33">入職比率</td>
                                            <td class="auto-style1" bgcolor="#CCFFFF">輔導人數</td>
                                            <td class="auto-style1" bgcolor="#CCFFFF">入職人數</td>
                                            <td class="auto-style1" bgcolor="#CCFFFF">入職比率</td>
                                            <td class="auto-style1" bgcolor="#66CCFF">輔導人數</td>
                                            <td class="auto-style1" bgcolor="#66CCFF">入職人數</td>
                                            <td class="auto-style1" bgcolor="#66CCFF">入職比率</td>
                                            <td class="auto-style1" bgcolor="#FFCC99">輔導人數</td>
                                            <td class="auto-style1" bgcolor="#FFCC99">入職人數</td>
                                            <td class="auto-style1" bgcolor="#FFCC99">入職比率</td>
                                            <td class="auto-style1" bgcolor="#CCFF99">輔導人數</td>
                                            <td class="auto-style1" bgcolor="#CCFF99">入職人數</td>
                                            <td class="auto-style1" bgcolor="#CCFF99">入職比率</td>
                                            <td class="auto-style1" bgcolor="#FFFF99">輔導人數</td>
                                            <td class="auto-style1" bgcolor="#FFFF99">入職人數</td>
                                            <td class="auto-style1" bgcolor="#FFFF99">入職比率</td>
                                            <td class="auto-style1" bgcolor="#66FF33">輔導人數</td>
                                            <td class="auto-style1" bgcolor="#66FF33">入職人數</td>
                                            <td class="auto-style1" bgcolor="#66FF33">入職比率</td>
                                            <td class="auto-style1" bgcolor="#CCFFFF">輔導人數</td>
                                            <td class="auto-style1" bgcolor="#CCFFFF">入職人數</td>
                                            <td class="auto-style1" bgcolor="#CCFFFF">入職比率</td>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td class="auto-style1">
                                                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ID") %>' Width="70px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Ex_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EX_Name") %>' Width="70px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="EX_Number_T" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EX_Number_T") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label6" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Number_T") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label7" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Ratio_T") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label8" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EX_Number_1") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label9" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Number_1") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label10" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Ratio_1") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label11" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EX_Number_2") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label12" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Number_2") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label13" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Ratio_2") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label14" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EX_Number_3") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label15" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Number_3") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label16" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Ratio_3") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label17" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EX_Number_4") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label18" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Number_4") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label19" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Ratio_4") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label20" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EX_Number_5") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label21" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Number_5") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label22" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Ratio_5") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label23" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EX_Number_6") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label24" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Number_6") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label25" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Ratio_6") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label26" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EX_Number_7") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label27" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Number_7") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66CCFF">
                                                <asp:Label ID="Label28" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Ratio_7") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label29" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EX_Number_8") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label30" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Number_8") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFCC99">
                                                <asp:Label ID="Label31" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Ratio_8") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label32" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EX_Number_9") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label33" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Number_9") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFF99">
                                                <asp:Label ID="Label34" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Ratio_9") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label35" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EX_Number_A") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label36" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Number_A") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#FFFF99">
                                                <asp:Label ID="Label37" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Ratio_A") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label38" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EX_Number_B") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label39" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Number_B") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#66FF33">
                                                <asp:Label ID="Label40" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Ratio_B") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label41" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EX_Number_C") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label42" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Number_C") %>' Width="40px"></asp:Label>
                                            </td>
                                            <td class="auto-style3" bgcolor="#CCFFFF">
                                                <asp:Label ID="Label43" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Fi_Ratio_C") %>' Width="40px"></asp:Label>
                                            </td>
                                        </tr>

                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </td>
                    </tr>
                </asp:View>
            </asp:MultiView>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Script" ContentPlaceHolderID="Script" runat="server">
    <script>
        document.domain = 'mommyhappy.com';
    </script>
</asp:Content>
