﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Template/IframeLayout.master" AutoEventWireup="true" CodeBehind="EContractList.aspx.cs" Inherits="MommyHappySchedule.iframe.EContractList" %>
<asp:Content ID="Style" ContentPlaceHolderID="Style" runat="server">
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="Content" runat="server">
    <div class="container-fluid">
  
        <% if (Alert != null) { %>
        <% string Status = (Convert.ToBoolean(Alert["Status"])) ? " alert-success" : "alert-danger"; %>
        <div class="alert <%=Status %>"><%=Alert["Message"] %></div>
        <% } %>
        <fieldset class="col-sm-10 offset-sm-1">
            <legend>資料搜尋</legend>
            <div class="form-group row">
                <div class="col-sm-3">
                    <asp:DropDownList ID="BranchSelect" AutoPostBack="False" AppendDataBoundItems="True" runat="server" CssClass="form-control"></asp:DropDownList>
                </div>
                <div class="col-sm-3">
                    <asp:DropDownList ID="SearchSelect" AppendDataBoundItems="True" runat="server" CssClass="form-control">
                        <asp:ListItem Value="Cust_No">客戶編號</asp:ListItem>
                        <asp:ListItem Value="Charge_Staff_No">簽約員工編號</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-sm-4">
                    <asp:TextBox ID="SearchValue" CssClass="form-control" placeholder="關鍵字" runat="server"></asp:TextBox>
                </div>
                <div class="col-sm-2">
                    <asp:Button ID="SearchBtn" OnClick="SearchClick" CssClass="btn btn-primary" runat="server" Text="搜尋"></asp:Button>
                </div>
            </div>
        </fieldset>
        <hr />
        <% if (ContractData != null && ContractData.Count > 0){ %>
        <table class="table table-striped table-hover">
            <thead class="thead-light text-center">
                <tr>
                    <th>編輯</th>
                    <th>合約ID</th>
                    <th>駐點</th>
                    <th>合約編號</th>
                    <th>到期日</th>
                    <th>客戶編號</th>
                    <th>客戶名稱</th>
                    <th>合約週次</th>
                    <th>簽約員工編號</th>
                    <th>簽約員工姓名</th>
                </tr>
            </thead>
            <tbody>
                <% foreach (Dictionary<string, object> Item in ContractData){ %>
                <tr class="text-center">
                    <td><a href="EContractEdit.aspx?Eid=<%=Item["ID"] %>" class="btn btn-sm btn-info">編輯</a></td>
                    <td><%=Item["ID"] %></td>
                    <td><%=Item["Branch_Name"] %></td>
                    <td><%=Item["Contract_Nums"] %></td>
                    <td><%=Item["Contract_E_Date"] %></td>
                    <td><%=Item["Cust_No"] %></td>
                    <td><%=Item["Cust_Name"] %></td>
                    <td><%=Item["Contract_Times"] %></td>
                    <td><%=Item["Charge_Staff_No"] %></td>
                    <td><%=Item["Staff_Name"] %></td>
                </tr>
                <% } %>
            </tbody>
        </table>
        <% } %>
    </div>
</asp:Content>
<asp:Content ID="Script" ContentPlaceHolderID="Script" runat="server">
    <script>
        document.domain = 'mommyhappy.com';
    </script>
</asp:Content>
