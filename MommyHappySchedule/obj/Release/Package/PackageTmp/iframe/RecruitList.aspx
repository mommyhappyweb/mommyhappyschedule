﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Template/IframeLayout.master" AutoEventWireup="true" CodeBehind="RecruitList.aspx.cs" Inherits="MommyHappySchedule.iframe.RecruitList" %>
<asp:Content ID="Style" ContentPlaceHolderID="Style" runat="server"></asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="Content" runat="server">
    <div class="container-fluid">
        
        <table class="table">
            <thead>
                <tr>
                    <td>駐點：</td>
                    <td>
                        <asp:DropDownList ID="Branch" runat="server" AutoPostBack="true" OnSelectedIndexChanged="Branch_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>年份</td>
                    <td>
                        <asp:DropDownList ID="Check_Year" runat="server">
                            <asp:ListItem>2016</asp:ListItem>
                            <asp:ListItem>2017</asp:ListItem>
                            <asp:ListItem Selected="True">2018</asp:ListItem>
                            <asp:ListItem>2019</asp:ListItem>
                            <asp:ListItem>2020</asp:ListItem>
                            <asp:ListItem>2021</asp:ListItem>
                            <asp:ListItem>2022</asp:ListItem>
                            <asp:ListItem>2023</asp:ListItem>
                            <asp:ListItem>2024</asp:ListItem>
                            <asp:ListItem>2025</asp:ListItem>
                        </asp:DropDownList></td>
                    <td>月份</td>
                    <td>
                        <asp:DropDownList ID="Check_Month" runat="server" >
                            <asp:ListItem Selected="True">1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                            <asp:ListItem>6</asp:ListItem>
                            <asp:ListItem>7</asp:ListItem>
                            <asp:ListItem>8</asp:ListItem>
                            <asp:ListItem>9</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>11</asp:ListItem>
                            <asp:ListItem>12</asp:ListItem>
                        </asp:DropDownList>
                     </td>
                    <td>日期：<asp:TextBox ID="Check_Day" runat="server" Width="30px"></asp:TextBox></td>
                    <td>搜尋姓名：<asp:TextBox ID="TextBox3" runat="server"></asp:TextBox></td>
                    <td><asp:Button ID="Check_Report" runat="server" Text="檢視資料" OnClick="Check_Report_Click" /></td>    
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="報表負責人：沛彤" bgcolor="#CCCCCC"></asp:Label>
                        <asp:HyperLink ID="New_Recruit" runat="server" NavigateUrl="Recruit.aspx">新增資料</asp:HyperLink>
                    </td>
                </tr>
            </thead>
            <tbody>
            <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                <asp:View ID="View1" runat="server">
                    <tr>
                        <td>篩選條件：</td>
                        <td colspan ="2" class="auto-style1">
                            <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="true" OnSelectedIndexChanged="Check_Report_Click">
                                <asp:ListItem>錄取</asp:ListItem>
                                <asp:ListItem>安排複試</asp:ListItem>
                                <asp:ListItem>不錄取</asp:ListItem>
                                <asp:ListItem>保留</asp:ListItem>
                                <asp:ListItem Selected="True">全部</asp:ListItem>
                            </asp:RadioButtonList>
                        </td><td colspan ="2" class="auto-style1">
                            <asp:RadioButtonList ID="RadioButtonList2" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="true" OnSelectedIndexChanged="Check_Report_Click">
                                <asp:ListItem>已交資料</asp:ListItem>
                                <asp:ListItem>無交資料</asp:ListItem>
                                <asp:ListItem Selected="True">全部</asp:ListItem>
                            </asp:RadioButtonList>
                        </td><td colspan ="2" class="auto-style1">
                            <asp:RadioButtonList ID="RadioButtonList3" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="true" OnSelectedIndexChanged="Check_Report_Click">
                                <asp:ListItem>已參新訓</asp:ListItem>
                                <asp:ListItem>無參新訓</asp:ListItem>
                                <asp:ListItem Selected="True">全部</asp:ListItem>
                            </asp:RadioButtonList>
                            </td><td colspan ="2" class="auto-style1">
                            <asp:RadioButtonList ID="RadioButtonList4" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="true" OnSelectedIndexChanged="Check_Report_Click">
                                <asp:ListItem>已參體驗</asp:ListItem>
                                <asp:ListItem>無參體驗</asp:ListItem>
                                <asp:ListItem Selected="True">全部</asp:ListItem>
                            </asp:RadioButtonList>
                        </td><td colspan ="2" class="auto-style1">
                            <asp:RadioButtonList ID="RadioButtonList5" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="true" OnSelectedIndexChanged="Check_Report_Click">
                                <asp:ListItem>已簽約</asp:ListItem>
                                <asp:ListItem>無簽約</asp:ListItem>
                                <asp:ListItem Selected="True">全部</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="13">
                            <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" CssClass="table"
                                OnItemCommand="Show_Job_Detail" OnItemDataBound="Show_Sc_Type">
                                <Columns>
                                    <asp:BoundColumn DataField="ID" HeaderText="Type" Visible="False" >
                                        <ItemStyle  />
                                    </asp:BoundColumn>
                                    <asp:ButtonColumn DataTextField="Branch_Name" HeaderText="部門" CommandName="部門" >
                                        <ItemStyle />
                                    </asp:ButtonColumn>
                                    <asp:ButtonColumn DataTextField="Name" HeaderText="姓名" CommandName="姓名">
                                        <ItemStyle />
                                    </asp:ButtonColumn>
                                    <asp:BoundColumn DataField="Birth" DataFormatString="{0:yyyy/MM/dd}" HeaderText="生日">
                                        <ItemStyle />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Cell_Phone" HeaderText="電話">
                                        <ItemStyle />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Interview_Date" DataFormatString="{0:yyyy/MM/dd}" HeaderText="面試日期">
                                        <ItemStyle />
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="是否錄取">
                                        <ItemTemplate>
                                            <asp:Label ID="Hire_FLag" runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="Interview_Result" Visible="False">
                                        <ItemStyle />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Hout_Info_Date" HeaderText="繳資料日" DataFormatString="{0:yyyy/MM/dd}">
                                        <ItemStyle />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Train_Date" HeaderText="新訓日期" DataFormatString="{0:yyyy/MM/dd}">
                                        <ItemStyle />
                                    </asp:BoundColumn>
                                        <asp:BoundColumn DataField="Charge_Staff_Name" HeaderText="負責管家">
                                        <ItemStyle />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Experi_Date_1" HeaderText="體驗日期_1" DataFormatString="{0:yyyy/MM/dd}">
                                        <ItemStyle />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Experi_Staff_1" HeaderText="帶訓人員_1" Visible="false">
                                        <ItemStyle />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Experi_Date_2" HeaderText="體驗日期_2" DataFormatString="{0:yyyy/MM/dd}">
                                        <ItemStyle />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Experi_Staff_2" HeaderText="帶訓人員_2" Visible="false">
                                        <ItemStyle />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Experi_Date_3" HeaderText="體驗日期_3" DataFormatString="{0:yyyy/MM/dd}">
                                        <ItemStyle />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Experi_Staff_3" HeaderText="帶訓人員_3" Visible="false">
                                        <ItemStyle />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Experi_Date_4" HeaderText="體驗日期_4" DataFormatString="{0:yyyy/MM/dd}">
                                        <ItemStyle />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Experi_Staff_4" HeaderText="帶訓人員_4" Visible="false">
                                        <ItemStyle />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Sign_Contr_Date" HeaderText="簽約日期" DataFormatString="{0:yyyy/MM/dd}">
                                        <ItemStyle />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="No_Sign_Reason" HeaderText="未簽約原因">
                                        <ItemStyle />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="No_Info_Reason" HeaderText="未交資料原因" >
                                        <ItemStyle />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="No_Train_Reason" HeaderText="未到訓原因" >
                                        <ItemStyle />
                                    </asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="追踨狀況">
                                        <ItemTemplate>
                                            <asp:Label ID="Trace_FLag" runat="server" Width="40px"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="Recruit_Status" Visible ="false" >
                                        <ItemStyle Width="120px" />
                                    </asp:BoundColumn>
                                </Columns>
                                <PagerStyle Mode="NumericPages" />
                            </asp:DataGrid>
                        </td>
                    </tr>
                </asp:View>
                <asp:View ID="View2" runat="server">
                    <tr class="auto-style1">

                        <td>姓名：</td>
                        <td>
                            <asp:Label ID="ID_Label" runat="server" Text="Label" Visible ="false"></asp:Label>
                            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></td>
                        <td>應徵日期：</td>
                        <td>
                            <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label></td>
                        <td>電話：</td>
                        <td>
                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox></td>
                            <td>負責管家：</td>
                        <td>
                            <asp:TextBox ID="TextBox2" runat="server" Width="70px"></asp:TextBox>
                            <asp:Button ID="Button1" runat="server" Text="..." OnClick="Show_DropDownlist" />
                        </td>
                        <td colspan="4">
                            <asp:RadioButtonList ID="Interview_Result" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                <asp:ListItem>錄取</asp:ListItem>
                                <asp:ListItem>安排複試</asp:ListItem>
                                <asp:ListItem>不錄取</asp:ListItem>
                                <asp:ListItem Selected="True">保留</asp:ListItem>
                            </asp:RadioButtonList>

                        </td>
                        <td rowspan="5">
                            <asp:Panel ID="Panel1" runat="server" Visible="false">
                                <asp:Calendar ID="Calendar1" runat="server" BackColor="White" BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px" Width="200px" OnSelectionChanged="Calendar1_SelectionChanged">
                                    <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                                    <NextPrevStyle VerticalAlign="Bottom" />
                                    <OtherMonthDayStyle ForeColor="#808080" />
                                    <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                    <SelectorStyle BackColor="#CCCCCC" />
                                    <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                                    <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                                    <WeekendDayStyle BackColor="#FFFFCC" />
                                </asp:Calendar>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr class="auto-style1">
                        <td>繳資料日：</td>
                        <td>
                            <asp:TextBox ID="Pa_Date" runat="server" Width="70px"></asp:TextBox><asp:Button ID="Pa_Date_B" runat="server" Text="..." OnClick="Show_Calendar" />
                        </td>
                        <td>未繳原因：</td>
                        <td colspan="3" align="left">
                            <asp:TextBox ID="No_Pa_Re" runat="server"></asp:TextBox>
                        </td>
                        <td>新訓日期：</td>
                        <td>
                            <asp:TextBox ID="Tr_Date" runat="server" Width="70px"></asp:TextBox><asp:Button ID="Tr_Date_B" runat="server" Text="..." OnClick="Show_Calendar" />
                        </td>
                        <td>未訓原因：</td>
                        <td colspan="4" align="left">
                            <asp:TextBox ID="No_Tr_Re" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr class="auto-style1">
                        <td>體驗日期1：</td>
                        <td>
                            <asp:TextBox ID="Ex_Date_1" runat="server" Width="70px"></asp:TextBox>
                            <asp:Button ID="Ex_Date_1_B" runat="server" Text="..." OnClick="Show_Calendar" />
                        </td>
                        <td>帶訓人員1：</td>
                        <td>
                            <asp:TextBox ID="Ex_Name_1" runat="server" Width="70px"></asp:TextBox>
                            <asp:Button ID="Ex_Name_1_B" runat="server" Text="..." OnClick="Show_DropDownlist" />
                        </td>
                        <td>體驗日期2：</td>
                        <td>
                            <asp:TextBox ID="Ex_Date_2" runat="server" Width="70px"></asp:TextBox>
                            <asp:Button ID="Ex_Date_2_B" runat="server" Text="..." OnClick="Show_Calendar" />
                        </td>
                        <td>帶訓人員2：</td>
                        <td>
                            <asp:TextBox ID="Ex_Name_2" runat="server" Width="70px"></asp:TextBox>
                            <asp:Button ID="Ex_Name_2_B" runat="server" Text="..." OnClick="Show_DropDownlist" />
                        </td>
                        <td>體驗日期3：</td>
                        <td>
                            <asp:TextBox ID="Ex_Date_3" runat="server" Width="70px"></asp:TextBox>
                            <asp:Button ID="Ex_Date_3_B" runat="server" Text="..." OnClick="Show_Calendar" />
                        </td>
                        <td>帶訓人員3：</td>
                        <td>
                            <asp:TextBox ID="Ex_Name_3" runat="server" Width="70px"></asp:TextBox>
                            <asp:Button ID="Ex_Name_3_B" runat="server" Text="..." OnClick="Show_DropDownlist" />
                        </td>

                    </tr>
                    <tr class="auto-style1">
                        <td>體驗日期4：</td>
                        <td>
                            <asp:TextBox ID="Ex_Date_4" runat="server" Width="70px"></asp:TextBox>
                            <asp:Button ID="Ex_Date_4_B" runat="server" Text="..." OnClick="Show_Calendar" />
                        </td>
                        <td>帶訓人員4：</td>
                            <td>
                            <asp:TextBox ID="Ex_Name_4" runat="server" Width="70px"></asp:TextBox>
                            <asp:Button ID="Ex_Name_4_B" runat="server" Text="..." OnClick="Show_DropDownlist" />
                        </td>
                        <td>入職日期：</td>
                        <td>
                            <asp:TextBox ID="Fi_Date" runat="server" Width="70px"></asp:TextBox>
                            <asp:Button ID="Fi_Date_B" runat="server" Text="..." OnClick="Show_Calendar" />
                        </td>
                        <td>未入職原因</td>
                        <td colspan="3" align="left">
                            <asp:TextBox ID="TextBox12" runat="server"></asp:TextBox>
                        </td>
                        <td colspan ="2">
                            <asp:DropDownList ID="DropDownList1" runat="server" Visible ="false" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="12">
                            <asp:RadioButtonList ID="RadioButtonList6" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Value="0">持續追踨</asp:ListItem>
                                <asp:ListItem Value="1">結案不交</asp:ListItem>
                                <asp:ListItem Value="2">結案己交</asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:Button ID="Save" runat="server" Text="儲存並返回" OnClick ="Save_Click"/>
                            <asp:Button ID="Rest" runat="server" Text="返回列表" OnClick="Rest_Click" />
                        </td>
                    </tr>
                </asp:View>
            </asp:MultiView>
            </tbody>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Script" ContentPlaceHolderID="Script" runat="server">
    <script>
        document.domain = 'mommyhappy.com';
    </script>
</asp:Content>