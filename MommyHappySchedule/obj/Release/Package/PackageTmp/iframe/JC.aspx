﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Template/IframeLayout.master" AutoEventWireup="true" CodeBehind="JC.aspx.cs" Inherits="MommyHappySchedule.iframe.JC" %>


<asp:Content ID="Style" ContentPlaceHolderID="Style" runat="server"></asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="Content" runat="server">
    <div class="container-fluid">
   
        <div class="row">
            <fieldset class="col-sm-6">
                <legend>搜尋</legend>
                <div class="form-group row">
                    <div class="col-sm-4">
                        <label for="Branch" class="col-form-label">駐點：</label>
                        <asp:DropDownList ID="Branch" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                    <div class="col-sm-4">
                        <label for="Staff_Name" class="col-form-label">員工名稱：</label>
                        <asp:TextBox ID="Staff_Name" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-sm-4">
                        <label for="Cust_Name" class="col-form-label">客戶名稱：</label>
                        <asp:TextBox ID="Cust_Name" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="mt-2 text-right">
                    <asp:Button ID="Check_Report" CssClass="btn btn-primary" runat="server" Text="產生報表" OnClick="Check_Report_Click" />
                    <asp:Button ID="Remove_Report" CssClass="btn btn-default" runat="server" Text="清除" OnClick="Remove_Report_Click" />
                </div>
            </fieldset>
            <fieldset class="col-sm-6">
                <legend>篩選</legend>
                <div class="form-group row">
                    <div class="col-sm-2">
                        <label for="PaymentCheck" class="col-form-label">排班篩選：</label>
                        <asp:DropDownList ID="PaymentCheck" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="PaymentCheckEvent" RepeatLayout="Flow" CssClass="form-control">
                            <asp:ListItem Value="0">全部</asp:ListItem>
                            <asp:ListItem Value="1">已確認</asp:ListItem>
                            <asp:ListItem Value="2">未確認</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-sm-4">
                        <label for="DetermineCheck" class="col-form-label">批次確認：</label>
                        <asp:DropDownList ID="DetermineCheck" runat="server" CssClass="form-control" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="true" OnSelectedIndexChanged="DetermineCheckEvent">
                            <asp:ListItem Value="2">取消批次確認</asp:ListItem>
                            <asp:ListItem Value="0">全部確定</asp:ListItem>
                            <asp:ListItem Value="1">全部取消</asp:ListItem>
                        </asp:DropDownList>
<%--                         <asp:RadioButtonList ID="DetermineCheck" runat="server" CssClass="form-control" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="true" OnSelectedIndexChanged="DetermineCheckEvent">
                            <asp:ListItem>全部確定</asp:ListItem>
                            <asp:ListItem>全部取消</asp:ListItem>
                            <asp:ListItem>無</asp:ListItem>
                        </asp:RadioButtonList>--%>
                    </div>
                    <div class="col-sm-6">
                        <label class="col-form-label">日期選擇：</label>
                        <div class="row text-center">
                            <div class="col-sm-3">
                                <asp:Button ID="Left_Screen" CssClass="btn btn-primary" runat="server" Text="前一天" OnClick="Change_Screen" />
                            </div>
                            <div class="col-sm-6">
                                <asp:TextBox ID="Show_Date" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                            <div class="col-sm-3">
                                <asp:Button ID="Right_Screen" CssClass="btn btn-primary" runat="server" Text="後一天" OnClick="Change_Screen" />
                            </div>
                        </div>  
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
    <hr />
    <div class="container-fluid">
        <div class="text-right mb-2">
            <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
            <asp:Label ID="Label1" runat="server" Text="0" Visible="false"></asp:Label>
            <asp:Label ID="Label4" runat="server" Text="0.00"></asp:Label>
            <asp:Label ID="Label5" runat="server" Text="0.00"></asp:Label>
            <asp:Button ID="SaveBtn" CssClass="btn btn-lg btn-success" runat="server" Text="存檔" OnClick="SaveBtnClick" />
        </div>
        <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" OnItemDataBound="Show_Sc_Type" CssClass="table table-striped table-hover">
            <HeaderStyle CssClass="thead-light" />
            <Columns>
                <asp:BoundColumn DataField="ID" HeaderText="排班編號"></asp:BoundColumn>
                <asp:BoundColumn DataField="Branch_Name" HeaderText="駐點" Visible="false"></asp:BoundColumn>
                <asp:BoundColumn DataField="Staff_No" HeaderText="管家編號"></asp:BoundColumn>
                <asp:BoundColumn DataField="Staff_Name" HeaderText="管家姓名"></asp:BoundColumn>
                <asp:BoundColumn DataField="Start_Time" HeaderText="起始時間"></asp:BoundColumn>
                <asp:BoundColumn DataField="End_Time" HeaderText="終止時間"></asp:BoundColumn>
                <asp:BoundColumn DataField="Cust_No" HeaderText="客戶編號"></asp:BoundColumn>
                <asp:BoundColumn DataField="Cust_Name" HeaderText="客戶姓名"></asp:BoundColumn>
                <asp:BoundColumn DataField="Cust_Addr" HeaderText="客戶地址">
                    <ItemStyle Width="150px" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Cust_Geometry" HeaderText="地址經緯度"></asp:BoundColumn>
                <asp:BoundColumn DataField="Note" HeaderText="單次需求"></asp:BoundColumn>
                <asp:BoundColumn DataField="Cust_Sp_Request" HeaderText="客戶需求">
                    <ItemStyle Width="200px" />
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Job_Result" HeaderText="狀態"></asp:BoundColumn>
                <asp:TemplateColumn HeaderText="排班確認">
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="Job_Flag" Visible="false"></asp:BoundColumn>
                <asp:TemplateColumn HeaderText="確認">
                    <ItemTemplate>
                        <asp:RadioButtonList ID="RadioButtonList3" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem>是</asp:ListItem>
                            <asp:ListItem>否</asp:ListItem>
                        </asp:RadioButtonList>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="SUID" HeaderText="Type" Visible="false"></asp:BoundColumn>
            </Columns>
        </asp:DataGrid>
    </div>
</asp:Content>

<asp:Content ID="Script" ContentPlaceHolderID="Script" runat="server">
    <script>
        document.domain = 'mommyhappy.com';
    </script>
</asp:Content>