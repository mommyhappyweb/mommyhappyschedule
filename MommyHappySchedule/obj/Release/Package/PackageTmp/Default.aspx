﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MommyHappySchedule.Default" MasterPageFile="~/Template/Main.Master" %>
<asp:Content ID="MainStyle" ContentPlaceHolderID="MainStyle" runat="server">
	<link href="/Public/Style/Login.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="login">
        <% if (!Status && Message != "") { %>
        <div class="alert alert-danger"><%=Message %></div>
        <% } %>
        <h1>登入</h1>
        <hr />
        <div class="form-group row">
            <label for="Account" class="col-sm-3 col-form-label">帳號：</label>
            <div class="col-sm-9">
                <asp:TextBox ID="Account" runat="server" AutoCompleteType="Disabled" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="form-group row">
            <label for="Password" class="col-sm-3 col-form-label">密碼：</label>
            <div class="col-sm-9">
                <asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="form-btn text-right">
            <asp:Button ID="LoginBtn" CssClass="btn btn-primary" OnClick="LoginClick" runat="server" Text="登入" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="MainScript" ContentPlaceHolderID="MainScript" runat="server"></asp:Content>
