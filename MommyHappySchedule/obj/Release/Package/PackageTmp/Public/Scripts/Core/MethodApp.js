﻿"use strict";
/**
 * Ajax 應用
 * @param {object} Request 請求
 * @returns {object} Response
 */
function AjaxApp(Request) {
    return function (Request, callback) {
        $.ajax(Request).done(function (Response) {
            callback(Response);
        }).fail(function (Error) {
            console.log(Error);
        });
    };
}