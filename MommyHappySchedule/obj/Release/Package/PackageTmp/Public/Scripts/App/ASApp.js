﻿"use strict";
$(function () {
    Window.ASApp = new ASApp();
});

/**
 * 建構子
 */
function ASApp() {
    this.ScheduleTable = $('.ScheduleTable');

    this.Init();
}

/**
 * 初始化
 */
ASApp.prototype.Init = function () {
    this.TableAddThead();
};

ASApp.prototype.TableAddThead = function () {
    let GetTitle = this.ScheduleTable.children('tbody').children('tr').eq(0);
    let Td = GetTitle.children('td');
    let Thead = '<thead class="thead-light text-center"><tr>';
    for (let i = 0; i < Td.length; i++) {
        Thead += '<td>' + Td.eq(i).text() + '</td>';
    }
    Thead += '</tr></thead>';
    GetTitle.remove();
    this.ScheduleTable.prepend(Thead);
};