﻿"use strict";
$(function () {
    Window.SupervisionApp = new SupervisionApp();
});

/**
 * 建構子
 */
function SupervisionApp()
{
    this.Search = $('#Search');

    this.ListAddBtn = $('#ListAddBtn');

    this.ListBlock = $('#ListBlock');

    this.Table = $('#Table');

    this.ListSave = $('#ListSave');

    this.Prompt = $('#Prompt');

    this.TimeData = [
        { Key: 8, Value: '08:00' },
        { Key: 8.5, Value: '08:30' },
        { Key: 9, Value: '09:00' },
        { Key: 9.5, Value: '09:30' },
        { Key: 10, Value: '10:00' },
        { Key: 10.5, Value: '10:30' },
        { Key: 11, Value: '11:00' },
        { Key: 11.5, Value: '11:30' },
        { Key: 12, Value: '12:00' },
        { Key: 12.5, Value: '12:30' },
        { Key: 13, Value: '13:00' },
        { Key: 13.5, Value: '13:30' },
        { Key: 14, Value: '14:00' },
        { Key: 14.5, Value: '14:30' },
        { Key: 15, Value: '15:00' },
        { Key: 15.5, Value: '15:30' },
        { Key: 16, Value: '16:00' },
        { Key: 16.5, Value: '16:30' },
        { Key: 17, Value: '17:00' },
        { Key: 17.5, Value: '17:30' },
        { Key: 18, Value: '18:00' },
        { Key: 18.5, Value: '18:30' }
    ];

    this.InsertTableTheadData = [
        { Title: '刪除' },
        { Title: '合約編號' },
        { Title: '客戶編號' },
        { Title: '員工編號' },
        { Title: '督導起始時間' },
        { Title: '督導結束時間' }
    ];

    this.QueryTableTheadData = [
        { Title: '刪除' },
        { Title: '合約編號' },
        { Title: '排班日期' },
        { Title: '客戶編號' },
        { Title: '客戶姓名' },
        { Title: '員工編號' },
        { Title: '員工姓名' },
        { Title: '督導起始時間' },
        { Title: '督導結束時間' }
    ];

    this.InsertData = {};

    this.Init();
}

/**
 * 初始化
 */
SupervisionApp.prototype.Init = function () {
    this.PromptMassageRemove();

    this.LoadingDefault();
};

/**
 * 資料載入初始化
 */
SupervisionApp.prototype.LoadingDefault = function () {
    let Selt = this;
    $.ajax({
        url: 'Api/Supervision.asmx/GetCustJobScheduleData',
        type: 'post',
        dataType: "json",
        contentType: 'application/json; charset=UTF-8'
    }).done(function (Response) {
        let Data = JSON.parse(Response.d);
        if (Data.length > 0) {
            Selt.ListAddBtn.remove();
            Selt.ListSave.remove();
            Selt.TableNewThead(Selt.QueryTableTheadData);
            Selt.TableQueryTbody(Data);
            Selt.ListSearchClick();
        } else {
            Selt.InsertData = Data;
            Selt.Search.remove();
            Selt.TableNewThead(Selt.InsertTableTheadData);
            Selt.TableInsertTbody(Data);
            Selt.ListAddClickEvent();
            Selt.ListSaveClick();
        }
        Selt.ListDeleteClickEvent();
    }).fail(function (Error) {
        console.log(Error);
    });
};

/**
 * 搜尋資料點擊
 */
SupervisionApp.prototype.ListSearchClick = function () {
    let Selt = this;
    let KeySelect = $('#KeySelect');
    let Keyword = $('#Keyword');
    let SearchBtn = $('#SearchBtn');

    SearchBtn.on('click', function () {
        let Data = { KeySelect: KeySelect.val(), Keyword: Keyword.val() };
        Selt.ListSearchEvent(Data);
    });
};

/**
 * 搜尋表格列
 * @param {object|array} Data 資料陣列
 */
SupervisionApp.prototype.ListSearchEvent = function (Data) {
    let Selt = this;
    $.ajax({
        url: 'Api/Supervision.asmx/Search',
        type: 'post',
        dataType: "json",
        data: JSON.stringify(Data),
        contentType: 'application/json; charset=UTF-8'
    }).done(function (Response) {
        let Data = JSON.parse(Response.d);
        if (Data.length > 0) {
            Selt.TableRemoveTbody();
            Selt.TableQueryTbody(Data);
        }
        Selt.ListDeleteClickEvent();
    }).fail(function (Error) {
        console.log(Error);
    });
};

/**
 * 儲存資料點擊
 */
SupervisionApp.prototype.ListSaveClick = function () {
    let Selt = this;
    this.ListSave.on('click', function () {
        let ListData = Selt.GetAllListData();
        if (ListData !== null) {
            Selt.ListSaveEvent(ListData);
            Selt.PromptMassageRemove();
        }
    });
};

/**
 * 取得全部列表資料
 * @returns {array} 陣列
 */
SupervisionApp.prototype.GetAllListData = function () {
    let Data = [];
    let Status = false;
    let List = this.Table.children('tbody').children('tr');
    for (let i = 0; i < List.length; i++) {
        let ListTd = List.eq(i).children('td');
        let Schedule_ID = ListTd.eq(1).children('Input[name="Schedule_ID"]').val();
        let Cust_No = ListTd.eq(2).children('Input[name="Cust_No"]').val();
        let Staff_No = ListTd.eq(3).children('Input[name="Staff_No"]').val();
        let E_Start_Time = ListTd.eq(4).children('select[name="E_Start_Time"]').val();
        let E_End_Time = ListTd.eq(5).children('select[name="E_End_Time"]').val();
        if (Schedule_ID !== '') {
            if (Cust_No !== '') {
                let ObjData = {};
                ObjData.Schedule_ID = Schedule_ID;
                ObjData.Cust_No = Cust_No;
                ObjData.Staff_No = Staff_No;
                ObjData.E_Start_Time = E_Start_Time;
                ObjData.E_End_Time = E_End_Time;
                Data.push(ObjData);
            }
        } else {
            alert('第' + (i + 1) + '列合約編號不可為空');
            break;
        }
    }
    return Data;
};

/**
 * 儲存資料事件
 * @param {object|array} Data 資料陣列
 */
SupervisionApp.prototype.ListSaveEvent = function (Data) {
    let Selt = this;
    $.ajax({
        url: 'Api/Supervision.asmx/Insert',
        type: 'post',
        dataType: "json",
        data: JSON.stringify({ Input: Data }),
        contentType: 'application/json; charset=UTF-8'
    }).done(function (Response) {
        let Data = JSON.parse(Response.d);
        if (Data.length > 0) {
            for (let Item in Data) {
                if (Data[Item].Status) {
                    Selt.PromptMassage('新增成功。', true);
                    Selt.TableRemoveTbody();
                } else {
                    Selt.PromptMassage(Data[Item].Message, false);
                }
            }
        } else {
            Selt.PromptMassage('新增失敗。', false);
        }
    }).fail(function (Error) {
        console.log(Error);
    });
};

/**
 * 提示訊息移除
 */
SupervisionApp.prototype.PromptMassageRemove = function () {
    let Selt = this;
    setTimeout(function () {
        Selt.Prompt.empty();
    }, 5000);
};

/**
 * 提示訊息
 * @param {string} Message 訊息
 * @param {boolean} Status 成功或失敗
 */
SupervisionApp.prototype.PromptMassage = function (Message, Status) {
    Status = Status || false;
    let Result = Status ? 'success' : 'danger';
    let Alert = '<div class="alert alert-' + Result + '" role="alert">' + Message + '</div>';
    this.Prompt.append(Alert);
};

/**
 * 新增表格列
 */
SupervisionApp.prototype.ListAddClickEvent = function () {
    let Selt = this;
    this.ListAddBtn.on('click', function () {
        Selt.TableInsertTbody(Selt.InsertData);
    });
};

/**
 * 刪除表格列
 */
SupervisionApp.prototype.ListDeleteClickEvent = function () {
    let Selt = this;
    let ListNumber = this.Table.children('tbody').children('tr');
    for (let i = 0; i < ListNumber.length; i++) {
        let DeleteBtn = ListNumber.eq(i).children('td');
        DeleteBtn.on('click', 'input[name="Delete"]', function () {
            let Supervision_ID = $(this).attr('data-Supervision_ID');
            if (Supervision_ID !== undefined) {
                let Data = { ScheduleSupervision_ID: Supervision_ID };
                Selt.ListDeleteEvent(Data);
            }
            $(this).parent('td').parent('tr').empty();
        });
    }
};

/**
 * 刪除資料
 * @param {object|array} Data 資料陣列
 */
SupervisionApp.prototype.ListDeleteEvent = function (Data) {
    let Selt = this;
    $.ajax({
        url: 'Api/Supervision.asmx/Delete',
        type: 'post',
        dataType: "json",
        data: JSON.stringify({ Input: Data }),
        contentType: 'application/json; charset=UTF-8'
    }).done(function (Response) {
        let Data = JSON.parse(Response.d);
        if (Data.Status) {
            Selt.PromptMassage('刪除成功。', true);
        } else {
            Selt.PromptMassage(Data.Message, false);
        }

        Selt.ListDeleteClickEvent();
    }).fail(function (Error) {
        console.log(Error);
    });
};

/**
 * 建立表格標題
 * @param {obejct|array} Data 資料陣列
 */
SupervisionApp.prototype.TableNewThead = function (Data) {
    let List = '';
    List += '<tr>';
    for (let Item in Data) {
        List += '<th>' + Data[Item].Title + '</th>';
    }
    List += '</tr>';
    let TableHead = this.Table.children('thead');
    $(List).appendTo(TableHead);
};

/**
 * 建立新增資料列
 * @param {object|array} Data 資料陣列
 */
SupervisionApp.prototype.TableInsertTbody = function (Data) {
    let StartDateOption = this.GetSupervisionTime(8);
    let EndDateOption = this.GetSupervisionTime(10);
    let List = '<tr>' +
        '<td><input class="btn btn-danger" type="button" name="Delete" value="刪除" /></td>' +
        '<td><input class="form-control" name="Schedule_ID" type="text" value="' + Data.Schedule_ID + '" required="required" title="合約編號必填" placeholder="合約編號" /></td>' +
        '<td><input class="form-control" name="Cust_No" type="text" required="required" title="客戶編號必填" placeholder="客戶編號" /></td>' +
        '<td><input class="form-control" name="Staff_No" type="text" value="' + Data.Staff_No + '" required="required" title="員工編號必填" placeholder="員工編號" /></td>' +
        '<td><select class="form-control" name="E_Start_Time">' + StartDateOption + '</select></td>' +
        '<td><select class="form-control" name="E_End_Time">' + EndDateOption + '</select></td>' +
        '</tr>';
    let TableBody = this.Table.children('tbody');
    $(List).appendTo(TableBody);
    this.ListDeleteClickEvent();
};

/**
 * 建立查詢資料列
 * @param {object|array} Data 資料陣列
 */
SupervisionApp.prototype.TableQueryTbody = function (Data) {
    let TableBody = this.Table.children('tbody');
    let List = '';
    if (Data.length > 0) {
        for (let Item in Data) {
            List += '<tr><td><input class="btn btn-danger btn-sm" type="button" name="Delete" data-Supervision_ID="' + Data[Item].ScheduleSupervision_ID + '" value="刪除" /></td>' +
                '<td>' + Data[Item].Schedule_ID + '</td>' +
                '<td>' + Data[Item].Job_Date + '</td>' +
                '<td>' + Data[Item].Cust_No + '</td>' +
                '<td>' + Data[Item].Cust_Name + '</td>' +
                '<td>' + Data[Item].Staff_No + '</td>' +
                '<td>' + Data[Item].Staff_Name + '</td>' +
                '<td>' + Data[Item].E_Start_Time + '</select></td>' +
                '<td>' + Data[Item].E_End_Time + '</td></tr>';
        }
        //this.ListDeleteClickEvent();
    } else {
        let TheadLength = this.QueryTableTheadData.length;
        List += '<tr><td colspan="' + TheadLength + '">查無資料</td></tr>';
    }
    $(List).appendTo(TableBody);
};

/**
 * 移除全部資料列
 */
SupervisionApp.prototype.TableRemoveTbody = function () {
    this.Table.children('Tbody').empty();
};

/**
 * 取得督導時間
 * @param {string} DefaultTime 預設時間
 * @returns {string} option字串
 */
SupervisionApp.prototype.GetSupervisionTime = function (DefaultTime) {
    let Str = '';
    let TimeData = this.TimeData;
    for (let Item in TimeData) {
        let Selected = '';
        if (TimeData[Item].Key === DefaultTime) {
            Selected = 'selected="selected"';
        }
        Str += '<option value=' + TimeData[Item].Value + ' ' + Selected + '>' + TimeData[Item].Value + '</option>';
    }
    return Str;
};