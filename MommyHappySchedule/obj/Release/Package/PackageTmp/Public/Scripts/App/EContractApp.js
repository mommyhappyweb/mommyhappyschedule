﻿const App = new Vue({
    el: '#App',
    watch: {
        Loading: function () {
            if (this.LoadingNumber > 8) {
                let Loading = document.getElementById('Loading');
                Loading.classList.remove('action');
                this.LoadingNumber = 0;
            }
        }
    },
    data: {
        LoadingNumber: 0,
        ApiUrl: '/Api/EContract.asmx/',
        Request: {
            url: '',
            data: '',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            method: 'post'
        },
        RequestData: {},
        HolidayData: [],
        HousekeeperData: [],
        BranchData: [],
        Branch_ID: 1,
        Branch_Name: '',
        BranchChargeAllData: [],
        BranchChargeData: [],
        JobTimeData: [],
        BranchStaffData: [],
        BranchStaff_No: '',
        BranchStaff_Name: '',
        Contract_WeekDateNumber: 0,
        Contract_ID: '',
        Contract_Nums: '',
        ContractTypeData: [],
        ContractType_ID: 0,
        ContractYearData: [],
        Contract_Years: 1,
        Contract_Status: 0,
        Contract_S_Date: '',
        Contract_E_Date: '',
        Contract_Total: 0,
        Contract_Week: 0,
        Contract_HolidayNumber: 0,
        Contract_Pay_Amt: 0,
        Contract_HK_Nums: 0,
        AddNumber_Amt: 0,
        Service_Cycle: '',
        Single_Cust_No: '',
        Cust_No: '',
        Cust_Name: '',
        IDCard: '',
        Sex: 1,
        House_Phone: '',
        Cell_Phone: '',
        Other_Phone: '',
        CityData: [],
        City_Name: '',
        AreaData: [],
        Area_Name: '',
        Address: '',
        Conversion_Cust_No: '',
        IsExpectedShow: false,
        IsPaymentShow: false,
        IsContractdateMenuShow: false,
        ScheduleData: [],
        WeekNumber: 0,
        PaymentTypeData: [],
        Payment_Type: 1,
        Payment_Name: '',
        BankData: [],
        Out_Bank: '',
        Out_Acct_No: '',
        C_Pay_Date: '',
        C_Pay_Numbers: 1,
        C_Pay_Tax: 0,
        C_Tax_Flag: 0,
        Payment_Memo: '',
        JobResultData: [],
        ContractPayDetailData: [],
        ContractDateMenu_Index: 0,
        ContractDateMenu_Title: '',
        ContractDateMenu_Job_Price: 0,
        ContractDateMenu_Job_Result_ID: 0,
        ContractDateMenu_Job_Time: 0,
        ContractDateMenu_Job_TimeName: '',
        ContractDateMenu_Holiday_Name: '',
        ContractDateMenu_Job_Note: ''
    },
    mounted: function () {
        this.ScrollFeaturesMenu();
        this.GetHolidayData();
        this.ContractTypeData = this.GetContractTypeData();
        this.ContractYearData = this.GetContractYearData();
        this.GetBranchData();
        this.GetCityData();
        this.JobTimeData = this.GetJobTimeData();
        this.HousekeeperData = this.GetHousekeeperData();
        this.GetPaymentTypeData();
        this.GetBankData();
        this.GetJobResultData();
        this.ContractDateMenuCloseClick();
    },
    methods: {
        /**
         * 判斷行程表代碼
         * @param {object} Data 行程表資料
         * @returns {number} Key
         */
        IsScheduleCode: function (Data) {
            let Key = 0;
            if (!Data.IsHoliday && Data.Job_Time === 1) {
                Key = 1;
            } else if (Data.IsHoliday) {
                Key = 2;
            }
            return Key;
        },
        /**
         * 判斷付款方式
         * @param {number} ID 付款編號
         * @returns {boolean} IsBool
         */
        IsPaymentCode: function () {
            ID = parseInt(this.Payment_Type);
            let IsBool = false;
            if (ID === 3 || ID === 4 || ID === 5 || ID === 7) {
                IsBool = true;
            }
            return IsBool;
        },
        /**
         * 滾動選單
         */
        ScrollFeaturesMenu: function () {
            let Features = document.getElementById('Features');

            window.addEventListener('scroll', function () {
                let Top = 0;
                if (typeof window.pageYOffset !== 'undefined') {
                    Top = window.pageYOffset;
                } else if (typeof document.compatMode !== 'undefined' && document.compatMode !== 'BackCompat') {
                    Top = document.documentElement.scrollTop;
                } else if (typeof document.body !== 'undefined') {
                    Top = document.body.scrollTop;
                }
                if (Top > 100) {
                    Features.classList.add('scroll');
                } else {
                    Features.classList.remove('scroll');
                }
            });
        },
        /**
         * 取得表單新客戶資料
         * @returns {object.<string|object>} Data
         */
        GetFormCustData: function () {
            let Data = {
                Cust_No: this.Cust_No,
                Cust_Name: this.Cust_Name,
                IDCard: this.IDCard,
                Sex: this.Sex,
                House_Phone: this.House_Phone,
                Cell_Phone: this.Cell_Phone,
                Other_Phone: this.Other_Phone,
                Cust_Addr: this.City_Name + this.Area_Name + this.Address
            };
            return Data;
        },
        /**
         * 取得表單合約資料
         * @returns {object.<string|object>} Data
         */
        GetFormContractData: function () {
            let Data = {
                Branch_ID: this.Branch_ID,
                Branch_Name: this.Branch_Name,
                Cust_No: this.Cust_No,
                Contract_Times: this.Contract_Week,
                Charge_Staff_No: this.BranchStaff_No,
                Charge_Staff_Name: this.BranchStaff_Name,
                ContractType_ID: this.ContractType_ID,
                Contract_Nums: this.Contract_Nums,
                Contract_Years: this.Contract_Years,
                Contract_S_Date: this.Contract_S_Date,
                Contract_E_Date: this.Contract_E_Date,
                Contract_U_Date: this.Contract_S_Date,
                Contract_Total: this.Contract_Total,
                Service_Cycle: this.Service_Cycle,
                HK_Nums: this.Contract_HK_Nums,
                Note: this.GetContractTypeName()
            };
            if (this.ContractType_ID === 2) {
                Data.Contract_ID = this.Contract_ID;
            }
            return Data;
        },
        /**
         * 取得合約到期資料
         * @returns {object.<string|object>} Data
         */
        GetFormContractExpire: function () {
            let Data = {
                Cust_No: this.Cust_No,
                Staff_Name: this.BranchStaff_Name,
                Contract_E_Date: this.Contract_E_Date,
                Contract_R_Date: this.Contract_E_Date,
                Service_Cycle: this.Service_Cycle,
                HK_Nums: this.Contract_HK_Nums,
                Contract_Times: this.Contract_Week,
                Contract_Years: this.Contract_Years,
                Contract_Result: 0
            };
            return Data;
        },
        /**
         * 取得合約付款交易內容
         * @returns {object.<string|object>} Data
         */
        GetFormContractPay: function () {
            let Data = {
                Cust_No: this.Cust_No,
                C_Pay_Numbers: this.C_Pay_Numbers,
                Payment_Type: this.Payment_Type,
                Out_Bank: this.Out_Bank,
                Out_Acct_No: this.Out_Acct_No,
                C_Pay_Date: this.C_Pay_Date,
                C_Pay_Amt: this.Contract_Pay_Amt - this.C_Pay_Tax,
                C_Pay_Tax: this.C_Pay_Tax,
                C_Tax_Flag: this.C_Tax_Flag,
                Payment_Memo: this.Payment_Memo,
                Contract_Type: this.ContractType_ID
            };
            return Data;
        },
        /**
         * 取得派班狀態資料
         */
        GetJobResultData: function () {
            this.Request.url = this.ApiUrl + 'GetJobResultData';
            this.Request.method = 'get';
            let Self = this;
            $.ajax(this.Request).done(function (Response) {
                if (typeof Response !== undefined) {
                    let Result = JSON.parse(Response.d);
                    if (Result.Status) {
                        Self.JobResultData = Result.Data;
                        Self.LoadingNumber++;
                    }
                }
            }).fail(function (Error) {
                console.log(Error);
            });
        },
        /**
         * 取得銀行資料
         */
        GetBankData: function () {
            this.Request.url = this.ApiUrl + 'GetBankData';
            this.Request.method = 'get';
            let Self = this;
            $.ajax(this.Request).done(function (Response) {
                if (typeof Response !== undefined) {
                    let Result = JSON.parse(Response.d);
                    if (Result.Status) {
                        Self.BankData = Result.Data;
                        Self.LoadingNumber++;
                    }
                }
            }).fail(function (Error) {
                console.log(Error);
            });
        },
        /**
         * 取得合約付款方式
         */
        GetPaymentTypeData: function () {
            this.Request.url = this.ApiUrl + 'GetPaymentTypeData';
            this.Request.method = 'get';
            let Self = this;
            $.ajax(this.Request).done(function (Response) {
                if (typeof Response !== undefined) {
                    let Result = JSON.parse(Response.d);
                    if (Result.Status) {
                        Self.PaymentTypeData = Result.Data;
                        Self.LoadingNumber++;
                    }
                }
            }).fail(function (Error) {
                console.log(Error);
            });
        },
        /**
         * 取得付款方式名稱
         * @returns {string} Name
         */
        GetPaymentTypeName: function () {
            let Name = '';
            for (let i = 0; i < this.PaymentTypeData.length; i++) {
                if (parseInt(this.Payment_Type) === parseInt(this.PaymentTypeData[i].ID)) {
                    Name = this.PaymentTypeData[i].Name;
                    break;
                }
            }
            return Name;
        },
        /**
         * 取得國定假日資料
         */
        GetHolidayData: function () {
            this.Request.url = this.ApiUrl + 'GetHolidayData';
            this.Request.method = 'get';
            let Self = this;
            $.ajax(this.Request).done(function (Response) {
                if (typeof Response !== undefined) {
                    let Result = JSON.parse(Response.d);
                    if (Result.Status) {
                        Self.HolidayData = Result.Data;
                        Self.LoadingNumber++;
                    }
                }
            }).fail(function (Error) {
                console.log(Error);
            });
        },
        /**
         * 取得管家資料
         * @returns {array.<object|string>} Data
         */
        GetHousekeeperData: function () {
            let Data = [];
            for (let i = 0; i <= 5; i++) {
                let NewObject = { ID: i, Name: i };
                Data.push(NewObject);
            }
            return Data;
        },
        /**
         * 取得部門資料
         */
        GetBranchData: function () {
            this.Request.url = this.ApiUrl + 'GetBranchData';
            this.Request.method = 'get';
            let Self = this;
            $.ajax(this.Request).done(function (Response) {
                if (typeof Response !== undefined) {
                    let Result = JSON.parse(Response.d);
                    if (Result.Status) {
                        Self.BranchData = Result.Data;
                        Self.Branch_ID = Result.Data[0].ID;
                        Self.Branch_Name = Result.Data[0].Branch_Name;
                        Self.GetBranchStaffData(Self.Branch_ID);
                        Self.GetBranchChargeData();
                        Self.LoadingNumber++;
                    }
                }
            }).fail(function (Error) {
                console.log(Error);
            });
        },
        /**
         * 取得駐點收費金額
         * @param {number} Branch_ID 部門編號
         */
        GetBranchChargeData: function () {
            this.Request.url = this.ApiUrl + 'GetBranchChargeData';
            this.Request.method = 'get';
            let Self = this;
            $.ajax(this.Request).done(function (Response) {
                if (typeof Response !== undefined) {
                    let Result = JSON.parse(Response.d);
                    if (Result.Status) {
                        Self.BranchChargeAllData = Result.Data;
                        Self.SetBranchChargeData(Self.Branch_ID);
                        Self.LoadingNumber++;
                    }
                }
            }).fail(function (Error) {
                console.log(Error);
            });
        },
        /**
         * 設定駐點收費金額
         * @param {number} Branch_ID 部門編號
         */
        SetBranchChargeData: function (Branch_ID) {
            for (let i = 0; i < this.BranchChargeAllData.length; i++) {
                if (this.BranchChargeAllData[i].Branch_ID === Branch_ID) {
                    this.BranchChargeData = this.BranchChargeAllData[i];
                    break;
                }
            }
        },
        /**
         * 取得部門員工資料
         * @param {number} Branch_ID 部門編號
         */
        GetBranchStaffData: function (Branch_ID) {
            this.Request.url = this.ApiUrl + 'GetBranchStaffData';
            this.Request.data = JSON.stringify({ 'Branch_ID': Branch_ID });
            this.Request.method = 'post';
            let Self = this;
            $.ajax(this.Request).done(function (Response) {
                if (typeof Response !== undefined) {
                    let Result = JSON.parse(Response.d);
                    if (Result.Status) {
                        Self.BranchStaffData = Result.Data;
                        Self.BranchStaff_No = Result.Data[0].Staff_No;
                        Self.BranchStaff_Name = Result.Data[0].Staff_Name;
                    }
                }
            }).fail(function (Error) {
                console.log(Error);
            });
        },
        /**
         * 取得合約狀態
         * @returns {Array.<object.<number, string>>} Data
         */
        GetContractTypeData: function () {
            let Data = [
                { ID: 0, Name: '新約' },
                { ID: 1, Name: '續約' },
                { ID: 2, Name: '加次' },
                { ID: 3, Name: '單次轉合約' }
            ];
            return Data;
        },
        /**
         * 取得合約狀態名稱
         * @returns {string} Name
         */
        GetContractTypeName: function () {
            let Name = '';
            let ContractTypeData = this.GetContractTypeData();
            for (let i = 0; i < ContractTypeData.length; i++) {
                if (ContractTypeData[i].ID === this.ContractType_ID) {
                    Name = this.ContractType_ID === 0 && this.Contract_Years === 0 ? '單次' : ContractTypeData[i].Name;
                }
            }
            return Name;
        },
        /**
         * 取得合約年限
         * @returns {Array.<object.<number, string>>} Data
         */
        GetContractYearData: function () {
            let Data = [
                { ID: 0, Name: '單次' },
                { ID: 1, Name: '一年約' },
                { ID: 2, Name: '二年約' }
            ];
            return Data;
        },
        /**
         * 取得工作時間
         * @returns {array.<number, string>} Data
         */
        GetJobTimeData: function () {
            let Data = [
                { ID: 0, Name: '上午班' },
                { ID: 1, Name: '下午班' }
            ];
            return Data;
        },
        /**
         * 取的工作時間名稱
         * @param {number} TimeCode 時間編號 
         * @returns {string} Name
         */
        GetJobTimeName: function (TimeCode) {
            let Name = '';
            for (let i = 0; i < this.JobTimeData.length; i++) {
                if (TimeCode === parseInt(this.JobTimeData[i].ID)) {
                    Name = this.JobTimeData[i].Name;
                    break;
                }
            }
            return Name;
        },
        /**
         * 取得城市資料
         */
        GetCityData: function () {
            this.Request.url = this.ApiUrl + 'GetCityData';
            this.Request.method = 'get';
            let Self = this;
            $.ajax(this.Request).done(function (Response) {
                if (typeof Response !== undefined) {
                    let Result = JSON.parse(Response.d);
                    if (Result.Status) {
                        Self.CityData = Result.Data;
                        Self.City_Name = Result.Data[0].City_Name;
                        Self.GetAreaData(Self.City_Name);
                        Self.LoadingNumber++;
                    }
                }
            }).fail(function (Error) {
                console.log(Error);
            });
        },
        /**
         * 取得行政區資料
         * @param {string} City_Name 城市名稱
         */
        GetAreaData: function (City_Name) {
            this.Request.url = this.ApiUrl + 'GetAreaData';
            this.Request.data = JSON.stringify({ 'City_Name': City_Name });
            this.Request.method = 'post';
            let Self = this;
            $.ajax(this.Request).done(function (Response) {
                if (typeof Response !== undefined) {
                    let Result = JSON.parse(Response.d);
                    if (Result.Status) {
                        Self.AreaData = Result.Data;
                        Self.Area_Name = Result.Data[0].Country_Name;
                    }
                }
            }).fail(function (Error) {
                console.log(Error);
            });
        },
        /**
         * 取得預計時間表資料
         * @returns {Array.<object.<string, object>>} Data
         */
        GetIntervalScheduleData: function () {
            let Weekdate = this.$el.querySelectorAll('.weekdate');
            let Data = [];
            for (let i = 0; i < Weekdate.length; i++) {
                let Item = Weekdate[i].querySelectorAll('.weekdate-item');
                let ItemData = [];
                for (let j = 0; j < Item.length; j++) {
                    let Title = Item[j].querySelector('.weekdate-title').textContent;
                    let WeekdateBlock = Item[j].querySelectorAll('.weekdate-block');
                    let ItemObjData = { WeekCode: j, Week: Title, PM: 0, AM: 0 };
                    for (let k = 0; k < WeekdateBlock.length; k++) {
                        let Number = WeekdateBlock[k].querySelector('select').value;
                        if (k > 0) {
                            ItemObjData.PM = Number;
                        } else {
                            ItemObjData.AM = Number;
                        }
                    }
                    if (ItemObjData.PM > 0 || ItemObjData.AM > 0) {
                        ItemData.push(ItemObjData);
                    }
                }
                if (ItemData.length > 0) {
                    Data.push(ItemData);
                }
            }
            this.Contract_WeekDateNumber = Data.length;
            return Data;
        },
        /**
         * 取得星期資料
         * @param {number} WeekCode 星期編號
         * @returns {Array.object<number, string>} Data
         */
        GetWeekName: function (WeekCode) {
            let Data = [
                { ID: 0, Name: '日' },
                { ID: 1, Name: '一' },
                { ID: 2, Name: '二' },
                { ID: 3, Name: '三' },
                { ID: 4, Name: '四' },
                { ID: 5, Name: '五' },
                { ID: 6, Name: '六' }
            ];
            let Name = '';
            for (let i = 0; i < Data.length; i++) {
                if (WeekCode === Data[i].ID) {
                    Name = Data[i].Name;
                    break;
                }
            }
            return Name;
        },
        /**
         * 取得間隔日期
         * @param {number} AddDay 時間戳日期
         * @returns {string} Date
         */
        GetIntervalDate: function (AddDay) {
            let IntervalDate = new Date(AddDay);
            let IntervalMonth = IntervalDate.getMonth() + 1 > 9 ? IntervalDate.getMonth() + 1 : '0' + (IntervalDate.getMonth() + 1);
            let IntervalDay = IntervalDate.getDate() > 9 ? IntervalDate.getDate() : '0' + IntervalDate.getDate();
            return IntervalDate.getFullYear() + '-' + IntervalMonth + '-' + IntervalDay;
        },
        /**
         * 時間表日期排序
         */
        ScheduleDataSort: function () {
            this.ScheduleData = this.ScheduleData.sort(function (a, b) {
                return a.Job_Date < b.Job_Date ? -1 : 1;
            });
        },
        /**
         * 判斷行程表節日
         * @param {string} Ymd 年月日
         * @param {object.<string, object>} Data 行程表資料
         * @returns {object.<string, object>} Data 行程表資料
         */
        IsScheduleHoliday: function (Ymd, Data) {
            for (let j = 0; j < this.HolidayData.length; j++) {
                if (Ymd === this.HolidayData[j].H_Date) {
                    Data.IsHoliday = true;
                    Data.HolidayName = this.HolidayData[j].H_Memo;
                    Data.Job_Result_ID = 9;
                    Data.Job_Result = '國';
                    if (this.Contract_Years !== 0) {
                        this.Contract_HolidayNumber++;
                    }
                }
            }
            return Data;
        },
        GetScheduleObj: function () {
            let Data = {
                Job_Date: '',
                Week: '',
                WeekName: '',
                Cust_No: this.Cust_No,
                IsHoliday: false,
                HolidayName: '',
                Job_Time: '',
                Job_Time_Name: '',
                Job_Result_ID: 0,
                Job_Result: '正常',
                Job_Price: 0,
                Job_Flag: 0,
                HK_Serial: '',
                Note: ''
            };
            return Data;
        },
        /**
         * 建立新的時間表
         * @param {number} ScheduleNumber 時間表數量
         * @param {string} StartDate 起始時間
         * @param {number} Job_Time 班制
         * @param {number} Serial 管家人數
         */
        NewSchedule: function (ScheduleNumber, StartDate, Job_Time, Serial) {
            for (let i = 0; i < ScheduleNumber; i++) {
                let AddDay = new Date(StartDate).setDate(new Date(StartDate).getDate() + i * 14);
                let IntervalDate = new Date(AddDay);
                let NewYmd = this.GetIntervalDate(AddDay);
                let NewObj = this.GetScheduleObj();
                NewObj.Job_Date = NewYmd;
                NewObj.Week = IntervalDate.getDay();
                NewObj.WeekName = this.GetWeekName(IntervalDate.getDay());
                NewObj.Job_Time = Job_Time;
                NewObj.Job_Time_Name = this.GetJobTimeName(Job_Time);
                NewObj.HK_Serial = Serial;
                NewObj = this.IsScheduleHoliday(NewYmd, NewObj);
                //if (NewObj.IsHoliday && this.Contract_Years !== 0) {
                // ScheduleNumber++;
                //}
                this.ScheduleData.push(NewObj);
            }
        },
        /**
         * 建立新的節日時間表
         */
        NewScheduleHoliday: function () {
            let Job_Date = this.ScheduleData[this.ScheduleData.length - 1].Job_Date;
            let Index = 1;
            let Length = this.ScheduleData.length;
            for (let i = 0; i < Length; i++) {
                if (this.ScheduleData[i].IsHoliday) {
                    let AddDay = '';
                    let SingleWeek = this.ScheduleData[Length - 2].Week;
                    let DoubleWeek = this.ScheduleData[Length - 1].Week;
                    if (this.Contract_WeekDateNumber > 1) {
                        if (SingleWeek === DoubleWeek) {
                            AddDay = new Date(Job_Date).setDate(new Date(Job_Date).getDate() + Index * 7);
                        } else {
                            let SingleDate = this.ScheduleData[Length - 2].Job_Date;
                            let DoubleDate = this.ScheduleData[Length - 1].Job_Date;
                            if (Index % 2 === 0) {
                                AddDay = new Date(DoubleDate).setDate(new Date(DoubleDate).getDate() + 1 * 14);
                            } else {
                                AddDay = new Date(SingleDate).setDate(new Date(SingleDate).getDate() + 1 * 14);
                            }
                        }
                    } else {
                        AddDay = new Date(Job_Date).setDate(new Date(Job_Date).getDate() + Index * 7);
                    }
                    let IntervalDate = new Date(AddDay);
                    let NewYmd = this.GetIntervalDate(AddDay);
                    let NewObj = this.GetScheduleObj();
                    NewObj.Job_Date = NewYmd;
                    NewObj.Week = IntervalDate.getDay();
                    NewObj.WeekName = this.GetWeekName(IntervalDate.getDay());
                    NewObj.Job_Time = this.ScheduleData[i].Job_Time;
                    NewObj.Job_Time_Name = this.GetJobTimeName(this.ScheduleData[i].Job_Time);
                    NewObj.HK_Serial = this.ScheduleData[i].HK_Serial;
                    NewObj = this.IsScheduleHoliday(NewYmd, NewObj);
                    this.ScheduleData.push(NewObj);
                    Index++;
                }
            }
        },
        /**
         * 取得時間表資料
         */
        GetScheduleData: function () {
            let IntervalScheduleData = this.GetIntervalScheduleData();
            let NewDate = new Date(this.Contract_S_Date);
            let ScheduleNumber = this.Contract_Years !== 0 ? 12 * this.Contract_Years * 2 : 1;
            this.WeekNumber = 0;
            this.Contract_HK_Nums = 0;
            for (let i = 0; i < IntervalScheduleData.length; i++) {
                let ItemData = IntervalScheduleData[i];
                let ScheduleLength = ItemData.length;
                for (let j = 0; j < ItemData.length; j++) {
                    let SubItemData = ItemData[j];
                    let Key = ScheduleLength;
                    let StartWeek = 0;
                    if (i === 0) {
                        StartWeek = Key > 0 ? SubItemData.WeekCode : SubItemData.WeekCode + 14;
                    } else {
                        StartWeek = Key > 0 ? SubItemData.WeekCode + 7 : SubItemData.WeekCode;
                    }
                    let IntervalDate = new Date(new Date(NewDate).setDate(new Date(NewDate).getDate() + StartWeek - NewDate.getDay()));
                    let StartDate = IntervalDate.getFullYear() + '-' + (IntervalDate.getMonth() + 1) + '-' + IntervalDate.getDate();
                    let AMNumber = parseInt(SubItemData.AM);
                    let PMNumber = parseInt(SubItemData.PM);
                    this.Contract_HK_Nums = AMNumber + PMNumber;
                    for (am = 0; am < AMNumber; am++) {
                        this.NewSchedule(ScheduleNumber, StartDate, 0, am + 1);
                        this.WeekNumber++;
                    }
                    for (pm = 0; pm < PMNumber; pm++) {
                        this.NewSchedule(ScheduleNumber, StartDate, 1, pm + 1);
                        this.WeekNumber++;
                    }
                    ScheduleLength--;
                }
            }

            this.ScheduleDataSort();

            this.NewScheduleHoliday();
        },
        /**
         * 取得服務週期資料
         * @returns {Array.object<number, string>} Data
         */
        GetServiceCycle: function () {
            let Data = [
                { ID: 0, Name: '單次', WeekNumber: 1 },
                { ID: 0.5, Name: '二週一次', WeekNumber: 1 },
                { ID: 1, Name: '一週一次', WeekNumber: 2 },
                { ID: 1.5, Name: '二週三次', WeekNumber: 1 },
                { ID: 2, Name: '一週二次', WeekNumber: 2 },
                { ID: 2.5, Name: '二週五次', WeekNumber: 1 },
                { ID: 3, Name: '一週三次', WeekNumber: 2 },
                { ID: 3.5, Name: '二週七次', WeekNumber: 1 },
                { ID: 4, Name: '一週四次', WeekNumber: 2 },
                { ID: 4.5, Name: '二週九次', WeekNumber: 1 },
                { ID: 5, Name: '一週五次', WeekNumber: 2 },
                { ID: 5.5, Name: '二週十一次', WeekNumber: 1 },
                { ID: 6, Name: '一週六次', WeekNumber: 2 }
            ];
            let Name = '';
            for (let i = 0; i < Data.length; i++) {
                if (this.Contract_Week === Data[i].ID) {
                    Name = Data[i].Name;
                    break;
                }
            }
            return Name;
        },
        /**
         * 計算合約總金額
         */
        GetCalculatedAmount: function () {
            if (this.BranchChargeData !== undefined) {
                this.Contract_Pay_Amt = 0;
                for (let i = 0; i < this.ScheduleData.length; i++) {
                    let Item = this.ScheduleData[i];
                    let Money = 0;
                    if (this.Contract_Years === 0) {
                        Money = parseInt(this.BranchChargeData.Charge_Fee_L4);
                    } else {
                        if (!Item.IsHoliday) {
                            Money = this.Contract_Total >= 24 && this.Contract_WeekDateNumber < 2 ? parseInt(this.BranchChargeData.Charge_Fee_L2) : parseInt(this.BranchChargeData.Charge_Fee_L1);
                            //Money = this.Contract_Total > 24 ? parseInt(this.BranchChargeData.Charge_Fee_L1) : parseInt(this.BranchChargeData.Charge_Fee_L2);
                            if (Item.Week === 0 || Item.Week === 6) {
                                Money += parseInt(this.BranchChargeData.Charge_Fee_L3);
                            }
                        }
                    }
                    this.ScheduleData[i].Job_Price = parseInt(this.AddNumber_Amt) > 0 ? this.AddNumber_Amt : Money;
                    this.Contract_Pay_Amt += parseInt(this.ScheduleData[i].Job_Price);
                }

                if (parseInt(this.C_Tax_Flag)) {
                    if (parseInt(this.AddNumber_Amt) > 0) {
                        this.C_Pay_Tax = Math.round(parseInt(this.AddNumber_Amt) * 0.05);
                        this.Contract_Pay_Amt = parseInt(this.AddNumber_Amt) + this.C_Pay_Tax;
                    } else {
                        this.C_Pay_Tax = Math.round(this.Contract_Pay_Amt * 0.05);
                        this.Contract_Pay_Amt = this.Contract_Pay_Amt + this.C_Pay_Tax;
                    }
                } else {
                    if (this.AddNumber_Amt > 0) {
                        this.Contract_Pay_Amt = this.AddNumber_Amt;
                    }
                }
            }
        },
        /**
         * 取得付款明細資料
         */
        GetContractPayDetailData: function () {
            this.ContractPayDetailData = [];
            let Money = Math.round((this.Contract_Pay_Amt - this.C_Pay_Tax) / this.C_Pay_Numbers);
            let Tax = Math.round(this.C_Pay_Tax / this.C_Pay_Numbers);
            for (let i = 0; i < this.C_Pay_Numbers; i++) {
                let NewDate = new Date(this.C_Pay_Date);
                let MyDate = '';
                if (NewDate.getDate() > 15) {
                    MyDate = NewDate.getFullYear() + '-' + (NewDate.getMonth() + 1) + '-' + 16;
                } else {
                    MyDate = NewDate.getFullYear() + '-' + (NewDate.getMonth() + 1) + '-' + 1;
                }
                let AddDay = new Date(MyDate).setMonth(new Date(MyDate).getMonth() + i * 1);
                let Pay_Date = i === 0 ? this.C_Pay_Date : this.GetIntervalDate(AddDay);
                let NewObj = {
                    Pay_Number: i + 1,
                    B_Pay_Date: Pay_Date,
                    Payment_Type: this.Payment_Type,
                    Pay_Payment_Name: this.GetPaymentTypeName(),
                    Pay_Amt: Money,
                    Amount: 1,
                    Tax: Tax,
                    Amt: Money + Tax,
                    Out_Bank: this.Out_Bank,
                    Out_Acct_No: this.Out_Acct_No,
                    Fulfill: 0,
                    Sales_Memo: this.BranchStaff_No
                };
                this.ContractPayDetailData.push(NewObj);
            }
        },
        /**
         * 付款方式選取事件
         */
        PaymentTypeChangeEvent: function () {
            let IsBool = this.IsPaymentCode();
            if (!IsBool) {
                this.Out_Bank = '';
                this.Out_Acct_No = '';
            }
        },
        /**
         * 部門選取事件
         * @param {object} e 事件
         */
        BranchChangeEvent: function (e) {
            let SelfOption = e.target.options;
            this.Branch_Name = SelfOption[SelfOption.selectedIndex].textContent;
            this.Branch_ID = SelfOption[SelfOption.selectedIndex].value;
            this.GetBranchStaffData(this.Branch_ID);
            this.SetBranchChargeData(this.Branch_ID);
            this.AddNumber_Amt = this.ContractType_ID === 2 ? this.BranchChargeData.Charge_Fee_L4 : 0;
        },
        /**
         * 部門員工選取事件
         * @param {object} e 事件
         */
        BranchChargeStaffEvent: function (e) {
            let SelfOption = e.target.options;
            this.BranchStaff_Name = SelfOption[SelfOption.selectedIndex].textContent;
        },
        /**
         * 城市選取事件
         */
        CityChangeEvent: function () {
            this.GetAreaData(this.City_Name);
        },
        /**
         * 合約項目選取事件
         */
        ContractTypeChargeEvent: function () {
            this.One_Cust_No = '';
            this.Cust_No = '';
            if (this.ContractType_ID === 2) {
                this.Contract_Years = 0;
                this.SetBranchChargeData(this.Branch_ID);
                this.AddNumber_Amt = this.BranchChargeData.Charge_Fee_L4;
            } else {
                this.Contract_Years = 1;
                this.AddNumber_Amt = 0;
            }
        },
        /**
         * 建立合約預計資料事件
         */
        ContractExpectedEvent: function () {
            if (this.Contract_S_Date === '') {
                alert('請選擇合約起始時間!');
                return;
            }
            if (this.C_Pay_Date === '') {
                alert('請選擇預計付款日期!');
                return;
            }
            this.RequestData = {};
            this.Contract_WeekDateNumber = 0;
            this.Contract_HolidayNumber = 0;
            this.Contract_Pay_Amt = 0;
            this.ScheduleData = [];
            this.Contract_E_Date = '';
            this.Contract_Week = 0;
            this.Service_Cycle = '';
            this.Contract_Total = 0;
            this.C_Pay_Tax = 0;

            this.GetScheduleData();

            if (this.ScheduleData.length <= 0) {
                alert('請選擇管家人數');
                this.ContractPayDetailData = [];
                this.IsExpectedShow = false;
                this.IsPaymentShow = false;
                return;
            }

            this.Contract_Total = this.ScheduleData.length - this.Contract_HolidayNumber;

            this.GetCalculatedAmount();

            let Self = this;
            if (this.ContractType_ID === 2) {
                if (this.Contract_ID === '') {
                    alert('請輸入合約ID!');
                    return;
                }
            } else if (this.ContractType_ID === 3) {
                if (this.Cust_No === '') {
                    alert('請輸入轉合約客戶編號!');
                    return;
                }
                this.GetSingleContractScheduleData(function (Data) {
                    if (Data.length > 0) {
                        Self.GetSingleScheduleData(Data);
                    }
                });
            }

            this.Contract_Week = this.Contract_Years !== 0 ? 0.5 * this.WeekNumber : 0;
            this.Service_Cycle = this.GetServiceCycle();

            if (this.ScheduleData.length > 0) {
                this.Contract_E_Date = this.ScheduleData[this.ScheduleData.length - 1].Job_Date;
            }

            this.IsExpectedShow = true;
            this.IsPaymentShow = true;
            this.GetContractPayDetailData();
        },
        /**
         * 取得單次合約行程表資料
         * @param {object.<string, object>} callback 回調
         */
        GetSingleContractScheduleData: function (callback) {
            let Data = { Cust_No: this.Single_Cust_No, Contract_Nums: this.Contract_Nums };
            this.Request.url = this.ApiUrl + 'GetSingleContractScheduleData';
            this.Request.data = JSON.stringify({ Input: Data });
            this.Request.method = 'post';
            let Self = this;
            $.ajax(this.Request).done(function (Response) {
                if (typeof Response !== undefined) {
                    let Result = JSON.parse(Response.d);
                    if (Result.Status) {
                        if (Result.Data.length > 0) {
                            callback(Result.Data);
                        } else {
                            alert('無此單次客戶合約編號');
                        }
                    }
                }
            }).fail(function (Error) {
                console.log(Error);
            });
        },
        /**
         * 取得單次轉合約行程表資料
         * @param {object.<string, object>} Data 單次合約資料
         */
        GetSingleScheduleData: function (Data) {
            let ScheduleDataLength = this.ScheduleData.length;
            for (let i = 0; i < Data.length; i++) {
                let ScheduleData = this.ScheduleData[ScheduleDataLength - (i + 1)];
                let SingleScheduleData = Data[i];
                let NewDate = new Date(SingleScheduleData.Job_Date);
                ScheduleData.Job_Date = SingleScheduleData.Job_Date;
                ScheduleData.Job_Time = SingleScheduleData.Job_Time;
                ScheduleData.Job_Time_Name = this.GetJobTimeName(SingleScheduleData.Job_Time),
                    ScheduleData.Job_Flag = parseInt(SingleScheduleData.Job_Flag);
                ScheduleData.HK_Serial = SingleScheduleData.HK_Serial;
                ScheduleData.IsHoliday = false;
                ScheduleData.HolidayName = SingleScheduleData.HolidayName;
                ScheduleData.Week = NewDate.getDay();
                ScheduleData.WeekName = this.GetWeekName(NewDate.getDay());
                ScheduleData.Job_Result_ID = 0;
                ScheduleData.Job_Result = '正常';
                if (SingleScheduleData.HolidayName !== '') {
                    ScheduleData.IsHoliday = true;
                    ScheduleData.Job_Result_ID = 9;
                    ScheduleData.Job_Result = '國';
                }
                this.ScheduleData[ScheduleDataLength - (i + 1)] = ScheduleData;
            }

            this.ScheduleDataSort();

            this.Contract_E_Date = this.ScheduleData[this.ScheduleData.length - 1].Job_Date;
        },
        /**
         * 合約儲存按鈕
         */
        ContractSaveBtn: function () {
            if (confirm('確定建立新合約?')) {
                //if (this.Contract_Nums === '') {
                //    alert('合約編號不可為空!');
                //    return;
                //} else {
                //    let ContractNumsRegExp = new RegExp('^[0-9]{8}$', 'gm');
                //    let IsCheck = ContractNumsRegExp.test(this.Contract_Nums);
                //    if (!IsCheck) {
                //        alert('合約編號格式錯誤');
                //        return;
                //    }
                //}
                if (this.Cust_No === '') {
                    alert('客戶編號不可為空!');
                    return;
                }

                if (this.Contract_Type === 0) {
                    if (this.Cust_Name === '') {
                        alert('客戶名稱不可為空!');
                        return;
                    }
                    if (this.IDCard !== '') {
                        let IDCardRegExp = new RegExp('^([A-Z]{1}[0-9]{9}|[0-9]{8})$', 'gm');
                        let IsCheck = IDCardRegExp.test(this.IDCard);
                        if (!IsCheck) {
                            alert('統一編號格式錯誤!');
                            return;
                        }
                    }
                    if (this.House_Phone !== '') {
                        let HousePhoneRegExp = new RegExp('[(][0-9]{2,3}[)][0-9]{7,10}$', 'gm');
                        let IsCheck = HousePhoneRegExp.test(this.House_Phone);
                        if (!IsCheck) {
                            alert('住家或公司電話格式錯誤!');
                            return;
                        }
                    }
                    if (this.Cell_Phone === '') {
                        alert('手機號碼不可為空!');
                        return;
                    } else {
                        let PhoneRegExp = new RegExp('^[0-9]{10}$', 'gm');
                        let IsCheck = PhoneRegExp.test(this.Cell_Phone);
                        if (!IsCheck) {
                            alert('手機號碼格式錯誤!');
                            return;
                        }
                    }
                    if (this.Other_Phone !== '') {
                        let PhoneRegExp = new RegExp('^[0-9]{10}$', 'gm');
                        let IsCheck = PhoneRegExp.test(this.Other_Phone);
                        if (!IsCheck) {
                            alert('其他手機號碼格式錯誤!');
                            return;
                        }
                    }
                    if (this.Address === '') {
                        alert('地址不可為空!');
                        return;
                    }
                }

                let IsPayment = this.IsPaymentCode();
                if (IsPayment) {
                    if (this.Out_Bank === '') {
                        alert('請選擇付款銀行!');
                        return;
                    }
                    if (this.Out_Acct_No === '') {
                        alert('請輸入銀行帳號!');
                        return;
                    } else {
                        let BankRegExp = new RegExp('^[0-9]{8,14}$', 'gm');
                        let IsCheck = BankRegExp.test(this.Out_Acct_No);
                        if (!IsCheck) {
                            alert('銀行帳號格式錯誤!');
                            return;
                        }
                    }
                }

                this.RequestData = {
                    CustData: this.GetFormCustData(),
                    CustContractData: this.GetFormContractData(),
                    CustJobSchedule: this.ScheduleData,
                    CustContractExpire: this.GetFormContractExpire(),
                    CustContractPay: this.GetFormContractPay(),
                    CustContractPayDetail: this.ContractPayDetailData
                };

                this.ContractSaveEvent();
            }
        },
        /**
         * 合約儲存事件
         */
        ContractSaveEvent: function () {
            this.Request.url = this.ApiUrl + 'InsertContractData';
            this.Request.data = JSON.stringify({ Input: this.RequestData });
            this.Request.method = 'post';
            let Self = this;
            $.ajax(this.Request).done(function (Response) {
                if (typeof Response !== undefined) {
                    let Result = JSON.parse(Response.d);
                    if (Result.Status) {
                        alert('新增成功');
                        location.href = '/EContract';
                    }
                }
            }).fail(function (Error) {
                console.log(Error);
            });
        },
        /**
         * 時間表日期右鍵選取
         * @param {object} e 事件
         */
        ScheduleDateRightClick: function (e) {
            e.preventDefault();
            let ContractDateMenu = e.target.closest('#App').querySelector('#ContractDateMenu');
            ContractDateMenu.style.top = e.pageY + 'px';
            ContractDateMenu.style.left = e.pageX + 'px';

            let Self = e.target;

            this.IsContractdateMenuShow = true;
            this.ContractDateMenu_Index = Self.dataset.index;
            this.ContractDateMenu_Title = Self.innerText;
            this.ContractDateMenu_Job_Result_ID = Self.dataset.job_result_id;
            this.ContractDateMenu_Job_Price = Self.dataset.job_price;
            this.ContractDateMenu_Job_Time = Self.dataset.job_time;
            this.ContractDateMenu_Job_TimeName = Self.dataset.job_time_name;
            this.ContractDateMenu_Holiday_Name = Self.dataset.holiday_name;
        },
        /**
         * 行程表右鍵選單隱藏
         */
        ContractDateMenuCloseClick: function () {
            let Self = this;
            document.addEventListener('click', function (e) {
                if (e.target.closest('fieldset') && Self.IsContractdateMenuShow) {
                    Self.IsContractdateMenuShow = false;
                }
            });
        },
        /**
         * 合約單筆變更儲存
         */
        ContractDateMenuSave: function () {
            this.Contract_Total = 0;

            let Index = parseInt(this.ContractDateMenu_Index);
            this.ScheduleData[Index].Job_Date = this.ContractDateMenu_Title.split('(')[0];
            this.ScheduleData[Index].Job_Result_ID = this.ContractDateMenu_Job_Result_ID;
            this.ScheduleData[Index].Job_Price = parseInt(this.ContractDateMenu_Job_Price);
            this.ScheduleData[Index].Job_Time = this.ContractDateMenu_Job_Time;
            this.ScheduleData[Index].Job_Time_Name = this.GetJobTimeName(parseInt(this.ContractDateMenu_Job_Time));
            this.ScheduleData[Index].IsHoliday = this.ContractDateMenu_Holiday_Name === '' ? false : true;
            this.ScheduleData[Index].HolidayName = this.ContractDateMenu_Holiday_Name;
            this.ScheduleData[Index].Note = this.ContractDateMenu_Job_Note;
            this.IsContractdateMenuShow = false;

            this.Contract_Total = this.ScheduleData.length - this.Contract_HolidayNumber;
            this.GetCalculatedAmount();
            this.GetContractPayDetailData();
        }
    },
    created() {
     
        var s = getCust();
        console.log(s);

        
    }
});