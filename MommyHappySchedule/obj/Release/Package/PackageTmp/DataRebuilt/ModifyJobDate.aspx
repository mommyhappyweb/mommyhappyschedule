﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ModifyJobDate.aspx.cs" Inherits="MommyHappySchedule.DataRebuilt.ModifyJobDate" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Staff Data</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <center>
                <table>
                    <tr>
                        <td align="center">
                            <table>

                                <tr>
                                   
                                    <td colspan ="3">
                                        <table border="1" cellpadding="1" cellspacing="0">
                                            <tr style="font-size: 10pt; font-family: Arial;">
                                                <td>原始班表</td>
                                                <td> <asp:TextBox ID="TextBox1" runat="server" Width="80px">0</asp:TextBox>
                                                    </td>
                                                <td>第一階班表</td>
                                                <td><asp:TextBox ID="TextBox2" runat="server" Width="80px" TextMode="MultiLine"></asp:TextBox>
                                                    </td>
                                                <td>第二階班表</td>
                                                <td >
                                                    <asp:TextBox ID="TextBox3" runat="server" Width="80px">0</asp:TextBox>
                                                    </td>

                                                <td>第三階班表</td>
                                                <td ><asp:TextBox ID="TextBox4" runat="server" Width="80px">0</asp:TextBox>
                                                    </td>
                                            </tr>
                                            <tr style="font-size: 10pt; font-family: Arial;">
                                                <td>Handling Charges I</td>
                                                <td><asp:TextBox ID="TextBox6" runat="server" Width="200px"></asp:TextBox>                                                   </td>
                                                <td style="width: 74px">Handling Charges II</td>
                                                <td><asp:Label ID="Label2" runat="server"></asp:Label>
                                                    </td>
                                                <td>Tariff Fee</td>
                                                <td><asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="True" Text="Received" />
                                                    </td>
                                                <td>匯率</td>
                                                <td style="width: 116px">
                                                    <asp:Label ID="Label1" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <asp:Button ID="Button1" runat="server" Text="Change Schedule" OnClick="Button1_Click" />
                                                    <asp:Button ID="Button2" runat="server" Text="Reset Change" />
                                                    <asp:Button ID="Button3" runat="server" Text="Save" />
                                                    <asp:Button ID="Button4" runat="server" Text="Calculate" /></td>
                                                <td align="right">
                                                    <asp:Label ID="Label3" runat="server" Text="0"></asp:Label>&nbsp;</td>

                                                <td>
                                                    <asp:Label ID="Label4" runat="server" Text="0.00"></asp:Label>&nbsp;</td>
                                                <td colspan="2" style="text-align: center">
                                                    <asp:Label ID="Label5" runat="server" Text="0.00"></asp:Label>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="8"></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>
                                                    <td style="width: 74px"></td>
                                                    <td>
                                                        <td>
                                                            <td>
                                                                <td></td>
                                                                <td style="width: 116px">
                                            </tr>

                                        </table>
                                    </td>
                                   
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:DataGrid ID="DataGrid1" runat="server" Font-Names="Arial"
                                            BorderWidth="1" Font-Size="Smaller" CellPadding="2" AutoGenerateColumns="True">
                                            
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                                 <tr>
                                    <td colspan="3">
                                        <asp:DataGrid ID="DataGrid2" runat="server" Font-Names="Arial"
                                            BorderWidth="1" Font-Size="Smaller" CellPadding="2" AutoGenerateColumns="True">
                                            
                                        </asp:DataGrid>
                                    </td>
                                </tr>

                                <tr>
                                    <td width="3%">
                                        <img src="images/circle_r4_c2.gif" width="20" height="20"></td>
                                    <td width="92%" background="images/circle_r4_c3.gif">&nbsp;</td>
                                    <td style="width: 11px">
                                        <img src="images/circle_r4_c4.gif" width="21" height="20"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#000000" width="100%">
                                <tr>
                                    <td height="30" valign="middle" bgcolor="#666666" style="width: 100%">
                                       
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="textbox5" runat="server" Width="100%"></asp:TextBox></td>
                    </tr>
                </table>
            </center>
        </div>
    </form>
</body>
</html>
