﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportScheduleJob.aspx.cs" Inherits="MommyHappySchedule.DataRebuilt.ImportScheduleJob" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Job_Detail_Repor</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <center>
                <table>
                    <tr>
                        <td align="center">
                            <table>

                                <tr>

                                    <td colspan="3">
                                        <table border="1" cellpadding="1" cellspacing="0">
                                            <tr style="font-size: 10pt; font-family: Arial;">
                                                <td>駐點：</td>
                                                <td>
                                                    <asp:DropDownList ID="Branch" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>年份</td>
                                                <td>
                                                    <asp:DropDownList ID="Check_Year" runat="server">
                                                        <asp:ListItem>2016</asp:ListItem>
                                                        <asp:ListItem>2017</asp:ListItem>
                                                        <asp:ListItem Selected="True">2018</asp:ListItem>
                                                        <asp:ListItem>2019</asp:ListItem>
                                                        <asp:ListItem>2020</asp:ListItem>
                                                        <asp:ListItem>2021</asp:ListItem>
                                                    </asp:DropDownList></td>
                                                <td>月份</td>
                                                <td>
                                                    <asp:DropDownList ID="Check_Month" runat="server">
                                                        <asp:ListItem Selected="True">1</asp:ListItem>
                                                        <asp:ListItem>2</asp:ListItem>
                                                        <asp:ListItem>3</asp:ListItem>
                                                        <asp:ListItem>4</asp:ListItem>
                                                        <asp:ListItem>5</asp:ListItem>
                                                        <asp:ListItem>6</asp:ListItem>
                                                        <asp:ListItem>7</asp:ListItem>
                                                        <asp:ListItem>8</asp:ListItem>
                                                        <asp:ListItem>9</asp:ListItem>
                                                        <asp:ListItem>10</asp:ListItem>
                                                        <asp:ListItem>11</asp:ListItem>
                                                        <asp:ListItem>12</asp:ListItem>
                                                    </asp:DropDownList></td>
                                                <td>
                                                    <asp:Button ID="Check_Report" runat="server" Text="產生報表" OnClick="Check_Report_Click" /></td>
                                                <td>
                                                    <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                                                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="Index.aspx">返回主選單</asp:HyperLink>
                                                </td>
                                            </tr>
                                            <tr style="font-size: 10pt; font-family: Arial; ">
                                                <td>原始班表</td>
                                                <td>
                                                    <asp:TextBox ID="TextBox1" runat="server" Width="80px">0</asp:TextBox>
                                                </td>
                                                <td>第一階班表</td>
                                                <td>
                                                    <asp:TextBox ID="TextBox2" runat="server" Width="80px" TextMode="MultiLine">0</asp:TextBox>
                                                </td>
                                                <td>第二階班表</td>
                                                <td>
                                                    <asp:TextBox ID="TextBox3" runat="server" Width="80px">0</asp:TextBox>
                                                </td>

                                                <td>第三階班表</td>
                                                <td>
                                                    <asp:TextBox ID="TextBox4" runat="server" Width="80px">0</asp:TextBox>
                                                </td>
                                            </tr>

                                            <tr style="font-size: 10pt; font-family: Arial;">
                                                <td colspan="4">
                                                    <asp:Button ID="Button1" runat="server" Text="Change Schedule" />
                                                    <asp:Button ID="Button2" runat="server" Text="Reset Change" />
                                                    <asp:Button ID="Button3" runat="server" Text="" />
                                                    <asp:Button ID="Button4" runat="server" Text=""  OnClick="Button4_Click"/></td>
                                                <td align="right">
                                                    <asp:Label ID="Label3" runat="server" Text="0"></asp:Label>&nbsp;</td>

                                                <td>
                                                    <asp:Label ID="Label4" runat="server" Text="0.00"></asp:Label>&nbsp;</td>
                                                <td colspan="2" style="text-align: center">
                                                    <asp:Label ID="Label5" runat="server" Text="0.00"></asp:Label>
                                                    <asp:Button ID="Left_Screen" runat="server" Text="<<" OnClick="Change_Screen" /><asp:Button ID="Right_Screen" runat="server" Text=">>" OnClick="Change_Screen" />
                                                </td>
                                            </tr>

                                        </table>
                                    </td>

                                </tr>

                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:DataGrid ID="DataGrid3" runat="server" Font-Names="Arial"
                                            BorderWidth="1" Font-Size="Smaller" CellPadding="2" AutoGenerateColumns="False"
                                            OnItemCommand="Show_Job_Detail" OnItemDataBound="Show_Sc_Type" OnPageIndexChanged="DataGrid_PageIndexChanged"
                                            ForeColor="#333333" GridLines="None" AllowCustomPaging="False" PagerStyle-NextPageText="下一頁" PagerStyle-PrevPageText="前一頁" PagerStyle-Mode="NumericPages" PageSize="15">
                                            <SelectedItemStyle Font-Bold="True" ForeColor="#333333" BackColor="#C5BBAF"></SelectedItemStyle>
                                            <ItemStyle BackColor="#E3EAEB"></ItemStyle>
                                            <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#1C5E55"></HeaderStyle>
                                            <FooterStyle ForeColor="White" BackColor="#1C5E55" Font-Bold="True"></FooterStyle>
                                            <Columns>
                                                <asp:BoundColumn DataField="ID" HeaderText="職稱">
                                                    <ItemStyle Width="30px" />
                                                </asp:BoundColumn>
                                                <%--<asp:TemplateColumn HeaderText="管家" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="HK_Name" runat="server" OnClick="Check_Sc" Text='<%# Bind("HK_Name") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>--%>
                                                <asp:BoundColumn DataField="HK_Name" HeaderText="管家" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle Width="60px"></ItemStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Day_ID" HeaderText="日期" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle Width="20px"></ItemStyle>
                                                </asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="1" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B1" runat="server" OnClick="Check_Sc" Text='<%# Bind("B1") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="2" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B2" runat="server" OnClick="Check_Sc" Text='<%# Bind("B2") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="3" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B3" runat="server" OnClick="Check_Sc" Text='<%# Bind("B3") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="4" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B4" runat="server" OnClick="Check_Sc" Text='<%# Bind("B4") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="5" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B5" runat="server" OnClick="Check_Sc" Text='<%# Bind("B5") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="6" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B6" runat="server" OnClick="Check_Sc" Text='<%# Bind("B6") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="7" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B7" runat="server" OnClick="Check_Sc" Text='<%# Bind("B7") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="8" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B8" runat="server" OnClick="Check_Sc" Text='<%# Bind("B8") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="9" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B9" runat="server" OnClick="Check_Sc" Text='<%# Bind("B9") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="10" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B10" runat="server" OnClick="Check_Sc" Text='<%# Bind("B10") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="11" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B11" runat="server" OnClick="Check_Sc" Text='<%# Bind("B11") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="12" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B12" runat="server" OnClick="Check_Sc" Text='<%# Bind("B12") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="13" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B13" runat="server" OnClick="Check_Sc" Text='<%# Bind("B13") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="14" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B14" runat="server" OnClick="Check_Sc" Text='<%# Bind("B14") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="RN" HeaderText="序" Visible="false">
                                                    <ItemStyle Width="0px" />
                                                </asp:BoundColumn>
                                            </Columns>

                                            <EditItemStyle BackColor="#7C6F57" />
                                            <AlternatingItemStyle BackColor="White" />
                                        </asp:DataGrid>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#000000" width="100%">
                                <tr>
                                    <td height="30" valign="middle" bgcolor="#666666" style="width: 100%"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="textbox5" runat="server" Width="100%"></asp:TextBox></td>
                    </tr>
                </table>
            </center>
        </div>
    </form>
</body>
</html>

