﻿using Models.List;
using Models.Library;
using Models.Library.DataBase;
using Models.Library.Regular;
using System.Configuration;

namespace Models.Method.Share
{
    /// <summary>
    /// 方法基礎
    /// </summary>
    public class MethodBase
    {
        /// <summary>
        /// 連線模式
        /// </summary>
        protected static string DBMode = ConfigurationManager.AppSettings["DBMode"];

        /// <summary>
        /// 連線字串
        /// </summary>
        protected static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];

        /// <summary>
        /// 資料庫方法
        /// </summary>
        protected DB DB = new DB();

        /// <summary>
        /// 日誌
        /// </summary>
        protected Logs Logs = new Logs();

        /// <summary>
        /// 日期方法
        /// </summary>
        protected Date Date = new Date();

        /// <summary>
        /// 正規表達式方法
        /// </summary>
        protected RegularDictionary RegularDictionary = new RegularDictionary();

        /// <summary>
        /// 正規表達式檢查
        /// </summary>
        protected RegularCheck RegularCheck = new RegularCheck();

        /// <summary>
        /// 轉換型態
        /// </summary>
        protected ConvertType ConvertType = new ConvertType();

        /// <summary>
        /// 回應訊息代碼
        /// </summary>
        protected ResponseJsonData MsgCode = new ResponseJsonData();
    }
}