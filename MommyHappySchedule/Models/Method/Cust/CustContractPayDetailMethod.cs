﻿using System.Collections.Generic;
using Models.List;
using Models.Method.Share;

namespace Models.Method.Cust
{
    /// <summary>
    /// 客戶付款明細
    /// </summary>
    public class CustContractPayDetailMethod : MethodBase
    {
        /// <summary>
        /// 取得詳細資料
        /// </summary>
        /// <param name="Eid"></param>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetDetailed(string Eid)
        {
            string Sql = @"SELECT CCPD.ID, CCPD.Pay_ID, CCPD.Pay_Number, CCPD.Payment_Type, CCPD.Out_Bank, CCPD.Out_Acct_No, CCPD.Amount, CCPD.Amt, FORMAT(CCPD.B_Pay_Date, 'yyyy-MM-dd') AS B_Pay_Date, CCPD.Fulfill, CCPD.Sales_Memo, FORMAT(CCPD.R_Pay_Date, 'yyyy-MM-dd') AS R_Pay_Date, CCPD.Tax FROM Cust_Contract_Pay AS CCP JOIN Cust_Contract_Pay_Detail AS CCPD ON CCPD.Pay_ID = CCP.ID WHERE CCP.Contract_ID = @Eid ORDER BY CCPD.B_Pay_Date ASC";
            Dictionary<string, object> SqlVal = new Dictionary<string, object>() { { "Eid", Eid } };
            return DB.GetData(Sql, SqlVal);
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public ResponseJsonData Insert(CustContractPayDetail Input)
        {
            InsertEvent(Input, out string Sql, out Dictionary<string, object> SqlVal);
            int Result = DB.SqlMethod(Sql, SqlVal);
            if (Result > 0)
            {
                MsgCode.Status = true;
                MsgCode.Code = "I0001";
                MsgCode.Message = "新增客戶付款明細成功";
            }
            else
            {
                MsgCode.Code = "I0000";
                MsgCode.Message = "新增客戶付款明細失敗";
            }
            return MsgCode;
        }

        /// <summary>
        /// 新增事件
        /// </summary>
        /// <param name="Input"></param>
        /// <param name="Sql"></param>
        /// <param name="SqlVal"></param>
        public void InsertEvent(CustContractPayDetail Input, out string Sql, out Dictionary<string, object> SqlVal)
        {
            Sql = "INSERT INTO Cust_Contract_Pay_Detail (Pay_ID, Pay_Number, Payment_Type, Out_Bank, Out_Acct_No, Amount, Amt, B_Pay_Date, Fulfill, Sales_Memo, R_Pay_Date, Tax) VALUES (@Pay_ID, @Pay_Number, @Payment_Type, @Out_Bank, @Out_Acct_No, @Amount, @Amt, @B_Pay_Date, @Fulfill, @Sales_Memo, @R_Pay_Date, @Tax)";
            ParameterCheck(Input, out SqlVal);
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public ResponseJsonData Update(CustContractPayDetail Input)
        {
            UpdateEvent(Input, out string Sql, out Dictionary<string, object> SqlVal);
            int Result = DB.SqlMethod(Sql, SqlVal);
            if (Result > 0)
            {
                MsgCode.Status = true;
                MsgCode.Code = "U0001";
                MsgCode.Message = "更新工作時間表成功";
            }
            else
            {
                MsgCode.Code = "U0000";
                MsgCode.Message = "更新工作時間表失敗";
            }
            return MsgCode;
        }

        /// <summary>
        /// 更新事件
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public void UpdateEvent(CustContractPayDetail Input, out string Sql, out Dictionary<string, object> SqlVal)
        {
            Sql = "UPDATE Cust_Contract_Pay_Detail SET Pay_ID = @Pay_ID, Pay_Number = @Pay_Number, Payment_Type = @Payment_Type, Out_Bank = @Out_Bank, Out_Acct_No = @Out_Acct_No, Amount = @Amount, Amt = @Amt, B_Pay_Date = IIF(@B_Pay_Date = '', GETDATE(), FORMAT(@B_Pay_Date, 'yyyy-MM-dd')), Fulfill = @Fulfill, Sales_Memo = @Sales_Memo, R_Pay_Date = IIF(@R_Pay_Date = '', NULL, @R_Pay_Date), Tax = @Tax WHERE ID = @ID";
            ParameterCheck(Input, out SqlVal);
        }

        /// <summary>
        /// 刪除
        /// </summary>
        /// <param name="Did"></param>
        /// <returns></returns>
        public ResponseJsonData Delete(int Did)
        {
            string Sql = "DELETE FROM Cust_Contract_Pay_Detail WHERE ID = @Did";
            Dictionary<string, object> SqlVal = new Dictionary<string, object>() { { "Did", Did } };
            int Result = DB.SqlMethod(Sql, SqlVal);
            if (Result > 0)
            {
                MsgCode.Status = true;
                MsgCode.Code = "D001";
                MsgCode.Message = "付款明細:" + Did + "，刪除成功";
            }
            else
            {
                MsgCode.Code = "D000";
                MsgCode.Message = "付款明細:" + Did + "，刪除失敗";
            }
            return MsgCode;
        }

        /// <summary>
        /// 參數檢查
        /// </summary>
        /// <param name="Input"></param>
        /// <param name="SqlVal"></param>
        private void ParameterCheck(CustContractPayDetail Input, out Dictionary<string, object> SqlVal)
        {
            int CustContractPayDetail_ID = Input.CustContractPayDetail_ID;
            int Pay_ID = Input.Pay_ID;
            int Pay_Number = Input.Pay_Number;
            int Payment_Type = Input.Payment_Type;
            string Out_Bank = Input.Out_Bank;
            string Out_Acct_No = Input.Out_Acct_No;
            int Amount = Input.Amount;
            int Amt = Input.Amt;
            string B_Pay_Date = Input.B_Pay_Date;
            byte Fulfill = Input.Fulfill;
            string Sales_Memo = Input.Sales_Memo;
            string R_Pay_Date = Input.R_Pay_Date;
            float Tax = Input.Tax;

            SqlVal = new Dictionary<string, object> {
                { "ID", CustContractPayDetail_ID },
                { "Pay_ID", Pay_ID },
                { "Pay_Number", Pay_Number },
                { "Payment_Type", Payment_Type },
                { "Out_Bank", Out_Bank },
                { "Out_Acct_No", Out_Acct_No },
                { "Amount", Amount },
                { "Amt", Amt },
                { "B_Pay_Date", B_Pay_Date },
                { "Fulfill", Fulfill },
                { "Sales_Memo", Sales_Memo },
                { "R_Pay_Date", R_Pay_Date },
                { "Tax", Tax }
            };
        }
    }
}