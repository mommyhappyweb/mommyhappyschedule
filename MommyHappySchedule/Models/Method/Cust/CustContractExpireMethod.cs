﻿using System.Collections.Generic;
using Models.List;
using Models.Method.Share;

namespace Models.Method.Cust
{
    /// <summary>
    /// 客戶合約到期資料方法
    /// </summary>
    public class CustContractExpireMethod : MethodBase
    {
        /// <summary>
        /// 新增事件
        /// </summary>
        /// <param name="Input"></param>
        /// <param name="Sql"></param>
        /// <param name="SqlVal"></param>
        public void InsertEvent(CustContractExpire Input, out string Sql, out Dictionary<string, object> SqlVal)
        {
            Sql = "INSERT INTO Cust_Contract_Expire (Contract_ID, Cust_No, Staff_Name, Contract_E_Date, Contract_R_Date, Service_Cycle, HK_Nums, Contract_Times, Contract_Years, Contract_Result, N_HK_Nums, N_Contract_Times, N_Contract_Years, N_Contract_S_Date) VALUES (@Contract_ID, @Cust_No, @Staff_Name, @Contract_E_Date, @Contract_R_Date, @Service_Cycle, @HK_Nums, @Contract_Times, @Contract_Years, @Contract_Result, @N_HK_Nums, @N_Contract_Times, @N_Contract_Years, @N_Contract_S_Date)";
            ParameterCheck(Input, out SqlVal);
        }

        /// <summary>
        /// 更新事件
        /// </summary>
        /// <param name="Input"></param>
        /// <param name="Sql"></param>
        /// <param name="SqlVal"></param>
        public void UpdateEvent(CustContractExpire Input, out string Sql, out Dictionary<string, object> SqlVal)
        {
            Sql = "UPDATE Cust_Contract_Expire SET Contract_ID = @Contract_ID, Cust_No = @Cust_No, Staff_Name = @Staff_Name, Contract_E_Date = @Contract_E_Date, Contract_R_Date = @Contract_R_Date, Service_Cycle = @Service_Cycle, HK_Nums = @HK_Nums, Contract_Times = @Contract_Times, Contract_Years = @Contract_Years, Contract_Result = @Contract_Result, N_HK_Nums = @N_HK_Nums, N_Contract_Times = @N_Contract_Times, N_Contract_Years = @N_Contract_Years, N_Contract_S_Date = @N_Contract_S_Date WHERE ID = @ID";
            ParameterCheck(Input, out SqlVal);

        }

        /// <summary>
        /// 參數檢查
        /// </summary>
        /// <param name="Input"></param>
        /// <param name="SqlVal"></param>
        private void ParameterCheck(CustContractExpire Input, out Dictionary<string, object> SqlVal)
        {
            int ContractExpire_ID = Input.ContractExpire_ID;
            int Contract_ID = Input.Contract_ID;
            string Cust_No = Input.Cust_No;
            string Staff_Name = Input.Staff_Name;
            string Contract_E_Date = Input.Contract_E_Date ?? null;
            string Contract_R_Date = Input.Contract_R_Date ?? null;
            string Service_Cycle = Input.Service_Cycle;
            byte HK_Nums = Input.HK_Nums;
            float Contract_Times = Input.Contract_Times;
            byte Contract_Years = Input.Contract_Years;
            byte Contract_Result = Input.Contract_Result;
            byte N_HK_Nums = Input.N_HK_Nums;
            float N_Contract_Times = Input.N_Contract_Times;
            byte N_Contract_Years = Input.N_Contract_Years;
            string N_Contract_S_Date = Input.N_Contract_S_Date ?? null;

            SqlVal = new Dictionary<string, object>
            {
                { "ID", ContractExpire_ID },
                { "Contract_ID", Contract_ID },
                { "Cust_No", Cust_No },
                { "Staff_Name", Staff_Name },
                { "Contract_E_Date", Contract_E_Date },
                { "Contract_R_Date", Contract_R_Date },
                { "Service_Cycle", Service_Cycle },
                { "HK_Nums", HK_Nums },
                { "Contract_Times", Contract_Times },
                { "Contract_Years", Contract_Years },
                { "Contract_Result", Contract_Result },
                { "N_HK_Nums", N_HK_Nums },
                { "N_Contract_Times", N_Contract_Times },
                { "N_Contract_Years", N_Contract_Years },
                { "N_Contract_S_Date", N_Contract_S_Date }
            };
        }
    }
}