﻿using System.Collections.Generic;
using Models.List;
using Models.Method.Share;

namespace Models.Method.Cust
{
    /// <summary>
    /// 客戶合約付款人資訊方法
    /// </summary>
    public class CustContractPayMethod : MethodBase
    {
        /// <summary>
        /// 取得合約資料
        /// </summary>
        /// <param name="Eid"></param>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetDetailed(string Eid)
        {
            string Sql = @"SELECT TOP (1) ID, Contract_ID, Cust_No, C_Pay_Numbers, Payment_Type, Out_Bank, Out_Acct_No, FORMAT(C_Pay_Date, 'yyyy-MM-dd hh:mm:ss') AS C_Pay_Date, C_Pay_Amt, C_Pay_Tax, C_Tax_Flag, Export_ID, Payment_Memo, ERP_No, Contract_Type FROM Cust_Contract_Pay WHERE Contract_ID = @Eid ORDER BY Contract_ID ASC";
            Dictionary<string, object> SqlVal = new Dictionary<string, object>() { { "Eid", Eid } };
            return DB.GetData(Sql, SqlVal);
        }

        /// <summary>
        /// 新增事件
        /// </summary>
        /// <param name="Input"></param>
        /// <param name="Sql"></param>
        /// <param name="SqlVal"></param>
        public void InsertEvent(CustContractPay Input, out string Sql, out Dictionary<string, object> SqlVal)
        {
            Sql = "INSERT INTO Cust_Contract_Pay (Contract_ID, Cust_No, C_Pay_Numbers, Payment_Type, Out_Bank, Out_Acct_No, C_Pay_Date, C_Pay_Amt, C_Pay_Tax, C_Tax_Flag, Payment_Memo, Contract_Type) VALUES (@Contract_ID, @Cust_No, @C_Pay_Numbers, @Payment_Type, @Out_Bank, @Out_Acct_No, FORMAT(CAST(@C_Pay_Date AS DATETIME), 'yyyy-MM-dd HH:mm:ss', 'zh-tw'), @C_Pay_Amt, @C_Pay_Tax, @C_Tax_Flag, @Payment_Memo, @Contract_Type)";
            ParameterCheck(Input, out SqlVal);
        }

        /// <summary>
        /// 更新事件
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public void UpdateEvent(CustContractPay Input, out string Sql, out Dictionary<string, object> SqlVal)
        {
            Sql = "UPDATE Cust_Contract_Pay SET Contract_ID = @Contract_ID, Cust_No = @Cust_No, C_Pay_Numbers = @C_Pay_Numbers, Payment_Type = @Payment_Type, Out_Bank = @Out_Bank, Out_Acct_No = @Out_Acct_No, C_Pay_Date = FORMAT(CAST(@C_Pay_Date AS DATETIME), 'yyyy-MM-dd HH:mm:ss', 'zh-tw'), C_Pay_Amt = @C_Pay_Amt, C_Pay_Tax = @C_Pay_Tax, C_Tax_Flag = @C_Tax_Flag, Payment_Memo = @Payment_Memo, Contract_Type = @Contract_Type WHERE ID = @ID";
            ParameterCheck(Input, out SqlVal);
        }

        /// <summary>
        /// 參數檢查
        /// </summary>
        /// <param name="Input"></param>
        /// <param name="SqlVal"></param>
        private void ParameterCheck(CustContractPay Input, out Dictionary<string, object> SqlVal)
        {
            int ContractPay_ID = Input.ContractPay_ID;
            int Contract_ID = Input.Contract_ID;
            string Cust_No = Input.Cust_No;
            byte C_Pay_Numbers = Input.C_Pay_Numbers;
            byte Payment_Type = Input.Payment_Type;
            string Out_Bank = Input.Out_Bank;
            string Out_Acct_No = Input.Out_Acct_No;
            string C_Pay_Date = Input.C_Pay_Date;
            int C_Pay_Amt = Input.C_Pay_Amt;
            int C_Pay_Tax = Input.C_Pay_Tax;
            byte C_Tax_Flag = Input.C_Tax_Flag;
            string Payment_Memo = Input.Payment_Memo;
            byte Contract_Type = Input.Contract_Type;

            SqlVal = new Dictionary<string, object>
            {
                { "ID", ContractPay_ID },
                { "Contract_ID", Contract_ID },
                { "Cust_No", Cust_No },
                { "C_Pay_Numbers", C_Pay_Numbers },
                { "Payment_Type", Payment_Type },
                { "Out_Bank", Out_Bank },
                { "Out_Acct_No", Out_Acct_No },
                { "C_Pay_Date", C_Pay_Date },
                { "C_Pay_Amt", C_Pay_Amt },
                { "C_Pay_Tax", C_Pay_Tax },
                { "C_Tax_Flag", C_Tax_Flag },
                { "Payment_Memo", Payment_Memo },
                { "Contract_Type", Contract_Type }
            };
        }
    }
}