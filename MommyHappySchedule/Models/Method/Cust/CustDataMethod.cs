﻿using System.Collections.Generic;
using Models.List;
using Models.Method.Share;
using Models.Library.Api.Google;

namespace Models.Method.Cust
{
    /// <summary>
    /// 客戶資料方法
    /// </summary>
    public class CustDataMethod : MethodBase
    {
        /// <summary>
        /// Google Geocode Api
        /// </summary>
        GoogleGeocodeApi GoogleGeocodeApi = new GoogleGeocodeApi();

        /// <summary>
        /// 新增事件
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public void InsertEvent(CustData Input, out string Sql, out Dictionary<string, object> SqlVal)
        {
            Sql = "IF NOT EXISTS (SELECT Cust_No FROM Cust_Data WHERE Cust_No = @Cust_No) BEGIN INSERT INTO Cust_Data (Cust_No, Cust_Name, Cell_Phone, House_Phone, Other_Phone, Cust_Source, House_Style, House_Floors, House_Area, Intern_Flag, Shift_Flag, Phone_S_Flag, Text_Flag, SP_Time_Flag, In_House, Key_Holder, Favor_Staff, Forbid_Staff, Cust_Job, Cust_Comp, Cust_Addr, Cust_Geometry, Cust_Bulid, Traff_Status, Cust_Sp_Request, Cust_Sp_Forbid, Sex, Birthday, Email, IDCard) VALUES(@Cust_No, @Cust_Name, @Cell_Phone, @House_Phone, @Other_Phone, @Cust_Source, @House_Style, @House_Floors, @House_Area, @Intern_Flag, @Shift_Flag, @Phone_S_Flag, @Text_Flag, @SP_Time_Flag, @In_House, @Key_Holder, @Favor_Staff, @Forbid_Staff, @Cust_Job, @Cust_Comp, @Cust_Addr, @Cust_Geometry, @Cust_Bulid, @Traff_Status, @Cust_Sp_Request, @Cust_Sp_Forbid, @Sex, @Birthday, @Email, @IDCard) END";
            ParameterCheck(Input, out SqlVal);
        }

        /// <summary>
        /// 參數檢查
        /// </summary>
        /// <param name="Input"></param>
        /// <param name="SqlVal"></param>
        private void ParameterCheck(CustData Input, out Dictionary<string, object> SqlVal)
        {
            int Cust_ID = Input.Cust_ID;
            string Cust_No = Input.Cust_No ?? "";
            string Cust_Name = Input.Cust_Name ?? "";
            string Cell_Phone = Input.Cell_Phone ?? "";
            string House_Phone = Input.House_Phone ?? "";
            string Other_Phone = Input.Other_Phone ?? "";
            string Cust_Source = Input.Cust_Source ?? "";
            byte House_Style = Input.House_Style;
            float House_Floors = Input.House_Floors;
            string House_Area = Input.House_Area;
            bool Intern_Flag = Input.Intern_Flag;
            bool Shift_Flag = Input.Shift_Flag;
            bool Phone_S_Flag = Input.Phone_S_Flag;
            bool Text_Flag = Input.Text_Flag;
            bool SP_Time_Flag = Input.SP_Time_Flag;
            bool In_House = Input.In_House;
            string Key_Holder = Input.Key_Holder ?? "";
            string Favor_Staff = Input.Favor_Staff ?? "";
            string Forbid_Staff = Input.Forbid_Staff ?? "";
            string Cust_Job = Input.Cust_Job ?? "";
            string Cust_Comp = Input.Cust_Comp ?? "";
            string Cust_Addr = Input.Cust_Addr ?? "";
            string Cust_Geometry = Input.Cust_Geometry ?? "";
            string Cust_Bulid = Input.Cust_Bulid ?? "";
            string Traff_Status = Input.Traff_Status ?? "";
            string Cust_Sp_Request = Input.Cust_Sp_Request ?? "";
            string Cust_Sp_Forbid = Input.Cust_Sp_Forbid ?? "";
            byte Sex = Input.Sex;
            string Birthday = Input.Birthday ?? null;
            string Email = Input.Email ?? "";
            string IDCard = Input.IDCard ?? "";

            Cust_Geometry = (Cust_Addr != "") ? GoogleGeocodeApi.GetCoordinate(Cust_Addr) : "";

            SqlVal = new Dictionary<string, object>
            {
                { "Cust_ID", Cust_ID },
                { "Cust_No", Cust_No },
                { "Cust_Name", Cust_Name },
                { "Cell_Phone", Cell_Phone },
                { "House_Phone", House_Phone },
                { "Other_Phone", Other_Phone },
                { "Cust_Source", Cust_Source },
                { "House_Style", House_Style },
                { "House_Floors", House_Floors },
                { "House_Area", House_Area },
                { "Intern_Flag", Intern_Flag },
                { "Shift_Flag", Shift_Flag },
                { "Phone_S_Flag", Phone_S_Flag },
                { "Text_Flag", Text_Flag },
                { "SP_Time_Flag", SP_Time_Flag },
                { "In_House", In_House },
                { "Key_Holder", Key_Holder },
                { "Favor_Staff", Favor_Staff },
                { "Forbid_Staff", Forbid_Staff },
                { "Cust_Job", Cust_Job },
                { "Cust_Comp", Cust_Comp },
                { "Cust_Addr", Cust_Addr },
                { "Cust_Geometry", Cust_Geometry },
                { "Cust_Bulid", Cust_Bulid },
                { "Traff_Status", Traff_Status },
                { "Cust_Sp_Request", Cust_Sp_Request },
                { "Cust_Sp_Forbid", Cust_Sp_Forbid },
                { "Sex", Sex },
                { "Birthday", Birthday },
                { "Email", Email },
                { "IDCard", IDCard }
            };
        }
    }
}