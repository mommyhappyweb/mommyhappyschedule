﻿using System.Collections.Generic;
using Models.List;
using Models.Method.Share;

namespace Models.Method.Cust
{
    /// <summary>
    /// 客戶工作時間表方法
    /// </summary>
    public class CustJobScheduleMethod : MethodBase
    {
        /// <summary>
        /// 取得詳細資料
        /// </summary>
        /// <param name="Eid"></param>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetDetailed(string Eid)
        {
            string Sql = @"SELECT ID, FORMAT(Job_Date, 'yyyy-MM-dd') AS Job_Date, Job_Time, Staff_No, Cust_No, Job_Result, Job_Price, Job_Flag, Job_Status, HK_Serial, Contract_ID, Job_Result_ID, Note FROM Cust_Job_Schedule WHERE Contract_ID = @Eid ORDER BY Job_Date ASC";
            Dictionary<string, object> SqlVal = new Dictionary<string, object>() { { "Eid", Eid } };
            return DB.GetData(Sql, SqlVal);
        }

        /// <summary>
        /// 取得時間表派班狀態總數
        /// </summary>
        /// <param name="Eid"></param>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetJobSchudleResultTotal(string Eid)
        {
            string Sql = @"SELECT COUNT(CJS.Job_Result_ID) AS Count, PJ.Name FROM Cust_Job_Schedule AS CJS LEFT JOIN (SELECT ID, Name FROM Public_JobResult) AS PJ ON PJ.ID = CJS.Job_Result_ID WHERE CJS.Contract_ID = @Eid GROUP BY CJS.Job_Result_ID, PJ.Name";
            Dictionary<string, object> SqlVal = new Dictionary<string, object>() { { "Eid", Eid } };
            return DB.GetData(Sql, SqlVal);
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public ResponseJsonData Insert(CustJobSchedule Input)
        {
            InsertEvent(Input, out string Sql, out Dictionary<string, object> SqlVal);
            int Result = DB.SqlMethod(Sql, SqlVal);
            if (Result > 0)
            {
                MsgCode.Status = true;
                MsgCode.Code = "I0001";
                MsgCode.Message = "新增工作時間表成功";
            }
            else
            {
                MsgCode.Code = "I0000";
                MsgCode.Message = "新增工作時間表失敗";
            }
            return MsgCode;
        }

        /// <summary>
        /// 新增事件
        /// </summary>
        /// <param name="Input"></param>
        /// <param name="Sql"></param>
        /// <param name="SqlVal"></param>
        public void InsertEvent(CustJobSchedule Input, out string Sql, out Dictionary<string, object> SqlVal)
        {
            Sql = "INSERT INTO Cust_Job_Schedule (Job_Date, Job_Time, Staff_No, Cust_No, Job_Result, Job_Price, Job_Flag, Contract_ID, Job_Result_ID, Note) VALUES (@Job_Date, @Job_Time, @Staff_No, @Cust_No, @Job_Result, @Job_Price, @Job_Flag, @Contract_ID, @Job_Result_ID, @Note)";
            ParameterCheck(Input, out SqlVal);
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public ResponseJsonData Update(CustJobSchedule Input)
        {
            UpdateEvent(Input, out string Sql, out Dictionary<string, object> SqlVal);
            int Result = DB.SqlMethod(Sql, SqlVal);
            if (Result > 0)
            {
                MsgCode.Status = true;
                MsgCode.Code = "U0001";
                MsgCode.Message = "更新工作時間表成功";
            }
            else
            {
                MsgCode.Code = "U0000";
                MsgCode.Message = "更新工作時間表失敗";
            }
            return MsgCode;
        }

        /// <summary>
        /// 更新事件
        /// </summary>
        /// <param name="Input"></param>
        /// <param name="Sql"></param>
        /// <param name="SqlVal"></param>
        public void UpdateEvent(CustJobSchedule Input, out string Sql, out Dictionary<string, object> SqlVal)
        {
            Sql = "UPDATE Cust_Job_Schedule SET Job_Date = @Job_Date, Job_Time = @Job_Time, Staff_No = @Staff_No, Cust_No = @Cust_No, Job_Result = @Job_Result, Job_Price = @Job_Price, Job_Flag = @Job_Flag, Contract_ID = @Contract_ID, Job_Result_ID = @Job_Result_ID, Note = @Note WHERE ID = @Schedule_ID";
            ParameterCheck(Input, out SqlVal);
        }

        /// <summary>
        /// 刪除
        /// </summary>
        /// <param name="Did"></param>
        /// <returns></returns>
        public ResponseJsonData Delete(int Did)
        {
            string Sql = "DELETE FROM Cust_Job_Schedule WHERE ID = @Did";
            Dictionary<string, object> SqlVal = new Dictionary<string, object>() { { "Did", Did } };
            int Result = DB.SqlMethod(Sql, SqlVal);
            if (Result > 0)
            {
                MsgCode.Status = true;
                MsgCode.Code = "D001";
                MsgCode.Message = "工作時間表:" + Did + "，刪除成功";
            }
            else
            {
                MsgCode.Code = "D000";
                MsgCode.Message = "工作時間表:" + Did + "，刪除失敗";
            }
            return MsgCode;
        }

        /// <summary>
        /// 取得服務時段
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetJobTimeData()
        {
            Dictionary<string, string> Data = new Dictionary<string, string>()
            {
                { "0", "上午" },
                { "1", "下午" }
            };
            return Data;
        }

        /// <summary>
        /// 取得服務狀態
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetJobFlagData()
        {
            Dictionary<string, string> Data = new Dictionary<string, string>()
            {
                { "0", "不能派班" },
                { "1", "可派班" },
                { "2", "已派班" },
                { "3", "服務中" },
                { "4", "已服務" },
                { "5", "扣次不加次" },
                { "255", "已解約" }
            };
            return Data;
        }

        /// <summary>
        /// 參數檢查
        /// </summary>
        /// <param name="Input"></param>
        /// <param name="SqlVal"></param>
        private void ParameterCheck(CustJobSchedule Input, out Dictionary<string, object> SqlVal)
        {
            int Schedule_ID = Input.Schedule_ID;
            string Job_Date = Input.Job_Date;
            byte Job_Time = Input.Job_Time;
            string Staff_No = Input.Staff_No;
            string Cust_No = Input.Cust_No;
            string Job_Result = Input.Job_Result;
            int Job_Price = Input.Job_Price;
            byte Job_Flag = Input.Job_Flag;
            int Contract_ID = Input.Contract_ID;
            byte Job_Result_ID = Input.Job_Result_ID;
            string Note = Input.Note;

            SqlVal = new Dictionary<string, object>
            {
                { "Schedule_ID", Schedule_ID },
                { "Job_Date", Job_Date },
                { "Job_Time", Job_Time  },
                { "Staff_No", Staff_No },
                { "Cust_No", Cust_No },
                { "Job_Result", Job_Result },
                { "Job_Price", Job_Price },
                { "Job_Flag", Job_Flag },
                { "Contract_ID", Contract_ID },
                { "Job_Result_ID", Job_Result_ID },
                { "Note", Note }
            };
        }
    }
}