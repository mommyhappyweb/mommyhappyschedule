﻿using System;
using System.Transactions;
using System.Collections.Generic;
using System.Data.SqlClient;
using Models.List;
using Models.Method.Share;
using System.Linq;
namespace Models.Method.Cust
{
    /// <summary>
    /// 客戶合約方法
    /// </summary>
    public class CustContractDataMethod : MethodBase
    {
        /// <summary>
        /// 取得單次合約行程表資料
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetSingleContractSchedule(CustContractData Input)
        {
            string Contract_Nums = Input.Contract_Nums;
            string Cust_No = Input.Cust_No;
            string Sql = @"SELECT FORMAT(Job_Date, 'yyyy-MM-dd') AS Job_Date, Job_Time, Staff_No, Cust_No, Job_Result, Job_Price, Job_Flag, HK_Serial, Contract_ID, Job_Result_ID, Note, (SELECT H_Memo FROM Holiday_Date WHERE H_Date = Job_Date) AS HolidayName FROM Cust_Job_Schedule WHERE Contract_ID = (SELECT ID FROM Cust_Contract_Data WHERE Cust_No = @Cust_No AND Contract_Nums = @Contract_Nums)";
            Dictionary<string, object> SqlVal = new Dictionary<string, object>
            {
                { "Contract_Nums", Contract_Nums },
                { "Cust_No", Cust_No }
            };
            return DB.GetData(Sql, SqlVal);
        }

        /// <summary>
        /// 取得合約資料
        /// </summary>
        /// <param name="Eid"></param>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetDetailed(string Eid)
        {
            string Sql = @"SELECT TOP (1) ID, Contract_Nums, Charge_Staff_Name, FORMAT(Contract_S_Date, 'yyyy-MM-dd') AS Contract_S_Date, FORMAT(Contract_E_Date, 'yyyy-MM-dd') AS Contract_E_Date, FORMAT(Contract_R_Date, 'yyyy-MM-dd') AS Contract_R_Date, Cust_No, Contract_Times, Contract_Status, FORMAT(Contract_U_Date, 'yyyy-MM-dd') AS Contract_U_Date, Branch_Name, Service_Cycle, Contract_Years, HK_Nums, Branch_ID, Note, Charge_Staff_No FROM Cust_Contract_Data WHERE ID = @Eid";
            Dictionary<string, object> SqlVal = new Dictionary<string, object>() { { "Eid", Eid } };
            return DB.GetData(Sql, SqlVal);
        }

        /// <summary>
        /// 取得合約編號
        /// </summary>
        /// <param name="Input"></param>
        /// <param name="Conn"></param>
        /// <returns></returns>
        public int GetContractID(CustContractData Input, SqlConnection Conn)
        {
            int ContractID = 0;
            string Cust_No = Input.Cust_No;
            string Contract_S_Date = Input.Contract_S_Date;
            string Sql = "SELECT TOP (1) ID FROM Cust_Contract_Data WHERE Cust_No = @Cust_No AND Contract_S_Date = @Contract_S_Date ORDER BY ID DESC";
            Dictionary<string, object> SqlVal = new Dictionary<string, object>
            {
                { "Cust_No", Cust_No },
                { "Contract_S_Date", Contract_S_Date }
            };
            using (SqlCommand Command = new SqlCommand(Sql, Conn))
            {
                foreach (KeyValuePair<string, object> Item in SqlVal)
                {
                    Command.Parameters.AddWithValue("@" + Item.Key, Item.Value);
                }
                using (SqlDataReader Reader = Command.ExecuteReader())
                {
                    if (Reader != null && Reader.HasRows)
                    {
                        while (Reader.Read())
                        {
                            ContractID = ConvertType.Int(Reader["ID"]);
                        }
                    }
                }
            }
            return ContractID;
        }

        /// <summary>
        /// 取得合約付款人資訊編號
        /// </summary>
        /// <param name="Input"></param>
        /// <param name="Conn"></param>
        /// <returns></returns>
        public int GetPayID(CustContractPay Input, SqlConnection Conn)
        {
            int PayID = 0;
            int Contract_ID = Input.Contract_ID;
            string Cust_No = Input.Cust_No;
            string Sql = "SELECT TOP (1) ID FROM Cust_Contract_Pay WHERE Contract_ID = @Contract_ID AND Cust_No = @Cust_No";
            Dictionary<string, object> SqlVal = new Dictionary<string, object>
            {
                { "Contract_ID", Contract_ID },
                { "Cust_No", Cust_No }
            };
            using (SqlCommand Command = new SqlCommand(Sql, Conn))
            {
                foreach (KeyValuePair<string, object> Item in SqlVal)
                {
                    Command.Parameters.AddWithValue("@" + Item.Key, Item.Value);
                }
                using (SqlDataReader Reader = Command.ExecuteReader())
                {
                    if (Reader != null && Reader.HasRows)
                    {
                        while (Reader.Read())
                        {
                            PayID = ConvertType.Int(Reader["ID"]);
                        }
                    }
                }
            }
            return PayID;
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public ResponseJsonData Insert(CustContractDataList Input)
        {
            using (TransactionScope Transaction = new TransactionScope())
            {
                using (SqlConnection Conn = DB.GetConn())
                {
                    try
                    {
                        Dictionary<string, bool> dict = new Dictionary<string, bool>();

                        int Contract_ID = 0;
                        int Pay_ID = 0;
                        // 新增客戶資料
                        int CustDataInsertResult = CustDataInsert(Input.CustData);
                        // 新增合約資料
                        int ContractDataInsertResult = ContractDataInsert(Input.CustContractData);
                        if (ContractDataInsertResult <= 0)
                        {
                            dict.Add("ContractDataInsertResult", false);
                        }

                        if (dict.Count <= 0)
                        {
                            // 取得新增合約資料編號
                            Contract_ID = GetContractID(Input.CustContractData, Conn);
                            // 新增行程表資料
                            foreach (CustJobSchedule Item in Input.CustJobSchedule)
                            {
                                if (dict.Count <= 0)
                                {
                                    Item.Contract_ID = Contract_ID;
                                    int CustJobScheduleInsertResult = CustJobScheduleInsert(Item);
                                    if (CustJobScheduleInsertResult <= 0)
                                    {
                                        dict.Add("CustJobScheduleInsertResult", false);
                                    }
                                }
                            }
                        }

                        if (dict.Count <= 0)
                        {
                            // 新增合約到期資料
                            Input.CustContractExpire.Contract_ID = Contract_ID;
                            int CustContractExpireInsertResult = CustContractExpireInsert(Input.CustContractExpire);
                            if (CustContractExpireInsertResult <= 0)
                            {
                                dict.Add("CustContractExpireInsertResult", false);
                            }
                        }


                        if (dict.Count <= 0)
                        {
                            // 新增合約付款交易內容
                            Input.CustContractPay.Contract_ID = Contract_ID;
                            int CustContractPayInsertResult = CustContractPayInsert(Input.CustContractPay);
                            if (CustContractPayInsertResult <= 0)
                            {
                                dict.Add("CustContractPayInsertResult", false);
                            }
                        }

                        if (dict.Count <= 0)
                        {
                            Pay_ID = GetPayID(Input.CustContractPay, Conn);
                            // 新增合約付款交易明細
                            foreach (CustContractPayDetail Item in Input.CustContractPayDetail)
                            {
                                if (dict.Count <= 0)
                                {
                                    Item.Pay_ID = Pay_ID;
                                    int CustContractPayDetailInsertResult = CustContractPayDetailInsert(Item);
                                    if (CustContractPayDetailInsertResult <= 0)
                                    {
                                        dict.Add("CustContractPayDetailInsertResult", false);
                                    }
                                }
                            }
                        }

                        if (dict.Count <= 0)
                        {
                            Transaction.Complete();
                            MsgCode.Status = true;
                            MsgCode.Code = "I0001";
                            MsgCode.Message = "新增成功";
                        }
                        else
                        {
                            MsgCode.Code = "I0000";
                            //MsgCode.Message = "新增失敗，請稍後再試:" + dict.First().Key;
                            MsgCode.Message = "新增失敗，請稍後再試。";

                            new Models.Library.DataBase.DB().RecException(string.Format("新增失敗。Data:{0},Dict:{1}", Newtonsoft.Json.JsonConvert.SerializeObject(Input), Newtonsoft.Json.JsonConvert.SerializeObject(dict)));

                        }
                    }
                    catch (InvalidOperationException Ex)
                    {
                        MsgCode.Code = "I0000";
                        MsgCode.Message = "新增失敗，" + Ex.ToString();
                    }
                }
            }
            return MsgCode;
        }

        /// <summary>
        /// 客戶資料新增
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public int CustDataInsert(CustData Input)
        {
            CustDataMethod CustDataMethod = new CustDataMethod();
            CustDataMethod.InsertEvent(Input, out string Sql, out Dictionary<string, object> SqlVal);
            int Result = DB.SqlMethodTransaction(Sql, SqlVal);
            return Result;
        }

        /// <summary>
        /// 合約資料新增
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public int ContractDataInsert(CustContractData Input)
        {
            InsertEvent(Input, out string Sql, out Dictionary<string, object> SqlVal);
            int Result = DB.SqlMethodTransaction(Sql, SqlVal);
            return Result;
        }

        /// <summary>
        /// 客戶工作時間表新增
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public int CustJobScheduleInsert(CustJobSchedule Input)
        {
            CustJobScheduleMethod CustJobScheduleMethod = new CustJobScheduleMethod();
            CustJobScheduleMethod.InsertEvent(Input, out string Sql, out Dictionary<string, object> SqlVal);
            int Result = DB.SqlMethodTransaction(Sql, SqlVal);
            return Result;
        }

        /// <summary>
        /// 合約到期資料新增
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public int CustContractExpireInsert(CustContractExpire Input)
        {
            CustContractExpireMethod CustContractExpireMethod = new CustContractExpireMethod();
            CustContractExpireMethod.InsertEvent(Input, out string Sql, out Dictionary<string, object> SqlVal);
            int Result = DB.SqlMethodTransaction(Sql, SqlVal);
            return Result;
        }

        /// <summary>
        /// 合約付款交易內容新增
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public int CustContractPayInsert(CustContractPay Input)
        {
            CustContractPayMethod CustContractPayMethod = new CustContractPayMethod();
            CustContractPayMethod.InsertEvent(Input, out string Sql, out Dictionary<string, object> SqlVal);
            int Result = DB.SqlMethodTransaction(Sql, SqlVal);
            return Result;
        }

        /// <summary>
        /// 合約付款交易明細新增
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public int CustContractPayDetailInsert(CustContractPayDetail Input)
        {
            CustContractPayDetailMethod CustContractPayDetailMethod = new CustContractPayDetailMethod();
            CustContractPayDetailMethod.InsertEvent(Input, out string Sql, out Dictionary<string, object> SqlVal);
            int Result = DB.SqlMethodTransaction(Sql, SqlVal);
            return Result;
        }

        /// <summary>
        /// 新增事件
        /// </summary>
        /// <param name="Input"></param>
        /// <param name="Sql"></param>
        /// <param name="SqlVal"></param>
        public void InsertEvent(CustContractData Input, out string Sql, out Dictionary<string, object> SqlVal)
        {
            Sql = "INSERT INTO Cust_Contract_Data (Contract_Nums, Contract_S_Date, Contract_E_Date, Cust_No, Contract_Times, Contract_Status, Contract_U_Date, Branch_Name, Service_Cycle, Contract_Years, HK_Nums, Branch_ID, Note, Charge_Staff_No, Charge_Staff_Name) VALUES (@Contract_Nums, @Contract_S_Date, @Contract_E_Date, @Cust_No, @Contract_Times, @Contract_Status, @Contract_U_Date, @Branch_Name, @Service_Cycle, @Contract_Years, @HK_Nums, @Branch_ID, @Note, @Charge_Staff_No, @Charge_Staff_Name)";
            ParameterCheck(Input, out SqlVal);
        }

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public ResponseJsonData Update(CustContractDataList Input)
        {
            using (TransactionScope Transaction = new TransactionScope())
            {
                using (SqlConnection Conn = DB.GetConn())
                {
                    int ContractDataUpdateResult = ContractDataUpdate(Input.CustContractData);
                    foreach (CustJobSchedule Item in Input.CustJobSchedule)
                    {
                        int CustJobScheduleUpdateResult = CustJobScheduleUpdate(Item);
                    }
                    //int CustContractExpireUpdateResult = CustContractExpireUpdate(Input.CustContractExpire);
                    int CustContractPayUpdateResult = CustContractPayUpdate(Input.CustContractPay);
                    foreach (CustContractPayDetail Item in Input.CustContractPayDetail)
                    {
                        int CustContractPayDetailResult = CustContractPayDetailUpdate(Item);
                    }
                    try
                    {
                        Transaction.Complete();
                        MsgCode.Status = true;
                        MsgCode.Code = "U0001";
                        MsgCode.Message = "更新成功";
                    }
                    catch (InvalidOperationException Ex)
                    {
                        MsgCode.Code = "U0000";
                        MsgCode.Message = "更新失敗" + Ex.ToString();
                    }
                }
            }
            return MsgCode;
        }

        /// <summary>
        /// 合約資料更新
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public int ContractDataUpdate(CustContractData Input)
        {
            UpdateEvent(Input, out string Sql, out Dictionary<string, object> SqlVal);
            int Result = DB.SqlMethodTransaction(Sql, SqlVal);
            return Result;
        }

        /// <summary>
        /// 客戶工作時間表更新
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public int CustJobScheduleUpdate(CustJobSchedule Input)
        {
            CustJobScheduleMethod CustJobScheduleMethod = new CustJobScheduleMethod();
            CustJobScheduleMethod.UpdateEvent(Input, out string Sql, out Dictionary<string, object> SqlVal);
            int Result = DB.SqlMethodTransaction(Sql, SqlVal);
            return Result;
        }

        /// <summary>
        /// 合約到期資料更新
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public int CustContractExpireUpdate(CustContractExpire Input)
        {
            CustContractExpireMethod CustContractExpireMethod = new CustContractExpireMethod();
            CustContractExpireMethod.UpdateEvent(Input, out string Sql, out Dictionary<string, object> SqlVal);
            int Result = DB.SqlMethodTransaction(Sql, SqlVal);
            return Result;
        }

        /// <summary>
        /// 客戶合約付款人更新
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public int CustContractPayUpdate(CustContractPay Input)
        {
            CustContractPayMethod CustContractPayMethod = new CustContractPayMethod();
            CustContractPayMethod.UpdateEvent(Input, out string Sql, out Dictionary<string, object> SqlVal);
            int Result = DB.SqlMethodTransaction(Sql, SqlVal);
            return Result;
        }

        /// <summary>
        /// 客戶合約付款明細更新
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public int CustContractPayDetailUpdate(CustContractPayDetail Input)
        {
            CustContractPayDetailMethod CustContractPayDetailMethod = new CustContractPayDetailMethod();
            CustContractPayDetailMethod.UpdateEvent(Input, out string Sql, out Dictionary<string, object> SqlVal);
            int Result = DB.SqlMethodTransaction(Sql, SqlVal);
            return Result;
        }

        /// <summary>
        /// 更新事件
        /// </summary>
        /// <param name="Input"></param>
        /// <param name="Sql"></param>
        /// <param name="SqlVaL"></param>
        public void UpdateEvent(CustContractData Input, out string Sql, out Dictionary<string, object> SqlVal)
        {
            Sql = "UPDATE Cust_Contract_Data SET Contract_Nums = @Contract_Nums, Contract_S_Date = IIF(@Contract_S_Date = '', NULL, @Contract_S_Date), Contract_E_Date = IIF(@Contract_E_Date = '', NULL, @Contract_E_Date), Contract_R_Date = IIF(@Contract_R_Date = '', NULL, @Contract_R_Date), Cust_No = @Cust_No, Contract_Times = @Contract_Times, Contract_Status = @Contract_Status, Contract_U_Date = IIF(@Contract_U_Date = '', NULL, @Contract_U_Date), Branch_Name = @Branch_Name, Service_Cycle = @Service_Cycle, Contract_Years = @Contract_Years, HK_Nums = @HK_Nums, Branch_ID = @Branch_ID, Note = @Note, Charge_Staff_No = @Charge_Staff_No, Charge_Staff_Name = @Charge_Staff_Name WHERE ID = @Contract_ID";
            ParameterCheck(Input, out SqlVal);
        }

        /// <summary>
        /// 刪除
        /// </summary>
        /// <param name="Contract_ID">合約編號</param>
        /// <returns></returns>
        public ResponseJsonData Delete(int Contract_ID)
        {
            using (TransactionScope Transaction = new TransactionScope())
            {
                using (SqlConnection Conn = DB.GetConn())
                {
                    int Result = 0;
                    string CustJobScheduleSupervisionSql = "DELETE FROM Cust_Job_Schedule_Supervision WHERE ID IN (SELECT CJSP.ID FROM Cust_Job_Schedule_Supervision AS CJSP JOIN Cust_Job_Schedule AS CJS ON CJS.ID = CJSP.Schedule_ID WHERE CJS.Contract_ID = @Contract_ID)";
                    using (SqlCommand Command = new SqlCommand(CustJobScheduleSupervisionSql, Conn))
                    {
                        Command.Parameters.AddWithValue("@Contract_ID", Contract_ID);
                        Command.ExecuteNonQuery();
                    }

                    string CustJobSchedulePunchSql = "DELETE FROM Cust_Job_Schedule_Punch WHERE ID IN (SELECT CJSP.ID FROM Cust_Job_Schedule_Punch AS CJSP JOIN Cust_Job_Schedule AS CJS ON CJS.ID = CJSP.Schedule_ID WHERE CJS.Contract_ID = @Contract_ID)";
                    using (SqlCommand Command = new SqlCommand(CustJobSchedulePunchSql, Conn))
                    {
                        Command.Parameters.AddWithValue("@Contract_ID", Contract_ID);
                        Command.ExecuteNonQuery();
                    }

                    string ContractPayDetailSql = "DELETE FROM Cust_Contract_Pay_Detail WHERE Pay_ID IN (SELECT CCPD.Pay_ID FROM Cust_Contract_Pay_Detail AS CCPD JOIN Cust_Contract_Pay AS CCP ON CCP.ID = CCPD.Pay_ID WHERE CCP.Contract_ID = @Contract_ID)";
                    using (SqlCommand Command = new SqlCommand(ContractPayDetailSql, Conn))
                    {
                        Command.Parameters.AddWithValue("@Contract_ID", Contract_ID);
                        Command.ExecuteNonQuery();
                    }

                    string ContractPaySql = "DELETE FROM Cust_Contract_Pay WHERE Contract_ID = @Contract_ID";
                    using (SqlCommand Command = new SqlCommand(ContractPaySql, Conn))
                    {
                        Command.Parameters.AddWithValue("@Contract_ID", Contract_ID);
                        Command.ExecuteNonQuery();
                    }

                    string JobScheduleSql = "DELETE FROM Cust_Job_Schedule WHERE Contract_ID = @Contract_ID";
                    using (SqlCommand Command = new SqlCommand(JobScheduleSql, Conn))
                    {
                        Command.Parameters.AddWithValue("@Contract_ID", Contract_ID);
                        Command.ExecuteNonQuery();
                    }

                    string ContractExpireSql = "DELETE FROM Cust_Contract_Expire WHERE Contract_ID = @Contract_ID AND Cust_No IN (SELECT Cust_No FROM Cust_Contract_Data WHERE Contract_ID = @Contract_ID)";
                    using (SqlCommand Command = new SqlCommand(ContractExpireSql, Conn))
                    {
                        Command.Parameters.AddWithValue("@Contract_ID", Contract_ID);
                        Command.ExecuteNonQuery();
                    }

                    string ContractSql = "DELETE FROM Cust_Contract_Data WHERE ID = @Contract_ID";
                    using (SqlCommand Command = new SqlCommand(ContractSql, Conn))
                    {
                        Command.Parameters.AddWithValue("@Contract_ID", Contract_ID);
                        Result = Command.ExecuteNonQuery();
                    }
                    if (Result > 0)
                    {
                        MsgCode.Status = true;
                        MsgCode.Code = "D001";
                        MsgCode.Message = "合約:" + Contract_ID + "，刪除成功";
                        Transaction.Complete();
                    }
                    else
                    {
                        MsgCode.Code = "D000";
                        MsgCode.Message = "合約:" + Contract_ID + "，刪除失敗";
                    }
                }
            }
            return MsgCode;
        }

        /// <summary>
        /// 取得是否外加稅
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetYesNoData()
        {
            Dictionary<string, string> Data = new Dictionary<string, string>()
            {
                { "0", "否" },
                { "1", "是" }
            };
            return Data;
        }

        /// <summary>
        /// 參數檢查
        /// </summary>
        /// <param name="Input"></param>
        /// <param name="SqlVal"></param>
        private void ParameterCheck(CustContractData Input, out Dictionary<string, object> SqlVal)
        {
            int Contract_ID = Input.Contract_ID;
            string Contract_Nums = Input.Contract_Nums;
            string Contract_S_Date = Input.Contract_S_Date;
            string Contract_E_Date = Input.Contract_E_Date;
            string Contract_R_Date = Input.Contract_R_Date;
            string Cust_No = Input.Cust_No;
            float Contract_Times = Input.Contract_Times;
            byte Contract_Status = Input.Contract_Status;
            string Contract_U_Date = Input.Contract_U_Date;
            string Branch_Name = Input.Branch_Name;
            string Service_Cycle = Input.Service_Cycle;
            byte Contract_Years = Input.Contract_Years;
            byte HK_Nums = Input.HK_Nums;
            int Branch_ID = Input.Branch_ID;
            string Note = Input.Note;
            string Charge_Staff_No = Input.Charge_Staff_No;
            string Charge_Staff_Name = Input.Charge_Staff_Name;

            SqlVal = new Dictionary<string, object>()
            {
                { "Contract_ID", Contract_ID },
                { "Contract_Nums", Contract_Nums },
                { "Contract_S_Date", Contract_S_Date },
                { "Contract_E_Date", Contract_E_Date },
                { "Contract_R_Date", Contract_R_Date },
                { "Cust_No", Cust_No },
                { "Contract_Times", Contract_Times },
                { "Contract_Status", Contract_Status },
                { "Contract_U_Date", Contract_U_Date },
                { "Branch_Name", Branch_Name },
                { "Service_Cycle", Service_Cycle },
                { "Contract_Years", Contract_Years },
                { "HK_Nums", HK_Nums },
                { "Branch_ID", Branch_ID },
                { "Note", Note },
                { "Charge_Staff_No", Charge_Staff_No },
                { "Charge_Staff_Name", Charge_Staff_Name }
            };
        }
    }
}