﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Models.List;
using Models.Library;
using Models.Method.Share;

namespace Models.Method.Cust
{
    /// <summary>
    /// 客戶合約督導方法
    /// </summary>
    public class CustJobScheduleSupervisionMethod : MethodBase
    {
        /// <summary>
        /// 取得督導資料
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public List<CustJobScheduleSupervision> GetData(CustJobSchedule Input)
        {
            int Schedule_ID = Input.Schedule_ID;
            string Job_Date = Input.Job_Date ?? "";
            int Job_Result_ID = Input.Job_Result_ID;

            string Sql = "SELECT CJS.ID, CJSS.ID, CJS.Job_Date, CJSS.Cust_No, CD.Cust_Name, CJSS.Staff_No, SD.Name AS Staff_Name, CJSS.E_Start_Time, CJSS.E_End_Time FROM Cust_Job_Schedule AS CJS JOIN Cust_Job_Schedule_Supervision AS CJSS ON CJSS.Schedule_ID = CJS.ID JOIN Cust_Data AS CD ON CD.Cust_No = CJSS.Cust_No JOIN Staff_Data AS SD ON SD.Staff_No = CJSS.Staff_No WHERE CJS.ID = @ID AND CJS.Job_Date = @Job_Date AND CJS.Job_Result_ID = @Job_Result_ID";
            Dictionary<string, object> SqlVal = new Dictionary<string, object>()
            {
                { "ID", Schedule_ID },
                { "Job_Date", Job_Date },
                { "Job_Result_ID", Job_Result_ID }
            };
            SqlDataReader Result = DB.Query(Sql, SqlVal);
            List<CustJobScheduleSupervision> DataList = ConvertArray(Result);
            DB.CloseReader(Result);
            DB.CloseConn();
            return DataList;
        }

        /// <summary>
        /// 取得督導搜尋資料
        /// </summary>
        /// <param name="SearchTitle">搜尋標題</param>
        /// <param name="SearchValue">搜尋值</param>
        /// <returns></returns>
        public List<CustJobScheduleSupervision> GetSearchData(string KeySelect = "", string Keyword = "")
        {
            string SqlLike = (KeySelect != "") ? "WHERE CJSS." + KeySelect + " LIKE @Keyword" : "";
            string Sql = "SELECT CJS.ID, CJSS.ID, CJS.Job_Date, CJS.Cust_No, CD.Cust_Name, CJS.Staff_No, SD.Name AS Staff_Name, CJSS.E_Start_Time, CJSS.E_End_Time FROM Cust_Job_Schedule AS CJS JOIN Cust_Job_Schedule_Supervision AS CJSS ON CJSS.Schedule_ID = CJS.ID JOIN Cust_Data AS CD ON CD.Cust_No = CJSS.Cust_No JOIN Staff_Data AS SD ON SD.Staff_No = CJSS.Staff_No " + SqlLike + " AND CJS.Job_Result_ID = 0";
            Dictionary<string, object> SqlVal = new Dictionary<string, object>()
            {
                { "Keyword", "%" + Keyword + "%" }
            };
            SqlDataReader Result = DB.Query(Sql, SqlVal);
            List<CustJobScheduleSupervision> DataList = ConvertArray(Result);
            DB.CloseReader(Result);
            DB.CloseConn();
            return DataList;
        }

        /// <summary>
        /// 資料轉換陣列
        /// </summary>
        /// <param name="Result"></param>
        /// <returns></returns>
        private List<CustJobScheduleSupervision> ConvertArray(SqlDataReader Result)
        {
            List<CustJobScheduleSupervision> DataList = new List<CustJobScheduleSupervision>();
            if (Result != null && Result.HasRows)
            {
                while (Result.Read())
                {
                    DataList.Add(new CustJobScheduleSupervision()
                    {
                        Schedule_ID = ConvertType.Int(Result[0]),
                        ScheduleSupervision_ID = ConvertType.String(Result[1]),
                        Job_Date = ConvertType.DateString(Result[2]),
                        Cust_No = ConvertType.String(Result[3]),
                        Cust_Name = ConvertType.String(Result[4]),
                        Staff_No = ConvertType.String(Result[5]),
                        Staff_Name = ConvertType.String(Result[6]),
                        E_Start_Time = ConvertType.String(Result[7]),
                        E_End_Time = ConvertType.String(Result[8])
                    });
                }
                DB.CloseReader(Result);
            }
            return DataList;
        }

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public List<ResponseJsonData> Insert(List<CustJobScheduleSupervision> Input)
        {
            List<ResponseJsonData> Result = new List<ResponseJsonData>();
            foreach (CustJobScheduleSupervision Item in Input)
            {
                int Schedule_ID = Item.Schedule_ID;
                string Cust_No = Item.Cust_No ?? "";
                string Staff_No = Item.Staff_No ?? "";
                string E_Start_Time = Item.E_Start_Time ?? "";
                string E_End_Time = Item.E_End_Time ?? "";
                string Sql = "INSERT INTO Cust_Job_Schedule_Supervision (Schedule_ID, Cust_No, Staff_No, E_Start_Time, E_End_Time) VALUES (@Schedule_ID, @Cust_No, @Staff_No, @E_Start_Time, @E_End_Time)";
                Dictionary<string, object> SqlVal = new Dictionary<string, object>()
                {
                    { "Schedule_ID", Schedule_ID },
                    { "Cust_No", Cust_No },
                    { "Staff_No", Staff_No },
                    { "E_Start_Time", E_Start_Time },
                    { "E_End_Time", E_End_Time }
                };
                int InsertResult = DB.SqlMethod(Sql, SqlVal);
                if (InsertResult > 0)
                {
                    MsgCode.Status = true;
                    MsgCode.Code = "Success";
                }
                else
                {
                    MsgCode.Message = "編號<" + Schedule_ID + ">，新增失敗。";
                    MsgCode.Code = "Error";
                }
                Result.Add(MsgCode);
            }
            return Result;
        }

        /// <summary>
        /// 刪除
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        public ResponseJsonData Delete(CustJobScheduleSupervision Input)
        {
            string ScheduleSupervision_ID = Input.ScheduleSupervision_ID ?? "";

            string Sql = "DELETE FROM Cust_Job_Schedule_Supervision WHERE ID = @ID";
            Dictionary<string, string> SqlVal = new Dictionary<string, string> { { "ID", ScheduleSupervision_ID } };
            int Result = DB.SqlMethod(Sql, SqlVal);
            if (Result > 0)
            {
                MsgCode.Status = true;
                MsgCode.Code = "Success";
            }
            else
            {
                MsgCode.Message = "編號<" + ScheduleSupervision_ID + ">，刪除失敗。";
                MsgCode.Code = "Error";
            }
            return MsgCode;
        }
    }
}