﻿using System.Collections.Generic;
using Models.Method.Share;

namespace Models.Method.Public
{
    public class JobTitleMethod : MethodBase
    {
        /// <summary>
        /// 取得職稱
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetData()
        {
            string Sql = "SELECT ID, Name FROM Public_JobTitle";
            List<Dictionary<string, object>> Data = DB.GetData(Sql);
            return Data;
        }
    }
}