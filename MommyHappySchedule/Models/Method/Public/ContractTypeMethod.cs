﻿using System.Collections.Generic;
using Models.Method.Share;

namespace Models.Method.Public
{
    public class ContractTypeMethod : MethodBase
    {
        /// <summary>
        /// 取得合約方式
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetData()
        {
            string Sql = "SELECT ID, Name FROM Public_Contract_Type";
            List<Dictionary<string, object>> Data = DB.GetData(Sql);
            return Data;
        }
    }
}