﻿using System.Collections.Generic;
using Models.Method.Share;

namespace Models.Method.Public
{
    public class JobResultMethod : MethodBase
    {
        /// <summary>
        /// 取得排班狀態
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetData()
        {
            string Sql = "SELECT ID, Name FROM Public_JobResult";
            List<Dictionary<string, object>> Data = DB.GetData(Sql);
            return Data;
        }
    }
}