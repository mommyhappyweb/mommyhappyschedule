﻿using System.Collections.Generic;
using Models.Method.Share;

namespace Models.Method.Public
{
    public class JobStatusMethod : MethodBase
    {
        /// <summary>
        /// 取得合約狀態
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetData()
        {
            string Sql = "SELECT ID, Name FROM Public_JobStatus";
            List<Dictionary<string, object>> Data = DB.GetData(Sql);
            return Data;
        }
    }
}