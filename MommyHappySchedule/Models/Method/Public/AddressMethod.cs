﻿using System.Collections.Generic;
using Models.Method.Share;

namespace Models.Method.Public
{
    public class AddressMethod : MethodBase
    {
        /// <summary>
        /// 取得城市資料
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetCityData()
        {
            string Sql = "SELECT City_Name FROM Addr_Code WHERE City_Name NOT IN ('連江縣', '釣魚臺', '南海島', '金門縣', '澎湖縣') AND Country_Name NOT IN ('釣魚臺', '東沙群島', '南沙群島') GROUP BY City_Name ORDER BY MIN(Addr_Code)";
            List<Dictionary<string, object>> Data = DB.GetData(Sql);
            return Data;
        }

        /// <summary>
        /// 取得行政區資料
        /// </summary>
        /// <param name="City_Name"></param>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetAreaData(string City_Name)
        {
            string Sql = "SELECT Country_Name FROM Addr_Code WHERE City_Name NOT IN ('連江縣', '釣魚臺', '南海島', '金門縣', '澎湖縣') AND Country_Name NOT IN ('釣魚臺', '東沙群島', '南沙群島') AND City_Name = @City_Name GROUP BY Country_Name ORDER BY MIN(Addr_Code)";
            Dictionary<string, object> SqlVal = new Dictionary<string, object>()
            {
                { "City_Name", City_Name }
            };
            List<Dictionary<string, object>> Data = DB.GetData(Sql, SqlVal);
            return Data;
        }
    }
}