﻿using System.Collections.Generic;
using Models.Method.Share;

namespace Models.Method.Public
{
    public class HolidayDateMethod : MethodBase
    {
        /// <summary>
        /// 取得國定假日資料
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetData()
        {
            string Sql = "SELECT FORMAT(H_Date, 'yyyy-MM-dd') AS H_Date, H_Memo FROM Holiday_Date ORDER BY H_Date ASC";
            List<Dictionary<string, object>> Data = DB.GetData(Sql);
            return Data;
        }
    }
}