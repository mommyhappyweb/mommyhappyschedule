﻿using System.Collections.Generic;
using Models.Method.Share;

namespace Models.Method.Public
{
    public class BankDataMethod : MethodBase
    {
        /// <summary>
        /// 取得銀行資料
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetData()
        {
            string Sql = "SELECT TRIM(Bank_Branch_Code) AS Bank_Code, TRIM(Bank_Branch_Name) AS Bank_Name FROM Bank_Data WHERE Bank_Branch_Code IN(SELECT DISTINCT(TRIM(Bank_Code) + '0000') AS Bank_Code FROM Bank_Data WHERE LEN(Bank_Code) < 4)";
            List<Dictionary<string, object>> Data = DB.GetData(Sql);
            return Data;
        }
    }
}