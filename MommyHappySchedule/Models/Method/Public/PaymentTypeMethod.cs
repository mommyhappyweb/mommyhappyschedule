﻿using System.Collections.Generic;
using Models.Method.Share;

namespace Models.Method.Public
{
    public class PaymentTypeMethod : MethodBase
    {
        /// <summary>
        /// 取得付款方式
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetData()
        {
            string Sql = "SELECT ID, Name FROM Public_Payment_Type";
            List<Dictionary<string, object>> Data = DB.GetData(Sql);
            return Data;
        }
    }
}