﻿using System.Collections.Generic;
using Models.Method.Share;

namespace Models.Method.Staff
{
    public class StaffJobDataMethod : MethodBase
    {
        /// <summary>
        /// 取得部門專員資料
        /// </summary>
        /// <param name="Branch_ID"></param>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetBranchStaffData(string Branch_ID)
        {
            string Sql = "SELECT Staff_No, Staff_Name FROM Staff_Job_Data WHERE Job_Title_ID IN (5, 6, 16, 17, 18, 19) AND Job_Status_ID = 2 AND Branch_ID = @Branch_ID ORDER BY Staff_No ASC";
            Dictionary<string, object> SqlVal = new Dictionary<string, object>()
            {
                { "Branch_ID", Branch_ID }
            };
            List<Dictionary<string, object>> Data = DB.GetData(Sql, SqlVal);
            return Data;
        }
    }
}