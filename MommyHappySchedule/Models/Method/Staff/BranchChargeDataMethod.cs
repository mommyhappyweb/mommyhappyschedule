﻿using System.Collections.Generic;
using Models.Method.Share;

namespace Models.Method.Staff
{
    public class BranchChargeDataMethod : MethodBase
    {
        /// <summary>
        /// 取得各駐點服務價格
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetData()
        {
            string Sql = "SELECT * FROM Branch_Charge_Data ORDER BY Branch_ID ASC";
            List<Dictionary<string, object>> Data = DB.GetData(Sql);
            return Data;
        }
    }
}