﻿using System.Collections.Generic;
using Models.Method.Share;

namespace Models.Method.Staff
{
    public class BranchMethod : MethodBase
    {
        /// <summary>
        /// 取得部門資料
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetData()
        {
            string Sql = "SELECT ID, Branch_Name FROM Branch_Data WHERE ID != 0";
            List<Dictionary<string, object>> Data = DB.GetData(Sql);
            return Data;
        }
    }
}