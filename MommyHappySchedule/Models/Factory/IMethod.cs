﻿using System.Collections.Generic;

namespace Models.Factory
{
    /// <summary>
    /// 方法介面
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IMethod<T>
    {
        /// <summary>
        /// 取得資料
        /// </summary>
        /// <param name="Input"></param>
        /// <returns></returns>
        List<T> GetData(T Input);

        /// <summary>
        /// 取得總筆數
        /// </summary>
        /// <param name="KeySelect">關鍵字選項</param>
        /// <param name="Keyword">關鍵字</param>
        /// <returns></returns>
        uint GetCount(string KeySelect = "", string Keyword = "");

        /// <summary>
        /// 取得詳細資訊
        /// </summary>
        /// <param name="Did">查詢編號</param>
        /// <returns></returns>
        List<T> GetDetailed(string Did);

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="Input">存取參數</param>
        /// <returns></returns>
        Dictionary<string, object> Update(T Input);

        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="Input">存取參數</param>
        /// <returns></returns>
        Dictionary<string, object> Insert(T Input);

        /// <summary>
        /// 刪除
        /// </summary>
        /// <param name="ID">刪除編號</param>
        /// <returns></returns>
        Dictionary<string, object> Delete(string ID);
    }
}