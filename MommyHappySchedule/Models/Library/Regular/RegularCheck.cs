﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Models.Library.Regular
{
    /// <summary>
    /// 正規表達式格式檢查
    /// </summary>
    public class RegularCheck
    {
        /// <summary>
        /// 正規表達式字典
        /// </summary>
        private readonly RegularDictionary RegularDictionary = new RegularDictionary();

        /// <summary>
        /// 格式驗證
        /// </summary>
        /// <param name="Mode">字典模式</param>
        /// <param name="Regular">表達式</param>
        /// <param name="Value">檢查值</param>
        /// <returns></returns>
        public bool Check(string Mode, string Regular, string Value)
        {
            string Pattern = GetRegularMode(Mode, Regular);
            Regex Regexs = new Regex(Pattern);
            MatchCollection Matche = Regexs.Matches(Value);
            bool Result = Convert.ToBoolean(Matche.Count);
            return Result;
        }

        /// <summary>
        /// 取得檢查模式
        /// </summary>
        /// <param name="Mode"></param>
        /// <param name="Regular"></param>
        /// <returns></returns>
        private string GetRegularMode(string Mode, string Regular)
        {
            Dictionary<string, string> Data = GetRegularDictionary(Mode);
            string Result = "";
            if (Data != null)
            {
                foreach (KeyValuePair<string, string> Item in Data)
                {
                    if (Item.Key == Regular)
                    {
                        Result = "^" + Item.Value;
                        break;
                    }
                }
            }
            return Result;
        }

        /// <summary>
        /// 取得正規表達式字典
        /// </summary>
        /// <param name="Mode"></param>
        /// <returns></returns>
        Dictionary<string, string> GetRegularDictionary(string Mode)
        {
            Dictionary<string, string> Data = new Dictionary<string, string>();
            switch (Mode)
            {
                case "Bank":
                    Data = RegularDictionary.BankRegular();
                    break;
                case "Phone":
                    Data = RegularDictionary.PhoneRegular();
                    break;
                case "Identity":
                    Data = RegularDictionary.IdentityRegular();
                    break;
                case "Date":
                    Data = RegularDictionary.DateRegular();
                    break;
                case "Mail":
                    Data = RegularDictionary.MailRegular();
                    break;
                default:
                    break;
            }
            return Data;
        }
    }
}