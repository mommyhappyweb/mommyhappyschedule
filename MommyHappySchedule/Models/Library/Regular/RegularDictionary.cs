﻿using System.Collections.Generic;

namespace Models.Library.Regular
{
    /// <summary>
    /// 正規表達式字典
    /// </summary>
    public class RegularDictionary
    {
        /// <summary>
        /// 銀行表達式
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> BankRegular()
        {
            Dictionary<string, string> DataList = new Dictionary<string, string>()
            {
                { "BankID", @"\d{3}" },
                { "BankClassID", @"\d{7}" },
                { "BankAccount", @"\d{8, 14}" }
            };
            return DataList;
        }

        /// <summary>
        /// 電話表達式
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> PhoneRegular()
        {
            Dictionary<string, string> DataList = new Dictionary<string, string>()
            {
                { "MobilePhone", @"09\d{8}" },
                { "HomePhone", @"\(\d{2,3}\)\d{7,8}" }
            };
            return DataList;
        }

        /// <summary>
        /// 身分證表達式
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> IdentityRegular()
        {
            Dictionary<string, string> DataList = new Dictionary<string, string>()
            {
                { "ID", @"[a-zA-Z]{1}[0-9]{9}" }
            };
            return DataList;
        }

        /// <summary>
        /// 日期表達式
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> DateRegular()
        {
            Dictionary<string, string> DataList = new Dictionary<string, string>() {
                { "Y", @"[0-9]{4}" },
                { "Ym", @"[0-9]{4}-[0-9]{2}" },
                { "Ymd", @"[0-9]{4}-[0-9]{2}-[0-9]{2}" },
                { "YmdHms", @"[0-9]{4}-[0-9]{2}-[0-9]{2}\s|T[0-9]{2}:[0-9]{2}:[0-9]{2}" }
            };
            return DataList;
        }

        /// <summary>
        /// 信箱表達式
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> MailRegular()
        {
            Dictionary<string, string> DataList = new Dictionary<string, string>() {
                { "Mail", @"[a-zA-Z0-9]{1,64}@([a-zA-Z]{1, 100}).[a-z]{3,4}" }
            };
            return DataList;
        }
    }
}