﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Models.Library
{
    /// <summary>
    /// 日期方法
    /// </summary>
    public class Date
    {
        /// <summary>
        /// 表達式庫
        /// </summary>
        private readonly Regular.RegularDictionary RegularDictionary = new Regular.RegularDictionary();

        /// <summary>
        /// 日期格式驗證
        /// </summary>
        /// <param name="DateVal"></param>
        /// <param name="Mode">驗證模式</param>
        /// <returns></returns>
        public bool DateCheck(string DateVal, string Mode = "Ymd")
        {
            string Pattern = DateCheckMode(Mode);
            Regex Regexs = new Regex(Pattern);
            MatchCollection Matche = Regexs.Matches(DateVal);
            bool Result = Convert.ToBoolean(Matche.Count);
            return Result;
        }

        /// <summary>
        /// 日期表達式
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> DatePattern()
        {
            return RegularDictionary.DateRegular();
        }

        /// <summary>
        /// 日期檢查模式
        /// </summary>
        /// <param name="Mode"></param>
        /// <returns></returns>
        private string DateCheckMode(string Mode)
        {
            Dictionary<string, string> Data = DatePattern();
            string Result = "";
            foreach (KeyValuePair<string, string> Item in Data)
            {
                if (Item.Key == Mode)
                {
                    Result = "^" + Item.Value;
                }
            }
            return Result;
        }

        /// <summary>
        /// 日期轉換yyyy-MM-dd
        /// </summary>
        /// <param name="DateVal"></param>
        /// <returns></returns>
        public string DateYmd(object DateVal)
        {
            return (DateVal == DBNull.Value) ? "" : DateTime.Parse(DateVal.ToString()).ToString("yyyy-MM-dd");
        }

        /// <summary>
        /// 日期轉換yyyy-MM-dd hh-mm-ss
        /// </summary>
        /// <param name="DateVal"></param>
        /// <returns></returns>
        public string DateYmdHms(object DateVal)
        {
            return (DateVal == DBNull.Value) ? "" : DateTime.Parse(DateVal.ToString()).ToString("yyyy-MM-dd hh:mm:ss");
        }

        /// <summary>
        /// 取得自訂日期yyyy-MM-dd
        /// </summary>
        /// <param name="DateTime"></param>
        /// <param name="YY"></param>
        /// <param name="MM"></param>
        /// <param name="DD"></param>
        /// <returns></returns>
        public string GetTodayDate(DateTime DateTime, int YY = 0, int MM = 0, int DD = 0)
        {
            DateTime = DateTime.AddYears(YY);
            DateTime = DateTime.AddMonths(MM);
            DateTime = DateTime.AddDays(DD);
            return DateYmd(DateTime.ToString());
        }

        /// <summary>
        /// 取得今日日期yyyy-MM-dd
        /// </summary>
        /// <param name="Year"></param>
        /// <param name="Month"></param>
        /// <param name="Day"></param>
        /// <returns></returns>
        public string GetTodayDate(int YY = 0, int MM = 0, int DD = 0)
        {
            DateTime Today = DateTime.Now;
            Today = Today.AddYears(YY);
            Today = Today.AddMonths(MM);
            Today = Today.AddDays(DD);
            return DateYmd(Today.ToString());
        }

        /// <summary>
        /// 取得自訂日期時間yyyy-MM-dd hh-mm-ss
        /// </summary>
        /// <param name="DateTime"></param>
        /// <param name="YY"></param>
        /// <param name="MM"></param>
        /// <param name="DD"></param>
        /// <param name="H"></param>
        /// <param name="M"></param>
        /// <param name="S"></param>
        /// <returns></returns>
        public string GetTodayDateTime(DateTime DateTime, int YY = 0, int MM = 0, int DD = 0, int H = 0, int M = 0, int S = 0)
        {
            DateTime = DateTime.AddYears(YY);
            DateTime = DateTime.AddMonths(MM);
            DateTime = DateTime.AddDays(DD);
            DateTime = DateTime.AddHours(H);
            DateTime = DateTime.AddMinutes(M);
            DateTime = DateTime.AddSeconds(S);
            return DateYmdHms(DateTime.ToString());
        }

        /// <summary>
        /// 取得今日目前日期時間yyyy-MM-dd hh-mm-ss
        /// </summary>
        /// <param name="YY"></param>
        /// <param name="MM"></param>
        /// <param name="DD"></param>
        /// <param name="H"></param>
        /// <param name="M"></param>
        /// <param name="S"></param>
        /// <returns></returns>
        public string GetTodayDateTime(int YY = 0, int MM = 0, int DD = 0, int H = 0, int M = 0, int S = 0)
        {
            DateTime Today = DateTime.Now;
            Today = Today.AddYears(YY);
            Today = Today.AddMonths(MM);
            Today = Today.AddDays(DD);
            Today = Today.AddHours(H);
            Today = Today.AddMinutes(M);
            Today = Today.AddSeconds(S);
            return DateYmdHms(Today.ToString());
        }
    }
}