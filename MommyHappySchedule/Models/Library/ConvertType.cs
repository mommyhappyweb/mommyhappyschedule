﻿using System;
using System.Globalization;

namespace Models.Library
{
    /// <summary>
    /// 轉換型態
    /// </summary>
    public class ConvertType
    {
        /// <summary>
        /// 轉換不帶正負號的 64 位元整數
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public ulong Ulong(object Value)
        {
            ulong Result = (Value == null || Value == DBNull.Value || Value.ToString() == "") ? Convert.ToUInt64(0) : Convert.ToUInt64(Value);
            return Result;
        }

        /// <summary>
        /// 轉換帶正負號的 64 位元整數
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public long Long(object Value)
        {
            long Result = (Value == null || Value == DBNull.Value || Value.ToString() == "") ? Convert.ToInt64(0) : Convert.ToInt64(Value);
            return Result;
        }

        /// <summary>
        /// 轉換不帶正負號的 32 位元整數
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public uint Uint(object Value)
        {
            uint Result = (Value == null || Value == DBNull.Value || Value.ToString() == "") ? Convert.ToUInt32(0) : Convert.ToUInt32(Value);
            return Result;
        }

        /// <summary>
        /// 轉換帶正負號的 32 位元整數
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public int Int(object Value)
        {
            int Result = (Value == null || Value == DBNull.Value || Value.ToString() == "") ? Convert.ToInt32(0) : Convert.ToInt32(Value);
            return Result;
        }

        /// <summary>
        /// 轉換帶正負號的 32 位元整數
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public ushort Ushort(object Value)
        {
            ushort Result = (Value == null || Value == DBNull.Value || Value.ToString() == "") ? Convert.ToUInt16(0) : Convert.ToUInt16(Value);
            return Result;
        }

        /// <summary>
        /// 轉換帶正負號的 16 位元整數
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public short Short(object Value)
        {
            short Result = (Value == null || Value == DBNull.Value || Value.ToString() == "") ? Convert.ToInt16(0) : Convert.ToInt16(Value);
            return Result;
        }

        /// <summary>
        /// 轉換帶正負號的 16 位元整數
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public char Char(object Value)
        {
            char Result = (Value == null || Value == DBNull.Value || Value.ToString() == "") ? Convert.ToChar(0) : Convert.ToChar(Value);
            return Result;
        }

        /// <summary>
        /// 轉換不帶正負號的 8 位元整數
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public byte Byte(object Value)
        {
            byte Result = (Value == null || Value == DBNull.Value || Value.ToString() == "") ? Convert.ToByte(0) : Convert.ToByte(Value);
            return Result;
        }

        /// <summary>
        /// 轉換帶正負號的 8 位元整數
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public sbyte Sbyte(object Value)
        {
            sbyte Result = (Value == null || Value == DBNull.Value || Value.ToString() == "") ? Convert.ToSByte(0) : Convert.ToSByte(Value);
            return Result;
        }

        /// <summary>
        /// 轉換單精度浮點數
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public float Float(object Value)
        {
            float Result = (Value == null || Value == DBNull.Value || Value.ToString() == "") ? 0 : float.Parse(String(Value).Trim(), CultureInfo.InvariantCulture.NumberFormat);
            return Result;
        }

        /// <summary>
        /// 轉換雙精度浮點數
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public double Double(object Value)
        {
            double Result = (Value == null || Value == DBNull.Value || Value.ToString() == "") ? 0 : Convert.ToDouble(Value);
            return Result;
        }

        /// <summary>
        /// 轉換十位數進位
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public decimal Decimal(object Value)
        {
            decimal Result = (Value == null || Value == DBNull.Value || Value.ToString() == "") ? 0 : Convert.ToDecimal(Value);
            return Result;
        }

        /// <summary>
        /// 轉換布林
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public bool Boolean(object Value)
        {
            bool Result = (Value == null || Value == DBNull.Value || Value.ToString() == "") ? false : Convert.ToBoolean(Value);
            return Result;
        }

        /// <summary>
        /// 轉換日期自定義
        /// 預設格式：yyyy-MM-dd 格式：yyyy-MM-dd hh:mm:ss
        /// </summary>
        /// <param name="Value"></param>
        /// <param name="Format">日期格式</param>
        /// <returns></returns>
        public string DateString(object Value, string Format = "yyyy-MM-dd")
        {
            string Result = (Value == null || Value == DBNull.Value || Value.ToString() == "") ? "" : DateTime.Parse(String(Value)).ToString(Format);
            return Result;
        }

        /// <summary>
        /// 轉換日期時間 yyyy-MM-dd hh:mm:ss
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public string DateTimeString(object Value)
        {
            string Result = (Value == null || Value == DBNull.Value || Value.ToString() == "") ? "" : DateTime.Parse(String(Value)).ToString("yyyy-MM-dd hh:mm:ss");
            return Result;
        }

        /// <summary>
        /// 轉換日期
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public DateTime DateTimes(object Value)
        {
            DateTime Result = (Value == null || Value == DBNull.Value || Value.ToString() == "") ? DateTime.Now : Convert.ToDateTime(Value);
            return Result;
        }

        /// <summary>
        /// 轉換字串
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public string String(object Value)
        {
            string Result = (Value == null || Value == DBNull.Value || Value.ToString() == "") ? "" : Convert.ToString(Value).Trim();
            return Result;
        }
    }
}