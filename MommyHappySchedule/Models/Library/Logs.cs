﻿using System;
using System.Web;

namespace Models.Library
{
    /// <summary>
    /// 日誌方法
    /// </summary>
    public class Logs
    {
        /// <summary>
        /// 檔案日誌
        /// </summary>
        /// <param name="Path">路徑</param>
        /// <param name="Message">訊息</param>
        public void FileLogs(string Message = null)
        {
            string Path = HttpContext.Current.Server.MapPath("/Logs/");
            if (Path != "")
            {
                Files Files = new Files();
                DateTime LocalDate = DateTime.Now;
                Files.FileWriting(Path + "Logs.log", "[" + LocalDate + "] 錯誤日誌:" + Message);
            }
        }
        public void ScheduleLogs(string Filename,string Message = null)
        {
            string Path = HttpContext.Current.Server.MapPath("/Logs/");
            if (Path != "")
            {
                Files Files = new Files();
                DateTime LocalDate = DateTime.Now;
                Files.FileWriting(Path + Filename ,  LocalDate + "|" + Message);
            }
        }
        
    }
}