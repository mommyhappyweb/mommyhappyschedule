﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Models.Library.DataBase
{
    /// <summary>
    /// 資料庫方法
    /// </summary>
    public class DB : Logs
    {
        /// <summary>錯誤訊息</summary>
        protected string ErrorMsg;

        /// <summary>錯誤堆疊</summary>
        protected string ErrorStacktrack;

        /// <summary>是否為測試機</summary>
        private readonly static string DBMode = ConfigurationManager.AppSettings["DBMode"];

        public readonly string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];

        private SqlConnection Conn;

        /// 共用SQL物件
        private SqlCommand SqlRun;

        /// <summary>
        /// SQL連線
        /// </summary>
        /// <returns></returns>
        public SqlConnection GetConn(string ConnStr = "")
        {
            if (ConnStr == "")
            {
                ConnStr = ConnString;
            }

            Conn = new SqlConnection(ConnStr);
            try
            {
                if (Conn.State == ConnectionState.Closed)
                {
                    Conn.Open();
                }
            }
            catch (SqlException Ex)
            {
                CloseConn();
                RecException(Ex);
            }
            return Conn;
        }

        /// <summary>
        /// 關閉連線釋放資源
        /// </summary>
        /// <param name="Conn"></param>
        public void CloseConn()
        {
            if (Conn != null)
            {
                Conn.Dispose();
                Conn.Close();
            }
        }

        /// <summary>
        /// 關閉資料讀取
        /// </summary>
        /// <param name="Reader"></param>
        public void CloseReader(SqlDataReader Reader)
        {
            if (Reader != null)
            {
                Reader.Close();
            }
        }

        /// <summary>
        /// 關閉命令所有資源
        /// </summary>
        /// <param name="Command"></param>
        public void CloseCommand(SqlCommand Command)
        {
            if (Command != null)
            {
                Command.Dispose();
            }
        }

        /// <summary>
        /// 錯誤訊息
        /// </summary>
        /// <param name="Ex"></param>
        public void RecException(SqlException Ex)
        {
            ErrorMsg = Ex.Message;
            ErrorStacktrack = Ex.StackTrace;
            FileLogs(ErrorMsg + ErrorStacktrack);
        }

        /// <summary>
        /// 錯誤訊息
        /// </summary>
        /// <param name="Ex"></param>
        public void RecException(string ExStr)
        {
            FileLogs(ExStr);
        }

        /// <summary>
        /// 錯誤訊息
        /// </summary>
        /// <param name="Ex"></param>
        public void RecException(InvalidOperationException Ex)
        {
            ErrorMsg = Ex.Message;
            ErrorStacktrack = Ex.StackTrace;
            FileLogs(ErrorMsg + ErrorStacktrack);
        }

        /// <summary>
        /// SQL查詢
        /// </summary>
        /// <param name="Sql">SQL字串</param>
        /// <param name="ConnString">連線字串</param>
        /// <returns name="SqlDataReader"></returns>
        public SqlDataReader Query(string Sql, string ConnString = "")
        {
            SqlConnection Conn = GetConn(ConnString);
            SqlDataReader Reader = null;
            try
            {
                SqlRun = new SqlCommand(Sql, Conn);
                Reader = SqlRun.ExecuteReader();
            }
            catch (InvalidOperationException Ex)
            {
                CloseConn();
                RecException(Ex);
            }
            CloseCommand(SqlRun);
            return Reader;
        }

        /// <summary>
        /// SQL查詢
        /// </summary>
        /// <param name="Sql">SQL查詢語句</param>
        /// <param name="SqlVal">SQL參數值</param>
        /// <param name="ConnString">連線字串</param>
        /// <returns name="SqlDataReader"></returns>
        public SqlDataReader Query(string Sql, Dictionary<string, string> SqlVal, string ConnString = "")
        {
            SqlConnection Conn = GetConn(ConnString);
            SqlDataReader Reader = null;
            try
            {
                SqlRun = new SqlCommand(Sql, Conn);
                foreach (KeyValuePair<string, string> Item in SqlVal)
                {
                    SqlRun.Parameters.AddWithValue(Item.Key, Item.Value);
                }
                Reader = SqlRun.ExecuteReader();
            }
            catch (InvalidOperationException Ex)
            {
                CloseConn();
                RecException(Ex);
            }
            CloseCommand(SqlRun);
            return Reader;
        }

        /// <summary>
        /// SQL查詢
        /// </summary>
        /// <param name="Sql">SQL查詢語句</param>
        /// <param name="SqlVal">SQL參數值</param>
        /// <param name="ConnString">連線字串</param>
        /// <returns name="SqlDataReader"></returns>
        public SqlDataReader Query(string Sql, Dictionary<string, int> SqlVal, string ConnString = "")
        {
            SqlConnection Conn = GetConn(ConnString);
            SqlDataReader Reader = null;
            try
            {
                SqlRun = new SqlCommand(Sql, Conn);
                foreach (KeyValuePair<string, int> Item in SqlVal)
                {
                    SqlRun.Parameters.AddWithValue(Item.Key, Item.Value);
                }
                Reader = SqlRun.ExecuteReader();
            }
            catch (InvalidOperationException Ex)
            {
                CloseConn();
                RecException(Ex);
            }
            CloseCommand(SqlRun);
            return Reader;
        }

        /// <summary>
        /// SQL查詢
        /// </summary>
        /// <param name="Sql">SQL查詢語句</param>
        /// <param name="SqlVal">SQL參數值</param>
        /// <param name="ConnString">連線字串</param>
        /// <returns name="SqlDataReader"></returns>
        public SqlDataReader Query(string Sql, Dictionary<string, object> SqlVal, string ConnString = "")
        {
            SqlConnection Conn = GetConn(ConnString);
            SqlDataReader Reader = null;
            try
            {
                SqlRun = new SqlCommand(Sql, Conn);
                foreach (KeyValuePair<string, object> Item in SqlVal)
                {
                    SqlRun.Parameters.AddWithValue(Item.Key, Item.Value ?? DBNull.Value);
                }
                Reader = SqlRun.ExecuteReader();
            }
            catch (InvalidOperationException Ex)
            {
                CloseConn();
                RecException(Ex);
            }
            CloseCommand(SqlRun);
            return Reader;
        }

        /// <summary>
        /// 取得資料
        /// </summary>
        /// <param name="Sql"></param>
        /// <param name="SqlVal"></param>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetData(string Sql, Dictionary<string, object> SqlVal = null)
        {
            List<Dictionary<string, object>> Data = new List<Dictionary<string, object>>();
            using (SqlConnection Conn = GetConn())
            {
                using (SqlCommand Command = new SqlCommand(Sql, Conn))
                {
                    if (SqlVal != null)
                    {
                        foreach (KeyValuePair<string, object> Item in SqlVal)
                        {
                            Command.Parameters.AddWithValue("@" + Item.Key, Item.Value);
                        }
                    }
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader != null && Reader.HasRows)
                        {
                            while (Reader.Read())
                            {
                                Dictionary<string, object> NewData = new Dictionary<string, object>();
                                for (int i = 0; i < Reader.FieldCount; i++)
                                {
                                    NewData.Add(Reader.GetName(i), Reader[i].ToString().Trim());
                                }
                                Data.Add(NewData);
                            }
                        }
                    }
                }
            }
            return Data;
        }

        /// <summary>
        /// SQL新增、修改、刪除
        /// </summary>
        /// <param name="Sql">SQL查詢語句</param>
        /// <param name="SqlVal">SQL參數值</param>
        /// <param name="ConnString">連線字串</param>
        /// <returns name="SqlDataReader"></returns>
        public int SqlMethod(string Sql, Dictionary<string, object> SqlVal, string ConnString = "")
        {
            SqlConnection Conn = GetConn(ConnString);
            int Now = 0;
            try
            {
                SqlRun = new SqlCommand(Sql, Conn);
                foreach (KeyValuePair<string, object> Item in SqlVal)
                {
                    SqlRun.Parameters.AddWithValue(Item.Key, Item.Value ?? DBNull.Value);
                }
                Now = SqlRun.ExecuteNonQuery();
                CloseCommand(SqlRun);
            }
            catch (SqlException Ex)
            {
                RecException(Ex);
            }
            finally
            {
                CloseConn();
            }
            return Now;
        }

        /// <summary>
        /// SQL新增、修改、刪除
        /// </summary>
        /// <param name="Sql">SQL查詢語句</param>
        /// <param name="SqlVal">SQL參數值</param>
        /// <param name="ConnString">連線字串</param>
        /// <returns name="SqlDataReader"></returns>
        public int SqlMethod(string Sql, Dictionary<string, string> SqlVal, string ConnString = "")
        {
            SqlConnection Conn = GetConn(ConnString);
            int Now = 0;
            try
            {
                SqlRun = new SqlCommand(Sql, Conn);
                foreach (KeyValuePair<string, string> Item in SqlVal)
                {
                    SqlRun.Parameters.AddWithValue(Item.Key, (object)Item.Value ?? DBNull.Value);
                }
                Now = SqlRun.ExecuteNonQuery();
                CloseCommand(SqlRun);
            }
            catch (SqlException Ex)
            {
                RecException(Ex);
            }
            finally
            {
                CloseConn();
            }
            return Now;
        }

        /// <summary>
        /// SQL新增、修改、刪除
        /// </summary>
        /// <param name="Sql"></param>
        /// <param name="SqlVal"></param>
        /// <returns></returns>
        public int SqlMethod(string Sql, Dictionary<string, object> SqlVal)
        {
            int Result = 0;
            using (SqlConnection Conn = GetConn())
            {
                Result = SqlMethodTransaction(Sql, SqlVal);
            }
            return Result;
        }

        /// <summary>
        /// SQL新增、修改、刪除交易模式
        /// </summary>
        /// <param name="Sql"></param>
        /// <param name="SqlVal"></param>
        /// <returns></returns>
        public int SqlMethodTransaction(string Sql, Dictionary<string, object> SqlVal)
        {
            int Result = 0;
            try
            {
                using (SqlCommand Command = new SqlCommand(Sql, Conn))
                {
                    foreach (KeyValuePair<string, object> Item in SqlVal)
                    {
                        Command.Parameters.AddWithValue("@" + Item.Key, Item.Value ?? DBNull.Value);
                    }
                    Result = Command.ExecuteNonQuery();
                }
            }
            catch (SqlException Ex)
            {
                //string exStr = Newtonsoft.Json.JsonConvert.SerializeObject(Ex);
                //string dictStr = Newtonsoft.Json.JsonConvert.SerializeObject(SqlVal);
                //string jsonStr = @"{EX:{0},Sql:""{1}"",Dict:{2}}";
                //RecException(string.Format(jsonStr, exStr, Sql, dictStr));
                RecException(Ex);
            }
            return Result;
        }
    }
}