﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models.Library.DataBase.List
{
    /// <summary>
    /// SQL替代參數
    /// </summary>
    public class SqlValue
    {
        /// <summary>
        /// 主鍵
        /// </summary>
        public string Key { set; get; }

        /// <summary>
        /// 參數
        /// </summary>
        public object Value { set; get; }
    }
}