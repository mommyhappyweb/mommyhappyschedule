﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Models.Library.Cyphertext
{
    /// <summary>
    /// 基礎MD5加密
    /// </summary>
    public class BaseMD5
    {
        /// <summary>
        /// 日誌
        /// </summary>
        private readonly Logs Logs = new Logs();

        /// <summary>
        /// 預設加密參數
        /// </summary>
        private const string DefaultHash = "MommyHappy";

        /// <summary>
        /// MD5加密
        /// </summary>
        /// <param name="Content">加密內容</param>
        /// <returns></returns>
        public string Md5Encrypt(string Content)
        {
            string Result = "";
            try
            {
                byte[] Original = Encoding.Default.GetBytes(Content.Trim());
                MD5 ND5Create = MD5.Create(); 
                byte[] Change = ND5Create.ComputeHash(Original);
                Result = Convert.ToBase64String(Change);
            }
            catch (Exception Ex)
            {
                Logs.FileLogs(Ex.ToString());
            }
            return Result;
        }

        /// <summary>
        /// MD5鹽加密
        /// </summary>
        /// <param name="Content"></param>
        /// <param name="Salt"></param>
        /// <returns></returns>
        public string Md5SaltedHash(string Content, string Salt = DefaultHash)
        {
            string Result = "";
            try
            {
                string Salted = Content.Trim();
                if (Salted.Length == 0) Content = DefaultHash;
                byte[] Original = Encoding.Default.GetBytes(Content);
                byte[] SaltValue = Encoding.Default.GetBytes(Salted);
                byte[] ToSalt = new byte[Original.Length + SaltValue.Length];
                Original.CopyTo(ToSalt, 0);
                SaltValue.CopyTo(ToSalt, Original.Length);
                MD5 MD5Create = MD5.Create();
                byte[] SaltPWD = MD5Create.ComputeHash(ToSalt);
                byte[] PWD = new byte[SaltPWD.Length + SaltValue.Length];
                SaltPWD.CopyTo(PWD, 0);
                SaltValue.CopyTo(PWD, SaltPWD.Length);
                Result = Convert.ToBase64String(PWD);
            }
            catch(Exception Ex)
            {
                Logs.FileLogs(Ex.ToString());
            }
            return Result;
        }
    }
}