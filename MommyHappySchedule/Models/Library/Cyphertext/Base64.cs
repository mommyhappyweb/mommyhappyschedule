﻿using System;
using System.Text;

namespace Models.Library.Cyphertext
{
    public class Base64
    {
        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="Str"></param>
        /// <returns></returns>
        public string Encryption(string Str)
        {
            byte[] Bytes = Encoding.UTF8.GetBytes(Str);

            return Convert.ToBase64String(Bytes);
        }

        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="Str"></param>
        /// <returns></returns>
        public string Decrypt(string Str)
        {
            byte[] FromBase = Convert.FromBase64String(Str);
            return Encoding.UTF8.GetString(FromBase);
        }
    }
}