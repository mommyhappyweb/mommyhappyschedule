﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Models.Library
{
    //for Google Geometry
    public class AddressComponent
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public string[] types { get; set; }
    }

    public class Location
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Northeast
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Southwest
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Viewport
    {
        public Northeast northeast { get; set; }
        public Southwest southwest { get; set; }
    }

    public class Geometry
    {
        public Location location { get; set; }
        public string location_type { get; set; }
        public Viewport viewport { get; set; }
    }

    public class Result
    {
        public AddressComponent[] address_components { get; set; }
        public string formatted_address { get; set; }
        public Geometry geometry { get; set; }
        public string place_id { get; set; }
        public string[] types { get; set; }
    }

    public class Address
    {
        public Result[] results { get; set; }
        public string status { get; set; }
    }
    /// <summary>
    /// ShareFunction 的摘要描述
    /// </summary>
    public class ShareFunction
    {
        public Boolean IsDate(string DateString)
        {
            try
            {
                Convert.ToDateTime(DateString);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public Boolean IsNumeric(string DateString)
        {
            try
            {
                Convert.ToInt32(DateString);
                return true;
            }
            catch
            {
                try
                {
                    Convert.ToDouble(DateString);
                    return true;
                }
                catch
                {
                    return false;
                }

            }
        }
        public string NSubString(string OrString, int NStart , int SLength)
        {
            if (NStart >= OrString.Length)
                return OrString;
            else if (OrString.Length < NStart + SLength)
                return OrString.Substring(NStart, OrString.Length - NStart);
            else
                return OrString.Substring(NStart, SLength);
                
        }
    }
}