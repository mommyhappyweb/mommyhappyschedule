﻿using System;
using System.IO;
using System.Web;
using System.Collections.Generic;
using System.Net;
using System.Configuration;

namespace Models.Library
{
    /// <summary>
    /// 檔案方法
    /// </summary>
    public class Files
    {
        /// <summary>
        /// 日誌方法
        /// </summary>
        private readonly Logs Logs = new Logs();

        /// <summary>
        /// 圖片FTP伺服器
        /// </summary>
        private readonly string ImageFTPServer = ConfigurationManager.AppSettings["ImageFTPServer"];

        /// <summary>
        /// 圖片FTP帳號
        /// </summary>
        private readonly string ImageFTPAccount = ConfigurationManager.AppSettings["ImageFTPAccount"];

        /// <summary>
        /// 圖片FTP密碼
        /// </summary>
        private readonly string ImageFTPPassword = ConfigurationManager.AppSettings["ImageFTPPassword"];

        /// <summary>
        /// 訊息代碼
        /// </summary>
        protected Dictionary<string, object> MsgCode = new Dictionary<string, object>() {
            { "Status", false },
            { "Code", "Upload00"},
            { "FileName", "" }
        };

        /// <summary>
        /// 資料夾列表
        /// </summary>
        /// <param name="Path">路徑</param>
        /// <returns></returns>
        public Dictionary<int, string> FolderList(string Path)
        {
            DirectoryInfo FileInfo = new DirectoryInfo(Path);
            Dictionary<int, string> FileName = new Dictionary<int, string>();
            int Key = 0;
            foreach (var Item in FileInfo.GetFiles())
            {
                if (Item.Name != null)
                {
                    FileName.Add(Key, Item.Name);
                    Key++;
                }
            }
            return FileName;
        }

        /// <summary>
        /// 建立資料夾
        /// </summary>
        /// <param name="Paths"></param>
        /// <returns></returns>
        public void FolderAdd(string Paths)
        {
            try
            {
                Directory.CreateDirectory(Paths);
            }
            catch (DirectoryNotFoundException Ex)
            {
                Logs.FileLogs(Ex.ToString());
            }
        }

        /// <summary>
        /// 檔案寫入
        /// </summary>
        /// <param name="Path">路徑</param>
        /// <param name="Message">訊息</param>
        /// <returns></returns>
        public bool FileWriting(string Path, string Message = null)
        {
            try
            {
                if (File.Exists(Path))
                {
                    using (StreamWriter StreamWriter = new StreamWriter(Path, true))
                    {
                        StreamWriter.WriteLine(Message);
                        StreamWriter.Flush();
                        StreamWriter.Close();
                        StreamWriter.Dispose();
                        return true;
                    }
                }
            }
            catch (IOException Ex)
            {
                Logs.FileLogs(Ex.ToString());
            }
            return false;
        }

        /// <summary>
        /// 檔案讀取
        /// </summary>
        /// <param name="FilePathName">檔案路徑名稱</param>
        /// <returns></returns>
        public string FileRead(string FilePathName)
        {
            try
            {
                if (File.Exists(FilePathName))
                {
                    StreamReader StreamReader = new StreamReader(FilePathName);
                    string Result = StreamReader.ReadToEnd();
                    StreamReader.Close();
                    StreamReader.Dispose();
                    return Result;
                }
            }
            catch (IOException Ex)
            {
                Logs.FileLogs(Ex.ToString());
            }
            return null;
        }

        /// <summary>
        /// 檢查副檔名
        /// </summary>
        /// <param name="FileType"></param>
        /// <param name="FileDeputy"></param>
        /// <returns></returns>
        public bool FileDeputyCheck(string FileType, string[] FileDeputy)
        {
            bool Result = false;
            foreach (string Item in FileDeputy)
            {
                string[] FileTypeSplit = FileType.Split('/');
                if (Item == FileTypeSplit[1])
                {
                    Result = true;
                    break;
                }
            }
            return Result;
        }

        /// <summary>
        /// 檢查副檔名
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="FileDeputy"></param>
        /// <returns></returns>
        public bool FileNameDeputyCheck(string FileName, string[] FileDeputy)
        {
            bool Result = false;
            foreach (string Item in FileDeputy)
            {
                string[] FileTypeSplit = FileName.Split('.');
                if (Item == FileTypeSplit[1])
                {
                    Result = true;
                    break;
                }
            }
            return Result;
        }

        /// <summary>
        /// 檔案名稱編碼
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="UploadPath"></param>
        /// <returns></returns>
        public string FileNameCode(string FileName, string UploadPath)
        {
            string[] FileNameSplit = FileName.Split('.');
            FileName = DateTime.Now.ToString("yyyyMMddHHmmssfff") + "." + FileNameSplit[1];
            string Paths = Path.Combine(HttpContext.Current.Server.MapPath(UploadPath), FileName);
            return Paths;
        }

        /// <summary>
        /// 檔案上傳
        /// </summary>
        /// <param name="Files">檔案</param>
        /// <param name="UploadPath">上傳路徑</param>
        /// <param name="FileDeputy">附檔名格式</param>
        /// <returns></returns>
        public Dictionary<string, object> FileUpload(HttpPostedFileBase Files, string UploadPath, string[] FileDeputy)
        {
            try
            {
                if (Files != null)
                {
                    if (Files.ContentLength > 0)
                    {
                        string FileName = Path.GetFileName(Files.FileName);
                        bool DeputyResult = FileNameDeputyCheck(FileName, FileDeputy);
                        if (DeputyResult)
                        {
                            string Paths = FileNameCode(FileName, UploadPath);
                            string FolderPaths = Path.Combine(HttpContext.Current.Server.MapPath(UploadPath));
                            if (!File.Exists(FolderPaths))
                            {
                                FolderAdd(FolderPaths);
                            }
                            Files.SaveAs(Paths);
                            MsgCode["Status"] = true;
                            MsgCode["Code"] = "Upload01";
                            MsgCode["FileName"] = Paths;
                        }
                        else
                        {
                            MsgCode["Code"] = "Upload02";
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Logs.FileLogs(Ex.ToString());
            }
            return MsgCode;
        }

        /// <summary>
        /// 檔案上傳
        /// </summary>
        /// <param name="Files">檔案</param>
        /// <param name="UploadPath">上傳路徑</param>
        /// <param name="FileDeputy">附檔名格式</param>
        /// <returns></returns>
        public Dictionary<string, object> FileUpload(IEnumerable<HttpPostedFileBase> Files, string UploadPath, string[] FileDeputy)
        {
            try
            {
                if (Files != null)
                {
                    foreach (HttpPostedFileBase FileItem in Files)
                    {
                        if (FileItem.ContentLength > 0)
                        {
                            string FileName = Path.GetFileName(FileItem.FileName);
                            bool DeputyResult = FileNameDeputyCheck(FileName, FileDeputy);
                            if (DeputyResult)
                            {
                                string Paths = FileNameCode(FileName, UploadPath);
                                string FolderPaths = Path.Combine(HttpContext.Current.Server.MapPath(UploadPath));
                                if (!File.Exists(FolderPaths))
                                {
                                    FolderAdd(FolderPaths);
                                }
                                FileItem.SaveAs(Paths);
                                MsgCode["Status"] = true;
                                MsgCode["Code"] = "Upload01";
                                MsgCode["FileName"] = Paths;
                            }
                            else
                            {
                                MsgCode["Code"] = "Upload02";
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Logs.FileLogs(Ex.ToString());
            }
            return MsgCode;
        }

        /// <summary>
        /// 檔案刪除
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="FilePath"></param>
        /// <returns></returns>
        public bool FileDelete(string FileName, string FilePath)
        {
            bool Result = false;
            try
            {
                string Paths = Path.Combine(HttpContext.Current.Server.MapPath(FilePath), FileName);
                if (File.Exists(Paths))
                {
                    File.Delete(Paths);
                    Result = true;
                }
            }
            catch (DirectoryNotFoundException Ex)
            {
                Logs.FileLogs(Ex.ToString());
            }
            return Result;
        }

        /// <summary>
        /// 檔案Ftp認證
        /// </summary>
        /// <returns></returns>
        public WebClient FileFtpAuth()
        {
            WebClient WebClient = new WebClient
            {
                Credentials = new NetworkCredential(ImageFTPAccount, ImageFTPPassword)
            };
            return WebClient;
        }

        /// <summary>
        /// 檔案上傳Ftp
        /// </summary>
        /// <param name="Files"></param>
        /// <param name="UploadPath"></param>
        /// <param name="FileDeputy"></param>
        /// <returns></returns>
        public Dictionary<string, object> FileFtpUpload(HttpPostedFileBase Files, string UploadPath, string[] FileDeputy)
        {
            try
            {
                if (Files != null)
                {
                    if (Files.ContentLength > 0)
                    {
                        string FileName = Path.GetFileName(Files.FileName);
                        string FileType = Files.ContentType.ToString();
                        bool DeputyResult = FileDeputyCheck(FileType, FileDeputy);
                        if (DeputyResult)
                        {
                            string[] FileNameSplit = FileName.Split('.');
                            FileName = DateTime.Now.ToString("yyyyMMddHHmmssfff") + "." + FileNameSplit[1];
                            string Paths = Path.Combine(HttpContext.Current.Server.MapPath(UploadPath), FileName);
                            Files.SaveAs(Paths);
                            WebClient WebClient = FileFtpAuth();
                            WebClient.UploadFile(ImageFTPServer + FileName, WebRequestMethods.Ftp.UploadFile, Paths);
                            bool DeleteResult = FileDelete(FileName, UploadPath);
                            if (DeleteResult)
                            {
                                MsgCode["Status"] = true;
                                MsgCode["Code"] = "Upload01";
                                MsgCode["FileName"] = FileName;
                            }
                        }
                        else
                        {
                            MsgCode["Code"] = "Upload02";
                        }
                    }
                }
            }
            catch (WebException Ex)
            {
                Logs.FileLogs(Ex.ToString());
            }
            return MsgCode;
        }

        /// <summary>
        /// 檔案上傳Ftp
        /// </summary>
        /// <param name="Files"></param>
        /// <param name="UploadPath"></param>
        /// <param name="FileDeputy"></param>
        /// <returns></returns>
        public Dictionary<string, object> FileFtpUpload(HttpPostedFile Files, string UploadPath, string[] FileDeputy)
        {
            try
            {
                if (Files != null)
                {
                    if (Files.ContentLength > 0)
                    {
                        string FileName = Path.GetFileName(Files.FileName);
                        string FileType = Files.ContentType.ToString();
                        bool DeputyResult = FileDeputyCheck(FileType, FileDeputy);
                        if (DeputyResult)
                        {
                            string[] FileNameSplit = FileName.Split('.');
                            FileName = DateTime.Now.ToString("yyyyMMddHHmmssfff") + "." + FileNameSplit[1];
                            string Paths = Path.Combine(HttpContext.Current.Server.MapPath(UploadPath), FileName);
                            Files.SaveAs(Paths);
                            WebClient WebClient = FileFtpAuth();
                            WebClient.UploadFile(ImageFTPServer + FileName, WebRequestMethods.Ftp.UploadFile, Paths);
                            bool DeleteResult = FileDelete(FileName, UploadPath);
                            if (DeleteResult)
                            {
                                MsgCode["Status"] = true;
                                MsgCode["Code"] = "Upload01";
                                MsgCode["FileName"] = FileName;
                            }
                        }
                        else
                        {
                            MsgCode["Code"] = "Upload02";
                        }
                    }
                }
            }
            catch (WebException Ex)
            {
                Logs.FileLogs(Ex.ToString());
            }
            return MsgCode;
        }

        /// <summary>
        /// 檔案FTP刪除
        /// </summary>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public bool FileFtpDelete(string FileName)
        {
            bool Result = false;
            try
            {
                FtpWebRequest FtpWebRequest = (FtpWebRequest)WebRequest.Create(ImageFTPServer + FileName);
                FtpWebRequest.Credentials = new NetworkCredential(ImageFTPAccount, ImageFTPPassword);
                FtpWebRequest.Method = WebRequestMethods.Ftp.DeleteFile;
                FtpWebResponse FtpWebResponse = (FtpWebResponse)FtpWebRequest.GetResponse();
                if (FtpWebResponse.StatusCode.ToString() == "FileActionOK")
                {
                    Result = true;
                };
            }
            catch (WebException Ex)
            {
                Logs.FileLogs(Ex.ToString());
            }
            return Result;
        }
    }
}