﻿using System;
using System.Net;
using System.Configuration;
using Newtonsoft.Json;
using System.Text;
using Models.Library.Api.Google.List;

namespace Models.Library.Api.Google
{
    /// <summary>
    /// GoogleGeocode Api
    /// </summary>
    public class GoogleGeocodeApi : Logs
    {
        /// <summary>
        /// GoogleGeocode Api key
        /// </summary>
        private readonly string Key = ConfigurationManager.AppSettings["GoogleMapKey"];

        /// <summary>
        /// GoogleGeocode Api Url
        /// </summary>
        private readonly string ApiUrl = @"https://maps.googleapis.com/maps/api/geocode/json";

        /// <summary>
        /// 取得GoogleMap Json字串
        /// </summary>
        /// <param name="Address"></param>
        /// <returns></returns>
        public string GetGeocodeData(string Address)
        {
            string Result = null;
            try
            {
                WebClient WebClient = new WebClient();
                string Url = ApiUrl + "?key=" + Key + "&address=" + Address;
                byte[] JsonData = WebClient.DownloadData(Url);
                Result = Encoding.UTF8.GetString(JsonData);
            }
            catch (Exception Ex)
            {
                FileLogs(Ex.ToString());
            }
            return Result;
        }

        /// <summary>
        /// 取得GoogleMap Json結果
        /// </summary>
        /// <param name="Address"></param>
        /// <returns></returns>
        public GeocodeResults GetDataJsonResult(string Address)
        {
            string GeocodeJson = GetGeocodeData(Address);
            GeocodeResults GeocodeResults = null;
            try
            {
                GeocodeData GeocodeData = JsonConvert.DeserializeObject<GeocodeData>(GeocodeJson);
                if (GeocodeData.Status == "OK")
                {
                    GeocodeResults = GeocodeData.Results[0];
                }
            }
            catch (Exception Ex)
            {
                FileLogs(Ex.ToString());
            }
            return GeocodeResults;
        }

        /// <summary>
        /// 取得完整地址
        /// </summary>
        /// <param name="Address"></param>
        /// <returns></returns>
        public string GetAddress(string Address)
        {
            GeocodeResults GeocodeResults = GetDataJsonResult(Address);
            string Result = null;
            if (GeocodeResults != null)
            {
                Result = GeocodeResults.Formatted_Address.ToString();
            }
            return Result;
        }

        /// <summary>
        /// 取得地址行政區域
        /// Mode 模式
        /// postal_code = 郵政編號
        /// administrative_area_level_1 = 縣市
        /// administrative_area_level_3 = 區
        /// route = 路
        /// street_number = 街牌號碼
        /// </summary>
        /// <param name="Address"></param>
        /// <param name="Mode"></param>
        /// <returns></returns>
        public string GetAdministrativeArea(string Address, string Mode = "postal_code")
        {
            GeocodeResults GeocodeResults = GetDataJsonResult(Address);
            string Result = null;
            if (GeocodeResults != null)
            {
                foreach (var Item in GeocodeResults.Address_Components)
                {
                    if (Item.Types.ToString() == Mode)
                    {
                        Result = Item.Long_Name;
                    }
                }
            }
            return Result;
        }

        /// <summary>
        /// 取得座標
        /// </summary>
        /// <param name="Address"></param>
        /// <returns></returns>
        public string GetCoordinate(string Address)
        {
            GeocodeResults GeocodeResults = GetDataJsonResult(Address);
            string Result = null;
            if (GeocodeResults != null)
            {
                string Lat = GeocodeResults.Geometry.Location.Lat.ToString();
                string Lng = GeocodeResults.Geometry.Location.Lng.ToString();
                Result = Lat + "," + Lng;
            }
            return Result;
        }
    }
}