﻿using System.Collections.Generic;

namespace Models.Library.Api.Google.List
{
    /// <summary>
    /// Google地理資料
    /// </summary>
    public class GeocodeData
    {
        /// <summary>
        /// 結果
        /// </summary>
        public List<GeocodeResults> Results { set; get; }

        /// <summary>
        /// 狀態
        /// </summary>
        public string Status { set; get; }
    }

    /// <summary>
    /// Google地理結果
    /// </summary>
    public class GeocodeResults
    {
        /// <summary>
        /// 地址組件
        /// </summary>
        public List<GeocodeAddressComponents> Address_Components { set; get; }

        /// <summary>
        /// 格式化地址
        /// </summary>
        public string Formatted_Address { set; get; }

        /// <summary>
        /// 幾何
        /// </summary>
        public GeocodeGeometry Geometry { set; get; }

        /// <summary>
        /// 地點編號
        /// </summary>
        public string Place_Id { set; get; }

        /// <summary>
        /// 型態
        /// </summary>
        public string[] Types { set; get; }
    }

    /// <summary>
    /// Google地理地址組件
    /// </summary>
    public class GeocodeAddressComponents
    {
        /// <summary>
        /// 長名
        /// </summary>
        public string Long_Name { set; get; }

        /// <summary>
        /// 簡稱
        /// </summary>
        public string Short_Name { set; get; }

        /// <summary>
        /// 型態
        /// </summary>
        public string[] Types { set; get; }
    }

    /// <summary>
    /// Google地理幾何
    /// </summary>
    public class GeocodeGeometry
    {
        /// <summary>
        /// 邊界
        /// </summary>
        public GeocodeBounds Bounds { set; get; }

        /// <summary>
        /// 位置
        /// </summary>
        public GeocodeCoordinate Location { set; get; }

        /// <summary>
        /// 位置型態
        /// </summary>
        public string Location_Type { set; get; }

        /// <summary>
        /// 視圖接口
        /// </summary>
        public GeocodeBounds Viewport { set; get; }
    }

    /// <summary>
    /// Google地理邊界
    /// </summary>
    public class GeocodeBounds
    {
        /// <summary>
        /// 東北
        /// </summary>
        public GeocodeCoordinate Northeast { set; get; }

        /// <summary>
        /// 西南
        /// </summary>
        public GeocodeCoordinate Southwest { set; get; }
    }

    /// <summary>
    /// Google地理座標
    /// </summary>
    public class GeocodeCoordinate
    {
        /// <summary>
        /// 緯度
        /// </summary>
        public double Lat { set; get; }

        /// <summary>
        /// 經度
        /// </summary>
        public double Lng { set; get; }
    }
}