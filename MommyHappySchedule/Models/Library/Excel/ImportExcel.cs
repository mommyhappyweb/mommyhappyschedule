﻿using System.IO;
using NPOI;
//HSSF － 提供讀寫Microsoft Excel XLS格式檔案的功能。
using NPOI.HSSF;
using NPOI.HSSF.Util;
using NPOI.HSSF.Model;
using NPOI.HSSF.UserModel;
//XSSF － 提供讀寫Microsoft Excel OOXML XLSX格式檔案的功能。
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Formula.Functions;

namespace Models.Library.Excel
{
    /// <summary>
    /// 匯入Excle
    /// </summary>
    public class ImportExcel
    {
        /// <summary>
        /// 設定Excle版本模式
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="Mode"></param>
        /// <returns></returns>
        public IWorkbook SetExcelMode(string FileName, int Mode = 2007)
        {
            FileStream File = new FileStream(FileName, FileMode.Open, FileAccess.Read);
            IWorkbook Excel = null;
            if (Mode == 2007)
            {
                Excel = new XSSFWorkbook(File);
            }
            if (Mode == 2003)
            {
                Excel = new HSSFWorkbook(File);
            }
            return Excel;
        }
    }
}