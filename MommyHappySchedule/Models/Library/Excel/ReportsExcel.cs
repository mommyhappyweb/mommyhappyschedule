﻿using System;
using System.IO;
using System.Web;
using System.Collections.Generic;
using NPOI;
//HSSF － 提供讀寫Microsoft Excel XLS格式檔案的功能。
using NPOI.HSSF;
using NPOI.HSSF.Util;
using NPOI.HSSF.Model;
using NPOI.HSSF.UserModel;
//XSSF － 提供讀寫Microsoft Excel OOXML XLSX格式檔案的功能。
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.SS.Formula.Functions;

namespace Models.Library.Excel
{
    /// <summary>
    /// 報表方法
    /// </summary>
    public class ReportsExcel : Logs
    {
        /// <summary>
        /// 設定Excle版本模式
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="Mode"></param>
        /// <returns></returns>
        public IWorkbook SetExcelMode(int Mode = 2007)
        {
            IWorkbook Excel = null;
            if (Mode == 2007)
            {
                Excel = new XSSFWorkbook();
            }
            if (Mode == 2003)
            {
                Excel = new HSSFWorkbook();
            }
            return Excel;
        }

        /// <summary>
        /// 本機匯出Excel
        /// </summary>
        /// <param name="Thead"></param>
        /// <param name="Tbody"></param>
        /// <param name="Path"></param>
        /// <param name="FileName"></param>
        public void GetExcel(List<string> Thead, List<string[]> Tbody, string PathName, string FileName, int Mode = 2007)
        {
            //建立XSSFWorkbook核心
            IWorkbook WB = SetExcelMode();

            //建立分頁
            ISheet Sheet = WB.CreateSheet(FileName);

            //設定好各種Style
            ICellStyle StyleConfig = SetCellsStyle(WB);

            //設定標頭
            SetTitle(Sheet, Thead);

            //設定內容
            SetContent(Sheet, Tbody);

            //儲存到實體路徑
            FileStream FS = new FileStream(Path.Combine(PathName, FileName + ".xlsx"), FileMode.Create, FileAccess.Write);
            WB.Write(FS);
            //WB.Dispose();
            //WB.Close();
            FS.Dispose();
            FS.Close();
        }

        /// <summary>
        /// 網頁匯出Excel
        /// </summary>
        /// <param name="Thead"></param>
        /// <param name="Tbody"></param>
        /// <param name="FileName"></param>
        public void GetExcel(List<string> Thead, List<string[]> Tbody, string FileName, int Mode = 2007)
        {
            try
            {
                IWorkbook WB = SetExcelMode(Mode);
                //直接下載檔案
                MemoryStream MS = new MemoryStream();

                //建立分頁
                ISheet Sheet = WB.CreateSheet(FileName);

                //設定好各種Style
                ICellStyle StyleConfig = SetCellsStyle(WB);

                //設定標頭
                SetTitle(Sheet, Thead);

                //設定內容
                SetContent(Sheet, Tbody);

                //建立新行
                //IRow tempIRow = Tabs.CreateRow(0);

                //建立單一表格
                //ICell tempICell = tempIRow.CreateCell(0, CellType.String);

                //取得要處理的一列
                //tempIRow = Tabs.GetRow(0);

                //取得要處理的Cell
                //tempICell = tempIRow.GetCell(0);

                //賦予Cell樣式
                //tempICell.CellStyle = colCellStyle;

                //設定Cell內文字。
                //tempICell.SetCellValue("123");

                //設定Cell內公式。
                //tempICell.SetCellFormula();

                //設定Cell內連結，先建立連結(IHyperlink)
                /*IHyperlink templink = new XSSFHyperlink(HyperlinkType.Document)
                {
                    //設定連結的路徑,內容可以參考Excel中輸入的文字
                    Address = "目錄!A1" //建立可以轉跳到Sheet(目錄)A1格子的連結
                };*/

                //把連結繫結到Cell
                //tempICell.Hyperlink = templink;

                //數字都是從0開始計算
                //Sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(1, 1, 1, 1));

                //將WorkBook寫入MemoryStream
                WB.Write(MS);
                string DateTimeNow = DateTime.Now.ToString("yyyyMMddHHmmss");
                //輸出
                HttpContext HttpContext = HttpContext.Current;
                HttpContext.Response.Clear();
                if (Mode == 2007)
                {
                    HttpContext.Response.AddHeader("Content-Disposition", "attachment;filename=" + DateTimeNow + FileName + ".xlsx");
                    HttpContext.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }
                else
                {
                    HttpContext.Response.AddHeader("Content-Disposition", "attachment;filename=" + DateTimeNow + FileName + ".xls");
                    HttpContext.Response.ContentType = "application/vnd.ms-excel";
                }
                HttpContext.Response.BinaryWrite(MS.ToArray());
                HttpContext.Response.Flush();
                HttpContext.Response.End();
                WB.Close();
                MS.Close();
                MS.Dispose();
            }
            catch (HttpException Ex)
            {
                FileLogs(Ex.ToString());
            }
        }

        /// <summary>
        /// 設定標題
        /// </summary>
        /// <param name="Sheet"></param>
        /// <param name="Data"></param>
        private void SetTitle(ISheet Sheet, List<string> Data)
        {
            try
            {
                for (int i = 0; i < Data.Count; i++)
                {
                    if (i == 0)
                    {
                        Sheet.CreateRow(0).CreateCell(i, CellType.String).SetCellValue(Data[i].ToString());
                    }
                    else
                    {
                        Sheet.GetRow(0).CreateCell(i, CellType.String).SetCellValue(Data[i].ToString());
                    }
                    SetAutoSizeCoulumn(Sheet, i);
                }
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }

        }

        /// <summary>
        /// 設定內容
        /// </summary>
        /// <param name="Sheet"></param>
        /// <param name="Data"></param>
        private void SetContent(ISheet Sheet, List<string[]> Data)
        {
            for (int i = 0; i < Data.Count; i++)
            {
                for (int j = 0; j < Data[i].Length; j++)
                {
                    if (j == 0)
                    {
                        Sheet.CreateRow(i + 1).CreateCell(j, CellType.String).SetCellValue(Data[i][j].ToString());
                    }
                    else
                    {
                        Sheet.GetRow(i + 1).CreateCell(j, CellType.String).SetCellValue(Data[i][j].ToString());
                    }
                    SetAutoSizeCoulumn(Sheet, j);
                }
            }
        }

        /// <summary>
        /// 設定自動調整欄寬
        /// 0=第一欄, 1=第二欄
        /// 後面是要設定的高 X 寬,單位是一個字的1/256
        /// </summary>
        /// <param name="Sheet"></param>
        /// <param name="Index"></param>
        /// <param name="Mode"></param>
        /// <param name="SetWidth"></param>
        private void SetAutoSizeCoulumn(ISheet Sheet, int Index, string Mode = "Auto", int SetWidth = 100)
        {
            if (Mode == "Auto")
            {
                Sheet.AutoSizeColumn(Index);
            }
            else
            {
                Sheet.SetColumnWidth(Index, SetWidth);
            }
        }

        /// <summary>
        /// 設定樣式
        /// </summary>
        /// <param name="WB"></param>
        /// <returns></returns>
        private ICellStyle SetCellsStyle(IWorkbook WB)
        {
            //建立新的CellStyle
            ICellStyle CellsStyle = WB.CreateCellStyle();

            //建立字型
            IFont StyleFont = WB.CreateFont();

            //設定文字字型
            StyleFont.FontName = "微軟正黑體";

            //設定文字大小
            //預設10px
            StyleFont.FontHeightInPoints = 10;
            CellsStyle.SetFont(StyleFont);

            //文字位置
            CellsStyle.Alignment = HorizontalAlignment.Left;

            //文字顏色
            StyleFont.Color = IndexedColors.Black.Index;

            //自訂RBG顏色
            IColor rbgColor = new XSSFColor(new byte[] { 200, 140, 20 });
            StyleFont.Color = rbgColor.Indexed;

            //背景顏色(一定要和圖樣同時存在才有作用)
            CellsStyle.FillForegroundColor = IndexedColors.LightYellow.Index;

            //背景圖樣
            CellsStyle.FillPattern = FillPattern.SolidForeground;

            //文字自動換列
            CellsStyle.WrapText = true;

            //Cell框線
            CellsStyle.BorderBottom = BorderStyle.Thin;
            CellsStyle.BorderLeft = BorderStyle.Thin;
            CellsStyle.BorderRight = BorderStyle.Thin;
            CellsStyle.BorderTop = BorderStyle.Thin;

            //Cell框線顏色
            CellsStyle.BottomBorderColor = IndexedColors.Black.Index;
            CellsStyle.LeftBorderColor = IndexedColors.Black.Index;
            CellsStyle.RightBorderColor = IndexedColors.Black.Index;
            CellsStyle.TopBorderColor = IndexedColors.Black.Index;
            return CellsStyle;
        }
    }
}