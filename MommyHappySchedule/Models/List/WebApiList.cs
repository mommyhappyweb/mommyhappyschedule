﻿namespace Models.List
{
    /// <summary>
    /// Web API
    /// </summary>
    public class WebApiList
    {
        /// <summary>
        /// 資料
        /// </summary>
        public object Data{ set; get; }

        /// <summary>
        /// 訊息代碼
        /// </summary>
        public string Code { set; get; }

        /// <summary>
        /// 訊息
        /// </summary>
        public string Message { set; get; }

        /// <summary>
        /// 認證參數
        /// </summary>
        public string MVC { set; get; }
    }

    /// <summary>
    /// API 回應資料
    /// </summary>
    public class ResponseJsonData
    {
        /// <summary>
        /// 狀態
        /// </summary>
        public bool Status { set; get; } = false;

        /// <summary>
        /// 訊息代碼
        /// </summary>
        public string Code { set; get; }

        /// <summary>
        /// 訊息
        /// </summary>
        public string Message { set; get; }

        /// <summary>
        /// 資料
        /// </summary>
        public object Data { set; get; }

        /// <summary>
        /// MVC
        /// </summary>
        public string MVC { set; get; }
    }
}