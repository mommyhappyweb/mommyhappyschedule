﻿using System.Collections.Generic;

namespace Models.List
{
    /// <summary>
    /// 客戶基本資料
    /// </summary>
    public class CustData
    {
        /// <summary>客戶索引</summary>
        public int Cust_ID { get; set; }

        /// <summary>客戶編號</summary>
        public string Cust_No { get; set; }

        /// <summary>客戶名稱</summary>
        public string Cust_Name { get; set; }

        /// <summary>手機電話</summary>
        public string Cell_Phone { get; set; }

        /// <summary>家裡電話</summary>
        public string House_Phone { get; set; }

        /// <summary>其他通訊資訊</summary>
        public string Other_Phone { get; set; }

        /// <summary>客戶來源</summary>
        public string Cust_Source { get; set; }

        /// <summary>房屋型態</summary>
        public byte House_Style { get; set; }

        /// <summary>樓層</summary>
        public float House_Floors { get; set; }

        /// <summary>坪數</summary>
        public string House_Area { get; set; }

        /// <summary>可否實習</summary>
        public bool Intern_Flag { get; set; }

        /// <summary>可否代班</summary>
        public bool Shift_Flag { get; set; }

        /// <summary>電話打卡</summary>
        public bool Phone_S_Flag { get; set; }

        /// <summary>前一日發簡訊</summary>
        public bool Text_Flag { get; set; }

        /// <summary>特殊服務時間</summary>
        public bool SP_Time_Flag { get; set; }

        /// <summary>是否在家</summary>
        public bool In_House { get; set; }

        /// <summary>開門鑰匙</summary>
        public string Key_Holder { get; set; }

        /// <summary>喜好的管家</summary>
        public string Favor_Staff { get; set; }

        /// <summary>不能派的管家</summary>
        public string Forbid_Staff { get; set; }

        /// <summary>職業</summary>
        public string Cust_Job { get; set; }

        /// <summary>公司</summary>
        public string Cust_Comp { get; set; }

        /// <summary>地址</summary>
        public string Cust_Addr { get; set; }

        /// <summary>地址座標</summary>
        public string Cust_Geometry { get; set; }

        /// <summary>大樓名稱</summary>
        public string Cust_Bulid { get; set; }

        /// <summary>交通指引</summary>
        public string Traff_Status { get; set; }

        /// <summary>客戶狀況背景</summary>
        public string Cust_Sp_Request { get; set; }

        /// <summary>特殊狀況禁忌</summary>
        public string Cust_Sp_Forbid { get; set; }

        /// <summary>姓別</summary>
        public byte Sex { set; get; }

        /// <summary>生日</summary>
        public string Birthday { set; get; }

        /// <summary>信箱</summary>
        public string Email { set; get; }

        /// <summary>身分證</summary>
        public string IDCard { set; get; }
    }

    /// <summary>
    /// 客戶合約資料列表
    /// </summary>
    public class CustContractDataList
    {
        public CustData CustData { set; get; }

        public CustContractData CustContractData { set; get; }

        public List<CustJobSchedule> CustJobSchedule { set; get; }

        public CustContractExpire CustContractExpire { set; get; }

        public CustContractPay CustContractPay { set; get; }

        public List<CustContractPayDetail> CustContractPayDetail { set; get; }
    }

    /// <summary>
    /// 客戶合約資料檔
    /// </summary>
    public class CustContractData
    {
        /// <summary>客戶合約資料檔編號</summary>
        public int Contract_ID { get; set; } = 0;

        /// <summary> 客戶編號 </summary>
        public string Cust_No { set; get; }

        /// <summary>合約編號</summary>
        public string Contract_Nums { get; set; }

        /// <summary>負責專員</summary>
        public string Charge_Staff_Name { get; set; }

        /// <summary>負責專員編號</summary>
        public string Charge_Staff_No { set; get; }

        /// <summary>合約開始日期</summary>
        public string Contract_S_Date { get; set; }

        /// <summary>合約結束日期</summary>
        public string Contract_E_Date { get; set; }

        /// <summary>實際結束日期</summary>
        public string Contract_R_Date { get; set; }

        /// <summary>合約次數</summary>
        public float Contract_Times { get; set; } = 0;

        /// <summary>合約狀況</summary>
        public byte Contract_Status { get; set; } = 0;

        /// <summary>合約狀況更新日期</summary>
        public string Contract_U_Date { get; set; }

        /// <summary>部門編號</summary>
        public byte Branch_ID { get; set; } = 0;

        /// <summary>部門</summary>
        public string Branch_Name { get; set; }

        /// <summary>服務週期</summary>
        public string Service_Cycle { get; set; }

        /// <summary>合約年限</summary>
        public byte Contract_Years { get; set; } = 0;

        /// <summary>管家人數</summary>
        public byte HK_Nums { get; set; } = 0;

        /// <summary>備註</summary>
        public string Note { set; get; }
    }

    /// <summary>
    /// 合約到期資料
    /// </summary>
    public class CustContractExpire
    {
        /// <summary>合約到期資料編號</summary>
        public int ContractExpire_ID { set; get; } = 0;

        /// <summary>合約編號</summary>
        public int Contract_ID { get; set; } = 0;

        /// <summary>客戶編號</summary>
        public string Cust_No { get; set; }

        /// <summary>員工姓名</summary>
        public string Staff_Name { set; get;}

        /// <summary>合約結束日期</summary>
        public string Contract_E_Date { get; set; }

        /// <summary>實際結束日期</summary>
        public string Contract_R_Date { get; set; }

        /// <summary>服務週期</summary>
        public string Service_Cycle { get; set; }

        /// <summary>管家人數</summary>
        public byte HK_Nums { get; set; } = 0;

        /// <summary>合約次數</summary>
        public float Contract_Times { get; set; } = 0;

        /// <summary>合約年限</summary>
        public byte Contract_Years { get; set; } = 0;

        /// <summary>合約結果</summary>
        public byte Contract_Result { get; set; } = 0;

        /// <summary>新約服務週期</summary>
        public string N_Service_Cycle { get; set; }

        /// <summary>新約管家人數</summary>
        public byte N_HK_Nums { get; set; } = 0;

        /// <summary>新約合約次數</summary>
        public float N_Contract_Times { get; set; } = 0;

        /// <summary>新約合約年限</summary>
        public byte N_Contract_Years { get; set; } = 0;

        /// <summary>新約起始日期</summary>
        public string N_Contract_S_Date { get; set; }
    }

    /// <summary>
    /// 客戶合約付款資訊
    /// </summary>
    public class CustContractPay
    {
        /// <summary>客戶合約交易內容編號</summary>
        public int ContractPay_ID { get; set; } = 0;

        /// <summary>客戶合約編號</summary>
        public int Contract_ID { set; get; } = 0;

        /// <summary>客戶編號</summary>
        public string Cust_No { set; get; }

        /// <summary>交易期數</summary>
        public byte C_Pay_Numbers { get; set; } = 0;

        /// <summary>交易方式</summary>
        public byte Payment_Type { get; set; } = 0;

        /// <summary>交易方式名稱</summary>
        public string Payment_Type_Name { get; set; }

        /// <summary>銀行代碼</summary>
        public string Out_Bank { get; set; }

        /// <summary>銀行帳號</summary>
        public string Out_Acct_No { get; set; }

        /// <summary>付款交易日期</summary>
        public string C_Pay_Date { get; set; }

        /// <summary>交易總金額</summary>
        public int C_Pay_Amt { get; set; } = 0;

        /// <summary>交易外加總稅額</summary>
        public int C_Pay_Tax { get; set; } = 0;

        /// <summary>是否外加稅</summary>
        public byte C_Tax_Flag { get; set; } = 0;

        /// <summary>鼎新訂單單號</summary>
        public string Export_ID { set; get; }

        /// <summary>備註</summary>
        public string Payment_Memo { get; set; }

        /// <summary>鼎新結帳單號</summary>
        public string ERP_No { set; get; }

        /// <summary>合約項目</summary>
        public byte Contract_Type { set; get; } = 0;
    }

    /// <summary>
    /// 客戶合約付款明細
    /// </summary>
    public class CustContractPayDetail
    {
        /// <summary>客戶合約分月交易編號</summary>
        public int CustContractPayDetail_ID { set; get; } = 0;

        /// <summary>客戶合約交易內容編號</summary>
        public int Pay_ID { set; get; } = 0;

        /// <summary>付款第幾期數</summary>
        public int Pay_Number { set; get; } = 0;

        /// <summary>交易方式</summary>
        public byte Payment_Type { get; set; } = 0;

        /// <summary>銀行代碼</summary>
        public string Out_Bank { get; set; }

        /// <summary>銀行帳號</summary>
        public string Out_Acct_No { get; set; }

        /// <summary>數量</summary>
        public byte Amount { set; get; } = 0;

        /// <summary>金額</summary>
        public int Amt { set; get; } = 0;

        /// <summary>預計交易日期</summary>
        public string B_Pay_Date { set; get; }

        /// <summary>是否付款</summary>
        public byte Fulfill { set; get; }

        /// <summary>是否付款名稱</summary>
        public string Fulfill_Name { set; get; }

        /// <summary>備註</summary>
        public string Sales_Memo { set; get; }

        /// <summary>實際付款日期</summary>
        public string R_Pay_Date { set; get; }

        /// <summary>外加稅額</summary>
        public float Tax { set; get; }
    }

    /// <summary>
    /// 客戶出勤時間表
    /// </summary>
    public class CustJobSchedule
    {
        /// <summary>指挀班別打卡出勤表編號</summary>
        public int Schedule_ID { get; set; } = 0;

        /// <summary>客戶合約資料檔編號</summary>
        public int Contract_ID { get; set; } = 0;

        /// <summary>客戶編號</summary>
        public string Cust_No { get; set; }

        /// <summary>排班日期</summary>
        public string Job_Date { get; set; }

        /// <summary>排班時間</summary>
        public byte Job_Time { get; set; } = 0;

        /// <summary>排班時間名稱</summary>
        public string Job_Time_Name { set; get; }

        /// <summary>員工編號</summary>
        public string Staff_No { get; set; }

        /// <summary>排班狀態</summary>
        public string Job_Result { get; set; }

        /// <summary>排班狀態編號</summary>
        public byte Job_Result_ID { get; set; } = 0;

        /// <summary>排班狀態名稱</summary>
        // public string Job_Result_Name { set; get; }

        /// <summary>服務屬性</summary>
        public int Job_Price { get; set; } = 0;

        /// <summary>異動屬性</summary>
        public byte Job_Flag { get; set; } = 0;

        /// <summary>管家評鑑</summary>
        public string Job_Status { get; set; }

        /// <summary>管家序號</summary>
        public byte HK_Serial { get; set; }

        /// <summary>單次備註</summary>
        public string Note { get; set; }
    }

    /// <summary>
    /// 指派班別督導出勤紀錄
    /// </summary>
    public class CustJobScheduleSupervision : CustJobSchedule
    {
        /// <summary>指派班別督導出勤紀錄編號</summary>
        public string ScheduleSupervision_ID { set; get; }

        /// <summary>
        /// 客戶名稱
        /// </summary>
        public string Cust_Name { set; get; }

        /// <summary>
        /// 員工名稱
        /// </summary>
        public string Staff_Name { set; get; }

        /// <summary>預估到達時間</summary>
        public string E_Start_Time { set; get; }

        /// <summary>預估離開時間</summary>
        public string E_End_Time { set; get; }

        /// <summary>到達時間</summary>
        public string Start_Time { set; get; }

        /// <summary>到達遲到原因</summary>
        public string Start_Id { set; get; }

        /// <summary>到達遲到原因說明</summary>
        public string Start_Des { set; get; }

        /// <summary>離開時間</summary>
        public string End_Time { set; get; }

        /// <summary>離開時間原因</summary>
        public string End_Id { set; get; }

        /// <summary>離開時間原因說明</summary>
        public string End_Des { set; get; }
    }
}