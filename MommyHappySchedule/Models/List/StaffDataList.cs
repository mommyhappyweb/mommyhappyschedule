﻿using System.Collections.Generic;

namespace Models.List
{
    /// <summary>
    /// 匯入EXCEL員工資料
    /// </summary>
    public class StaffDataList
    {
        public List<StaffData> StaffData { set; get; }
        public List<StaffJobData> StaffJobData { set; get; }
        public List<StaffRight> StaffRightData { set; get; }
    }

    /// <summary>
    /// 員工基本資料
    /// </summary>
    public class StaffData
    {
        /// <summary>員工索引</summary>
        public int Staff_ID { set; get; } = 0;

        /// <summary>員工編號</summary>
        public string Staff_No { set; get; }

        /// <summary>員工姓名</summary>
        public string Name { set; get; }

        /// <summary>性別</summary>
        public int Sex { set; get; } = 0;

        /// <summary>性別名稱</summary>
        public string Sex_Name { set; get; }

        /// <summary>生日</summary>
        public string Birth { set; get; }

        /// <summary>出生地</summary>
        public string Birth_Host { set; get; }

        /// <summary>血型</summary>
        public string Blood_Type { set; get; }

        /// <summary>身高</summary>
        public byte Height { set; get; } = 0;

        /// <summary>體重</summary>
        public byte Weight { set; get; } = 0;

        /// <summary>婚姻編號</summary>
        public byte Marrige_ID { set; get; } = 0;

        /// <summary>婚姻</summary>
        public string Marrige { set; get; }

        /// <summary>小孩人數</summary>
        public byte Childs { set; get; } = 0;

        /// <summary>照顧方式</summary>
        public string Take_Care_Style { set; get; }

        /// <summary>身分證</summary>
        public string RID { set; get; }

        /// <summary>通訊縣市</summary>
        public string L_City_Name { set; get; }

        /// <summary>通訊行政區</summary>
        public string L_Country_Name { set; get; }

        /// <summary>通訊門牌</summary>
        public string L_Road_Name { set; get; }

        /// <summary>戶籍縣市</summary>
        public string H_Country_Name { set; get; }

        /// <summary>戶籍行政區</summary>
        public string H_City_Name { set; get; }

        /// <summary>戶籍門牌</summary>
        public string H_Road_Name { set; get; }

        /// <summary>手機</summary>
        public string Cell_Phone { set; get; }

        /// <summary>市話區碼</summary>
        public string Tele_Code { set; get; }

        /// <summary>市話號碼</summary>
        public string House_Phone { set; get; }

        /// <summary>應徵日</summary>
        public string Interview_Date { set; get; }

        /// <summary>地址座標</summary>
        public string Geometry { set; get; }

        /// <summary>部門</summary>
        public int Branch_ID { set; get; } = 0;

        /// <summary>部門名稱</summary>
        public string Branch_Name { set; get; }

        /// <summary>應徵職位</summary>
        public int R_Job { set; get; } = 0;

        /// <summary>應徵職位名稱</summary>
        public string R_Job_Name { set; get; }

        /// <summary>緊急聯絡人</summary>
        public string Emg_Name { set; get; }

        /// <summary>緊急聯絡人關係</summary>
        public string Emg_Relation { set; get; }

        /// <summary>緊急聯絡人電話</summary>
        public string Emg_Cell_Phone { set; get; }

        /// <summary>應徵途徑</summary>
        public string Recurit_Source { set; get; }

        /// <summary>交通工具</summary>
        public string Traff_Type { set; get; }

        /// <summary>駕照</summary>
        public string Drive_Licence { set; get; }

        /// <summary>語文能力</summary>
        public string Lang_Ability { set; get; }

        /// <summary>車禍或手術</summary>
        public string Car_Surg_Status { set; get; }

        /// <summary>身心障礙</summary>
        public string Disable_Status { set; get; }

        /// <summary>健康狀況</summary>
        public string Health_Status { set; get; }

        /// <summary>可否上網</summary>
        public string Internet_Flag { set; get; }

        /// <summary>家人了解</summary>
        public string Family_Support { set; get; }

        /// <summary>有無負債</summary>
        public string Loan_Status { set; get; }

        /// <summary>希望待遇</summary>
        public string Wish_Salary { set; get; }

        /// <summary>健保資格</summary>
        public string Free_Insurance { set; get; }

        /// <summary>眷保人數</summary>
        public byte Insur_Nums { set; get; } = 0;

        /// <summary>扶養人數</summary>
        public byte Raise_Nums { set; get; } = 0;

        /// <summary>個人投保</summary>
        public string Insurance_Status { set; get; }

        /// <summary>可受訓日期</summary>
        public string Train_Date { set; get; }

        /// <summary>可外派</summary>
        public bool Out_Country_Flag { set; get; } = false;

        /// <summary>外派縣市</summary>
        public string Work_City_Name { set; get; }
    }

    /// <summary>
    /// 員工人事資料
    /// </summary>
    public class StaffJobData
    {
        /// <summary>合約狀況編號</summary>
        public int Job_Status_ID { set; get; } = 0;

        /// <summary>員工編號</summary>
        public string Staff_No { set; get; }

        /// <summary>合約狀況</summary>
        public string Job_Status { set; get; }

        /// <summary>姓名</summary>
        public string Staff_Name { set; get; }

        /// <summary>駐地</summary>
        public string Branch_Company { set; get; }

        /// <summary>部門</summary>
        public string Branch_Name { set; get; }

        /// <summary>職稱</summary>
        public string Job_Title { set; get; }

        /// <summary>職稱編號</summary>
        public int Job_Title_ID { set; get; } = 0;

        /// <summary>班制</summary>
        public string Job_TS { set; get; }

        /// <summary>班制編號</summary>
        public int Job_TS_ID { set; get; } = 0;

        /// <summary>主管姓名</summary>
        public string Mgr_Name { set; get; }

        /// <summary>補導組長</summary>
        public string Svr_Name { set; get; }

        /// <summary>到職日</summary>
        public string Join_Date { set; get; }

        /// <summary>離職日</summary>
        public string Depart_Date { set; get; }

        /// <summary>預計離職日</summary>
        public string ExpLeaveDate { set; get; }

        /// <summary>未晉升原因</summary>
        public string NoPrm_Reason { set; get; }

        /// <summary>部門</summary>
        public int Branch_ID { set; get; } = 0;
    }
    /// <summary>
    /// 員工權限管理
    /// </summary>
    public class StaffRight
    {
        /// <summary> 員工權限ID </summary>
        public int StaffRight_ID { set; get; }

        /// <summary>員工姓名</summary>
        public string Name { set; get; }

        /// <summary>帳號</summary>
        public string Account { set; get; }

        /// <summary>密碼</summary>
        public string Password { set; get; }

        /// <summary>群組</summary>
        public int Groups { set; get; } = 0;

        /// <summary> 群組名稱 </summary>
        public string Groups_Name { set; get; }

        /// <summary> 是否為最高權限 </summary>
        public bool IsAdmin { set; get; }
    }

    /// <summary>
    /// 員工權限群組管理
    /// </summary>
    public class StaffRightGroups
    {
        /// <summary>員工權限群組編號</summary>
        public int StaffRightGroups_ID { set; get; } = 0;

        /// <summary>群組代碼</summary>
        public string Groups_Code { set; get; }

        /// <summary>群組名稱</summary>
        public string Name { set; get; }

        /// <summary>總管理</summary>
        public bool Admin { set; get; } = false;

        /// <summary>總經理室</summary>
        public bool GM { set; get; } = false;

        /// <summary>人資管理部</summary>
        public bool HR { set; get; } = false;

        /// <summary>財務管理部</summary>
        public bool FIN { set; get; } = false;

        /// <summary>資訊管理部</summary>
        public bool IT { set; get; } = false;

        /// <summary>綜合計劃部</summary>
        public bool CP { set; get; } = false;

        /// <summary>行銷計畫部</summary>
        public bool MK { set; get; } = false;

        /// <summary>營業部-高雄-旗艦店</summary>
        public bool SD_KH_Flagship { set; get; } = false;
    }
}