﻿using System;
using System.Data;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using Models.Library;

namespace MommyHappySchedule
{
    public partial class JobScheduleTrans : System.Web.UI.Page
    {
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        SqlConnection INConnection1 = new SqlConnection(ConnString);
        SqlConnection INConnection2 = new SqlConnection("server=60.248.240.187\\ERPDB,2899;database=MH;uid=sa;pwd=Mh@ht5357");
        SqlCommand INCommand;
        SqlDataReader SQLReader;
        DataSet MyDataSet = new DataSet();
        SqlDataAdapter MyCommand;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((string)Session["Account"] == "" | (Session["Account"] == null))
                {
                    Session["msg"] = "您沒有使用此程式的權限!!";
                    Response.Redirect("Index.aspx");
                }


            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string INString;

            INString = "Select ID,Cust_Name,Cust_No,Staff_No,HK_Nums,Contract_S_Date,Contract_E_Date,Contract_Times,Contract_Years,Branch_name,Note" +
                       " from Cust_Contract_Data_View where Cust_No = '" + CustNo.Text.Trim() + "'";
            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            if (SQLReader.Read() == true)
            {
                CustName.Text = DBNull.Value.Equals(SQLReader[1]) ? "" : SQLReader[1].ToString(); //客戶姓名
                Cust_No.Text = DBNull.Value.Equals(SQLReader[2]) ? "" : SQLReader[2].ToString();   //客戶編號
                CustNo.Text = DBNull.Value.Equals(SQLReader[2]) ? "" : SQLReader[2].ToString();   //客戶編號
                ChargeStaff.Text = DBNull.Value.Equals(SQLReader[3]) ? "" : SQLReader[3].ToString();   //員工名稱
                HKNums.Text = DBNull.Value.Equals(SQLReader[4]) ? "1" : SQLReader[4].ToString(); //管家人數
                StDate.Text = DBNull.Value.Equals(SQLReader[5]) ? "" : Convert.ToDateTime(SQLReader[5].ToString()).ToShortDateString();   //合約起始日期
                EnDate.Text = DBNull.Value.Equals(SQLReader[6]) ? "" : Convert.ToDateTime(SQLReader[6].ToString()).ToShortDateString();   //合約終止日期
                ContractTimes.Text = DBNull.Value.Equals(SQLReader[7]) ? "0" : SQLReader[7].ToString();   //合約次數
                ContractYears.Text = DBNull.Value.Equals(SQLReader[8]) ? "1" : SQLReader[8].ToString(); //Contract_Year
                Contract_ID.Text = DBNull.Value.Equals(SQLReader[0]) ? "" : SQLReader[0].ToString();   //Contract_ID
                Branch_Name.Text = DBNull.Value.Equals(SQLReader[9]) ? "" : SQLReader[9].ToString();   //Branch_Name
                Note.Text = DBNull.Value.Equals(SQLReader[10]) ? "" : SQLReader[10].ToString();   //Note
            }
            else
            {
                Response.Write("<script language=JavaScript>alert('沒有來源客戶記錄');</script>");
                return;
            }
            INCommand.Connection.Close();

            //轉換至客戶合約資料

            INString = "Select ID,Cust_Name,Cust_No,Staff_No,HK_Nums,Contract_S_Date,Contract_E_Date,Contract_Times,Contract_Years,Branch_ID,Note" +
                      " from Cust_Contract_Data_View where Cust_No = '" + TCustNo.Text.Trim() + "'";
            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            if (SQLReader.Read() == true)
            {
                TCustName.Text = DBNull.Value.Equals(SQLReader[1]) ? "" : SQLReader[1].ToString(); //客戶姓名
                TCust_No.Text = DBNull.Value.Equals(SQLReader[2]) ? "" : SQLReader[2].ToString();   //客戶編號
                TCustNo.Text = DBNull.Value.Equals(SQLReader[2]) ? "" : SQLReader[2].ToString();   //客戶編號
                TChargeStaff.Text = DBNull.Value.Equals(SQLReader[3]) ? "" : SQLReader[3].ToString();   //員工名稱
                THKNums.Text = DBNull.Value.Equals(SQLReader[4]) ? "1" : SQLReader[4].ToString(); //管家人數
                TStDate.Text = DBNull.Value.Equals(SQLReader[5]) ? "" : Convert.ToDateTime(SQLReader[5].ToString()).ToShortDateString();   //合約起始日期
                TEnDate.Text = DBNull.Value.Equals(SQLReader[6]) ? "" : Convert.ToDateTime(SQLReader[6].ToString()).ToShortDateString();   //合約終止日期
                TContractTimes.Text = DBNull.Value.Equals(SQLReader[7]) ? "0" : SQLReader[7].ToString();   //合約次數
                TContractYears.Text = DBNull.Value.Equals(SQLReader[8]) ? "1" : SQLReader[8].ToString(); //Contract_Year
                TContract_ID.Text = DBNull.Value.Equals(SQLReader[0]) ? "" : SQLReader[0].ToString();   //Contract_ID
                TBranch_Name.Text = DBNull.Value.Equals(SQLReader[9]) ? "" : SQLReader[9].ToString();   //Branch_Name
                TNote.Text = DBNull.Value.Equals(SQLReader[10]) ? "" : SQLReader[10].ToString();   //Note
            }
            else
            {
                Response.Write("<script language=JavaScript>alert('沒有轉換至客戶記錄');</script>");
                return;
            }
            INCommand.Connection.Close();



            MyDataSet = new DataSet();
            MyCommand = new SqlDataAdapter(
                        "Select Convert(char,[Job_Date],111) as DJob,IIF(Job_Result = '正常', '', Job_Result ),Job_Price,ID,Job_Flag as JFlag from Cust_Job_Schedule_View where Contract_ID = " + Contract_ID.Text.Trim() +
                        " order by Job_Date DESC,ID DESC", INConnection1);
            MyCommand.Fill(MyDataSet, "DJob");
            MyCommand = new SqlDataAdapter("SELECT * FROM Branch_Data where Branch_Name ='" + Branch_Name.Text.Trim() + "'", INConnection1);
            MyCommand.Fill(MyDataSet, "Branch");
            MyCommand = new SqlDataAdapter("SELECT *  FROM Holiday_Date", INConnection1);
            MyCommand.Fill(MyDataSet, "Holiday_Date");
            MyCommand = new SqlDataAdapter("SELECT *  FROM Branch_Charge_Data where Branch_ID= " +
                                                                 MyDataSet.Tables["Branch"].DefaultView[0][0], INConnection1);
            MyCommand.Fill(MyDataSet, "Branch_Charge");

            DataTable ds = new DataTable();
            ds.Columns.Add(new DataColumn("DJob", typeof(string)));
            ds.Columns.Add(new DataColumn("ID", typeof(int)));
            ds.Columns.Add(new DataColumn("JFlag", typeof(int)));


            // 計算合次合約顯示

            for (int ii = 0; ii < MyDataSet.Tables["DJob"].DefaultView.Count; ii++)
            {
                DataRow dr;
                dr = ds.NewRow();
                dr[1] = MyDataSet.Tables["DJob"].DefaultView[ii][3];
                dr[2] = MyDataSet.Tables["DJob"].DefaultView[ii][4];

                if (DBNull.Value.Equals(MyDataSet.Tables["DJob"].DefaultView[ii][1]))
                {
                    dr[0] = MyDataSet.Tables["DJob"].DefaultView[ii][0].ToString().Trim();
                }
                else
                {
                    if (MyDataSet.Tables["DJob"].DefaultView[ii][1].ToString().Trim() == "")
                        dr[0] = MyDataSet.Tables["DJob"].DefaultView[ii][0].ToString().Trim();
                    else
                        dr[0] = MyDataSet.Tables["DJob"].DefaultView[ii][0].ToString().Trim() +
                                MyDataSet.Tables["DJob"].DefaultView[ii][1].ToString().Trim();
                }
                ds.Rows.Add(dr);
            }

            ds.DefaultView.Sort = "DJob ASC";
            Repeater2.DataSource = ds.DefaultView;
            Repeater2.DataBind();

            CheckBox1.Checked = false;
            CheckBox1.Visible = true;
            Button2.Visible = true;
        }

        protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            ListItemType objItemType = (ListItemType)e.Item.ItemType;
            Button TempButton = default(Button);
            ShareFunction SF = new ShareFunction();
            Label TempLabel = default(Label);

            if (objItemType == ListItemType.Item | objItemType == ListItemType.AlternatingItem)
            {
                TempButton = (Button)e.Item.FindControl("DJob");
                TempLabel = (Label)e.Item.FindControl("JFlag");
                //判斷是否是假日
                if (!SF.IsDate(TempButton.Text))
                {
                    TempButton.BackColor = Color.Red;
                    TempButton.ForeColor = Color.White;
                }
                else
                {
                    if (TempLabel != null)
                        switch (TempLabel.Text.Trim())
                        {
                            case "4":    //完成服務
                                TempButton.BackColor = Color.LightBlue;
                                TempButton.ForeColor = Color.Black;
                                break;
                            case "255":  //解約
                                TempButton.BackColor = Color.DarkGray;
                                TempButton.ForeColor = Color.White;
                                break;
                        }
                }
            }
        }

        protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Button TempButton = (Button)e.Item.FindControl("DJob");
            Label TempLabel2 = (Label)e.Item.FindControl("ID");
            Label TempLabel3 = (Label)e.Item.FindControl("JFlag");
            CheckBox TempCheckBox = (CheckBox)e.Item.FindControl("DCheckBox");
            ShareFunction SF = new ShareFunction();

            //判斷不是已服務或是已解約才處理
            if (TempLabel3.Text.Trim() != "4" & TempLabel3.Text.Trim() != "255")
            {
                TempCheckBox.Checked = (TempCheckBox.Checked) ? false : true;
                if (TempCheckBox.Checked)
                {
                    TempButton.BorderColor = Color.HotPink;
                    TempButton.BorderStyle = BorderStyle.Double;
                }
                else
                {
                    TempButton.BorderColor = Color.FromArgb(255, 255, 255, 255);
                    TempButton.BorderStyle = BorderStyle.Outset;
                }
            }


            Session["JDate"] = TempButton.Text.Trim();
            Session["WID"] = Convert.ToInt32(TempLabel2.Text);
            Session["UP"] = "1";
            //Session("WID") = tmpButton.Text
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (CheckBox1.Checked == false)
            {
                Response.Write("<script language=JavaScript>alert('未勾選確定轉換!!');</script>");
                return;
            }
            CheckBox1.Checked = false;
            string IDString = "(";
            string JDString = "";
            foreach (Control item in Repeater2.Items)  
            {
                Button TempButton = (Button)item.FindControl("DJob");
                Label TempLabel2 = (Label)item.FindControl("ID");
                Label TempLabel3 = (Label)item.FindControl("JFlag");
                CheckBox TempCheckBox = (CheckBox)item.FindControl("DCheckBox");
                if (TempCheckBox.Checked)   //如果有選擇轉移
                {
                    IDString = IDString + TempLabel2.Text.Trim() + ",";
                    JDString = JDString + TempButton.Text.Trim() + ",";
                }
            }
            IDString = IDString.Substring(0, IDString.Length - 1) + ")";
            JDString = JDString.Substring(0, JDString.Length - 1);

            string TContractID = TContract_ID.Text;
            string INString;
            //此部份是將移轉次數寫成新合約,但一樣的客編,不一樣的 ID 如此次數比較不會亂
            //如果拿掉則會寫會到目標合約中,不產生新合約
            //--------------------------------------------------------------------------------------------------------------
            string[] JDArray = JDString.Split(',');
            //產生新次數合約
            INString = "Insert into Cust_Contract_Data (Charge_Staff_Name,Contract_S_Date,Contract_E_Date,Contract_R_Date,Cust_No," +
                              "Contract_Times,Contract_Status,Contract_U_Date,Branch_ID,Service_Cycle,Contract_Years,HK_Nums,Note) Values ( '" +
                              TChargeStaff.Text + "','" + JDArray[0] + "','" + JDArray[JDArray.Length - 1] + "',null,'" + TCust_No.Text +
                              "',0,0,getdate()," + TBranch_Name.Text + ",'',0," + THKNums.Text + ",'')";
            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            INCommand.ExecuteNonQuery();
            INCommand.Connection.Close();

            //找出最後一筆客編號ID號
            INString = "Select Top 1 ID from Cust_Contract_Data where Cust_No = '" + TCustNo.Text.Trim() + "' Order by ID DESC";
            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            if (SQLReader.Read() == true)
                TContractID = SQLReader[0].ToString().Trim();
            INCommand.Connection.Close();
            //--------------------------------------------------------------------------------------------------------------

            //移轉合約次數
            INString = "Update Cust_Job_Schedule set Cust_No ='" + TCust_No.Text.Trim() + "',Contract_ID = " + TContractID +
                              " where ID in " + IDString ;
            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            INCommand.ExecuteNonQuery();
            INCommand.Connection.Close();

            //記錄至舊合約備註
            INString = "Update Cust_Contract_Data set Note =  CONCAT(note,'" + DateTime.Now.ToShortDateString() + "移轉以下日期次數[" +
                       JDString  + "]到[" + TCust_No.Text +"]去')  where ID = " + Contract_ID.Text ;
            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            INCommand.ExecuteNonQuery();
            INCommand.Connection.Close();

            //記錄至新合約備註
            INString = "Update Cust_Contract_Data set Note =  CONCAT(note,'" + DateTime.Now.ToShortDateString() + "從[" + Cust_No.Text +
                       "]移轉以下日期次數[" +  JDString + "]來')  where ID = " + TContract_ID.Text;
            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            INCommand.ExecuteNonQuery();
            INCommand.Connection.Close();

            //Logs
            Logs SLogs = new Logs();     //記錄更改內容
            SLogs.ScheduleLogs("Schedule.log", Session["Username"].ToString() + "|從[" + Cust_No.Text +
                       "]移轉以下日期次數[" + JDString + "]到[" + TCust_No.Text + "]");

            Button1_Click(sender, e);
        }
    }
}