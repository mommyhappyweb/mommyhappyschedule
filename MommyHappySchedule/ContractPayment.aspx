﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContractPayment.aspx.cs" Inherits="MommyHappySchedule.ContractPayment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        .auto-style1 {
            border-style: solid; 
            border-width: thin;
            width: 12%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="border-style: double; border-width: medium; width: 100%">
                <tr>
                    <td>
                        <asp:Label ID="ContractID" runat="server" Visible="false"></asp:Label>
                        <asp:TextBox ID="CustNo" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="Button1" runat="server" Text="查尋" OnClick="Button1_Click" />
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="Index.aspx">返回主選單</asp:HyperLink>
                    </td>
                    <td>
                        <asp:Button ID="Button2" runat="server" Text="滙出" OnClick="Button2_Click" Visible="False"  />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">客戶姓名：<asp:Label ID="Contract_ID" runat="server" Visible="false"></asp:Label>
                        <asp:Label ID="Branch_Name" runat="server" Visible="false"></asp:Label>
                    </td>
                    <td class="auto-style1">
                        <asp:Label ID="CustName" runat="server" Text=""></asp:Label></td>
                    <td class="auto-style1">合約編號：</td>
                    <td class="auto-style1">
                        <asp:Label ID="Cust_No" runat="server" Text=""></asp:Label></td>
                    <td class="auto-style1">負責專員：</td>
                    <td class="auto-style1">
                        <asp:Label ID="ChargeStaff" runat="server" Text=""></asp:Label></td>
                    <td class="auto-style1">管家人數：</td>
                    <td class="auto-style1">
                        <asp:Label ID="HKNums" runat="server" Text="1"></asp:Label></td>
                </tr>
                <tr>
                    <td class="auto-style1">合約起始：</td>
                    <td class="auto-style1">
                        <asp:Label ID="StDate" runat="server" Text=""></asp:Label></td>
                    <td class="auto-style1">合約終止：</td>
                    <td class="auto-style1">
                        <asp:Label ID="EnDate" runat="server" Text=""></asp:Label></td>
                    <td class="auto-style1">合約次數：</td>
                    <td class="auto-style1">
                        <asp:Label ID="ContractTimes" runat="server" Text="1"></asp:Label></td>
                    <td class="auto-style1">合約年限：</td>
                    <td class="auto-style1">
                        <asp:Label ID="ContractYears" runat="server" Text="1"></asp:Label></td>
                </tr>
                <tr>
                    <td class="auto-style1">備註：</td>
                    <td colspan="7" class="auto-style1">
                        <asp:Label ID="Note" runat="server" Text="Label"></asp:Label></td>
                </tr> 
                <tr>
                    <td colspan="8" style="background-color: #FFFFCC">前期服務次數</td>
                </tr>
                <tr>
                    <td colspan="8">
                        <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemDataBound" OnItemCommand="Repeater1_ItemCommand">
                            <ItemTemplate>
                                <asp:Label ID="ID" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "ID") %>'></asp:Label>
                                <asp:Label ID="JFlag" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "JFlag") %>'></asp:Label>
                                <asp:Button ID="DJob" runat="server" Width="8%" Text='<%# DataBinder.Eval(Container.DataItem, "DJob") %>'></asp:Button>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
                <tr>
                    <td colspan="8" style="background-color: #FFFFCC">本期服務次數</td>
                </tr>
                <asp:Panel ID="Panel1" runat="server" Visible ="false">
                 <tr>
                    <td colspan="8" style="background-color: #FFFFCC"><asp:CheckBox ID="CheckBox1" runat="server" Text="取消假日" /><asp:Button ID="Button5" runat="server" Text="確定取消" OnClick="Button5_Click" /></td>
                </tr>
                </asp:Panel>
                <tr>
                    <td colspan="8">
                        <asp:Repeater ID="Repeater2" runat="server" OnItemDataBound="Repeater1_ItemDataBound" OnItemCommand="Repeater1_ItemCommand">
                            <ItemTemplate>
                                <asp:Label ID="ID" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "ID") %>'></asp:Label>
                                <asp:Label ID="JFlag" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "JFlag") %>'></asp:Label>
                                <asp:Button ID="DJob" runat="server" Width="8%" Text='<%# DataBinder.Eval(Container.DataItem, "DJob") %>'></asp:Button>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
                <tr>
                    <td colspan="8" style="background-color: #FFFFCC">所有服務次數</td>
                </tr>
                <tr>
                    <td colspan="8">
                        <asp:Repeater ID="Repeater3" runat="server" Visible="false">
                            <ItemTemplate>
                                <asp:Button ID="DJob" runat="server" Width="8%" Text='<%# DataBinder.Eval(Container.DataItem, "DJob") %>'></asp:Button>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
                <tr>
                    <td colspan="8" class="auto-style8"></td>
                </tr>
                <tr>
                    <td class="auto-style7" colspan ="2">
                        付款方式：<asp:DropDownList ID="RadioButtonList3" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="RadioButtonList3_SelectedIndexChanged">
                            <asp:ListItem>請選擇</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        建檔日期：<asp:Label ID="CR_Date" runat="server" Text="無"></asp:Label>
                    </td>
                    <td>
                        銀行代碼：<asp:TextBox ID="Bank_No" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        銀行(支票)帳號：<asp:TextBox ID="Account_No" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        總金額：<asp:TextBox ID="TotalFee" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        應稅：<asp:CheckBox ID="TaxCheck" runat="server" AutoPostBack="true" OnCheckedChanged="TaxCheck_CheckedChanged" />
                    </td>
                    <td>
                        稅額：<asp:Label ID="TotalTax" runat="server"></asp:Label>
                    </td>

                </tr>
                <tr>
                    <td colspan="8" class="auto-style7">
                        <table>
                            <asp:Repeater ID="Repeater4" runat="server" OnItemDataBound="Repeater4_ItemDataBound">
                                <HeaderTemplate>
                                    <tr>
                                        <td class="auto-style7">
                                            <asp:Label ID="Label4" runat="server" Text="期" class="auto-style7" Width="20"></asp:Label>
                                            <asp:Label ID="Label5" runat="server" Text="日期" class="auto-style7" Width="80"></asp:Label>
                                            <asp:Label ID="Label6" runat="server" Text="方式" class="auto-style7" Width="70"></asp:Label>
                                            <asp:Label ID="Label7" runat="server" Text="帳號" class="auto-style7" Width="120"></asp:Label>
                                            <asp:Label ID="Label8" runat="server" Text="金額" class="auto-style7" Width="50"></asp:Label>
                                            <asp:Label ID="Label11" runat="server" Text="稅" class="auto-style7" Width="50"></asp:Label>
                                            <asp:Label ID="Label13" runat="server" Text="付" class="auto-style7" Width="20"></asp:Label>
                                            <asp:Label ID="Label12" runat="server" Text="實付日期" class="auto-style7" Width="80"></asp:Label>
                                        </td>
                                    </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td class="auto-style7">
                                            <asp:Label ID="ID" runat="server" class="auto-style7" Width="20" Text='<%# DataBinder.Eval(Container.DataItem, "ID", "{0:00}") %>'></asp:Label>
                                            <asp:TextBox ID="PayDate" runat="server" class="auto-style7" Width="80" Text='<%# DataBinder.Eval(Container.DataItem, "PayDate", "{0:yyyy/MM/dd}") %>'></asp:TextBox>
                                            <asp:DropDownList ID="PayType" runat="server" Width="74" CssClass="auto-style7">
                                                <asp:ListItem>請選擇</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="PayText" runat="server" class="auto-style7" Width="120" Text='<%# DataBinder.Eval(Container.DataItem, "PayText") %>'></asp:TextBox>
                                            <asp:TextBox ID="PayAmt" runat="server" class="auto-style7" Width="50" Text='<%# DataBinder.Eval(Container.DataItem, "PayAmt","{0:#,##0}") %>'></asp:TextBox>
                                            <asp:TextBox ID="PayTax" runat="server" class="auto-style7" Width="50" Text='<%# DataBinder.Eval(Container.DataItem, "PayTax","{0:#,##0}") %>'></asp:TextBox>
                                            <asp:CheckBox ID="PayDone" class="auto-style7" Width="20" runat="server" />
                                            <asp:Label ID="R_PayDate" runat="server" class="auto-style7" Width="80" Text='<%# DataBinder.Eval(Container.DataItem, "RPayDate", "{0:yyyy/MM/dd}") %>'></asp:Label>
                                            <asp:Label ID="PaymentType" runat="server" class="auto-style7" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "PayType") %>'></asp:Label>
                                            <asp:Label ID="PayDoneFlag" runat="server" class="auto-style7" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "PayDone") %>'></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>

                            </asp:Repeater>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="auto-style7">儲存付款資料</td>
                    <td colspan="2" class="auto-style7">
                        <asp:Button ID="Button3" runat="server" Text="存檔" OnClick="Button2_Click" />
                        <asp:Button ID="Button4" runat="server" Text="清除" />
                    </td>
                </tr>
                <tr>
                    <td colspan="8">
                        <asp:DataGrid ID="DataGrid1" runat="server"></asp:DataGrid></td>
                </tr>
                <tr>
                    <td colspan="8">
                        <asp:GridView ID="GridView1" runat="server"></asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
