﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Data;
using System.Drawing;
using System.Configuration;
using Models.Library;

namespace MommyHappySchedule
{
    public partial class CustJobScheduleConfirm : System.Web.UI.Page
    {
        private System.Data.DataSet MyDataSet = new System.Data.DataSet();
        private System.Data.SqlClient.SqlDataAdapter MyCommand;
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        System.Data.SqlClient.SqlConnection INConnection1 = new System.Data.SqlClient.SqlConnection(ConnString);
        private System.Data.SqlClient.SqlCommand INCommand;
        private string INString;

        protected void Page_Load(object sender, System.EventArgs e)
        {

            // If Session("account") = "" Or Session("Username") <> "mommyhappy" Then
            // Session("msg") = "您沒有使用此程式的權限!!"
            // Response.Redirect("Index.aspx")
            // End If


            if (!Page.IsPostBack)
            {
                MyDataSet = new System.Data.DataSet();
                MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Branch_Data where ID > 0 ", INConnection1);
                MyCommand.Fill(MyDataSet, "Branch");


                Branch.DataSource = MyDataSet.Tables["Branch"].DefaultView;
                Branch.DataTextField = "Branch_Name";
                Branch.DataValueField = "ID";
                Branch.DataBind();

                Show_Date.Text = DateTime.Now.AddDays(1).ToShortDateString();
                RadioButtonList1.SelectedIndex = 0;
            }
        }

        protected void Check_Report_Click(object sender, EventArgs e)
        {
            ShareFunction SF = new ShareFunction();
            if (!SF.IsDate(Show_Date.Text))
                return;


            Label2.Text = DateTime.Now.ToLongTimeString();

            string Select_Banch;
            if (Branch.SelectedItem.Text.Trim() == "南高雄")
                Select_Banch = " (Branch_Name ='南高雄' or Branch_Name = '屏東')";
            else
                Select_Banch = " Branch_Name ='" + Branch.SelectedItem.Text.Trim() + "'";


            // 找出尚未離職的管家
            MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                "SELECT Staff_No,Staff_Name,Job_Title FROM Staff_Job_Data_View where (Depart_Date is null or Depart_Date >= '" +
                Show_Date.Text + "') and " +
                Select_Banch + " and Staff_No <> '' and Staff_No is not null  Order by Staff_No", INConnection1);
            MyCommand.Fill(MyDataSet, "Staff_Nu");
            string Select_Staff = "";
            foreach (DataRowView Si in MyDataSet.Tables["Staff_Nu"].DefaultView)
                Select_Staff = Select_Staff + "'" + Si[0].ToString().Trim() + "',";
            if (Select_Staff.Length > 0)
                Select_Staff = " Staff_No in (" + Select_Staff.Substring(0, Select_Staff.Length - 1) + ")";

            if (TextBox6.Text != "")
            {
                Select_Banch = " (Staff_Name like '%" + TextBox6.Text + "%' or Staff_No like '%" + TextBox6.Text + "%') ";
                Select_Staff = " (Staff_Name like '%" + TextBox6.Text + "%' or Staff_No like '%" + TextBox6.Text + "%') ";
                //以下是針對駐點內尋找
                //Select_Banch = Select_Banch + " and (Staff_Name like '%" + TextBox6.Text + "%' or Staff_No like '%" + TextBox6.Text + "%') ";
                //Select_Staff = Select_Staff + " and (Staff_Name like '%" + TextBox6.Text + "%' or Staff_No like '%" + TextBox6.Text + "%') ";

            }

            if (TextBox7.Text != "")
            {
                Select_Banch = "( Cust_Name like '%" + TextBox7.Text + "%' or Cust_No like '%" + TextBox7.Text + "%' )";
                Select_Staff = "( Cust_Name like '%" + TextBox7.Text + "%' or Cust_No like '%" + TextBox7.Text + "%' )";
                //以下是針對駐點內尋找
                //Select_Banch = Select_Banch + " and Cust_Name like '%" + TextBox7.Text + "%' or Cust_No like '%" + TextBox7.Text + "%' ";
                //Select_Staff = Select_Staff + " and Cust_Name like '%" + TextBox7.Text + "%' or Cust_No like '%" + TextBox7.Text + "%' ";

            }


            switch (RadioButtonList1.SelectedIndex)
            {
                default:
                case 0:
                    {
                        Select_Banch = Select_Banch + " and Job_Flag <> 255";
                        Select_Staff = Select_Staff + " and Job_Flag <> 255";
                        break;
                    }

                case 1:
                    {
                        Select_Banch = Select_Banch + " and Job_Flag >= 2 ";
                        Select_Staff = Select_Staff + " and Job_Flag >= 2 ";
                        break;
                    }

                case 2:
                    {
                        Select_Banch = Select_Banch + " and ((Job_Flag < 2 or Job_Flag is null ) and Job_Flag <> 255)  ";
                        Select_Staff = Select_Staff + " and ((Job_Flag < 2 or Job_Flag is null ) and Job_Flag <> 255)  ";
                        break;
                    }
            }



            MyDataSet = new System.Data.DataSet();
            //配合可以跨區支援,改判斷管家編號並顯示工作
            if (Select_Staff.Length > 0)
                MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                    "SELECT * FROM Job_Schedule_Confirm_View where Job_Date = '" + Show_Date.Text + "' and " + Select_Staff +
                    "  Order by Job_Time,Staff_No,Cust_No", INConnection1);
            else
                MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                    "SELECT * FROM Job_Schedule_Confirm_View where Job_Date = '" + Show_Date.Text + "' and " + Select_Banch +
                    "  Order by Job_Time,Staff_No,Cust_No", INConnection1);

            MyCommand.Fill(MyDataSet, "Cust_Job_Schedule");
            DataGrid1.DataSource = MyDataSet.Tables["Cust_Job_Schedule"].DefaultView;
            DataGrid1.DataBind();

            Label2.Text = Label2.Text + "-" + DateTime.Now.ToLongTimeString();
        }

        public void Show_Sc_Type(object sender, DataGridItemEventArgs e)
        {
            ListItemType objItemType = (ListItemType)e.Item.ItemType;
            Label TmpLabel;
            RadioButtonList tmpRadioButtonList;


            if (objItemType == ListItemType.Item | objItemType == ListItemType.AlternatingItem)
            {
                TmpLabel = (Label)e.Item.FindControl("Label3");
                tmpRadioButtonList = (RadioButtonList)e.Item.FindControl("RadioButtonList3");

                    switch (e.Item.Cells[14].Text)
                {
                    case "0":
                        {
                            TmpLabel.Text = "未繳費";
                            //tmpRadioButtonList.Enabled = false;    //先暫時不用
                            break;
                        }

                    case "1":
                        {
                            TmpLabel.Text = "可排班";
                            //tmpRadioButtonList.Enabled = true;     //先暫時不用
                            break;
                        }

                    case "2":
                        {
                            TmpLabel.Text = "已確認";
                            //tmpRadioButtonList.Enabled = true;     //先暫時不用
                            break;
                        }

                    case "3":
                        {
                            TmpLabel.Text = "服務中";
                            TmpLabel.BackColor = Color.LightGreen;
                            //tmpRadioButtonList.Enabled = false ;   //先暫時不用
                            break;
                        }
                    case "4":
                        {
                            TmpLabel.Text = "已服務";
                            TmpLabel.BackColor = Color.LightPink;
                            //tmpRadioButtonList.Enabled = false ;   //先暫時不用
                            break;
                        }
                }
            }
        }

        public void Show_Job_Detail(object sender, DataGridCommandEventArgs e)
        {
        }


        protected void Change_Screen(object sender, EventArgs e)
        {
            if (((Control)sender).ID == "Left_Screen")
                Show_Date.Text = Convert.ToDateTime(Show_Date.Text).AddDays(-1).ToShortDateString();
            if (((Control)sender).ID == "Right_Screen")
                Show_Date.Text = Convert.ToDateTime(Show_Date.Text).AddDays(1).ToShortDateString();
            if ((Convert.ToDateTime(Show_Date.Text) - Convert.ToDateTime(DateTime.Now.ToShortDateString())).TotalDays < 0)
                Button7.Enabled = false;
            else
                Button7.Enabled = true;
            RadioButtonList2.ClearSelection();
            RadioButtonList1.ClearSelection();

            Check_Report_Click(sender, e);
        }


        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            RadioButtonList2.ClearSelection();
            Check_Report_Click(sender, e);
        }


        protected void Button6_Click(object sender, EventArgs e)
        {
            TextBox6.Text = "";
            TextBox7.Text = "";
        }
        protected void RadioButtonList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            RadioButtonList tmpRadioButtonList;
            int Gi;

            for (Gi = 0; Gi <= DataGrid1.Items.Count - 1; Gi++)
            {
                tmpRadioButtonList = (RadioButtonList)DataGrid1.Items[Gi].FindControl("RadioButtonList3");
                if (tmpRadioButtonList != null)
                    if (DataGrid1.Items[Gi].Cells[14].Text == "1" | DataGrid1.Items[Gi].Cells[14].Text == "2" | DataGrid1.Items[Gi].Cells[14].Text == "0")
                        tmpRadioButtonList.SelectedIndex = RadioButtonList2.SelectedIndex;
            }

            Page_Load(sender, e);
        }
        protected void Button7_Click(object sender, EventArgs e)
        {
            RadioButtonList tmpRadioButtonList;
            int tmpID;
            int Gi;

            Label4.Text = "";
            for (Gi = 0; Gi <= DataGrid1.Items.Count - 1; Gi++)
            {
                tmpID = System.Convert.ToInt32(DataGrid1.Items[Gi].Cells[0].Text);
                tmpRadioButtonList = (RadioButtonList)DataGrid1.Items[Gi].FindControl("RadioButtonList3");
                if (tmpRadioButtonList != null)
                {
                    if (tmpRadioButtonList.SelectedIndex >= 0)
                    {
                        if (tmpRadioButtonList.SelectedIndex == 0)
                            INString = "Update Cust_Job_Schedule set Job_Flag = 2 where Job_Flag <=1 and  ID = " + tmpID;
                        else
                            INString = "Update Cust_Job_Schedule set Job_Flag = 1 where Job_Flag <=2 and  ID = " + tmpID;
                        INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                        INCommand.Connection.Open();
                        INCommand.ExecuteNonQuery();
                        INCommand.Connection.Close();
                    }
                }
            }
            RadioButtonList2.ClearSelection();
            RadioButtonList1.ClearSelection();
            Check_Report_Click(sender, e);
        }
       
    }

}