﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Drawing;
using System.Data;
using System.Web.Script.Serialization;
using System.Configuration;
using Models.Library;

namespace MommyHappySchedule
{
    public partial class Customer_Contract_Report : System.Web.UI.Page
    {

        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        System.Data.SqlClient.SqlConnection INConnection1 = new System.Data.SqlClient.SqlConnection(ConnString);
 
        private System.Data.DataSet MyDataSet;
        private System.Data.SqlClient.SqlDataAdapter MyCommand;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if ((string)Session["account"] == "")
            {
                Session["msg"] = "您沒有使用此程式的權限!!";
                Response.Redirect("Index.aspx");
            }

            if (!Page.IsPostBack)
            {
                MyDataSet = new System.Data.DataSet();
                MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Branch_Data ", INConnection1);
                MyCommand.Fill(MyDataSet, "Branch");

                Branch.DataSource = MyDataSet.Tables["Branch"].DefaultView;
                Branch.DataTextField = "Branch_Name";
                Branch.DataValueField = "ID";
                Branch.DataBind();

                Check_Year.SelectedValue = DateTime.Now.Year.ToString();
                Check_Month.SelectedValue = DateTime.Now.Month.ToString();

                MultiView1.ActiveViewIndex = 1;
            }
        }


        protected void Check_Report_Click(object sender, EventArgs e)
        {
            // Dim CToday As Integer = Year(Today)
            System.Data.DataTable dt;
            System.Data.DataRow dr;
            int Start_Banch, End_Banch;
            int [] CGrade = { 0, 0, 0, 0 };

            // Label1.Text = Branch.SelectedItem.Text


            Label1.Text = DateTime.Now.ToLongTimeString();

            dt = new System.Data.DataTable();
            dt.Columns.Add(new System.Data.DataColumn("ID", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("Contract_Number", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("Contract_Name", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("Staff_Name", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("Last_Year_Cu_Lo", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("Last_Year_HK_Lo", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("Last_Year_Cu_Gr", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("This_Year_Cu_Lo", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("This_Year_HK_Lo", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("This_Year_Cu_Gr", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("Contract_E_Date", typeof(DateTime)));

            if (Branch.SelectedIndex == 0)
            {
                Start_Banch = 1;
                End_Banch = Branch.Items.Count - 1;
            }
            else
            {
                Start_Banch = Branch.SelectedIndex;
                End_Banch = Branch.SelectedIndex;
            }

            int jj;

            int [] HC = { 0, 0, 0, 0, 0, 0, 0, 0, 0};

            for (jj = Start_Banch; jj <= End_Banch; jj++)
            {
                string Te_String;
                int [] TB = { 0, 0, 0, 0,0, 0, 0,  0 };

                if (RadioButtonList1.SelectedIndex == 0)
                    Te_String = "";
                else
                    Te_String = " and ( Cust_Service_Level = " + (RadioButtonList1.SelectedIndex - 1).ToString() +
                                " or N_Cust_Service_Level = " + (RadioButtonList1.SelectedIndex - 1).ToString() + ")";

                MyDataSet = new System.Data.DataSet();
                MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                            "SELECT * from Cust_Service_View where Branch_ID = " + jj + " and year(Report_Gen_Date) = " +
                            (Convert.ToInt16(Check_Year.SelectedValue) - 1).ToString() + Te_String + " Order by Cust_No", INConnection1);
                MyCommand.Fill(MyDataSet, "Service_View");

                if (MyDataSet.Tables["Service_View"].DefaultView.Count ==0 )
                {
                    continue;
                }

                int ii, DC;

                if (Branch.SelectedIndex == 0)
                    DC = 0;
                else
                    DC = MyDataSet.Tables["Service_View"].DefaultView.Count - 1;

                for (ii = 0; ii <= DC; ii++)
                {
                    dr = dt.NewRow();
                    dr[0] = Branch.Items[jj].Text;
                    dr[1] = MyDataSet.Tables["Service_View"].DefaultView[ii][1];
                    dr[2] = MyDataSet.Tables["Service_View"].DefaultView[ii][22];
                    dr[3] = MyDataSet.Tables["Service_View"].DefaultView[ii][2];
                    dr[4] = MyDataSet.Tables["Service_View"].DefaultView[ii][7];
                    dr[5] = MyDataSet.Tables["Service_View"].DefaultView[ii][8];
                    dr[10] = MyDataSet.Tables["Service_View"].DefaultView[ii][3];

                    switch (Convert.ToInt16(MyDataSet.Tables["Service_View"].DefaultView[ii][11]))
                    {
                        case 0:
                            {
                                dr[6] = "優";
                                CGrade[0] = CGrade[0] + 1;
                                break;
                            }

                        case 1:
                            {
                                dr[6] = "A";
                                CGrade[1] = CGrade[1] + 1;
                                break;
                            }

                        case 2:
                            {
                                dr[6] = "B";
                                CGrade[2] = CGrade[2] + 1;
                                break;
                            }

                        default:
                            {
                                dr[6] = "C";
                                CGrade[3] = CGrade[3] + 1;
                                break;
                            }
                    }
                    dr[7] = MyDataSet.Tables["Service_View"].DefaultView[ii][16];
                    dr[8] = MyDataSet.Tables["Service_View"].DefaultView[ii][17];
                    switch (Convert.ToInt16(MyDataSet.Tables["Service_View"].DefaultView[ii][20]))
                    {
                        case 0:
                            {
                                dr[9] = "優";
                                CGrade[0] = CGrade[0] + 1;
                                break;
                            }

                        case 1:
                            {
                                dr[9] = "A";
                                CGrade[1] = CGrade[1] + 1;
                                break;
                            }

                        case 2:
                            {
                                dr[9] = "B";
                                CGrade[2] = CGrade[2] + 1;
                                break;
                            }

                        default:
                            {
                                dr[9] = "C";
                                CGrade[3] = CGrade[3] + 1;
                                break;
                            }
                    }

                    dt.Rows.Add(dr);
                }
            }



            DataGrid1.DataSource = dt.DefaultView;
            DataGrid1.DataBind();
            Session["dt"] = dt;

            // Label1.Text = Label1.Text & "-" & Now.ToLongTimeString
            // If Branch.SelectedIndex <> 0 Then
            // Dim tmpTotal As Int16
            // tmpTotal = CGrade(0) + CGrade(1) + CGrade(2) + CGrade(3)
            // Label4.Text = "優:" & Format(CGrade(0) / tmpTotal, "0.0%")
            // Label5.Text = "A:" & Format(CGrade(1) / tmpTotal, "0.0%")
            // Label6.Text = "B:" & Format(CGrade(2) / tmpTotal, "0.0%")
            // Label7.Text = "C:" & Format(CGrade(3) / tmpTotal, "0.0%")
            // End If
            Page_Load(sender, e);
        }

        protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            ListItemType objItemType = (ListItemType)e.Item.ItemType;
            Button tmpLabel1;
            ShareFunction SF = new ShareFunction();

            if (objItemType == ListItemType.Item | objItemType == ListItemType.AlternatingItem)
            {
                tmpLabel1 = (Button)e.Item.FindControl("DJob");
                if (SF.IsNumeric(tmpLabel1.Text.Substring(tmpLabel1.Text.Length - 1, 1)) == false)
                {
                    tmpLabel1.ForeColor = Color.Red;
                    tmpLabel1.Font.Bold = true;
                }
            }
        }

        public void Show_Sc_Type(object sender, DataGridItemEventArgs e)
        {
            ListItemType objItemType = (ListItemType)e.Item.ItemType;
            ShareFunction SF = new ShareFunction();

            if (objItemType == ListItemType.Item | objItemType == ListItemType.AlternatingItem)
            {
                if (SF.IsDate(e.Item.Cells[4].Text))
                {
                    if (Convert.ToDateTime(e.Item.Cells[4].Text).Year  == Convert.ToInt16(Check_Year.SelectedValue )- 1)
                        e.Item.ForeColor = Color.BlueViolet;
                    if (e.Item.Cells[10].Text == "C")
                    {
                        e.Item.Font.Bold = true;
                        e.Item.Font.Underline = true;
                        e.Item.ForeColor = Color.Red;
                    }
                }
            }
        }

        protected void DataGrid1_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            System.Data.DataTable dt = (DataTable)Session["dt"];

            if (e.SortExpression == "Contract_Name")
                dt.DefaultView.Sort = "";
            else
                dt.DefaultView.Sort = e.SortExpression + " DESC";

            DataGrid1.DataSource = dt.DefaultView;
            DataGrid1.DataBind();
            Label1.Text = e.SortExpression;

            Page_Load(source, e);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Label2.Text = "";
            MultiView1.ActiveViewIndex = 1;
        }

        protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            LinkButton tmpLB = (LinkButton)e.Item.Cells[1].Controls[0];

            MyDataSet = new System.Data.DataSet();
            MyCommand = new System.Data.SqlClient.SqlDataAdapter("Select convert(varchar,Job_Date) + rtrim(Job_Result) as DJob from Cust_Job_Schedule where Cust_No = '" + tmpLB.Text + "'", INConnection1);
            MyCommand.Fill(MyDataSet, "DJob");

            Label2.Text = "客戶編號：" + tmpLB.Text;

            Repeater1.DataSource = MyDataSet.Tables["DJob"].DefaultView;
            Repeater1.DataBind();

            MultiView1.ActiveViewIndex = 0;
        }
        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
    }

}