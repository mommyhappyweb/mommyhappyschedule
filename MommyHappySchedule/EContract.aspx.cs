﻿using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using System.Data.SqlClient;

namespace MommyHappySchedule
{

    partial class EContract : Page
    {
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];

        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = "建立合約";
            if (!Page.IsPostBack)
            {

            }
        }
    }
}