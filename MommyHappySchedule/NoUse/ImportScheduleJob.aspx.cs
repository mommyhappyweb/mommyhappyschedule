﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Data;
using System.Drawing;
using Models.Library;
using System.Configuration;

namespace MommyHappySchedule.DataRebuilt
{
    public partial class ImportScheduleJob : System.Web.UI.Page
    {
        
        private System.Data.DataTable dt, dt2;
        private System.Data.DataRow dr;
        private System.Data.DataSet MyDataSet = new System.Data.DataSet();
        private System.Data.SqlClient.SqlDataAdapter MyCommand;
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        private static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        private System.Data.SqlClient.SqlConnection INConnection1 = new System.Data.SqlClient.SqlConnection(ConnString);
        private System.Data.SqlClient.SqlCommand INCommand, INCommand1;
        private System.Data.SqlClient.SqlDataReader SQLReader;
        private string INString, INString1;



        protected void Page_Load(object sender, System.EventArgs e)
        {
            // If Session("dept") <> "Finance Dept." And Session("dept") <> "IT Dept." Then
            // Session("msg") = "您沒有使用此程式的權限!!"
            Response.Redirect("/Index.aspx");
            // End If

            if (!Page.IsPostBack)
            {
                MyDataSet = new System.Data.DataSet();
                MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Branch_Data where ID > 0 ", INConnection1);
                MyCommand.Fill(MyDataSet, "Branch");

                Branch.DataSource = MyDataSet.Tables["Branch"].DefaultView;
                Branch.DataTextField = "Branch_Name";
                Branch.DataValueField = "ID";
                Branch.DataBind();

                Check_Year.SelectedValue = DateTime.Now.Year.ToString();
                Check_Month.SelectedValue = DateTime.Now.Month.ToString();
            }
        }

        protected void Check_Report_Click(object sender, EventArgs e)
        {
            int Start_Branch, End_Branch;
            int Ki;

            TextBox1.Text = "0";
            TextBox2.Text = "0";
            TextBox3.Text = "0";
            TextBox4.Text = "0";
            Label3.Text = "";
            Label4.Text = "";

            Label2.Text = DateTime.Now.ToLongTimeString();
            if (Branch.SelectedIndex == 0)
            {
                Start_Branch = 1;
                End_Branch = 10;
            }
            else
            {
                Start_Branch = Branch.SelectedIndex;
                End_Branch = Branch.SelectedIndex;
            }
            string[] FileArray =
            {
            "南高派工明細表",
            "中高派工明細表",
            "北高派工明細表",
            "台南派工明細表",
            "南台中派工明細表",
            "北台中派工明細表",
            "新竹派工明細表",
            "桃園派工明細表",
            "中壢派工明細表",
            "板橋派工明細表",
            "旗艦派工明細表"
        };

            Ki = Convert.ToInt32 (Branch.SelectedValue);

            string String4;
            if (Convert.ToInt32(Check_Month.SelectedValue) > DateTime.Now.Month)
                String4 = "Provider=Microsoft.ACE.OLEDB.12.0;" + ((DBMode == "Local") ? ConfigurationManager.AppSettings["FSLocal"]:
                          ConfigurationManager.AppSettings["FSServer"]) + "派工明細表\\" + Check_Year.SelectedItem.Text + (DateTime.Now.Month).ToString("00") + 
                          FileArray[Ki - 1] + ".xlsx;Extended Properties='EXCEL 12.0 Xml;HDR=NO;IMEX=1;'";
            else
                String4 = "Provider=Microsoft.ACE.OLEDB.12.0;" + ((DBMode == "Local") ? ConfigurationManager.AppSettings["FSLocal"] : 
                          ConfigurationManager.AppSettings["FSServer"]) + "派工明細表\\" + Check_Year.SelectedItem.Text + (Check_Month.SelectedIndex + 1).ToString("00") +
                          FileArray[Ki - 1] + ".xlsx;Extended Properties='EXCEL 12.0 Xml;HDR=NO;IMEX=1;'";

            System.Data.OleDb.OleDbConnection INConnection4 = new System.Data.OleDb.OleDbConnection(String4);
            System.Data.OleDb.OleDbCommand INCommand;
            System.Data.OleDb.OleDbDataReader OleDbReader;
            int RCount = 1;
            string HK_Name = ".";
            string HKU = ".";
            string HKT = ".";
            string Sheet_Name = ".";
            string Sheet_Name_1 = ".";

            dt = new System.Data.DataTable();
            dt.Columns.Add(new System.Data.DataColumn("ID", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("HK_Name", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("Day_ID", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B3", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B4", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B5", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B6", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B7", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B8", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B9", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B10", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B11", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B12", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B13", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B14", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B15", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B16", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B17", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B18", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B19", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B20", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B21", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B22", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B23", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B24", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B25", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B26", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B27", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B28", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B29", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B30", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B31", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("RN", typeof(int)));
            dt.Columns.Add(new System.Data.DataColumn("HKN", typeof(string)));


            string Select_Banch;
            if (Branch.Items[Ki - 1].Text.Trim() == "南高雄")
                Select_Banch = " (營業處 ='南高雄' or 營業處 = '屏東') ";
            else
                Select_Banch = " 營業處 ='" + Branch.Items[Ki - 1].Text.Trim() + "'";

            if (Branch.Items[Ki - 1].Text.Trim() == "南高雄")
                MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Staff_Addr_View where Branch_ID = 1 or Branch_ID = 11 ", INConnection1);
            else
                MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Staff_Addr_View where Branch_ID = " + Ki, INConnection1);
            MyCommand.Fill(MyDataSet, "Staff_Data");



            INConnection4.Open();
            dt2 = INConnection4.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new[] { null, null, null, "TABLE" });
            Sheet_Name = (Convert.ToInt32(Check_Year.SelectedValue) - 1911).ToString("000") + "." + Check_Month.SelectedValue.ToString() + "$";
            Sheet_Name_1 = "'" + (Convert.ToInt32(Check_Year.SelectedValue) - 1911).ToString("000") + "#" + (Check_Month.SelectedIndex + 1).ToString("00") + "$'";
            for (var cellNum = 0; cellNum <= dt2.Rows.Count - 1; cellNum++)
            {
                if ((string)dt2.Rows[cellNum]["TABLE_NAME"] == Sheet_Name_1)
                {
                    Sheet_Name = Sheet_Name_1;
                    break;
                }
            }
            INConnection4.Close();

            INCommand = new System.Data.OleDb.OleDbCommand("Select * from [" + Sheet_Name + "]", INConnection4);
            INCommand.Connection.Open();
            OleDbReader = INCommand.ExecuteReader();
            // Create more rows for the table.
            OleDbReader.Read();
            OleDbReader.Read();
            OleDbReader.Read();

            while (OleDbReader.Read() == true)
            {
                if (DBNull.Value.Equals(OleDbReader[0]) & DBNull.Value.Equals(OleDbReader[1]) & DBNull.Value.Equals(OleDbReader[2]) & 
                    DBNull.Value.Equals(OleDbReader[3]) & DBNull.Value.Equals(OleDbReader[4]))
                    break;

                dr = dt.NewRow();
                dr[34] = RCount;

                if (DBNull.Value.Equals(OleDbReader[2]))
                {
                    dr[1] = HK_Name.Trim();
                    dr[35] = HKU.Trim();
                    dr[0] = HKT.Trim();
                }
                else
                {
                    int tmpC = 0;
                    while (Convert.ToInt32(OleDbReader[2].ToString()[tmpC]) < 255 & OleDbReader[2].ToString().Length > tmpC + 1) // 找出第一個中文名字
                        tmpC = tmpC + 1;

                    int tmpE = tmpC;
                    while (tmpE < OleDbReader[2].ToString().Length)    // 找出最後一個中文名字
                    {
                        if (Convert.ToInt32 (OleDbReader[2].ToString()[tmpE]) > 255)
                            tmpE = tmpE + 1;
                        else
                            break;
                    }
                    dr[1] = OleDbReader[2].ToString().Substring(tmpC, tmpE - tmpC);    // 員工姓名


                    // 避免錯誤員編
                    DataView dv = new DataView(MyDataSet.Tables["Staff_Data"])
                    {
                        RowFilter = "Name = '" + dr[1] + "'"
                    };
                    if (dv.Count > 0)
                        dr[35] = dv[0][0].ToString();
                    else
                        dr[35] = OleDbReader[2].ToString().Substring(0, (6 < OleDbReader[2].ToString().Length ) ? 6 : OleDbReader[2].ToString().Length );

                    if (dr[1].ToString() != "無人服務")
                    {
                        string tmpString = "SELECT Job_Title FROM Staff_Job_Data_View where Staff_Name = '" + dr[1] + "'";
                        // Label4.Text = Label4.Text & tmpString
                        INCommand1 = new System.Data.SqlClient.SqlCommand(tmpString, INConnection1);
                        INCommand1.Connection.Open();
                        System.Data.SqlClient.SqlDataReader SQLReader1 = INCommand1.ExecuteReader();
                        if (SQLReader1.Read() == true)
                            dr[0] = DBNull.Value.Equals(SQLReader1[0]) ? "" : SQLReader1[0].ToString().Substring(0, 2);
                        INCommand1.Connection.Close();
                    }
                    if (OleDbReader[2].ToString().Contains("計次") == true )
                        dr[0] = "計次";

                    HK_Name = dr[1].ToString();
                    HKU = dr[35].ToString();
                    if (!DBNull.Value.Equals(dr[0]))
                        HKT = dr[0].ToString();
                    else
                        HKT = ".";
                }

                // 上下午
                dr[2] = DBNull.Value.Equals(OleDbReader[3]) ? "" : OleDbReader[3].ToString().Substring(0,1);

                for (var cellNum = 3; cellNum <= 33; cellNum++) // 管家,日期及31天資料
                    dr[cellNum] = string.Format("{0}", OleDbReader[1 + cellNum]).Trim();
                RCount += 1;
                dt.Rows.Add(dr);
            }
            INCommand.Connection.Close();

            Session["dt"] = dt;
            Session["CS"] = null;
            Session["ds"] = null;
            Session["Pa"] = 0;
            Change_Screen(sender, e);

            Label2.Text = Label2.Text + "-" + DateTime.Now.ToLongTimeString();
            Page_Load(sender, e);
        }

        public void Check_Sc(object sender, System.EventArgs e)
        {
            DataGridItem Dtemp;
            Button tmpButton;

            Dtemp = (DataGridItem)((Control)sender).Parent.Parent;


            // textbox5.Text = Dtemp.ItemIndex  '第幾筆

            // tmpButton = CType(Dtemp.FindControl("HK_Name"), Button)
            Label3.Text = Dtemp.Cells[1].Text + "->" + Dtemp.Cells[17].Text;
            tmpButton = (Button)Dtemp.FindControl(((Control)sender).ID);
            Label4.Text += tmpButton.Text;
            tmpButton.BackColor = Color.LightBlue;
            if (TextBox1.Text == "0")
                TextBox1.Text = string.Format("{0:0000}", Dtemp.ItemIndex) + ((Control)sender).ID;
            else if (TextBox2.Text == "0")
                TextBox2.Text = string.Format("{0:0000}", Dtemp.ItemIndex) + ((Control)sender).ID;
            else if (TextBox3.Text == "0")
                TextBox3.Text = string.Format("{0:0000}", Dtemp.ItemIndex) + ((Control)sender).ID;
            else
                TextBox4.Text = string.Format("{0:0000}", Dtemp.ItemIndex) + ((Control)sender).ID;

            Page_Load(sender, e);
        }

        public void Show_Sc_Type(object sender, DataGridItemEventArgs e)
        {
            ListItemType objItemType = (ListItemType)e.Item.ItemType;
            Button TempButton;
            string TmpString;
            string INString;
            System.Data.SqlClient.SqlDataReader SQLReader;
            int CS = 0;
            int iCount = 1;


            if (Session["CS"] == null)
                CS = 0;
            else
                CS = Convert.ToInt32(Session["CS"]);
            if (objItemType == ListItemType.Header)
            {
                for (iCount = 1; iCount <= 14; iCount++)
                    e.Item.Cells[iCount + 2].Text = (iCount + CS - 1).ToString();
            }


            if (objItemType == ListItemType.Item | objItemType == ListItemType.AlternatingItem)
            {
                for (iCount = 1; iCount <= 14; iCount++)
                {
                    TempButton = (Button)e.Item.FindControl("B" + iCount.ToString());
                    if (TempButton != null)
                    {
                        TempButton.Width = 75;
                        // TmpString = "2018/01/" + Right(TempButton.ID, Len(TempButton.ID) - 1)
                        TmpString = Check_Year.SelectedItem.Text + "/" + (Check_Month.SelectedIndex + 1).ToString("00") + "/" + (iCount + CS - 1).ToString();
                        // TextBox1.Text = TextBox1.Text + TmpString
                        ShareFunction SF = new ShareFunction();
                        if (SF.IsDate(TmpString))
                        {
                            INString = "SELECT  H_Date ,H_Memo FROM Holiday_Date where H_Date = '" + TmpString + "'";
                            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                            INCommand.Connection.Open();
                            SQLReader = INCommand.ExecuteReader();

                            if (SQLReader.Read())
                            {
                                TempButton.BackColor = Color.LightPink;
                                TempButton.Width = 75;
                                e.Item.Cells[iCount + 2].BackColor = Color.LightBlue;
                            }
                            else if (Convert.ToDateTime(TmpString).DayOfWeek  == DayOfWeek.Sunday  | Convert.ToDateTime(TmpString).DayOfWeek == DayOfWeek.Saturday)
                            {
                                TempButton.BackColor = Color.LightPink;
                                TempButton.Width = 75;
                                e.Item.Cells[iCount + 2].BackColor = Color.LightBlue;
                            }
                            INCommand.Connection.Close();
                        }
                    }
                }
            }
        }

        public void Show_Job_Detail(object sender, DataGridCommandEventArgs e)
        {
            Page_Load(sender, e);
        }


        protected void Button1_Click(object sender, System.EventArgs e)
        {
            DataGrid tmpGrid;
            string tmpText, ft, st;
            int Mi, fi, si;
            Button fButton, sButton;

            if (Session["ds"] == null | Session["dt"] == null | Session["CS"] == null)
                Check_Report_Click(sender, e);

            tmpGrid = DataGrid3;

            if (TextBox1.Text != "0" & TextBox2.Text != "0")
            {
                // first cell
                fi = Convert.ToInt32 (TextBox1.Text.Substring(0, 4));
                ft = TextBox1.Text.Substring(4 , TextBox1.Text.Length  - 4);
                fButton = (Button)tmpGrid.Items[fi].FindControl(ft);
                // second cell
                si = Convert.ToInt32 (TextBox2.Text.Substring(0, 4));
                st = TextBox2.Text.Substring(4,TextBox2.Text.Length  - 4);
                sButton = (Button)tmpGrid.Items[si].FindControl(st);
                // swap
                tmpText = fButton.Text;
                fButton.Text = sButton.Text;
                fButton.BackColor= Color.Empty;
                sButton.Text = tmpText;
                sButton.BackColor = Color.Empty;
            }

            if (TextBox1.Text != "0" & TextBox3.Text != "0")
            {
                // first cell
                fi = Convert.ToInt32(TextBox1.Text.Substring(0, 4));
                ft = TextBox1.Text.Substring(4, TextBox1.Text.Length - 4);
                fButton = (Button)tmpGrid.Items[fi].FindControl(ft);
                // second cell
                si = Convert.ToInt32(TextBox3.Text.Substring(0, 4));
                st = TextBox3.Text.Substring(4, TextBox3.Text.Length - 4);
                sButton = (Button)tmpGrid.Items[si].FindControl(st);
                // swap
                tmpText = fButton.Text;
                fButton.Text = sButton.Text;
                fButton.BackColor = Color.Empty;
                sButton.Text = tmpText;
                sButton.BackColor = Color.Empty;
            }

            if (TextBox1.Text != "0" & TextBox4.Text != "0")
            {
                // first cell
                fi = Convert.ToInt32(TextBox1.Text.Substring(0, 4));
                ft = TextBox1.Text.Substring(4, TextBox1.Text.Length - 4);
                fButton = (Button)tmpGrid.Items[fi].FindControl(ft);
                // second cell
                si = Convert.ToInt32(TextBox4.Text.Substring(0, 4));
                st = TextBox4.Text.Substring(4, TextBox4.Text.Length - 4);
                sButton = (Button)tmpGrid.Items[si].FindControl(st);
                // swap
                tmpText = fButton.Text;
                fButton.Text = sButton.Text;
                fButton.BackColor = Color.Empty;
                sButton.Text = tmpText;
                sButton.BackColor = Color.Empty;
            }
            TextBox1.Text = "0";
            TextBox2.Text = "0";
            TextBox3.Text = "0";
            TextBox4.Text = "0";

            System.Data.DataTable ds;
            ds = new System.Data.DataTable();
            ds.Columns.Add(new System.Data.DataColumn("ID", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("HK_Name", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("Day_ID", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B1", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B2", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B3", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B4", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B5", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B6", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B7", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B8", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B9", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B10", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B11", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B12", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B13", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B14", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("RN", typeof(int)));

            for (Mi = 0; Mi <= DataGrid3.Items.Count - 1; Mi++)
            {
                dr = ds.NewRow();
                dr[0] = DataGrid3.Items[Mi].Cells[0].Text;
                dr[1] = DataGrid3.Items[Mi].Cells[1].Text;
                dr[2] = DataGrid3.Items[Mi].Cells[2].Text;
                dr[17] = DataGrid3.Items[Mi].Cells[17].Text;

                for (var Ci = 1; Ci <= 14; Ci++)
                {
                    sButton = (Button)DataGrid3.Items[Mi].FindControl("B" + Ci.ToString());
                    dr[Ci + 2] = sButton.Text;
                }
                ds.Rows.Add(dr);
            }

            // Dim CS As Integer = Session("CS")
            // Dim dt As Data.DataTable = Session("dt")

            // '反拷貝14欄位到 dt
            // For Mi = 0 To dt.Rows.Count - 1
            // For Ci = 0 To 13
            // dt.Rows(Mi).Item(CS + Ci + 2) = ds.Rows(Mi).Item(Ci + 3)
            // Next
            // Next
            // Session("dt") = dt
            Session["ds"] = ds;
            DataGrid3.DataSource = ds.DefaultView;
            DataGrid3.CurrentPageIndex = (Session["Pa"] == null) ? 1 : Convert.ToInt32(Session["Pa"]);
            DataGrid3.DataBind();

            Page_Load(sender, e);
        }

        protected void Button2_Click(object sender, System.EventArgs e)
        {
            if (Session["ds"] == null)
                Check_Report_Click(sender, e);

            TextBox1.Text = "0";
            TextBox2.Text = "0";
            TextBox3.Text = "0";
            TextBox4.Text = "0";
            Label3.Text = "";
            Label4.Text = "";
            DataTable ds =(DataTable)Session["ds"];
            DataGrid3.DataSource = ds.DefaultView;
            DataGrid3.CurrentPageIndex = (Session["Pa"] == null ) ? 1 : Convert.ToInt32(Session["Pa"]);
            DataGrid3.DataBind();

            Page_Load(sender, e);
        }

        protected void Button3_Click(object sender, System.EventArgs e)
        {
            Page_Load(sender, e);
        }
        protected void Button4_Click(object sender, System.EventArgs e)
        {
            // 儲存資料至資料庫
            System.Data.DataTable dt = (DataTable)Session["dt"];
            int ii, jj;
            TextBox2.Text = "";
            Label2.Text = DateTime.Now.ToLongTimeString();
            //讀取假況資料表
            string[] ReCust = new string[] { };
            INString = "Select * from Public_JobResult";
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            while (SQLReader.Read())
            {
                ReCust = ReCust.Concat(new string[] { SQLReader[1].ToString().Trim() }).ToArray();
            }
            INCommand.Connection.Close();


            for (ii = 0; ii <= dt.Rows.Count - 1; ii++)   // 判斷
            {
                string HK_Nu;
                if (DBNull.Value.Equals(dt.Rows[ii][35]))
                    HK_Nu = "";
                else
                {
                    HK_Nu = dt.Rows[ii][35].ToString();     // 管家編號
                    if ("ABCDEFGHIJKLMNOPQRSTUVWXYZ".Contains(HK_Nu.Substring(0, 1).ToUpper()) == false )
                        HK_Nu = "";
                }
                for (jj = 1; jj <= 31; jj++)   // 1-31日
                {
                    string tmpDate = Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/" + jj.ToString();
                    int tmpHKs = 0;
                    ShareFunction SF = new ShareFunction();
                    if (SF.IsDate(tmpDate))
                    {
                        if (!DBNull.Value.Equals(dt.Rows[ii][2 + jj]))
                        {
                            if (dt.Rows[ii][2 + jj].ToString() != "")
                            {
                                string tmpCustNo = "";
                                string tmpJobResult = "";
                                if ("ABCDEFGHIJKLMNOPQRSTUVWXYZ".Contains (dt.Rows[ii][2 + jj].ToString().Substring(0, 1)) == false  )   
                                {
                                    //如果不是客戶編號
                                    tmpCustNo = "";
                                    tmpJobResult = dt.Rows[ii][2 + jj].ToString();
                                }
                                else
                                {
                                    tmpCustNo = dt.Rows[ii][2 + jj].ToString();
                                    tmpJobResult = "";
                                }
                                if (tmpCustNo.Length > 8)
                                    tmpCustNo = tmpCustNo.Remove(4, tmpCustNo.Length - 8);
                                // Dim tmpPrice As Integer = 0
                                // If HT >= 2 Then   '一周一次以上
                                // If CDate(tmpDate).DayOfWeek = 6 Or CDate(tmpDate).DayOfWeek = 0 Then   '星期六、日價格
                                // tmpPrice = MyDataSet.Tables("Branch_Charge").DefaultView.Item(0)(2)
                                // Else
                                // tmpPrice = MyDataSet.Tables("Branch_Charge").DefaultView.Item(0)(1)
                                // End If
                                // Else              '二周一次
                                // If CDate(tmpDate).DayOfWeek = 6 Or CDate(tmpDate).DayOfWeek = 0 Then   '星期六、日價格
                                // tmpPrice = MyDataSet.Tables("Branch_Charge").DefaultView.Item(0)(4)
                                // Else
                                // tmpPrice = MyDataSet.Tables("Branch_Charge").DefaultView.Item(0)(3)
                                // End If
                                // End If
                                INString = "Select ID from Cust_Job_Schedule where Job_Date = '" + tmpDate + "' and Cust_No = '" + tmpCustNo +
                                           "' and Job_Result_ID = " + (Array.IndexOf(ReCust, tmpJobResult) + 1).ToString() +
                                           " and Job_Time = " + ((dt.Rows[ii][2].ToString() == "上") ? 0 : 1) +
                                           " and Staff_No= '" + HK_Nu + "'";
                                INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                                INCommand.Connection.Open();
                                SQLReader = INCommand.ExecuteReader();
                                if (SQLReader.Read()==false)
                                {
                                    INCommand.Connection.Close();
                                    if (tmpCustNo != "")
                                    {
                                        // 先算出是否有相同日期/時段/客戶名稱的資料
                                        INString = "Select count(ID) from Cust_Job_Schedule group by Cust_No,Job_Date,Job_Time having Job_Date = '" +
                                                   tmpDate + "' and Cust_No = '" + tmpCustNo + "' and  Job_Time = " + ((dt.Rows[ii][2].ToString() == "上")? 0 : 1);
                                        INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                                        INCommand.Connection.Open();
                                        SQLReader = INCommand.ExecuteReader();
                                        if (SQLReader.Read() == true)
                                            tmpHKs = Convert.ToInt32 (SQLReader[0]) + 1;
                                        else
                                            tmpHKs = 1;
                                        INCommand.Connection.Close();

                                        // 利用日期及客戶編號找出資料填入管家資料(找出沒有安排管家的第一筆)
                                        INString = "Select top 1 ID,HK_Serial from Cust_Job_Schedule where Job_Date = '" + tmpDate + "' and Cust_No = '" + 
                                                   tmpCustNo + "' and (staff_No is null or staff_No = '') order by HK_Serial";
                                        INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                                        INCommand.Connection.Open();
                                        SQLReader = INCommand.ExecuteReader();
                                        if (SQLReader.Read() == true)
                                        {
                                            INString1 = "Update Cust_Job_Schedule set Job_Time = " + ((dt.Rows[ii][2].ToString() == "上") ? 0 : 1) + 
                                                        ",Staff_No= '" + HK_Nu + "' where id = " + SQLReader[0];
                                            tmpHKs = DBNull.Value.Equals(SQLReader[1]) ? 0 : Convert.ToInt32(SQLReader[1]);
                                            INCommand.Connection.Close();
                                            INCommand = new System.Data.SqlClient.SqlCommand(INString1, INConnection1);
                                            INCommand.Connection.Open();
                                            INCommand.ExecuteNonQuery();
                                        }
                                        else
                                        {
                                        }
                                        // TextBox2.Text = TextBox2.Text & INString1 & Chr(13) & Chr(10)
                                        // INCommand.Connection.Close()
                                        // INCommand = New Data.SqlClient.SqlCommand(INString1, INConnection1)
                                        // INCommand.Connection.Open()
                                        // INCommand.ExecuteNonQuery()
                                        INCommand.Connection.Close();
                                    }
                                    else
                                    {
                                        // 請假
                                        INString = "Select top 1 ID,HK_Serial from Cust_Job_Schedule where Job_Date = '" + tmpDate + 
                                                   "' and Cust_No = ''  and Job_Time = " + ((dt.Rows[ii][2].ToString() == "上") ? 0 : 1) + " and Staff_No= '" +
                                                   HK_Nu + "'";
                                        INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                                        INCommand.Connection.Open();
                                        SQLReader = INCommand.ExecuteReader();
                                        if (SQLReader.Read() == true)
                                        {
                                            INString1 = "Update Cust_Job_Schedule set Job_Result_ID = " + (Array.IndexOf(ReCust, tmpJobResult) + 1).ToString() +
                                                        "  where id = " +  SQLReader[0];
                                            tmpHKs = DBNull.Value.Equals(SQLReader[1]) ? 0 : Convert.ToInt32(SQLReader[1]);
                                        }
                                        else
                                        {
                                            INString1 = "insert into  Cust_Job_Schedule (Job_Date,Job_Time,Staff_No,Cust_No,Job_Result_ID,Job_Price,Job_Flag," +
                                                        "Job_Status,HK_Serial," + "Contract_ID) Values ( '" + tmpDate + "'," +
                                                        ((dt.Rows[ii][2].ToString() == "上") ? 0 : 1) + ",'" + HK_Nu + "',''," +
                                                        (Array.IndexOf(ReCust, tmpJobResult) + 1).ToString() + ",0,1,null,null,0)";
                                            tmpHKs = 1;
                                        }
                                        // TextBox2.Text = TextBox2.Text & INString1 & Chr(13) & Chr(10)
                                        INCommand.Connection.Close();
                                        INCommand = new System.Data.SqlClient.SqlCommand(INString1, INConnection1);
                                        INCommand.Connection.Open();
                                        INCommand.ExecuteNonQuery();
                                        INCommand.Connection.Close();
                                    }
                                }
                                INCommand.Connection.Close();
                            }
                        }
                    }
                }
            }
            Label2.Text = Label2.Text + "-" + DateTime.Now.ToLongTimeString();

            System.Data.DataTable ds = (DataTable)Session["ds"];
            DataGrid3.DataSource = ds.DefaultView;
            DataGrid3.CurrentPageIndex = (Session["Pa"]==null) ? 1 : Convert.ToInt32 (Session["Pa"]);
            DataGrid3.DataBind();
            Page_Load(sender, e);
        }


        protected void DataGrid_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
        {
            if (Session["ds"] == null)
                Check_Report_Click(sender, e);

            System.Data.DataTable ds = (DataTable)Session["ds"];
            DataGrid3.DataSource = ds.DefaultView;
            DataGrid3.CurrentPageIndex = e.NewPageIndex;
            DataGrid3.DataBind();

            Session["Pa"] = e.NewPageIndex;
            Page_Load(sender, e);
        }

        protected void Change_Screen(object sender, EventArgs e)
        {
            int CS, Ci;
            System.Data.DataTable dt = (DataTable)Session["dt"];
            System.Data.DataTable ds;

            if (Session["dt"] == null)
                Check_Report_Click(sender, e);

            if (Session["CS"] == null)
            {
                if (Convert.ToInt32(Check_Year.SelectedValue) == DateTime.Now.Year & Convert.ToInt32(Check_Month.SelectedValue) == DateTime.Now.Month)
                    CS = DateTime.Now.Day - 2;
                else
                    CS = 1;
            }
            else
                CS = Convert.ToInt32(Session["CS"]);

            if (((Control)sender).ID  == "Left_Screen")
                CS = CS - 1;
            if (((Control)sender).ID == "Right_Screen")
                CS = CS + 1;

            int Mi = 31;
            // 找出該月的最後一天
            ShareFunction SF = new ShareFunction();
            while (!SF.IsDate(Check_Year.SelectedValue + "/" +  Check_Month.SelectedValue + "/" + Mi.ToString()))
                Mi = Mi - 1;
            // 如果顯示 14 欄位 超出最後一天
            if (CS + 13 >= Mi)
                CS = Mi - 13;
            if (CS < 1)
                CS = 1;
            Session["CS"] = CS;

            ds = new System.Data.DataTable();
            ds.Columns.Add(new System.Data.DataColumn("ID", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("HK_Name", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("Day_ID", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B1", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B2", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B3", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B4", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B5", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B6", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B7", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B8", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B9", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B10", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B11", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B12", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B13", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B14", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("RN", typeof(int)));

            // 拷貝14欄位到 ds
            for (Mi = 0; Mi <= dt.DefaultView.Count - 1; Mi++)
            {
                dr = ds.NewRow();
                dr[0] = dt.Rows[Mi][0];
                dr[1] = dt.Rows[Mi][1];
                dr[2] = dt.Rows[Mi][2];
                dr[17] = dt.Rows[Mi][34];


                for (Ci = 0; Ci <= 13; Ci++)
                    dr[Ci + 3] = dt.Rows[Mi][CS + Ci + 2];
                ds.Rows.Add(dr);
            }

            DataGrid3.DataSource = ds.DefaultView;
            DataGrid3.DataBind();
            Label5.Text = CS.ToString();
            Session["ds"] = ds;

            Page_Load(sender, e);
        }
    }

}