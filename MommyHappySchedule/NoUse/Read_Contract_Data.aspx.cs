﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Models;
using Models.Library;

namespace MommyHappySchedule.DataRebuilt
{
    public partial class Read_Contract_Data : System.Web.UI.Page
    {
        private System.Data.DataTable dt, dt1, dt2;
        private System.Data.DataRow dr;
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        System.Data.SqlClient.SqlConnection INConnection = new System.Data.SqlClient.SqlConnection(ConnString);

        protected void Page_Load(object sender, System.EventArgs e)
        {
            Response.Redirect("/Index.aspx");
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            string[] FileArray1 = {"南高派工明細表", "中高派工明細表","北高派工明細表", "台南派工明細表","南台中派工明細表", "北台中派工明細表","新竹派工明細表",
                "桃園派工明細表", "中壢派工明細表", "板橋派工明細表", "旗艦派工明細表" };
            System.Data.SqlClient.SqlCommand INCommand;
            System.Data.SqlClient.SqlDataReader SQLReader;
            System.Data.DataSet MyDataSet1, MyDataSet;
            System.Data.SqlClient.SqlDataAdapter MyCommand1;


            MyDataSet1 = new System.Data.DataSet();
            MyCommand1 = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Branch_Data", INConnection);
            MyCommand1.Fill(MyDataSet1, "Branch");
            MyCommand1 = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Staff_Job_Data_View where (Job_title like '%服務員%' or Job_title like '%組長%'" +
                " or Job_title like '%金牌%') and Branch_Name= '" + MyDataSet1.Tables["Branch"].DefaultView[Convert.ToInt32(TextBox1.Text)][1] + "'", INConnection);
            MyCommand1.Fill(MyDataSet1, "Staff");
            MyDataSet = new System.Data.DataSet();
            MyCommand1 = new System.Data.SqlClient.SqlDataAdapter("SELECT *  FROM Holiday_Date", INConnection);
            MyCommand1.Fill(MyDataSet1, "Holiday_Date");
            MyCommand1 = new System.Data.SqlClient.SqlDataAdapter("SELECT *  FROM Branch_Charge_Data where Branch_ID=" + TextBox1.Text, INConnection);
            MyCommand1.Fill(MyDataSet1, "Branch_Charge");

            // TextBox2.Text = "SELECT *  FROM Branch_Charge_Data where Branch_ID=" & TextBox1.Text
            // Exit Sub

            DateTime StartTime = DateTime.Now;
            int LMD = 31;
            ShareFunction SF = new ShareFunction();
            while (!SF.IsDate(DateTime.Now.Year.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + LMD.ToString()))
                LMD = LMD - 1;
            DateTime EMDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, LMD);

            int endI;
            int ii, Ki;
            int OI, NI;
            OI = 0;
            NI = 0;

            endI = (Convert.ToInt32(TextBox1.Text) == 11) ? 8 : 1;

            if (Convert.ToInt32(TextBox1.Text) <= FileArray1.Length)
            {
                for (Ki = DateTime.Now.Month; Ki >= DateTime.Now.Month; Ki += -1)
                {
                    dt = new System.Data.DataTable();
                    dt.Columns.Add(new System.Data.DataColumn("Contract_Number", typeof(string)));
                    dt.Columns.Add(new System.Data.DataColumn("Service_Date", typeof(DateTime)));
                    dt.Columns.Add(new System.Data.DataColumn("Service_Result", typeof(string)));
                    dt.Columns.Add(new System.Data.DataColumn("Service_Period", typeof(int)));
                    dt.Columns.Add(new System.Data.DataColumn("Contract_Staff", typeof(string)));
                    dt.Columns.Add(new System.Data.DataColumn("HK_Serial", typeof(int)));
                    dt.Columns.Add(new System.Data.DataColumn("Job_Price", typeof(int)));


                    dt1 = new System.Data.DataTable(); 
                    dt1.Columns.Add(new System.Data.DataColumn("Contract_Number", typeof(string)));
                    dt1.Columns.Add(new System.Data.DataColumn("Charge_Staff", typeof(string)));
                    dt1.Columns.Add(new System.Data.DataColumn("End_Date", typeof(string)));
                    dt1.Columns.Add(new System.Data.DataColumn("Contract_Times", typeof(string)));
                    dt1.Columns.Add(new System.Data.DataColumn("Contract_Status", typeof(int)));

                    for (ii = Convert.ToInt32(TextBox1.Text); ii <= Convert.ToInt32(TextBox1.Text); ii++)
                    {

                        string String4 = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                                         ((DBMode == "Local") ? ConfigurationManager.AppSettings["FSLocal"] : ConfigurationManager.AppSettings["FSServer"]) +
                                         "派工明細表\\" +  DateTime.Now.Year.ToString("0000") + Ki.ToString("00") + FileArray1[ii - 1] +
                                         ".xlsx;Extended Properties='EXCEL 12.0;HDR=NO;IMEX=1'";
                        System.Data.OleDb.OleDbConnection INConnection4 = new System.Data.OleDb.OleDbConnection(String4);
                        System.Data.OleDb.OleDbDataAdapter MyCommand;
                        string Sheet_Name, Sheet_Name_1;

                        MyDataSet = new System.Data.DataSet();
                        // MyCommand1 = New System.Data.SqlClient.SqlDataAdapter("select * FROM ERP_Bonus", INConnection1)
                        // MyCommand1.Fill(MyDataSet, "ProductGroup")
                        INConnection4.Open();
                        dt2 = INConnection4.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new[] { null, null, null, "TABLE" });
                        Sheet_Name = "派工明細" + (DateTime.Now.Year - 1911).ToString("000") + "#" + Ki.ToString("0") + "$";
                        Sheet_Name_1 = "派工明細" + (DateTime.Now.Year - 1911).ToString("000") + "#" + Ki.ToString("00") + "$";
                        for (var cellNum = 0; cellNum <= dt2.Rows.Count - 1; cellNum++)
                        {
                            // TextBox2.Text = TextBox2.Text & dt2.Rows(cellNum).Item("TABLE_NAME")
                            if ((string)dt2.Rows[cellNum]["TABLE_NAME"] == Sheet_Name_1)
                            {
                                Sheet_Name = Sheet_Name_1;
                                break;
                            }
                        }
                        INConnection4.Close();

                        // Exit Sub
                        MyCommand = new System.Data.OleDb.OleDbDataAdapter("Select * from [" + Sheet_Name + "]' ", INConnection4);
                        MyCommand.Fill(MyDataSet, "Arrange_Job");


                        int Rw_Count, Cl_Count;
                        string tmpContract_Number = "";
                        string O_tmpContract_Number = "";
                        string tmpContract_Period = "";
                        string tmpService_Date, tmpService_Result;
                        DateTime tmpS_Date = DateTime.Now;
                        string tmpContract_Charge = "";
                        string tmpContract_Times;
                        string tmpContract_End_Time = "";
                        int tmpContract_Status = 0;
                        int tmpContract_Price = 0;
                        string[] tmpContract_Staff = { }; // 如果有二位管家同時服務
                        int CsameDate = 0; // 用來判斷有多少日期重複,表示要讀取第幾個管家資料
                        int HKSerial = 0; // same as up
                        string sameDate = DateTime.Now.ToShortDateString();

                        int R_S;

                        if (ii == 4 | ii == 7 | ii == 10)
                            R_S = 6;
                        else
                            R_S = 7;

                        for (Rw_Count = 6; Rw_Count <= MyDataSet.Tables["Arrange_Job"].Rows.Count - 1; Rw_Count++)
                        {
                            if (!DBNull.Value.Equals(MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][0]))
                            {
                                tmpContract_Number = (string)MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][0];
                                tmpContract_Charge = "";
                                tmpContract_Times = "0";
                                tmpContract_End_Time = "null";
                                tmpContract_Status = 0;
                                if (O_tmpContract_Number != tmpContract_Number)
                                {
                                    O_tmpContract_Number = tmpContract_Number;
                                    tmpContract_Staff = tmpContract_Staff.Where(str => str != "").ToArray();
                                }

                                if (!DBNull.Value.Equals(MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S]))
                                {
                                    // 讀取合約狀態
                                    if (!DBNull.Value.Equals(MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][1]))
                                    {
                                        if ((MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][1]).ToString().Trim() == "保留")
                                            tmpContract_Status = 2;
                                    }
                                    // 管家姓名
                                    if (!DBNull.Value.Equals(MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S - 5]))
                                        tmpContract_Staff = MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S - 5].ToString().Split('+');
                                    // 合約價格
                                    if (!DBNull.Value.Equals(MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S + 9 - 5]))
                                        if (MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S + 9 - 5].ToString().Contains(','))
                                            tmpContract_Price = Convert.ToInt32(MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S + 9 - 5]
                                             .ToString().Remove(MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S + 9 - 5].ToString().IndexOf(','), 1));
                                        else
                                            tmpContract_Price = Convert.ToInt32(MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S + 9 - 5].ToString());

                                    // 讀取工作星期及時段
                                    if (MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S].ToString().Contains("上") == true)
                                        tmpContract_Period = MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S].ToString().Substring(
                                            MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S].ToString().IndexOf('上') - 1, 2);
                                    else if (MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S].ToString().Contains("下") == true)
                                        tmpContract_Period = MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S].ToString().Substring(
                                            MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S].ToString().IndexOf('下') - 1, 2);
                                    if (MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S].ToString().Trim() != "")
                                    {
                                        // 讀取Charge_Staff_Name
                                        if (ii == 7 | ii == 10)
                                        {
                                            if (!DBNull.Value.Equals(MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S + 8 + 16]))
                                                tmpContract_Charge = MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S + 8 + 16].ToString().Trim();
                                        }
                                        else if (!DBNull.Value.Equals(MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S + 8 + 15]))
                                            tmpContract_Charge = MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S + 8 + 15].ToString().Trim();

                                        // 讀取Contract_E_Date
                                        if (ii == 4)
                                        {
                                            if (!DBNull.Value.Equals(MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S + 8 + 15 + 6]))
                                            {
                                                if (SF.IsDate(MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S + 8 + 15 + 6].ToString().Trim()))
                                                    tmpContract_End_Time = MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S + 8 + 15 + 6].ToString().Trim();
                                            }
                                        }
                                        else if (!DBNull.Value.Equals(MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S + 8 + 15 + 7]))
                                        {
                                            if (SF.IsDate(MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S + 8 + 15 + 7].ToString().Trim()))
                                                tmpContract_End_Time = MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S + 8 + 15 + 7].ToString().Trim();
                                        }

                                        // 讀取Contract_Times
                                        if (!DBNull.Value.Equals(MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S + 1]))
                                            tmpContract_Times = MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S + 1].ToString().Trim();

                                        dr = dt1.NewRow();
                                        //dr[0] = tmpContract_Number.ToString().Trim().Substring(0, 8);
                                        dr[0] = tmpContract_Number.ToString().Trim();
                                        dr[1] = tmpContract_Charge;
                                        dr[2] = tmpContract_End_Time;
                                        dr[3] = tmpContract_Times;
                                        dr[4] = tmpContract_Status;
                                        dt1.Rows.Add(dr);
                                    }
                                    message.Text = "OK";
                                }

                                for (Cl_Count = 1; Cl_Count <= 12; Cl_Count++)   // 服務日期欄位    R_S + 8 + Cl_Count
                                {
                                    if (DBNull.Value.Equals(MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S + 8 + Cl_Count]))
                                        break;

                                    tmpService_Date = MyDataSet.Tables["Arrange_Job"].DefaultView[Rw_Count][R_S + 8 + Cl_Count].ToString().Trim();
                                    if (SF.IsDate(tmpService_Date))
                                    {
                                        tmpService_Result = "";
                                    }
                                    else if (tmpService_Date.Length > 0)
                                    {
                                        if (SF.IsDate(tmpService_Date.Substring(0, tmpService_Date.Length - 1)))
                                        {
                                            tmpService_Result = tmpService_Date.Substring(tmpService_Date.Length - 1, 1);
                                            tmpService_Date = tmpService_Date.Substring(0, tmpService_Date.Length - 1);
                                        }
                                        else
                                            break;
                                    }
                                    else
                                        break;
                                    tmpS_Date = Convert.ToDateTime(tmpService_Date);
                                    // 判斷價格
                                    if (tmpContract_Times != "0.5")
                                    {
                                        tmpContract_Price = Convert.ToInt32(MyDataSet1.Tables["Branch_Charge"].DefaultView[0][1]);
                                        if (tmpS_Date.DayOfWeek == DayOfWeek.Saturday | tmpS_Date.DayOfWeek == DayOfWeek.Sunday)
                                            tmpContract_Price = Convert.ToInt32(MyDataSet1.Tables["Branch_Charge"].DefaultView[0][2]);
                                        else
                                        {
                                            MyDataSet1.Tables["Holiday_Date"].DefaultView.RowFilter = "H_Date = '" + tmpS_Date.ToShortDateString() + "'";
                                            if (MyDataSet1.Tables["Holiday_Date"].DefaultView.Count == 1)
                                                tmpContract_Price = Convert.ToInt32(MyDataSet1.Tables["Branch_Charge"].DefaultView[0][2]);
                                        }
                                    }
                                    else
                                    {
                                        tmpContract_Price = Convert.ToInt32(MyDataSet1.Tables["Branch_Charge"].DefaultView[0][3]);
                                        if (tmpS_Date.DayOfWeek == DayOfWeek.Saturday | tmpS_Date.DayOfWeek == DayOfWeek.Sunday)
                                            tmpContract_Price = Convert.ToInt32(MyDataSet1.Tables["Branch_Charge"].DefaultView[0][4]);
                                        else
                                        {
                                            MyDataSet1.Tables["Holiday_Date"].DefaultView.RowFilter = "H_Date = '" + tmpS_Date.ToShortDateString() + "'";
                                            if (MyDataSet1.Tables["Holiday_Date"].DefaultView.Count == 1)
                                                tmpContract_Price = Convert.ToInt32(MyDataSet1.Tables["Branch_Charge"].DefaultView[0][4]);
                                        }
                                    }
                                    if (tmpService_Date != sameDate)
                                    {
                                        sameDate = tmpService_Date;
                                        CsameDate = 0;
                                        HKSerial = 1;
                                    }
                                    else
                                    {
                                        CsameDate = CsameDate + 1;    // 如果讀取日期資料相同,則使用下一個管家名字
                                        HKSerial = HKSerial + 1;
                                    }

                                    dr = dt.NewRow();
                                    dr[0] = tmpContract_Number;
                                    dr[1] = Convert.ToDateTime(tmpService_Date);
                                    dr[2] = tmpService_Result;
                                    dr[3] = (tmpContract_Period.Contains("上") == true) ? 0 : 1;

                                    if (CsameDate > tmpContract_Staff.Length - 1)
                                        CsameDate = 0;
                                    dr[4] = "";
                                    if (tmpContract_Staff.Length > 0)
                                        if (tmpContract_Staff[CsameDate].Trim() != "")
                                        {
                                            MyDataSet1.Tables["Staff"].DefaultView.RowFilter = "Staff_Name like '%" + tmpContract_Staff[CsameDate] + "%'";
                                            // 找出管家編號
                                            if (MyDataSet1.Tables["Staff"].DefaultView.Count >= 1)
                                                dr[4] = MyDataSet1.Tables["Staff"].DefaultView[0][1].ToString();
                                        }
                                    dr[5] = HKSerial;
                                    dr[6] = tmpContract_Price;

                                    dt.Rows.Add(dr);
                                }
                            }
                        }
                    }
                    // Repeater1.DataSource = dt.DefaultView
                    // Repeater1.DataBind()

                    // DataGrid1.DataSource = dt1.DefaultView
                    // DataGrid1.DataBind()

                    // Exit Sub
                    // Delete  From [MommyHappy].[dbo].[Cust_Job_Schedule] Where Cust_No ='KS16-098'

                    string INString;
                    string tmpDate = "";
                    string tmpID = "";
                    string tmpContractNO = "";

                    DataView dv1 = new DataView(dt1)
                    {
                        RowFilter = ""
                    };
                    for (ii = 0; ii <= dv1.Count - 1; ii++)
                    {
                        if (!(dv1[ii][1].ToString() == "" & dv1[ii][2].ToString() == "null" & dv1[ii][3].ToString() == "0"))
                        {
                            // 沒有問題的資料

                            tmpContractNO = dv1[ii][0].ToString();
                            if (SF.IsDate(dv1[ii][2].ToString()))
                                tmpDate = "'" + dv1[ii][2].ToString() + "'";
                            else
                                tmpDate = "null";

                            INString = "Select * from Cust_Contract_Data where Cust_No = '" + tmpContractNO + "'";
                            // TextBox2.Text = TextBox2.Text & INString
                            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection);
                            INCommand.Connection.Open();
                            SQLReader = INCommand.ExecuteReader();
                            if (SQLReader.Read() == true)
                            {
                                //INString = "Update Cust_Contract_Data set Branch_Name = '" + MyDataSet1.Tables["Branch"].DefaultView[Convert.ToInt32(
                                //    TextBox1.Text)][1].ToString().Trim() + "',Charge_Staff_Name = '" + dv1[ii][1] + "',Contract_E_Date =" + tmpDate +
                                //    ",Contract_Status= " + dv1[ii][4] + ",Contract_U_Date = Getdate(),Contract_Times =" + dv1[ii][3] + " where Cust_No = '" +
                                //    dv1[ii][0] + "'";
                                INCommand.Connection.Close();
                            }
                            else
                            {
                                INString = "Insert into Cust_Contract_Data (Branch_Name,Cust_No,Charge_Staff_Name,Contract_E_Date,Contract_R_Date,Contract_Times," +
                                    "Branch_ID) Values ('" + MyDataSet1.Tables["Branch"].DefaultView[Convert.ToInt32(TextBox1.Text)][1].ToString().Trim() + "','" +
                                    dv1[ii][0] + "','" + dv1[ii][1] + "'," + tmpDate + "," + tmpDate + "," + dv1[ii][3] + "," + TextBox1.Text + " )";
                                // TextBox2.Text = TextBox2.Text & INString
                                INCommand.Connection.Close();
                                INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection);
                                INCommand.Connection.Open();
                                INCommand.ExecuteNonQuery();
                                INCommand.Connection.Close();
                                // TextBox1.Text = TextBox1.Text & INString & Chr(13) & Chr(10)
                                // End If
                                // INCommand.Connection.Close()

                                INString = "Select ID from Cust_Contract_Data where Cust_No = '" + tmpContractNO + "'";
                                // TextBox2.Text = TextBox2.Text & INString
                                INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection);
                                INCommand.Connection.Open();
                                SQLReader = INCommand.ExecuteReader();
                                if (SQLReader.Read() == true)
                                    tmpID = SQLReader[0].ToString();
                                INCommand.Connection.Close();

                                // Dim dv As DataView = New DataView(dt) With {
                                // .RowFilter = "Contract_Number = '" & tmpContractNO & "' and Service_Date >= '2018/1/1'"
                                // }
                                DataView dv = new DataView(dt)
                                {
                                    RowFilter = "Contract_Number = '" + tmpContractNO + "'"
                                };
                                // TextBox2.Text = TextBox2.Text & tmpContractNO & "-" & dv.Count.ToString & ","
                                for (var Rw_Count = 0; Rw_Count <= dv.Count - 1; Rw_Count++)
                                {
                                    INString = "Select * from Cust_Job_Schedule where Cust_No = '" + dv1[ii][0] + "' and Job_Date = '" +
                                        Convert.ToDateTime(dv[Rw_Count][1]).ToShortDateString() +
                                        "' and Job_Result = '" + dv[Rw_Count][2] + "' and Job_Time =" + dv[Rw_Count][3] + " and HK_Serial = " + dv[Rw_Count][5] +
                                        "  and Job_Price = " + dv[Rw_Count][6];

                                    // TextBox2.Text = TextBox2.Text & INString

                                    INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection);
                                    INCommand.Connection.Open();
                                    SQLReader = INCommand.ExecuteReader();
                                    if (SQLReader.Read() == false)
                                    {
                                        INCommand.Connection.Close();

                                        INString = "Select * from Cust_Job_Schedule where Cust_No = '" + dv1[ii][0] + "' and Job_Date = '" +
                                            Convert.ToDateTime(dv[Rw_Count][1]).ToShortDateString() +
                                            "'" + " and HK_Serial = " + dv[Rw_Count][5];

                                        // TextBox2.Text = TextBox2.Text & INString
                                        INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection);
                                        INCommand.Connection.Open();
                                        SQLReader = INCommand.ExecuteReader();
                                        if (SQLReader.Read() == true)
                                        {
                                            if ((DateTime)dv[Rw_Count][1] > EMDate)
                                                // 如果是未來月份,則寫入管家名字
                                                INString = "Update Cust_Job_Schedule set Job_Result = '" + dv[Rw_Count][2] + "',Staff_No = '" + dv[Rw_Count][4] +
                                                    "',Job_Time =" + dv[Rw_Count][3] + ", Job_Price =" + dv[Rw_Count][6] + " where id = " + SQLReader[0];
                                            else
                                                // 如果是本月以前月份,則空白
                                                INString = "Update Cust_Job_Schedule set Job_Result = '" + dv[Rw_Count][2] + "'" + ",Job_Time =" + dv[Rw_Count][3] +
                                                    ",Job_Price =" + dv[Rw_Count][6] + " where id = " + SQLReader[0];
                                            OI = OI + 1;
                                        }
                                        else
                                        {
                                            if ((DateTime)dv[Rw_Count][1] > EMDate)
                                                // 如果是未來月份,則寫入管家名字
                                                INString = "Insert into Cust_Job_Schedule (Job_Date,Job_Time,Cust_No,Job_Result,Staff_No,Contract_Id,HK_Serial," +
                                                    "Job_Price) Values ('" + Convert.ToDateTime(dv[Rw_Count][1]).ToShortDateString() + "'," + dv[Rw_Count][3] + ",'" + dv[Rw_Count][0] + "','" +
                                                    dv[Rw_Count][2] + "','" + dv[Rw_Count][4] + "'," + tmpID + "," + dv[Rw_Count][5] + "," + dv[Rw_Count][6] + ")";
                                            else
                                                // 如果是本月以前月份,則空白
                                                INString = "Insert into Cust_Job_Schedule (Job_Date,Job_Time,Cust_No,Job_Result,Contract_Id,HK_Serial,Job_Price) " +
                                                    "Values ('" + Convert.ToDateTime(dv[Rw_Count][1]).ToShortDateString() + "'," + dv[Rw_Count][3] + ",'" + dv[Rw_Count][0] + "','" + dv[Rw_Count][2] +
                                                    "'," + tmpID + "," + dv[Rw_Count][5] + "," + dv[Rw_Count][6] + ")";
                                            NI = NI + 1;
                                        }
                                        // TextBox2.Text = TextBox2.Text & INString & Chr(13) & Chr(10)
                                        INCommand.Connection.Close();
                                        INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection);
                                        INCommand.Connection.Open();
                                        INCommand.ExecuteNonQuery();
                                        INCommand.Connection.Close();
                                    }
                                    else
                                        INCommand.Connection.Close();
                                }
                            }
                        }
                    }
                    TextBox3.Text = OI.ToString() + "-" + NI.ToString();
                }
            }
            TextBox4.Text = ((TimeSpan)(DateTime.Now - StartTime )).ToString();

            Page_Load(sender, e);
        }
    }
}