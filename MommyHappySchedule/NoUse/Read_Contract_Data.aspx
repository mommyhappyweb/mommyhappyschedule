﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Read_Contract_Data.aspx.cs" Inherits="MommyHappySchedule.DataRebuilt.Read_Contract_Data" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
     <form id="form1" runat="server">
        <div>


            <table>
                <tr>
                    <td>
                        <h1>班表資料 </h1>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox><asp:Button ID="Button1" runat="server" Text="Button"  OnClick="Button1_Click"/>
                    </td>
                </tr>
                 <tr>
                    <td>
                        <asp:TextBox ID="TextBox4" runat="server" />
                    </td>
                </tr>
                 <tr>
                    <td>
                        <asp:TextBox ID="TextBox2" runat="server" TextMode="MultiLine" />
                    </td>
                </tr>
                 <tr>
                    <td>
                        <asp:TextBox ID="TextBox3" runat="server" TextMode="MultiLine" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" ID="message" Text="Test"></asp:Label>
                    </td>
                </tr>

                <asp:Repeater ID="Repeater1" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td class="auto-style2" bgcolor="#FFFF99">
                                <asp:Label ID="LBY_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Contract_Number") %>'></asp:Label>
                                <asp:Label ID="LBN_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Service_Date", "{0:yyyy/MM/dd}") %>'></asp:Label>
                                <asp:Label ID="LBT_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Service_Result") %>'></asp:Label>
                                <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Service_Period") %>'></asp:Label>
                                <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Contract_Staff") %>'></asp:Label>
                                <asp:Label ID="Label3" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "HK_Serial") %>'></asp:Label>
                                <asp:Label ID="Label4" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Job_Price") %>'></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>

                <tr>
                    <td>
                        <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="true" ItemStyle-Width="200">
                        </asp:DataGrid>
                    </td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>

            </table>


        </div>
    </form>
</body>
</html>
