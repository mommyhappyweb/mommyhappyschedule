﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.VisualBasic;
using System.Data;
using System.Configuration;
using Models.Library;
using System.Net;
using System.Web.Script.Serialization;
using System.Text;

namespace MommyHappySchedule.DataRebuilt
{
    public partial class ImportCustData : System.Web.UI.Page
    {

        private System.Data.DataTable dt1;
        private System.Data.DataRow dr;
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        System.Data.SqlClient.SqlConnection INConnection = new System.Data.SqlClient.SqlConnection(ConnString);
        System.Data.SqlClient.SqlConnection INConnection1 = new System.Data.SqlClient.SqlConnection(ConnString);
        static string String2 = "server=HM-ERPDB;database=MH;uid=sa;pwd=Mh@ht5357";
        System.Data.SqlClient.SqlConnection INConnection2 = new System.Data.SqlClient.SqlConnection(String2);

        protected void Page_Load(object sender, System.EventArgs e)
        {
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            string[] FileArray1 = { "高雄市", "屏東市", "高雄市", "高雄市", "台南市", "台中市", "台中市", "新竹市", "桃園市", "桃園市", "新北市", "高雄市" };
            string[] FileArray = {"客戶資料總檔-南高","客戶資料總檔-屏東","中高客戶資料總檔","北高客戶資料總檔","客戶資料總檔-台南","客戶資料總檔-南台中",
                                  "客戶資料總檔-北台中", "客戶資料總檔-新竹", "桃園客戶資料總檔", "中壢客戶資料總檔","板橋客戶資料總檔", "旗艦客戶資料總檔"  };

            System.Data.SqlClient.SqlCommand INCommand;
            System.Data.SqlClient.SqlDataReader SQLReader;
            System.Data.DataSet MyDataSet1, MyDataSet;
            System.Data.SqlClient.SqlDataAdapter MyCommand1;
            string String4;
            System.Data.OleDb.OleDbConnection INConnection4;
            System.Data.OleDb.OleDbDataAdapter MyCommand;
            DateTime StartTime = DateTime.Now;


            MyDataSet1 = new System.Data.DataSet();
            MyCommand1 = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Branch_Data", INConnection);
            MyCommand1.Fill(MyDataSet1, "Branch");

            //讀取房型資料表
            string[] House_Style = new string[] { };
            string[] House_Style1 = new string[] { };

            string INString = "SELECT * FROM Public_House_Style";
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            while (SQLReader.Read())
            {
                House_Style = House_Style.Concat(new string[] { SQLReader[1].ToString().Trim() }).ToArray();
                House_Style1 = House_Style.Concat(new string[] { SQLReader[2].ToString().Trim() }).ToArray();
            }
            INCommand.Connection.Close();

            //讀取客戶來源資料表
            string[] Cust_Source = new string[] { };
            INString = "Select * from Public_Cust_Source";
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            while (SQLReader.Read())
            {
                Cust_Source = Cust_Source.Concat(new string[] { SQLReader[1].ToString().Trim() }).ToArray();
            }
            INCommand.Connection.Close();


            int ii;
            int OI, NI;
            OI = 0;
            NI = 0;

            if (Convert.ToInt32(TextBox1.Text) <= FileArray.Length)
            {
                string tmpContractNO = "";
                DataView dv1;
                string FString;


                dt1 = new System.Data.DataTable();
                dt1.Columns.Add(new System.Data.DataColumn("Cust_No", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Cust_Name", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Cell_Phone", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("House_Phone", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Other_Phone", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Cust_Source", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("House_Style", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("House_Floors", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("House_Area", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Intern_Flag", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Shift_Flag", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Phone_S_Flag", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Text_Flag", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("SP_Time_Flag", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("In_House", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Key_Holder", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Favor_Staff", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Forbid_Staff", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Cust_Job", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Cust_Comp", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Cust_Addr", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Cust_Geometry", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Cust_Bulid", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Traff_Status", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Cust_Sp_Request", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Cust_Sp_Forbid", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("IDCard", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("BankNo", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("AccountNo", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Cust_Source_ID", typeof(int)));
                dt1.Columns.Add(new System.Data.DataColumn("House_Style_ID", typeof(int)));


                for (ii = Convert.ToInt32(TextBox1.Text); ii <= Convert.ToInt32(TextBox1.Text); ii++)
                //for (ii = 0; ii <= 11; ii++)
                {
                    {
                        string dblString = (DBMode == "Local") ? ConfigurationManager.AppSettings["FSLocal"] : ConfigurationManager.AppSettings["FSServer"];

                        FString = dblString + "人事基本資料\\" + FileArray[ii - 1] + ".xls";
                        String4 = "Provider=Microsoft.ACE.OLEDB.12.0;" + FString + ";Extended Properties='EXCEL 8.0;HDR=NO;IMEX=1'";
                        if (!System.IO.File.Exists(FString.Remove(0, 12)))
                        {
                            FString = dblString + "人事基本資料\\" + FileArray[ii - 1] + ".xlsx";
                            String4 = "Provider=Microsoft.ACE.OLEDB.12.0;" + FString + ";Extended Properties='EXCEL 12.0;HDR=NO;IMEX=1'";
                        }
                        INConnection4 = new System.Data.OleDb.OleDbConnection(String4);

                        // MyCommand1 = New System.Data.SqlClient.SqlDataAdapter("select * FROM ERP_Bonus", INConnection1)
                        // MyCommand1.Fill(MyDataSet, "ProductGroup")


                        MyDataSet = new System.Data.DataSet();
                        MyCommand = new System.Data.OleDb.OleDbDataAdapter("Select * from [客戶總檔$]' ", INConnection4);
                        MyCommand.Fill(MyDataSet, "Cust_Data");

                        int kk = 0;

                        for (kk = 2; kk <= MyDataSet.Tables["Cust_Data"].DefaultView.Count - 1; kk++)
                        {
                            if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][4]))
                            {
                                dr = dt1.NewRow();

                                dr[0] = MyDataSet.Tables["Cust_Data"].DefaultView[kk][4]; // Cust_No
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][5]))
                                    dr[1] = MyDataSet.Tables["Cust_Data"].DefaultView[kk][5].ToString().Trim().Replace("'", "");// Cust_Name
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][6]))
                                    dr[2] = MyDataSet.Tables["Cust_Data"].DefaultView[kk][6].ToString().Trim().Replace("'", "");// Cell_Phone
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][7]))
                                    dr[3] = MyDataSet.Tables["Cust_Data"].DefaultView[kk][7].ToString().Trim().Replace("'", "");// House_Phone
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][8]))
                                    dr[4] = MyDataSet.Tables["Cust_Data"].DefaultView[kk][8].ToString().Trim().Replace("'", "");// Other_Phone
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][25]))
                                {
                                    dr[5] = MyDataSet.Tables["Cust_Data"].DefaultView[kk][25].ToString().Replace("'", "");// Cust_Source
                                    dr[29] = (Array.IndexOf(Cust_Source, dr[5]) + 1);  // Cust_Source_ID
                                }
                                else
                                    dr[29] = 0;
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][26]))
                                {
                                    dr[6] = MyDataSet.Tables["Cust_Data"].DefaultView[kk][26].ToString().Trim().Replace("'", "");// House_Style
                                                                                                                                 // House_Style_ID  用 NAME判斷 或 SNAME判斷
                                    if (Array.IndexOf(House_Style, dr[6]) == -1)
                                        dr[30] = Array.IndexOf(House_Style1, dr[6]) + 1;
                                    else
                                        dr[30] = Array.IndexOf(House_Style, dr[6]) + 1;
                                }
                                else
                                    dr[30] = 0;
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][27]))
                                    dr[7] = MyDataSet.Tables["Cust_Data"].DefaultView[kk][27].ToString().Trim().Replace("'", "");// House_Floors
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][28]))
                                    dr[8] = MyDataSet.Tables["Cust_Data"].DefaultView[kk][28].ToString().Trim().Replace("'", "");// House_Area
                                dr[9] = "0";
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][29]))
                                {
                                    if (MyDataSet.Tables["Cust_Data"].DefaultView[kk][29].ToString() == "是")
                                        dr[9] = "1";    //Intern_Flag
                                }
                                dr[10] = "0";
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][30]))
                                {
                                    if (MyDataSet.Tables["Cust_Data"].DefaultView[kk][30].ToString() == "是")
                                        dr[10] = "1"; // Shift_Flag
                                }
                                dr[11] = "0";
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][31]))
                                {
                                    if (MyDataSet.Tables["Cust_Data"].DefaultView[kk][31].ToString() == "是")
                                        dr[11] = "1"; // Phone_S_Flag
                                }
                                dr[12] = "0";
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][32]))
                                {
                                    if (MyDataSet.Tables["Cust_Data"].DefaultView[kk][32].ToString() == "是")
                                        dr[12] = "1"; // Text_Flag
                                }
                                dr[13] = "0";
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][33]))
                                {
                                    if (MyDataSet.Tables["Cust_Data"].DefaultView[kk][33].ToString() == "是")
                                        dr[13] = "1"; // SP_Time_Flag
                                }
                                dr[14] = "0";
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][34]))
                                {
                                    if (MyDataSet.Tables["Cust_Data"].DefaultView[kk][34].ToString() == "是")
                                        dr[14] = "1"; // In_House
                                }
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][36]))
                                    dr[15] = MyDataSet.Tables["Cust_Data"].DefaultView[kk][36].ToString().Trim().Replace("'", "");// Key_Holder
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][37]))
                                    dr[16] = MyDataSet.Tables["Cust_Data"].DefaultView[kk][37].ToString().Trim().Replace("'", "");// Favor_Staff
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][38]))
                                    dr[17] = MyDataSet.Tables["Cust_Data"].DefaultView[kk][38].ToString().Trim().Replace("'", "");// Forbid_Staff
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][49]))
                                    dr[18] = MyDataSet.Tables["Cust_Data"].DefaultView[kk][49].ToString().Trim().Replace("'", "");// Cust_Job
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][50]))
                                    dr[19] = MyDataSet.Tables["Cust_Data"].DefaultView[kk][50].ToString().Trim().Replace("'", "");// Cust_Comp
                                FString = "";
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][55]))
                                {
                                    FString = MyDataSet.Tables["Cust_Data"].DefaultView[kk][55].ToString().Trim().Replace("'", ""); // Cust_Addr
                                    if (FString.Contains(FileArray1[ii - 1]) == false)
                                        FString = FileArray1[ii - 1] + FString;
                                    dr[20] = FString;
                                }
                                if (FString != "")
                                {
                                    //找出定位資訊
                                    WebClient webClient = new WebClient();
                                    webClient.Encoding = Encoding.UTF8;
                                    string JsonString;
                                    JavaScriptSerializer Serializer = new JavaScriptSerializer();
                                    Address ItemList;

                                    JsonString = webClient.DownloadString("https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCFAVGBNpJ4rdb5YP36TGGvVS0WEBEZMpc&address=" + FString);   // Gmail
                                                                                                                                                                                                 //JsonString = webClient.DownloadString("https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCg6PptSmUzNUUw-BZW5h5ZXSe4LI8SMyU&address=" + FString);   // Mommyhappy
                                    ItemList = Serializer.Deserialize<Address>(JsonString);

                                    if (ItemList.status == "OK")
                                    {
                                        // Label1.Text = ItemList.results(0).geometry.location.lat & "," & ItemList.results(0).geometry.location.lng
                                        dr[21] = ItemList.results[0].geometry.location.lat + "," + ItemList.results[0].geometry.location.lng;
                                    }
                                }
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][56]))
                                    dr[22] = MyDataSet.Tables["Cust_Data"].DefaultView[kk][56].ToString().Trim().Replace("'", "");// Cust_Bulid
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][57]))
                                    dr[23] = MyDataSet.Tables["Cust_Data"].DefaultView[kk][57].ToString().Trim().Replace("'", "");// Traff_Status
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][58]))
                                    dr[24] = MyDataSet.Tables["Cust_Data"].DefaultView[kk][58].ToString().Trim().Replace("'", "");// Cust_Sp_Request
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][35]))
                                    dr[25] = MyDataSet.Tables["Cust_Data"].DefaultView[kk][35].ToString().Trim().Replace("'", "");// Cust_Sp_Forbid
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][53]))
                                    dr[26] = MyDataSet.Tables["Cust_Data"].DefaultView[kk][53].ToString().Trim().Replace("'", "");// IDCard
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][51]))
                                    dr[27] = MyDataSet.Tables["Cust_Data"].DefaultView[kk][51].ToString().Trim().Replace("'", "");// BankNo
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Cust_Data"].DefaultView[kk][52]))
                                    dr[28] = MyDataSet.Tables["Cust_Data"].DefaultView[kk][52].ToString().Trim().Replace("'", "");// AccountNo

                                dt1.Rows.Add(dr);
                            }
                        }
                    }

                    // Repeater1.DataSource = dt.DefaultView
                    // Repeater1.DataBind()

                    DataGrid1.DataSource = dt1.DefaultView;
                    DataGrid1.DataBind();

                    // Exit Sub

                    dv1 = new DataView(dt1);

                    // TextBox2.Text = dv1.Count

                    for (ii = 0; ii <= dv1.Count - 1; ii++)
                    {
                        int ti;
                        var il = new[] { 10, 10, 12, 30, 30, 1, 1, 4, 10, 0, 0, 0, 0, 0, 0, 20, 20, 20, 20, 20, 70, 30, 20, 30, 0, 0, 10, 10, 20 };
                        for (ti = 0; ti <= 28; ti++)
                        {
                            if (il[ti] != 0)
                            {
                                // 拮取資料庫長度
                                if (!DBNull.Value.Equals(dv1[ii][ti]))
                                    dv1[ii][ti] = dv1[ii][ti].ToString().Substring(0, (il[ti] < dv1[ii][ti].ToString().Length) ? il[ti] : dv1[ii][ti].ToString().Length);
                            }
                        }

                        tmpContractNO = dv1[ii][0].ToString();

                        INString = "Select * from Cust_Data where Cust_No = '" + tmpContractNO + "'";
                        // TextBox2.Text = TextBox2.Text & INString & Chr(13) & Chr(10)
                        INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection);
                        INCommand.Connection.Open();
                        SQLReader = INCommand.ExecuteReader();
                        if (SQLReader.Read())
                        {
                            INString = "UPDATE Cust_Data  SET " +
                                       "  Cust_Name = '" + dv1[ii][1] + "',Cell_Phone = '" + dv1[ii][2] + "',House_Phone = '" + dv1[ii][3] +
                                       "',Other_Phone = '" + dv1[ii][4] + "',Cust_Source = " + dv1[ii][29] + ",House_Style = " + dv1[ii][30] +
                                       " ,House_Floors = '" + dv1[ii][7] + "',House_Area = '" + dv1[ii][8] + "',Intern_Flag = '" + dv1[ii][9] +
                                       "',Shift_Flag = '" + dv1[ii][10] + "',Phone_S_Flag = '" + dv1[ii][11] + "',Text_Flag = '" + dv1[ii][12] +
                                       "',SP_Time_Flag = '" + dv1[ii][13] + "',In_House = '" + dv1[ii][14] + "',Key_Holder = '" + dv1[ii][15] +
                                       "',Favor_Staff = '" + dv1[ii][16] + "',Forbid_Staff = '" + dv1[ii][17] + "',Cust_Job = '" + dv1[ii][18] +
                                       "',Cust_Comp = '" + dv1[ii][19] + "',Cust_Addr = '" + dv1[ii][20] + "',Cust_Geometry = '" + dv1[ii][21] +
                                       "',Cust_Bulid = '" + dv1[ii][22] + "',Traff_Status = '" + dv1[ii][23] + "',Cust_Sp_Request = '" + dv1[ii][24] +
                                       "',Cust_Sp_Forbid = '" + dv1[ii][25] + "',IDCard = '" + dv1[ii][26] + "',BankNO = '" + dv1[ii][27] +
                                       "',AccountNo = '" + dv1[ii][28] + "' WHERE Cust_No ='" + dv1[ii][0] + "'";
                            OI = OI + 1;
                        }
                        else
                        {
                            INString = "Insert into Cust_Data (Cust_No,Cust_Name,Cell_Phone,House_Phone,Other_Phone,Cust_Source,House_Style,House_Floors" +
                                       ",House_Area,Intern_Flag,Shift_Flag,Phone_S_Flag,Text_Flag,SP_Time_Flag,In_House,Key_Holder,Favor_Staff,Forbid_Staff" +
                                       ",Cust_Job,Cust_Comp,Cust_Addr,Cust_Geometry,Cust_Bulid,Traff_Status,Cust_Sp_Request,Cust_Sp_Forbid,IDCard,BankNO,AccountNo)" +
                                       " Values ('" + dv1[ii][0] + "','" + dv1[ii][1] + "','" + dv1[ii][2] + "','" + dv1[ii][3] + "','" + dv1[ii][4] + "'," +
                                       dv1[ii][29] + "," + dv1[ii][30] + ",'" + dv1[ii][7] + "','" + dv1[ii][8] + "','" + dv1[ii][9] + "','" + dv1[ii][10] + "','" +
                                       dv1[ii][11] + "','" + dv1[ii][12] + "','" + dv1[ii][13] + "','" + dv1[ii][14] + "','" + dv1[ii][15] + "','" + dv1[ii][16] + "','" +
                                       dv1[ii][17] + "','" + dv1[ii][18] + "','" + dv1[ii][19] + "','" + dv1[ii][20] + "','" + dv1[ii][21] + "','" + dv1[ii][22] + "','" +
                                       dv1[ii][23] + "','" + dv1[ii][24] + "','" + dv1[ii][25] + "','" + dv1[ii][26] + "','" + dv1[ii][27] + "','" + dv1[ii][28] + "')";
                            NI = NI + 1;
                        }
                        // TextBox2.Text = TextBox2.Text & INString & vbCrLf
                        INCommand.Connection.Close();
                        INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection);
                        INCommand.Connection.Open();
                        INCommand.ExecuteNonQuery();
                        INCommand.Connection.Close();


                        //// 寫入ERP資料庫
                        //INString = "Select * from COPMA where MA001 = '" + tmpContractNO + "'";
                        //// TextBox2.Text = TextBox2.Text & INString & Chr(13) & Chr(10)
                        //INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
                        //INCommand.Connection.Open();
                        //SQLReader = INCommand.ExecuteReader();
                        //if (SQLReader.Read())
                        //{
                        //    INString = "UPDATE COPMA   SET MA003 = '" + dv1[ii][19] + "',MA006 = '" + dv1[ii][2] + "'" + ",MA002 = '" + dv1[ii][1] + "',MA015 = '" +
                        //               dv1[ii][0].ToString().Substring(0, 2) + "'" + ",MA007 = '" + dv1[ii][3] + "',MA008 = '" + dv1[ii][4].ToString() +
                        //               "',MA017 = '" + dv1[ii][5] + "'" + ",MA023 = '" + dv1[ii][20] + "'" + ",MA024 = '" + dv1[ii][22] + 
                        //               "'  WHERE MA001 ='" + dv1[ii][0] + "'";
                        //    OI = OI + 1;
                        //}
                        //else
                        //{
                        //    INString = "Insert into  COPMA (COMPANY,CREATOR,CREATE_DATE,FLAG,CREATE_TIME,MA001,MA002,MA003,MA006,MA007,MA008,MA011,MA012,MA013," +
                        //               "MA014,MA105,MA016" + ",MA023,MA025,MA027,MA032,MA033,MA034,MA035,MA036,MA037,MA038,MA039,MA041,MA042,MA046,MA048,MA059," +
                        //               "MA060,MA061,MA066,MA067,MA071,MA075" + ",MA082,MA084,MA086,MA087,MA088,MA089,MA090,MA091,MA092,MA093,MA094,MA095,MA096," +
                        //               "MA097,MA099,MA102,MA103,MA109,MA112,MA118,MA119,MA120" + ",MA123,MA127,MA132,MA133,MA135,MA137,MA141,MA142,MA143,MA144," +
                        //               "MA145,MA150) Values ('MH','DS','" + DateTime.Now.ToString("yyyyMMdd") + "',1,'" + DateTime.Now.ToString("HH:mm:ss") + 
                        //               "','" + dv1[ii][0] + "','" + dv1[ii][1] + "','" + dv1[ii][19] + "','" + dv1[ii][2] + "'" + ",'" + dv1[ii][3] + "','" +
                        //               dv1[ii][4] + "',0,0,0,'NT$','','','" + dv1[ii][20] + "','" + dv1[ii][20] + "','" + dv1[ii][20] + "'," + 
                        //               "'N',0,0,'1',0,'7','2','1','1','1','','1',0,0,0,'Y',0,'','142','N','N','N','1','1','1',0,0,0,0,0,0,'1','N','" +
                        //               dv1[ii][20] + "',0,'N','1','0000','S21','N','1','N',0,'1','2','N','" + (dv1[ii][1].ToString().Contains("公司") ? "2" : "1") +
                        //               "','1','Y','N','Y','0','1')";
                        //    NI = NI + 1;
                        //}
                        //TextBox2.Text = TextBox2.Text + INString + "\n";
                        //INCommand.Connection.Close();
                        //INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
                        //INCommand.Connection.Open();
                        //INCommand.ExecuteNonQuery();
                        //INCommand.Connection.Close();
                    }

                    TextBox3.Text = OI.ToString() + "-" + NI.ToString();
                }
                TextBox4.Text = ((TimeSpan)(DateTime.Now - StartTime)).ToString();

                Page_Load(sender, e);
            }

        }
    }
}