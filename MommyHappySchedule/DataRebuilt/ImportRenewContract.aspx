﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportRenewContract.aspx.cs" Inherits="MommyHappySchedule.DataRebuilt.ImportRenewContract" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>派工明細表</title>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                <tr>
                <td>駐點：</td>
                <td>                    <asp:Button ID="Button1" runat="server" Text="Button"  OnClick="Button1_Click"/>

                    <asp:DropDownList ID="Branch" runat="server">
                    </asp:DropDownList>
                </td>
                <td >年份</td>
                <td>
                    <asp:DropDownList ID="Check_Year" runat="server" Visible ="false" >
                        <asp:ListItem>2016</asp:ListItem>
                        <asp:ListItem Selected="True">2017</asp:ListItem>
                        <asp:ListItem>2018</asp:ListItem>
                        <asp:ListItem>2019</asp:ListItem>
                        <asp:ListItem>2020</asp:ListItem>
                        <asp:ListItem>2021</asp:ListItem>
                    </asp:DropDownList></td>
                <td>月份</td>
                <td>
                    <asp:DropDownList ID="Check_Month" runat="server"  Visible ="false" >
                        <asp:ListItem Selected="True">全年</asp:ListItem>
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                    </asp:DropDownList>
                </td>
                    </tr>
                <tr>
                    <td colspan ="12">
                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td colspan ="12">
                        <asp:TextBox ID="TextBox4" runat="server" />
                    </td>
                </tr>
                 <tr>
                    <td colspan ="12">
                        <asp:TextBox ID="TextBox2" runat="server" TextMode="MultiLine" />
                    </td>
                </tr>
                 <tr>
                    <td colspan ="12">
                        <asp:TextBox ID="TextBox3" runat="server" TextMode="MultiLine" />
                    </td>
                </tr>
                <tr>
                    <td colspan ="12">
                        <asp:Label runat="server" ID="message" Text="Test"></asp:Label>
                    </td>
                </tr>

                <asp:Repeater ID="Repeater1" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td class="auto-style2" bgcolor="#FFFF99">
                                <asp:Label ID="LBY_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "No") %>'></asp:Label>
                                <asp:Label ID="LBT_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Name") %>'></asp:Label>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>

                <tr>
                    <td colspan ="24">
                        <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="true" ItemStyle-Width="100">
                        </asp:DataGrid>
                    </td>
                </tr>
                 <tr>
                    <td colspan ="24">
                        <asp:DataGrid ID="DataGrid2" runat="server" AutoGenerateColumns="true" ItemStyle-Width="100">
                        </asp:DataGrid>
                    </td>
                </tr>

                <tr>
                    <td>&nbsp;</td>
                </tr>

            </table>


        </div>
    </form>
</body>
</html>