﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Data;
using System.Drawing;
using Models.Library;
using System.Configuration;

namespace MommyHappySchedule.DataRebuilt
{
    public partial class ImportPayment : System.Web.UI.Page
    {

        private System.Data.DataTable dt, dt2;
        private System.Data.DataRow dr;
        private System.Data.DataSet MyDataSet = new System.Data.DataSet();
        private System.Data.SqlClient.SqlDataAdapter MyCommand;
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        private static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        private System.Data.SqlClient.SqlConnection INConnection1 = new System.Data.SqlClient.SqlConnection(ConnString);
        private System.Data.SqlClient.SqlConnection INConnection2 = new System.Data.SqlClient.SqlConnection("server=60.248.240.187\\ERPDB,2899;database=MH;uid=sa;pwd=Mh@ht5357");
        private System.Data.SqlClient.SqlCommand INCommand;
        private System.Data.SqlClient.SqlDataReader SQLReader;
        //private string INString;



        protected void Page_Load(object sender, System.EventArgs e)
        {
            if ((string)Session["Groups"] != "1" & (string)Session["Groups"] != "2" &
               (string)Session["Groups"] != "3" & (string)Session["Groups"] != "4" & (string)Session["Groups"] != "5")
            {
                Response.Write("<script language=JavaScript>alert('無此權限!請洽IT部門');</script>");
                return;
            }

            if (!Page.IsPostBack)
            {
                MyDataSet = new System.Data.DataSet();
                MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Branch_Data where ID > 0 ", INConnection1);
                MyCommand.Fill(MyDataSet, "Branch");

                Branch.DataSource = MyDataSet.Tables["Branch"].DefaultView;
                Branch.DataTextField = "Branch_Name";
                Branch.DataValueField = "ID";
                Branch.DataBind();

            }
        }

        protected void Check_Report_Click(object sender, EventArgs e)
        {
            int Start_Branch, End_Branch;
            int Ki;

            TextBox1.Text = "";
            TextBox2.Text = "";

            Label2.Text = DateTime.Now.ToLongTimeString();

            Start_Branch = Branch.SelectedIndex;
            End_Branch = Branch.SelectedIndex;

            string[] FileArray =
            {
            "南高KS","中高KC","北高KN","台南TN","台中TC","北台中TX","新竹HC","桃園TU","中壢CL",
            "板橋BC","旗艦FS","屏東PT" };

            Ki = Convert.ToInt32(Branch.SelectedValue);

            //讀取未服務數
            string String4;
            String4 = "Provider=Microsoft.ACE.OLEDB.12.0;" + ((DBMode == "Local") ? ConfigurationManager.AppSettings["FSLocal"] :
                      ConfigurationManager.AppSettings["FSServer"]) + "派工明細表\\" + FileArray[Ki - 1].Substring(0, 2) +
                      "客戶未完成服務次數總表.xlsx;Extended Properties='EXCEL 12.0 Xml;HDR=NO;IMEX=1;'";

            System.Data.OleDb.OleDbConnection INConnection4 = new System.Data.OleDb.OleDbConnection(String4);
            System.Data.OleDb.OleDbCommand INCommand1;
            System.Data.OleDb.OleDbDataReader OleDbReader;

            dt = new System.Data.DataTable();
            dt.Columns.Add(new System.Data.DataColumn("Cust_No", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("No_S_Count", typeof(int)));
            dt.Columns.Add(new System.Data.DataColumn("Contract_Times", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("Note", typeof(string)));



            INCommand1 = new System.Data.OleDb.OleDbCommand("Select * from [工作表1$]", INConnection4);
            INCommand1.Connection.Open();
            OleDbReader = INCommand1.ExecuteReader();
            // Create more rows for the table.
            OleDbReader.Read();

            while (OleDbReader.Read() == true)
            {
                if (DBNull.Value.Equals(OleDbReader[0]) | DBNull.Value.Equals(OleDbReader[1]))
                    break;

                dr = dt.NewRow();
                dr[0] = OleDbReader[0].ToString();   //客戶編號
                dr[1] = Convert.ToInt16(OleDbReader[1].ToString());   //未服務次數
                dr[2] = OleDbReader[2].ToString();   //未服務次數

                dt.Rows.Add(dr);
            }
            INCommand1.Connection.Close();

            String4 = "Provider=Microsoft.ACE.OLEDB.12.0;" + ((DBMode == "Local") ? ConfigurationManager.AppSettings["FSLocal"] :
                      ConfigurationManager.AppSettings["FSServer"]) + "派工明細表\\" +
                      "授權扣款檔.xlsx;Extended Properties='EXCEL 12.0 Xml;HDR=NO;IMEX=1;'";

            INConnection4 = new System.Data.OleDb.OleDbConnection(String4);

            dt2 = new System.Data.DataTable();
            //收款資料表
            dt2.Columns.Add(new System.Data.DataColumn("CustNo", typeof(string)));
            dt2.Columns.Add(new System.Data.DataColumn("PayDate", typeof(System.DateTime)));
            dt2.Columns.Add(new System.Data.DataColumn("PayBank", typeof(int)));
            dt2.Columns.Add(new System.Data.DataColumn("PayText", typeof(string)));
            dt2.Columns.Add(new System.Data.DataColumn("PayAmt", typeof(int)));
            dt2.Columns.Add(new System.Data.DataColumn("PayTax", typeof(int)));


            INCommand1 = new System.Data.OleDb.OleDbCommand("Select * from [" + FileArray[Ki - 1] + "$]", INConnection4);
            INCommand1.Connection.Open();
            OleDbReader = INCommand1.ExecuteReader();
            // Create more rows for the table.
            OleDbReader.Read();

            while (OleDbReader.Read() == true)
            {
                if (DBNull.Value.Equals(OleDbReader[4]))   //若客戶編號為空白
                    break;
                if (!DBNull.Value.Equals(OleDbReader[11]))   //如果付款日期不是空白,而且沒有  "不" 及 "失敗" 表示扣款成功,則跳過
                    if (OleDbReader[11].ToString().Trim() != "" & OleDbReader[11].ToString().Contains("不") == false &
                        OleDbReader[11].ToString().Contains("失敗") == false)
                        continue;

                dr = dt2.NewRow();
                dr[0] = OleDbReader[4].ToString().Substring(4, 1) == "0" ?
                        OleDbReader[4].ToString().Substring(0, 4) + "-" + OleDbReader[4].ToString().Substring(5, 3) :
                        OleDbReader[4].ToString();   //客戶編號
                dr[1] = Convert.ToDateTime(OleDbReader[10].ToString()).AddYears(1911);   //扣款日期
                dr[2] = OleDbReader[0].ToString();   //扣款銀行
                Bank_No.Text = OleDbReader[0].ToString();
                dr[3] = OleDbReader[1].ToString();   //扣款帳號
                Account_No.Text = OleDbReader[1].ToString();
                //如果除以100不是整除,表示有含稅
                if (Convert.ToInt16(OleDbReader[3].ToString()) % 100 != 0)
                {
                    dr[4] = Convert.ToInt16(Convert.ToDouble(OleDbReader[3].ToString()) / 1.05);
                    dr[5] = Convert.ToInt16(OleDbReader[3].ToString()) - Convert.ToInt16(dr[4]);
                }
                else
                {
                    dr[4] = Convert.ToInt16(OleDbReader[3].ToString());
                    dr[5] = 0;
                }
                dt2.Rows.Add(dr);
            }
            INCommand1.Connection.Close();

            dt2.DefaultView.Sort = "CustNo ASC,PayDate ASC";

            string INString;
            string ToERP = "";

            for (int di = 0; di < dt.DefaultView.Count; di++)
            {
                INString = "Select ID,Cust_Name,Cust_No,Staff_No,HK_Nums,Contract_S_Date,Contract_E_Date,Contract_Times,Contract_Years,Branch_name,Note" +
                           " from Cust_Contract_Data_View where Cust_No = '" + dt.Rows[di][0].ToString() + "'";

                INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                INCommand.Connection.Open();
                SQLReader = INCommand.ExecuteReader();
                if (SQLReader.Read() == true)
                {
                    CustName.Text = DBNull.Value.Equals(SQLReader[1]) ? "" : SQLReader[1].ToString(); //客戶姓名
                    Cust_No.Text = DBNull.Value.Equals(SQLReader[2]) ? "" : SQLReader[2].ToString();   //客戶編號
                    ChargeStaff.Text = DBNull.Value.Equals(SQLReader[3]) ? "" : SQLReader[3].ToString();   //員工編號
                    HKNums.Text = DBNull.Value.Equals(SQLReader[4]) ? "" : SQLReader[4].ToString(); //管家人數
                    StDate.Text = DBNull.Value.Equals(SQLReader[5]) ? "" : Convert.ToDateTime(SQLReader[5].ToString()).ToShortDateString();   //合約起始日期
                    EnDate.Text = DBNull.Value.Equals(SQLReader[6]) ? "" : Convert.ToDateTime(SQLReader[6].ToString()).ToShortDateString();   //合約終止日期
                    ContractTimes.Text = DBNull.Value.Equals(SQLReader[7]) ? "0" : SQLReader[7].ToString();   //合約次數
                    ContractYears.Text = DBNull.Value.Equals(SQLReader[8]) ? "1" : SQLReader[8].ToString(); //Contract_Year
                    Contract_ID.Text = DBNull.Value.Equals(SQLReader[0]) ? "" : SQLReader[0].ToString();   //Contract_ID
                    Branch_Name.Text = DBNull.Value.Equals(SQLReader[9]) ? "" : SQLReader[9].ToString();   //Branch_Name
                    Note.Text = DBNull.Value.Equals(SQLReader[10]) ? "" : SQLReader[10].ToString();   //Note
                }
                INCommand.Connection.Close();

                //未付款期數
                System.Data.DataView dv = new System.Data.DataView(dt2)
                { RowFilter = "CustNo = '" + dt.Rows[di][0].ToString() + "'" };

                //如果未服務次數大於等於未收款次數才處理,否則為異常
                if (dv.Count > 0)
                    if (dv.Count * 4 * Convert.ToDouble(dt.Rows[di][2].ToString()) <= Convert.ToInt16(dt.Rows[di][1].ToString()))
                    {
                        WriteToERP(dv, dt.Rows[di]);
                        ToERP = ToERP + "'" + dt.Rows[di][0].ToString() + "',";
                        dt.Rows[di][3] = "成功!";
                    }
                    else
                    {
                        dt.Rows[di][3] = "未服務次數 小於 未收款次數!";
                    }
                else
                {
                    dt.Rows[di][3] = "找不到付款資料!";
                }
            }
            Label2.Text = Label2.Text + "-" + DateTime.Now.ToLongTimeString();
            if (ToERP.Length > 0)
            {
                ToERP = ToERP.Substring(0, ToERP.Length - 1);
                System.Data.DataView dv = new System.Data.DataView(dt)
                { RowFilter = "Cust_No not in (" + ToERP + ")" };
                foreach (DataRowView rw in dv)
                    TextBox1.Text = TextBox1.Text + rw[0].ToString() + ",";

                dv = new System.Data.DataView(dt2)
                { RowFilter = "CustNo not in (" + ToERP  + ")" };
                foreach (DataRowView rw in dv)
                    TextBox2.Text = TextBox2.Text + rw[0].ToString() + ",";
            }
            GridView2.DataSource = dt.DefaultView;
            GridView2.DataBind();
            GridView3.DataSource = dt2.DefaultView;
            GridView3.DataBind();

        }

        protected void WriteToERP(DataView dv, DataRow Rw)
        {
            //儲存收款資料
            //如果曾經儲存過則不做
            string INString = "Select * from Cust_Contract_Pay where Contract_ID = " + Contract_ID.Text.Trim();

            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            if (SQLReader.Read() == true)
            {
                INCommand.Connection.Close ();
                return;
            }
            INCommand.Connection.Close();

            string[] FileArray2 = { "2303", "2301", "2304", "2302", "2201", "2202", "2102", "2101", "2104", "2103", "2306" };
            int BID = 0;
            string tmpCNo;
            int tmpPayID = 0;
            //單次價格 = 每期未付款金額 / ( 合約次數 * 4)
            int tmpFee = Convert.ToInt16(Convert.ToDouble(dv[0][4]) / (Convert.ToDouble(Rw[2]) * 4));
            int tmpTax = Convert.ToInt16(Convert.ToDouble(dv[0][5]) / (Convert.ToDouble(Rw[2]) * 4));
            int tmpTotalFee = Convert.ToInt16(Convert.ToInt16(tmpFee) * Convert.ToInt16(Rw[1]));
            int tmpTotalTax = Convert.ToInt16(Convert.ToInt16(tmpTax) * Convert.ToInt16(Rw[1]));
            int firstFee = tmpTotalFee - Convert.ToInt16(dv.Count * Convert.ToDouble(Rw[2]) * 4 * tmpFee);
            int firstTax = tmpTax - Convert.ToInt16(dv.Count * Convert.ToDouble(Rw[2]) * 4 * tmpTax);
            ShareFunction SF = new ShareFunction();
            string tmpNumber;

            //新增一筆記錄到 dv 
            DataRowView dr = dv.AddNew();
            dr[0] = dv[0][0];
            dr[1] = DateTime.Now.ToShortDateString();
            dr[2] = dv[0][2];
            dr[3] = dv[0][3];
            dr[4] = firstFee;
            dr[5] = firstTax;

            dr.EndEdit();

            dv.Sort = "PayDate Asc";


            //收款期數 為未收款期數 + 1
            string tmpPayNumber = dv.Count.ToString();

            INString = "Insert into Cust_Contract_Pay (Contract_ID,Cust_No,C_Pay_Numbers,Payment_Type,Out_Bank,Out_Acct_No," +
                       "C_Pay_Date,C_Pay_Amt,Payment_memo,C_Pay_Tax,C_Tax_Flag) values ('" + Contract_ID.Text + "','" +
                       Cust_No.Text + "'," + tmpPayNumber + ",3,'" + SF.NSubString(Bank_No.Text.Trim(), 0, 10) + "','" +
                       SF.NSubString(Account_No.Text.Trim(), 0, 20) + "',getdate()," + tmpTotalFee + ",''," +
                       tmpTotalTax + "," + ((tmpTotalTax == 0) ? "0" : "1") + ")";

            INCommand.Connection.Close();
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            INCommand.ExecuteNonQuery();
            INCommand.Connection.Close();

            //讀取收款單號
            INString = "Select ID from Cust_Contract_Pay where Contract_ID = " + Contract_ID.Text.Trim();
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
            INCommand.Connection.Close();
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            //如果有找到資料
            if (SQLReader.Read() == true)
                tmpPayID = Convert.ToInt32(SQLReader[0]);
            INCommand.Connection.Close();

            //寫入交易收款資料表
            for (int vi = 0; vi < dv.Count; vi++)    //收款方式記錄
            {

                INString = "Insert into Cust_Contract_Pay_Detail (Pay_ID,Pay_Number,Payment_Type,Out_Bank,Out_Acct_No,B_Pay_Date,Amount," +
                           "Amt,Sales_Memo,FulFill,R_Pay_Date,Tax) values (" + tmpPayID.ToString() + "," + (vi + 1).ToString() + "," +
                           "3,'" + dv[vi][2] + "','" + dv[vi][3] + "','" + Convert.ToDateTime(dv[vi][1]).ToShortDateString() +
                           "',1," + dv[vi][4] + ",'" + ChargeStaff.Text.Trim() + "'," + ((vi == 0) ? "1" : "0") +
                           "," + (vi == 0 ? "'" + DateTime.Now.ToShortDateString() + "'" : "null") + "," +
                           dv[vi][5] + ")";

                INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                INCommand.Connection.Open();
                INCommand.ExecuteNonQuery();
                INCommand.Connection.Close();
            }

            //讀取部門代號
            INString = "Select top 1 Branch_ID from  Cust_Contract_Data where Cust_No = '" + Cust_No.Text.Trim() +
                       "' order by ID DESC";
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
            INCommand.Connection.Close();
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            //如果有找到資料
            if (SQLReader.Read() == true)
            {
                //INString = "Select ID from  Branch_Data where Branch_Name = '" + SQLReader[0].ToString().Trim() + "'";
                //INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                //INCommand.Connection.Close();
                //INCommand.Connection.Open();
                //SQLReader = INCommand.ExecuteReader();
                ////如果有找到資料
                //if (SQLReader.Read() == true)
                BID = Convert.ToInt32(SQLReader[0]) - 1;
            }
            INCommand.Connection.Close();


            //找出最後一筆訂單記錄編號
            string tmpONo = DateTime.Now.ToString("yyyyMMdd");
            INString = "Select top 1 TC002 from COPTC where TC001 = '2201' and TC002 like '" + tmpONo + "%' Order by TC002 DESC ";
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
            INCommand.Connection.Close();
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            //如果有找到資料
            if (SQLReader.Read() == true)
                tmpONo = SF.NSubString(SQLReader[0].ToString(), 0, 8) +
                         (Convert.ToInt32(SF.NSubString(SQLReader[0].ToString(), 8, 3)) + 1).ToString("000");
            else
                tmpONo = tmpONo + "001";
            INCommand.Connection.Close();
            string DN = (SF.NSubString(Cust_No.Text, 0, 2) == "PT") ? "KN" : SF.NSubString(Cust_No.Text, 0, 2);


            //訂單單頭
            INString = "Insert into COPTC (COMPANY,CREATOR,CREATE_DATE,FLAG,CREATE_TIME,TC001,TC002,TC003,TC004,TC005,TC006,TC007," +
                       "TC008,TC009,TC012,TC015,TC016,TC026,TC027,TC028,TC029,TC030,TC031,TC039,TC041,TC043,TC044,TC045,TC046,TC048," +
                       "TC050,TC052,TC053,TC056,TC060,TC068,TC069,TC070,TC077,TC078,TC091) Values ('MH','DS','" +
                       DateTime.Now.ToString("yyyyMMdd") + "',1,'" + DateTime.Now.ToString("HH:mm:ss") + "','2201','" + tmpONo +
                       "','" + SF.NSubString(tmpONo, 0, 8) + "','" + Cust_No.Text.Trim() + "','" + FileArray2[BID] + "','" + ChargeStaff.Text.Trim() + "'," +
                       "'0000','NT$','1','" + Contract_ID.Text.Trim() + "','','" + (tmpTotalTax != 0 ? "2" : "9") + "',0,'Y',0," +
                       tmpTotalFee + "," + tmpTotalTax + "," + tmpPayNumber + ",'" + SF.NSubString(tmpONo, 0, 8) + "'," +
                       (tmpTotalTax != 0 ? "0.05" : "0.00") + ",0,0,1,0,'N','N',0,'" + Cust_No.Text.Trim() +
                       "','1','N','1','0000','Y','N','" + (tmpTotalTax != 0 ? "S21" : "S17") + "','N')";

            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
            INCommand.Connection.Open();
            INCommand.ExecuteNonQuery();
            INCommand.Connection.Close();

            for (int vi = 0; vi < dv.Count; vi++)   //收款方式記錄
            {
                //訂單-訂金分批子單頭
                tmpNumber = "000" + (vi + 1).ToString();
                tmpNumber = SF.NSubString(tmpNumber, tmpNumber.Length - 4, 4);
                INString = "Insert into COPUC (COMPANY,CREATOR,CREATE_DATE,FLAG,CREATE_TIME,UC001,UC002,UC003,UC004,UC005,UC006,UC007)" +
                           " Values ('MH','DS','" + DateTime.Now.ToString("yyyyMMdd") + "',1,'" + DateTime.Now.ToString("HH:mm:ss") + "','2201','" + tmpONo +
                           "','" + tmpNumber + "',0," + dv[vi][4] + ",'" + Convert.ToDateTime(dv[vi][1]).ToString("yyyyMMdd") + "','N')";
                INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
                INCommand.Connection.Open();
                INCommand.ExecuteNonQuery();
                INCommand.Connection.Close();

                //訂單單身
                INString = "Insert into COPTD (COMPANY,CREATOR,CREATE_DATE,FLAG,CREATE_TIME,TD001,TD002,TD003,TD004,TD007,TD008,TD009,TD010,TD011,TD012,TD013" +
                           ",TD016," + "TD020,TD021,TD024,TD025,TD026,TD030,TD031,TD032,TD033,TD034,TD035,TD045,TD047,TD048,TD049,TD050,TD051,TD052,TD053,TD069" +
                           ",TD070,TD076,TD077,TD078,TD079,TD080) Values ('MH','DS','" + DateTime.Now.ToString("yyyyMMdd") + "',1,'" +
                           DateTime.Now.ToString("HH:mm:ss") + "','2201','" + tmpONo + "','" + tmpNumber + "','AA0101001','" +
                           DN + "'," + "1" + ",0,'件'," + dv[vi][4] + "," + Convert.ToInt32("1") * Convert.ToInt32(dv[vi][4]) +
                           ",'" + Convert.ToDateTime(dv[vi][1]).ToString("yyyyMMdd") + "','N','','Y',0,0,1,0,0,0,0,0,0,'9','" +
                           Convert.ToDateTime(dv[vi][1]).ToString("yyyyMMdd") + "','" + Convert.ToDateTime(dv[vi][1]).ToString("yyyyMMdd") +
                           "','1',0,0,0,0,'N'," + (tmpTotalTax != 0 ? "0.05" : "0.00") + "," + "1,'件',0,'" + (tmpTotalTax != 0 ? "S21" : "S17") + "',0)";
                INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
                INCommand.Connection.Open();
                INCommand.ExecuteNonQuery();
                INCommand.Connection.Close();
            }

            //回寫訂單編號到LOCAL資料庫
            INString = "Update Cust_Contract_Pay set Export_ID ='" + tmpONo + "' where ID = " + tmpPayID;
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            INCommand.ExecuteNonQuery();
            INCommand.Connection.Close();


            //找出最後一筆結帳單記錄編號
            tmpCNo = DateTime.Now.ToString("yyyyMMdd");
            INString = "Select top 1 TA002 from ACRTA where TA001 = '6401' and TA002 like '" + tmpCNo + "%' Order by TA002 DESC ";
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
            INCommand.Connection.Close();
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            //如果有找到資料
            if (SQLReader.Read() == true)
                tmpCNo = SQLReader[0].ToString();
            else
                tmpCNo = tmpCNo + "000";

            INCommand.Connection.Close();

            //只寫入第一筆記錄到結帳單               

            //編號 + 1
            tmpCNo = SF.NSubString(tmpCNo, 0, 8) + (Convert.ToInt32(SF.NSubString(tmpCNo, 8, 3)) + 1).ToString("000");

            //結帳單頭
            INString = "Insert into ACRTA (COMPANY,CREATOR,CREATE_DATE,FLAG,CREATE_TIME,TA001,TA002,TA003,TA004,TA005,TA006,TA007,TA008,TA009,TA010,TA011,TA012," +
                       "TA013,TA014,TA017,TA018,TA019,TA020,TA021,TA022,TA025,TA026,TA027,TA028,TA029,TA030,TA031,TA032,TA034,TA037,TA038,TA040,TA041,TA042,TA044," +
                       "TA045,TA046,TA047,TA048,TA049,TA053,TA054,TA055,TA058,TA066,TA068,TA073,TA083,TA093,TA101)" +
                       " Values ('MH','DS','" + DateTime.Now.ToString("yyyyMMdd") + "',1,'" + DateTime.Now.ToString("HH:mm:ss") + "','6401','" +
                       tmpCNo + "','" + SF.NSubString(tmpCNo, 0, 8) + "','" + Cust_No.Text.Trim() + "','" + ChargeStaff.Text.Trim() + "','0000','','','NT$',1,'" +
                       (tmpTotalTax != 0 ? "7" : "6") + "','" + (tmpTotalTax != 0 ? "2" : "9") + "','N','1',0,0,'N','" + SF.NSubString(tmpONo, 0, 8) +
                       "','" + SF.NSubString(tmpONo, 0, 8) + "','','N','N','N',0," + dv[0][4] + "," + dv[0][5] + ",0,'" + SF.NSubString(tmpCNo, 0, 6) +
                       "',0,0,'" + SF.NSubString(tmpCNo, 0, 8) + "'," + (tmpTotalTax != 0 ? "0.05" : "0.00") + "," + dv[0][4] + "," + dv[0][5] + ",'" +
                       SF.NSubString(tmpONo, 0, 8) + "','" + SF.NSubString(tmpONo, 0, 8) + "',0,0,'N','N','N',0,0,0,'" + (tmpTotalTax != 0 ? "S21" : "S17") +
                       "','N','N','0','N','N')";
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
            INCommand.Connection.Open();
            INCommand.ExecuteNonQuery();
            INCommand.Connection.Close();

            //結帳單身   
            //string tmpNumber = "000" + (vi+1).ToString();  
            //tmpNumber = tmpNumber.Substring(tmpNumber.Length - 4, 4);
            tmpNumber = "0001";

            INString = "Insert into ACRTB (COMPANY,CREATOR,CREATE_DATE,FLAG,CREATE_TIME,TB001,TB002,TB003,TB004,TB005,TB006,TB008,TB009,TB010,TB011,TB012,TB013," +
                       "TB014,TB015,TB017,TB018,TB019,TB020,TB021,TB023,TB029)" +
                       " Values ('MH','DS','" + DateTime.Now.ToString("yyyyMMdd") + "',1,'" + DateTime.Now.ToString("HH:mm:ss") + "','6401','" + tmpCNo +
                       "','" + tmpNumber + "','M','2201','" + tmpONo + "','" + Convert.ToDateTime(dv[0][1]).ToString("yyyyMMdd") + "'," +
                       dv[0][4] + ",0,'','N','2131',0,1," + dv[0][4] + "," + dv[0][5] + "," + dv[0][4] + "," +
                      dv[0][5] + ",'" + FileArray2[BID] + "','" + tmpNumber + "'," + (tmpTotalTax != 0 ? "0.05" : "0.00") + ")";
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
            INCommand.Connection.Open();
            INCommand.ExecuteNonQuery();
            INCommand.Connection.Close();

            //回寫最後一筆結帳單編號到LOCAL資料庫
            INString = "Update Cust_Contract_Pay set ERP_No ='" + tmpCNo + "' where ID = " + tmpPayID;
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            INCommand.ExecuteNonQuery();
            INCommand.Connection.Close();



            //收款單存入

            //此段應讀取ERP內資料檔,目前先寫定
            string tmpCode = "6301";
            string tmpBCode = "110311";
            //switch (tmpPayType.SelectedIndex.ToString().Trim())
            //{
            //    case "7":   //eDDA
            //    case "3":   //ACH
            //        tmpCode = "6301";
            //        tmpBCode = "110311";
            //        break;
            //    case "4":   //滙款
            //    case "5":   //轉帳
            //        tmpCode = "6302";
            //        tmpBCode = "110311";
            //        break;
            //    case "2":   //支票
            //        tmpCode = "6304";
            //        tmpBCode = "1151";
            //        break;
            //    case "1":   //現金
            //        tmpCode = "6303";
            //        tmpBCode = "1101";
            //        break;
            //}
            //找出最後一筆收款單記錄編號
            //string tmpPNo = Convert.ToDateTime(dv[vi][1].ToShortDateString()).ToString("yyyyMMdd");    //利用每筆應收款日期來找尋
            string tmpPNo = DateTime.Now.ToString("yyyyMMdd");    //改用今天日期來找尋
            INString = "Select top 1 TC002 from ACRTC where TC001 = '" + tmpCode + "' and TC002 like '" + tmpPNo + "%' Order by TC002 DESC ";
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
            INCommand.Connection.Close();
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            //如果有找到資料
            if (SQLReader.Read() == true)
                tmpPNo = SF.NSubString(SQLReader[0].ToString(), 0, 8) + (Convert.ToInt32(SF.NSubString(SQLReader[0].ToString(), 8, 3)) + 1).ToString("000");
            else
                tmpPNo = tmpPNo + "001";

            INCommand.Connection.Close();

            //收款單頭
            INString = "Insert into ACRTC (COMPANY,CREATOR,CREATE_DATE,FLAG,CREATE_TIME,TC001,TC002,TC003,TC004,TC005,TC006,TC007,TC008,TC009,TC010,TC011,TC012," +
                       "TC013,TC014,TC015,TC016,TC017,TC019,TC020,TC028,TC033)" +
                       " Values ('MH','DS','" + DateTime.Now.ToString("yyyyMMdd") + "',1,'" + DateTime.Now.ToString("HH:mm:ss") + "','" + tmpCode + "','" +
                       tmpPNo + "','" + SF.NSubString(tmpPNo, 0, 8) + "','" + Cust_No.Text.Trim() + "','NT$','0','" + dv[0][2] + "','N','N','0000'," +
                       dv[0][4] + "," + dv[0][4] + "," + dv[0][4] + "," + dv[0][4] + ",'" + ChargeStaff.Text.Trim() + "','N','" + SF.NSubString(tmpPNo, 0, 8) + "','N',0,'N','0')";
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
            INCommand.Connection.Open();
            INCommand.ExecuteNonQuery();
            INCommand.Connection.Close();



            //收款單身
            //借方
            INString = "Insert into ACRTD (COMPANY,CREATOR,CREATE_DATE,FLAG,CREATE_TIME,TD001,TD002,TD003,TD004,TD005,TD006,TD007,TD008,TD009,TD010," +
                       "TD011,TD012,TD013,TD014,TD015,TD017,TD020,TD021,TD022)" +
                       " Values ('MH','DS','" + DateTime.Now.ToString("yyyyMMdd") + "',1,'" + DateTime.Now.ToString("HH:mm:ss") + "','" + tmpCode + "','" +
                       tmpPNo + "','0001',1,'" + ((tmpCode == "6304") ? "2" : "1") + "','','','" + tmpBCode + "','','NT$',1," + dv[0][4] + "," +
                       dv[0][4] + "," + dv[0][4] + "," + dv[0][4] + ",'','N','" + FileArray2[BID] + "','')";
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
            INCommand.Connection.Open();
            INCommand.ExecuteNonQuery();
            INCommand.Connection.Close();
            //貸方
            INString = "Insert into ACRTD (COMPANY,CREATOR,CREATE_DATE,FLAG,CREATE_TIME,TD001,TD002,TD003,TD004,TD005,TD006,TD007,TD008,TD009,TD010," +
                       "TD011,TD012,TD013,TD014,TD015,TD017,TD020,TD021,TD022)" +
                      " Values ('MH','DS','" + DateTime.Now.ToString("yyyyMMdd") + "',1,'" + DateTime.Now.ToString("HH:mm:ss") + "','" + tmpCode + "','" +
                      tmpPNo + "','0002',-1,'4','6401','" + tmpCNo + "','1172','" + Convert.ToDateTime(dv[0][1]).ToString("yyyyMMdd") +
                      "','NT$',1," + dv[0][4] + "," + dv[0][4] + "," + dv[0][4] + "," + dv[0][4] + ",'','N','" + FileArray2[BID] + "','')";
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
            INCommand.Connection.Open();
            INCommand.ExecuteNonQuery();
            INCommand.Connection.Close();


            Response.Write("<script language=JavaScript>alert('開帳作業完成!!');</script>");

        }

    }

}