﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.VisualBasic;
using System.Data;
using System.Configuration;
using Models.Library;

namespace MommyHappySchedule.DataRebuilt
{
    public partial class ImportRenewContract : System.Web.UI.Page
    {

        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        private static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        private System.Data.SqlClient.SqlConnection INConnection = new System.Data.SqlClient.SqlConnection(ConnString);
        private System.Data.SqlClient.SqlCommand INCommand;
        private System.Data.SqlClient.SqlDataReader SQLReader;
        private System.Data.DataSet MyDataSet1, MyDataSet;
        private System.Data.SqlClient.SqlDataAdapter MyCommand1;
        private string String4;
        private System.Data.OleDb.OleDbConnection INConnection4;
        private System.Data.OleDb.OleDbDataAdapter MyCommand;


        private System.Data.DataTable  dt1;
        private System.Data.DataRow dr;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Create a TableItemStyle object that can be
            // set as the default style for all cells
            // in the table.
            // If Session("account") = "" Then
            // Session("msg") = "您沒有使用此程式的權限!!"
            // Response.Redirect("Index.aspx")
            // End If
            if (!Page.IsPostBack)
            {
                MyDataSet = new System.Data.DataSet();
                MyCommand1 = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Branch_Data where ID > 0 ", INConnection);
                MyCommand1.Fill(MyDataSet, "Branch");


                Branch.DataSource = MyDataSet.Tables["Branch"].DefaultView;
                Branch.DataTextField = "Branch_Name";
                Branch.DataValueField = "ID";
                Branch.DataBind();

                Check_Year.SelectedValue = DateTime.Now.Year.ToString();
                Check_Month.SelectedValue = DateTime.Now.Month.ToString();
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            string[] FileArray1 =  {
            "2018年合約到期資料表-南高雄",
            "2018年合約到期資料表-中高雄",
            "2018年合約到期資料表-北高雄",
            "2018年合約到期資料表-台南",
            "2018年合約到期資料表-南台中",
            "2018年合約到期資料表-北台中",
            "2018年合約到期資料表-新竹",
            "2018年合約到期資料表-桃園",
            "2018年合約到期資料表-中壢",
            "2018年合約到期資料表-板橋"
            };
            // string[] Array = {"客戶資料總檔-南高", "中高客戶資料總檔", "北高客戶資料總檔", "客戶資料總檔-台南", "客戶資料總檔-南台中", "客戶資料總檔-北台中",
            //                   "客戶資料總檔-新竹", "桃園客戶資料總檔", "中壢客戶資料總檔", "板橋客戶資料總檔"}
            string[] FileArray2 =
            {
            "2018年南高來電客戶資料庫",
            "2018年中高來電客戶資料庫",
            "2018年北高來電客戶資料庫",
            "2018年台南來電客戶資料庫",
            "2018年南台中來電客戶資料庫",
            "2018年北台中來電客戶資料庫",
            "2018年新竹來電客戶資料庫",
            "2018年桃園來電客戶資料庫",
            "2018年中壢來電客戶資料庫",
            "2018年板橋來電客戶資料庫"
            };

            string dblString = (DBMode == "Local") ? ConfigurationManager.AppSettings["FSLocal"] : ConfigurationManager.AppSettings["FSServer"];

            MyDataSet1 = new System.Data.DataSet();
            DateTime StartTime = DateTime.Now;

            int ii, Ki;
            int OI, NI;
            OI = 0;
            NI = 0;

            if (Convert.ToInt16(Branch.SelectedValue) <= FileArray1.Length)
            {
                TextBox2.Text = "";
                string INString;
                string tmpDate = "";
                string tmpContractNO = "";
                DataView dv1;
                string FString;

                DataColumn[] keys = new DataColumn[2];
                DataColumn tmpColumn;
                dt1 = new System.Data.DataTable();
                dt1.Columns.Add(new System.Data.DataColumn("ID", typeof(int)));
                tmpColumn = new DataColumn();
                tmpColumn.DataType = typeof(string);
                tmpColumn.ColumnName = "Contract_Number";
                dt1.Columns.Add(tmpColumn);
                keys[0] = tmpColumn;
                dt1.Columns.Add(new System.Data.DataColumn("Contract_Charge", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Contract_E_Date", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Contract_R_Date", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Contract_Times", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Contract_Period", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Contract_Employee", typeof(int)));
                dt1.Columns.Add(new System.Data.DataColumn("Contract_Years", typeof(int)));
                dt1.Columns.Add(new System.Data.DataColumn("Contract_Status", typeof(int)));
                dt1.Columns.Add(new System.Data.DataColumn("N_Contract_Times", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("N_Contract_Period", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("N_Contract_Employee", typeof(int)));
                dt1.Columns.Add(new System.Data.DataColumn("N_Contract_Years", typeof(int)));
                dt1.Columns.Add(new System.Data.DataColumn("N_Contract_E_Date", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Cust_Name", typeof(string)));
                dt1.PrimaryKey = keys;


                for (ii = Convert.ToInt16(Branch.SelectedValue); ii <= Convert.ToInt16(Branch.SelectedValue); ii++)
                {

                    // 合約到期資料表
                    FString = dblString + "派工明細表\\" + FileArray1[ii - 1] + ".xlsx";
                    String4 = "Provider=Microsoft.ACE.OLEDB.12.0;" + FString + ";Extended Properties='EXCEL 12.0;HDR=NO;IMEX=1'";
                    if (!System.IO.File.Exists(FString.Remove(0, 12)))
                    {
                        FString = dblString + "派工明細表\\" + FileArray1[ii - 1] + ".xls";
                        String4 = "Provider=Microsoft.ACE.OLEDB.12.0;" + FString + ";Extended Properties='EXCEL 8.0;HDR=NO;IMEX=1'";
                    }
                    INConnection4 = new System.Data.OleDb.OleDbConnection(String4);

                    for (Ki = 1; Ki <= 12; Ki++)
                    {
                        MyDataSet = new System.Data.DataSet();
                        MyCommand = new System.Data.OleDb.OleDbDataAdapter("Select * from [" + Ki.ToString() + "月$]' ", INConnection4);
                        MyCommand.Fill(MyDataSet, "Contract_Expire");

                        int Rw_Count;
                        string tmpCust_No = "";
                        string tmpContract_Period = "";
                        string tmpContract_Charge = "";
                        string tmpContract_Times;
                        string tmpContract_End_Time = "";
                        int tmpContract_Status, tmpContract_Employee, tmpContract_Years, tmpContract_Cancel;
                        string N_tmpContract_Period = "";
                        int tmpContract_ID = 0;
                        string N_tmpContract_Times;
                        string N_tmpContract_End_Time = "";
                        int N_tmpContract_Employee, N_tmpContract_Years;
                        string tmpContract_Cust_Name = "";

                        for (Rw_Count = 2; Rw_Count <= 72; Rw_Count++) // 讀 50 列
                        {
                            if (!DBNull.Value.Equals(MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][4]))
                            {
                                tmpCust_No = MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][4].ToString().Trim();
                                tmpContract_Charge = "";
                                tmpContract_Times = "0";
                                tmpContract_End_Time = DateTime.Now.ToShortDateString();
                                tmpContract_Status = 0;
                                tmpContract_Employee = 0;
                                tmpContract_Years = 0;
                                tmpContract_Cust_Name = "";
                                tmpContract_Cancel = 0;
                                N_tmpContract_Times = "0.0";
                                N_tmpContract_End_Time = DateTime.Now.ToShortDateString();
                                N_tmpContract_Employee = 0;
                                N_tmpContract_Years = 0;
                                N_tmpContract_Period = "";

                                // 讀取合約狀態
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][0]))
                                {
                                    switch (MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][0].ToString().Trim())
                                    {
                                        case "解約":
                                            {
                                                tmpContract_Status = 1;
                                                break;
                                            }

                                        case "保留":
                                            {
                                                tmpContract_Status = 2;
                                                break;
                                            }

                                        case "已續約":
                                            {
                                                tmpContract_Status = 3;
                                                break;
                                            }

                                        case "減次":
                                            {
                                                tmpContract_Status = 4;
                                                break;
                                            }
                                    }
                                }
                                // 讀取Charge_Staff_Name
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][11]))
                                    tmpContract_Charge = MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][11].ToString().Trim();

                                // 讀取客戶姓名
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][6]))
                                    tmpContract_Cust_Name = MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][6].ToString().Trim();

                                // 讀取Contract_E_Date
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][12]))
                                {
                                    ShareFunction SF = new ShareFunction();
                                    if (SF.IsDate(MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][12].ToString().Trim()))
                                    {
                                        tmpContract_End_Time = MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][12].ToString().Trim();
                                        if (System.Math.Abs(Convert.ToDateTime(tmpContract_End_Time).Year - Convert.ToInt32(DateTime.Now.Year)) > 3)
                                            tmpContract_End_Time = DateTime.Parse(DateTime.Now.Year.ToString("0000") + "/" +
                                                                   Convert.ToDateTime(tmpContract_End_Time).Month.ToString() + "/" +
                                                                   Convert.ToDateTime(tmpContract_End_Time).Day.ToString()).ToString();
                                    }
                                }
                                if (tmpContract_End_Time.Length < 6)
                                    tmpContract_End_Time = "2018/" + tmpContract_End_Time;
                                // 讀取HK_Nums
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][13]))
                                    tmpContract_Employee = Convert.ToInt32(MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][13]);

                                // 讀取Service_Cycle
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][14]))
                                    tmpContract_Period = MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][14].ToString().Trim();

                                // 讀取合約次數
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][19]))
                                    tmpContract_Times = MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][19].ToString().Trim();
                                // 讀取合約中途解約
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][18]))
                                {
                                    if (MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][18].ToString().Trim() == "合約異動" & tmpContract_Status == 1)
                                        tmpContract_Cancel = 1;
                                    else
                                        tmpContract_Cancel = 0;
                                }

                                // 讀取服務年限
                                if (!DBNull.Value.Equals(MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][15]))
                                {
                                    if (MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][15].ToString().Trim() == "二年約")
                                        tmpContract_Years = 2;
                                    else
                                        tmpContract_Years = 1;
                                }

                                INString = "Select * from Cust_Contract_data where Cust_No = '" + tmpCust_No + "'";
                                INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection);
                                INCommand.Connection.Open();
                                SQLReader = INCommand.ExecuteReader();
                                if (SQLReader.Read())
                                    tmpContract_ID = Convert.ToInt32(SQLReader[0]);
                                INCommand.Connection.Close();

                                if (tmpContract_Status != 4)
                                {
                                    if (tmpContract_Status == 3)
                                    {
                                        // 讀取新約HK_Nums
                                        if (!DBNull.Value.Equals(MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][25]))
                                            N_tmpContract_Employee = Convert.ToInt32(MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][25]);
                                        else
                                            N_tmpContract_Employee = tmpContract_Employee;

                                        // 讀取新約Service_Cycle
                                        if (!DBNull.Value.Equals(MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][24]))
                                            N_tmpContract_Period = MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][24].ToString().Trim();
                                        else
                                            N_tmpContract_Period = tmpContract_Period;

                                        // 讀取新約服務年限
                                        if (!DBNull.Value.Equals(MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][23]))
                                        {
                                            if (MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][23].ToString().Trim() == "二年約")
                                                N_tmpContract_Years = 2;
                                            else
                                                N_tmpContract_Years = 1;
                                        }
                                        else
                                            N_tmpContract_Years = tmpContract_Years;

                                        // 讀取新約 Contract_Times
                                        N_tmpContract_Times = tmpContract_Times;
                                        if (!DBNull.Value.Equals(MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][30]))
                                        {
                                            if (Convert.ToInt32(MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][30]) != 0)
                                                N_tmpContract_Times = System.Convert.ToString(System.Convert.ToDouble(
                                                    MyDataSet.Tables["Contract_Expire"].DefaultView[Rw_Count][30]) / (double)4);
                                        }
                                    }
                                    dr = dt1.Rows.Find(tmpCust_No);
                                    if (dr == null)
                                    {
                                        dr = dt1.NewRow();
                                        dr[0] = tmpContract_ID;
                                        dr[1] = tmpCust_No.Trim().Substring(0, ( 8 < tmpCust_No.Trim().Length ) ? 8 : tmpCust_No.Trim().Length );
                                        dr[2] = tmpContract_Charge;
                                        dr[3] = tmpContract_End_Time;
                                        if (tmpContract_Cancel == 1)
                                            dr[4] = (Convert.ToDateTime(tmpContract_End_Time).AddDays(1)).ToShortDateString();    // 合約預計到期日
                                        else
                                            dr[4] = tmpContract_End_Time;
                                        dr[5] = tmpContract_Times;
                                        dr[6] = tmpContract_Period;
                                        dr[7] = tmpContract_Employee;
                                        dr[8] = tmpContract_Years;
                                        dr[9] = tmpContract_Status;
                                        dr[10] = N_tmpContract_Times;
                                        dr[11] = N_tmpContract_Period;
                                        dr[12] = N_tmpContract_Employee;
                                        dr[13] = N_tmpContract_Years;
                                        dr[14] = "";
                                        dr[15] = tmpContract_Cust_Name;
                                        dt1.Rows.Add(dr);
                                    }
                                }
                                else
                                {
                                    dr = dt1.Rows.Find(tmpCust_No);
                                    if (!(dr == null))
                                    {
                                        // TextBox2.Text = TextBox2.Text & dr.Item(1).ToString
                                        dr[9] = tmpContract_Status;
                                        dr[10] = (Convert.ToDouble(dr[5]) - Convert.ToDouble(tmpContract_Times)).ToString();
                                        dr[11] = tmpContract_Period;
                                        dr[12] = tmpContract_Employee;
                                        dr[13] = tmpContract_Years;
                                    }
                                    else
                                    {
                                        INString = "Select * from Cust_Contract_data where Cust_No = '" + tmpCust_No + "'";
                                        INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection);
                                        INCommand.Connection.Open();
                                        SQLReader = INCommand.ExecuteReader();
                                        // TextBox2.Text = TextBox2.Text & INString
                                        if (SQLReader.Read())
                                        {
                                            // TextBox2.Text = TextBox2.Text & tmpCust_No
                                            dr = dt1.NewRow();
                                            // '讀取Contract_E_Date
                                            // If Not IsDBNull(SQLReader.Item(4)) Then
                                            // If IsDate(SQLReader.Item(4)) Then
                                            // dr(3) = SQLReader.Item(4).ToShortDateString
                                            // dr(4) = SQLReader.Item(4).ToShortDateString
                                            // End If
                                            // End If
                                            dr[3] = tmpContract_End_Time;
                                            dr[4] = tmpContract_End_Time;
                                            // 讀取HK_Nums
                                            if (!DBNull.Value.Equals(SQLReader[13]))
                                                dr[7] = SQLReader[13];
                                            else
                                                dr[7] = 1;
                                            // 讀取Service_Cycle
                                            if (!DBNull.Value.Equals(SQLReader[11]))
                                                dr[6] = SQLReader[11];
                                            // 讀取合約次數
                                            if (!DBNull.Value.Equals(SQLReader[7]))
                                                dr[5] = SQLReader[7];
                                            else
                                                dr[5] = 1;
                                            // 讀取服務年限
                                            dr[8] = 1;
                                            if (!DBNull.Value.Equals(SQLReader[12]))
                                            {
                                                if (SQLReader[12].ToString().Trim() == "二年約")
                                                    dr[8] = 2;
                                            }
                                            dr[0] = tmpContract_ID;
                                            dr[1] = tmpCust_No.Trim().Substring(0, (8 < tmpCust_No.Trim().Length) ? 8 : tmpCust_No.Trim().Length);
                                            dr[2] = tmpContract_Charge;
                                            dr[9] = tmpContract_Status;
                                            dr[10] = (Convert.ToDouble(dr[5]) - Convert.ToDouble(tmpContract_Times)).ToString();
                                            dr[11] = tmpContract_Period;
                                            dr[12] = tmpContract_Employee;
                                            dr[13] = tmpContract_Years;
                                            dr[14] = "";
                                            dr[15] = tmpContract_Cust_Name;
                                            dt1.Rows.Add(dr);
                                        }
                                        INCommand.Connection.Close();
                                    }
                                }
                            }
                        }
                    }
                }
                // MyDataSet1.Tables("Staff_Data").DefaultView.RowFilter = Nothing
                // Repeater1.DataSource = MyDataSet1.Tables("Staff_Data").DefaultView
                // Repeater1.DataBind()

                dt1.DefaultView.Sort = "Contract_E_Date ASC,Contract_Number ASC";
                DataGrid1.DataSource = dt1.DefaultView;
                DataGrid1.DataBind();

                dv1 = new DataView(dt1);

                // TextBox2.Text = dv1.Count

                for (ii = 0; ii <= dv1.Count - 1; ii++)
                {
                    ShareFunction SF = new ShareFunction();
                    tmpContractNO = dv1[ii][1].ToString().Trim();    // 合約編號
                    if (SF.IsDate(dv1[ii][14].ToString().Trim()))
                        tmpDate = "'" + dv1[ii][14] + "'";
                    else
                        tmpDate = "null";

                    INString = "Select * from Cust_Contract_Expire where Contract_ID = " + dv1[ii][0];
                    // TextBox2.Text = TextBox2.Text & INString & Chr(13) & Chr(10)
                    INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection);
                    INCommand.Connection.Open();
                    SQLReader = INCommand.ExecuteReader();
                    if (SQLReader.Read())
                    {
                        INString = "Update Cust_Contract_Expire set Staff_Name = '" + dv1[ii][2] + "', Contract_E_Date ='" + 
                                   dv1[ii][3].ToString().Substring(0, (10 < dv1[ii][3].ToString().Length) ? 10 : dv1[ii][3].ToString().Length) + 
                                   "', Contract_R_Date = '" +
                                   dv1[ii][4].ToString().Substring(0, (10 < dv1[ii][4].ToString().Length) ? 10 : dv1[ii][4].ToString().Length) +
                                   "', Contract_Times = " + dv1[ii][5] + ", Service_Cycle = '" + dv1[ii][6] + "', HK_Nums = " + dv1[ii][7] +
                                   ", Contract_Years = " + dv1[ii][8] + ", Contract_Result = " + dv1[ii][9] + ", N_Contract_Times = " + dv1[ii][10] +
                                   ", N_Service_Cycle = '" + dv1[ii][11] + "', N_HK_Nums = " + dv1[ii][12] + ", N_Contract_Years = " + dv1[ii][13] +
                                   ", N_Contract_S_Date = " + tmpDate + " where Contract_ID = " + dv1[ii][0];
                        OI = OI + 1;
                    }
                    else
                    {
                        INString = "Insert into Cust_Contract_Expire (Contract_ID,Cust_No,Staff_Name,Contract_E_Date,Contract_R_Date,Service_Cycle,HK_Nums," +
                                   "Contract_Times,Contract_Years,Contract_Result,N_Service_Cycle,N_HK_Nums,N_Contract_Times,N_Contract_Years,N_Contract_S_Date)" +
                                   " values (" + dv1[ii][0] + ",'" + dv1[ii][1] + "','" + dv1[ii][2] + "','" +
                                   dv1[ii][3].ToString().Substring(0, (10 < dv1[ii][3].ToString().Length ) ? 10 : dv1[ii][3].ToString().Length ) + "','" +
                                   dv1[ii][4].ToString().Substring(0, (10 < dv1[ii][4].ToString().Length ) ? 10 : dv1[ii][4].ToString().Length) + "','" + 
                                   dv1[ii][6] + "'," +
                                   dv1[ii][7] + "," + dv1[ii][5] + "," + dv1[ii][8] + "," + dv1[ii][9] + ",'" + dv1[ii][11] + "'," + dv1[ii][12] + "," +
                                   dv1[ii][10] + "," + dv1[ii][13] + "," + tmpDate + ")";
                        NI = NI + 1;
                    }
                    // TextBox2.Text = TextBox2.Text & INString
                    INCommand.Connection.Close();
                    INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection);
                    INCommand.Connection.Open();
                    INCommand.ExecuteNonQuery();
                    INCommand.Connection.Close();
                    TextBox2.Text = TextBox2.Text + INString + "\n";
                }

                // 來電資料庫滙入更新
                dt1 = new System.Data.DataTable();
                dt1.Columns.Add(new System.Data.DataColumn("Cust_No", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Contract_Charge", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Contract_End_Date", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Contract_Times", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Contract_Status", typeof(int)));
                dt1.Columns.Add(new System.Data.DataColumn("Contract_Period", typeof(string)));
                dt1.Columns.Add(new System.Data.DataColumn("Contract_Years", typeof(int)));
                dt1.Columns.Add(new System.Data.DataColumn("Contract_Employee", typeof(int)));



                for (ii = Convert.ToInt16(Branch.SelectedValue); ii <= Convert.ToInt16(Branch.SelectedValue); ii++)
                {
                    FString = dblString + "派工明細表\\" + FileArray2[ii - 1] + ".xlsx";
                    String4 = "Provider=Microsoft.ACE.OLEDB.12.0;" + FString + ";Extended Properties='EXCEL 12.0;HDR=NO;IMEX=1'";
                    if (!System.IO.File.Exists(FString.Remove(0, 12)))
                    {
                        FString = dblString + FileArray2[ii - 1] + ".xls";
                        String4 = "Provider=Microsoft.ACE.OLEDB.12.0;" + FString + ";Extended Properties='EXCEL 8.0;HDR=NO;IMEX=1'";
                    }

                    INConnection4 = new System.Data.OleDb.OleDbConnection(String4);


                    string[] CM = { "一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二" };
                    // MyCommand1 = New System.Data.SqlClient.SqlDataAdapter("select * FROM ERP_Bonus", INConnection1)
                    // MyCommand1.Fill(MyDataSet, "ProductGroup")

                    for (Ki = 1; Ki <= 12; Ki++)
                    {
                        MyDataSet = new System.Data.DataSet();
                        MyCommand = new System.Data.OleDb.OleDbDataAdapter("Select * from [" + CM[Ki - 1] + "月$]' ", INConnection4);
                        MyCommand.Fill(MyDataSet, "InCome_Calls");


                        int Rw_Count;
                        string tmpCust_No = "";
                        string tmpContract_Period = "";
                        string tmpContract_Charge = "";
                        string tmpContract_Times;
                        string tmpContract_End_Time = "";
                        int tmpContract_Status, tmpContract_Employee, tmpContract_Years;

                        for (Rw_Count = 2; Rw_Count <= 72; Rw_Count++) // 讀 50 列
                        {
                            bool RFlag = false;

                            if (!DBNull.Value.Equals(MyDataSet.Tables["InCome_Calls"].DefaultView[Rw_Count][1]))
                            {
                                // 長期合約
                                if (!DBNull.Value.Equals(MyDataSet.Tables["InCome_Calls"].DefaultView[Rw_Count][17]))
                                {
                                    if (MyDataSet.Tables["InCome_Calls"].DefaultView[Rw_Count][1].ToString().Contains('-'))
                                    {
                                        if (DBNull.Value.Equals(MyDataSet.Tables["InCome_Calls"].DefaultView[Rw_Count][0]))
                                            RFlag = true;
                                        else if (MyDataSet.Tables["InCome_Calls"].DefaultView[Rw_Count][0].ToString().Trim() != "取消")
                                            RFlag = true;
                                    }
                                }


                                if (RFlag == true)
                                {
                                    tmpCust_No = MyDataSet.Tables["InCome_Calls"].DefaultView[Rw_Count][1].ToString().Trim();
                                    tmpContract_Charge = "";
                                    tmpContract_Times = "0";
                                    tmpContract_End_Time = "null";
                                    tmpContract_Status = 0;
                                    tmpContract_Employee = 0;
                                    tmpContract_Years = 0;

                                    // 讀取合約狀態
                                    tmpContract_Status = 5;   // 新約
                                    if (!DBNull.Value.Equals(MyDataSet.Tables["InCome_Calls"].DefaultView[Rw_Count][0]))
                                    {
                                        if (MyDataSet.Tables["InCome_Calls"].DefaultView[Rw_Count][0].ToString().Trim() == "回流")
                                            tmpContract_Status = 6;// 回流
                                    }
                                    if (!DBNull.Value.Equals(MyDataSet.Tables["InCome_Calls"].DefaultView[Rw_Count][6]))
                                    {
                                        if (MyDataSet.Tables["InCome_Calls"].DefaultView[Rw_Count][6].ToString().Trim() == "回流")
                                            tmpContract_Status = 6;// 回流
                                    }

                                    // 讀取Charge_Staff_Name
                                    if (!DBNull.Value.Equals(MyDataSet.Tables["InCome_Calls"].DefaultView[Rw_Count][17]))
                                        tmpContract_Charge = MyDataSet.Tables["InCome_Calls"].DefaultView[Rw_Count][17].ToString().Trim();

                                    // 讀取Contract_S_Date
                                    if (!DBNull.Value.Equals(MyDataSet.Tables["InCome_Calls"].DefaultView[Rw_Count][15]))
                                    {
                                        ShareFunction SF = new ShareFunction();
                                        if (SF.IsDate(MyDataSet.Tables["InCome_Calls"].DefaultView[Rw_Count][15].ToString()))
                                            tmpContract_End_Time = MyDataSet.Tables["InCome_Calls"].DefaultView[Rw_Count][15].ToString().Trim();
                                    }

                                    // 讀取HK_Nums
                                    if (!DBNull.Value.Equals(MyDataSet.Tables["InCome_Calls"].DefaultView[Rw_Count][13]))
                                        tmpContract_Employee = Convert.ToInt32(MyDataSet.Tables["InCome_Calls"].DefaultView[Rw_Count][13]);

                                    // 讀取Service_Cycle
                                    if (!DBNull.Value.Equals(MyDataSet.Tables["InCome_Calls"].DefaultView[Rw_Count][21]))
                                        tmpContract_Period = MyDataSet.Tables["InCome_Calls"].DefaultView[Rw_Count][21].ToString().Trim();

                                    // 讀取服務年限
                                    if (!DBNull.Value.Equals(MyDataSet.Tables["InCome_Calls"].DefaultView[Rw_Count][24]))
                                    {
                                        if (MyDataSet.Tables["InCome_Calls"].DefaultView[Rw_Count][24].ToString().Trim() == "二年約")
                                            tmpContract_Years = 2;
                                        else
                                            tmpContract_Years = 1;
                                    }

                                    // 讀取Contract_Times
                                    if (!DBNull.Value.Equals(MyDataSet.Tables["InCome_Calls"].DefaultView[Rw_Count][22]))
                                        tmpContract_Times = MyDataSet.Tables["InCome_Calls"].DefaultView[Rw_Count][22].ToString().Trim();

                                    dr = dt1.NewRow();
                                    dr[0] = tmpCust_No.Trim().Substring(0, (8 < tmpCust_No.Trim().Length) ? 8 : tmpCust_No.Trim().Length);
                                    dr[1] = tmpContract_Charge;
                                    dr[2] = tmpContract_End_Time;
                                    dr[3] = tmpContract_Times;
                                    dr[4] = tmpContract_Status;
                                    dr[5] = tmpContract_Period;
                                    dr[6] = tmpContract_Years;
                                    dr[7] = tmpContract_Employee;
                                    dt1.Rows.Add(dr);
                                }
                            }
                        }
                    }
                }

                DataGrid2.DataSource = dt1.DefaultView;
                DataGrid2.DataBind();

                // Exit Sub

                dv1 = new DataView(dt1);

                // TextBox2.Text = dv1.Count

                for (ii = 0; ii <= dv1.Count - 1; ii++)
                {
                    int tmpContractID = 0;
                    tmpContractNO = dv1[ii][0].ToString().Trim();
                    ShareFunction SF = new ShareFunction();
                    if (SF.IsDate(dv1[ii][2].ToString().Trim()))
                        tmpDate = "'" + dv1[ii][2].ToString().Trim() + "'";
                    else
                        tmpDate = "null";

                    INString = "Select Top 1 * from Cust_Contract_data where Cust_No = '" + tmpContractNO + "' order by Contract_E_Date Desc";
                    INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection);
                    INCommand.Connection.Open();
                    SQLReader = INCommand.ExecuteReader();
                    // TextBox2.Text = TextBox2.Text & INString
                    if (SQLReader.Read())
                    {
                        tmpContractID = Convert.ToInt32(SQLReader[0]);
                    }
                    INCommand.Connection.Close();

                    // =============================新寫法==========================
                    INString = "Select * from Cust_Contract_Expire where Contract_ID = " + tmpContractID ;
                    // TextBox2.Text = TextBox2.Text & INString & Chr(13) & Chr(10)
                    INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection);
                    INCommand.Connection.Open();
                    SQLReader = INCommand.ExecuteReader();
                    if (SQLReader.Read())
                    {
                        double NewContractTime;
                        string NewServiceCycle;
                        // 將新合約次數加入舊合約次數並更新
                        NewContractTime = System.Convert.ToDouble((DBNull.Value.Equals(SQLReader[7]) ? 0 : SQLReader[7])) + System.Convert.ToDouble(dv1[ii][3]);
                        if (NewContractTime == 0.5)
                            NewServiceCycle = "二周一次";
                        else
                            NewServiceCycle = "一周" + NewContractTime.ToString("0.0") + "次";

                        INString = "Update Cust_Contract_Expire set N_Contract_Times = " + NewContractTime + ", N_Service_Cycle = '" + NewServiceCycle +
                                   "'  where Contract_ID = " + tmpContractID ;
                        OI = OI + 1;
                    }
                    else
                    {
                        INString = "Insert into Cust_Contract_Expire (Contract_ID,Cust_No,Staff_Name,Contract_E_Date,Contract_R_Date,Service_Cycle,HK_Nums," +
                                   "Contract_Times,Contract_Years,Contract_Result,N_Service_Cycle,N_HK_Nums,N_Contract_Times,N_Contract_Years,N_Contract_S_Date)" +
                                   " values (" + tmpContractID + ",'" + dv1[ii][0] + "','" + dv1[ii][1] +
                                   "',null,null,'',null,null,null," + dv1[ii][4] + ",'" + dv1[ii][5] + "'," + dv1[ii][7] + "," + dv1[ii][3] + "," +
                                   dv1[ii][6] + "," + tmpDate + ")";
                        NI = NI + 1;
                    }
                    TextBox2.Text = TextBox2.Text + INString + "\n";
                    INCommand.Connection.Close();
                    INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection);
                    INCommand.Connection.Open();
                    INCommand.ExecuteNonQuery();
                    INCommand.Connection.Close();
                }

                return;
            }
            TextBox4.Text = ((TimeSpan)(DateTime.Now - StartTime)).ToString();

            Page_Load(sender, e);
        }
    }

}