﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportPayment.aspx.cs" Inherits="MommyHappySchedule.DataRebuilt.ImportPayment" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>開帳滙入ERP</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <center>
                <table>
                    <tr>
                        <td align="center">
                            <table>

                                <tr>

                                    <td colspan="3">
                                        <table border="1" cellpadding="1" cellspacing="0">
                                            <tr style="font-size: 10pt; font-family: Arial;">
                                                <td>駐點：</td>
                                                <td>
                                                    <asp:DropDownList ID="Branch" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>年份</td>
                                                <td></td>
                                                <td>月份</td>
                                                <td></td>
                                                <td>
                                                    <asp:Button ID="Check_Report" runat="server" Text="產生報表" OnClick="Check_Report_Click" /></td>
                                                <td>
                                                    <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                                                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="/Index.aspx">返回主選單</asp:HyperLink>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>未處理合約編號：</td>
                                    <td>
                                        <asp:TextBox ID="TextBox1" runat="server" ReadOnly="True" TextMode="MultiLine" Width ="500" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>未處理付款合約編號：</td>
                                    <td colspan="2">
                                        <asp:TextBox ID="TextBox2" runat="server" ReadOnly="True" TextMode="MultiLine" Width ="500" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:GridView ID="GridView2" runat="server"></asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:GridView ID="GridView3" runat="server"></asp:GridView>
                                    </td>
                                </tr>
                                <asp:Panel ID="Panel1" runat="server" Visible="false">
                                    <tr>
                                        <td colspan="3">
                                            <table style="border-style: double; border-width: medium; width: 100%">
                                                <tr>
                                                    <td class="auto-style1">客戶姓名：<asp:Label ID="Contract_ID" runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="Branch_Name" runat="server" Visible="false"></asp:Label>
                                                    </td>
                                                    <td class="auto-style1">
                                                        <asp:Label ID="CustName" runat="server" Text="Label"></asp:Label></td>
                                                    <td class="auto-style1">合約編號：</td>
                                                    <td class="auto-style1">
                                                        <asp:Label ID="Cust_No" runat="server" Text="Label"></asp:Label></td>
                                                    <td class="auto-style1">負責專員：</td>
                                                    <td class="auto-style1">
                                                        <asp:Label ID="ChargeStaff" runat="server" Text="Label"></asp:Label></td>
                                                    <td class="auto-style1">管家人數：</td>
                                                    <td class="auto-style1">
                                                        <asp:Label ID="HKNums" runat="server" Text="Label"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style1">合約起始：</td>
                                                    <td class="auto-style1">
                                                        <asp:Label ID="StDate" runat="server" Text="Label"></asp:Label></td>
                                                    <td class="auto-style1">合約終止：</td>
                                                    <td class="auto-style1">
                                                        <asp:Label ID="EnDate" runat="server" Text="Label"></asp:Label></td>
                                                    <td class="auto-style1">合約次數：</td>
                                                    <td class="auto-style1">
                                                        <asp:Label ID="ContractTimes" runat="server" Text="1"></asp:Label></td>
                                                    <td class="auto-style1">合約年限：</td>
                                                    <td class="auto-style1">
                                                        <asp:Label ID="ContractYears" runat="server" Text="1"></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style1">備註：</td>
                                                    <td colspan="7" class="auto-style1">
                                                        <asp:Label ID="Note" runat="server" Text="Label"></asp:Label></td>
                                                </tr>

                                                <tr>
                                                    <td class="auto-style7" colspan="2">付款方式：
                                                    </td>
                                                    <td>建檔日期：<asp:Label ID="CR_Date" runat="server" Text="無"></asp:Label>
                                                    </td>
                                                    <td>銀行代碼：<asp:TextBox ID="Bank_No" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>銀行(支票)帳號：<asp:TextBox ID="Account_No" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>總金額：<asp:TextBox ID="TotalFee" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>應稅：<asp:CheckBox ID="TaxCheck" runat="server" />
                                                    </td>
                                                    <td>稅額：<asp:Label ID="TotalTax" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </asp:Panel>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#000000" width="100%">
                                <tr>
                                    <td height="30" valign="middle" bgcolor="#666666" style="width: 100%"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="textbox5" runat="server" Width="100%"></asp:TextBox></td>
                    </tr>
                </table>
            </center>
        </div>
    </form>
</body>
</html>

