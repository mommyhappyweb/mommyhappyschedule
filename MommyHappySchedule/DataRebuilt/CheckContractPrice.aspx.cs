﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.SqlClient;

namespace MommyHappySchedule.DataRebuilt
{
    public partial class CheckContractPrice : Page
    {

        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        SqlConnection INConnection1 = new SqlConnection(ConnString);
        SqlConnection INConnection2 = new SqlConnection(ConnString);
        SqlConnection INConnection3 = new SqlConnection(ConnString);
        private SqlCommand INCommand, INCommand1, INCommand2;
        private SqlDataReader SQLReader, SQLReader1;
        private System.Data.DataSet MyDataSet;
        private SqlDataAdapter MyCommand;

        protected void Page_Load(object sender, EventArgs e)
        {
            //為避免程式被誤執行,已將最重要的命令先關閉了,要執行者必需把153列的標註拿掉才會有結果

            if (!Page.IsPostBack)
            {
                // If Not IsNothing(Request("Cust_NO")) Then
                MyDataSet = new DataSet();
                MyCommand = new SqlDataAdapter("SELECT * FROM Branch_Data", INConnection1);
                MyCommand.Fill(MyDataSet, "Branch");

                Branch.DataSource = MyDataSet.Tables["Branch"].DefaultView;
                Branch.DataTextField = "Branch_Name";
                Branch.DataValueField = "ID";
                Branch.DataBind();
            }
        }

        protected void Branch_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Branch.SelectedIndex != 0)
                Show_Data(Branch.SelectedItem.Text);
            Page_Load(sender, e);
        }
        private void Show_Data(string BranchSelect)
        {
            Label12.Text = DateTime.Now.ToLongTimeString();

            MyDataSet = new DataSet();
            MyCommand = new SqlDataAdapter("SELECT * FROM Branch_Charge_Data where Branch_ID=" + Branch.SelectedValue, INConnection1);
            MyCommand.Fill(MyDataSet, "Branch_Charge");

            string INString = "Select Note,Cust_No,Service_Cycle,HK_Nums,Contract_S_Date,Contract_E_Date,Contract_Times," +
                              "Contract_Years,ID from Cust_Contract_Data_View where Branch_Name = '" + BranchSelect.Trim() +
                              "' order by Cust_No";
            DataTable dt;

            dt = new DataTable();
            dt.Columns.Add(new DataColumn("ID", typeof(int)));
            dt.Columns.Add(new DataColumn("CustNo", typeof(string)));
            dt.Columns.Add(new DataColumn("CustName", typeof(string)));
            dt.Columns.Add(new DataColumn("DBCount", typeof(int)));
            dt.Columns.Add(new DataColumn("CTCount", typeof(string)));
            dt.Columns.Add(new DataColumn("NotMatch", typeof(string)));
            dt.Columns.Add(new DataColumn("NotContract", typeof(string)));


            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            while (SQLReader.Read() == true)
            {
                Label1.Text = DBNull.Value.Equals(SQLReader[0]) ? "" : SQLReader[0].ToString();     //Note
                Label2.Text = DBNull.Value.Equals(SQLReader[1]) ? "" : SQLReader[1].ToString();     //Cust_No
                Label3.Text = DBNull.Value.Equals(SQLReader[2]) ? "" : SQLReader[2].ToString();     //Service_Cycle
                Label4.Text = DBNull.Value.Equals(SQLReader[3]) ? "1" : SQLReader[3].ToString();    //HK_Nums
                Label5.Text = DBNull.Value.Equals(SQLReader[4]) ? "" : SQLReader[4].ToString();     //Contract_S_Date
                Label6.Text = DBNull.Value.Equals(SQLReader[5]) ? "" : SQLReader[5].ToString();     //Contract_E_Date
                Label7.Text = DBNull.Value.Equals(SQLReader[6]) ? "1" : SQLReader[6].ToString();    //Contract_Times
                Label8.Text = DBNull.Value.Equals(SQLReader[7]) ? "1" : SQLReader[7].ToString();    //COntract_years

                if (Label4.Text == "0" | Label4.Text == "")
                    Label4.Text = "1";

                if (Label7.Text == "0" | Label7.Text == "")
                    Label7.Text = "1";

                if (Label8.Text == "0" | Label8.Text == "")
                    Label8.Text = "1";


                // 計算合約次數-扣除有假況
                int tmpPrice;
                string SumString = "Select ID,Job_Result_ID,Job_Date,Job_Price from Cust_Job_Schedule where Contract_ID = " + SQLReader[8];
                INCommand1 = new SqlCommand(SumString, INConnection2);
                INCommand1.Connection.Open();
                SQLReader1 = INCommand1.ExecuteReader();
                while (SQLReader1.Read() == true)
                {
                    DateTime Fdate = (DateTime)SQLReader1[2];
                    if (SQLReader1[1].ToString().Trim() == "0")  //正常排班
                    {
                       if (Label1.Text.Trim() != "單次" & Label2.Text.Trim().Contains("-") == true )   //非單次
                        {
                            //一周一次以上
                            if (Label3.Text.Trim().Contains("二周") == false & Convert.ToDouble(Label7.Text) > 0.5 )    //判斷舊合約次數 
                            {
                                //星期六、日價格
                                if (Fdate.DayOfWeek == DayOfWeek.Saturday | Fdate.DayOfWeek == DayOfWeek.Sunday)
                                {
                                    tmpPrice = (int)MyDataSet.Tables["Branch_Charge"].DefaultView[0][2];
                                }
                                else
                                {
                                    tmpPrice = (int)MyDataSet.Tables["Branch_Charge"].DefaultView[0][1];
                                }
                                //二周一次
                            }
                            else
                            {
                                //星期六、日價格
                                if (Fdate.DayOfWeek == DayOfWeek.Saturday | Fdate.DayOfWeek == DayOfWeek.Sunday)
                                {
                                    tmpPrice = (int)MyDataSet.Tables["Branch_Charge"].DefaultView[0][4];
                                }
                                else
                                {
                                    tmpPrice = (int)MyDataSet.Tables["Branch_Charge"].DefaultView[0][3];
                                }
                            }
                        }
                        else
                            tmpPrice = 2200;

                    INString = "Update Cust_Job_Schedule Set Job_Price = " + tmpPrice.ToString() + " where ID = " + SQLReader1[0];

                    INCommand2 = new SqlCommand(INString, INConnection3);
                    INCommand2.Connection.Open();
                    //INCommand2.ExecuteNonQuery();
                    INCommand2.Connection.Close();
                }
                }
                INCommand1.Connection.Close();


            }
            INCommand.Connection.Close();
            Label12.Text = Label12.Text + "-" + DateTime.Now.ToLongTimeString();

            //DataGrid1.DataSource = dt.DefaultView;
            //DataGrid1.DataBind();
        }

        public void Show_Job_Detail(object sender, DataGridCommandEventArgs e)
        {
            LinkButton tmpLB;
            tmpLB = (LinkButton)e.Item.Cells[0].Controls[0];

            Response.Redirect("../CP.aspx?ID=" + tmpLB.Text);
        }
    }

}