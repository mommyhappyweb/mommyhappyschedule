﻿using System;
using System.Web.UI;
using System.Data.OleDb;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Configuration;
using Models.Library;

namespace MommyHappySchedule.DataRebuilt
{
    public partial class ImportStaffData_1 : Page
    {

        private DataTable dt, dt1;
        private DataRow dr;
        private DataSet MyDataSet;
        private SqlDataAdapter MyCommand;
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        SqlConnection INConnection = new SqlConnection(ConnString);
        SqlConnection INConnection1 = new SqlConnection(ConnString);

        private SqlCommand INCommand;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string INString;
            SqlDataReader SQLReader;
            MyDataSet = new DataSet();
            MyCommand = new SqlDataAdapter("SELECT * FROM Branch_Data where ID > 0 ", INConnection1);
            MyCommand.Fill(MyDataSet, "Branch");

            //讀取排班資料表
            string[] Branch = new string[] { };
            INString = "SELECT * FROM Branch_Data";
            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            while (SQLReader.Read())
            {
                Branch = Branch.Concat(new string[] { SQLReader[1].ToString().Trim() }).ToArray();
            }
            INCommand.Connection.Close();

            //讀取排班資料表
            string[] JobTS = new string[] { };
            INString = "Select * from Public_JobTS";
            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            while (SQLReader.Read())
            {
                JobTS = JobTS.Concat(new string[] { SQLReader[1].ToString().Trim() }).ToArray();
            }
            INCommand.Connection.Close();

            //讀取合約狀態資料表
            string[] JobStatus = new string[] { };
            INString = "Select * from Public_JobStatus";
            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            while (SQLReader.Read())
            {
                JobStatus = JobStatus.Concat(new string[] { SQLReader[1].ToString().Trim().Trim() }).ToArray();
            }
            INCommand.Connection.Close();

            //讀取管家狀態資料表
            string[] JobTitle = new string[] { };
            INString = "Select * from Public_JobTitle";
            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            while (SQLReader.Read())
            {
                JobTitle = JobTitle.Concat(new string[] { SQLReader[2].ToString().Trim() }).ToArray();
            }
            INCommand.Connection.Close();


            int ii;

            string Year = DateTime.Now.ToString("yyyy");
            string[] FileArray ={
                "內勤人事基本資料表", Year + "年南高人事基本資料表",Year + "年中高人事基本資料表",Year + "年北高人事基本資料表", Year + "年台南人事基本資料表",Year + "年南台中人事基本資料表",Year + "年北台中人事基本資料表",Year + "年新竹人事基本資料表",
                                  Year + "年桃園人事基本資料表",Year + "年中壢人事基本資料表",Year + "年板橋人事基本資料表",Year + "年旗艦人事基本資料表"};

            dt = new DataTable();
            dt.Columns.Add(new DataColumn("狀態", typeof(string)));
            dt.Columns.Add(new DataColumn("員工編號", typeof(string)));
            dt.Columns.Add(new DataColumn("姓名", typeof(string)));
            dt.Columns.Add(new DataColumn("駐地", typeof(string)));
            dt.Columns.Add(new DataColumn("Branch_Name", typeof(string)));
            dt.Columns.Add(new DataColumn("職稱", typeof(string)));
            dt.Columns.Add(new DataColumn("班制", typeof(string)));
            dt.Columns.Add(new DataColumn("主管姓名", typeof(string)));
            dt.Columns.Add(new DataColumn("輔導組長", typeof(string)));
            dt.Columns.Add(new DataColumn("到職日", typeof(string)));
            dt.Columns.Add(new DataColumn("離職日期", typeof(string)));
            dt.Columns.Add(new DataColumn("TEST", typeof(string)));

            dt1 = new DataTable();
            dt1.Columns.Add(new DataColumn("員工編號", typeof(string)));
            dt1.Columns.Add(new DataColumn("姓名", typeof(string)));
            dt1.Columns.Add(new DataColumn("性別", typeof(string)));
            dt1.Columns.Add(new DataColumn("生日", typeof(DateTime)));
            dt1.Columns.Add(new DataColumn("婚姻", typeof(string)));
            dt1.Columns.Add(new DataColumn("身份証", typeof(string)));
            dt1.Columns.Add(new DataColumn("手機", typeof(string)));
            dt1.Columns.Add(new DataColumn("市話號碼", typeof(string)));
            dt1.Columns.Add(new DataColumn("通訊地址", typeof(string)));
            dt1.Columns.Add(new DataColumn("戶籍地址", typeof(string)));
            dt1.Columns.Add(new DataColumn("緊急連絡人", typeof(string)));
            dt1.Columns.Add(new DataColumn("連絡人電話", typeof(string)));



            for (ii = 0; ii <= 11; ii++)
            {
                string dblString = (DBMode == "Local") ? ConfigurationManager.AppSettings["FSLocal"] : ConfigurationManager.AppSettings["FSServer"];

                string FString =  dblString + "人事基本資料\\" + FileArray[ii] + ".xlsx";
                if (!File.Exists(FString.Remove(0, 12)))
                {
                    FString = dblString + "人事基本資料\\" + FileArray[ii] + ".xls";
                }
                string FileConnString = "Provider=Microsoft.ACE.OLEDB.12.0;" + FString + ";Extended Properties='EXCEL 12.0;HDR=NO;IMEX=1'";

                OleDbConnection OleDbConn = new OleDbConnection(FileConnString);
                OleDbCommand INCommand = new OleDbCommand("Select * from [人事基本資料$]", OleDbConn);
                INCommand.Connection.Open();
                OleDbDataReader OleDbReader = INCommand.ExecuteReader();
                // 前三筆是欄位

                OleDbReader.Read();
                OleDbReader.Read();
                OleDbReader.Read();

                // Create more rows for the table.
                while (OleDbReader.Read() == true)
                {
                    if (DBNull.Value.Equals(OleDbReader[2]) & DBNull.Value.Equals(OleDbReader[3]))
                        continue;
                    if (OleDbReader[2].ToString().Trim() == "" & OleDbReader[3].ToString().Trim() == "")
                        continue;
                    if (OleDbReader[1].ToString().Trim() == "調職")
                        continue;

                    dr = dt.NewRow();
                    dr[0] = OleDbReader[1];
                    dr[1] = OleDbReader[2];
                    dr[2] = OleDbReader[3];
                    // If ii >= 3 Then
                    // dr(2) = OleDbReader.Item(3)
                    // Else
                    // dr(2) = OleDbReader.Item(4)
                    // End If

                    dr[3] = OleDbReader[12];
                    dr[4] = OleDbReader[13];
                    dr[5] = OleDbReader[14].ToString().Substring(0, (10 > OleDbReader[14].ToString().Length) ? OleDbReader[14].ToString().Length : 10);
                    // dr(5) = OleDbReader.Item(14)
                    dr[6] = OleDbReader[15];
                    // dr(7) = OleDbReader.Item(17)
                    dr[8] = OleDbReader[17];
                    string tmpDate;
                    ShareFunction SF = new ShareFunction();
                    tmpDate = "null";

                    if (!DBNull.Value.Equals(OleDbReader[18]) & !DBNull.Value.Equals(OleDbReader[19]) & !DBNull.Value.Equals(OleDbReader[20]))
                        if (SF.IsDate((Convert.ToInt32(OleDbReader[18]) + 1911).ToString() + "/" + OleDbReader[19] + "/" + OleDbReader[20]))
                            tmpDate = "'" + (Convert.ToInt32(OleDbReader[18]) + 1911).ToString() + "/" + OleDbReader[19] + "/" + OleDbReader[20] + "'";

                    dr[9] = tmpDate;

                    if (DBNull.Value.Equals(OleDbReader[64]))
                    {
                        if (DBNull.Value.Equals(OleDbReader[61]))
                        {
                        }
                        else
                        {
                            if (SF.IsDate((Convert.ToInt32(OleDbReader[61]) + 1911).ToString() + "/" + OleDbReader[62] + "/" + OleDbReader[63]))
                                tmpDate = "'" + (Convert.ToInt32(OleDbReader[61]) + 1911).ToString() + "/" + OleDbReader[62] + "/" + OleDbReader[63] + "'";
                            else
                                tmpDate = "null";
                            dr[10] = tmpDate;
                            dr[11] = OleDbReader[61].ToString() + OleDbReader[62].ToString() + OleDbReader[63].ToString();
                        }
                    }
                    else
                    {
                        if (SF.IsDate((Convert.ToInt32(OleDbReader[64]) + 1911).ToString() + "/" + OleDbReader[65] + "/" + OleDbReader[66]))
                            tmpDate = "'" + (Convert.ToInt32(OleDbReader[64]) + 1911).ToString() + "/" + OleDbReader[65] + "/" + OleDbReader[66] + "'";
                        else
                            tmpDate = "null";
                        dr[10] = tmpDate;
                        dr[11] = OleDbReader[64].ToString() + OleDbReader[65].ToString() + OleDbReader[66].ToString();
                    }
                    dt.Rows.Add(dr);

                    dr = dt1.NewRow();
                    dr[0] = OleDbReader[2];  // 員編
                    dr[1] = OleDbReader[3];  // 姓名
                    if (DBNull.Value.Equals(OleDbReader[10]))
                        dr[2] = 0;
                    else
                        dr[2] = (OleDbReader[10].ToString() == "女") ? 2 : 1;// 性別
                                                                            // Birth
                    if (DBNull.Value.Equals(OleDbReader[6]) | DBNull.Value.Equals(OleDbReader[7]) | DBNull.Value.Equals(OleDbReader[8]))
                    {
                    }
                    else if (SF.IsDate((Convert.ToInt64(OleDbReader[6]) + 1911).ToString() + "/" + OleDbReader[7] + "/" + OleDbReader[8]))
                        dr[3] = Convert.ToDateTime((Convert.ToInt64(OleDbReader[6]) + 1911).ToString() + "/" + OleDbReader[7] + "/" + OleDbReader[8]);

                    dr[4] = OleDbReader[11];   // 婚姻
                    dr[5] = OleDbReader[5].ToString().Trim();    // 身份証
                    dr[6] = OleDbReader[34].ToString().Trim();   // 手機
                    dr[7] = OleDbReader[33];   // 市話
                    dr[8] = (DBNull.Value.Equals(OleDbReader[35])) ? "" : OleDbReader[35];   // 通訊地址
                    dr[9] = (DBNull.Value.Equals(OleDbReader[36])) ? dr[8] : OleDbReader[36];   // 戶籍地址
                    dr[10] = (DBNull.Value.Equals(OleDbReader[39])) ? "" : OleDbReader[39];  // 緊急連絡人
                    dr[11] = (DBNull.Value.Equals(OleDbReader[40])) ? "" : OleDbReader[40];  // 連絡人電話

                    dt1.Rows.Add(dr);
                }

                INCommand.Connection.Close();
            }


            DataGrid1.DataSource = dt1.DefaultView;
            DataGrid1.DataBind();

            // Exit Sub
            int OI, NI;
            OI = 0;
            NI = 0;
            string H_Data = "";

            for (ii = 0; ii <= dt.Rows.Count - 1; ii++)
            {
                int ti;
                var il = new[] { 10, 10, 0, 0, 10, 12, 15, 15, 0, 0, 20, 50 } ;
                for (ti = 0; ti <= 11; ti++)
                {
                    if (il[ti] != 0)
                    {
                        // 拮取資料庫長度
                        if (!DBNull.Value.Equals(dt1.Rows[ii][ti]))
                            dt1.Rows[ii][ti] = dt1.Rows[ii][ti].ToString().Substring(0, (il[ti] < dt1.Rows[ii][ti].ToString().Length) ?
                                il[ti] : dt1.Rows[ii][ti].ToString().Length);
                    }
                }

                INString = "Select * from Staff_Job_Data where Staff_No = '" + dt.Rows[ii][1] + "'";
                H_Data = H_Data + "'" + dt.Rows[ii][1] + "',";

                INCommand = new SqlCommand(INString, INConnection);
                INCommand.Connection.Open();
                SQLReader = INCommand.ExecuteReader();
                if (SQLReader.Read() == true)
                {
                    if (DBNull.Value.Equals(dt.Rows[ii][10]))
                        INString = "Update Staff_Job_Data set Staff_Name = N'" + dt.Rows[ii][2] +
                                   "',Job_Status_ID = " + (Array.IndexOf(JobStatus, dt.Rows[ii][0]) + 1).ToString() +
                                   ",Branch_Company = '" + dt.Rows[ii][3] + "',Branch_ID = " + 
                                   (dt.Rows[ii][3].ToString().Trim() == "屏東" ? "1" : (Array.IndexOf(Branch, dt.Rows[ii][4])).ToString()) +
                                   ",Job_Title_ID = " + (Array.IndexOf(JobTitle, dt.Rows[ii][5]) + 1).ToString() +
                                   ",Job_TS_ID = " + (Array.IndexOf(JobTS, dt.Rows[ii][6]) + 1).ToString() +
                                   ",Svr_Name = '" + dt.Rows[ii][8] + "',Join_Date = " + dt.Rows[ii][9] +
                                   ",Depart_Date = null where Staff_No = '" + dt.Rows[ii][1] + "'";

                    else
                        INString = "Update Staff_Job_Data set Staff_Name = N'" + dt.Rows[ii][2] +
                                   "',Job_Status_ID = " + (Array.IndexOf(JobStatus, dt.Rows[ii][0]) + 1).ToString() +
                                   ",Branch_Company = '" + dt.Rows[ii][3] + "',Branch_ID = " +
                                   (dt.Rows[ii][3].ToString().Trim() == "屏東" ? "1" : (Array.IndexOf(Branch, dt.Rows[ii][4])).ToString()) +
                                   ",Job_Title_ID = " + (Array.IndexOf(JobTitle, dt.Rows[ii][5]) + 1).ToString() +
                                   ",Job_TS_ID = " + (Array.IndexOf(JobTS, dt.Rows[ii][6]) + 1).ToString() +
                                   ",Svr_Name = '" + dt.Rows[ii][8] + "',Join_Date = " + dt.Rows[ii][9] +
                                   ",Depart_Date = " + dt.Rows[ii][10] + " where Staff_No = '" + dt.Rows[ii][1] + "'";

                    OI = OI + 1;
                }
                else
                {
                    INCommand.Connection.Close();
                    INString = "Select * from Staff_Job_Data_View where Staff_Name = '" + dt.Rows[ii][2] + "' and Branch_Company = '" + dt.Rows[ii][3] + "'";

                    INCommand = new SqlCommand(INString, INConnection);
                    INCommand.Connection.Open();
                    SQLReader = INCommand.ExecuteReader();
                    if (SQLReader.Read() == true)
                    {
                        if (DBNull.Value.Equals(dt.Rows[ii][10]))
                            INString = "Update Staff_Job_Data set Staff_No = '" + dt.Rows[ii][1] +
                                       "',Job_Status_ID = " + (Array.IndexOf(JobStatus, dt.Rows[ii][0]) + 1).ToString() +
                                       ",Branch_Company = '" + dt.Rows[ii][3] + "',Branch_ID = " +
                                       (dt.Rows[ii][3].ToString().Trim() == "屏東" ? "1" : (Array.IndexOf(Branch, dt.Rows[ii][4])).ToString()) +
                                       ",Job_Title_ID = " + (Array.IndexOf(JobTitle, dt.Rows[ii][5]) + 1).ToString() +
                                       ",Job_TS_ID = " + (Array.IndexOf(JobTS, dt.Rows[ii][6]) + 1).ToString() +
                                       ",Svr_Name = '" + dt.Rows[ii][8] + "',Join_Date = " + dt.Rows[ii][9] +
                                       ",Depart_Date = null where Staff_Name = '" + dt.Rows[ii][2] + "' and Branch_Company = '" + dt.Rows[ii][3] + "'";
                        else
                            INString = "Update Staff_Job_Data set Staff_No = '" + dt.Rows[ii][1] + "',Job_Status_ID = " +
                                       (Array.IndexOf(JobStatus, dt.Rows[ii][0]) + 1).ToString() +
                                       ",Branch_Company = '" + dt.Rows[ii][3] + "',Branch_ID = " +
                                       (dt.Rows[ii][3].ToString().Trim() == "屏東" ? "1" : (Array.IndexOf(Branch, dt.Rows[ii][4])).ToString()) +
                                       ",Job_Title_ID = " + (Array.IndexOf(JobTitle, dt.Rows[ii][5]) + 1).ToString() +
                                       ",Job_TS_ID = " + (Array.IndexOf(JobTS, dt.Rows[ii][6]) + 1).ToString() +
                                       ",Svr_Name = '" + dt.Rows[ii][8] + "',Join_Date = " + dt.Rows[ii][9] +
                                       ",Depart_Date = " + dt.Rows[ii][10] + " where Staff_Name = '" + dt.Rows[ii][2] +
                                       "' and Branch_Company = '" + dt.Rows[ii][3] + "'";
                        OI = OI + 1;
                    }
                    else
                    {
                        if (DBNull.Value.Equals(dt.Rows[ii][10]))
                            INString = "Insert into  Staff_Job_Data (Job_Status_ID,Staff_No,Staff_Name,Branch_Company,Branch_ID,Job_Title_ID,Job_TS_ID," +
                                       "Mgr_Name,Svr_Name,Join_Date,Depart_Date,NoPrm_Reason) values (" + (Array.IndexOf(JobStatus, dt.Rows[ii][0])).ToString() +
                                       ",'" + dt.Rows[ii][1] + "',N'" + dt.Rows[ii][2] + "','" + dt.Rows[ii][3] + "'," +
                                       (dt.Rows[ii][3].ToString().Trim() == "屏東" ? "1" : (Array.IndexOf(Branch, dt.Rows[ii][4])).ToString()) + 
                                       "," + (Array.IndexOf(JobTitle, dt.Rows[ii][5]) + 1).ToString() +
                                       "," + (Array.IndexOf(JobTS, dt.Rows[ii][6]) + 1).ToString() + ",null,'" + dt.Rows[ii][8] + "'," +
                                       dt.Rows[ii][9] + ",null,null)";
                        else
                            INString = "Insert into  Staff_Job_Data (Job_Status_ID,Staff_No,Staff_Name,Branch_Company,Branch_ID,Job_Title_ID,Job_TS_ID," +
                                       "Mgr_Name,Svr_Name,Join_Date,Depart_Date,NoPrm_Reason) values (" + (Array.IndexOf(JobStatus, dt.Rows[ii][0])).ToString() +
                                       ",'" + dt.Rows[ii][1] + "',N'" + dt.Rows[ii][2] + "','" + dt.Rows[ii][3] + "'," +
                                       (dt.Rows[ii][3].ToString().Trim() == "屏東" ? "1" : (Array.IndexOf(Branch, dt.Rows[ii][4])).ToString()) +
                                       "," + (Array.IndexOf(JobTitle, dt.Rows[ii][5]) + 1).ToString() +
                                       "," + (Array.IndexOf(JobTS, dt.Rows[ii][6]) + 1).ToString() + ",null,'" + dt.Rows[ii][8] + "'," +
                                       dt.Rows[ii][9] + "," + dt.Rows[ii][10] +
                                       ",null)";
                        NI = NI + 1;
                    }
                }
                TextBox2.Text = TextBox2.Text + INString + "\n";
                INCommand.Connection.Close();
                INCommand = new SqlCommand(INString, INConnection);
                INCommand.Connection.Open();
                INCommand.ExecuteNonQuery();
                INCommand.Connection.Close();


                // Staff_Data
                string LCityName, LCountryName, LRoad_Name, HCityName, HCountryName, HRoad_Name;
                try
                {
                    LCityName = dt1.Rows[ii][8].ToString().Substring(0, 3);
                }
                catch
                {
                    LCityName = "";
                }
                try
                {
                    LCountryName = dt1.Rows[ii][8].ToString().Substring(3, 3);
                }
                catch
                {
                    LCountryName = "";
                }
                try
                {
                    LRoad_Name = dt1.Rows[ii][8].ToString().Substring(6);
                }
                catch
                {
                    LRoad_Name = "";
                }
                try
                {
                    HCityName = dt1.Rows[ii][9].ToString().Substring(0, 3);
                }
                catch
                {
                    HCityName = "";
                }
                try
                {
                    HCountryName = dt1.Rows[ii][9].ToString().Substring(3, 3);
                }
                catch
                {
                    HCountryName = "";
                }
                try
                {
                    HRoad_Name = dt1.Rows[ii][9].ToString().Substring(6);
                }
                catch
                {
                    HRoad_Name = "";
                }
                INCommand.Connection.Close();
                INString = "Select ID from Staff_Data where Staff_No = '" + dt1.Rows[ii][0] + "'";

                INCommand = new SqlCommand(INString, INConnection);
                INCommand.Connection.Open();
                SQLReader = INCommand.ExecuteReader();
                if (SQLReader.Read() == true)
                {


                    if (dt1.Rows[ii][9] == dt1.Rows[ii][10])
                        INString = "Update Staff_Data set Marrige ='" + dt1.Rows[ii][4] + "',Cell_Phone = '" + dt1.Rows[ii][6] + "',House_Phone = '" +
                                   dt1.Rows[ii][7].ToString().Substring(0, (15 > dt1.Rows[ii][7].ToString().Length) ? dt1.Rows[ii][7].ToString().Length : 15) +
                                   "',L_City_Name = '" + LCityName + "',L_Country_Name = '" + LCountryName + "',L_Road_Name = '" +
                                   LRoad_Name + "',Emg_Name = '" + dt1.Rows[ii][10] + "',Emg_Cell_Phone = '" + dt1.Rows[ii][11] + "',RID = '" + dt1.Rows[ii][5] +
                                   "',Name = N'" + dt1.Rows[ii][1] + "' where ID = " + SQLReader[0];
                    else
                        INString = "Update Staff_Data set Marrige ='" + dt1.Rows[ii][4] + "',Cell_Phone = '" + dt1.Rows[ii][6] + "',House_Phone = '" +
                                   dt1.Rows[ii][7].ToString().Substring(0, (15 > dt1.Rows[ii][7].ToString().Length) ? dt1.Rows[ii][7].ToString().Length : 15) +
                                   "',L_City_Name = '" + LCityName + "',L_Country_Name = '" + LCountryName + "',L_Road_Name = '" +
                                   LRoad_Name + "',H_City_Name = '" + HCityName + "',H_Country_Name = '" + HCountryName + "',H_Road_Name = '" + HRoad_Name +
                                   "',Emg_Name = '" + dt1.Rows[ii][10] + "',Emg_Cell_Phone = '" + dt1.Rows[ii][11] + "',RID = '" + dt1.Rows[ii][5] +
                                   "',Name = N'" + dt1.Rows[ii][1] + "' where ID = " + SQLReader[0];

                    OI = OI + 1;
                }
                else
                {
                    if (dt1.Rows[ii][9] == dt1.Rows[ii][10])
                        INString = "Insert into  Staff_Data (Staff_No,Name,Sex,Birth,Marrige,RID,Cell_Phone,House_Phone,L_City_Name,L_Country_Name,L_Road_Name," +
                                   "Emg_Name,Emg_Cell_Phone) values ('" + dt1.Rows[ii][0] + "',N'" + dt1.Rows[ii][1] + "'," +
                                   (((string)dt1.Rows[ii][2] == "女") ? 2 : 1) + ",'" + Convert.ToDateTime(dt1.Rows[ii][3]).ToShortDateString() + "','" +
                                   dt1.Rows[ii][4] + "','" + dt1.Rows[ii][5] + "','" + dt1.Rows[ii][6] + "','" +
                                   dt1.Rows[ii][7].ToString().Substring(0, (15 > dt1.Rows[ii][7].ToString().Length) ? dt1.Rows[ii][7].ToString().Length : 15) +
                                   "','" + LCityName + "','" + LCountryName + "','" + LRoad_Name + "','" + dt1.Rows[ii][10] + "','" + dt1.Rows[ii][11] + "')";
                    else
                        INString = "Insert into  Staff_Data (Staff_No,Name,Sex,Birth,Marrige,RID,Cell_Phone,House_Phone,L_City_Name,L_Country_Name,L_Road_Name," +
                                   "H_City_Name,H_Country_Name,H_Road_Name,Emg_Name,Emg_Cell_Phone) values ('" + dt1.Rows[ii][0] + "',N'" + dt1.Rows[ii][1] + "'," +
                                   (((string)dt1.Rows[ii][2] == "女") ? 2 : 1) + ",'" + Convert.ToDateTime(dt1.Rows[ii][3]).ToShortDateString() + "','" +
                                   dt1.Rows[ii][4] + "','" + dt1.Rows[ii][5] + "','" + dt1.Rows[ii][6] + "','" +
                                   dt1.Rows[ii][7].ToString().Substring(0, (15 > dt1.Rows[ii][7].ToString().Length) ? dt1.Rows[ii][7].ToString().Length : 15) +
                                   "','" + LCityName + "','" + LCountryName + "','" + LRoad_Name + "','" + HCityName + "','" + HCountryName + "','" +
                                   HRoad_Name + "','" + dt1.Rows[ii][10] + "','" + dt1.Rows[ii][11] + "')";
                    NI = NI + 1;
                }

                INCommand.Connection.Close();
                INCommand = new SqlCommand(INString, INConnection);
                INCommand.Connection.Open();
                INCommand.ExecuteNonQuery();
                INCommand.Connection.Close();
                TextBox2.Text = TextBox2.Text + INString + "\n";
            }
            if (H_Data.Length > 1)
                H_Data = H_Data.Substring(0, H_Data.Length - 1);
            INString = " Update Staff_Job_Data set Job_Status_ID = " + (Array.IndexOf(JobStatus, "離職")).ToString() +
                       " where Job_Status_ID = " + (Array.IndexOf(JobStatus, "在職")).ToString() + " and Staff_No not in (" + H_Data + ")";
            INCommand.Connection.Close();
            INCommand = new SqlCommand(INString, INConnection);
            INCommand.Connection.Open();
            INCommand.ExecuteNonQuery();
            INCommand.Connection.Close();

            Label5.Text = "更新:" + OI.ToString() + " 新增:" + NI.ToString();
            return;
      
        }
    }

}