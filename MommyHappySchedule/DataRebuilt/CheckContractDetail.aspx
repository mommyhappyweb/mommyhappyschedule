﻿<%@ Page Language="C#" MasterPageFile="~/Template/Layout.master" AutoEventWireup="true" CodeBehind="CheckContractDetail.aspx.cs" Inherits="MommyHappySchedule.DataRebuilt.CheckContractDetail" %>

<asp:Content ID="Style" ContentPlaceHolderID="Style" runat="server">
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="Content" runat="server">
    <div class="container-fluid">
        <nav class="mt-3 mb-3" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/Index.aspx">首頁</a></li>
                <li class="breadcrumb-item active">檢查合約</li>
            </ol>
        </nav>
        <fieldset class="col-sm-10 offset-sm-1">
            <legend>資料搜尋</legend>
            <div class="form-group row">
                <div class="col-sm-12">
                    <label for="Branch">駐點</label>
                    <asp:DropDownList ID="Branch" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="Branch_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
        </fieldset>
        <table class="table">
            <asp:Panel ID="Panel2" runat="server" Visible="false">
                <tr>
                    <td class="auto-style1">客戶姓名：</td>
                    <td class="auto-style1">
                        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></td>
                    <td class="auto-style1">合約編號：</td>
                    <td class="auto-style1">
                        <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label></td>
                    <td class="auto-style1">負責專員：</td>
                    <td class="auto-style1">
                        <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label></td>
                    <td class="auto-style1">管家人數：</td>
                    <td class="auto-style1">
                        <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td class="auto-style1">合約起始：</td>
                    <td class="auto-style1">
                        <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label></td>
                    <td class="auto-style1">合約終止：</td>
                    <td class="auto-style1">
                        <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label></td>
                    <td class="auto-style1">合約次數：</td>
                    <td class="auto-style1">
                        <asp:Label ID="Label7" runat="server" Text="1"></asp:Label></td>
                    <td class="auto-style1">合約年限：</td>
                    <td class="auto-style1">
                        <asp:Label ID="Label8" runat="server" Text="1"></asp:Label></td>
                </tr>
            </asp:Panel>
        </table>
        <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" CssClass="table table-striped">
            <HeaderStyle CssClass="thead-light">
            </HeaderStyle>
            <Columns>
                <asp:ButtonColumn DataTextField="ID" HeaderText="編號">
                </asp:ButtonColumn>
                <asp:BoundColumn DataField="CustNo" HeaderText="客戶編號">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="CustName" HeaderText="客戶姓名">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="DBCount" HeaderText="資料庫次數">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="CTCount" HeaderText="合約次數">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="NotMatch" HeaderText="次數不合">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="NotContract" HeaderText="次數非正常">
                </asp:BoundColumn>
            </Columns>
        </asp:DataGrid>
    </div>
</asp:Content>
<asp:Content ID="Script" ContentPlaceHolderID="Script" runat="server">
</asp:Content>
