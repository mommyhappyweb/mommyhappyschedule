﻿using System;
using System.Web.UI;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using Models.List;
using Models.Library.Regular;
using Models.Method.Public;
using Models.Method.Staff;
using Models.Library;
using Models.Library.Cyphertext;

namespace MommyHappySchedule.DataRebuilt
{
    public partial class ImportStaffData : Page
    {
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];

        private readonly string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];

        private readonly string FilePathString = (DBMode == "Local") ? ConfigurationManager.AppSettings["FSLocal"] : ConfigurationManager.AppSettings["FSServer"];

        private SqlConnection Conn = null;

        private readonly ConvertType ConvertType = new ConvertType();

        private readonly RegularCheck RegularCheck = new RegularCheck();

        private readonly Logs Logs = new Logs();

        private readonly BaseMD5 BaseMD5 = new BaseMD5();

        private List<Dictionary<string, object>> BranchData = null;

        private List<Dictionary<string, object>> JobStatusData = null;

        private List<Dictionary<string, object>> JobTitleData = null;

        private static string Year = DateTime.Now.ToString("yyyy");

        private string[] FileArray = new string[]
        {
            "內勤人事基本資料表",
            Year + "年南高人事基本資料表",
            Year + "年中高人事基本資料表",
            Year + "年北高人事基本資料表",
            Year + "年台南人事基本資料表",
            Year + "年南台中人事基本資料表",
            Year + "年北台中人事基本資料表",
            Year + "年新竹人事基本資料表",
            Year + "年桃園人事基本資料表",
            Year + "年中壢人事基本資料表",
            Year + "年板橋人事基本資料表",
            Year + "年旗艦人事基本資料表",
            Year + "年北屯人事基本資料表"
        };

        protected void Page_Load(object sender, EventArgs e)
        {
            BranchMethod BranchMethod = new BranchMethod();
            JobStatusMethod JobStatusMethod = new JobStatusMethod();
            JobTitleMethod JobTitleMethod = new JobTitleMethod();
            BranchData = BranchMethod.GetData();
            JobStatusData = JobStatusMethod.GetData();
            JobTitleData = JobTitleMethod.GetData();

            Conn = new SqlConnection(ConnString);
            Conn.Open();
        }

        /// <summary>
        /// 匯入資料事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImportStaffData_Click(object sender, EventArgs e)
        {
            string[] DeputyArray = new string[2] { ".xlsx", ".xls" };
            foreach (string FileName in FileArray)
            {
                string FilePath = FilePathString + @"人事基本資料\" + FileName;
                string FileConnString = "";
                foreach (string DeputyName in DeputyArray)
                {
                    string FilePathName = (FilePath + DeputyName).Remove(0, 12);
                    if (File.Exists(FilePathName))
                    {
                        FileConnString = "Provider =Microsoft.ACE.OLEDB.12.0;Data Source=" + FilePathName + ";Extended Properties='EXCEL 12.0;HDR=YES;IMEX=1'";
                    }
                }
                StaffDataList Data = ExcelConvertData(FileConnString);
                if (Data.StaffData != null) WriteStaffData(Data.StaffData);
                if (Data.StaffJobData != null) WriteStaffJobData(Data.StaffJobData);
                if (Data.StaffRightData != null) WriteStaffRightData(Data.StaffRightData);
            }
            Conn.Close();
            Conn.Dispose();
        }

        /// <summary>
        /// EXCEL 轉換 資料列表
        /// </summary>
        /// <param name="FileConnString"></param>
        private StaffDataList ExcelConvertData(string FileConnString)
        {
            StaffDataList Data = new StaffDataList();
            List<StaffData> StaffDataList = new List<StaffData>();
            List<StaffJobData> StaffJobDataList = new List<StaffJobData>();
            List<StaffRight> StaffRightDataList = new List<StaffRight>();
            if (FileConnString != "")
            {
                using (OleDbConnection Conn = new OleDbConnection(FileConnString))
                {
                    string Sql = "SELECT * FROM [人事基本資料$]";
                    using (OleDbCommand Command = new OleDbCommand(Sql, Conn))
                    {
                        Command.Connection.Open();
                        using (OleDbDataReader Reader = Command.ExecuteReader())
                        {
                            if (Reader != null && Reader.HasRows)
                            {
                                StaffData StaffData = null;
                                StaffJobData StaffJobData = null;
                                StaffRight StaffRightData = null;
                                Reader.Read();
                                Reader.Read();
                                while (Reader.Read())
                                {
                                    string Job_Status = ConvertType.String(Reader[1]);
                                    string Staff_No = ConvertType.String(Reader[2]);
                                    string Staff_Name = ConvertType.String(Reader[3]);
                                    string RID = ConvertType.String(Reader[5]);
                                    string BirthY = (ConvertType.String(Reader[6]).Length > 2) ? ConvertType.String(Reader[6]) : "0" + ConvertType.String(Reader[6]);
                                    string BirthM = (ConvertType.String(Reader[7]).Length > 1) ? ConvertType.String(Reader[7]) : "0" + ConvertType.String(Reader[7]);
                                    string BirthD = (ConvertType.String(Reader[8]).Length > 1) ? ConvertType.String(Reader[8]) : "0" + ConvertType.String(Reader[8]);
                                    string Birth = ConvertDate(BirthY + BirthM + BirthD);
                                    int Sex = (ConvertType.String(Reader[10]) == "男" ) ? 1 : 2;
                                    string Marrige = GetMarrige(ConvertType.String(Reader[11]));
                                    byte Marrige_ID = GetMarrige_ID(Marrige);
                                    string Branch_Company = ConvertType.String(Reader[12]);
                                    string Branch_Name = ConvertType.String(Reader[13]);
                                    string Job_Title = ConvertType.String(Reader[14]);
                                    string Job_TS = ConvertType.String(Reader[15]);
                                    string Svr_Name = ConvertType.String(Reader[17]);
                                    string Join_DateY = (ConvertType.String(Reader[18]).Length > 2) ? ConvertType.String(Reader[18]) : "0" + ConvertType.String(Reader[18]);
                                    string Join_DateM = (ConvertType.String(Reader[19]).Length > 1) ? ConvertType.String(Reader[19]) : "0" + ConvertType.String(Reader[19]);
                                    string Join_DateD = (ConvertType.String(Reader[20]).Length > 1) ? ConvertType.String(Reader[20]) : "0" + ConvertType.String(Reader[20]);
                                    string Join_Date = ConvertDate(Join_DateY + Join_DateM + Join_DateD);
                                    string House_Phone = (ConvertType.String(Reader[33]).Replace("-", "").Length <= 15) ? ConvertType.String(Reader[33]).Replace("-", "") : "";
                                    string Cell_Phone = ConvertType.String(Reader[34]).Replace("-", "");
                                    string[] Address = AddressParting(ConvertType.String(Reader[35]));
                                    string L_City_Name = (Address[0] != "") ? Address[0] : "";
                                    string L_Country_Name = (Address[1] != "") ? Address[1] : "";
                                    string L_Road_Name = (Address[2] != "") ? Address[2] : "";
                                    string[] HouseAddress = AddressParting(ConvertType.String(Reader[36]));
                                    string H_City_Name = (HouseAddress[0] != "") ? HouseAddress[0] : "";
                                    string H_Country_Name = (HouseAddress[1] != "") ? HouseAddress[1] : "";
                                    string H_Road_Name = (HouseAddress[2] != "") ? HouseAddress[2] : "";
                                    string Emg_Name = ConvertType.String(Reader[54]);
                                    string Emg_Relation = ConvertType.String(Reader[55]);
                                    string Emg_Cell_Phone = ConvertType.String(Reader[56]).Replace("-", "");
                                    string ExpLeaveDateY = (ConvertType.String(Reader[61]).Length > 2) ? ConvertType.String(Reader[61]) : "0" + ConvertType.String(Reader[61]);
                                    string ExpLeaveDateM = (ConvertType.String(Reader[62]).Length > 1) ? ConvertType.String(Reader[62]) : "0" + ConvertType.String(Reader[62]);
                                    string ExpLeaveDateD = (ConvertType.String(Reader[63]).Length > 1) ? ConvertType.String(Reader[63]) : "0" + ConvertType.String(Reader[63]);
                                    string ExpLeaveDate = ConvertDate(ExpLeaveDateY + ExpLeaveDateM + ExpLeaveDateD);
                                    string Depart_DateY = (ConvertType.String(Reader[64]).Length > 2) ? ConvertType.String(Reader[64]) : "0" + ConvertType.String(Reader[64]);
                                    string Depart_DateM = (ConvertType.String(Reader[65]).Length > 1) ? ConvertType.String(Reader[65]) : "0" + ConvertType.String(Reader[65]);
                                    string Depart_DateD = (ConvertType.String(Reader[66]).Length > 1) ? ConvertType.String(Reader[66]) : "0" + ConvertType.String(Reader[66]);
                                    string Depart_Date = ConvertDate(Depart_DateY + Depart_DateM + Depart_DateD);
                                    int Job_Status_ID = GetJobStatus_ID(Job_Status);
                                    int Job_TS_ID = (Job_TS == "全職") ? 2 : 1;
                                    int Branch_ID = GetBranch_ID(Branch_Name);
                                    int JobTitle_ID = GetJobTitle_ID(Job_Title);
                                    if (Job_Status != "" && Staff_No != "" && Staff_Name != "")
                                    {
                                        StaffData = new StaffData()
                                        {
                                            Staff_No = Staff_No,
                                            Name = Staff_Name,
                                            RID = RID,
                                            Birth = Birth,
                                            Marrige = Marrige,
                                            Marrige_ID = Marrige_ID,
                                            Sex = Sex,
                                            House_Phone = House_Phone,
                                            Cell_Phone = Cell_Phone,
                                            L_City_Name = L_City_Name,
                                            L_Country_Name = L_Country_Name,
                                            L_Road_Name = L_Road_Name,
                                            H_City_Name = H_City_Name,
                                            H_Country_Name = H_Country_Name,
                                            H_Road_Name = H_Road_Name,
                                            Emg_Name = Emg_Name,
                                            Emg_Relation = Emg_Relation,
                                            Emg_Cell_Phone = Emg_Cell_Phone,
                                            Branch_ID = Branch_ID,
                                            R_Job = JobTitle_ID
                                        };
                                        StaffDataList.Add(StaffData);
                                        StaffJobData = new StaffJobData()
                                        {
                                            Job_Status = Job_Status,
                                            Staff_No = Staff_No,
                                            Staff_Name = Staff_Name,
                                            Branch_Company = Branch_Company,
                                            Branch_Name = Branch_Name,
                                            Job_Title = Job_Title,
                                            Job_TS = (Job_TS == "週休" || Job_TS == "全職") ? Job_TS : "週休",
                                            Mgr_Name = "",
                                            Svr_Name = Svr_Name,
                                            Join_Date = (Join_Date != "") ? Join_Date : null,
                                            Depart_Date = (Depart_Date != "") ? Depart_Date : null,
                                            Job_Status_ID = Job_Status_ID,
                                            Job_TS_ID = Job_TS_ID,
                                            Branch_ID = Branch_ID,
                                            Job_Title_ID = JobTitle_ID,
                                            ExpLeaveDate = (ExpLeaveDate != "") ? ExpLeaveDate : null,
                                        };
                                        StaffJobDataList.Add(StaffJobData);
                                        StaffRightData = new StaffRight()
                                        {
                                            Account = Staff_No,
                                            Name = Staff_Name,
                                            IsAdmin = false,
                                            Groups = 0,
                                            Password = BaseMD5.Md5Encrypt("123456")
                                        };
                                        StaffRightDataList.Add(StaffRightData);
                                    }
                                }
                            }
                            Data.StaffData = StaffDataList;
                            Data.StaffJobData = StaffJobDataList;
                            Data.StaffRightData = StaffRightDataList;
                        }
                    }
                }
            }
            return Data;
        }

        /// <summary>
        /// 寫入員工資料
        /// </summary>
        private void WriteStaffData(List<StaffData> Input)
        {
            foreach (StaffData Item in Input)
            {
                int UpdateResult = UpdateStaffData(Item);
                Logs.FileLogs(Item.Staff_No + "|" + (UpdateResult > 0 ? "更新成功" : "更新失敗"));
                if (UpdateResult == 0)
                {
                    int InsertResult = InsertStaffData(Item);
                    Logs.FileLogs(Item.Staff_No + "|" + (InsertResult > 0 ? "新增成功": "新增失敗"));
                }
            }
        }

        /// <summary>
        /// 新增員工資料
        /// </summary>
        /// <returns></returns>
        private int InsertStaffData(StaffData Input)
        {
            string Sql = "INSERT INTO Staff_Data (Staff_No, Name, RID, Birth, Marrige, Marrige_ID, Sex, House_Phone, Cell_Phone, L_City_Name, L_Country_Name, L_Road_Name, H_City_Name, H_Country_Name, H_Road_Name, Emg_Name, Emg_Relation, Emg_Cell_Phone, Branch_ID, R_Job) VALUES (@Staff_No, @Name, @RID, @Birth, @Marrige, @Marrige_ID, @Sex, @House_Phone, @Cell_Phone, @L_City_Name, @L_Country_Name, @L_Road_Name, @H_City_Name, @H_Country_Name, @H_Road_Name, @Emg_Name, @Emg_Relation, @Emg_Cell_Phone, @Branch_ID, @R_Job)";
            return SqlMethodStaffData(Input, Sql);
        }

        /// <summary>
        /// 更新員工資料
        /// </summary>
        /// <returns></returns>
        private int UpdateStaffData(StaffData Input)
        {
            string Sql = "UPDATE Staff_Data SET Name = @Name, RID = @RID, Birth = @Birth, Marrige = @Marrige, Marrige_ID = @Marrige_ID, Sex = @Sex, House_Phone = @House_Phone, Cell_Phone = @Cell_Phone, L_City_Name = @L_City_Name, L_Country_Name = @L_Country_Name, L_Road_Name = @L_Road_Name, H_City_Name = @H_City_Name, H_Country_Name = @H_Country_Name, H_Road_Name = @H_Road_Name, Emg_Name = @Emg_Name, Emg_Relation = @Emg_Relation, Emg_Cell_Phone = @Emg_Cell_Phone, Branch_ID = @Branch_ID, R_Job = @R_Job WHERE Staff_No = @Staff_No";
            return SqlMethodStaffData(Input, Sql);
        }

        /// <summary>
        /// SQL執行員工資料
        /// </summary>
        /// <param name="Input"></param>
        private int SqlMethodStaffData(StaffData Input, string Sql)
        {
            string Staff_No = Input.Staff_No;
            string Name = Input.Name;
            string RID = Input.RID;
            string Birth = Input.Birth;
            string Marrige = Input.Marrige;
            byte Marrige_ID = Input.Marrige_ID;
            int Sex = Input.Sex;
            string House_Phone = Input.House_Phone;
            string Cell_Phone = Input.Cell_Phone;
            string L_City_Name = Input.L_City_Name;
            string L_Country_Name = Input.L_Country_Name;
            string L_Road_Name = Input.L_Road_Name;
            string H_City_Name = Input.H_City_Name;
            string H_Country_Name = Input.H_Country_Name;
            string H_Road_Name = Input.H_Road_Name;
            string Emg_Name = Input.Emg_Name;
            string Emg_Relation = Input.Emg_Relation;
            string Emg_Cell_Phone = Input.Emg_Cell_Phone;
            int Branch_ID = Input.Branch_ID;
            int R_Job = Input.R_Job;
            int Result = 0;
            Dictionary<string, object> SqlVal = new Dictionary<string, object>()
            {
                { "Staff_No", Staff_No },
                { "Name", Name },
                { "RID", RID },
                { "Birth", Birth },
                { "Marrige", Marrige },
                { "Marrige_ID", Marrige_ID },
                { "Sex", Sex },
                { "House_Phone", House_Phone },
                { "Cell_Phone", Cell_Phone },
                { "L_City_Name", L_City_Name },
                { "L_Country_Name", L_Country_Name },
                { "L_Road_Name", L_Road_Name },
                { "H_City_Name", H_City_Name },
                { "H_Country_Name", H_Country_Name },
                { "H_Road_Name", H_Road_Name },
                { "Emg_Name", Emg_Name },
                { "Emg_Relation", Emg_Relation },
                { "Emg_Cell_Phone", Emg_Cell_Phone },
                { "Branch_ID", Branch_ID },
                { "R_Job", R_Job }
            };
            using (SqlCommand Command = new SqlCommand(Sql, Conn))
            {
                foreach (KeyValuePair<string, object> Item in SqlVal)
                {
                    Command.Parameters.AddWithValue("@" + Item.Key, Item.Value ?? DBNull.Value);
                }
                try
                {
                    Result = Command.ExecuteNonQuery();
                }
                catch (Exception Ex)
                {
                    Logs.FileLogs(Staff_No + "," + Ex.ToString());
                }
            }
            return Result;
        }

        /// <summary>
        /// 寫入員工入職資料
        /// </summary>
        private void WriteStaffJobData(List<StaffJobData> Input)
        {
            foreach (StaffJobData Item in Input)
            {
                int UpdateResult = UpdateStaffJobData(Item);
                Logs.FileLogs(Item.Staff_No + "|" + (UpdateResult > 0 ? "更新成功" : "更新失敗"));
                if (UpdateResult == 0)
                {
                    int InsertResult = InsertStaffJobData(Item);
                    Logs.FileLogs(Item.Staff_No + "|" + (InsertResult > 0 ? "新增成功" : "新增失敗"));
                }
            }
        }

        /// <summary>
        /// 新增員工入職資料
        /// </summary>
        /// <returns></returns>
        private int InsertStaffJobData(StaffJobData Input)
        {
            string Sql = "INSERT INTO Staff_Job_Data (Job_Status, Staff_No, Staff_Name, Branch_Company, Branch_Name, Job_Title, Job_TS, Mgr_Name, Svr_Name, Join_Date, Depart_Date, Job_Status_ID, Job_TS_ID, Branch_ID, Job_Title_ID, ExpLeaveDate) VALUES (@Job_Status, @Staff_No, @Staff_Name, @Branch_Company, @Branch_Name, @Job_Title, @Job_TS, @Mgr_Name, @Svr_Name, @Join_Date, @Depart_Date, @Job_Status_ID, @Job_TS_ID, @Branch_ID, @Job_Title_ID, @ExpLeaveDate)";
            return SqlMethodStaffJobData(Input, Sql);
        }

        /// <summary>
        /// 更新員工入職資料
        /// </summary>
        /// <returns></returns>
        private int UpdateStaffJobData(StaffJobData Input)
        {
            string Sql = "UPDATE Staff_Job_Data SET Job_Status = @Job_Status, Staff_Name = @Staff_Name, Branch_Company = @Branch_Company, Branch_Name = @Branch_Name, Job_Title = @Job_Title, Job_TS = @Job_TS, Mgr_Name = @Mgr_Name, Svr_Name = @Svr_Name, Join_Date = @Join_Date, Depart_Date = @Depart_Date, Job_Status_ID = @Job_Status_ID, Job_TS_ID = @Job_TS_ID, Branch_ID = @Branch_ID, Job_Title_ID = @Job_Title_ID, ExpLeaveDate = @ExpLeaveDate WHERE Staff_No = @Staff_No";
            return SqlMethodStaffJobData(Input, Sql);
        }

        /// <summary>
        /// SQL執行員工入職資料
        /// </summary>
        /// <param name="Input"></param>
        private int SqlMethodStaffJobData(StaffJobData Input, string Sql)
        {
            string Job_Status = Input.Job_Status;
            string Staff_No = Input.Staff_No;
            string Staff_Name = Input.Staff_Name;
            string Branch_Company = Input.Branch_Company;
            string Branch_Name = Input.Branch_Name;
            string Job_Title = Input.Job_Title;
            string Job_TS = Input.Job_TS;
            string Mgr_Name = Input.Mgr_Name;
            string Svr_Name = Input.Svr_Name;
            string Join_Date = Input.Join_Date;
            string Depart_Date = Input.Depart_Date;
            int Job_Status_ID = Input.Job_Status_ID;
            int Job_TS_ID = Input.Job_TS_ID;
            int Branch_ID = Input.Branch_ID;
            int Job_Title_ID = Input.Job_Title_ID;
            string ExpLeaveDate = Input.ExpLeaveDate;
            int Result = 0;
            Dictionary<string, object> SqlVal = new Dictionary<string, object>()
            {
                { "Staff_No", Staff_No },
                { "Job_Status", Job_Status },
                { "Staff_Name", Staff_Name },
                { "Branch_Company", Branch_Company },
                { "Branch_Name", Branch_Name },
                { "Job_Title", Job_Title },
                { "Job_TS", Job_TS },
                { "Mgr_Name", Mgr_Name },
                { "Svr_Name", Svr_Name },
                { "Join_Date", Join_Date },
                { "Depart_Date", Depart_Date },
                { "Job_Status_ID", Job_Status_ID },
                { "Job_TS_ID", Job_TS_ID },
                { "Branch_ID", Branch_ID },
                { "Job_Title_ID", Job_Title_ID },
                { "ExpLeaveDate", ExpLeaveDate }
            };
            using (SqlCommand Command = new SqlCommand(Sql, Conn))
            {
                foreach (KeyValuePair<string, object> Item in SqlVal)
                {
                    Command.Parameters.AddWithValue("@" + Item.Key, Item.Value ?? DBNull.Value);
                }
                try
                {
                    Result = Command.ExecuteNonQuery();
                }
                catch (Exception Ex)
                {
                    Logs.FileLogs(Staff_No + "," + Ex.ToString());
                }
            }
            return Result;
        }

        /// <summary>
        /// 寫入員工預設權限
        /// </summary>
        private void WriteStaffRightData(List<StaffRight> Input)
        {
            foreach (StaffRight Item in Input)
            {
                //int UpdateResult = UpdateStaffRightData(Item);
                //if (UpdateResult == 0)
                //{
                //    int InsertResult = InsertStaffRightData(Item);
                //}
                int InsertResult = InsertStaffRightData(Item);
                Logs.FileLogs(Item.Account + "|" + (InsertResult > 0 ? "新增成功" : "新增失敗"));
            }
        }

        /// <summary>
        /// 新增員工預設權限
        /// </summary>
        /// <returns></returns>
        private int InsertStaffRightData(StaffRight Input)
        {
            string QuerySql = "SELECT ID, IIF(SR.Account IS NULL, '', SR.Account) AS Account FROM Staff_Data AS SD LEFT JOIN(SELECT Account FROM Staff_Right WHERE Account = @Account) AS SR ON SR.Account = SD.Staff_No WHERE Staff_No = @Account";
            int StaffRight_ID = 0;
            using (SqlCommand Command = new SqlCommand(QuerySql, Conn))
            {
                Command.Parameters.AddWithValue("@Account", Input.Account);
                using (SqlDataReader Reader = Command.ExecuteReader())
                {
                    if (Reader != null && Reader.HasRows)
                    {
                        while (Reader.Read())
                        {
                            if (ConvertType.String(Reader["Account"]) == "") {
                                StaffRight_ID = ConvertType.Int(Reader["ID"]);
                            }
                        }
                    }
                }
            }
            int Result = 0;
            if (StaffRight_ID > 0) {
                Input.StaffRight_ID = StaffRight_ID;
                string Sql = "INSERT INTO Staff_Right (Account, Password, Groups, IsAdmin, ID, UserName) VALUES (@Account, @Password, @Groups, @IsAdmin, @ID, @UserName)";
                Result = SqlMethodStaffRightData(Input, Sql);
            }
            return Result;
        }

        /// <summary>
        /// 更新員工預設權限
        /// </summary>
        /// <returns></returns>
        private int UpdateStaffRightData(StaffRight Input)
        {
            string Sql = "UPDATE Staff_Right SET Password = @Password, Groups = @Groups, IsAdmin = @IsAdmin, ID = @ID, UserName = @UserName WHERE Account = @Account";
            return SqlMethodStaffRightData(Input, Sql);
        }

        /// <summary>
        /// SQL執行員工預設權限
        /// </summary>
        /// <param name="Input"></param>
        private int SqlMethodStaffRightData(StaffRight Input, string Sql)
        {
            string Account = Input.Account;
            string Password = Input.Password;
            int Groups = Input.Groups;
            bool IsAdmin = Input.IsAdmin;
            int ID = Input.StaffRight_ID;
            string UserName = Input.Name;
            int Result = 0;
            Dictionary<string, object> SqlVal = new Dictionary<string, object>()
            {
                { "Account", Account },
                { "Password", Password },
                { "Groups", Groups },
                { "IsAdmin", IsAdmin },
                { "ID", ID },
                { "UserName", UserName }
            };
            using (SqlCommand Command = new SqlCommand(Sql, Conn))
            {
                foreach (KeyValuePair<string, object> Item in SqlVal)
                {
                    Command.Parameters.AddWithValue("@" + Item.Key, Item.Value ?? DBNull.Value);
                }
                try
                {
                    Result = Command.ExecuteNonQuery();
                }
                catch (Exception Ex)
                {
                    Logs.FileLogs(Account + "," + Ex.ToString());
                }
            }
            return Result;
        }

        /// <summary>
        /// 民國轉西元
        /// </summary>
        /// <param name="DateString"></param>
        /// <param name="Format"></param>
        /// <returns></returns>
        public string ConvertDate(string DateString, string Format = "yyyy/MM/dd")
        {
            string Result = "";
            if (DateString.Length == 7) {
                DateTime Date = DateTime.ParseExact(DateString, "yyyMMdd", CultureInfo.InvariantCulture).AddYears(1911);
                Result = ConvertType.String(Date.ToString(Format));
            }
            return Result;
        }

        /// <summary>
        /// 地址轉換
        /// </summary>
        /// <param name="Address"></param>
        /// <returns></returns>
        public string[] AddressParting(string Address)
        {
            string Pattern = @"(?<zipcode>(^\d{5}|^\d{3})?)(?<city>\D+[縣市])(?<district>\D+?(市區|鎮區|鎮市|[鄉鎮市區]))(?<others>.+)";
            Match Match = Regex.Match(Address, Pattern);
            string[] Result = new string[3]
            {
                ConvertType.String(Match.Groups[4]),
                ConvertType.String(Match.Groups[5]),
                ConvertType.String(Match.Groups[6])
            };
            return Result;
        }

        /// <summary>
        /// 取得婚姻名稱
        /// </summary>
        /// <param name="Marrige"></param>
        /// <returns></returns>
        private string GetMarrige(string Marrige)
        {
            if (Marrige != "已婚" || Marrige != "未婚" || Marrige != "離婚")
            {
                Marrige = "未婚";
            }
            return Marrige;
        }

        /// <summary>
        /// 取得婚姻編號
        /// </summary>
        /// <param name="Marrige"></param>
        /// <returns></returns>
        private byte GetMarrige_ID(string Marrige)
        {
            byte Result = 0;
            switch (Marrige)
            {
                case "未婚":
                    Result = 1;
                    break;
                case "離婚":
                    Result = 2;
                    break;
                default:
                    Result = 0;
                    break;
            }
            return Result;
        }

        /// <summary>
        /// 取得部門編號
        /// </summary>
        /// <param name="Branch_Name"></param>
        /// <returns></returns>
        private int GetBranch_ID(string Branch_Name)
        {
            int Result = 0;
            foreach (Dictionary<string, object> Item in BranchData)
            {
                if (Item["Branch_Name"].Equals(Branch_Name))
                {
                    Result = ConvertType.Int(Item["ID"]);
                    break;
                }
            }
            return Result;
        }

        /// <summary>
        /// 取得工作狀態編號
        /// </summary>
        /// <param name="JboStatus"></param>
        /// <returns></returns>
        private int GetJobStatus_ID(string JobStatus)
        {
            int Result = 0;
            foreach(Dictionary<string, object> Item in JobStatusData)
            {
                if (Item["Name"].Equals(JobStatus))
                {
                    Result = ConvertType.Int(Item["ID"]);
                    break;
                }
            }
            return Result;
        }

        /// <summary>
        /// 取得職稱編號
        /// </summary>
        /// <param name="JobTitle"></param>
        /// <returns></returns>
        private int GetJobTitle_ID(string JobTitle)
        {
            int Result = 21;
            foreach (Dictionary<string, object> Item in JobTitleData)
            {
                if (Item["Name"].Equals(JobTitle))
                {
                    Result = ConvertType.Int(Item["ID"]);
                    break;
                }
            }
            return Result;
        }
    }
}