﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CheckContractPrice.aspx.cs" Inherits="MommyHappySchedule.DataRebuilt.CheckContractPrice" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
     <style type="text/css">
        .auto-style1 {
            border-style: solid; 
            border-width: thin;
            width: 12%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="border-style: double; border-width: medium; width: 100%">
                <tr>
                    <td>駐點：</td>
                <td>
                    <asp:DropDownList ID="Branch" runat="server" AutoPostBack="true" OnSelectedIndexChanged="Branch_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                    <td colspan="6">
                        <asp:Label ID="Label12" runat="server"></asp:Label><asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="../Index.aspx">返回主選單</asp:HyperLink></td>
                </tr>
                <asp:Panel ID="Panel2" runat="server" Visible="false">
                <tr>
                    <td class="auto-style1">客戶姓名：</td>
                    <td class="auto-style1">
                        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></td>
                    <td class="auto-style1">合約編號：</td>
                    <td class="auto-style1">
                        <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label></td>
                    <td class="auto-style1">負責專員：</td>
                    <td class="auto-style1">
                        <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label></td>
                    <td class="auto-style1">管家人數：</td>
                    <td class="auto-style1">
                        <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label></td>
                </tr>
                <tr>
                    <td class="auto-style1">合約起始：</td>
                    <td class="auto-style1">
                        <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label></td>
                    <td class="auto-style1">合約終止：</td>
                    <td class="auto-style1">
                        <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label></td>
                    <td class="auto-style1">合約次數：</td>
                    <td class="auto-style1">
                        <asp:Label ID="Label7" runat="server" Text="1"></asp:Label></td>
                    <td class="auto-style1">合約年限：</td>
                    <td class="auto-style1">
                        <asp:Label ID="Label8" runat="server" Text="1"></asp:Label></td>
                </tr>
                </asp:Panel>
                 <tr>
                    <td colspan="8" style="background-color: #FFFFCC">
                         <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" BackColor="White"
                                BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" Font-Names="Arial" Font-Size="Smaller"
                                OnItemCommand="Show_Job_Detail" BorderStyle="None">
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <SelectedItemStyle BackColor="#669999" ForeColor="White" Font-Bold="True" />
                                <Columns>
                                    <asp:ButtonColumn DataTextField="ID" HeaderText="Type" >
                                        <ItemStyle Width="10px" />
                                    </asp:ButtonColumn>
                                    <asp:BoundColumn DataField="CustNo" HeaderText="客戶編號" >
                                        <ItemStyle Width="80px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="CustName" HeaderText="客戶姓名">
                                        <ItemStyle Width="120px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="DBCount" HeaderText="資料庫次數">
                                        <ItemStyle Width="90px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="CTCount" HeaderText="合約次數">
                                        <ItemStyle Width="90px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="NotMatch" HeaderText="次數不合">
                                        <ItemStyle Width="70px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="NotContract" HeaderText="次數非正常">
                                        <ItemStyle Width="70px" />
                                    </asp:BoundColumn>
                                </Columns>
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                <PagerStyle HorizontalAlign="Left" Mode="NumericPages" BackColor="White" ForeColor="#000066" />
                                <ItemStyle ForeColor="#000066" />
                            </asp:DataGrid>
                    </td> 
                 </tr> 
            </table>

        </div>
    </form>
</body>
</html>