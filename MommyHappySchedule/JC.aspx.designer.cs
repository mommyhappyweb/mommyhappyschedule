﻿//------------------------------------------------------------------------------
// <自動產生的>
//     這段程式碼是由工具產生的。
//
//     變更這個檔案可能會導致不正確的行為，而且如果已重新產生
//     程式碼，則會遺失變更。
// </自動產生的>
//------------------------------------------------------------------------------

namespace MommyHappySchedule {
    
    
    public partial class JC {
        
        /// <summary>
        /// Branch 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList Branch;
        
        /// <summary>
        /// Staff_Name 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox Staff_Name;
        
        /// <summary>
        /// Cust_Name 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox Cust_Name;
        
        /// <summary>
        /// Check_Report 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button Check_Report;
        
        /// <summary>
        /// Remove_Report 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button Remove_Report;
        
        /// <summary>
        /// PaymentCheck 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList PaymentCheck;
        
        /// <summary>
        /// DetermineCheck 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList DetermineCheck;
        
        /// <summary>
        /// Left_Screen 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button Left_Screen;
        
        /// <summary>
        /// Show_Date 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox Show_Date;
        
        /// <summary>
        /// Right_Screen 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button Right_Screen;
        
        /// <summary>
        /// Label2 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label2;
        
        /// <summary>
        /// Label1 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label1;
        
        /// <summary>
        /// Label4 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label4;
        
        /// <summary>
        /// Label5 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label5;
        
        /// <summary>
        /// SaveBtn 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button SaveBtn;
        
        /// <summary>
        /// DataGrid1 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.DataGrid DataGrid1;
    }
}
