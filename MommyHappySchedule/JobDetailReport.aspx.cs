﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Drawing;
using System.Configuration;
using Models.Library;
using System.Data;

namespace MommyHappySchedule
{
    public partial class JobDetailReport : System.Web.UI.Page
    {
       

        private System.Data.DataTable dt, dt1;
        private System.Data.DataRow dr, dr1;
        private System.Data.DataSet MyDataSet = new System.Data.DataSet();
        private System.Data.SqlClient.SqlDataAdapter MyCommand;
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        System.Data.SqlClient.SqlConnection INConnection1 = new System.Data.SqlClient.SqlConnection(ConnString);
        private System.Data.SqlClient.SqlCommand INCommand, INCommand1;
        System.Data.SqlClient.SqlDataReader SQLReader;
        ShareFunction SF = new ShareFunction();

        protected void Page_Load(object sender, System.EventArgs e)
        {



            // If Session("dept") <> "Finance Dept." And Session("dept") <> "IT Dept." Then
            // Session("msg") = "您沒有使用此程式的權限!!"
            // Response.Redirect("Index.aspx")
            // End If

            if (!Page.IsPostBack)
            {
                MyDataSet = new System.Data.DataSet();
                MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Branch_Data ", INConnection1);
                MyCommand.Fill(MyDataSet, "Branch");

                Branch.DataSource = MyDataSet.Tables["Branch"].DefaultView;
                Branch.DataTextField = "Branch_Name";
                Branch.DataValueField = "ID";
                Branch.DataBind();

                Check_Year.SelectedValue = DateTime.Now.Year.ToString();
                Check_Month.SelectedValue = DateTime.Now.Month.ToString ();
            }
        }

        public void Show_Sc_Type(object sender, DataGridItemEventArgs e)
        {
            ListItemType objItemType = (ListItemType)e.Item.ItemType;
            Button TempButton;
            string TmpString;
            string INString;
            System.Data.SqlClient.SqlDataReader SQLReader;

            if (objItemType == ListItemType.Item | objItemType == ListItemType.AlternatingItem)
            {
                if (((Control)sender).ID == "DataGrid1")
                {
                    TempButton = (Button)e.Item.FindControl("HK_Name");
                    if (TempButton.Text.Contains("合計") == false )
                    {
                        // 訓練
                        if (e.Item.Cells[6].Text != "&nbsp;")
                        {
                            if (SF.IsNumeric(e.Item.Cells[6].Text))
                            {
                                if (Convert.ToInt32(e.Item.Cells[6].Text) >= 12)
                                {
                                    e.Item.Cells[6].ForeColor = Color.Red;
                                    e.Item.Cells[6].Font.Bold = true;
                                }
                            }
                        }
                        // 事+家
                        int tmpInt = 0;
                        if (e.Item.Cells[12].Text != "&nbsp;")
                        {
                            if (SF.IsNumeric(e.Item.Cells[12].Text))
                                tmpInt = System.Convert.ToInt32(e.Item.Cells[12].Text);
                        }
                        if (e.Item.Cells[15].Text != "&nbsp;")
                        {
                            if (SF.IsNumeric(e.Item.Cells[15].Text))
                                tmpInt = tmpInt + System.Convert.ToInt32(e.Item.Cells[15].Text);
                        }
                        if (tmpInt >= 6)
                        {
                            e.Item.Cells[12].ForeColor = Color.Red;
                            e.Item.Cells[12].Font.Bold = true;
                            e.Item.Cells[15].ForeColor = Color.Red;
                            e.Item.Cells[15].Font.Bold = true;
                        }
                        // 病假
                        if (e.Item.Cells[13].Text != "&nbsp;")
                        {
                            if (SF.IsNumeric(e.Item.Cells[13].Text))
                            {
                                if (System.Convert.ToInt32(e.Item.Cells[13].Text) >= 6)
                                {
                                    e.Item.Cells[13].ForeColor = Color.Red;
                                    e.Item.Cells[13].Font.Bold = true;
                                }
                            }
                        }
                        // 曠職
                        if (e.Item.Cells[18].Text != "&nbsp;")
                        {
                            if (SF.IsNumeric(e.Item.Cells[18].Text))
                            {
                                if (System.Convert.ToInt32(e.Item.Cells[18].Text) > 0)
                                {
                                    e.Item.Cells[18].ForeColor = Color.Red;
                                    e.Item.Cells[18].Font.Bold = true;
                                }
                            }
                        }
                    }
                }
                else
                {
                    int iCount = 1;

                    for (iCount = 1; iCount <= 31; iCount++)
                    {
                        TempButton = (Button)e.Item.FindControl("B" + iCount.ToString());
                        // TmpString = "2018/01/" + Right(TempButton.ID, Len(TempButton.ID) - 1)
                        TmpString = Check_Year.SelectedItem.Text + "/" + (Check_Month.SelectedIndex + 1).ToString("00") + "/" +
                                    TempButton.ID.Substring(TempButton.ID.Length - 1,1);
                        // TextBox1.Text = TextBox1.Text + TmpString

                        if (SF.IsDate(TmpString))
                        {
                            INString = "SELECT  H_Date ,H_Memo FROM Holiday_Date where H_Date = '" + TmpString + "'";
                            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                            INCommand.Connection.Open();
                            SQLReader = INCommand.ExecuteReader();

                            if (SQLReader.Read())
                            {
                                TempButton.BackColor = Color.LightPink;
                                TempButton.Width = 100;
                                e.Item.Cells[iCount + 2].BackColor = Color.LightBlue;
                            }
                            else if (Convert.ToDateTime(TmpString).DayOfWeek  == DayOfWeek.Saturday  | Convert.ToDateTime(TmpString).DayOfWeek == DayOfWeek.Sunday )
                            {
                                TempButton.BackColor = Color.LightPink;
                                TempButton.Width = 100;
                                e.Item.Cells[iCount + 2].BackColor = Color.LightBlue;
                            }
                            INCommand.Connection.Close();
                        }
                    }
                }
            }
        }

        public void Show_Job_Detail(object sender, DataGridCommandEventArgs e)
        {
            Page_Load(sender, e);
        }
        public void Check_Sc(object sender, System.EventArgs e)
        {
            DataGridItem Dtemp;
            Button tmpButton;

            Dtemp = (DataGridItem)((Control)sender).Parent.Parent;

            // textbox5.Text = Dtemp.ItemIndex  '第幾筆

            tmpButton = (Button)Dtemp.FindControl("HK_Name");
            Label3.Text = tmpButton.Text + "->";
            tmpButton = (Button)Dtemp.FindControl(((Control)sender).ID);
            Label4.Text += tmpButton.Text;
            tmpButton.BackColor = Color.LightBlue;
            if (TextBox1.Text == "0")
                TextBox1.Text = string.Format(Dtemp.Cells[0].Text, "0000") + ((Control)sender).ID;
            else if (TextBox2.Text == "0")
                TextBox2.Text = string.Format(Dtemp.Cells[0].Text, "0000") + ((Control)sender).ID;
            else if (TextBox3.Text == "0")
                TextBox3.Text = string.Format(Dtemp.Cells[0].Text, "0000") + ((Control)sender).ID;
            else
                TextBox4.Text = string.Format(Dtemp.Cells[0].Text, "0000") + ((Control)sender).ID;

            Page_Load(sender, e);
        }

        protected void ChangeProduct(object sender, System.EventArgs e)
        {
            Page_Load(sender, e);
        }

        protected void Button1_Click(object sender, System.EventArgs e)
        {
            DataGrid tmpGrid;
            string tmpText, ft, st;
            int fi, si;
            Button fButton, sButton;

            tmpGrid = DataGrid1;

            if (TextBox1.Text != "0" & TextBox2.Text != "0")
            {
                // first cell
                fi = Convert.ToInt16(TextBox1.Text.Substring(0, 4));
                ft = TextBox1.Text.Substring(TextBox1.Text.Length - 4,4);
                fButton = (Button)tmpGrid.Items[fi].FindControl(ft);
                // second cell
                si = Convert.ToInt16(TextBox2.Text.Substring(0, 4));
                st = TextBox2.Text.Substring(TextBox2.Text.Length - 4,4);
                sButton = (Button)tmpGrid.Items[si].FindControl(st);
                // swap
                tmpText = fButton.Text;
                fButton.Text = sButton.Text;
                fButton.BackColor = Color.Empty;
                sButton.Text = tmpText;
                sButton.BackColor = Color.Empty;
            }

            if (TextBox1.Text != "0" & TextBox3.Text != "0")
            {
                // first cell
                fi = Convert.ToInt16(TextBox1.Text.Substring(0, 4));
                ft = TextBox1.Text.Substring(TextBox1.Text.Length - 4, 4);
                fButton = (Button)tmpGrid.Items[fi].FindControl(ft);
                // second cell
                si = Convert.ToInt16(TextBox3.Text.Substring(0, 4));
                st = TextBox3.Text.Substring(TextBox3.Text.Length - 4, 4);
                sButton = (Button)tmpGrid.Items[si].FindControl(st);
                // swap
                tmpText = fButton.Text;
                fButton.Text = sButton.Text;
                fButton.BackColor = Color.Empty;
                sButton.Text = tmpText;
                sButton.BackColor = Color.Empty;
            }

            if (TextBox1.Text != "0" & TextBox4.Text != "0")
            {
                // first cell
                fi = Convert.ToInt16(TextBox1.Text.Substring(0, 4));
                ft = TextBox1.Text.Substring(TextBox1.Text.Length - 4, 4);
                fButton = (Button)tmpGrid.Items[fi].FindControl(ft);
                // second cell
                si = Convert.ToInt16(TextBox4.Text.Substring(0, 4));
                st = TextBox4.Text.Substring(TextBox4.Text.Length - 4, 4);
                sButton = (Button)tmpGrid.Items[si].FindControl(st);
                // swap
                tmpText = fButton.Text;
                fButton.Text = sButton.Text;
                fButton.BackColor = Color.Empty;
                sButton.Text = tmpText;
                sButton.BackColor = Color.Empty;
            }
            TextBox1.Text = "0";
            TextBox2.Text = "0";
            TextBox3.Text = "0";
            TextBox4.Text = "0";

            Page_Load(sender, e);
        }

        protected void Button2_Click(object sender, System.EventArgs e)
        {
            TextBox1.Text = "0";
            TextBox2.Text = "0";
            TextBox3.Text = "0";
            TextBox4.Text = "0";
            Page_Load(sender, e);
        }

        protected void Button3_Click(object sender, System.EventArgs e)
        {
            Page_Load(sender, e);
        }

        protected void Button4_Click(object sender, System.EventArgs e)
        {
            Page_Load(sender, e);
        }

        protected void Check_Report_Click(object sender, EventArgs e)
        {
            int Start_Branch, End_Branch;
            int Ki,KKi;
            System.Data.DataView dv;

            Label2.Text = DateTime.Now.ToLongTimeString();
            if (Branch.SelectedIndex == 0)
            {
                Start_Branch = 1;
                End_Branch = 10;
                RadioButtonList1.SelectedIndex = 0;
                MultiView1.ActiveViewIndex = 0;
            }
            else
            {
                Start_Branch = Branch.SelectedIndex;
                End_Branch = Branch.SelectedIndex;
            }
            //string[] FileArray = {"南高派工明細表", "中高派工明細表", "北高派工明細表", "台南派工明細表", "南台中派工明細表", "北台中派工明細表","新竹派工明細表",
            //                      "桃園派工明細表", "中壢派工明細表", "板橋派工明細表", "旗艦派工明細表"   };
            int HC1, HC2, HC3, HC4, HC5, HC6, HC7, HC8, HC9, HC10, HC11, HC12, HC13, HC14, HC15, HC16, HC17, HC18, TCii;

            HC1 = 0;
            HC2 = 0;
            HC3 = 0;
            HC4 = 0;
            HC5 = 0;
            HC6 = 0;
            HC7 = 0;
            HC8 = 0;
            HC9 = 0;
            HC10 = 0;
            HC11 = 0;
            HC12 = 0;
            HC13 = 0;
            HC14 = 0;
            HC15 = 0;
            HC16 = 0;
            HC17 = 0;
            HC18 = 0;
            //HC19 = 0;
            TCii = 0;

            dt1 = new System.Data.DataTable();
            dt1.Columns.Add(new System.Data.DataColumn("HK_Name", typeof(string)));
            dt1.Columns.Add(new System.Data.DataColumn("職稱", typeof(string)));
            dt1.Columns.Add(new System.Data.DataColumn("空班", typeof(string)));
            dt1.Columns.Add(new System.Data.DataColumn("公班", typeof(string)));
            dt1.Columns.Add(new System.Data.DataColumn("督班", typeof(string)));
            dt1.Columns.Add(new System.Data.DataColumn("訓班", typeof(string)));
            dt1.Columns.Add(new System.Data.DataColumn("續做", typeof(string)));
            dt1.Columns.Add(new System.Data.DataColumn("補做", typeof(string)));
            dt1.Columns.Add(new System.Data.DataColumn("特假", typeof(string)));
            dt1.Columns.Add(new System.Data.DataColumn("休假", typeof(string)));
            dt1.Columns.Add(new System.Data.DataColumn("事假", typeof(string)));
            dt1.Columns.Add(new System.Data.DataColumn("病假", typeof(string)));
            dt1.Columns.Add(new System.Data.DataColumn("安假", typeof(string)));
            dt1.Columns.Add(new System.Data.DataColumn("家假", typeof(string)));
            dt1.Columns.Add(new System.Data.DataColumn("喪假", typeof(string)));
            dt1.Columns.Add(new System.Data.DataColumn("颱風", typeof(string)));
            dt1.Columns.Add(new System.Data.DataColumn("曠職", typeof(string)));
            dt1.Columns.Add(new System.Data.DataColumn("假加", typeof(string)));
            dt1.Columns.Add(new System.Data.DataColumn("出勤未服務", typeof(string)));
            dt1.Columns.Add(new System.Data.DataColumn("未出勤次數", typeof(string)));
            dt1.Columns.Add(new System.Data.DataColumn("未排", typeof(string)));
            dt1.Columns.Add(new System.Data.DataColumn("出總", typeof(string)));
            dt1.Columns.Add(new System.Data.DataColumn("假加計算", typeof(string)));



            for (KKi = Start_Branch; KKi <= End_Branch; KKi++)
            {


               TextBox1.Text = "0";
                //TextBox2.Text = "0";
                //TextBox3.Text = "0";
                //TextBox4.Text = "0";
                //Label3.Text = "";
                //Label4.Text = "";


                string Select_Banch;
                if (Branch.SelectedItem.Text.Trim() == "南高雄")
                    Select_Banch = " (Branch_Name ='南高雄' or Branch_Name = '屏東')";
                else
                    Select_Banch = " Branch_Name ='" + Branch.SelectedItem.Text.Trim() + "'";

                MyDataSet = new System.Data.DataSet();
                //找出可以相互支援的駐點名單 以  ,駐點編號, 方式判斷
                string Support_Branch = "";
                MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT ID,Branch_Name FROM Branch_Data where Support_Branch like '%," +
                    Branch.SelectedValue.ToString().Trim() + ",%'", INConnection1);
                MyCommand.Fill(MyDataSet, "Support_Branch");

                foreach (DataRowView Si in MyDataSet.Tables["Support_Branch"].DefaultView)
                    Support_Branch = Support_Branch + "'" + Si[1].ToString().Trim() + "',";
                Support_Branch = "Branch_Name in (" + Support_Branch.Substring(0, Support_Branch.Length - 1) + ")";

                string EndDate = Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/01";
                for (Ki = 1; Ki <= 31; Ki++)
                {
                    ShareFunction SF = new ShareFunction();
                    if (!SF.IsDate(Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/" + Ki.ToString()))
                    {
                        EndDate = Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/" + (Ki - 1).ToString();
                        break;
                    }
                }
                // 找出尚未離職的管家
                MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT Staff_No,Staff_Name,Job_Title FROM Staff_Job_Data_View where (Depart_Date is null or " +
                    " Depart_Date >= '" + EndDate  + "') and " + Select_Banch + " and Job_Status = '在職' " +
                    " and (not (Job_Title like '%專%' or Job_Title like '%備%' or Job_Title like '%理%' ))  Order by Staff_No", INConnection1);
                MyCommand.Fill(MyDataSet, "Staff_Nu");
                // 非客人請假的工作排程及沒有解
                MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                    "SELECT ID,Job_Date,Cust_No,Job_Result,Cust_Name,Cust_Sp_Request,Staff_No,Job_Time,Note FROM Cust_Job_Schedule_View where  (Year(Job_Date) = " +
                    Check_Year.SelectedValue +
                    " and Month(Job_Date) = " + Check_Month.SelectedValue + ") and " + Support_Branch + " and Staff_No is not null and Staff_No <> ''" +
                    " and ( Rtrim(Job_Result) = '' or Job_Result is null) and Job_Flag <> 255 Order by Staff_No,Job_Date", INConnection1);
                MyCommand.Fill(MyDataSet, "Job_Schedule");
                // 管家請假的工作排程
                MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                    "SELECT ID,Job_Date,Cust_No,Job_Result,Cust_Name,Cust_Sp_Request,Staff_No,Job_Time,Note FROM Cust_Job_Schedule_View where  (Year(Job_Date) = " +
                    Check_Year.SelectedValue +
                    " and Month(Job_Date) = " + Check_Month.SelectedValue + ")  and Staff_No is not null and Staff_No <> '' and " +
                    " Rtrim(Job_Result) <> '客' and ( Cust_No is null or Cust_No = '' ) Order by Staff_No,Job_Date", INConnection1);
                MyCommand.Fill(MyDataSet, "Day_Off_Schedule");
                
                // 
                int RCount = 1;
                //string HK_Name = ".";
                //string HKU = ".";
                //string Sheet_Name = ".";
                //string Sheet_Name_1 = ".";

                dt = new System.Data.DataTable();
                dt.Columns.Add(new System.Data.DataColumn("ID", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("HK_Name", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("Day_ID", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B1", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B2", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B3", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B4", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B5", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B6", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B7", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B8", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B9", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B10", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B11", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B12", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B13", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B14", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B15", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B16", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B17", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B18", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B19", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B20", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B21", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B22", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B23", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B24", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B25", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B26", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B27", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B28", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B29", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B30", typeof(string)));
                dt.Columns.Add(new System.Data.DataColumn("B31", typeof(string)));

                int ii;

                for (ii = 0; ii <= MyDataSet.Tables["Staff_Nu"].DefaultView.Count - 1; ii++)   // 每個管家
                {
                    for (Ki = 0; Ki <= 1; Ki++)   // 上午及下午班別
                    {
                        dr = dt.NewRow();
                        dr[0] = MyDataSet.Tables["Staff_Nu"].DefaultView[ii][2].ToString().Substring(0, 2);  // Job Title
                        dr[1] = MyDataSet.Tables["Staff_Nu"].DefaultView[ii][1].ToString() + " " + MyDataSet.Tables["Staff_Nu"].DefaultView[ii][0].ToString();
                        // Staff No & Staff Name
                        dr[2] = (Ki == 0) ? "上" : "下";


                        dv = new DataView(MyDataSet.Tables["Job_Schedule"])
                        {
                            RowFilter = "Staff_No = '" + MyDataSet.Tables["Staff_Nu"].DefaultView[ii][0].ToString() + "' and Job_Time = " + Ki
                        };
                        for (RCount = 0; RCount <= dv.Count - 1; RCount++)
                        {
                            int tmpDay = Convert.ToDateTime(dv[RCount][1]).Day; // 找出資料的日期
                            dr[2 + tmpDay] = dv[RCount][2];   // 客戶編號
                        }

                        // 管家請假排班記錄
                        dv = null;
                        dv = new DataView(MyDataSet.Tables["Day_Off_Schedule"])
                        {
                            RowFilter = "Staff_No = '" + MyDataSet.Tables["Staff_Nu"].DefaultView[ii][0].ToString() + "' and Job_Time = " + Ki
                        };
                        for (RCount = 0; RCount <= dv.Count - 1; RCount++)
                        {
                            int tmpDay = Convert.ToDateTime(dv[RCount][1]).Day; // 找出資料的日期
                            dr[2 + tmpDay] = DBNull.Value.Equals(dv[RCount][3]) ? "假" : dv[RCount][3].ToString().Trim();   // 假別
                        }
                        dt.Rows.Add(dr);
                    }
                }

                DataGrid2.DataSource = dt.DefaultView;
                DataGrid2.DataBind();
                //break;


                string INString;
                int[] HDate = new[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                int Hi = 0;

                INString = "SELECT  H_Date ,H_Memo FROM Holiday_Date where Year(H_Date) = " + Check_Year.SelectedValue + " and Month(H_Date) = " + 
                           Check_Month.SelectedValue;
                INCommand1 = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                INCommand1.Connection.Open();
                SQLReader = INCommand1.ExecuteReader();
                while (SQLReader.Read() == true)
                {
                    HDate[Hi] = Convert.ToDateTime(SQLReader[0]).Day ;
                    Hi = Hi + 1;
                }
                INCommand1.Connection.Close();

                ii = 0;
                int jj = 0;
                int C1, C2, C3, C4, C5, C6, C7, C8, C9, C10, C11, C12, C13, C14, C15, C16, C17, C18;
                int TC1, TC2, TC3, TC4, TC5, TC6, TC7, TC8, TC9, TC10, TC11, TC12, TC13, TC14, TC15, TC16, TC17, TC18;
                int BTC1, BTC2, BTC3, BTC4, BTC5, BTC6, BTC7, BTC8, BTC9, BTC10, BTC11, BTC12, BTC13, BTC14, BTC15, BTC16, BTC17, BTC18;


                string tmpString = ".";

                // 計算部門全部資料量

                BTC1 = 0;
                BTC2 = 0;
                BTC3 = 0;
                BTC4 = 0;
                BTC5 = 0;
                BTC6 = 0;
                BTC7 = 0;
                BTC8 = 0;
                BTC9 = 0;
                BTC10 = 0;
                BTC11 = 0;
                BTC12 = 0;
                BTC13 = 0;
                BTC14 = 0;
                BTC15 = 0;
                BTC16 = 0;
                BTC17 = 0;
                BTC18 = 0;
                //BTC19 = 0;

                for (jj = 0; jj <= dt.DefaultView.Count - 1; jj++)
                {
                    for (var cellNum = 3; cellNum <= 33; cellNum++) // 管家,日期及31天資料
                    {
                        tmpString = dt.DefaultView[jj][cellNum].ToString().Trim();
                        if (tmpString != "")
                        {
                            switch (tmpString)
                            {
                                case "空":
                                    {
                                        BTC1 = BTC1 + 1;
                                        break;
                                    }

                                case "公":
                                    {
                                        BTC2 = BTC2 + 1;
                                        break;
                                    }

                                case "督":
                                    {
                                        BTC3 = BTC3 + 1;
                                        break;
                                    }

                                case "訓":
                                    {
                                        BTC4 = BTC4 + 1;
                                        break;
                                    }

                                case "續":
                                    {
                                        BTC5 = BTC5 + 1;
                                        break;
                                    }

                                case "補":
                                    {
                                        BTC6 = BTC6 + 1;
                                        break;
                                    }

                                case "特":
                                    {
                                        BTC7 = BTC7 + 1;
                                        break;
                                    }

                                case "休":
                                    {
                                        BTC8 = BTC8 + 1;
                                        break;
                                    }

                                case "事":
                                    {
                                        BTC9 = BTC9 + 1;
                                        break;
                                    }

                                case "病":
                                    {
                                        BTC10 = BTC10 + 1;
                                        break;
                                    }

                                case "安":
                                    {
                                        BTC11 = BTC11 + 1;
                                        break;
                                    }

                                case "家":
                                    {
                                        BTC12 = BTC12 + 1;
                                        break;
                                    }

                                case "喪":
                                    {
                                        BTC13 = BTC13 + 1;
                                        break;
                                    }

                                case "颱":
                                    {
                                        BTC14 = BTC14 + 1;
                                        break;
                                    }

                                case "曠":
                                    {
                                        BTC15 = BTC15 + 1;
                                        break;
                                    }

                                default:
                                    {
                                        BTC18 = BTC18 + 1;
                                        int kk;
                                        for (kk = 0; kk < Hi ; kk++)
                                        {
                                            if (HDate[kk] == cellNum - 2)
                                            {
                                                BTC16 = BTC16 + 1;
                                                break;
                                            }
                                        }
                                        string DString;
                                        DString = Check_Year.SelectedItem.Text + "/" + (Check_Month.SelectedIndex + 1).ToString("00") + "/" + (cellNum - 2).ToString();
                                        if (SF.IsDate(DString))
                                        {
                                            if (Convert.ToDateTime(DString).DayOfWeek  ==  DayOfWeek.Saturday  | 
                                                Convert.ToDateTime(DString).DayOfWeek  ==  DayOfWeek.Sunday )
                                            {
                                                BTC16 = BTC16 + 1;
                                                break;
                                            }
                                        }

                                        break;
                                    }
                            }
                        }
                        else
                        {
                            // 未排任何班別,又不是例假日
                            string DString;
                            DString = Check_Year.SelectedItem.Text + "/" + (Check_Month.SelectedIndex + 1).ToString("00") + "/" + (cellNum - 2).ToString();
                            if (SF.IsDate(DString))
                            {
                                if (!(Convert.ToDateTime(DString).DayOfWeek == DayOfWeek.Saturday |  Convert.ToDateTime(DString).DayOfWeek == DayOfWeek.Sunday))
                                {
                                    int kk;
                                    int CF = 0;
                                    for (kk = 0; kk <= HDate.Length - 1; kk++)
                                    {
                                        if (HDate[kk] == cellNum - 2)
                                            CF = 1;
                                    }
                                    if (CF == 0)
                                        BTC17 = BTC17 + 1;
                                }
                            }
                        }
                    }
                }


                dr1 = dt1.NewRow();
                dr1[0] = Branch.Items[KKi].Text.Trim() + "合計";
                dr1[1] = MyDataSet.Tables["Staff_Nu"].DefaultView.Count;
                dr1[2] = (BTC1 == 0) ? "." : BTC1.ToString();
                dr1[3] = (BTC2 == 0) ? "." : BTC2.ToString();
                dr1[4] = (BTC3 == 0) ? "." : BTC3.ToString();
                dr1[5] = (BTC4 == 0) ? "." : BTC4.ToString();
                dr1[6] = (BTC5 == 0) ? "." : BTC5.ToString();
                dr1[7] = (BTC6 == 0) ? "." : BTC6.ToString();
                dr1[8] = (BTC7 == 0) ? "." : BTC7.ToString();
                dr1[9] = (BTC8 == 0) ? "." : BTC8.ToString();
                dr1[10] = (BTC9 == 0) ? "." : BTC9.ToString();
                dr1[11] = (BTC10 == 0) ? "." : BTC10.ToString();
                dr1[12] = (BTC11 == 0) ? "." : BTC11.ToString();
                dr1[13] = (BTC12 == 0) ? "." : BTC12.ToString();
                dr1[14] = (BTC13 == 0) ? "." : BTC13.ToString();
                dr1[15] = (BTC14 == 0) ? "." : BTC14.ToString();
                dr1[16] = (BTC15 == 0) ? "." : BTC15.ToString();
                dr1[17] = (BTC16 == 0) ? "." : BTC16.ToString("#,##0");
                dr1[18] = (BTC1 + BTC2 + BTC3 + BTC4 + BTC5 + BTC6).ToString("#,##0");
                dr1[19] = (BTC7 + BTC8 + BTC9 + BTC10 + BTC11 + BTC12 + BTC13 + BTC14).ToString("#,##0");
                dr1[20] = (BTC17 == 0) ? "." : BTC17.ToString("#,##0");
                dr1[21] = ((BTC18 + BTC2 + BTC3 + BTC4) == 0) ? "." : (BTC18 + BTC2 + BTC3 + BTC4).ToString("#,##0");
                dr1[22] = ((BTC16 - BTC9 - BTC10 - BTC12) == 0) ? "." : (BTC16 - BTC9 - BTC10 - BTC12).ToString("#,##0");

                dt1.Rows.Add(dr1);

                if (Branch.SelectedIndex != 0)
                {
                    TC1 = 0;
                    TC2 = 0;
                    TC3 = 0;
                    TC4 = 0;
                    TC5 = 0;
                    TC6 = 0;
                    TC7 = 0;
                    TC8 = 0;
                    TC9 = 0;
                    TC10 = 0;
                    TC11 = 0;
                    TC12 = 0;
                    TC13 = 0;
                    TC14 = 0;
                    TC15 = 0;
                    TC16 = 0;
                    TC17 = 0;
                    TC18 = 0;
                    //TC19 = 0;

                    for (ii = 0; ii <= MyDataSet.Tables["Staff_Nu"].DefaultView.Count - 1; ii++)
                    {
                        dv = new System.Data.DataView(dt)
                        {
                            RowFilter = "HK_Name like '%" + MyDataSet.Tables["Staff_Nu"].DefaultView[ii][0].ToString().Trim() + "%'"
                        };

                        C1 = 0;
                        C2 = 0;
                        C3 = 0;
                        C4 = 0;
                        C5 = 0;
                        C6 = 0;
                        C7 = 0;
                        C8 = 0;
                        C9 = 0;
                        C10 = 0;
                        C11 = 0;
                        C12 = 0;
                        C13 = 0;
                        C14 = 0;
                        C15 = 0;
                        C16 = 0;
                        C17 = 0;
                        C18 = 0;
                        //C19 = 0;

                        for (jj = 0; jj <= dv.Count - 1; jj++)
                        {
                            for (var cellNum = 3; cellNum <= 33; cellNum++) // 管家,日期及31天資料
                            {
                                tmpString = dv[jj][cellNum].ToString().Trim();
                                if (tmpString != "")
                                {
                                    switch (tmpString)
                                    {
                                        case "空":
                                            {
                                                C1 = C1 + 1;
                                                TC1 = TC1 + 1;
                                                break;
                                            }

                                        case "公":
                                            {
                                                C2 = C2 + 1;
                                                TC2 = TC2 + 1;
                                                break;
                                            }

                                        case "督":
                                            {
                                                C3 = C3 + 1;
                                                TC3 = TC3 + 1;
                                                break;
                                            }

                                        case "訓":
                                            {
                                                C4 = C4 + 1;
                                                TC4 = TC4 + 1;
                                                break;
                                            }

                                        case "續":
                                            {
                                                C5 = C5 + 1;
                                                TC5 = TC5 + 1;
                                                break;
                                            }

                                        case "補":
                                            {
                                                C6 = C6 + 1;
                                                TC6 = TC6 + 1;
                                                break;
                                            }

                                        case "特":
                                            {
                                                C7 = C7 + 1;
                                                TC7 = TC7 + 1;
                                                break;
                                            }

                                        case "休":
                                            {
                                                C8 = C8 + 1;
                                                TC8 = TC8 + 1;
                                                break;
                                            }

                                        case "事":
                                            {
                                                C9 = C9 + 1;
                                                TC9 = TC9 + 1;
                                                break;
                                            }

                                        case "病":
                                            {
                                                C10 = C10 + 1;
                                                TC10 = TC10 + 1;
                                                break;
                                            }

                                        case "安":
                                            {
                                                C11 = C11 + 1;
                                                TC11 = TC11 + 1;
                                                break;
                                            }

                                        case "家":
                                            {
                                                C12 = C12 + 1;
                                                TC12 = TC12 + 1;
                                                break;
                                            }

                                        case "喪":
                                            {
                                                C13 = C13 + 1;
                                                TC13 = TC13 + 1;
                                                break;
                                            }

                                        case "颱":
                                            {
                                                C14 = C14 + 1;
                                                TC14 = TC14 + 1;
                                                break;
                                            }

                                        case "曠":
                                            {
                                                C15 = C15 + 1;
                                                TC15 = TC15 + 1;
                                                break;
                                            }

                                        default:
                                            {
                                                C18 = C18 + 1;
                                                TC18 = TC18 + 1;
                                                int kk;
                                                for (kk = 0; kk < Hi; kk++)
                                                {
                                                    if (HDate[kk] == cellNum - 2)
                                                    {
                                                        C16 = C16 + 1;
                                                        TC16 = TC16 + 1;
                                                        break;
                                                    }
                                                }
                                                string DString;
                                                DString = Check_Year.SelectedItem.Text + "/" + (Check_Month.SelectedIndex + 1).ToString("00") + "/" + (cellNum - 2).ToString();
                                                if (SF.IsDate(DString))
                                                {
                                                    if (Convert.ToDateTime(DString).DayOfWeek == DayOfWeek.Saturday | Convert.ToDateTime(DString).DayOfWeek == DayOfWeek.Sunday)
                                                    {
                                                        C16 = C16 + 1;
                                                        TC16 = TC16 + 1;
                                                        break;
                                                    }
                                                }

                                                break;
                                            }
                                    }
                                }
                                else
                                {
                                    // 未排任何班別,又不是例假日
                                    string DString;
                                    DString = Check_Year.SelectedItem.Text + "/" + (Check_Month.SelectedIndex + 1).ToString("00") + "/" + (cellNum - 2).ToString();
                                    if (SF.IsDate(DString))
                                    {
                                        if (!(Convert.ToDateTime(DString).DayOfWeek == DayOfWeek.Saturday | Convert.ToDateTime(DString).DayOfWeek == DayOfWeek.Sunday))
                                        {
                                            int kk;
                                            int CF = 0;
                                            for (kk = 0; kk < Hi ; kk++)
                                            {
                                                if (HDate[kk] == cellNum - 2)
                                                    CF = 1;
                                            }
                                            if (CF == 0)
                                            {
                                                C17 = C17 + 1;
                                                TC17 = TC17 + 1;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (Branch.SelectedIndex == 0)
                        {
                        }
                        else
                        {
                            dr1 = dt1.NewRow();
                            dr1[0] = MyDataSet.Tables["Staff_Nu"].DefaultView[ii][0].ToString();
                            dr1[1] = MyDataSet.Tables["Staff_Nu"].DefaultView[ii][1].ToString();
                            dr1[2] = (C1 == 0) ? "." : C1.ToString();      // 空
                            dr1[3] = (C2 == 0) ? "." : C2.ToString();      // 公
                            dr1[4] = (C3 == 0) ? "." : C3.ToString();      // 督
                            dr1[5] = (C4 == 0) ? "." : C4.ToString();      // 訓
                            dr1[6] = (C5 == 0) ? "." : C5.ToString();      // 續
                            dr1[7] = (C6 == 0) ? "." : C6.ToString();      // 補
                            dr1[8] = (C7 == 0) ? "." : C7.ToString();      // 特
                            dr1[9] = (C8 == 0) ? "." : C8.ToString();      // 休
                            dr1[10] = (C9 == 0) ? "." : C9.ToString();     // 事
                            dr1[11] = (C10 == 0) ? "." : C10.ToString();   // 病
                            dr1[12] = (C11 == 0) ? "." : C11.ToString();   // 安
                            dr1[13] = (C12 == 0) ? "." : C12.ToString();   // 家
                            dr1[14] = (C13 == 0) ? "." : C13.ToString();   // 喪
                            dr1[15] = (C14 == 0) ? "." : C14.ToString();   // 颱
                            dr1[16] = (C15 == 0) ? "." : C15.ToString();   // 曠
                            dr1[17] = (C16 == 0) ? "." : C16.ToString();   // 假加 
                            dr1[18] = (C1 + C2 + C3 + C4 + C5 + C6).ToString();  // 出勤未服務
                            dr1[19] = (C7 + C8 + C9 + C10 + C11 + C12 + C13 + C14).ToString();   // 未出勤次𢿙
                            dr1[20] = (C17 == 0) ? "." : C17.ToString();   // 未排班
                            dr1[21] = ((C18 + C2 + C3 + C4) == 0) ? "." : (C18 + C2 + C3 + C4).ToString();   // 出總總時數要加上 督、公、訓
                            dr1[22] = ((C16 - C9 - C10 - C12) == 0) ? "." : (C16 - C9 - C10 - C12).ToString();   // 假加計算 要先減去,事,病,家

                            dt1.Rows.Add(dr1);
                        }
                    }



                    // 不在員工基本資料表中
                    TC1 = BTC1 - TC1;
                    TC2 = BTC2 - TC2;
                    TC3 = BTC3 - TC3;
                    TC4 = BTC4 - TC4;
                    TC5 = BTC5 - TC5;
                    TC6 = BTC6 - TC6;
                    TC7 = BTC7 - TC7;
                    TC8 = BTC8 - TC8;
                    TC9 = BTC9 - TC9;
                    TC10 = BTC10 - TC10;
                    TC11 = BTC11 - TC11;
                    TC12 = BTC12 - TC12;
                    TC13 = BTC13 - TC13;
                    TC14 = BTC14 - TC14;
                    TC15 = BTC15 - TC15;
                    TC16 = BTC16 - TC16;
                    TC17 = BTC17 - TC17;
                    TC18 = BTC18 - TC18;
                    // -------------------------------------------------------------------------------------------------------------------------------------
                    dr1 = dt1.NewRow();
                    dr1[0] = "其它員工";
                    dr1[1] = "";
                    dr1[2] = (TC1 == 0) ? "." : TC1.ToString();
                    dr1[3] = (TC2 == 0) ? "." : TC2.ToString();
                    dr1[4] = (TC3 == 0) ? "." : TC3.ToString();
                    dr1[5] = (TC4 == 0) ? "." : TC4.ToString();
                    dr1[6] = (TC5 == 0) ? "." : TC5.ToString();
                    dr1[7] = (TC6 == 0) ? "." : TC6.ToString();
                    dr1[8] = (TC7 == 0) ? "." : TC7.ToString();
                    dr1[9] = (TC8 == 0) ? "." : TC8.ToString();
                    dr1[10] = (TC9 == 0) ? "." : TC9.ToString();
                    dr1[11] = (TC10 == 0) ? "." : TC10.ToString();
                    dr1[12] = (TC11 == 0) ? "." : TC11.ToString();
                    dr1[13] = (TC12 == 0) ? "." : TC12.ToString();
                    dr1[14] = (TC13 == 0) ? "." : TC13.ToString();
                    dr1[15] = (TC14 == 0) ? "." : TC14.ToString();
                    dr1[16] = (TC15 == 0) ? "." : TC15.ToString();
                    dr1[17] = (TC16 == 0) ? "." : TC16.ToString();
                    dr1[18] = (TC1 + TC2 + TC3 + TC4 + TC5 + TC6).ToString();
                    dr1[19] = (TC7 + TC8 + TC9 + TC10 + TC11 + TC12 + TC13 + TC14).ToString();
                    dr1[20] = (TC17 == 0) ? "." : TC17.ToString();
                    dr1[21] = ((TC18 + TC2 + TC3 + TC4) == 0) ? "." : (TC18 + TC2 + TC3 + TC4).ToString();
                    dr1[22] = ((TC16 - TC9 - TC10 - TC12) == 0) ? "." : (TC16 - TC9 - TC10 - TC12).ToString();

                    dt1.Rows.Add(dr1);
                }

                if (Branch.SelectedIndex == 0)
                {
                    TCii = TCii + MyDataSet.Tables["Staff_Nu"].DefaultView.Count;
                    HC1 = HC1 + BTC1;
                    HC2 = HC2 + BTC2;
                    HC3 = HC3 + BTC3;
                    HC4 = HC4 + BTC4;
                    HC5 = HC5 + BTC5;
                    HC6 = HC6 + BTC6;
                    HC7 = HC7 + BTC7;
                    HC8 = HC8 + BTC8;
                    HC9 = HC9 + BTC9;
                    HC10 = HC10 + BTC10;
                    HC11 = HC11 + BTC11;
                    HC12 = HC12 + BTC12;
                    HC13 = HC13 + BTC13;
                    HC14 = HC14 + BTC14;
                    HC15 = HC15 + BTC15;
                    HC16 = HC16 + BTC16;
                    HC17 = HC17 + BTC17;
                    HC18 = HC18 + BTC18;
                }
            }

            if (Branch.SelectedIndex == 0)
            {
                dr1 = dt1.NewRow();
                dr1[0] = "總公司合計";
                dr1[1] = TCii.ToString();
                dr1[2] = (HC1 == 0) ? "." : HC1.ToString();
                dr1[3] = (HC2 == 0) ? "." : HC2.ToString();
                dr1[4] = (HC3 == 0) ? "." : HC3.ToString();
                dr1[5] = (HC4 == 0) ? "." : HC4.ToString();
                dr1[6] = (HC5 == 0) ? "." : HC5.ToString();
                dr1[7] = (HC6 == 0) ? "." : HC6.ToString();
                dr1[8] = (HC7 == 0) ? "." : HC7.ToString();
                dr1[9] = (HC8 == 0) ? "." : HC8.ToString();
                dr1[10] = (HC9 == 0) ? "." : HC9.ToString();
                dr1[11] = (HC10 == 0) ? "." : HC10.ToString();
                dr1[12] = (HC11 == 0) ? "." : HC11.ToString();
                dr1[13] = (HC12 == 0) ? "." : HC12.ToString();
                dr1[14] = (HC13 == 0) ? "." : HC13.ToString();
                dr1[15] = (HC14 == 0) ? "." : HC14.ToString();
                dr1[16] = (HC15 == 0) ? "." : HC15.ToString();
                dr1[17] = (HC16 == 0) ? "." : HC16.ToString("#,##0");
                dr1[18] = (HC1 + HC2 + HC3 + HC4 + HC5 + HC6).ToString("#,##0");
                dr1[19] = (HC7 + HC8 + HC9 + HC10 + HC11 + HC12 + HC13 + HC14).ToString("#,##0");
                dr1[20] = (HC17 == 0) ? "." : HC17.ToString("#,##0");
                dr1[21] = ((HC18 + HC2 + HC3 + HC4) == 0) ? "." : (HC18 + HC2 + HC3 + HC4).ToString("#,##0");
                dr1[22] = ((HC16 - HC9 - HC10 - HC12) == 0) ? "." : (HC16 - HC9 - HC10 - HC12).ToString("#,##0");

                dt1.Rows.InsertAt(dr1, 0);
            }
            DataGrid1.DataSource = dt1.DefaultView;
            DataGrid1.DataBind();

            Label2.Text = Label2.Text + "-" + DateTime.Now.ToLongTimeString();
            Page_Load(sender, e);
        }
        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Branch.SelectedIndex == 0)
            {
                MultiView1.ActiveViewIndex = 0;
                RadioButtonList1.SelectedIndex = 0;
            }
            else
                MultiView1.ActiveViewIndex = RadioButtonList1.SelectedIndex;

            Page_Load(sender, e);
        }
        protected void DataGrid2_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            DataGrid2.CurrentPageIndex = e.NewPageIndex;
            // DataGrid2.DataBind()
            Check_Report_Click(source, e);
        }
    }

}