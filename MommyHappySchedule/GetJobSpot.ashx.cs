﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using System.Configuration;

namespace MommyHappySchedule
{

    /// <summary>
    /// GetJobSpot 的摘要描述
    /// </summary>
    public class GetJobSpot : IHttpHandler
    {
        int zip_no = 1;//南高
        int time_no = (DateTime.Now.Hour * 60 + DateTime.Now.Minute > ( 12 * 60 + 30 )) ? 1 : 0;   //如果超過12:30則顯示下午班
        
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        SqlConnection INConnection = new SqlConnection(ConnString);

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (!string.IsNullOrEmpty(context.Request["zip_no"]))
            {
                int.TryParse(context.Request["zip_no"], out this.zip_no);//防SQL Injection，轉型別失敗就用預設值
            }

            if (!string.IsNullOrEmpty(context.Request["time_no"]))
            {
                int.TryParse(context.Request["time_no"], out this.time_no);//防SQL Injection，轉型別失敗就用預設值

            }

            //取得管家名單原始資料
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                SqlCommand command = new SqlCommand(
                                     @"SELECT Staff_No, Staff_Name, Job_Title FROM Staff_Job_Data_View where (Depart_Date is null or" +
                                     " Depart_Date >= '" +  DateTime.Now.ToShortDateString() + @"') and Branch_ID = " +
                                     (this.zip_no) + @" and Staff_No <> '' and Staff_No is not null  Order by Staff_No", conn);
                conn.Open();
                SqlDataReader SqlReader = command.ExecuteReader();
                string Select_Staff = "";

                while(SqlReader.Read())
                {
                    Select_Staff = Select_Staff + "'" + SqlReader[0].ToString().Trim() + "',";
                }
                if (Select_Staff.Length > 0)
                    Select_Staff = " Staff_No in (" + Select_Staff.Substring(0, Select_Staff.Length - 1) + ")";
                
                // Call Close when done reading.
                SqlReader.Close();
            }

            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                SqlDataAdapter da;
                if (this.time_no == 2)
                {
                    if(this.zip_no == 0 )
                    {
                        da = new SqlDataAdapter(
                        @"SELECT  Job_Flag as Type,Concat(ltrim(rtrim(Staff_No)),'-',ltrim(rtrim(Staff_Name))) as No,
                        Concat(ltrim(rtrim(Cust_No)),'-',ltrim(rtrim(Cust_Name))) as Name,HK_Serial as Addr,
                        LEFT(Cust_Geometry,CHARINDEX(',',Cust_Geometry)-1) as lat
                        ,right(rtrim(Cust_Geometry),len(Cust_Geometry)-CHARINDEX(',',Cust_Geometry)) as lng,Cust_Geometry
                        FROM Job_Schedule_Confirm_View Where Job_Flag >=2 and Job_Flag <=4 and Cust_Geometry is not null and
                        Cust_Geometry <> '' and Job_Date = '" + DateTime.Now.ToShortDateString() + @"' Order by No ASC", conn);
                    }
                    else
                    {
                        da = new SqlDataAdapter(
                        @"SELECT  Job_Flag as Type,Concat(ltrim(rtrim(Staff_No)),'-',ltrim(rtrim(Staff_Name))) as No,
                        Concat(ltrim(rtrim(Cust_No)),'-',ltrim(rtrim(Cust_Name))) as Name,HK_Serial as Addr,
                        LEFT(Cust_Geometry,CHARINDEX(',',Cust_Geometry)-1) as lat
                        ,right(rtrim(Cust_Geometry),len(Cust_Geometry)-CHARINDEX(',',Cust_Geometry)) as lng,Cust_Geometry
                        FROM Job_Schedule_Confirm_View Where Job_Flag >=2 and Job_Flag <=4 and Cust_Geometry is not null and
                        Cust_Geometry <> '' and Job_Date = '" + DateTime.Now.ToShortDateString() + @"' and Branch_ID = " +
                        (this.zip_no) + @" Order by No ASC", conn);
                    }
                    
                }
                else
                {
                    if(this.zip_no== 0)
                    {
                        da = new SqlDataAdapter(
                             @"SELECT  Job_Flag as Type,Concat(ltrim(rtrim(Staff_No)),'-',ltrim(rtrim(Staff_Name))) as No,
                             Concat(ltrim(rtrim(Cust_No)),'-',ltrim(rtrim(Cust_Name))) as Name,HK_Serial as Addr,
                             LEFT(Cust_Geometry,CHARINDEX(',',Cust_Geometry)-1) as lat
                             ,right(rtrim(Cust_Geometry),len(Cust_Geometry)-CHARINDEX(',',Cust_Geometry)) as lng,Cell_Phone as Cust_Geometry
                             FROM Job_Schedule_Confirm_View Where Job_Flag >=2 and Job_Flag <=4 and Cust_Geometry is not null and
                             Cust_Geometry <> '' and Job_Date = '" + DateTime.Now.ToShortDateString() + @"' and Job_Time = " + 
                             (this.time_no) + @" Order by No ASC", conn);
                    }
                    else
                    {
                        da = new SqlDataAdapter(
                             @"SELECT  Job_Flag as Type,Concat(ltrim(rtrim(Staff_No)),'-',ltrim(rtrim(Staff_Name))) as No,
                             Concat(ltrim(rtrim(Cust_No)),'-',ltrim(rtrim(Cust_Name))) as Name,HK_Serial as Addr,
                             LEFT(Cust_Geometry,CHARINDEX(',',Cust_Geometry)-1) as lat
                             ,right(rtrim(Cust_Geometry),len(Cust_Geometry)-CHARINDEX(',',Cust_Geometry)) as lng,Cell_Phone as Cust_Geometry
                             FROM Job_Schedule_Confirm_View Where Job_Flag >=2 and Job_Flag <=4 and Cust_Geometry is not null and
                             Cust_Geometry <> '' and Job_Date = '" + DateTime.Now.ToShortDateString() + @"' and Branch_ID = " +
                             (this.zip_no) + @" and Job_Time = " + (this.time_no) + @" Order by No ASC", conn);
                    }
                   
                }
                  da.Fill(ds);

                //公督訓
                if (this.time_no == 2)
                {
                    if (this.zip_no == 0)
                    {
                        da = new SqlDataAdapter(
                        @"SELECT  Job_Flag as Type,Concat(ltrim(rtrim(Staff_No)),'-',ltrim(rtrim(Staff_Name))) as No,
                        Concat(ltrim(rtrim(Cust_No)),'-',ltrim(rtrim(Cust_Name))) as Name,10 as Addr,
                        LEFT(Cust_Geometry,CHARINDEX(',',Cust_Geometry)-1) as lat
                        ,right(rtrim(Cust_Geometry),len(Cust_Geometry)-CHARINDEX(',',Cust_Geometry)) as lng,Cust_Geometry
                        FROM Job_Schedule_Supervision_Confirm_View Where Job_Flag >=2 and Job_Flag <=4 and Cust_Geometry is not null and
                        Cust_Geometry <> '' and Job_Date = '" + DateTime.Now.ToShortDateString() + @"' Order by No ASC", conn);
                    }
                    else
                    {
                        da = new SqlDataAdapter(
                        @"SELECT  Job_Flag as Type,Concat(ltrim(rtrim(Staff_No)),'-',ltrim(rtrim(Staff_Name))) as No,
                        Concat(ltrim(rtrim(Cust_No)),'-',ltrim(rtrim(Cust_Name))) as Name,10 as Addr,
                        LEFT(Cust_Geometry,CHARINDEX(',',Cust_Geometry)-1) as lat
                        ,right(rtrim(Cust_Geometry),len(Cust_Geometry)-CHARINDEX(',',Cust_Geometry)) as lng,Cust_Geometry
                        FROM Job_Schedule_Supervision_Confirm_View Where Job_Flag >=2 and Job_Flag <=4 and Cust_Geometry is not null and
                        Cust_Geometry <> '' and Job_Date = '" + DateTime.Now.ToShortDateString() + @"' and Branch_ID = " +
                        (this.zip_no) + @" Order by No ASC", conn);
                    }

                }
                else
                {
                    if (this.zip_no == 0)
                    {
                        da = new SqlDataAdapter(
                             @"SELECT  Job_Flag as Type,Concat(ltrim(rtrim(Staff_No)),'-',ltrim(rtrim(Staff_Name))) as No,
                             Concat(ltrim(rtrim(Cust_No)),'-',ltrim(rtrim(Cust_Name))) as Name,10 as Addr,
                             LEFT(Cust_Geometry,CHARINDEX(',',Cust_Geometry)-1) as lat
                             ,right(rtrim(Cust_Geometry),len(Cust_Geometry)-CHARINDEX(',',Cust_Geometry)) as lng,Cell_Phone as Cust_Geometry
                             FROM Job_Schedule_Supervision_Confirm_View Where Job_Flag >=2 and Job_Flag <=4 and Cust_Geometry is not null and
                             Cust_Geometry <> '' and Job_Date = '" + DateTime.Now.ToShortDateString() + @"' and Job_Time = " +
                             (this.time_no) + @" Order by No ASC", conn);
                    }
                    else
                    {
                        da = new SqlDataAdapter(
                             @"SELECT  Job_Flag as Type,Concat(ltrim(rtrim(Staff_No)),'-',ltrim(rtrim(Staff_Name))) as No,
                             Concat(ltrim(rtrim(Cust_No)),'-',ltrim(rtrim(Cust_Name))) as Name,10 as Addr,
                             LEFT(Cust_Geometry,CHARINDEX(',',Cust_Geometry)-1) as lat
                             ,right(rtrim(Cust_Geometry),len(Cust_Geometry)-CHARINDEX(',',Cust_Geometry)) as lng,Cell_Phone as Cust_Geometry
                             FROM Job_Schedule_Supervision_Confirm_View Where Job_Flag >=2 and Job_Flag <=4 and Cust_Geometry is not null and
                             Cust_Geometry <> '' and Job_Date = '" + DateTime.Now.ToShortDateString() + @"' and Branch_ID = " +
                             (this.zip_no) + @" and Job_Time = " + (this.time_no) + @" Order by No ASC", conn);
                    }

                }
                da.Fill(ds);
            }
            DataTable dt = ds.Tables[0];

            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                SqlCommand command = new SqlCommand(@"Select ID,Branch_Name,Email,Address,Geometry,LEFT(Geometry,CHARINDEX(',',Geometry)-1) as lat
                                                   ,right(Rtrim(Geometry),len(Geometry)-CHARINDEX(',',Geometry)) as lng from Branch_Data where ID = " + (this.zip_no), conn);
                conn.Open();
                SqlDataReader SqlReader = command.ExecuteReader();

                // Call Read before accessing data.
                if (SqlReader.Read())
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = 1;
                    dr[1] = SqlReader[1].ToString().Trim();
                    dr[2] = SqlReader[1].ToString().Trim();
                    dr[3] = 1;//SqlReader[3].ToString().Trim();
                    dr[4] = SqlReader[5].ToString().Trim();
                    dr[5] = SqlReader[6].ToString().Trim();
                    dr[6] = SqlReader[4].ToString().Trim();
                    dt.Rows.InsertAt(dr, 0);
                }

                // Call Close when done reading.
                SqlReader.Close();
            }

            //因為本範例的資料都沒有緯度和經度，所以把原始資料DataTable傳入取得一個新的DataTable(有緯度、經度的)
            //利用Json.NET將DataTable轉成JSON字串，請參考另一篇文章：http://www.dotblogs.com.tw/shadow/archive/2011/11/30/60083.aspx
            //string str_json = JsonConvert.SerializeObject(dt, Formatting.Indented);
            var addr_json = new List<Addr_Json>();

            int i;
            for (i = 0; i < dt.DefaultView.Count; i++)
            {
                Addr_Json tmpAddr = new Addr_Json
                {
                    Type = Convert.ToInt16(dt.Rows[i][0]),
                    No = (string)dt.Rows[i][1],
                    Name = (string)dt.Rows[i][2],
                    Addr = dt.Rows[i][3].ToString(),
                    lat = (string)dt.Rows[i][4],
                    lng = (string)dt.Rows[i][5]
                };
                if (tmpAddr.Geometry != null) {
                    tmpAddr.Geometry = (string)dt.Rows[i][6];
                }
                addr_json.Add(tmpAddr);
            }
            var serializer = new JavaScriptSerializer();
            string str_json = serializer.Serialize(addr_json);
            context.Response.Write(str_json);   
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}