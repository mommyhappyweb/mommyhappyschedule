﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Configuration;
using Models.Library;

namespace MommyHappySchedule
{
    public partial class ReNewReport : Page
    {

        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        SqlConnection INConnection1 = new SqlConnection(ConnString);
        private SqlCommand INCommand;
        private SqlDataReader SQLReader;
        private DataSet MyDataSet;
        private SqlDataAdapter MyCommand;

        protected void Page_Load(object sender, EventArgs e)
        {
            //if ((string)Session["account"] == "" | (string)Session["Username"] != "mommyhappy")
            //{
            //    Session["msg"] = "您沒有使用此程式的權限!!";
            //    Response.Redirect("Index.aspx");
            //}

            if (!IsPostBack)
            {
                MyDataSet = new DataSet();
                MyCommand = new SqlDataAdapter("SELECT * FROM Branch_Data", INConnection1);
                MyCommand.Fill(MyDataSet, "Branch");

                Branch.DataSource = MyDataSet.Tables["Branch"].DefaultView;
                Branch.DataTextField = "Branch_Name";
                Branch.DataValueField = "ID";
                Branch.DataBind();

                Check_Year.SelectedValue = DateTime.Now.Year.ToString();
                Check_Month.SelectedValue = DateTime.Now.Month.ToString();
            }
        }

        protected void Check_Report_Click(object sender, EventArgs e)
        {
            int CToday = DateTime.Now.Year;
            DataTable dt;
            DataRow dr;
            string Select_Banch;
            int Start_Banch, End_Banch;


            //顯示月份選項
            Check_Month.Visible = true;

            // 專員合約績效表
            dt = new DataTable();
            dt.Columns.Add(new DataColumn("ID", typeof(string)));
            dt.Columns.Add(new DataColumn("EX_Name", typeof(string)));

            dt.Columns.Add(new DataColumn("Cr_Cu_T", typeof(string)));   // 負責客數
            dt.Columns.Add(new DataColumn("Cr_Cn_T", typeof(string)));   // 負責次數
            dt.Columns.Add(new DataColumn("Ne_Cn_T", typeof(string)));   // 新約次數
            dt.Columns.Add(new DataColumn("Cl_Cn_T", typeof(string)));   // 解約次數
            dt.Columns.Add(new DataColumn("Gr_Cn_T", typeof(string)));   // 成長次數
            dt.Columns.Add(new DataColumn("Ex_Cu_T", typeof(string)));   // 到期客戶
            dt.Columns.Add(new DataColumn("Ex_Cn_T", typeof(string)));   // 到期次數
            dt.Columns.Add(new DataColumn("Re_Cu_T", typeof(string)));   // 續約筆數
            dt.Columns.Add(new DataColumn("Cl_Cu_T", typeof(string)));   // 解約筆數
            dt.Columns.Add(new DataColumn("Ct_Cu_T", typeof(string)));   // 中途解約
            dt.Columns.Add(new DataColumn("NF_Cu_T", typeof(string)));   // 合約異動
            dt.Columns.Add(new DataColumn("Tw_Cn_T", typeof(string)));   // 二年約數
            dt.Columns.Add(new DataColumn("Rc_Cu_T", typeof(string)));   // 解約客率
            dt.Columns.Add(new DataColumn("Rc_Cn_T", typeof(string)));   // 解約次率
            dt.Columns.Add(new DataColumn("Rn_Cu_T", typeof(string)));   // 續約進度

            dt.Columns.Add(new DataColumn("Cr_Cu_1", typeof(string)));   // 負責客數
            dt.Columns.Add(new DataColumn("Cr_Cn_1", typeof(string)));   // 負責次數
            dt.Columns.Add(new DataColumn("Ne_Cn_1", typeof(string)));   // 新約次數
            dt.Columns.Add(new DataColumn("Cl_Cn_1", typeof(string)));   // 解約次數
            dt.Columns.Add(new DataColumn("Gr_Cn_1", typeof(string)));   // 成長次數
            dt.Columns.Add(new DataColumn("Ex_Cu_1", typeof(string)));   // 到期客戶
            dt.Columns.Add(new DataColumn("Ex_Cn_1", typeof(string)));   // 到期次數
            dt.Columns.Add(new DataColumn("Re_Cu_1", typeof(string)));   // 續約筆數
            dt.Columns.Add(new DataColumn("Cl_Cu_1", typeof(string)));   // 解約筆數
            dt.Columns.Add(new DataColumn("Ct_Cu_1", typeof(string)));   // 中途解約
            dt.Columns.Add(new DataColumn("NF_Cu_1", typeof(string)));   // 合約異動
            dt.Columns.Add(new DataColumn("Tw_Cn_1", typeof(string)));   // 二年約數
            dt.Columns.Add(new DataColumn("Rc_Cu_1", typeof(string)));   // 解約客率
            dt.Columns.Add(new DataColumn("Rc_Cn_1", typeof(string)));   // 解約次率
            dt.Columns.Add(new DataColumn("Rn_Cu_1", typeof(string)));   // 續約進度

            dt.Columns.Add(new DataColumn("Cr_Cu_2", typeof(string)));   // 負責客數
            dt.Columns.Add(new DataColumn("Cr_Cn_2", typeof(string)));   // 負責次數
            dt.Columns.Add(new DataColumn("Ne_Cn_2", typeof(string)));   // 新約次數
            dt.Columns.Add(new DataColumn("Cl_Cn_2", typeof(string)));   // 解約次數
            dt.Columns.Add(new DataColumn("Gr_Cn_2", typeof(string)));   // 成長次數
            dt.Columns.Add(new DataColumn("Ex_Cu_2", typeof(string)));   // 到期客戶
            dt.Columns.Add(new DataColumn("Ex_Cn_2", typeof(string)));   // 到期次數
            dt.Columns.Add(new DataColumn("Re_Cu_2", typeof(string)));   // 續約筆數
            dt.Columns.Add(new DataColumn("Cl_Cu_2", typeof(string)));   // 解約筆數
            dt.Columns.Add(new DataColumn("Ct_Cu_2", typeof(string)));   // 中途解約
            dt.Columns.Add(new DataColumn("NF_Cu_2", typeof(string)));   // 合約異動
            dt.Columns.Add(new DataColumn("Tw_Cn_2", typeof(string)));   // 二年約數
            dt.Columns.Add(new DataColumn("Rc_Cu_2", typeof(string)));   // 解約客率
            dt.Columns.Add(new DataColumn("Rc_Cn_2", typeof(string)));   // 解約次率
            dt.Columns.Add(new DataColumn("Rn_Cu_2", typeof(string)));   // 續約進度

            dt.Columns.Add(new DataColumn("Cr_Cu_3", typeof(string)));   // 負責客數
            dt.Columns.Add(new DataColumn("Cr_Cn_3", typeof(string)));   // 負責次數
            dt.Columns.Add(new DataColumn("Ne_Cn_3", typeof(string)));   // 新約次數
            dt.Columns.Add(new DataColumn("Cl_Cn_3", typeof(string)));   // 解約次數
            dt.Columns.Add(new DataColumn("Gr_Cn_3", typeof(string)));   // 成長次數
            dt.Columns.Add(new DataColumn("Ex_Cu_3", typeof(string)));   // 到期客戶
            dt.Columns.Add(new DataColumn("Ex_Cn_3", typeof(string)));   // 到期次數
            dt.Columns.Add(new DataColumn("Re_Cu_3", typeof(string)));   // 續約筆數
            dt.Columns.Add(new DataColumn("Cl_Cu_3", typeof(string)));   // 解約筆數
            dt.Columns.Add(new DataColumn("Ct_Cu_3", typeof(string)));   // 中途解約
            dt.Columns.Add(new DataColumn("NF_Cu_3", typeof(string)));   // 合約異動
            dt.Columns.Add(new DataColumn("Tw_Cn_3", typeof(string)));   // 二年約數
            dt.Columns.Add(new DataColumn("Rc_Cu_3", typeof(string)));   // 解約客率
            dt.Columns.Add(new DataColumn("Rc_Cn_3", typeof(string)));   // 解約次率
            dt.Columns.Add(new DataColumn("Rn_Cu_3", typeof(string)));   // 續約進度

            dt.Columns.Add(new DataColumn("Cr_Cu_4", typeof(string)));   // 負責客數
            dt.Columns.Add(new DataColumn("Cr_Cn_4", typeof(string)));   // 負責次數
            dt.Columns.Add(new DataColumn("Ne_Cn_4", typeof(string)));   // 新約次數
            dt.Columns.Add(new DataColumn("Cl_Cn_4", typeof(string)));   // 解約次數
            dt.Columns.Add(new DataColumn("Gr_Cn_4", typeof(string)));   // 成長次數
            dt.Columns.Add(new DataColumn("Ex_Cu_4", typeof(string)));   // 到期客戶
            dt.Columns.Add(new DataColumn("Ex_Cn_4", typeof(string)));   // 到期次數
            dt.Columns.Add(new DataColumn("Re_Cu_4", typeof(string)));   // 續約筆數
            dt.Columns.Add(new DataColumn("Cl_Cu_4", typeof(string)));   // 解約筆數
            dt.Columns.Add(new DataColumn("Ct_Cu_4", typeof(string)));   // 中途解約
            dt.Columns.Add(new DataColumn("NF_Cu_4", typeof(string)));   // 合約異動
            dt.Columns.Add(new DataColumn("Tw_Cn_4", typeof(string)));   // 二年約數
            dt.Columns.Add(new DataColumn("Rc_Cu_4", typeof(string)));   // 解約客率
            dt.Columns.Add(new DataColumn("Rc_Cn_4", typeof(string)));   // 解約次率
            dt.Columns.Add(new DataColumn("Rn_Cu_4", typeof(string)));   // 續約進度

            dt.Columns.Add(new DataColumn("Cr_Cu_5", typeof(string)));   // 負責客數
            dt.Columns.Add(new DataColumn("Cr_Cn_5", typeof(string)));   // 負責次數
            dt.Columns.Add(new DataColumn("Ne_Cn_5", typeof(string)));   // 新約次數
            dt.Columns.Add(new DataColumn("Cl_Cn_5", typeof(string)));   // 解約次數
            dt.Columns.Add(new DataColumn("Gr_Cn_5", typeof(string)));   // 成長次數
            dt.Columns.Add(new DataColumn("Ex_Cu_5", typeof(string)));   // 到期客戶
            dt.Columns.Add(new DataColumn("Ex_Cn_5", typeof(string)));   // 到期次數
            dt.Columns.Add(new DataColumn("Re_Cu_5", typeof(string)));   // 續約筆數
            dt.Columns.Add(new DataColumn("Cl_Cu_5", typeof(string)));   // 解約筆數
            dt.Columns.Add(new DataColumn("Ct_Cu_5", typeof(string)));   // 中途解約
            dt.Columns.Add(new DataColumn("NF_Cu_5", typeof(string)));   // 合約異動
            dt.Columns.Add(new DataColumn("Tw_Cn_5", typeof(string)));   // 二年約數
            dt.Columns.Add(new DataColumn("Rc_Cu_5", typeof(string)));   // 解約客率
            dt.Columns.Add(new DataColumn("Rc_Cn_5", typeof(string)));   // 解約次率
            dt.Columns.Add(new DataColumn("Rn_Cu_5", typeof(string)));   // 續約進度

            dt.Columns.Add(new DataColumn("Cr_Cu_6", typeof(string)));   // 負責客數
            dt.Columns.Add(new DataColumn("Cr_Cn_6", typeof(string)));   // 負責次數
            dt.Columns.Add(new DataColumn("Ne_Cn_6", typeof(string)));   // 新約次數
            dt.Columns.Add(new DataColumn("Cl_Cn_6", typeof(string)));   // 解約次數
            dt.Columns.Add(new DataColumn("Gr_Cn_6", typeof(string)));   // 成長次數
            dt.Columns.Add(new DataColumn("Ex_Cu_6", typeof(string)));   // 到期客戶
            dt.Columns.Add(new DataColumn("Ex_Cn_6", typeof(string)));   // 到期次數
            dt.Columns.Add(new DataColumn("Re_Cu_6", typeof(string)));   // 續約筆數
            dt.Columns.Add(new DataColumn("Cl_Cu_6", typeof(string)));   // 解約筆數
            dt.Columns.Add(new DataColumn("Ct_Cu_6", typeof(string)));   // 中途解約
            dt.Columns.Add(new DataColumn("NF_Cu_6", typeof(string)));   // 合約異動
            dt.Columns.Add(new DataColumn("Tw_Cn_6", typeof(string)));   // 二年約數
            dt.Columns.Add(new DataColumn("Rc_Cu_6", typeof(string)));   // 解約客率
            dt.Columns.Add(new DataColumn("Rc_Cn_6", typeof(string)));   // 解約次率
            dt.Columns.Add(new DataColumn("Rn_Cu_6", typeof(string)));   // 續約進度

            dt.Columns.Add(new DataColumn("Cr_Cu_7", typeof(string)));   // 負責客數
            dt.Columns.Add(new DataColumn("Cr_Cn_7", typeof(string)));   // 負責次數
            dt.Columns.Add(new DataColumn("Ne_Cn_7", typeof(string)));   // 新約次數
            dt.Columns.Add(new DataColumn("Cl_Cn_7", typeof(string)));   // 解約次數
            dt.Columns.Add(new DataColumn("Gr_Cn_7", typeof(string)));   // 成長次數
            dt.Columns.Add(new DataColumn("Ex_Cu_7", typeof(string)));   // 到期客戶
            dt.Columns.Add(new DataColumn("Ex_Cn_7", typeof(string)));   // 到期次數
            dt.Columns.Add(new DataColumn("Re_Cu_7", typeof(string)));   // 續約筆數
            dt.Columns.Add(new DataColumn("Cl_Cu_7", typeof(string)));   // 解約筆數
            dt.Columns.Add(new DataColumn("Ct_Cu_7", typeof(string)));   // 中途解約
            dt.Columns.Add(new DataColumn("NF_Cu_7", typeof(string)));   // 合約異動
            dt.Columns.Add(new DataColumn("Tw_Cn_7", typeof(string)));   // 二年約數
            dt.Columns.Add(new DataColumn("Rc_Cu_7", typeof(string)));   // 解約客率
            dt.Columns.Add(new DataColumn("Rc_Cn_7", typeof(string)));   // 解約次率
            dt.Columns.Add(new DataColumn("Rn_Cu_7", typeof(string)));   // 續約進度

            dt.Columns.Add(new DataColumn("Cr_Cu_8", typeof(string)));   // 負責客數
            dt.Columns.Add(new DataColumn("Cr_Cn_8", typeof(string)));   // 負責次數
            dt.Columns.Add(new DataColumn("Ne_Cn_8", typeof(string)));   // 新約次數
            dt.Columns.Add(new DataColumn("Cl_Cn_8", typeof(string)));   // 解約次數
            dt.Columns.Add(new DataColumn("Gr_Cn_8", typeof(string)));   // 成長次數
            dt.Columns.Add(new DataColumn("Ex_Cu_8", typeof(string)));   // 到期客戶
            dt.Columns.Add(new DataColumn("Ex_Cn_8", typeof(string)));   // 到期次數
            dt.Columns.Add(new DataColumn("Re_Cu_8", typeof(string)));   // 續約筆數
            dt.Columns.Add(new DataColumn("Cl_Cu_8", typeof(string)));   // 解約筆數
            dt.Columns.Add(new DataColumn("Ct_Cu_8", typeof(string)));   // 中途解約
            dt.Columns.Add(new DataColumn("NF_Cu_8", typeof(string)));   // 合約異動
            dt.Columns.Add(new DataColumn("Tw_Cn_8", typeof(string)));   // 二年約數
            dt.Columns.Add(new DataColumn("Rc_Cu_8", typeof(string)));   // 解約客率
            dt.Columns.Add(new DataColumn("Rc_Cn_8", typeof(string)));   // 解約次率
            dt.Columns.Add(new DataColumn("Rn_Cu_8", typeof(string)));   // 續約進度

            dt.Columns.Add(new DataColumn("Cr_Cu_9", typeof(string)));   // 負責客數
            dt.Columns.Add(new DataColumn("Cr_Cn_9", typeof(string)));   // 負責次數
            dt.Columns.Add(new DataColumn("Ne_Cn_9", typeof(string)));   // 新約次數
            dt.Columns.Add(new DataColumn("Cl_Cn_9", typeof(string)));   // 解約次數
            dt.Columns.Add(new DataColumn("Gr_Cn_9", typeof(string)));   // 成長次數
            dt.Columns.Add(new DataColumn("Ex_Cu_9", typeof(string)));   // 到期客戶
            dt.Columns.Add(new DataColumn("Ex_Cn_9", typeof(string)));   // 到期次數
            dt.Columns.Add(new DataColumn("Re_Cu_9", typeof(string)));   // 續約筆數
            dt.Columns.Add(new DataColumn("Cl_Cu_9", typeof(string)));   // 解約筆數
            dt.Columns.Add(new DataColumn("Ct_Cu_9", typeof(string)));   // 中途解約
            dt.Columns.Add(new DataColumn("NF_Cu_9", typeof(string)));   // 合約異動
            dt.Columns.Add(new DataColumn("Tw_Cn_9", typeof(string)));   // 二年約數
            dt.Columns.Add(new DataColumn("Rc_Cu_9", typeof(string)));   // 解約客率
            dt.Columns.Add(new DataColumn("Rc_Cn_9", typeof(string)));   // 解約次率
            dt.Columns.Add(new DataColumn("Rn_Cu_9", typeof(string)));   // 續約進度

            dt.Columns.Add(new DataColumn("Cr_Cu_A", typeof(string)));   // 負責客數
            dt.Columns.Add(new DataColumn("Cr_Cn_A", typeof(string)));   // 負責次數
            dt.Columns.Add(new DataColumn("Ne_Cn_A", typeof(string)));   // 新約次數
            dt.Columns.Add(new DataColumn("Cl_Cn_A", typeof(string)));   // 解約次數
            dt.Columns.Add(new DataColumn("Gr_Cn_A", typeof(string)));   // 成長次數
            dt.Columns.Add(new DataColumn("Ex_Cu_A", typeof(string)));   // 到期客戶
            dt.Columns.Add(new DataColumn("Ex_Cn_A", typeof(string)));   // 到期次數
            dt.Columns.Add(new DataColumn("Re_Cu_A", typeof(string)));   // 續約筆數
            dt.Columns.Add(new DataColumn("Cl_Cu_A", typeof(string)));   // 解約筆數
            dt.Columns.Add(new DataColumn("Ct_Cu_A", typeof(string)));   // 中途解約
            dt.Columns.Add(new DataColumn("NF_Cu_A", typeof(string)));   // 合約異動
            dt.Columns.Add(new DataColumn("Tw_Cn_A", typeof(string)));   // 二年約數
            dt.Columns.Add(new DataColumn("Rc_Cu_A", typeof(string)));   // 解約客率
            dt.Columns.Add(new DataColumn("Rc_Cn_A", typeof(string)));   // 解約次率
            dt.Columns.Add(new DataColumn("Rn_Cu_A", typeof(string)));   // 續約進度

            dt.Columns.Add(new DataColumn("Cr_Cu_B", typeof(string)));   // 負責客數
            dt.Columns.Add(new DataColumn("Cr_Cn_B", typeof(string)));   // 負責次數
            dt.Columns.Add(new DataColumn("Ne_Cn_B", typeof(string)));   // 新約次數
            dt.Columns.Add(new DataColumn("Cl_Cn_B", typeof(string)));   // 解約次數
            dt.Columns.Add(new DataColumn("Gr_Cn_B", typeof(string)));   // 成長次數
            dt.Columns.Add(new DataColumn("Ex_Cu_B", typeof(string)));   // 到期客戶
            dt.Columns.Add(new DataColumn("Ex_Cn_B", typeof(string)));   // 到期次數
            dt.Columns.Add(new DataColumn("Re_Cu_B", typeof(string)));   // 續約筆數
            dt.Columns.Add(new DataColumn("Cl_Cu_B", typeof(string)));   // 解約筆數
            dt.Columns.Add(new DataColumn("Ct_Cu_B", typeof(string)));   // 中途解約
            dt.Columns.Add(new DataColumn("NF_Cu_B", typeof(string)));   // 合約異動
            dt.Columns.Add(new DataColumn("Tw_Cn_B", typeof(string)));   // 二年約數
            dt.Columns.Add(new DataColumn("Rc_Cu_B", typeof(string)));   // 解約客率
            dt.Columns.Add(new DataColumn("Rc_Cn_B", typeof(string)));   // 解約次率
            dt.Columns.Add(new DataColumn("Rn_Cu_B", typeof(string)));   // 續約進度

            dt.Columns.Add(new DataColumn("Cr_Cu_C", typeof(string)));   // 負責客數
            dt.Columns.Add(new DataColumn("Cr_Cn_C", typeof(string)));   // 負責次數
            dt.Columns.Add(new DataColumn("Ne_Cn_C", typeof(string)));   // 新約次數
            dt.Columns.Add(new DataColumn("Cl_Cn_C", typeof(string)));   // 解約次數
            dt.Columns.Add(new DataColumn("Gr_Cn_C", typeof(string)));   // 成長次數
            dt.Columns.Add(new DataColumn("Ex_Cu_C", typeof(string)));   // 到期客戶
            dt.Columns.Add(new DataColumn("Ex_Cn_C", typeof(string)));   // 到期次數
            dt.Columns.Add(new DataColumn("Re_Cu_C", typeof(string)));   // 續約筆數
            dt.Columns.Add(new DataColumn("Cl_Cu_C", typeof(string)));   // 解約筆數
            dt.Columns.Add(new DataColumn("Ct_Cu_C", typeof(string)));   // 中途解約
            dt.Columns.Add(new DataColumn("NF_Cu_C", typeof(string)));   // 合約異動
            dt.Columns.Add(new DataColumn("Tw_Cn_C", typeof(string)));   // 二年約數
            dt.Columns.Add(new DataColumn("Rc_Cu_C", typeof(string)));   // 解約客率
            dt.Columns.Add(new DataColumn("Rc_Cn_C", typeof(string)));   // 解約次率
            dt.Columns.Add(new DataColumn("Rn_Cu_C", typeof(string)));   // 續約進度

            dt.Columns.Add(new DataColumn("S1", typeof(int)));  // 排序用
            dt.Columns.Add(new DataColumn("S2", typeof(int)));  // 排序用

            if (Branch.SelectedIndex == 0)
            {
                Start_Banch = 0;
                End_Banch = Branch.Items.Count - 1;
            }
            else
            {
                Start_Banch = Branch.SelectedIndex;
                End_Banch = Branch.SelectedIndex;
            }

            double N1, N2, N3, N4, N5, N6, N7, N8, N9, NA, NB, NC, ND, NE, NF;
            double TN1, TN2, TN3, TN4, TN5, TN6, TN7, TN8, TN9, TNA, TNB, TNC;
            // Dim TT1, TT2, TT3, TT4, TT5, TT6, TT7, TT8, TT9, TTA, TTB, TTC, TTD, TTE, TTF As Double


            // TT1 = 0
            // TT2 = 0
            // TT3 = 0
            // TT4 = 0
            // TT5 = 0
            // TT6 = 0


            for (var jj = Start_Banch; jj <= End_Banch; jj++)
            {
                TN1 = 0.0;
                TN2 = 0.0;
                TN3 = 0.0;
                TN4 = 0.0;
                TN5 = 0.0;
                TN6 = 0.0;
                TN7 = 0.0;
                TN8 = 0.0;
                TN9 = 0.0;
                TNA = 0.0;
                TNB = 0.0;
                TNC = 0.0;

                string Query_String = "(Contract_E_Date >= '" + Check_Year.SelectedValue + "/1/1' or Contract_E_Date is null) ";
                int End_Name;

                if (jj == 0)
                {
                    string Sql = "";
                    MyDataSet = new DataSet();
                    Sql = "SELECT *,Year(Contract_S_Date) as Contract_S_Year, Month(Contract_S_Date) as Contract_S_Month,Year(Contract_E_Date) as" +
                        " Contract_E_Year, Month(Contract_E_Date) as Contract_E_Month FROM Cust_Contract_Data where " + Query_String;
                    MyCommand = new SqlDataAdapter(Sql, INConnection1);
                    MyCommand.Fill(MyDataSet, "RNumber");

                    Sql = "SELECT *,Year(N_Contract_S_Date) as Contract_S_Year, Month(N_Contract_S_Date) as Contract_S_Month,Year(Contract_E_Date) as" +
                        " Contract_E_Year, Month(Contract_E_Date) as Contract_E_Month FROM Cust_Contract_Expire_View where " + Query_String;
                    MyCommand = new SqlDataAdapter(Sql, INConnection1);
                    MyCommand.Fill(MyDataSet, "CNumber");

                    //string Sql = "SELECT * FROM Staff_Job_Data_View WHERE (Depart_Date Is NULL OR " + " YEAR(Depart_Date) >= " + Check_Year.SelectedValue + ") AND (Job_Title like '%專%' or Job_Title like '%儲備%' or Job_Title like '%經理%') and Branch_Company <> '總公司' order by Branch_Name,Job_Title Desc,Staff_No ";
                    Sql = "  SELECT * FROM Staff_Job_Data_View WHERE(Depart_Date Is NULL OR YEAR(Depart_Date) >= " + Check_Year.SelectedValue + ") AND Job_Title_ID IN(5, 6, 16, 17, 18, 19) AND Branch_ID = " + Branch.SelectedIndex + " AND Job_Status_ID = 2 ORDER BY Branch_ID, Job_Title_ID DESC, Staff_No";
                    MyCommand = new SqlDataAdapter(Sql, INConnection1);
                    MyCommand.Fill(MyDataSet, "專員");
                    Select_Banch = "合計";
                    End_Name = MyDataSet.Tables["專員"].DefaultView.Count;
                }
                else
                {
                    string Sql = "";
                    if (Branch.Items[jj].Text.Trim() == "南高雄")
                        Select_Banch = "Branch_Name ='南高雄' or Branch_Name = '屏東'";
                    else
                        Select_Banch = "Branch_Name ='" + Branch.Items[jj].Text.Trim() + "'";// 營業所名稱

                    MyDataSet = new DataSet();
                    Sql = "SELECT *,Year(Contract_S_Date) as Contract_S_Year, Month(Contract_S_Date) as Contract_S_Month,Year(Contract_E_Date) as Contract_E_Year, Month(Contract_E_Date) as Contract_E_Month FROM Cust_Contract_Data where " + Query_String + " and " + Select_Banch;
                    MyCommand = new SqlDataAdapter(Sql, INConnection1);
                    MyCommand.Fill(MyDataSet, "RNumber");
                    MyCommand = new SqlDataAdapter(
                        "SELECT *,Year(N_Contract_S_Date) as Contract_S_Year, Month(N_Contract_S_Date) as Contract_S_Month,Year(Contract_E_Date) as Contract_E_Year," +
                        " Month(Contract_E_Date) as Contract_E_Month FROM Cust_Contract_Expire_View where " + Query_String + " and " + Select_Banch, INConnection1);
                    MyCommand.Fill(MyDataSet, "CNumber");

                    Sql = "SELECT * FROM Staff_Job_Data_View WHERE Job_Title_ID IN(5, 6, 16, 17, 18, 19) AND Job_Status_ID = 2 AND Branch_ID = " + Branch.SelectedIndex + " AND YEAR(Join_Date) <= " + Check_Year.SelectedValue;
                    MyCommand = new SqlDataAdapter(Sql, INConnection1);
                    MyCommand.Fill(MyDataSet, "專員");
                    End_Name = MyDataSet.Tables["專員"].DefaultView.Count;
                }

                int ii;

                for (ii = 0; ii <= End_Name; ii++)
                {
                    TN1 = 0.0;
                    TN2 = 0.0;
                    TN3 = 0.0;
                    TN4 = 0.0;
                    TN5 = 0.0;
                    TN6 = 0.0;
                    TN7 = 0.0;
                    TN8 = 0.0;
                    TN9 = 0.0;
                    TNA = 0.0;
                    TNB = 0.0;
                    TNC = 0.0;
                    dr = dt.NewRow();

                    // 排序用
                    string Se_String, Sc_String;
                    if (ii == 0)
                    {
                        Se_String = "";
                        Sc_String = "";
                        dr[0] = Branch.Items[jj].Text;
                    }
                    else
                    {
                        Se_String = " and  Charge_Staff_Name = '" + MyDataSet.Tables["專員"].DefaultView[ii - 1][1] + "'";
                        Sc_String = " and  Staff_No = '" + MyDataSet.Tables["專員"].DefaultView[ii - 1][0] + "'";
                        dr[0] = Branch.Items[jj].Text;
                        dr[1] = MyDataSet.Tables["專員"].DefaultView[ii - 1][1];
                    }
                    dr[197] = jj;                   // 各營業所 KEY 值
                    dr[198] = ii;                   // 各輔導組長 KEY 值


                    int kk;

                    for (kk = 1; kk <= 12; kk++)   // 1-12月
                    {

                        // 如果己有資料存在 專員編號及報表日期
                        string INString;
                        if (ii == 0)
                            INString = "Select * from Staff_Business_Data where Staff_No = '" + Branch.Items[jj].Text.Trim() + "' and year(Report_Gen_Date) = " +
                                       Check_Year.SelectedValue + " and month(Report_Gen_Date) = " + kk;
                        else
                            INString = "Select * from Staff_Business_Data where year(Report_Gen_Date) = " + Check_Year.SelectedValue +
                                       " and month(Report_Gen_Date) = " + kk + Sc_String;
                        // TextBox2.Text = TextBox2.Text & INString
                        INCommand = new SqlCommand(INString, INConnection1);
                        INCommand.Connection.Open();
                        SQLReader = INCommand.ExecuteReader();
                        if (SQLReader.Read())
                        {
                            // 負責客數
                            N1 = Convert.ToDouble(SQLReader[3]);
                            // 負責次數
                            N2 = Convert.ToDouble(SQLReader[4]);
                            // 新約次數
                            N3 = Convert.ToDouble(SQLReader[5]);
                            // 解約次數
                            N4 = Convert.ToDouble(SQLReader[6]);
                            // 成長次數
                            N5 = N3 - N4;
                            // 到期客數
                            N6 = Convert.ToDouble(SQLReader[8]);
                            // 到期次數
                            N7 = Convert.ToDouble(SQLReader[9]);
                            // 續約客數
                            N8 = Convert.ToDouble(SQLReader[10]);
                            // 解約客數
                            N9 = Convert.ToDouble(SQLReader[11]);
                            // 中途解約
                            NA = Convert.ToDouble(SQLReader[12]);
                            // 合約異動
                            NB = Convert.ToDouble(SQLReader[13]);
                            // 二年約合約
                            NC = Convert.ToDouble(SQLReader[7]);
                            // 解約次數比率
                            ND = (N7 == 0) ? 0 : (N4 / N7);
                            // 解約客數比率
                            NE = (N6 == 0) ? 0 : (NA / N6);
                            // 續約進度
                            NF = (N6 == 0) ? 0 : ((N8 + N9 + NB) / N6);
                        }
                        else
                        {
                            // 負責客數
                            if (DBNull.Value.Equals(MyDataSet.Tables["RNumber"].Compute("Count(ID)", "True " + Se_String)))
                                N1 = 0;
                            else
                                N1 = Convert.ToDouble(MyDataSet.Tables["RNumber"].Compute("Count(ID)", "True " + Se_String));
                            // 負責次數
                            if (DBNull.Value.Equals(MyDataSet.Tables["RNumber"].Compute("Sum(Contract_Times)", "True " + Se_String)))
                                N2 = 0;
                            else
                                N2 = Convert.ToDouble(MyDataSet.Tables["RNumber"].Compute("Sum(Contract_Times)", "True " + Se_String));
                            // 新約次數
                            if (DBNull.Value.Equals(MyDataSet.Tables["CNumber"].Compute("Sum(N_Contract_Times)", "Contract_Result = 5 and Contract_S_Year = " +
                                Check_Year.SelectedValue + " and Contract_S_Month = " + kk + Sc_String)))
                                N3 = 0;
                            else
                                N3 = Convert.ToDouble(MyDataSet.Tables["CNumber"].Compute("Sum(N_Contract_Times)", "Contract_Result = 5 and Contract_S_Year = " +
                                    Check_Year.SelectedValue + " and Contract_S_Month = " + kk + Sc_String));
                            // 回流也算新約次數
                            if (DBNull.Value.Equals(MyDataSet.Tables["CNumber"].Compute("Sum(N_Contract_Times)", "Contract_Result = 6 and Contract_S_Year = " +
                                Check_Year.SelectedValue + " and Contract_S_Month = " + kk + Sc_String)))
                                N3 = N3 + 0;
                            else
                                N3 = N3 + Convert.ToDouble(MyDataSet.Tables["CNumber"].Compute("Sum(N_Contract_Times)", "Contract_Result = 6 and Contract_S_Year = " +
                                     Check_Year.SelectedValue + " and Contract_S_Month = " + kk + Sc_String));
                            // 加次也算新約次數
                            double fstC, sndC;
                            // 原合約次數
                            if (DBNull.Value.Equals(MyDataSet.Tables["CNumber"].Compute("Sum(Contract_Times)", "Contract_Result = 3 and Contract_E_Year = " +
                                Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String)))
                                fstC = 0;
                            else
                                fstC = Convert.ToDouble(MyDataSet.Tables["CNumber"].Compute("Sum(Contract_Times)", "Contract_Result = 3 and Contract_E_Year = " +
                                       Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String));
                            // 新合約次數
                            if (DBNull.Value.Equals(MyDataSet.Tables["CNumber"].Compute("Sum(N_Contract_Times)", "Contract_Result = 3 and Contract_E_Year = " +
                                Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String)))
                                sndC = 0;
                            else
                                sndC = Convert.ToDouble(MyDataSet.Tables["CNumber"].Compute("Sum(N_Contract_Times)", "Contract_Result = 3 and Contract_E_Year = " +
                                       Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String));
                            N3 = N3 + sndC - fstC;
                            // 解約次數
                            if (DBNull.Value.Equals(MyDataSet.Tables["CNumber"].Compute("Sum(Contract_Times)", "Contract_Result = 1 and Contract_E_Year = " +
                                Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String)))
                                N4 = 0;
                            else
                                N4 = Convert.ToDouble(MyDataSet.Tables["CNumber"].Compute("Sum(Contract_Times)", "Contract_Result = 1 and Contract_E_Year = " +
                                     Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String));
                            // 減次也算解約次數
                            // 原合約次數
                            if (DBNull.Value.Equals(MyDataSet.Tables["CNumber"].Compute("Sum(Contract_Times)", "Contract_Result = 4 and Contract_E_Year = " +
                                Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String)))
                                fstC = 0;
                            else
                                fstC = Convert.ToDouble(MyDataSet.Tables["CNumber"].Compute("Sum(Contract_Times)", "Contract_Result = 4 and Contract_E_Year = " +
                                       Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String));
                            // 新合約次數
                            if (DBNull.Value.Equals(MyDataSet.Tables["CNumber"].Compute("Sum(N_Contract_Times)", "Contract_Result = 4 and Contract_E_Year = " +
                                Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String)))
                                sndC = 0;
                            else
                                sndC = Convert.ToDouble(MyDataSet.Tables["CNumber"].Compute("Sum(N_Contract_Times)", "Contract_Result = 4 and Contract_E_Year = " +
                                       Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String));
                            N4 = N4 + fstC - sndC;
                            // 成長次數
                            N5 = N3 - N4;
                            // 到期客數

                            // 找出二資料表沒有重覆的記錄
                            DataRow[] findrows;
                            findrows = MyDataSet.Tables["CNumber"].Select("Contract_Result < 5 and Contract_E_Year = " + Check_Year.SelectedValue.ToString() +
                                       " and Contract_E_Month = " + kk.ToString() + Sc_String);
                            string FCNO = "";
                            Int16 qq;
                            for (qq = 0; qq <= findrows.Count() - 1; qq++)
                                FCNO = FCNO + "'" + findrows[qq][1].ToString() + "',";
                            if (FCNO.Length > 0)
                                FCNO = "(" + FCNO.Substring(0, FCNO.Length - 1) + ")";
                            else
                                FCNO = "('')";

                            if (DBNull.Value.Equals(MyDataSet.Tables["CNumber"].Compute("Count(ID)", "Contract_Result < 5 and Contract_E_Year = " +
                                Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String)))
                                N6 = 0;
                            else
                            {
                                N6 = Convert.ToDouble(MyDataSet.Tables["CNumber"].Compute("Count(ID)", "Contract_Result < 5 and Contract_E_Year = " +
                                     Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String));
                                if (!DBNull.Value.Equals(MyDataSet.Tables["RNumber"].Compute("Count(ID)", "Cust_No not in " + FCNO + " and Contract_E_Year = " +
                                    Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Se_String)))
                                    N6 = N6 + Convert.ToDouble(MyDataSet.Tables["RNumber"].Compute("Count(ID)", "Cust_No not in " + FCNO +
                                         " and Contract_E_Year = " + Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Se_String));
                            }
                            // 到期次數
                            if (DBNull.Value.Equals(MyDataSet.Tables["CNumber"].Compute("Sum(Contract_Times)", "Contract_Result < 5 and Contract_E_Year = " +
                                Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String)))
                                N7 = 0;
                            else
                            {
                                N7 = Convert.ToDouble(MyDataSet.Tables["CNumber"].Compute("Sum(Contract_Times)", "Contract_Result < 5 and Contract_E_Year = " +
                                     Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String));
                                if (!DBNull.Value.Equals(MyDataSet.Tables["RNumber"].Compute("Sum(Contract_Times)", "Cust_No not in " + FCNO +
                                    " and Contract_E_Year = " + Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Se_String)))
                                    N7 = N7 + Convert.ToDouble(MyDataSet.Tables["RNumber"].Compute("Sum(Contract_Times)", "Cust_No not in " + FCNO +
                                         " and Contract_E_Year = " + Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Se_String));
                            }
                            // 續約客數
                            if (DBNull.Value.Equals(MyDataSet.Tables["CNumber"].Compute("Count(ID)", "Contract_Result = 3 and Contract_E_Year = " +
                                Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String)))
                                N8 = 0;
                            else
                                N8 = Convert.ToDouble(MyDataSet.Tables["CNumber"].Compute("Count(ID)", "Contract_Result = 3 and Contract_E_Year = " +
                                     Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String));
                            // 減次也算續約
                            if (DBNull.Value.Equals(MyDataSet.Tables["CNumber"].Compute("Count(ID)", "Contract_Result = 4 and Contract_E_Year = " +
                                Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String)))
                                N8 = N8 + 0;
                            else
                                N8 = N8 + Convert.ToDouble(MyDataSet.Tables["CNumber"].Compute("Count(ID)", "Contract_Result = 4 and Contract_E_Year = " +
                                     Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String));
                            // 解約客數
                            if (DBNull.Value.Equals(MyDataSet.Tables["CNumber"].Compute("Count(ID)", "Contract_Result = 1 and Contract_E_Year = " +
                                Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String)))
                                N9 = 0;
                            else
                                N9 = Convert.ToDouble(MyDataSet.Tables["CNumber"].Compute("Count(ID)", "Contract_Result = 1 and Contract_E_Year = " +
                                     Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String));
                            // 保留也算解約
                            if (DBNull.Value.Equals(MyDataSet.Tables["CNumber"].Compute("Count(ID)", "Contract_Result = 2 and Contract_E_Year = " +
                                Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String)))
                                N9 = N9 + 0;
                            else
                                N9 = N9 + Convert.ToDouble(MyDataSet.Tables["CNumber"].Compute("Count(ID)", "Contract_Result = 2 and Contract_E_Year = " +
                                     Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String));
                            // 中途解約
                            if (DBNull.Value.Equals(MyDataSet.Tables["CNumber"].Compute("Count(ID)", "Contract_E_Date < Contract_R_Date" + Sc_String)))
                                NA = 0;
                            else
                                NA = Convert.ToDouble(MyDataSet.Tables["CNumber"].Compute("Count(ID)", "Contract_E_Date < Contract_R_Date" + Sc_String));
                            // 合約異動
                            if (DBNull.Value.Equals(MyDataSet.Tables["CNumber"].Compute("Count(ID)", "Contract_Result = 2 and Contract_E_Year = " +
                                Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String)))
                                NB = 0;
                            else
                                NB = Convert.ToDouble(MyDataSet.Tables["CNumber"].Compute("Count(ID)", "Contract_Result = 2 and Contract_E_Year = " +
                                     Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String));
                            // 二年約合約
                            if (DBNull.Value.Equals(MyDataSet.Tables["CNumber"].Compute("Count(ID)", "N_Contract_Years >= 2 and  Contract_E_Year = " +
                                Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String)))
                                NC = 0;
                            else
                                NC = Convert.ToDouble(MyDataSet.Tables["CNumber"].Compute("Count(ID)", "N_Contract_Years >= 2 and  Contract_E_Year = " +
                                     Check_Year.SelectedValue + " and Contract_E_Month = " + kk + Sc_String));
                            // 解約次數比率
                            ND = (N7 == 0) ? 0 : (N4 / N7);
                            // 解約客數比率
                            NE = (N6 == 0) ? 0 : (N9 / N6);
                            // 續約進度
                            NF = (N6 == 0) ? 0 : ((N8 + N9 + NB) / N6);
                        }
                        INCommand.Connection.Close();
                        TN1 = TN1 + N1;
                        TN2 = TN2 + N2;
                        TN3 = TN3 + N3;
                        TN4 = TN4 + N4;
                        TN5 = TN5 + N5;
                        TN6 = TN6 + N6;
                        TN7 = TN7 + N7;
                        TN8 = TN8 + N8;
                        TN9 = TN9 + N9;
                        TNA = TNA + NA;
                        TNB = TNB + NB;
                        TNC = TNC + NC;


                        dr[2 + kk * 15] = N1.ToString("0");
                        dr[3 + kk * 15] = N2.ToString("0.0");
                        dr[4 + kk * 15] = N3.ToString("0.0");
                        dr[5 + kk * 15] = N4.ToString("0.0");
                        dr[6 + kk * 15] = N5.ToString("0.0");
                        dr[7 + kk * 15] = N6.ToString("0");
                        dr[8 + kk * 15] = N7.ToString("0.0");
                        dr[9 + kk * 15] = N8.ToString("0");
                        dr[10 + kk * 15] = N9.ToString("0");
                        dr[11 + kk * 15] = NA.ToString("0");
                        dr[12 + kk * 15] = NB.ToString("0");
                        dr[13 + kk * 15] = NC.ToString("0");
                        dr[14 + kk * 15] = (N7 != 0) ? (N4 / N7).ToString("0.0%") : "0";
                        dr[15 + kk * 15] = (N6 != 0) ? (N9 / N6).ToString("0.0%") : "0";
                        dr[16 + kk * 15] = (N6 != 0) ? ((N8 + N9 + NB) / N6).ToString("0.0%") : "0";
                    }
                    dr[2] = (TN1 / 12).ToString("0");
                    dr[3] = (TN2 / 12).ToString("0.0");
                    dr[4] = TN3.ToString("0.0");
                    dr[5] = TN4.ToString("0.0");
                    dr[6] = TN5.ToString("0.0");
                    dr[7] = TN6.ToString("0");
                    dr[8] = TN7.ToString("0.0");
                    dr[9] = TN8.ToString("0");
                    dr[10] = TN9.ToString("0");
                    dr[11] = TNA.ToString("0");
                    dr[12] = TNB.ToString("0");
                    dr[13] = TNC.ToString("0");
                    dr[14] = (TN7 != 0) ? (TN4 / TN7).ToString("0.0%") : "0";
                    dr[15] = (TN6 != 0) ? (TN9 / TN6).ToString("0.0%") : "0";
                    dr[16] = (TN6 != 0) ? ((TN8 + TN9 + TNB) / TN6).ToString("0.0%") : "0";
                    dt.Rows.Add(dr);
                }
            }

            dt.DefaultView.Sort = "S1 ASC, S2 ASC";     // 排序
            Repeater2.DataSource = dt.DefaultView;
            Repeater2.DataBind();
            Check_Month_SelectedIndexChanged(sender, e);

            Page_Load(sender, e);
        }
        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {

            Page_Load(sender, e);
        }
        public void Show_Sc_Type(object sender, DataGridItemEventArgs e)
        {
            ListItemType objItemType = (ListItemType)e.Item.ItemType;
            Label TempLabel;

            if (objItemType == ListItemType.Item | objItemType == ListItemType.AlternatingItem)
            {
                if (((Control)sender).ID == "DataGrid1")
                {
                    TempLabel = (Label)e.Item.FindControl("Hire_FLag");
                    switch (e.Item.Cells[7].Text)
                    {
                        case "0":
                            {
                                TempLabel.Text = "錄取";
                                break;
                            }

                        case "1":
                            {
                                TempLabel.Text = "安排複試";
                                break;
                            }

                        case "2":
                            {
                                TempLabel.Text = "不錄取";
                                break;
                            }

                        case "3":
                            {
                                TempLabel.Text = "保留";
                                break;
                            }
                    }
                }
                if (((Control)sender).ID == "DataGrid5")
                {
                    e.Item.Cells[2].BackColor = Color.LightPink;
                    if (e.Item.Cells[2].Text != "&nbsp;")
                    {
                        if (Convert.ToInt32(e.Item.Cells[2].Text) >= 2)
                            e.Item.Cells[2].BackColor = Color.White;
                    }
                    e.Item.Cells[3].BackColor = Color.LightPink;
                    if (e.Item.Cells[3].Text != "&nbsp;")
                    {
                        if (Convert.ToInt32(e.Item.Cells[3].Text) >= 1)
                            e.Item.Cells[3].BackColor = Color.White;
                    }
                    e.Item.Cells[4].BackColor = Color.LightPink;
                    if (e.Item.Cells[4].Text != "&nbsp;")
                    {
                        if (Convert.ToInt32(e.Item.Cells[4].Text) >= 16)
                            e.Item.Cells[4].BackColor = Color.White;
                    }
                    e.Item.Cells[5].BackColor = Color.LightPink;
                    if (e.Item.Cells[5].Text != "&nbsp;")
                    {
                        if (Convert.ToInt32(e.Item.Cells[5].Text) >= 4)
                            e.Item.Cells[5].BackColor = Color.White;
                    }
                    e.Item.Cells[6].BackColor = Color.LightPink;
                    if (e.Item.Cells[6].Text != "&nbsp;")
                    {
                        if (Convert.ToInt32(e.Item.Cells[6].Text) >= 1)
                            e.Item.Cells[6].BackColor = Color.White;
                    }
                    e.Item.Cells[7].BackColor = Color.LightPink;
                    if (e.Item.Cells[7].Text != "&nbsp;")
                    {
                        if (Convert.ToInt32(e.Item.Cells[7].Text) >= 1)
                            e.Item.Cells[7].BackColor = Color.White;
                    }
                }
            }
        }

        protected void Check_Month_SelectedIndexChanged(object sender, EventArgs e)
        {
            Panel tmpView;
            for (int ii = 1; ii <= 12; ii++)
            {
                tmpView = (Panel)form1.FindControl("ViewT" + ii.ToString("00"));
                tmpView.Visible = (Check_Month.SelectedValue == ii.ToString() ? true : false);

                //tmpView = (Panel)form1.FindControl("ViewA" + ii.ToString("00"));
                //tmpView.Visible = (Check_Month.SelectedValue == ii.ToString() ? true : false);

                for (int iiii = 0; iiii < Repeater2.Items.Count; iiii++)
                {
                    tmpView = (Panel)Repeater2.Items[iiii].FindControl("ViewB" + ii.ToString("00"));
                    tmpView.Visible = (Check_Month.SelectedValue == ii.ToString() ? true : false);
                }

            }
        }

        public void Show_Job_Detail(object sender, DataGridCommandEventArgs e)
        {
        }
        protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            ListItemType objItemType = (ListItemType)e.Item.ItemType;
            Label tmpLabel1, tmpLabel2;
            int MPlus = 12;
            ShareFunction SF = new ShareFunction();

            if (Check_Year.SelectedValue == DateTime.Now.Year.ToString())
                MPlus = DateTime.Now.Month;

            if (objItemType == ListItemType.Item | objItemType == ListItemType.AlternatingItem)
            {
                if (((Control)sender).ID == "Repeater1")
                {
                    int XPlus = 1;

                    if (Branch.SelectedIndex == 0)
                        XPlus = Branch.Items.Count - 1;

                    tmpLabel1 = (Label)e.Item.FindControl("Label1");
                    // 應徵人數
                    tmpLabel2 = (Label)e.Item.FindControl("Recruit_Number");
                    if (SF.IsNumeric(tmpLabel1.Text.Substring(0, 1)) == true)
                    {
                        if (Convert.ToInt32(tmpLabel2.Text) < 16 * XPlus)
                        {
                            tmpLabel2.ForeColor = Color.Red;
                            tmpLabel2.Font.Bold = true;
                        }
                    }
                    else if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "")
                    {
                        if (Convert.ToInt32(tmpLabel2.Text) < 16 * MPlus)
                        {
                            tmpLabel2.ForeColor = Color.Red;
                            tmpLabel2.Font.Bold = true;
                        }
                    }
                    // 錄取人數
                    tmpLabel2 = (Label)e.Item.FindControl("Hire_Number");
                    if (SF.IsNumeric(tmpLabel1.Text.Substring(0, 1)) == true)
                    {
                        if (Convert.ToInt32(tmpLabel2.Text) < 8 * XPlus)
                        {
                            tmpLabel2.ForeColor = Color.Red;
                            tmpLabel2.Font.Bold = true;
                        }
                    }
                    else if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "")
                    {
                        if (Convert.ToInt32(tmpLabel2.Text) < 8 * MPlus)
                        {
                            tmpLabel2.ForeColor = Color.Red;
                            tmpLabel2.Font.Bold = true;
                        }
                    }
                    // 繳交人數
                    tmpLabel2 = (Label)e.Item.FindControl("Paper_Number");
                    if (SF.IsNumeric(tmpLabel1.Text.Substring(0, 1)) == true)
                    {
                        if (Convert.ToInt32(tmpLabel2.Text) < 5 * XPlus)
                        {
                            tmpLabel2.ForeColor = Color.Red;
                            tmpLabel2.Font.Bold = true;
                        }
                    }
                    else if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "")
                    {
                        if (Convert.ToInt32(tmpLabel2.Text) < 5 * MPlus)
                        {
                            tmpLabel2.ForeColor = Color.Red;
                            tmpLabel2.Font.Bold = true;
                        }
                    }
                    // 體驗人數
                    tmpLabel2 = (Label)e.Item.FindControl("Experience_Number");
                    if (SF.IsNumeric(tmpLabel1.Text.Substring(0, 1)) == true)
                    {
                        if (Convert.ToInt32(tmpLabel2.Text) < 4 * XPlus)
                        {
                            tmpLabel2.ForeColor = Color.Red;
                            tmpLabel2.Font.Bold = true;
                        }
                    }
                    else if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "")
                    {
                        if (Convert.ToInt32(tmpLabel2.Text) < 4 * MPlus)
                        {
                            tmpLabel2.ForeColor = Color.Red;
                            tmpLabel2.Font.Bold = true;
                        }
                    }
                    // 入職人數
                    tmpLabel2 = (Label)e.Item.FindControl("Final_Number");
                    if (SF.IsNumeric(tmpLabel1.Text.Substring(0, 1)) == true)
                    {
                        if (Convert.ToInt32(tmpLabel2.Text) < 3 * XPlus)
                        {
                            tmpLabel2.ForeColor = Color.Red;
                            tmpLabel2.Font.Bold = true;
                        }
                    }
                    else if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "")
                    {
                        if (Convert.ToInt32(tmpLabel2.Text) < 3 * MPlus)
                        {
                            tmpLabel2.ForeColor = Color.Red;
                            tmpLabel2.Font.Bold = true;
                        }
                    }

                    // 錄取率
                    tmpLabel2 = (Label)e.Item.FindControl("R_H_P");
                    if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "")
                    {
                        if (SF.IsNumeric(tmpLabel2.Text.Substring(0, tmpLabel2.Text.Length - 1)))
                        {
                            if (System.Convert.ToDouble(tmpLabel2.Text.Substring(0, tmpLabel2.Text.Length - 1)) < 50.0)
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                        }
                    }

                    // 繳交率
                    tmpLabel2 = (Label)e.Item.FindControl("H_P_P");
                    if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "")
                    {
                        if (SF.IsNumeric(tmpLabel2.Text.Substring(0, tmpLabel2.Text.Length - 1)))
                        {
                            if (System.Convert.ToDouble(tmpLabel2.Text.Substring(0, tmpLabel2.Text.Length - 1)) < 60.0)
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                        }
                    }

                    // 體驗入職率
                    tmpLabel2 = (Label)e.Item.FindControl("E_W_P");
                    if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "")
                    {
                        if (SF.IsNumeric(tmpLabel2.Text.Substring(0, tmpLabel2.Text.Length - 1)))
                        {
                            if (System.Convert.ToDouble(tmpLabel2.Text.Substring(0, tmpLabel2.Text.Length - 1)) < 70.0)
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                        }
                    }

                    // 應徵入職率
                    tmpLabel2 = (Label)e.Item.FindControl("R_W_P");
                    if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "")
                    {
                        if (SF.IsNumeric(tmpLabel2.Text.Substring(0, tmpLabel2.Text.Length - 1)))
                        {
                            if (System.Convert.ToDouble(tmpLabel2.Text.Substring(0, tmpLabel2.Text.Length - 1)) < 15.5)
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                        }
                    }
                }
                else
                {
                    //int BM = 10;
                    int ii;

                    tmpLabel1 = (Label)e.Item.FindControl("Label1");
                    tmpLabel2 = (Label)e.Item.FindControl("Label7");

                    if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "")
                    {
                        if (SF.IsNumeric(tmpLabel2.Text.Substring(0, tmpLabel2.Text.Length - 1)))
                        {
                            if (System.Convert.ToInt32(tmpLabel2.Text.Substring(0, tmpLabel2.Text.Length - 1)) < 70)
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                        }
                    }

                    for (ii = 0; ii <= MPlus - 1; ii++)
                    {
                        string tmpName = "Label";

                        tmpName = tmpName + (ii * 3 + 10).ToString();
                        tmpLabel2 = (Label)e.Item.FindControl(tmpName);

                        if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "")
                        {
                            if (SF.IsNumeric(tmpLabel2.Text.Substring(0, tmpLabel2.Text.Length - 1)))
                            {
                                if (System.Convert.ToInt32(tmpLabel2.Text.Substring(0, tmpLabel2.Text.Length - 1)) < 70)
                                {
                                    tmpLabel2.ForeColor = Color.Red;
                                    tmpLabel2.Font.Bold = true;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}