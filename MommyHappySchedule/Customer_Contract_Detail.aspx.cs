﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Drawing;
using System.Data;
using System.Web.Script.Serialization;
using System.Configuration;
using Models.Library;

namespace MommyHappySchedule
{
    public partial class Customer_Contract_Detail : System.Web.UI.Page
    {

        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        System.Data.SqlClient.SqlConnection INConnection1 = new System.Data.SqlClient.SqlConnection(ConnString);
        // Dim INConnection2 As Data.SqlClient.SqlConnection = New Data.SqlClient.SqlConnection(String2)
        private System.Data.SqlClient.SqlCommand INCommand;
        private System.Data.SqlClient.SqlDataReader SQLReader;
        private System.Data.DataSet MyDataSet;
        private System.Data.SqlClient.SqlDataAdapter MyCommand;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if ((string)Session["account"] == "")
            {
                Session["msg"] = "您沒有使用此程式的權限!!";
                Response.Redirect("Index.aspx");
            }

            if (!Page.IsPostBack)
            {
                MyDataSet = new System.Data.DataSet();
                MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Branch_Data where id > 0 ", INConnection1);
                MyCommand.Fill(MyDataSet, "Branch");

                Branch.DataSource = MyDataSet.Tables["Branch"].DefaultView;
                Branch.DataTextField = "Branch_Name";
                Branch.DataValueField = "ID";
                Branch.DataBind();

                Check_Year.SelectedValue = DateTime.Now.Year.ToString();
                Check_Month.SelectedValue = DateTime.Now.Month.ToString();

                MultiView1.ActiveViewIndex = 1;
            }
        }


        protected void Check_Report_Click(object sender, EventArgs e)
        {
            // Dim CToday As Integer = Year(Today)
            System.Data.DataTable dt;
            System.Data.DataRow dr;
            string Select_Banch;
            int Start_Banch, End_Banch;
            int [] CGrade ={ 0, 0, 0, 0 };

            // Label1.Text = Branch.SelectedItem.Text


            Label1.Text = DateTime.Now.ToLongTimeString();

            dt = new System.Data.DataTable();
            dt.Columns.Add(new System.Data.DataColumn("ID", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("Contract_Name", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("Staff_Name", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("Contract_Number", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("Contract_E_Date", typeof(DateTime)));
            dt.Columns.Add(new System.Data.DataColumn("CT_Number", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("YC_Number", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("YT_Number", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("CT_Percent", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("TL_Number", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("TL_Percent", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("CL_Number", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("CL_Percent", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("HL_Number", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("HL_Percent", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("OL_Number", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("OL_Percent", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("GL_Number", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("GL_Percent", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("Grade", typeof(string)));



            // 如果選營業所,則顯示單營業所
            Start_Banch = Branch.SelectedIndex;
            End_Banch = Branch.SelectedIndex;

            int jj;

            int [] HC ={0, 0, 0, 0, 0, 0, 0, 0, 0 };

            for (jj = Start_Banch; jj <= End_Banch; jj++)
            {
                string Te_String;
                int RD = 0;
                int [] TB = { 0, 0, 0, 0, 0, 0, 0, 0 };

                if (Check_Month.SelectedIndex == 0)
                    Te_String = " Year(Report_Gen_Date) = " + Check_Year.SelectedValue + " and  Month(Report_Gen_Date) = 12 ";
                else
                    Te_String = " Year(Report_Gen_Date) = " + Check_Year.SelectedValue + " and  Month(Report_Gen_Date) = " + Check_Month.SelectedValue;
                INCommand = new System.Data.SqlClient.SqlCommand("SELECT * FROM Cust_Serv_Mon_Reports where Branch_ID= " + jj + 1 + " and " + Te_String, INConnection1);

                INCommand.Connection.Open();
                SQLReader = INCommand.ExecuteReader();
                while (SQLReader.Read())      // 如果有記錄
                {
                    RD = RD + 1;
                    dr = dt.NewRow();
                    // 排序用
                    dr[0] = Branch.Items[System.Convert.ToInt32(SQLReader[1]) - 1].Text;
                    dr[1] = SQLReader[2];
                    dr[2] = SQLReader[3];
                    dr[3] = DBNull.Value.Equals(SQLReader[4]) ? 1 : SQLReader[4];
                    dr[4] = SQLReader[5];


                    // 計算應服務次數
                    dr[6] = SQLReader[7];
                    TB[1] = TB[1] + Convert.ToInt16(SQLReader[7]);
                    HC[1] = HC[1] + Convert.ToInt16(SQLReader[7]);

                    // 計算客戶總次數
                    dr[5] = SQLReader[6];
                    TB[0] = TB[0] + Convert.ToInt16(SQLReader[6]);
                    HC[0] = HC[0] + Convert.ToInt16(SQLReader[6]);

                    // 客戶時間內所做次數
                    dr[7] = string.Format("{0:#,##0}",SQLReader[8]);
                    if (SQLReader[7].ToString() == "0")
                        dr[8] = "00.0%";
                    else
                        dr[8] = string.Format("{0:00.0%}",(Convert.ToDouble(SQLReader[8]) / Convert.ToDouble(SQLReader[7])));
                    TB[2] = TB[2] + Convert.ToInt16(SQLReader[8]);
                    HC[2] = HC[2] + Convert.ToInt16(SQLReader[8]);  // YT_Number


                    // 國春假總次數
                    // SQLReader(13) GL_Number = TGL1 + TGL2
                    dr[17] = string.Format("{0:#,##0}",SQLReader[13]);
                    if (SQLReader[8].ToString() == "0")
                        dr[18] = "00.0%";
                    else
                        dr[18] = string.Format("{0:00.0%}",(Convert.ToDouble(SQLReader[13]) / Convert.ToDouble(SQLReader[8])));
                    TB[7] = TB[7] + Convert.ToInt16(SQLReader[13]);
                    HC[7] = HC[7] + Convert.ToInt16(SQLReader[13]);


                    // 未服務總次數
                    // TL_Number = MyDataSet.Tables("CNumber").Compute("Count(ID)", "Job_Result <> '' and " & Se_String)
                    // SQLReader(9) TL_Number = TL_Number - GL_Number   '減去國春假,屬於真正異常的部份
                    dr[9] = string.Format("{0:#,##0}",SQLReader[9]);
                    if (SQLReader[8].ToString() == "0")
                        dr[10] = "00.0%";
                    else
                        dr[10] = string.Format("{0:00.0%}", Convert.ToDouble(SQLReader[9]) / Convert.ToDouble(SQLReader[8]));
                    TB[3] = TB[3] + Convert.ToInt16(SQLReader[9]);
                    HC[3] = HC[3] + Convert.ToInt16(SQLReader[9]);

                    // 客人請假總次數
                    // SQLReader(10) CL_Number = MyDataSet.Tables("CNumber").Compute("Count(ID)", "Job_Result = '客' and " & Se_String)
                    dr[11] = string.Format("{0:#,##0}", SQLReader[10]);
                    if (SQLReader[8].ToString() == "0")
                        dr[12] = "00.0%";
                    else
                        dr[12] = string.Format("{0:00.0%}", (Convert.ToDouble(SQLReader[10]) / Convert.ToDouble(SQLReader[8])));
                    TB[4] = TB[4] + Convert.ToInt16(SQLReader[10]);
                    HC[4] = HC[4] + Convert.ToInt16(SQLReader[10]);
                    // 客戶等級
                    switch (SQLReader[14])
                    {
                        case 0:
                            {
                                dr[19] = "優";
                                CGrade[0] = CGrade[0] + 1;
                                break;
                            }

                        case 1:
                            {
                                dr[19] = "A";
                                CGrade[1] = CGrade[1] + 1;
                                break;
                            }

                        case 2:
                            {
                                dr[19] = "B";
                                CGrade[2] = CGrade[2] + 1;
                                break;
                            }

                        default:
                            {
                                dr[19] = "C";
                                CGrade[3] = CGrade[3] + 1;
                                break;
                            }
                    }
                    // dr(19) = SQLReader(14)


                    // 管家請假總次數
                    // SQLReader(11) HL_Number = MyDataSet.Tables("CNumber").Compute("Count(ID)", "Job_Result = '管' and " & Se_String)
                    dr[13] = string.Format("{0:#,##0}", SQLReader[11]);
                    if (SQLReader[8].ToString() == "0")
                        dr[14] = "00.0%";
                    else
                        dr[14] = string.Format("{0:00.0%}", Convert.ToDouble(SQLReader[11]) / Convert.ToDouble(SQLReader[8]));
                    TB[5] = TB[5] + Convert.ToInt16(SQLReader[11]);
                    HC[5] = HC[5] + Convert.ToInt16(SQLReader[11]);

                    // 其它請假總次數
                    // SQLReader(12) OL_Number = TL_Number - CL_Number - HL_Number
                    dr[15] = string.Format("{0:#,##0}", SQLReader[12]);
                    if (SQLReader[8].ToString() == "0")
                        dr[16] = "00.0%";
                    else
                        dr[16] = string.Format("{0:00.0%}", Convert.ToDouble(SQLReader[12]) / Convert.ToDouble(SQLReader[8]));
                    TB[6] = TB[6] + Convert.ToInt16(SQLReader[12]);
                    HC[6] = HC[6] + Convert.ToInt16(SQLReader[12]);

                    dt.Rows.Add(dr);
                }
                INCommand.Connection.Close();

                if (RD > 0)
                {
                    dr = dt.NewRow();
                    dr[0] = Branch.Items[jj].Text;
                    dr[1] = RD;
                    HC[8] = HC[8] + RD;
                    dr[5] = string.Format("{0:#,##0}", TB[0]);
                    dr[6] = string.Format("{0:#,##0}", TB[1]);
                    dr[7] = string.Format("{0:#,##0}", TB[2]);
                    if (TB[1] == 0)
                        dr[8] = "0.0%";
                    else
                        dr[8] = string.Format("{0:00.0%}", Convert.ToDouble(TB[2]) / Convert.ToDouble(TB[1]));

                    dr[9] = string.Format("{0:#,##0}",TB[3]);
                    if (TB[2].ToString() == "0")
                        dr[10] = "00.0%";
                    else
                        dr[10] = string.Format("{0:00.0%}", Convert.ToDouble(TB[3]) / Convert.ToDouble(TB[2]));

                    dr[11] = string.Format("{0:#,##0}", TB[4]);
                    if (TB[2].ToString() == "0")
                        dr[12] = "00.0%";
                    else
                        dr[12] = string.Format("{0:00.0%}", (Convert.ToDouble(TB[4]) / Convert.ToDouble(TB[2])));

                    dr[13] = string.Format("{0:#,##0}", TB[5]);
                    if (TB[2].ToString() == "0")
                        dr[14] = "00.0%";
                    else
                        dr[14] = string.Format("{0:00.0%}", (Convert.ToDouble(TB[5]) / Convert.ToDouble(TB[2])));

                    dr[15] = string.Format("{0:#,##0}", TB[6]);
                    if (TB[2].ToString() == "0")
                        dr[16] = "00.0%";
                    else
                        dr[16] = string.Format("{0:00.0%}", (Convert.ToDouble(TB[6]) / Convert.ToDouble(TB[2])));
                    dr[17] = string.Format("{0:#,##0}", TB[7]);
                    if (TB[2].ToString() == "0")
                        dr[18] = "00.0%";
                    else
                        dr[18] = string.Format("{0:00.0%}", (Convert.ToDouble(TB[7]) / Convert.ToDouble(TB[2])));

                    dt.Rows.InsertAt(dr, 0);
                }

                // 如果沒有記錄檔,則重新產生
                if (RD == 0)
                {
                    if (Branch.Items[jj].Text.Trim() == "南高雄")
                        Select_Banch = " (Branch_Name ='南高雄' or Branch_Name = '屏東') ";
                    else
                        Select_Banch = " Branch_Name ='" + Branch.Items[jj].Text + "'";// 營業所名稱
                    if (Check_Month.SelectedIndex == 0)
                        Te_String = " Year(Job_Date) = " + Check_Year.SelectedValue;
                    else
                        Te_String = " Year(Job_Date) = " + Check_Year.SelectedValue + " and  Month(Job_Date) <= " + Check_Month.SelectedValue;

                    MyDataSet = new System.Data.DataSet();
                    MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT ID,Charge_Staff_Name,Contract_E_Date,Cust_No,Contract_Times,Contract_Status,Contract_U_Date,Branch_Name From Cust_Contract_Data where " + Select_Banch + " Order by Cust_No", INConnection1);
                    MyCommand.Fill(MyDataSet, "Contract_Data");
                    MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT ID,Job_Date,Cust_No,Ltrim(Rtrim(Job_Result)) as Job_Result,Branch_Name FROM Cust_Contract_Job_View where " + Select_Banch + " and " + Te_String, INConnection1);
                    MyCommand.Fill(MyDataSet, "CNumber");


                    int ii, DC;

                    DC = MyDataSet.Tables["Contract_Data"].DefaultView.Count - 1;

                    for (ii = 0; ii <= DC; ii++)
                    {
                        dr = dt.NewRow();
                        // 排序用
                        string Se_String;
                        int CT_Number, YC_Number, YT_Number, TL_Number, CL_Number, HL_Number, OL_Number, GL_Number;
                        double tmpTimes = 1.0;

                        if (DBNull.Value.Equals(MyDataSet.Tables["Contract_Data"].DefaultView[ii][4]) == false)
                            tmpTimes = Convert.ToDouble(MyDataSet.Tables["Contract_Data"].DefaultView[ii][4]);


                        Se_String = " Cust_No = '" + MyDataSet.Tables["Contract_Data"].DefaultView[ii][3] + "'";
                        if (Check_Month.SelectedIndex == 0)
                            YC_Number = Convert.ToInt16(tmpTimes  * 48);    // 合約應做次數
                        else
                            YC_Number = Convert.ToInt16(tmpTimes  * 4 );// 合約應做次數
                        dr[0] = MyDataSet.Tables["Contract_Data"].DefaultView[ii][7];
                        dr[1] = MyDataSet.Tables["Contract_Data"].DefaultView[ii][3];
                        dr[2] = MyDataSet.Tables["Contract_Data"].DefaultView[ii][1];
                        dr[3] = DBNull.Value.Equals(MyDataSet.Tables["Contract_Data"].DefaultView[ii][4])? 1 : MyDataSet.Tables["Contract_Data"].DefaultView[ii][4];
                        dr[4] = MyDataSet.Tables["Contract_Data"].DefaultView[ii][2];
                        dr[6] = YC_Number.ToString();
                        TB[1] = TB[1] + YC_Number;
                        HC[1] = HC[1] + YC_Number;

                        // 計算客戶總次數
                        INCommand = new System.Data.SqlClient.SqlCommand("SELECT Count(ID) FROM Cust_Contract_Job_View where " + Select_Banch + " and " + Se_String, INConnection1);
                        INCommand.Connection.Open();
                        SQLReader = INCommand.ExecuteReader();
                        if (SQLReader.Read() == true)
                            CT_Number = Convert.ToInt16(SQLReader[0]);
                        else
                            CT_Number = 0;
                        INCommand.Connection.Close();
                        dr[5] = CT_Number.ToString();
                        TB[0] = TB[0] + CT_Number;
                        HC[0] = HC[0] + CT_Number;

                        // 客戶時間內所做次數
                        YT_Number = Convert.ToInt16(MyDataSet.Tables["CNumber"].Compute("Count(ID)", Se_String));
                        dr[7] = YT_Number.ToString("#,##0");
                        if (YC_Number == 0)
                            dr[8] = "00.0%";
                        else
                            dr[8] = (YT_Number / (double)YC_Number).ToString("00.0%");
                        TB[2] = TB[2] + YT_Number;
                        HC[2] = HC[2] + YT_Number;


                        // 國春假總次數
                        int TGL1, TGL2;
                        TGL1 = Convert.ToInt16(MyDataSet.Tables["CNumber"].Compute("Count(ID)", "Job_Result = '國' and " + Se_String));
                        TGL2 = Convert.ToInt16(MyDataSet.Tables["CNumber"].Compute("Count(ID)", "Job_Result = '春' and " + Se_String));
                        GL_Number = TGL1 + TGL2;
                        dr[17] = GL_Number.ToString("#,##0");
                        if (YT_Number == 0)
                            dr[18] = "00.0%";
                        else
                            dr[18] = (GL_Number / (double)YT_Number).ToString("00.0%");
                        TB[7] = TB[7] + GL_Number;
                        HC[7] = HC[7] + GL_Number;


                        // 未服務總次數
                        TL_Number = Convert.ToInt16(MyDataSet.Tables["CNumber"].Compute("Count(ID)", "Job_Result <> '' and " + Se_String));
                        TL_Number = TL_Number - GL_Number;   // 減去國春假,屬於真正異常的部份
                        dr[9] = TL_Number.ToString("#,##0");
                        if (YT_Number == 0)
                            dr[10] = "00.0%";
                        else
                            dr[10] = (TL_Number / (double)YT_Number).ToString("00.0%");
                        TB[3] = TB[3] + TL_Number;
                        HC[3] = HC[3] + TL_Number;

                        // 客人請假總次數
                        CL_Number = Convert.ToInt16(MyDataSet.Tables["CNumber"].Compute("Count(ID)", "Job_Result = '客' and " + Se_String));
                        dr[11] = CL_Number.ToString("#,##0");
                        if (YT_Number == 0)
                            dr[12] = "00.0%";
                        else
                            dr[12] = (CL_Number / (double)YT_Number).ToString("00.0%");
                        if (Convert.ToDouble(dr[12].ToString().Substring(0, dr[12].ToString().Length - 1)) < 5.0)
                        {
                            CGrade[0] = CGrade[0] + 1;
                            dr[19] = "優";
                        }
                        else if (Convert.ToDouble(dr[12].ToString().Substring(0, dr[12].ToString().Length - 1)) < 10.0)
                        {
                            CGrade[1] = CGrade[1] + 1;
                            dr[19] = "A";
                        }
                        else if (System.Convert.ToDouble(dr[12].ToString().Substring(0, dr[12].ToString().Length - 1)) < 25.0)
                        {
                            CGrade[2] = CGrade[2] + 1;
                            dr[19] = "B";
                        }
                        else
                        {
                            CGrade[3] = CGrade[3] + 1;
                            dr[19] = "C";
                        }
                        TB[4] = TB[4] + CL_Number;
                        HC[4] = HC[4] + CL_Number;

                        // 管家請假總次數
                        HL_Number = Convert.ToInt16(MyDataSet.Tables["CNumber"].Compute("Count(ID)", "Job_Result = '管' and " + Se_String));
                        dr[13] = HL_Number.ToString("#,##0");
                        if (YT_Number == 0)
                            dr[14] = "00.0%";
                        else
                            dr[14] = (HL_Number / (double)YT_Number).ToString("00.0%");
                        TB[5] = TB[5] + HL_Number;
                        HC[5] = HC[5] + HL_Number;

                        // 其它請假總次數
                        OL_Number = TL_Number - CL_Number - HL_Number;
                        dr[15] = OL_Number.ToString("#,##0");
                        if (YT_Number == 0)
                            dr[16] = "00.0%";
                        else
                            dr[16] = (OL_Number / (double)YT_Number).ToString("00.0%");
                        TB[6] = TB[6] + OL_Number;
                        HC[6] = HC[6] + OL_Number;

                        dt.Rows.Add(dr);
                    }
                    ShareFunction SF = new ShareFunction();

                    // 新增至資料庫
                    for (ii = 0; ii <= dt.Rows.Count - 1; ii++)
                    {
                        string INString;
                        int CG = 3;
                        switch (dt.Rows[ii][19])
                        {
                            case "優":
                                {
                                    CG = 0;
                                    break;
                                }

                            case "A":
                                {
                                    CG = 1;
                                    break;
                                }

                            case "B":
                                {
                                    CG = 2;
                                    break;
                                }
                        }
                        string tmpDate = "";
                        if (SF.IsDate(dt.Rows[ii][4].ToString()))
                            tmpDate = string.Format(dt.Rows[ii][4].ToString(), "yyyy/MM/dd");
                        else
                            tmpDate = "";

                        INString = "Insert into Cust_Serv_Mon_Reports values ( " + Branch.SelectedValue + ",'" + 
                                   dt.Rows[ii][1].ToString().Trim() + "','" + dt.Rows[ii][2].ToString().Trim() + 
                                   "'," + Convert.ToDouble(dt.Rows[ii][3]) + ",'" + Convert.ToDateTime(tmpDate).ToShortDateString() + "'," + 
                                   Convert.ToInt32(dt.Rows[ii][5]) + "," + Convert.ToInt32(dt.Rows[ii][6]) + "," + 
                                   Convert.ToInt32(dt.Rows[ii][7]) + "," + Convert.ToInt32(dt.Rows[ii][9]) + "," + 
                                   Convert.ToInt32(dt.Rows[ii][11]) + "," + Convert.ToInt32(dt.Rows[ii][13]) + "," + 
                                   Convert.ToInt32(dt.Rows[ii][15]) + "," + System.Convert.ToInt32(dt.Rows[ii][17]) + "," +
                                   CG + ",'" + Convert.ToDateTime(Check_Year.SelectedValue+ "/"+ 
                                   (Check_Month.SelectedIndex == 0 ? 12 : Check_Month.SelectedIndex) + "/01").ToShortDateString() + "')";

                        // Label2.Text = Label2.Text & INString
                        INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                        INCommand.Connection.Open();
                        INCommand.ExecuteNonQuery();
                        INCommand.Connection.Close();
                    }


                    dr = dt.NewRow();
                    dr[0] = Branch.Items[jj].Text;
                    dr[1] = MyDataSet.Tables["Contract_Data"].DefaultView.Count;
                    HC[8] = HC[8] + MyDataSet.Tables["Contract_Data"].DefaultView.Count;
                    dr[5] = string.Format("{0:#,##0}", TB[0]);
                    dr[6] = string.Format("{0:#,##0}", TB[1]);
                    dr[7] = string.Format("{0:#,##0}", TB[2]);
                    if (TB[1].ToString() == "0")
                        dr[8] = "0.0%";
                    else
                        dr[8] = string.Format("{0:00.0%}", (Convert.ToDouble(TB[2]) / Convert.ToDouble(TB[1])));

                    dr[9] = string.Format("{0:#,##0}", TB[3]);
                    if (TB[2].ToString() == "0")
                        dr[10] = "00.0%";
                    else
                        dr[10] = string.Format("{0:00.0%}", (Convert.ToDouble(TB[3]) / Convert.ToDouble(TB[2])));

                    dr[11] = string.Format("{0:#,##0}", TB[4]);
                    if (TB[2].ToString() == "0")
                        dr[12] = "00.0%";
                    else
                        dr[12] = string.Format("{0:00.0%}", (Convert.ToDouble(TB[4]) / Convert.ToDouble(TB[2])));

                    dr[13] = string.Format("{0:#,##0}", TB[5]);
                    if (TB[2].ToString() == "0")
                        dr[14] = "00.0%";
                    else
                        dr[14] = string.Format("{0:00.0%}", (Convert.ToDouble(TB[5]) / Convert.ToDouble(TB[2])));

                    dr[15] = string.Format("{0:#,##0}", TB[6]);
                    if (TB[2].ToString() == "0")
                        dr[16] = "00.0%";
                    else
                        dr[16] = string.Format("{0:00.0%}", (Convert.ToDouble(TB[6]) / Convert.ToDouble(TB[2])));
                    dr[17] = string.Format("{0:#,##0}", TB[7]);
                    if (TB[2].ToString() == "0")
                        dr[18] = "00.0%";
                    else
                        dr[18] = string.Format("{0:00.0%}", (Convert.ToDouble(TB[7]) / Convert.ToDouble(TB[2])));
                    if (Branch.SelectedIndex == 0)
                        dt.Rows.Add(dr);
                    else
                        dt.Rows.InsertAt(dr, 0);
                }
            }


            DataGrid1.DataSource = dt.DefaultView;
            DataGrid1.DataBind();
            Session["dt"] = dt;

            Label1.Text = Label1.Text + "-" + DateTime.Now.ToLongTimeString();

            int tmpTotal;

            tmpTotal = CGrade[0] + CGrade[1] + CGrade[2] + CGrade[3];
            Label4.Text = "優:" + string.Format("{0:P1}",(CGrade[0] / (double)tmpTotal));
            Label5.Text = "A:" + string.Format("{0:P1}", (CGrade[1] / (double)tmpTotal));
            Label6.Text = "B:" + string.Format("{0:P1}", (CGrade[2] / (double)tmpTotal));
            Label7.Text = "C:" + string.Format("{0:P1}", (CGrade[3] / (double)tmpTotal));

            Page_Load(sender, e);
        }

        protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            ListItemType objItemType = (ListItemType)e.Item.ItemType;
            Button tmpLabel1;
            ShareFunction SF = new ShareFunction();

            if (objItemType == ListItemType.Item | objItemType == ListItemType.AlternatingItem)
            {
                tmpLabel1 = (Button)e.Item.FindControl("DJob");
                if (SF.IsNumeric(tmpLabel1.Text.Substring(tmpLabel1.Text.Length - 1 , 1)) == false)
                {
                    tmpLabel1.ForeColor = Color.Red;
                    tmpLabel1.Font.Bold = true;
                }
            }
        }

        public void Show_Sc_Type(object sender, DataGridItemEventArgs e)
        {
            ListItemType objItemType = (ListItemType)e.Item.ItemType;
            double tmpdbl = 0.0;

            if (objItemType == ListItemType.Item | objItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.Cells[19].Text == "B" | e.Item.Cells[19].Text == "C")
                    e.Item.ForeColor = Color.Red;
                tmpdbl = e.Item.Cells[16].Text == "0" ? 0 :Convert.ToDouble(e.Item.Cells[16].Text.Substring(0, e.Item.Cells[16].Text.Length - 1));
                if (tmpdbl > 10.0)
                    e.Item.ForeColor = Color.Red;
            }
        }

        protected void DataGrid1_SortCommand(object source, DataGridSortCommandEventArgs e)
        {
            System.Data.DataTable dt = (DataTable)Session["dt"];

            if (e.SortExpression == "Contract_Name")
                dt.DefaultView.Sort = "";
            else
                dt.DefaultView.Sort = e.SortExpression + " DESC";

            DataGrid1.DataSource = dt.DefaultView;
            DataGrid1.DataBind();
            Label1.Text = e.SortExpression;

            Page_Load(source, e);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Label2.Text = "";
            MultiView1.ActiveViewIndex = 1;
        }

        protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            LinkButton tmpLB= (LinkButton)e.Item.Cells[1].Controls[0];

            MyDataSet = new System.Data.DataSet();
            MyCommand = new System.Data.SqlClient.SqlDataAdapter("Select convert(varchar,Job_Date) + rtrim(Job_Result) as DJob from Cust_Job_Schedule where Cust_No = '" + tmpLB.Text + "' order by Job_Date", INConnection1);
            MyCommand.Fill(MyDataSet, "DJob");

            Label2.Text = "客戶編號：" + tmpLB.Text;

            Repeater1.DataSource = MyDataSet.Tables["DJob"].DefaultView;
            Repeater1.DataBind();

            MultiView1.ActiveViewIndex = 0;
        }
        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string RS;

            if (RadioButtonList1.SelectedIndex == 0)
                RS = "";
            else
                RS = " Grade =  '" + RadioButtonList1.SelectedValue + "'";
            System.Data.DataTable dt = (DataTable)Session["dt"];
            DataView dv = new DataView(dt)
            {
                RowFilter = RS
            };

            DataGrid1.DataSource = dv;
            DataGrid1.DataBind();

            // Label1.Text = e.SortExpression

            Page_Load(sender, e);
        }
    }

}