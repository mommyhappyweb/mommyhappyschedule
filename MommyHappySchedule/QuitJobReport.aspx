﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuitJobReport.aspx.cs" Inherits="MommyHappySchedule.QuitJobReport"  MasterPageFile="~/Template/Layout.Master" %>
<asp:Content ID="Style" ContentPlaceHolderID="Style" runat="server">

</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID ="Content" runat="server">
    <div class="container-fluid" id="App">
        <nav class="mt-3 mb-3" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="Index.aspx">首頁</a></li>
                <li class="breadcrumb-item active">離職率報表</li>
            </ol>
        </nav>
        <fieldset class="col-sm-10 offset-sm-1">
            <legend>搜尋條件</legend>
            <div class="form-group row">
                <div class="col-sm-3">
                    <select class="form-control" id="BranchSelect" v-model="BranchSelected">
                        <option v-for="Item in BranchSelectData" :value="Item.Branch_ID">{{ Item.Branch_Name }}</option>
                    </select>
                </div>
                <div class="col-sm-3">
                    <select class="form-control" id="YearSelect" v-model="YearSelected">
                        <option v-for="Item in YearSelectData" :value="Item.ID">{{ Item.Name }}</option>
                    </select>
                </div>
                <div class="col-sm-3">
                    <select class="form-control" id="ReportSelect" v-model="ReportSelected">
                        <option v-for="Item in ReportSelectData" :value="Item.ID">{{ Item.Name }}</option>
                    </select>
                </div>
                <div class="col-sm-3">
                    <input class="btn btn-primary" id="ReportBtn" type="button" value="產生報表" @click="ReportClickEvent" />
                </div>
            </div>
        </fieldset>
        <hr />
        <table class="table table-responsive-lg table-striped table-bordered table-hover text-center" v-if="StayReportShow && StayReportData.length > 0">
            <thead class="thead-light">
                <tr>
                    <th>單位</th>
                    <th>期初滿一年</th>
                    <th>期初未滿一年</th>
                    <th>期初總員工數</th>
                    <th>新入職員工數</th>
                    <th>總員工人數</th>
                    <th>總離職數</th>
                    <th>期末人數</th>
                    <th>留任率百分比</th>
                    <th>舊管家離職數</th>
                    <th>留任率百分比</th>
                    <th>新進總離職數</th>
                    <th>留任率百分比</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="Item in StayReportData">
                    <td>{{ Item.name }}</td>
                    <td>{{ Item.AboveOne }}</td>
                    <td>{{ Item.LessOne }}</td>
                    <td>{{ Item.initTotal }}</td>
                    <td>{{ Item.newComer }}</td>
                    <td>{{ Item.totalStaff }}</td>
                    <td>{{ Item.leftStaff }}</td>
                    <td>{{ Item.currentTotal }}</td>
                    <td v-if="Item.StayProportion > 80">{{ Item.StayProportion }}%</td>
                    <td class="text-danger" v-else>{{ Item.StayProportion }}%</td>
                    <td>{{ Item.oldleft }}</td>
                    <td v-if="Item.oldleftProportion > 80">{{ Item.oldleftProportion }}%</td>
                    <td class="text-danger" v-else>{{ Item.oldleftProportion }}%</td>
                    <td>{{ Item.newleft }}</td>
                    <td v-if="Item.newleftProportion > 80">{{ Item.newleftProportion }}%</td>
                    <td class="text-danger" v-else>{{ Item.newleftProportion }}%</td>
                </tr>
            </tbody>
        </table>
        <table class="table table-responsive-lg table-striped table-bordered table-hover text-center" v-if="PromotionReportShow && PromotionReportData.length > 0">
            <thead class="thead-light">
                <tr>
                    <th>功能</th>
                    <th>部門</th>
                    <th>日期</th>
                    <th>員工編號</th>
                    <th>員工名稱</th>
                    <th>未晉升原因</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="Item in PromotionReportData">
                    <td><input class="btn btn-sm btn-outline-success" type="button" :data-staff_no="Item.Staff_No" @click="PromotionReportUpdateEvent" value="儲存" /></td>
                    <td>{{ Item.Name }}</td>
                    <td>{{ Item.Join_date }}</td>
                    <td>{{ Item.Staff_No }}</td>
                    <td>{{ Item.Staff_Name }}</td>
                    <td>
                        <input class="form-control" type="text" :value="Item.NoPrm_Reason" />
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="table table-responsive-lg table-striped table-bordered table-hover text-center" v-if="HousekeeperReportShow && HousekeeperReportData.length > 0">
            <thead class="thead-light">
                <tr>
                    <th>部門</th>
                    <th>員工名稱</th>
                    <th>期初滿一年</th>
                    <th>期初未滿一年</th>
                    <th>期初總員工數</th>
                    <th>新入職員工數</th>
                    <th>總員工人數</th>
                    <th>總離職數</th>
                    <th>期末人數</th>
                    <th>留任率百分比</th>
                    <th>舊管家離職數</th>
                    <th>留任率百分比</th>
                    <th>新進總離職數</th>
                    <th>留任率百分比</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="Item in HousekeeperReportData">
                    <td>{{ Item.name }}</td>
                    <td>{{ Item.svr_name }}</td>
                    <td>{{ Item.AboveOne }}</td>
                    <td>{{ Item.LessOne }}</td>
                    <td>{{ Item.initTotal }}</td>
                    <td>{{ Item.newComer }}</td>
                    <td>{{ Item.totalStaff }}</td>
                    <td>{{ Item.leftStaff }}</td>
                    <td>{{ Item.currentTotal }}</td>
                    <td v-if="Item.StayProportion > 80">{{ Item.StayProportion }}%</td>
                    <td class="text-danger" v-else>{{ Item.StayProportion }}%</td>
                    <td>{{ Item.oldleft }}</td>
                    <td v-if="Item.oldleftProportion > 80">{{ Item.oldleftProportion }}%</td>
                    <td class="text-danger" v-else>{{ Item.oldleftProportion }}%</td>
                    <td>{{ Item.newleft }}</td>
                    <td v-if="Item.newleftProportion > 80">{{ Item.newleftProportion }}%</td>
                    <td class="text-danger" v-else>{{ Item.newleftProportion }}%</td>
                </tr>
            </tbody>
        </table>
    </div>
</asp:Content>

<asp:Content ID="Script" ContentPlaceHolderID="Script" runat="server">
    <script src="Public/Scripts/Vue/vue.min.js"></script>
    <script src="Public/Scripts/App/QuitJobReportApp.js"></script>
</asp:Content>