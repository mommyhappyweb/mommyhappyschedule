﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Models.Library.DataBase;
using Models.Library;
using Models.Method.Cust;
using Models.Method.Staff;
using Models.Method.Public;
using Models.List;

namespace MommyHappySchedule
{
    public partial class EContractEdit : Page
    {
        private DB DB = new DB();

        private ConvertType ConvertType = new ConvertType();

        public ResponseJsonData Alert = new ResponseJsonData();

        public List<Dictionary<string, object>> ContractData = new List<Dictionary<string, object>>();

        public List<Dictionary<string, object>> JobScheduleData = new List<Dictionary<string, object>>();

        public List<Dictionary<string, object>> JobSchudleResultTotal = new List<Dictionary<string, object>>();

        public List<Dictionary<string, object>> ContractPayData = new List<Dictionary<string, object>>();

        public List<Dictionary<string, object>> ContractPayDetailData = new List<Dictionary<string, object>>();

        public List<Dictionary<string, object>> JobResultData = new List<Dictionary<string, object>>();

        public List<Dictionary<string, object>> PaymentTypeData = new List<Dictionary<string, object>>();

        public List<Dictionary<string, object>> ContractTypeData = new List<Dictionary<string, object>>();

        public List<Dictionary<string, object>> BranchData = new List<Dictionary<string, object>>();

        public List<Dictionary<string, object>> ContractStatusData = new List<Dictionary<string, object>>();

        public Dictionary<string, string> JobTimeData = new Dictionary<string, string>();

        public Dictionary<string, string> JobFlagData = new Dictionary<string, string>();

        public Dictionary<string, string> YesNoData = new Dictionary<string, string>();

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = "合約編輯";
            if (!IsPostBack)
            {
                string Eid = Request.QueryString["Eid"] ?? "";
                if (Eid != "")
                {
                    SetEditForm(Eid);
                }
            }
        }

        /// <summary>
        /// 設置編輯表單
        /// </summary>
        /// <param name="Eid"></param>
        private void SetEditForm(string Eid)
        {
            CustContractDataMethod CustContractDataMethod = new CustContractDataMethod();
            CustJobScheduleMethod CustJobScheduleMethod = new CustJobScheduleMethod();
            CustContractPayMethod CustContractPayMethod = new CustContractPayMethod();
            CustContractPayDetailMethod CustContractPayDetailMethod = new CustContractPayDetailMethod();
            BranchMethod BranchMethod = new BranchMethod();
            ContractStatusMethod ContractStatusMethod = new ContractStatusMethod();
            JobResultMethod JobResultMethod = new JobResultMethod();
            PaymentTypeMethod PaymentTypeMethod = new PaymentTypeMethod();
            ContractTypeMethod ContractTypeMethod = new ContractTypeMethod();

            ContractData = CustContractDataMethod.GetDetailed(Eid);
            JobScheduleData = CustJobScheduleMethod.GetDetailed(Eid);
            JobSchudleResultTotal = CustJobScheduleMethod.GetJobSchudleResultTotal(Eid);
            ContractPayData = CustContractPayMethod.GetDetailed(Eid);
            ContractPayDetailData = CustContractPayDetailMethod.GetDetailed(Eid);
            BranchData = BranchMethod.GetData();
            ContractStatusData = ContractStatusMethod.GetData();
            JobResultData = JobResultMethod.GetData();
            ContractTypeData = ContractTypeMethod.GetData();
            PaymentTypeData = PaymentTypeMethod.GetData();
            JobTimeData = CustJobScheduleMethod.GetJobTimeData();
            JobFlagData = CustJobScheduleMethod.GetJobFlagData();
            YesNoData = CustContractDataMethod.GetYesNoData();
        }
    }
}