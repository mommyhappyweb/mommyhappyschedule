﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JobScheduleTrans.aspx.cs" Inherits="MommyHappySchedule.JobScheduleTrans" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        .auto-style1 {
            border-style: solid; 
            border-width: thin;
            width: 12%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="border-style: double; border-width: medium; width: 100%">
                <tr>
                    <td>
                       來源客戶：
                    </td>
                    <td>
                        <asp:Label ID="ContractID" runat="server" Visible="false"></asp:Label>
                        <asp:TextBox ID="CustNo" runat="server"></asp:TextBox>
                    </td>
                     <td>
                       轉換至客戶：
                    </td>
                    <td>
                        <asp:TextBox ID="TCustNo" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="Button1" runat="server" Text="查尋" OnClick="Button1_Click" />
                    </td>
                     <td>
                         <asp:CheckBox ID="CheckBox1" runat="server" Text="確定轉換" Visible ="false" />    
                        <asp:Button ID="Button2" runat="server" Text="轉換" OnClick="Button2_Click" Visible ="false"   />
                    </td>
                    <td>
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="Index.aspx">返回主選單</asp:HyperLink>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">來源客戶姓名：<asp:Label ID="Contract_ID" runat="server" Visible="false"></asp:Label>
                        <asp:Label ID="Branch_Name" runat="server" Visible="false"></asp:Label>
                    </td>
                    <td class="auto-style1">
                        <asp:Label ID="CustName" runat="server" Text=""></asp:Label></td>
                    <td class="auto-style1">合約編號：</td>
                    <td class="auto-style1">
                        <asp:Label ID="Cust_No" runat="server" Text=""></asp:Label></td>
                    <td class="auto-style1">負責專員：</td>
                    <td class="auto-style1">
                        <asp:Label ID="ChargeStaff" runat="server" Text=""></asp:Label></td>
                    <td class="auto-style1">管家人數：</td>
                    <td class="auto-style1">
                        <asp:Label ID="HKNums" runat="server" Text="1"></asp:Label></td>
                </tr>
                <tr>
                    <td class="auto-style1">合約起始：</td>
                    <td class="auto-style1">
                        <asp:Label ID="StDate" runat="server" Text=""></asp:Label></td>
                    <td class="auto-style1">合約終止：</td>
                    <td class="auto-style1">
                        <asp:Label ID="EnDate" runat="server" Text=""></asp:Label></td>
                    <td class="auto-style1">合約次數：</td>
                    <td class="auto-style1">
                        <asp:Label ID="ContractTimes" runat="server" Text="1"></asp:Label></td>
                    <td class="auto-style1">合約年限：</td>
                    <td class="auto-style1">
                        <asp:Label ID="ContractYears" runat="server" Text="1"></asp:Label></td>
                </tr>
                <tr>
                    <td class="auto-style1">備註：</td>
                    <td colspan="7" class="auto-style1">
                        <asp:Label ID="Note" runat="server" Text=""></asp:Label></td>
                </tr> 
                 <tr>
                    <td class="auto-style1">轉至客戶姓名：<asp:Label ID="TContract_ID" runat="server" Visible="false"></asp:Label>
                        <asp:Label ID="TBranch_Name" runat="server" Visible="false"></asp:Label>
                    </td>
                    <td class="auto-style1">
                        <asp:Label ID="TCustName" runat="server" Text=""></asp:Label></td>
                    <td class="auto-style1">合約編號：</td>
                    <td class="auto-style1">
                        <asp:Label ID="TCust_No" runat="server" Text=""></asp:Label></td>
                    <td class="auto-style1">負責專員：</td>
                    <td class="auto-style1">
                        <asp:Label ID="TChargeStaff" runat="server" Text=""></asp:Label></td>
                    <td class="auto-style1">管家人數：</td>
                    <td class="auto-style1">
                        <asp:Label ID="THKNums" runat="server" Text="1"></asp:Label></td>
                </tr>
                <tr>
                    <td class="auto-style1">合約起始：</td>
                    <td class="auto-style1">
                        <asp:Label ID="TStDate" runat="server" Text=""></asp:Label></td>
                    <td class="auto-style1">合約終止：</td>
                    <td class="auto-style1">
                        <asp:Label ID="TEnDate" runat="server" Text=""></asp:Label></td>
                    <td class="auto-style1">合約次數：</td>
                    <td class="auto-style1">
                        <asp:Label ID="TContractTimes" runat="server" Text="1"></asp:Label></td>
                    <td class="auto-style1">合約年限：</td>
                    <td class="auto-style1">
                        <asp:Label ID="TContractYears" runat="server" Text="1"></asp:Label></td>
                </tr>
                <tr>
                    <td class="auto-style1">備註：</td>
                    <td colspan="7" class="auto-style1">
                        <asp:Label ID="TNote" runat="server" Text=""></asp:Label></td>
                </tr> 
                <tr>
                    <td colspan="8" style="background-color: #FFFFCC">服務次數</td>
                </tr>
                <tr>
                    <td colspan="8">
                        <asp:Repeater ID="Repeater2" runat="server" OnItemDataBound="Repeater1_ItemDataBound" OnItemCommand="Repeater1_ItemCommand">
                            <ItemTemplate>
                                <asp:CheckBox ID="DCheckBox" runat="server"  Visible="false" />
                                <asp:Label ID="ID" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "ID") %>'></asp:Label>
                                <asp:Label ID="JFlag" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "JFlag") %>'></asp:Label>
                                <asp:Button ID="DJob" runat="server" Width="8%" Text='<%# DataBinder.Eval(Container.DataItem, "DJob") %>' BorderStyle="Outset" BorderColor ="#ffffff"></asp:Button>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td>
                </tr>
                <tr>
                    <td colspan="8" class="auto-style8"></td>
                </tr>
               
                <tr>
                    <td colspan="8">
                        <asp:DataGrid ID="DataGrid1" runat="server"></asp:DataGrid></td>
                </tr>
                <tr>
                    <td colspan="8">
                        <asp:GridView ID="GridView1" runat="server"></asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
