﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Models.Library;

namespace MommyHappySchedule
{

    public partial class AddressToGeometry : System.Web.UI.Page
    {
        /// <summary>是否為測試機</summary>
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        System.Data.SqlClient.SqlConnection INConnection = new System.Data.SqlClient.SqlConnection(ConnString);
        System.Data.SqlClient.SqlConnection INConnection1 = new System.Data.SqlClient.SqlConnection(ConnString);

        private System.Data.SqlClient.SqlCommand INCommand, INCommand1;
        private System.Data.SqlClient.SqlDataReader SQLReader;
        private string INString;

        protected void Page_Load(object sender, System.EventArgs e)
        {
        }


        protected void Button1_Click(object sender, EventArgs e)
        {
            WebClient webClient = new WebClient();
            webClient.Encoding = Encoding.UTF8;
            string JsonString, ADString;
            JavaScriptSerializer Serializer = new JavaScriptSerializer();
            Address ItemList;
            int AC = 0;

            INString = "Select Cust_No,Cust_Addr from Cust_Data where  Cust_Addr <> '' ";

            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            while (SQLReader.Read() == true)
            {
                if (!DBNull.Value.Equals(SQLReader[1]))
                {
                    if ((string)SQLReader[1] != "")
                    {
                        // If Not (InStr(SQLReader.Item(1), ",") > 0 Or InStr(SQLReader.Item(1), "/") > 0 Or InStr(SQLReader.Item(1), "\") > 0) Then
                        if (SQLReader[1].ToString().IndexOf('(') > 0)
                            ADString = SQLReader[1].ToString().Substring(1, SQLReader[1].ToString().IndexOf('('));
                        else
                            ADString = (string)SQLReader[1];
                        if (ADString.Length > 0)
                        {

                            // Label1.Text = Label1.Text & "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCg6PptSmUzNUUw-BZW5h5ZXSe4LI8SMyU&address=" & ADString & Chr(10) & Chr(13)
                            // JsonString = webClient.DownloadString("https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCFAVGBNpJ4rdb5YP36TGGvVS0WEBEZMpc&address=" & ADString)   'Gmail
                            JsonString = webClient.DownloadString("https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCMyvr47tuN5E0bwEgemLMaT-fvbztedV4&address=" + ADString);   // Mommyhappy
                            ItemList = Serializer.Deserialize<Address>(JsonString);
                            // 
                            if (ItemList.status == "OK")
                            {
                                // Label1.Text = ItemList.results(0).geometry.location.lat & "," & ItemList.results(0).geometry.location.lng
                                INString = "Update Cust_Data SET Cust_Geometry = '" + ItemList.results[0].geometry.location.lat + "," + ItemList.results[0].geometry.location.lng + "' where Cust_No ='" + SQLReader[0] + "'";
                                INCommand1 = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                                INCommand1.Connection.Open();
                                INCommand1.ExecuteNonQuery();
                                INCommand1.Connection.Close();
                                AC = AC + 1;
                            }
                        }
                    }
                }
            }
            INCommand.Connection.Close();

            // 員工地址轉換
            INString = "Select ID,L_City_Name+L_Country_Name+L_Road_Name as 地址 from Staff_Data  where Geometry is null or  Geometry = ''";

            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            while (SQLReader.Read() == true)
            {
                if (!DBNull.Value.Equals(SQLReader[1]))
                {
                    if ((string)SQLReader[1] != "")
                    {
                        // If Not (InStr(SQLReader.Item(1), ",") > 0 Or InStr(SQLReader.Item(1), "/") > 0 Or InStr(SQLReader.Item(1), "\") > 0) Then
                        if (SQLReader[1].ToString().IndexOf('(') > 0)
                            ADString = SQLReader[1].ToString().Substring(1, SQLReader[1].ToString().IndexOf('('));
                        else
                            ADString = (string)SQLReader[1];
                        // Label1.Text = Label1.Text & "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCg6PptSmUzNUUw-BZW5h5ZXSe4LI8SMyU&address=" & ADString & Chr(10) & Chr(13)
                        // JsonString = webClient.DownloadString("https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCFAVGBNpJ4rdb5YP36TGGvVS0WEBEZMpc&address=" & ADString)   'Gmail
                        JsonString = webClient.DownloadString("https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCg6PptSmUzNUUw-BZW5h5ZXSe4LI8SMyU&address=" + ADString);   // Mommyhappy
                        ItemList = Serializer.Deserialize<Address>(JsonString);
                        // 
                        if (ItemList.status == "OK")
                        {
                            // Label1.Text = ItemList.results(0).geometry.location.lat & "," & ItemList.results(0).geometry.location.lng
                            INString = "Update Staff_Data  SET Geometry = '" + ItemList.results[0].geometry.location.lat + "," + ItemList.results[0].geometry.location.lng + "' where ID ='" + SQLReader[0] + "'";
                            INCommand1 = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                            INCommand1.Connection.Open();
                            INCommand1.ExecuteNonQuery();
                            INCommand1.Connection.Close();
                            AC = AC + 1;
                        }
                    }
                }
            }
            INCommand.Connection.Close();
            Label1.Text = AC.ToString();
        }
    }
}