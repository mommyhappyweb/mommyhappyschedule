﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using Models.Library;
using Models.Method.Staff;
using Models.Library.DataBase;

namespace MommyHappySchedule
{
    public partial class EContractList : Page
    {
        private readonly DB DB = new DB();

        public List<Dictionary<string, object>> ContractData = null;

        public Dictionary<string, object> Alert = null;

        /// <summary>
        /// 轉換型態方法
        /// </summary>
        private readonly ConvertType ConvertType = new ConvertType();

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = "合約列表";
            if (!IsPostBack) {
                if (Request.QueryString["Status"] != null) {
                    Alert = new Dictionary<string, object>()
                    {
                        { "Status",  ConvertType.Boolean(Request.QueryString["Status"])},
                        { "Message",  Request.QueryString["Message"]}
                    };
                }
                SetBranchData();
            }
        }

        /// <summary>
        /// 搜尋事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SearchClick(object sender, EventArgs e)
        {
            string Branch = BranchSelect.Text;
            string SearchSelectVal = SearchSelect.Text;
            string Keyword = SearchValue.Text.Trim();
            ContractData = GetContractData(Branch, SearchSelectVal, Keyword);
        }

        /// <summary>
        /// 取得合約資料
        /// </summary>
        /// <param name="SearchSelectVal"></param>
        /// <param name="Keyword"></param>
        public List<Dictionary<string, object>> GetContractData(string Branch, string SearchSelectVal, string Keyword = "")
        {
            string SearchStr = "";
            if (SearchSelectVal != "" && Keyword != "")
            {
                SearchStr = " AND CCD." + SearchSelectVal + " = @Keyword";
            }
            string Sql = "SELECT CCD.ID, BD.Branch_Name, CCD.Contract_Nums, FORMAT(CCD.Contract_E_Date, 'yyyy-MM-dd') AS Contract_E_Date, CCD.Cust_No, CD.Cust_Name, CCD.Contract_Times, CCD.Charge_Staff_No, SD.Staff_Name FROM Cust_Contract_Data AS CCD LEFT JOIN(SELECT Cust_No, Cust_Name FROM Cust_Data) AS CD ON CD.Cust_No = CCD.Cust_No LEFT JOIN(SELECT Staff_No, Name AS Staff_Name FROM Staff_Data) AS SD ON SD.Staff_No = CCD.Charge_Staff_No LEFT JOIN(SELECT ID, Branch_Name FROM Branch_Data) AS BD ON BD.ID = CCD.Branch_ID WHERE BD.ID = @Branch" + SearchStr + " ORDER BY CCD.Cust_No, CCD.ID ASC";
            Dictionary<string, object> SqlVal = new Dictionary<string, object>()
            {
                { "Keyword", Keyword },
                { "Branch", Branch }
            };
            List<Dictionary<string, object>> Data = DB.GetData(Sql, SqlVal);
            return Data;
        }

        /// <summary>
        /// 設定部門下拉選單
        /// </summary>
        private void SetBranchData()
        {
            BranchMethod BranchMethod = new BranchMethod();
            List<Dictionary<string, object>> Data = BranchMethod.GetData();
            foreach (Dictionary<string, object> Item in Data)
            {
                BranchSelect.Items.Add(new ListItem(ConvertType.String(Item["Branch_Name"]), ConvertType.String(Item["ID"])));
            }
        }
    }
}