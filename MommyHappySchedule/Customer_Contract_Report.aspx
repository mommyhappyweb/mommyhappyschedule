﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Customer_Contract_Report.aspx.cs" Inherits="MommyHappySchedule.Customer_Contract_Report" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>客戶服務次數報表</title>
    <style type="text/css">
        .auto-style1 {
            border-style: solid;
            border-width: 1px;
            padding: 1px 4px;
            text-align: center
        }

        .auto-style2 {
            border-style: solid;
            border-width: 1px;
            padding: 1px 4px;
            text-align: center;
        }
    </style>
</head>
<body>

    <form id="form1" runat="server">


        <h3>&nbsp;</h3>


        <table class="auto-style1">

            <tr>
                <td>駐點：</td>
                <td>
                    <asp:DropDownList ID="Branch" runat="server">
                    </asp:DropDownList>
                </td>
                <td>年份</td>
                <td>
                    <asp:DropDownList ID="Check_Year" runat="server">
                        <asp:ListItem>2016</asp:ListItem>
                        <asp:ListItem Selected="True">2017</asp:ListItem>
                        <asp:ListItem>2018</asp:ListItem>
                        <asp:ListItem>2019</asp:ListItem>
                        <asp:ListItem>2020</asp:ListItem>
                        <asp:ListItem>2021</asp:ListItem>
                    </asp:DropDownList></td>
                <td>月份</td>
                <td>
                    <asp:DropDownList ID="Check_Month" runat="server">
                        <asp:ListItem Selected="True">全年</asp:ListItem>
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                    </asp:DropDownList></td>
                <td>
                    <asp:Button ID="Check_Report" runat="server" Text="產生報表" OnClick="Check_Report_Click" /></td>
                <td>
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="Index.aspx">返回主選單</asp:HyperLink>
                </td>
                <td colspan="2">
                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                </td>
                <td colspan="2" bgcolor="#CCCCCC">報表負責人：韻秋經理</td>
                <td colspan="2">
                    
                   
                </td>
            </tr>
            <tr>
                 <td colspan="4" align="left">
                    <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" AutoPostBack ="true" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged">
                            <asp:ListItem>全部</asp:ListItem>
                            <asp:ListItem>優</asp:ListItem>
                            <asp:ListItem>A</asp:ListItem>
                            <asp:ListItem>B</asp:ListItem>
                            <asp:ListItem Selected ="True" >C</asp:ListItem>
                </asp:RadioButtonList>
                    
                </td>
                <td colspan="3" align="left" bgcolor="#CCCCCC">實際=>
                    <asp:Label ID="Label4" runat="server" Text="0"></asp:Label>
                    <asp:Label ID="Label5" runat="server" Text="0"></asp:Label>
                    <asp:Label ID="Label6" runat="server" Text="0"></asp:Label>
                    <asp:Label ID="Label7" runat="server" Text="0"></asp:Label>

                </td>
                   
            </tr>
        </table>
        <table class="auto-style1">
            <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                <asp:View ID="View1" runat="server">
                    <tr><td colspan="13" align="left">
                    <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Check_Report_Click">
                            <ItemTemplate>
                                <asp:Button ID="DJob" runat="server" Width="115" Text='<%# DataBinder.Eval(Container.DataItem, "DJob") %>'></asp:Button>
                            </ItemTemplate>
                        </asp:Repeater>
                    </td></tr>
                    <tr>
                        <td colspan="13">
                            <asp:Button ID="Button1" runat="server" Text="返回列表" OnClick="Button1_Click" /></td>
                    </tr>
                </asp:View>
                <asp:View ID="View2" runat="server">
                    
                    <tr>
                        <td colspan="13">
                            <asp:DataGrid ID="DataGrid1" runat="server" Font-Names="Arial" OnItemCommand="DataGrid1_ItemCommand"
                                BorderWidth="1" Font-Size="Smaller" CellPadding="2" AutoGenerateColumns="False"
                                OnItemDataBound="Show_Sc_Type" AllowSorting="true" 
                                ForeColor="#333333" GridLines="None">
                                <SelectedItemStyle Font-Bold="True" ForeColor="#333333" BackColor="#C5BBAF"></SelectedItemStyle>
                                <ItemStyle BackColor="#E3EAEB"></ItemStyle>
                                <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#1C5E55"></HeaderStyle>
                                <FooterStyle ForeColor="White" BackColor="#1C5E55" Font-Bold="True"></FooterStyle>
                                <Columns>
                                    <asp:BoundColumn DataField="ID" HeaderText="單位">
                                        <ItemStyle Width="60px" BackColor="#FFFF99" />
                                    </asp:BoundColumn>
                                    <asp:ButtonColumn DataTextField="Contract_Number" HeaderText="客戶編號" SortExpression="Contract_Name">
                                        <ItemStyle Width="80px" BackColor="#FFFF99" />
                                    </asp:ButtonColumn>
                                     <asp:ButtonColumn DataTextField="Contract_Name" HeaderText="客戶姓名" SortExpression="Contract_Name">
                                        <ItemStyle Width="100px" BackColor="#FFFF99" />
                                    </asp:ButtonColumn>
                                    <asp:BoundColumn DataField="Staff_Name" HeaderText="負責專員" HeaderStyle-HorizontalAlign="Center" SortExpression="Staff_Name">
                                        <ItemStyle Width="60px" BackColor="#FFFF99"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Contract_E_Date" HeaderText="合約結束日期" HeaderStyle-HorizontalAlign="Center" DataFormatString="{0:yyyy/MM/dd}">
                                        <ItemStyle Width="60px" BackColor="#FFFF99"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Last_Year_Cu_Lo" HeaderText="去年客戶請假次數" HeaderStyle-HorizontalAlign="Center" SortExpression="Last_Year_Cu_Lo">
                                        <ItemStyle Width="30px" BackColor="#99FF99" HorizontalAlign="Right"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Last_Year_HK_Lo" HeaderText="去年管家請假次數" HeaderStyle-HorizontalAlign="Center" SortExpression="Last_Year_HK_Lo">
                                        <ItemStyle Width="30px" BackColor="#99FF99" HorizontalAlign="Right"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Last_Year_Cu_Gr" HeaderText="去年客戶等級" HeaderStyle-HorizontalAlign="Center" SortExpression="Last_Year_Cu_Gr">
                                        <ItemStyle Width="30px" BackColor="#99FF99" HorizontalAlign="Right"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="This_Year_Cu_Lo" HeaderText="今年客戶請假次數" HeaderStyle-HorizontalAlign="Center" SortExpression="This_Year_Cu_Lo">
                                        <ItemStyle Width="30px" BackColor="#99CCFF" HorizontalAlign="Right"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="This_Year_HK_Lo" HeaderText="今年管家請假次數" HeaderStyle-HorizontalAlign="Center" SortExpression="This_Year_HK_Lo">
                                        <ItemStyle Width="30px" BackColor="#99CCFF" HorizontalAlign="Right"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="This_Year_Cu_Gr" HeaderText="今年客戶等級" HeaderStyle-HorizontalAlign="Center" SortExpression="This_Year_Cu_Gr">
                                        <ItemStyle Width="30px" BackColor="#99CCFF" HorizontalAlign="Right"></ItemStyle>
                                    </asp:BoundColumn>
                                   
                                   
                                </Columns>

                                <EditItemStyle BackColor="#7C6F57" />
                                <AlternatingItemStyle BackColor="White" />
                            </asp:DataGrid>
                        </td>
                    </tr>
                </asp:View>
            </asp:MultiView>
            <tr>
               
            </tr>
        </table>


    </form>
</body>
</html>

