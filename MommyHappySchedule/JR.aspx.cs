﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;
using System.Configuration;
using Models.Library;
using Models.Library.Excel;

namespace MommyHappySchedule
{
    public partial class JR : Page
    {
        private DataSet MyDataSet = new DataSet();
        private SqlDataAdapter MyCommand;
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        SqlConnection INConnection1 = new SqlConnection(ConnString);
        SqlConnection INConnection2 = new SqlConnection("server=60.248.240.187\\ERPDB,2899;database=MH;uid=sa;pwd=Mh@ht5357");
        SqlCommand INCommand, INCommand1;
        SqlDataReader SQLReader, SQLReader1;
        //DataTable dt1;
        //DataRow dr1;


        protected void Page_Load(object sender, EventArgs e)
        {

            if ((string)Session["Account"] == "" | (Session["Account"] == null))
            {
                Session["msg"] = "您沒有使用此程式的權限!!";
                Response.Redirect("Index.aspx");
            }


            if (!Page.IsPostBack)
            {
                MyDataSet = new DataSet();
                MyCommand = new SqlDataAdapter("SELECT * FROM Branch_Data Order by ID ", INConnection1);
                MyCommand.Fill(MyDataSet, "Branch");


                Branch.DataSource = MyDataSet.Tables["Branch"].DefaultView;
                Branch.DataTextField = "Branch_Name";
                Branch.DataValueField = "ID";
                Branch.DataBind();

                Show_Date.Text = DateTime.Now.ToShortDateString();
                End_Date.Text = DateTime.Now.ToShortDateString();
                RadioButtonList1.SelectedIndex = 0;
            }
        }

        protected void Check_Report_Click(object sender, EventArgs e)
        {
            ShareFunction SF = new ShareFunction();
            if (!SF.IsDate(Show_Date.Text) | !SF.IsDate(End_Date.Text))
                return;


            Label2.Text = DateTime.Now.ToLongTimeString();

            string Select_Banch;
            if (Branch.SelectedItem.Text.Trim() == "南高雄")
                Select_Banch = " and (Branch_Name ='南高雄' or Branch_Name = '屏東')";
            else
            {
                if (Branch.SelectedIndex == 0)
                    Select_Banch = " and 1=1 ";
                else
                    Select_Banch = " and Branch_Name ='" + Branch.SelectedItem.Text.Trim() + "'";
            }


            // 找出尚未離職的管家
            string Sql = "SELECT Staff_No,Staff_Name,Job_Title FROM Staff_Job_Data_View where (Depart_Date is null or Depart_Date >= '" +
                Show_Date.Text + "') " +
                Select_Banch + " and Staff_No <> '' and Staff_No is not null  Order by Staff_No";
            MyCommand = new SqlDataAdapter(Sql, INConnection1);
            MyCommand.Fill(MyDataSet, "Staff_Nu");
            string Select_Staff = "";
            foreach (DataRowView Si in MyDataSet.Tables["Staff_Nu"].DefaultView)
                Select_Staff = Select_Staff + "'" + Si[0].ToString().Trim() + "',";
            if (Select_Staff.Length > 0)
                Select_Staff = " and Staff_No in (" + Select_Staff.Substring(0, Select_Staff.Length - 1) + ")";

            if (TextBox6.Text != "")
            {
                Select_Banch = " and (Staff_Name like '%" + TextBox6.Text + "%' or Staff_No like '%" + TextBox6.Text + "%') ";
                Select_Staff = " and (Staff_Name like '%" + TextBox6.Text + "%' or Staff_No like '%" + TextBox6.Text + "%') ";
                //以下是針對駐點內尋找
                //Select_Banch = Select_Banch + " and (Staff_Name like '%" + TextBox6.Text + "%' or Staff_No like '%" + TextBox6.Text + "%') ";
                //Select_Staff = Select_Staff + " and (Staff_Name like '%" + TextBox6.Text + "%' or Staff_No like '%" + TextBox6.Text + "%') ";

            }

            if (TextBox7.Text != "")
            {
                Select_Banch = " and ( Cust_Name like '%" + TextBox7.Text + "%' or Cust_No like '%" + TextBox7.Text + "%' )";
                Select_Staff = " and ( Cust_Name like '%" + TextBox7.Text + "%' or Cust_No like '%" + TextBox7.Text + "%' )";
                //以下是針對駐點內尋找
                //Select_Banch = Select_Banch + " and Cust_Name like '%" + TextBox7.Text + "%' or Cust_No like '%" + TextBox7.Text + "%' ";
                //Select_Staff = Select_Staff + " and Cust_Name like '%" + TextBox7.Text + "%' or Cust_No like '%" + TextBox7.Text + "%' ";

            }


            switch (RadioButtonList1.SelectedIndex)
            {
                default:
                case 0:
                    {
                        Select_Banch = Select_Banch + " and Job_Flag <> 255 ";
                        Select_Staff = Select_Staff + " and Job_Flag <> 255 ";
                        break;
                    }

                case 1:
                    {
                        Select_Banch = Select_Banch + " and Job_Flag = 4 ";
                        Select_Staff = Select_Staff + " and Job_Flag = 4 ";
                        break;
                    }
                case 2:
                    {
                        Select_Banch = Select_Banch + " and Job_Flag = 3 ";
                        Select_Staff = Select_Staff + " and Job_Flag = 3 ";
                        break;
                    }

                case 3:
                    {
                        Select_Banch = Select_Banch + " and Job_Flag = 2 ";
                        Select_Staff = Select_Staff + " and Job_Flag = 2 ";
                        break;
                    }
                case 4:
                    {
                        //如果選擇日期範圍是在今天以前,則打卡異常需包含【服務中】
                        if (DateTime.Compare(Convert.ToDateTime(Show_Date.Text), Convert.ToDateTime(DateTime.Now.ToShortDateString())) < 0
                            & DateTime.Compare(Convert.ToDateTime(End_Date.Text), Convert.ToDateTime(DateTime.Now.ToShortDateString())) < 0)
                        {
                            Select_Banch = Select_Banch + " and (R_In_ID <> 0 or R_Out_ID <> 0 or Job_Flag = 3) ";
                            Select_Staff = Select_Staff + " and (R_In_ID <> 0 or R_Out_ID <> 0 or Job_Flag = 3) ";
                        }
                        else
                        {
                            Select_Banch = Select_Banch + " and (R_In_ID <> 0 or R_Out_ID <> 0) ";
                            Select_Staff = Select_Staff + " and (R_In_ID <> 0 or R_Out_ID <> 0) ";
                        }
                        break;
                    }
            }

            switch (RadioButtonList2.SelectedIndex)
            {
                default:
                case 0:
                    break;
                case 1:
                    {
                        Select_Banch = Select_Banch + " and Job_Time = 0 ";
                        Select_Staff = Select_Staff + " and Job_Time = 0 ";
                        break;
                    }
                case 2:
                    {
                        Select_Banch = Select_Banch + " and Job_Time = 1";
                        Select_Staff = Select_Staff + " and Job_Time = 1 ";
                        break;
                    }

            }


            MyDataSet = new DataSet();
            //配合可以跨區支援,改判斷管家編號並顯示工作
            if (Select_Staff.Length > 0)
                MyCommand = new SqlDataAdapter(
                            "SELECT *,0 as SU_ID FROM Job_Schedule_Punch_Detail where Job_Date >= '" + Show_Date.Text + "' and Job_Date <= '" + End_Date.Text + "' " +
                            Select_Staff + "  Order by Staff_No,Job_Date,Job_Time,Cust_No", INConnection1);
            else
                MyCommand = new SqlDataAdapter(
                            "SELECT *,0 as SU_ID FROM Job_Schedule_Punch_Detail where Job_Date >= '" + Show_Date.Text + "' and Job_Date <= '" + End_Date.Text +
                            "' " + Select_Banch + "  Order by Staff_No,Job_Date,Job_Time,Cust_No", INConnection1);

            MyCommand.Fill(MyDataSet, "Job_Schedule_Punch_Detail");

            //加入公-督-訓的打卡記錄--SU_ID即為公督訓的ID
            if (Select_Staff.Length > 0)
                MyCommand = new SqlDataAdapter(
                            "SELECT * FROM Cust_Job_Schedule_Supervision_View where Job_Date >= '" + Show_Date.Text + "' and Job_Date <= '" + End_Date.Text + "' " +
                            Select_Staff + "  Order by Staff_No,Job_Date,Job_Time,Cust_No", INConnection1);
            else
                MyCommand = new SqlDataAdapter(
                            "SELECT * FROM Cust_Job_Schedule_Supervision_View where Job_Date >= '" + Show_Date.Text + "' and Job_Date <= '" + End_Date.Text +
                            "' " + Select_Banch + "  Order by Staff_No,Job_Date,Job_Time,Cust_No", INConnection1);

            MyCommand.Fill(MyDataSet, "Job_Schedule_Punch_Detail");
            MyDataSet.Tables["Job_Schedule_Punch_Detail"].DefaultView.Sort = "Branch_Name Asc,Staff_No Asc,Job_Date Asc,Job_Time Asc,Cust_No Asc";

            DataGrid1.DataSource = MyDataSet.Tables["Job_Schedule_Punch_Detail"].DefaultView;
            DataGrid1.DataBind();

            Label2.Text = Label2.Text + "-" + DateTime.Now.ToLongTimeString();
        }

        public void Show_Sc_Type(object sender, DataGridItemEventArgs e)
        {
            ListItemType objItemType = (ListItemType)e.Item.ItemType;
            Label TmpLabel;
            //RadioButtonList tmpRadioButtonList;
            DropDownList tmpDropDownList;

            if (e.Item.ItemIndex == DataGrid1.EditItemIndex & e.Item.ItemIndex != -1)
            {
                ((TextBox)e.Item.Cells[9].Controls[0]).CssClass = "form-control";
                ((TextBox)e.Item.Cells[11].Controls[0]).CssClass = "form-control";
                ((TextBox)e.Item.Cells[11].Controls[0]).Wrap = true;
                ((TextBox)e.Item.Cells[13].Controls[0]).CssClass = "form-control";
                ((TextBox)e.Item.Cells[15].Controls[0]).CssClass = "form-control";
                ((TextBox)e.Item.Cells[15].Controls[0]).Wrap = true;
                ((DropDownList)e.Item.FindControl("DropDownList1")).Enabled = true;
                ((DropDownList)e.Item.FindControl("DropDownList2")).Enabled = true;

                //((RadioButtonList)e.Item.FindControl("RadioButtonList3")).SelectedValue = e.Item.Cells[18].Text;
            }

            if (objItemType == ListItemType.Item | objItemType == ListItemType.AlternatingItem)
            {
                TmpLabel = (Label)e.Item.FindControl("Label3");
                tmpDropDownList = (DropDownList)e.Item.FindControl("DropDownList1");
                tmpDropDownList.SelectedValue = e.Item.Cells[20].Text;
                tmpDropDownList = (DropDownList)e.Item.FindControl("DropDownList2");
                tmpDropDownList.SelectedValue = e.Item.Cells[21].Text;

                if (e.Item.Cells[22].Text != "0")   //表示公督訓
                {
                    e.Item.BackColor = Color.LightGray;
                    if (e.Item.ItemIndex > 1)
                        if (DataGrid1.Items[e.Item.ItemIndex - 1].Cells[22].Text == "0")
                            DataGrid1.Items[e.Item.ItemIndex - 1].Cells[19].Enabled = false;
                }

                switch (e.Item.Cells[18].Text)
                {
                    case "0":
                        {
                            TmpLabel.Text = "未繳費";
                            break;
                        }

                    case "1":
                        {
                            TmpLabel.Text = "可排班";
                            break;
                        }

                    case "2":
                        {
                            TmpLabel.Text = "已確認";
                            break;
                        }

                    case "3":
                        {
                            TmpLabel.Text = "服務中";
                            TmpLabel.BackColor = Color.LightGreen;
                            break;
                        }
                    case "4":
                        {
                            TmpLabel.Text = "已服務";
                            TmpLabel.BackColor = Color.LightPink;
                            break;
                        }
                }
            }
        }

        public void Show_Job_Detail(object sender, DataGridCommandEventArgs e)
        {
            LinkButton tmpLB = (LinkButton)e.CommandSource;

            if (tmpLB.CommandName == "照片")
            {
                string rw = "http://mhhsweb.mommyhappy.com/Cust/Edit/?PageName=CustJobSchedulePunch&Mode=U&Did=" + e.Item.Cells[0].Text;
                Response.Write("<script>window.open('" + rw + " ','_blank');</script>");
            }

        }

        protected void Change_Screen(object sender, EventArgs e)
        {
            if (((Control)sender).ID == "Left_Screen")
            {
                Show_Date.Text = Convert.ToDateTime(Show_Date.Text).AddDays(-1).ToShortDateString();
                End_Date.Text = Convert.ToDateTime(End_Date.Text).AddDays(-1).ToShortDateString();
            }
            if (((Control)sender).ID == "Right_Screen")
            {
                Show_Date.Text = Convert.ToDateTime(Show_Date.Text).AddDays(1).ToShortDateString();
                End_Date.Text = Convert.ToDateTime(End_Date.Text).AddDays(1).ToShortDateString();
            }

            RadioButtonList1.ClearSelection();

            Check_Report_Click(sender, e);
        }


        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Check_Report_Click(sender, e);
        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            TextBox6.Text = "";
            TextBox7.Text = "";
            Show_Date.Text = DateTime.Now.ToShortDateString();
            End_Date.Text = DateTime.Now.ToShortDateString();
            DataGrid1.Dispose();
        }

        protected void ExportForFIN_Click(object sender, EventArgs e)
        {
            if ((string)Session["Groups"] != "1" & (string)Session["Groups"] != "2" &
                (string)Session["Groups"] != "4" & (string)Session["Groups"] != "5")
            {
                Response.Write("<script language=JavaScript>alert('無此權限!請洽IT部門');</script>");
                return;
            }



            List<string> Thead = new List<string>();
            List<string[]> Tbodys = new List<string[]>();

            //滙出營收
            if (DataGrid1.Items.Count > 0)
            {
                Thead.Add("月份");
                Thead.Add("駐點");
                Thead.Add("定期次數");
                Thead.Add("定期金額");
                Thead.Add("單次次數");
                Thead.Add("單次金額");
                Thead.Add("合計次數");
                Thead.Add("合計金額");
                Thead.Add("平均售價");

                string BName = DataGrid1.Items[0].Cells[1].Text.Trim();
                int SCount = 0;     //單次次數
                int SAmt = 0;       //單次金額
                int LCount = 0;     //合約次數
                int LAmt = 0;       //
                ShareFunction SF = new ShareFunction();


                for (var ii = 0; ii < DataGrid1.Items.Count; ii++)
                {
                    if (DataGrid1.Items[ii].Cells[18].Text.Trim() == "4")   //如果是已服務
                    {
                        if (BName == DataGrid1.Items[ii].Cells[1].Text.Trim())     //如果是同駐點
                        {
                            if (DataGrid1.Items[ii].Cells[4].Text.Contains("-") == false |     //不是長期合約編號
                                DataGrid1.Items[ii].Cells[24].Text.Contains("次") == true)   //或是合約備註為單次或加次
                            {
                                SCount++;
                                SAmt = SAmt + Convert.ToInt16(SF.IsNumeric(DataGrid1.Items[ii].Cells[23].Text) ? DataGrid1.Items[ii].Cells[23].Text : "0");   //Job_Price
                            }
                            else
                            {
                                LCount++;
                                LAmt = LAmt + Convert.ToInt16(SF.IsNumeric(DataGrid1.Items[ii].Cells[23].Text) ? DataGrid1.Items[ii].Cells[23].Text : "0");   //Job_Price
                            }
                        }
                        else
                        {
                            Tbodys.Add(
                                new string[]
                                {
                                    Show_Date.Text.Substring(0,7),
                                    BName,
                                    LCount.ToString(),
                                    LAmt.ToString(),
                                    SCount.ToString(),
                                    SAmt.ToString(),
                                    (LCount + SCount).ToString(),
                                    (LAmt + SAmt).ToString(),
                                    ((LCount + SCount) == 0) ? "0" : ((LAmt + SAmt)/(LCount + SCount)).ToString()
                                }
                            );
                            BName = DataGrid1.Items[ii].Cells[1].Text.Trim();
                            LCount = LAmt = SAmt = SCount = 0;
                        }
                    }
                }
                //加上最後一筆
                Tbodys.Add(
                     new string[]
                     {
                        Show_Date.Text.Substring(0,7),
                        BName,
                        LCount.ToString(),
                        LAmt.ToString(),
                        SCount.ToString(),
                        SAmt.ToString(),
                        (LCount + SCount).ToString(),
                        (LAmt + SAmt).ToString(),
                        ((LCount + SCount) == 0) ? "0" : ((LAmt + SAmt)/(LCount + SCount)).ToString()
                     }
                );

                ReportsExcel ReportsExcel = new ReportsExcel();
                ReportsExcel.GetExcel(Thead, Tbodys, "Excel", 2003);

            }
        }

        protected void ExportToExcel_Click(object sender, EventArgs e)
        {
            if ((string)Session["Groups"] != "1" & (string)Session["Groups"] != "2" &
                (string)Session["Groups"] != "3" & (string)Session["Groups"] != "4" & (string)Session["Groups"] != "5")
            {
                Response.Write("<script language=JavaScript>alert('無此權限!請洽IT部門');</script>");
                return;
            }
            List<string> Thead = new List<string>();
            List<string[]> Tbodys = new List<string[]>();

            //滙出請假
            if (DataGrid1.Items.Count > 0)
            {
                Thead.Add("序號");
                Thead.Add("駐點");
                Thead.Add("工號");
                Thead.Add("姓名");
                Thead.Add("日期");
                Thead.Add("起始時間");
                Thead.Add("上班時間");
                Thead.Add("上班異常");
                Thead.Add("異常描述");
                Thead.Add("終止時間");
                Thead.Add("下班時間");
                Thead.Add("下班異常");
                Thead.Add("異常描述");
                Thead.Add("狀態");
                Thead.Add("服務情況");

                for (var ii = 0; ii < DataGrid1.Items.Count; ii++)
                {
                    Tbodys.Add(
                        new string[]
                        {
                        ii+1.ToString (),                                                               //流水號
                        DataGrid1.Items[ii].Cells[1].Text.Trim(),                                       //駐點
                        ((LinkButton)DataGrid1.Items[ii].Cells[2].Controls[0]).Text.Trim(),           //管家編號
                        DataGrid1.Items[ii].Cells[3].Text.Trim(),           //管家姓名
                        DataGrid1.Items[ii].Cells[7].Text.Trim(),           //日期
                        DataGrid1.Items[ii].Cells[8].Text.Trim(),           //起始時間
                        DataGrid1.Items[ii].Cells[9].Text.Trim() == "&nbsp;" ? "" : DataGrid1.Items[ii].Cells[9].Text.Trim() ,           //上班時間
                        ((DropDownList)DataGrid1.Items[ii].Cells[10].FindControl("DropDownList1")).SelectedValue ,          //上班異常
                        DataGrid1.Items[ii].Cells[11].Text.Trim() == "&nbsp;" ? "" : DataGrid1.Items[ii].Cells[11].Text.Trim() ,          //異常描述
                        DataGrid1.Items[ii].Cells[12].Text.Trim() == "&nbsp;" ? "" : DataGrid1.Items[ii].Cells[12].Text.Trim() ,          //終止時間
                        DataGrid1.Items[ii].Cells[13].Text.Trim() == "&nbsp;" ? "" : DataGrid1.Items[ii].Cells[13].Text.Trim() ,          //下班時間
                        ((DropDownList)DataGrid1.Items[ii].Cells[14].FindControl("DropDownList2")).SelectedValue ,          //下班異常
                        DataGrid1.Items[ii].Cells[15].Text.Trim() == "&nbsp;" ? "" : DataGrid1.Items[ii].Cells[15].Text.Trim() ,          //異常描述
                        DataGrid1.Items[ii].Cells[16].Text.Trim() == "&nbsp;" ? "" : DataGrid1.Items[ii].Cells[16].Text.Trim() ,          //狀態
                        DataGrid1.Items[ii].Cells[17].Text.Trim() == "&nbsp;" ? "" : DataGrid1.Items[ii].Cells[17].Text.Trim()            //服務情況
                       }
                    );
                }

                ReportsExcel ReportsExcel = new ReportsExcel();
                ReportsExcel.GetExcel(Thead, Tbodys, "Excel", 2003);

            }
        }

        protected void ExportToERP_Click(object sender, EventArgs e)
        {
            if ((string)Session["Groups"] != "1" & (string)Session["Groups"] != "2" &
                (string)Session["Groups"] != "3" & (string)Session["Groups"] != "4" & (string)Session["Groups"] != "5")
            {
                Response.Write("<script language=JavaScript>alert('無此權限!請洽IT部門');</script>");
                return;
            }
            string[] FileArray2 = { "2303", "2301", "2304", "2302", "2201", "2202", "2102", "2101", "2104", "2103", "2306" };
            ShareFunction SF = new ShareFunction();
            int BID = 0;
            Boolean tE = false;

            string Select_Banch;
            if (Branch.SelectedItem.Text.Trim() == "南高雄")
                Select_Banch = " (Branch_Name ='南高雄' or Branch_Name = '屏東')";
            else
                Select_Banch = " Branch_Name ='" + Branch.SelectedItem.Text.Trim() + "'";

            Select_Banch = Select_Banch + " and Job_Flag = 4 ";

            MyDataSet = new DataSet();
            //配合可以跨區支援,改判斷管家編號並顯示工作
            string INString = "SELECT ID,Job_Date,Job_Time,Staff_No,Cust_No,Job_Price,Job_Flag,Job_Status,HK_Serial,Contract_ID," +
                              "Job_Result_ID,Charege_Staff_No,Export_ID,ERP_No,Branch_Name,Branch_ID,C_Tax_Flag FROM Job_Schedule_To_ERP " +
                              " where Job_Date >= '" + Show_Date.Text + "' and Job_Date <= '" + End_Date.Text +
                              "' and " + Select_Banch + "  Order by Cust_No,Job_Date,Job_Time";
            INCommand1 = new SqlCommand(INString, INConnection1);
            INCommand1.Connection.Close();
            INCommand1.Connection.Open();
            SQLReader1 = INCommand1.ExecuteReader();

            while (SQLReader1.Read())
            {
                //先判斷是否有重覆匯入利用 Job_Schedule 的 ID欄位
                INString = "Select TG001 from COPTG where TG001 = '2301' and TG020 like 'ID:" + SQLReader1[0] + "%'";
                INCommand = new SqlCommand(INString, INConnection2);
                INCommand.Connection.Close();
                INCommand.Connection.Open();
                SQLReader = INCommand.ExecuteReader();
                //如果有找到資料則跳過
                if (SQLReader.Read() == true)
                {
                    INCommand.Connection.Close();
                    continue;
                }
                else
                {
                    INCommand.Connection.Close();

                    BID = Convert.ToInt16(SQLReader1[15]) - 1;
                    Boolean TaxCheck = false;
                    if (!DBNull.Value.Equals(SQLReader1[16]))
                        if (Convert.ToInt16(SQLReader1[16]) == 1)
                            TaxCheck = true;
                    //找出最後一筆銷貨單記錄編號
                    string tmpCNo = Convert.ToDateTime(SQLReader1[1]).ToString("yyyyMMdd");
                    INString = "Select top 1 TG002 from COPTG where TG001 = '2301' and TG002 like '" + tmpCNo + "%' Order by TG002 DESC ";
                    INCommand = new SqlCommand(INString, INConnection2);
                    INCommand.Connection.Close();
                    INCommand.Connection.Open();
                    SQLReader = INCommand.ExecuteReader();
                    //如果有找到資料
                    if (SQLReader.Read() == true)
                        tmpCNo = SF.NSubString(SQLReader[0].ToString(), 0, 8) + (Convert.ToInt32(SF.NSubString(SQLReader[0].ToString(), 8, 3)) + 1).ToString("000");
                    else
                        tmpCNo = tmpCNo + "001";

                    INCommand.Connection.Close();
                    //銷貨單頭
                    //INString = "Insert into COPTG (COMPANY,CREATOR,CREATE_DATE,FLAG,CREATE_TIME,TG001,TG002,TG003,TG004,TG005,TG006,TG007," +
                    //           "TG010,TG011,TG012,TG013,TG015,TG016,TG017,TG020,TG022,TG023,TG024,TG025,TG026,TG027,TG028,TG029,TG030,TG031," +
                    //           "TG032,TG033,TG034,TG035,TG036,TG037,TG038,TG041,TG042,TG044,TG045,TG046,TG048,TG049,TG050,TG051,TG052,TG053,TG054," +
                    //           "TG055,TG056,TG059,TG061,TG062,TG063,TG068,TG069,TG070,TG071,TG072,TG082,TG089,TG094,TG097,TG099,TG100,TG101," +
                    //           "TG111) Values ('MH','DS','" + DateTime.Now.ToString("yyyyMMdd") + "',1,'" + DateTime.Now.ToString("HH:mm:ss") +
                    //           "','2301','" + tmpCNo + "','" + SF.NSubString(tmpCNo, 0, 8) + "','" + SQLReader1[4].ToString().Trim() +
                    //           "','" + FileArray2[BID] + "','" + SQLReader1[11].ToString().Trim() + "','','0000','NT$',1, " +
                    //           Convert.ToInt16(SQLReader1[5]) + ",'','" + (TaxCheck ? "7" : "6") + "','" + (TaxCheck ? "2" : "9") + "','ID:" + SQLReader1[0] +
                    //           "',0,'N','N'," + (TaxCheck ? Convert.ToInt16(SQLReader1[5]) * 0.05 : 0).ToString() +
                    //           ",'" + SQLReader1[11].ToString().Trim() + "','','','','N','1',0,1,'N','" + SQLReader1[3].ToString().Trim() +
                    //           "','N','N','" + SF.NSubString(tmpCNo, 0, 8) + "',0,'" +
                    //           SF.NSubString(tmpCNo, 0, 8) + "'," + (TaxCheck ? "0.05" : "0.00") + "," + Convert.ToInt16(SQLReader1[5]) +
                    //           "," + (TaxCheck ? Convert.ToInt16(SQLReader1[5]) * 0.05 : 0).ToString() + ",'2201','" +
                    //           SQLReader1[12].ToString() + "','6401','" + SQLReader1[13].ToString() + "'," + Convert.ToInt16(SQLReader1[5]) +
                    //           "," + (TaxCheck ? Convert.ToInt16(SQLReader1[5]) * 0.05 : 0).ToString() + ",0,'N','N','N','N',0,0,'1',0,'N'" +
                    //           ",0,'5','1','N','" + (TaxCheck ? "S21" : "S17") + "','N',1,'N',0,'0')";
                    INString = "Insert into COPTG (COMPANY,CREATOR,CREATE_DATE,FLAG,CREATE_TIME,TG001,TG002,TG003,TG004,TG005,TG006,TG007," +
                              "TG010,TG011,TG012,TG013,TG015,TG016,TG017,TG020,TG022,TG023,TG024,TG025,TG026,TG027,TG028,TG029,TG030,TG031," +
                              "TG032,TG033,TG034,TG035,TG036,TG037,TG038,TG041,TG042,TG044,TG045,TG046,TG048,TG049,TG050,TG051,TG052,TG053,TG054," +
                              "TG055,TG056,TG059,TG061,TG062,TG063,TG068,TG069,TG070,TG071,TG072,TG082,TG089,TG094,TG097,TG099,TG100,TG101," +
                              "TG111) Values ('MH','DS','" + DateTime.Now.ToString("yyyyMMdd") + "',1,'" + DateTime.Now.ToString("HH:mm:ss") +
                              "','2301','" + tmpCNo + "','" + SF.NSubString(tmpCNo, 0, 8) + "','" + SQLReader1[4].ToString().Trim() +
                              "','" + FileArray2[BID] + "','" + SQLReader1[11].ToString().Trim() + "','','0000','NT$',1, " +
                              Convert.ToInt16(SQLReader1[5]) + ",'','" + (TaxCheck ? "7" : "6") + "','" + (TaxCheck ? "2" : "9") + "','ID:" + SQLReader1[0] +
                              "',0,'N','N'," + (TaxCheck ? Convert.ToInt16(SQLReader1[5]) * 0.05 : 0).ToString() +
                              ",'" + SQLReader1[11].ToString().Trim() + "','','','','N','1',0,1,'N','" + SQLReader1[3].ToString().Trim() +
                              "','N','N','" + SF.NSubString(tmpCNo, 0, 6) + "',0,'" +
                              SF.NSubString(tmpCNo, 0, 8) + "'," + (TaxCheck ? "0.05" : "0.00") + "," + Convert.ToInt16(SQLReader1[5]) +
                              "," + (TaxCheck ? Convert.ToInt16(SQLReader1[5]) * 0.05 : 0).ToString() + ",'','','','',0,0,0,'N','N','N','N',0,0,'1',0,'N'" +
                              ",0,'5','1','N','" + (TaxCheck ? "S21" : "S17") + "','N',1,'N',0,'0')";
                    INCommand = new SqlCommand(INString, INConnection2);
                    INCommand.Connection.Open();
                    INCommand.ExecuteNonQuery();
                    INCommand.Connection.Close();

                    //銷貨單身   
                    //string tmpNumber = "000" + tmpID.Text.Trim();  
                    //tmpNumber = tmpNumber.Substring(tmpNumber.Length - 4, 4);
                    string tmpNumber = "0001";

                    //如果是屏東,則入庫南高
                    string DN = (SF.NSubString(SQLReader1[4].ToString(), 0, 2) == "PT") ? "KN" : SF.NSubString(SQLReader1[4].ToString(), 0, 2);

                    INString = "Insert into COPTH (COMPANY,CREATOR,CREATE_DATE,FLAG,CREATE_TIME,TH001,TH002,TH003,TH004,TH007,TH008,TH009," +
                               "TH012,TH013,TH014,TH015,TH016,TH018,TH020,TH021,TH024,TH025,TH026,TH031,TH035,TH036,TH037,TH038,TH039,TH040," +
                               "TH042,TH043,TH044,TH045,TH046,TH047,TH048,TH049,TH050,TH052,TH053,TH054,TH057,TH061,TH062,TH068,TH069,TH071," +
                               "TH072,TH073,TH080,TH091)" +
                               " Values ('MH','DS','" + DateTime.Now.ToString("yyyyMMdd") + "',1,'" + DateTime.Now.ToString("HH:mm:ss") +
                               "','2301','" + tmpCNo + "','" + tmpNumber + "','AA0101001','" + DN + "',1,'件'," +
                               Convert.ToInt16(SQLReader1[5]) + "," + Convert.ToInt16(SQLReader1[5]) + ",'2201','" +
                               SQLReader1[12].ToString() + "','0001','ID:" + SQLReader1[0] + "','N','N',0,1,'N','1'," +
                               Convert.ToInt16(SQLReader1[5]) + "," + (TaxCheck ? Convert.ToInt16(SQLReader1[5]) * 0.05 : 0).ToString() +
                               "," + Convert.ToInt16(SQLReader1[5]) + "," + (TaxCheck ? Convert.ToInt16(SQLReader1[5]) * 0.05 : 0).ToString() +
                               ",0,0,'N',0,0,0,0,0,0,0,0,0,0,'N',0,1,'件','N',0,0,0," + (TaxCheck ? "0.05" : "0.00") + ",0,0)";

                    INCommand = new SqlCommand(INString, INConnection2);
                    INCommand.Connection.Open();
                    INCommand.ExecuteNonQuery();
                    INCommand.Connection.Close();
                    tE = true;
                }

            }
            if (tE)
                Response.Write("<script language=JavaScript>alert('轉入ERP作業完成!!');</script>");
        }
        public void ItemsGrid_Edit(object sender, DataGridCommandEventArgs e)
        {

            // Set the EditItemIndex property to the index of the item clicked 
            // in the DataGrid control to enable editing for that item. Be sure
            // to rebind the DateGrid to the data source to refresh the control.
            // 儲存舊資料
            Session["OPString"] = (e.Item.Cells[9].Text == "&nbsp;" ? "" : e.Item.Cells[9].Text) + "," +
                                  (e.Item.Cells[11].Text == "&nbsp;" ? "" : e.Item.Cells[11].Text) + "," +
                                  (e.Item.Cells[13].Text == "&nbsp;" ? "" : e.Item.Cells[13].Text) + "," +
                                  (e.Item.Cells[15].Text == "&nbsp;" ? "" : e.Item.Cells[15].Text);
            DataGrid1.EditItemIndex = e.Item.ItemIndex;
            Check_Report_Click(sender, e);
        }

        public void ItemsGrid_Cancel(object sender, DataGridCommandEventArgs e)
        {

            // Set the EditItemIndex property to -1 to exit editing mode.
            // Be sure to rebind the DateGrid to the data source to refresh
            // the control.
            DataGrid1.EditItemIndex = -1;
            Check_Report_Click(sender, e);
        }

        public void ItemsGrid_Update(object sender, DataGridCommandEventArgs e)
        {
            //RadioButtonList PFlag = (RadioButtonList)e.Item.FindControl("RadioButtonList3");

            if (Session["OPString"] != null & (string)Session["OPString"] != "")
            {
                string INString;
                string PID = e.Item.Cells[0].Text;
                string PDate = e.Item.Cells[7].Text;
                string SUID = e.Item.Cells[22].Text;
                TextBox PInTime = (TextBox)e.Item.Cells[9].Controls[0];
                DropDownList PInID = (DropDownList)e.Item.FindControl("DropDownList1");
                TextBox PInDes = (TextBox)e.Item.Cells[11].Controls[0];
                TextBox POutTime = (TextBox)e.Item.Cells[13].Controls[0];
                DropDownList POutID = (DropDownList)e.Item.FindControl("DropDownList2");
                TextBox POutDes = (TextBox)e.Item.Cells[15].Controls[0];
                LinkButton tmpLB = (LinkButton)e.Item.Cells[2].Controls[0];
                string PIn = PDate + " " + PInTime.Text;
                string POut = PDate + " " + POutTime.Text;
                string eMSG = "";
                ShareFunction SF = new ShareFunction();
                Logs SLogs = new Logs();     //記錄更改內容
                string[] OPString = Session["OPString"].ToString().Split(',');   //PinTime,PinDes,PoutTime,PoutDes

                if ((string)Session["Groups"] == "3" & PInTime.Text.Trim() == "" & POutTime.Text.Trim() == "" & SUID.Trim() == "0")
                {
                    //人資部刪除打卡上班及下班時間,則表示清除打卡記錄如果不是公督訓
                    INString = "Delete From Cust_Job_Schedule_Punch  where Schedule_ID = " + PID;
                    INCommand = new SqlCommand(INString, INConnection1);
                    INCommand.Connection.Close();
                    INCommand.Connection.Open();
                    INCommand.ExecuteNonQuery();
                    INCommand.Connection.Close();

                    //更新Job_Schedule 的 Job_Flag
                    INString = "Update Cust_Job_Schedule set Job_Flag = 2 where ID = " + PID;
                    INCommand = new SqlCommand(INString, INConnection1);
                    INCommand.Connection.Open();
                    INCommand.ExecuteNonQuery();
                    INCommand.Connection.Close();

                    //Logs
                    SLogs.ScheduleLogs("Punch.log", Session["Username"].ToString() + "|刪除[" + tmpLB.Text + e.Item.Cells[3].Text +
                          "]的客戶[" + e.Item.Cells[4].Text + e.Item.Cells[4].Text + "][" + PDate + "]-原上班時間[" + OPString[0] +
                          "]-原下班時間[" + OPString[2] + "]的記錄");
                }
                else
                {

                    //上班時間
                    if (!SF.IsDate(PIn))
                        eMSG = "上班時間輸入錯誤!!";

                    if (PInTime.Text.Trim() == "" | (PInID.SelectedValue == "1000" & PInDes.Text.Trim() == ""))
                        eMSG = "上班時間或原因未輸入!!";

                    if (PInTime.Text.Trim() != OPString[0].Trim() & PInID.SelectedValue == "0")
                        eMSG = "有更改上班時間,則原因不能為正常上班!";

                    //如果原因未輸入,則自動帶入
                    if (PInID.SelectedValue != "1000" & PInDes.Text.Trim() == "")
                        PInDes.Text = PInID.SelectedItem.Text;

                    //下班時間
                    if (!SF.IsDate(POut))
                        eMSG = "結束時間輸入錯誤!!";

                    if (POutTime.Text.Trim() == "" | (POutID.SelectedValue == "1000" & POutDes.Text.Trim() == ""))
                        eMSG = "結束時間或原因未輸入!!";

                    if (POutTime.Text.Trim() != OPString[2].Trim() & POutID.SelectedValue == "0")
                        eMSG = "有更改下班時間,則原因不能為正常上班!";

                    //如果原因未輸入,則自動帶入
                    if (POutID.SelectedValue != "1000" & POutDes.Text.Trim() == "")
                        POutDes.Text = POutID.SelectedItem.Text;

                    if (eMSG.Length > 0)    //如果有錯誤
                        Response.Write("<script language=JavaScript>alert('" + eMSG + "');</script>");
                    else
                    {
                        if (SUID.Trim() == "0")   //如果不是公督訓
                        {
                            INString = "Select ID from Cust_Job_Schedule_Punch where Schedule_ID = " + PID;
                            INCommand = new SqlCommand(INString, INConnection1);
                            INCommand.Connection.Close();
                            INCommand.Connection.Open();
                            SQLReader = INCommand.ExecuteReader();
                            //如果有找到資料則更新
                            if (SQLReader.Read() == true)
                                INString = "Update Cust_Job_Schedule_Punch set " +
                                           " Punch_In_Time = '" + PIn + "', R_In_Id = " + PInID.SelectedValue + ", R_In_Des = '" + PInDes.Text + "'," +
                                           " Punch_Out_Time = '" + POut + "', R_Out_Id = " + POutID.SelectedValue + ", R_Out_Des = '" + POutDes.Text + "'" +
                                           " where Schedule_ID = " + PID;
                            else
                                INString = "Insert into Cust_Job_Schedule_Punch (Schedule_ID,Punch_In_Time,R_In_Id,R_In_Des,Punch_Out_Time,R_Out_Id," +
                                           "R_Out_Des ) Values (" + PID + ",'" + PIn + "'," + PInID.SelectedValue + ",'" + PInDes.Text + "','" + POut + "'," +
                                           POutID.SelectedValue + ",'" + POutDes.Text + "')";
                            INCommand = new SqlCommand(INString, INConnection1);
                            INCommand.Connection.Close();
                            INCommand.Connection.Open();
                            INCommand.ExecuteNonQuery();
                            INCommand.Connection.Close();
                        }
                        else
                        {
                            //公督訓
                            INString = "Update Cust_Job_Schedule_Supervision set " +
                                       " Start_Time = '" + PIn + "', Start_Id = " + PInID.SelectedValue + ", Start_Des = '" + PInDes.Text + "'," +
                                       " End_Time = '" + POut + "', End_Id = " + POutID.SelectedValue + ", End_Des = '" + POutDes.Text + "'" +
                                       " where ID = " + SUID;

                            INCommand = new SqlCommand(INString, INConnection1);
                            INCommand.Connection.Close();
                            INCommand.Connection.Open();
                            INCommand.ExecuteNonQuery();
                            INCommand.Connection.Close();
                        }
                        //更新Job_Schedule 的 Job_Flag
                        INString = "Update Cust_Job_Schedule set Job_Flag = 4 where ID = " + PID;
                        INCommand = new SqlCommand(INString, INConnection1);
                        INCommand.Connection.Open();
                        INCommand.ExecuteNonQuery();
                        INCommand.Connection.Close();

                        //Logs
                        SLogs.ScheduleLogs("Punch.log", Session["Username"].ToString() + "|變更[" + tmpLB.Text + e.Item.Cells[3].Text +
                              "]的客戶[" + e.Item.Cells[4].Text + e.Item.Cells[5].Text + "][" + PDate + "]-原上班時間[" + OPString[0] +
                              "]成[" + PInTime.Text + "]-原下班時間[" + OPString[2] + "]成[" + POutTime.Text + "]");
                    }
                }
            }
            // Label2.Text = INString
            DataGrid1.EditItemIndex = -1;
            Check_Report_Click(sender, e);
        }
    }
}