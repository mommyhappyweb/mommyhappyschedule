﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Configuration;
using Models.Library;

namespace MommyHappySchedule
{
    public partial class QuitJobReport : Page
    {
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];

        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = "離職率報表";

            if (Session["account"] == null)
            {
                Session["msg"] = "您沒有使用此程式的權限!!";
                Response.Redirect("Index.aspx");
            }

            if (!Page.IsPostBack)
            {

            }
        }

        protected void SearchClick(object sender, EventArgs e)
        {

        }
    }

}