﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReNewContract.aspx.cs" Inherits="MommyHappySchedule.ReNewContract" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>續約統計報表</title>
    <style type="text/css">
        .auto-style1 {
            border-style: solid;
            border-width: 1px;
            padding: 1px 4px;
            text-align: center
        }
    </style>
</head>
<body>

    <form id="form1" runat="server">


        <h3>&nbsp;</h3>


        <table class="auto-style1">
            <tr>
                <td>駐點：</td>
                <td>
                    <asp:DropDownList ID="Branch" runat="server" AutoPostBack="true" OnSelectedIndexChanged="Branch_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td>年份</td>
                <td>
                    <asp:DropDownList ID="Check_Year" runat="server">
                        <asp:ListItem>2016</asp:ListItem>
                        <asp:ListItem Selected="True">2017</asp:ListItem>
                        <asp:ListItem>2018</asp:ListItem>
                        <asp:ListItem>2019</asp:ListItem>
                        <asp:ListItem>2020</asp:ListItem>
                        <asp:ListItem>2021</asp:ListItem>
                    </asp:DropDownList></td>
                <td>月份</td>
                <td>
                    <asp:DropDownList ID="Check_Month" runat="server">
                        <asp:ListItem Selected="True">1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                    </asp:DropDownList></td>
                <td>搜尋：<asp:TextBox ID="TextBox3" runat="server"></asp:TextBox></td>
                <td>
                    <asp:Button ID="Check_Report" runat="server" Text="檢視資料" OnClick="Check_Report_Click" /></td>


                <td>
                    <asp:Label ID="Label3" runat="server" Text="報表負責人：" bgcolor="#CCCCCC"></asp:Label>

                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="Index.aspx">返回主選單</asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td colspan="9" class="auto-style1">
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox></td>
            </tr>
            <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                <asp:View ID="View1" runat="server">

                    <tr>
                        <td colspan="13">
                            <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" BackColor="White"
                                BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" Font-Names="Arial" Font-Size="Smaller"
                                OnItemCommand="Show_Job_Detail" OnItemDataBound="Show_Sc_Type" BorderStyle="None">
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <SelectedItemStyle BackColor="#669999" ForeColor="White" Font-Bold="True" />
                                <Columns>
                                    <asp:BoundColumn DataField="ID" HeaderText="Type" Visible="False">
                                        <ItemStyle Width="10px" />
                                    </asp:BoundColumn>
                                    <asp:ButtonColumn DataTextField="Branch_Name" HeaderText="部門" CommandName="部門">
                                        <ItemStyle Width="60px" />
                                    </asp:ButtonColumn>
                                    <asp:ButtonColumn DataTextField="Cust_No" HeaderText="客戶編號" CommandName="客戶編號">
                                        <ItemStyle Width="80px" />
                                    </asp:ButtonColumn>
                                    <asp:BoundColumn DataField="Cust_Name" HeaderText="客戶姓名">
                                        <ItemStyle Width="140px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Charge_Staff_Name" HeaderText="負責專員">
                                        <ItemStyle Width="80px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Contract_E_Date" DataFormatString="{0:yyyy/MM/dd}" HeaderText="合約結束日期">
                                        <ItemStyle Width="80px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Contract_Times" HeaderText="合約次數">
                                        <ItemStyle Width="90px" />
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="合約狀況">
                                        <ItemTemplate>
                                            <asp:Label ID="Contract_FLag" runat="server" Width="40px"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="Contract_Status" Visible="False">
                                        <ItemStyle Width="20px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Service_Cycle" HeaderText="服務週期">
                                        <ItemStyle Width="70px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Contract_Years" HeaderText="合約年限">
                                        <ItemStyle Width="70px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="HK_Nums" HeaderText="管家人數">
                                        <ItemStyle Width="40px" />
                                    </asp:BoundColumn>
                                    
                                </Columns>
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                <PagerStyle HorizontalAlign="Left" Mode="NumericPages" BackColor="White" ForeColor="#000066" />
                                <ItemStyle ForeColor="#000066" />
                            </asp:DataGrid>
                        </td>
                    </tr>
                     <tr>
                        <td colspan="13">
                            <asp:DataGrid ID="DataGrid2" runat="server" AutoGenerateColumns="False" BackColor="White"
                                BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" Font-Names="Arial" Font-Size="Smaller"
                                OnItemCommand="Show_Job_Detail" OnItemDataBound="Show_Sc_Type" BorderStyle="None">
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <SelectedItemStyle BackColor="#669999" ForeColor="White" Font-Bold="True" />
                                <Columns>
                                    <asp:BoundColumn DataField="ID" HeaderText="Type" Visible="False">
                                        <ItemStyle Width="10px" />
                                    </asp:BoundColumn>
                                    <asp:ButtonColumn DataTextField="Branch_Name" HeaderText="部門" CommandName="部門">
                                        <ItemStyle Width="60px" />
                                    </asp:ButtonColumn>
                                    <asp:ButtonColumn DataTextField="Cust_No" HeaderText="客戶編號" CommandName="客戶編號">
                                        <ItemStyle Width="80px" />
                                    </asp:ButtonColumn>
                                    <asp:BoundColumn DataField="Cust_Name" HeaderText="客戶姓名">
                                        <ItemStyle Width="140px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Staff_Name" HeaderText="負責專員">
                                        <ItemStyle Width="80px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Contract_E_Date" DataFormatString="{0:yyyy/MM/dd}" HeaderText="原合約結束日">
                                        <ItemStyle Width="80px" />
                                    </asp:BoundColumn>
                                     <asp:BoundColumn DataField="Contract_R_Date" DataFormatString="{0:yyyy/MM/dd}" HeaderText="實際合約結束">
                                        <ItemStyle Width="80px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Contract_Times" HeaderText="合約次數">
                                        <ItemStyle Width="40px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Service_Cycle" HeaderText="服務週期">
                                        <ItemStyle Width="70px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Contract_Years" HeaderText="合約年限">
                                        <ItemStyle Width="40px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="HK_Nums" HeaderText="管家人數">
                                        <ItemStyle Width="40px" />
                                    </asp:BoundColumn> 
                                    <asp:TemplateColumn HeaderText="合約狀況">
                                        <ItemTemplate>
                                            <asp:Label ID="Contract_FLag" runat="server" Width="60px"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="Contract_Result" Visible="False">
                                        <ItemStyle Width="20px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="N_Contract_S_Date" DataFormatString="{0:yyyy/MM/dd}" HeaderText="新約起始日期">
                                        <ItemStyle Width="80px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="N_Contract_Times" HeaderText="合約次數">
                                        <ItemStyle Width="40px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="N_Service_Cycle" HeaderText="服務週期">
                                        <ItemStyle Width="70px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="N_Contract_Years" HeaderText="合約年限">
                                        <ItemStyle Width="40px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="N_HK_Nums" HeaderText="管家人數">
                                        <ItemStyle Width="40px" />
                                    </asp:BoundColumn> 
                                    <asp:BoundColumn DataField="Contract_ID" HeaderText="Type" Visible="False">
                                        <ItemStyle Width="10px" />
                                    </asp:BoundColumn>

                                </Columns>
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                <PagerStyle HorizontalAlign="Left" Mode="NumericPages" BackColor="White" ForeColor="#000066" />
                                <ItemStyle ForeColor="#000066" />
                            </asp:DataGrid>
                        </td>
                    </tr>
                </asp:View>
                <asp:View ID="View2" runat="server">


                    <tr class="auto-style1">

                        <td>客戶編號：</td>
                        <td>
                            <asp:Label ID="ID_Label" runat="server" Text="Label" Visible="false"></asp:Label>
                            <asp:Label ID="CN_Number" runat="server" Width="70px"></asp:Label></td>
                        <td>負責專員：</td>
                        <td>
                            <asp:TextBox ID="SF_Name" runat="server" Width="70px"></asp:TextBox></td>
                        <td>合約次數：</td>
                        <td>
                            <asp:TextBox ID="CT_Number" runat="server" Width="70px"></asp:TextBox></td>
                        <td>服務周期：</td>
                        <td>
                            <asp:DropDownList ID="DropDownList2" runat="server">
                                <asp:ListItem Value="0.5">二周一次</asp:ListItem>
                                <asp:ListItem Value="1">一周一次</asp:ListItem>
                                <asp:ListItem Value="1.5">一周1.5次</asp:ListItem>
                                <asp:ListItem Value="2">一周二次</asp:ListItem>
                                <asp:ListItem Value="3">一周三次</asp:ListItem>
                                <asp:ListItem Value="4">一周四次</asp:ListItem>
                                <asp:ListItem Value="5">一周五次</asp:ListItem>
                                <asp:ListItem Value="6">一周六次</asp:ListItem>
                                <asp:ListItem Value="7">一周七次</asp:ListItem>
                            </asp:DropDownList>
                        </td>

                        <td rowspan="8">
                            <asp:Panel ID="Panel1" runat="server" Visible="false">
                                <asp:Calendar ID="Calendar1" runat="server" BackColor="White" BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px" Width="200px" OnSelectionChanged="Calendar1_SelectionChanged">
                                    <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                                    <NextPrevStyle VerticalAlign="Bottom" />
                                    <OtherMonthDayStyle ForeColor="#808080" />
                                    <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                    <SelectorStyle BackColor="#CCCCCC" />
                                    <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                                    <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                                    <WeekendDayStyle BackColor="#FFFFCC" />
                                </asp:Calendar>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>管家人數：</td>
                        <td>
                            <asp:TextBox ID="HK_Number" runat="server" Width="70px"></asp:TextBox>
                        </td>

                        <td>合約結束日期：</td>
                        <td>
                            <asp:TextBox ID="EC_Date" runat="server" Width="70px"></asp:TextBox><asp:Button ID="EC_Date_B" runat="server" Text="..." OnClick="Show_Calendar" />
                        </td>
                        <td>合約年限：</td>
                        <td>
                            <asp:TextBox ID="CT_Year" runat="server" Width="70px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:RadioButtonList ID="CT_Result" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" OnSelectedIndexChanged="CT_Result_SelectedIndexChanged" AutoPostBack ="true">
                                <asp:ListItem Selected="True">進行中</asp:ListItem>
                                <asp:ListItem>解約</asp:ListItem>
                                <asp:ListItem>保留</asp:ListItem>
                                <asp:ListItem Enabled="false">續約</asp:ListItem>
                                <asp:ListItem Enabled="false">減次</asp:ListItem>
                            </asp:RadioButtonList>

                        </td>
                        <td></td>
                    </tr>

                    <asp:Panel ID="Panel2" runat="server" Visible="false">
                        <tr>
                            <td colspan ="2" class="auto-style1">起始日期：</td>
                            <td colspan ="2" class="auto-style1">
                                <asp:TextBox ID="Start_Date" runat="server" Width="70px"></asp:TextBox><asp:Button ID="Start_Date_B" runat="server" Text="..." OnClick="Show_Calendar" />
                            </td>
                            <td colspan ="2"class="auto-style1">保留周數：</td>
                            <td colspan ="2"class="auto-style1">
                                <asp:DropDownList ID="DropDownList1" runat="server">
                                    <asp:ListItem>0</asp:ListItem>
                                    <asp:ListItem>1</asp:ListItem>
                                    <asp:ListItem>2</asp:ListItem>
                                    <asp:ListItem>3</asp:ListItem>
                                    <asp:ListItem>4</asp:ListItem>
                                    <asp:ListItem>5</asp:ListItem>
                                    <asp:ListItem>6</asp:ListItem>
                                    <asp:ListItem>7</asp:ListItem>
                                    <asp:ListItem>8</asp:ListItem>
                                    <asp:ListItem>9</asp:ListItem>
                                    <asp:ListItem>10</asp:ListItem>
                                </asp:DropDownList>
                            </td>

                        </tr>
<!--                        <tr>
                            <td colspan="9" class="auto-style1" style="color: #FF0000">若選擇【續約】或【減次】，則必需輸入【新約】的相關資料</td>
                        </tr>
                        <tr class="auto-style1">
                            <td>客戶編號：</td>
                            <td>
                                <asp:Label ID="N_ID_Label" runat="server" Text="Label" Visible="false"></asp:Label>
                                <asp:Label ID="N_CN_Number" runat="server" Width="70px"></asp:Label></td>
                            <td>負責專員：</td>
                            <td>
                                <asp:TextBox ID="N_SF_Name" runat="server" Width="70px"></asp:TextBox></td>
                            <td>合約次數：</td>
                            <td>
                                <asp:TextBox ID="N_CT_Number" runat="server" Width="70px"></asp:TextBox></td>
                            <td>服務周期：</td>
                            <td>
                                <asp:DropDownList ID="DropDownList3" runat="server">
                                    <asp:ListItem Value="0.5">二周一次</asp:ListItem>
                                    <asp:ListItem Value="1">一周一次</asp:ListItem>
                                    <asp:ListItem Value="1.5">一周1.5次</asp:ListItem>
                                    <asp:ListItem Value="2">一周二次</asp:ListItem>
                                    <asp:ListItem Value="3">一周三次</asp:ListItem>
                                    <asp:ListItem Value="4">一周四次</asp:ListItem>
                                    <asp:ListItem Value="5">一周五次</asp:ListItem>
                                    <asp:ListItem Value="6">一周六次</asp:ListItem>
                                    <asp:ListItem Value="7">一周七次</asp:ListItem>
                                </asp:DropDownList>
                            </td>


                        </tr>
                        <tr class="auto-style1">
                            <td>管家人數：</td>
                            <td>
                                <asp:TextBox ID="N_HK_Number" runat="server" Width="70px"></asp:TextBox>
                            </td>
                            <td>合約結束日期：</td>
                            <td>
                                <asp:TextBox ID="N_EC_Date" runat="server" Width="70px"></asp:TextBox><asp:Button ID="N_EC_Date_B" runat="server" Text="..." OnClick="Show_Calendar" />
                            </td>
                            <td>合約年限：</td>
                            <td>
                                <asp:TextBox ID="N_CT_Year" runat="server" Width="70px"></asp:TextBox>
                            </td>
                        </tr>

-->
                    </asp:Panel>

                    <tr>
                        <td colspan="9" class="auto-style1">
                            <asp:Button ID="Save" runat="server" Text="儲存並返回" OnClick="Save_Click" />
                            <asp:Button ID="Rest" runat="server" Text="返回列表" OnClick="Rest_Click" /></td>
                    </tr>


                </asp:View>
            </asp:MultiView>
        </table>
    </form>
</body>
</html>
