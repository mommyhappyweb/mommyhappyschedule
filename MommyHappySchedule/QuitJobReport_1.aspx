﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QuitJobReport_1.aspx.cs" Inherits="MommyHappySchedule.QuitJobReport_1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>離職率報表</title>
    <style type="text/css">
        .auto-style1 {
            border-style: solid;
            border-width: 1px;
            padding: 1px 4px;
            text-align: center
        }

        .auto-style2 {
            border-style: solid;
            border-width: 1px;
            padding: 1px 4px;
            text-align: center;
        }
    </style>
</head>
<body>

    <form id="form1" runat="server">

        <table class="auto-style1">
            <tr>
                <td>駐點：</td>
                <td>
                    <asp:DropDownList ID="Branch" runat="server">
                    </asp:DropDownList>
                </td>
                <td>年份</td>
                <td>
                    <asp:DropDownList ID="Check_Year" runat="server">
                        <asp:ListItem>2016</asp:ListItem>
                        <asp:ListItem Selected="True">2017</asp:ListItem>
                        <asp:ListItem>2018</asp:ListItem>
                        <asp:ListItem>2019</asp:ListItem>
                        <asp:ListItem>2020</asp:ListItem>
                        <asp:ListItem>2021</asp:ListItem>
                    </asp:DropDownList></td>
                <td>月份</td>
                <td>
                    <asp:DropDownList ID="Check_Month" runat="server" Visible ="false">
                        <asp:ListItem Selected="True">1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                    </asp:DropDownList></td>
                <td>
                    <asp:Button ID="Check_Report" runat="server" Text="產生報表" OnClick="Check_Report_Click" /></td>
                <td>
                    <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="Index.aspx">返回主選單</asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td colspan="7" align="left">
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged">
                        <asp:ListItem>留任率報表</asp:ListItem>
                        <asp:ListItem>未晉升統計表</asp:ListItem>
                        <asp:ListItem>管家績效表</asp:ListItem>
                    </asp:RadioButtonList>
                                    </td>
                <td bgcolor="#CCCCCC">報表負責人：Peggy</td>
            </tr>
        </table>
        <table class="auto-style1">
            <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                <asp:View ID="View1" runat="server">
                    <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemDataBound">
                        <HeaderTemplate>
                            <tr>
                                <td class="auto-style1" width="100px">單位<br />
                                    月份</td>
                                <td class="auto-style1" bgcolor="#FFFF99">期初滿一年</td>
                                <td class="auto-style1" bgcolor="#FFFF99">期初未滿一年</td>
                                <td class="auto-style1" bgcolor="#FFFF99">期初總員工數</td>
                                <td class="auto-style1" bgcolor="#FFFF99">新入職員工數</td>
                                <td class="auto-style1" bgcolor="#FFFF99">總員工人數</td>
                                <td class="auto-style1" bgcolor="#FFFF99">總離職數</td>
                                <td class="auto-style1" bgcolor="#FFFF99">期末人數</td>
                                <td class="auto-style1" bgcolor="#FFFF99">留任率百分比</td>
                                <td class="auto-style1" bgcolor="#66CCFF">舊管家離職數</td>
                                <td class="auto-style1" bgcolor="#66CCFF">留任率百分比</td>
                                <td class="auto-style1" bgcolor="#66FF33">新進總離職數</td>
                                <td class="auto-style1" bgcolor="#66FF33">留任率百分比</td>
                            </tr>
                            <tr>
                                <td class="auto-style1" width="100px">KPI</td>
                                <td class="auto-style1" ></td>
                                <td class="auto-style1" ></td>
                                <td class="auto-style1" ></td>
                                <td class="auto-style1" ></td>
                                <td class="auto-style1" ></td>
                                <td class="auto-style1" ></td>
                                <td class="auto-style1" ></td>
                                <td class="auto-style1" >80.00%</td>
                                <td class="auto-style1" ></td>
                                <td class="auto-style1" >80.00%</td>
                                <td class="auto-style1" ></td>
                                <td class="auto-style1" >80.00%</td>
                            </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td class="auto-style1">
                                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ID") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#FFFF99">
                                    <asp:Label ID="LBY_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LBY_Number") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#FFFF99">
                                    <asp:Label ID="LBN_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LBN_Number") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#FFFF99">
                                    <asp:Label ID="LBT_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LBT_Number") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#FFFF99">
                                    <asp:Label ID="LBI_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LBI_Number") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#FFFF99">
                                    <asp:Label ID="LT_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LT_Number") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#FFFF99">
                                    <asp:Label ID="LLT_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LLT_Number") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#FFFF99">
                                    <asp:Label ID="LC_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LC_Number") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#FFFF99">
                                    <asp:Label ID="LLT_P" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LLT_P") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#66CCFF">
                                    <asp:Label ID="LLY1_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LLY1_Number") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="LLY2_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LLY2_Number") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="LLY3_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LLY3_Number") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="LLYT_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LLYT_Number") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#66CCFF">
                                    <asp:Label ID="LLY_P" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LLY_P") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#66FF33">
                                    <asp:Label ID="LLN1_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LLN1_Number") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="LLN2_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LLN2_Number") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="LLN3_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LLN3_Number") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="LLNT_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LLNT_Number") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#66FF33">
                                    <asp:Label ID="LLN_P" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LLN_P") %>'> </asp:Label>
                                </td>
                            </tr>

                        </ItemTemplate>
                    </asp:Repeater>
                    <tr>
                        <td colspan="13"></td>
                    </tr>
                </asp:View>
                <asp:View ID="View2" runat="server">
                    <tr>
                        <td colspan="13" align="left">

                            <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" BackColor="White" OnItemDataBound="DataGrid1_ItemDataBound"
                                BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" Font-Names="Arial" Font-Size="Smaller"
                                OnEditCommand="ItemsGrid_Edit" OnCancelCommand="ItemsGrid_Cancel" OnUpdateCommand="ItemsGrid_Update" BorderStyle="None">
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <SelectedItemStyle BackColor="#669999" ForeColor="White" Font-Bold="True" />
                                <Columns>
                                    <asp:EditCommandColumn EditText="編輯" CancelText="取消" UpdateText="存檔" HeaderText="編輯">
                                        <ItemStyle Wrap="False"></ItemStyle>
                                        <HeaderStyle Wrap="False"></HeaderStyle>
                                    </asp:EditCommandColumn>
                                    <asp:BoundColumn DataField="ID" HeaderText="Type" Visible="False" ReadOnly="true">
                                        <ItemStyle Width="10px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Branch" HeaderText="駐點" ReadOnly="true">
                                        <ItemStyle Width="90px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Staff_Name" HeaderText="管家姓名" ReadOnly="true">
                                        <ItemStyle Width="90px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Join_Date" DataFormatString="{0:d}" HeaderText="入職日期" ReadOnly="true">
                                        <ItemStyle Width="80px" />
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="No_P_Re" HeaderText="未晉升原因">
                                        <ItemStyle Width="120px" />
                                    </asp:BoundColumn>
                                </Columns>
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                <PagerStyle HorizontalAlign="Left" Mode="NumericPages" BackColor="White" ForeColor="#000066" />
                                <ItemStyle ForeColor="#000066" />
                            </asp:DataGrid>

                        </td>
                    </tr>
                    <tr>
                        <td>未晉升人數：</td>
                        <td colspan="3">
                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                        </td>
                        <td colspan="9">(入職滿60天，職稱仍為一般服務員的管家)</td>
                    </tr>
                </asp:View>
                 <asp:View ID="View3" runat="server">
                    <asp:Repeater ID="Repeater2" runat="server"  OnItemDataBound="Repeater1_ItemDataBound">
                        <HeaderTemplate>
                            <tr>
                                <td class="auto-style1" width="100px">單位<br />
                                    月份</td>
                                <td class="auto-style1" bgcolor="#FFFF99">期初滿一年</td>
                                <td class="auto-style1" bgcolor="#FFFF99">期初未滿一年</td>
                                <td class="auto-style1" bgcolor="#FFFF99">期初總員工數</td>
                                <td class="auto-style1" bgcolor="#FFFF99">新入職員工數</td>
                                <td class="auto-style1" bgcolor="#FFFF99">總員工人數</td>
                                <td class="auto-style1" bgcolor="#FFFF99">總離職數</td>
                                <td class="auto-style1" bgcolor="#FFFF99">期末人數</td>
                                <td class="auto-style1" bgcolor="#FFFF99">留任率百分比</td>
                                <td class="auto-style1" bgcolor="#66CCFF">舊管家離職數</td>
                                <td class="auto-style1" bgcolor="#66CCFF">留任率百分比</td>
                                <td class="auto-style1" bgcolor="#66FF33">新進總離職數</td>
                                <td class="auto-style1" bgcolor="#66FF33">留任率百分比</td>
                            </tr>
                            <tr>
                                <td class="auto-style1" width="100px">KPI</td>
                                <td class="auto-style1" ></td>
                                <td class="auto-style1" ></td>
                                <td class="auto-style1" ></td>
                                <td class="auto-style1" ></td>
                                <td class="auto-style1" ></td>
                                <td class="auto-style1" ></td>
                                <td class="auto-style1" ></td>
                                <td class="auto-style1" >80.00%</td>
                                <td class="auto-style1" ></td>
                                <td class="auto-style1" >80.00%</td>
                                <td class="auto-style1" ></td>
                                <td class="auto-style1" >80.00%</td>
                            </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td class="auto-style1">
                                    <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ID") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#FFFF99">
                                    <asp:Label ID="LBY_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LBY_Number") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#FFFF99">
                                    <asp:Label ID="LBN_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LBN_Number") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#FFFF99">
                                    <asp:Label ID="LBT_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LBT_Number") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#FFFF99">
                                    <asp:Label ID="LBI_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LBI_Number") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#FFFF99">
                                    <asp:Label ID="LT_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LT_Number") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#FFFF99">
                                    <asp:Label ID="LLT_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LLT_Number") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#FFFF99">
                                    <asp:Label ID="LC_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LC_Number") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#FFFF99">
                                    <asp:Label ID="LLT_P" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LLT_P") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#66CCFF">
                                    <asp:Label ID="LLY1_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LLY1_Number") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="LLY2_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LLY2_Number") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="LLY3_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LLY3_Number") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="LLYT_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LLYT_Number") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#66CCFF">
                                    <asp:Label ID="LLY_P" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LLY_P") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#66FF33">
                                    <asp:Label ID="LLN1_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LLN1_Number") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="LLN2_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LLN2_Number") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="LLN3_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LLN3_Number") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="LLNT_Number" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LLNT_Number") %>'></asp:Label>
                                </td>
                                <td class="auto-style2" bgcolor="#66FF33">
                                    <asp:Label ID="LLN_P" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LLN_P") %>'> </asp:Label>
                                </td>
                            </tr>

                        </ItemTemplate>
                    </asp:Repeater>
                    <tr>
                        <td colspan="13"></td>
                    </tr>
                </asp:View>
            </asp:MultiView>
        </table>


    </form>
</body>
</html>
