﻿using System;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using Models.Library;

namespace MommyHappySchedule
{
    public partial class CP : Page
    {
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        SqlConnection INConnection1 = new SqlConnection(ConnString);
        SqlConnection INConnection2 = new SqlConnection("server=60.248.240.187\\ERPDB,2899;database=MH;uid=sa;pwd=Mh@ht5357");
        SqlCommand INCommand;
        SqlDataReader SQLReader;
        DataSet MyDataSet = new DataSet();
        SqlDataAdapter MyCommand;
        DataTable dt1;
        DataRow dr1;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if ((string)Session["Account"] == "" | (Session["Account"] == null))
                {
                    Session["msg"] = "您沒有使用此程式的權限!!";
                    Response.Redirect("Index.aspx");
                }
                //讀取收款方式資料表
                string INString = "Select * from Public_Payment_Type";
                INCommand = new SqlCommand(INString, INConnection1);
                INCommand.Connection.Open();
                SQLReader = INCommand.ExecuteReader();
                while (SQLReader.Read())
                {
                    RadioButtonList3.Items.Add(new ListItem(SQLReader[1].ToString()));
                }
                INCommand.Connection.Close();

                //讀取假況資料表
                INString = "Select * from Public_JobResult";
                INCommand = new SqlCommand(INString, INConnection1);
                INCommand.Connection.Open();
                SQLReader = INCommand.ExecuteReader();
                while (SQLReader.Read())
                {
                    RadioButtonList1.Items.Add(new ListItem(SQLReader[1].ToString().Trim()));
                }
                INCommand.Connection.Close();

                Panel2.Visible = false;


                if (Page.Request["ID"] != null)
                {
                    ContractID.Text = Page.Request["ID"].Trim();
                    if (ContractID.Text != "")
                        Button1_Click(sender, e);
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Panel3.Visible = false;
            string INString;
            if (ContractID.Text == "")
                INString = "Select ID,Cust_Name,Cust_No,Staff_No,HK_Nums,Contract_S_Date,Contract_E_Date,Contract_Times,Contract_Years,Branch_name,Note" +
                           " from Cust_Contract_Data_View where Cust_No = '" + CustNo.Text + "'";
            else
                INString = "Select ID,Cust_Name,Cust_No,Staff_No,HK_Nums,Contract_S_Date,Contract_E_Date,Contract_Times,Contract_Years,Branch_name,Note" +
                           " from Cust_Contract_Data_View where ID = " + ContractID.Text;

            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            if (SQLReader.Read() == true)
            {
                CustName.Text = DBNull.Value.Equals(SQLReader[1]) ? "" : SQLReader[1].ToString(); //客戶姓名
                Cust_No.Text = DBNull.Value.Equals(SQLReader[2]) ? "" : SQLReader[2].ToString();   //客戶編號
                CustNo.Text = DBNull.Value.Equals(SQLReader[2]) ? "" : SQLReader[2].ToString();   //客戶編號
                ChargeStaff.Text = DBNull.Value.Equals(SQLReader[3]) ? "" : SQLReader[3].ToString();   //員工名稱
                HKNums.Text = DBNull.Value.Equals(SQLReader[4]) ? "1" : SQLReader[4].ToString(); //管家人數
                StDate.Text = DBNull.Value.Equals(SQLReader[5]) ? "" : Convert.ToDateTime(SQLReader[5].ToString()).ToShortDateString();   //合約起始日期
                EnDate.Text = DBNull.Value.Equals(SQLReader[6]) ? "" : Convert.ToDateTime(SQLReader[6].ToString()).ToShortDateString();   //合約終止日期
                ContractTimes.Text = DBNull.Value.Equals(SQLReader[7]) ? "0" : SQLReader[7].ToString();   //合約次數
                ContractYears.Text = DBNull.Value.Equals(SQLReader[8]) ? "1" : SQLReader[8].ToString(); //Contract_Year
                Contract_ID.Text = DBNull.Value.Equals(SQLReader[0]) ? "" : SQLReader[0].ToString();   //Contract_ID
                Branch_Name.Text = DBNull.Value.Equals(SQLReader[9]) ? "" : SQLReader[9].ToString();   //Branch_Name
                Note.Text = DBNull.Value.Equals(SQLReader[10]) ? "" : SQLReader[10].ToString();   //Note
            }
            else
            {
                Response.Write("<script language=JavaScript>alert('沒有此筆記錄');</script>");
                return;
            }
            INCommand.Connection.Close();


            MyDataSet = new DataSet();
            MyCommand = new SqlDataAdapter(
                        "Select Convert(char,[Job_Date],111) as DJob, IIF(Job_Result = '正常', '', Job_Result) AS Job_Result,Job_Price,ID,Job_Flag as JFlag from Cust_Job_Schedule_View where Contract_ID = " + Contract_ID.Text.Trim() +
                        " order by Job_Date DESC,ID DESC", INConnection1);
            MyCommand.Fill(MyDataSet, "DJob");
            MyCommand = new SqlDataAdapter("SELECT * FROM Branch_Data where Branch_Name ='" + Branch_Name.Text.Trim() + "'", INConnection1);
            MyCommand.Fill(MyDataSet, "Branch");
            MyCommand = new SqlDataAdapter("SELECT *  FROM Holiday_Date", INConnection1);
            MyCommand.Fill(MyDataSet, "Holiday_Date");
            MyCommand = new SqlDataAdapter("SELECT *  FROM Branch_Charge_Data where Branch_ID= " +
                                                                 MyDataSet.Tables["Branch"].DefaultView[0][0], INConnection1);
            MyCommand.Fill(MyDataSet, "Branch_Charge");

            DataTable ds = new DataTable();
            ds.Columns.Add(new DataColumn("DJob", typeof(string)));
            ds.Columns.Add(new DataColumn("ID", typeof(int)));
            ds.Columns.Add(new DataColumn("JFlag", typeof(int)));


            // 計算合次合約顯示
            double CN = 1;
            if (ContractTimes.Text != "0")
                //CN = Convert.ToDouble(ContractTimes.Text) * Convert.ToDouble(ContractYears.Text) * Convert.ToDouble(HKNums.Text) * 48;
                CN = Convert.ToDouble(ContractTimes.Text) * Convert.ToDouble(ContractYears.Text) * 48;
            if (Note.Text.Contains("單次") == true & Note.Text.Contains("單次轉合約") == false)
                CN = 14;
            int ii = 0;
            int tmpTotal = 0;
            DateTime SDate = DateTime.Today;
            while (CN > 0 & ii < MyDataSet.Tables["DJob"].DefaultView.Count)
            {
                DataRow dr;
                dr = ds.NewRow();
                dr[1] = MyDataSet.Tables["DJob"].DefaultView[ii][3];
                dr[2] = MyDataSet.Tables["DJob"].DefaultView[ii][4];
                SDate = Convert.ToDateTime(MyDataSet.Tables["DJob"].DefaultView[ii][0]);   //合約起始日
                if (DBNull.Value.Equals(MyDataSet.Tables["DJob"].DefaultView[ii][1]))
                {
                    dr[0] = MyDataSet.Tables["DJob"].DefaultView[ii][0].ToString().Trim();
                    tmpTotal = tmpTotal + Convert.ToInt32(MyDataSet.Tables["DJob"].DefaultView[ii][2]);
                }
                else
                {
                    if (MyDataSet.Tables["DJob"].DefaultView[ii][1].ToString().Trim() == "")
                    {
                        dr[0] = MyDataSet.Tables["DJob"].DefaultView[ii][0].ToString().Trim();
                        tmpTotal = tmpTotal + Convert.ToInt32(MyDataSet.Tables["DJob"].DefaultView[ii][2]);
                    }
                    else
                        dr[0] = MyDataSet.Tables["DJob"].DefaultView[ii][0].ToString().Trim() +
                                MyDataSet.Tables["DJob"].DefaultView[ii][1].ToString().Trim();
                }
                ds.Rows.Add(dr);
                if (!DBNull.Value.Equals(MyDataSet.Tables["DJob"].DefaultView[ii][1]))
                    if (MyDataSet.Tables["DJob"].DefaultView[ii][1].ToString().Trim() != "")
                        CN = CN + 1;     //如果有請假
                CN = CN - 1;
                ii = ii + 1;
            }

            ds.DefaultView.Sort = "DJob ASC";
            Repeater2.DataSource = ds.DefaultView;
            Repeater2.DataBind();

            // 非合約內次數資料
            ds = new DataTable();
            ds.Columns.Add(new DataColumn("DJob", typeof(string)));
            ds.Columns.Add(new DataColumn("ID", typeof(int)));
            ds.Columns.Add(new DataColumn("JFlag", typeof(int)));

            for (CN = ii; CN <= MyDataSet.Tables["DJob"].DefaultView.Count - 1; CN++)
            {
                DataRow dr;
                dr = ds.NewRow();
                if (DBNull.Value.Equals(MyDataSet.Tables["DJob"].DefaultView[(int)CN][1]))
                    dr[0] = MyDataSet.Tables["DJob"].DefaultView[(int)CN][0].ToString().Trim();
                else
                {
                    if (MyDataSet.Tables["DJob"].DefaultView[(int)CN][1].ToString().Trim() == "")
                        dr[0] = MyDataSet.Tables["DJob"].DefaultView[(int)CN][0].ToString().Trim();
                    else
                        dr[0] = MyDataSet.Tables["DJob"].DefaultView[(int)CN][0].ToString().Trim() +
                                MyDataSet.Tables["DJob"].DefaultView[(int)CN][1].ToString().Trim();
                }
                dr[1] = MyDataSet.Tables["DJob"].DefaultView[(int)CN][3];
                dr[2] = MyDataSet.Tables["DJob"].DefaultView[(int)CN][4];
                ds.Rows.Add(dr);
            }
            ds.DefaultView.Sort = "DJob ASC";
            Repeater1.DataSource = ds.DefaultView;
            Repeater1.DataBind();

            MyDataSet.Tables["DJob"].DefaultView.Sort = "DJob ASC";
            Repeater3.DataSource = MyDataSet.Tables["DJob"].DefaultView;
            Repeater3.DataBind();

            //判斷是否為單次轉合約
            int STotal = 0;
            if (Note.Text.Contains("單次轉合約") == true)
            {
                //INString = "Select ID,Cust_Name,Cust_No,Staff_No,HK_Nums,Contract_S_Date,Contract_E_Date,Contract_Times,Contract_Years,Branch_name,Note" +
                //          " from Cust_Contract_Data_View where Cust_No = '" + Note.Text.Substring(5, 8) + "'";
                INString = "Select ID,Cust_Name,Cust_No,Staff_No,HK_Nums,Contract_S_Date,Contract_E_Date,Contract_Times,Contract_Years,Branch_name,Note" + " from Cust_Contract_Data_View where Cust_No = '" + Cust_No.Text + "'";

                INCommand = new SqlCommand(INString, INConnection1);
                INCommand.Connection.Open();
                SQLReader = INCommand.ExecuteReader();
                if (SQLReader.Read() == true)
                {
                    string tmpSID = SQLReader[0].ToString();
                    INCommand.Connection.Close();
                    Panel3.Visible = true;
                    ds = new DataTable();
                    ds.Columns.Add(new DataColumn("DJob", typeof(string)));
                    ds.Columns.Add(new DataColumn("ID", typeof(int)));
                    ds.Columns.Add(new DataColumn("JFlag", typeof(int)));

                    string QueryJobDataSql = "Select Convert(char,[Job_Date],111) as DJob,IIF(Job_Result = '正常', '', Job_Result) AS Job_Result,Job_Price,ID,Job_Flag as JFlag from " +
                       "Cust_Job_Schedule_View where Contract_ID = " + tmpSID + " order by Job_Date DESC,ID DESC";
                    MyCommand = new SqlDataAdapter(QueryJobDataSql, INConnection1);
                    MyCommand.Fill(MyDataSet, "SDJob");

                    for (CN = 0; CN <= MyDataSet.Tables["SDJob"].DefaultView.Count - 1; CN++)
                    {
                        DataRow dr;
                        dr = ds.NewRow();
                        if (DBNull.Value.Equals(MyDataSet.Tables["SDJob"].DefaultView[(int)CN][1]))
                            dr[0] = MyDataSet.Tables["SDJob"].DefaultView[(int)CN][0].ToString().Trim();
                        else
                        {
                            if (MyDataSet.Tables["SDJob"].DefaultView[(int)CN][1].ToString().Trim() == "")
                                dr[0] = MyDataSet.Tables["SDJob"].DefaultView[(int)CN][0].ToString().Trim();
                            else
                                dr[0] = MyDataSet.Tables["SDJob"].DefaultView[(int)CN][0].ToString().Trim() +
                                        MyDataSet.Tables["SDJob"].DefaultView[(int)CN][1].ToString().Trim();
                        }
                        dr[1] = MyDataSet.Tables["SDJob"].DefaultView[(int)CN][3];
                        dr[2] = MyDataSet.Tables["SDJob"].DefaultView[(int)CN][4];
                        ds.Rows.Add(dr);
                        STotal = STotal + Convert.ToInt16(MyDataSet.Tables["SDJob"].DefaultView[(int)CN][2]);

                    }
                    ds.DefaultView.Sort = "DJob ASC";
                    Repeater5.DataSource = ds.DefaultView;
                    Repeater5.DataBind();
                }
                INCommand.Connection.Close();
            }
            TotalFee.Text = tmpTotal.ToString("#,##0");
            TotalTax.Text = ((TaxCheck.Checked == true) ? (double)tmpTotal * 0.05 : 0).ToString("#,##0");

            tmpTotal = tmpTotal + STotal;   //加上單次合約金額

            dt1 = new DataTable();
            dr1 = default(DataRow);

            //收款資料表
            dt1.Columns.Add(new DataColumn("ID", typeof(int)));
            dt1.Columns.Add(new DataColumn("PayDate", typeof(System.DateTime)));
            dt1.Columns.Add(new DataColumn("PayType", typeof(int)));
            dt1.Columns.Add(new DataColumn("PayText", typeof(string)));
            dt1.Columns.Add(new DataColumn("PayAmt", typeof(int)));
            dt1.Columns.Add(new DataColumn("PayTax", typeof(int)));
            dt1.Columns.Add(new DataColumn("PayDone", typeof(bool)));
            dt1.Columns.Add(new DataColumn("RPayDate", typeof(System.DateTime)));

            //找尋即有的收款資料
            MyCommand = new SqlDataAdapter("SELECT top 1 * FROM Cust_Contract_Pay where Contract_ID = '" + Contract_ID.Text.Trim() +
                                                                "' order by Contract_ID DESC", INConnection1);
            MyCommand.Fill(MyDataSet, "Contract_Pay");

            if (MyDataSet.Tables["Contract_Pay"].DefaultView.Count > 0)  //表示有記錄
            {
                RadioButtonList3.SelectedIndex = Convert.ToInt16(MyDataSet.Tables["Contract_Pay"].DefaultView[0][4]);
                CR_Date.Text = MyDataSet.Tables["Contract_Pay"].DefaultView[0][7].ToString();
                Bank_No.Text = MyDataSet.Tables["Contract_Pay"].DefaultView[0][5].ToString();
                Account_No.Text = MyDataSet.Tables["Contract_Pay"].DefaultView[0][6].ToString();
                TotalFee.Text = string.Format("{0:#,##0}", MyDataSet.Tables["Contract_Pay"].DefaultView[0][8].ToString());
                TotalTax.Text = string.Format("{0:#,##0}", MyDataSet.Tables["Contract_Pay"].DefaultView[0][9].ToString());
                TaxCheck.Checked = (bool)MyDataSet.Tables["Contract_Pay"].DefaultView[0][10];

                MyCommand = new SqlDataAdapter("SELECT * FROM Cust_Contract_Pay_Detail where Pay_ID = " +
                                                                              MyDataSet.Tables["Contract_Pay"].Rows[0][0].ToString(), INConnection1);
                MyCommand.Fill(MyDataSet, "Contract_Pay_Detail");

                for (int Ki = 0; Ki < MyDataSet.Tables["Contract_Pay_Detail"].DefaultView.Count; Ki++)
                {
                    dr1 = dt1.NewRow();
                    dr1[0] = MyDataSet.Tables["Contract_Pay_Detail"].DefaultView[Ki][2];
                    dr1[1] = MyDataSet.Tables["Contract_Pay_Detail"].DefaultView[Ki][8];
                    dr1[2] = MyDataSet.Tables["Contract_Pay_Detail"].DefaultView[Ki][3];
                    dr1[3] = MyDataSet.Tables["Contract_Pay_Detail"].DefaultView[Ki][5];
                    dr1[4] = MyDataSet.Tables["Contract_Pay_Detail"].DefaultView[Ki][7];
                    dr1[5] = MyDataSet.Tables["Contract_Pay_Detail"].DefaultView[Ki][12];
                    dr1[6] = MyDataSet.Tables["Contract_Pay_Detail"].DefaultView[Ki][9];
                    dr1[7] = MyDataSet.Tables["Contract_Pay_Detail"].DefaultView[Ki][11];
                    dt1.Rows.Add(dr1);
                }
                //DataGrid1.DataSource = MyDataSet.Tables["Contract_Pay"].DefaultView;
                //DataGrid1.DataBind();

                //GridView1.DataSource = MyDataSet.Tables["Contract_Pay_Detail"].DefaultView;
                //GridView1.DataBind();
            }

            else
            {
                if (ContractTimes.Text == "0")  //單次
                {
                    dr1 = dt1.NewRow();
                    dr1[0] = 1;
                    dr1[1] = SDate;
                    dr1[2] = 4;
                    dr1[3] = "";
                    dr1[4] = tmpTotal;
                    dr1[5] = (TaxCheck.Checked == true) ? (double)tmpTotal * 0.05 : 0;
                    dt1.Rows.Add(dr1);
                }
                else
                {
                    //顯示每次收款方式及內容
                    int i;
                    for (i = 1; i <= Convert.ToInt16(ContractYears.Text) * 12; i++)
                    {
                        dr1 = dt1.NewRow();
                        dr1[0] = i;
                        if (ds.Rows.Count > 0)   //表示續約
                            dr1[1] = (i <= 1) ? SDate.AddDays(-15) : Convert.ToDateTime((SDate.Day > 15) ? SDate.AddMonths(i - 1).ToString("yyyy/MM") + "/15" : SDate.AddMonths(i - 1).ToString("yyyy/MM") + "/01");
                        else
                            dr1[1] = (i <= 2) ? SDate.AddDays(-15) : Convert.ToDateTime((SDate.Day > 15) ? SDate.AddMonths(i - 1).ToString("yyyy/MM") + "/15" : SDate.AddMonths(i - 1).ToString("yyyy/MM") + "/01");
                        dr1[2] = 0;
                        dr1[3] = "";
                        //第2期之後都是依照每月扣款金額,第一期則是依實際金額減去每月的扣款金額剩下的
                        if (STotal > (tmpTotal / (12 * Convert.ToInt16(ContractYears.Text))))   //如果前單次還沒扣完
                        {
                            dr1[4] = 0;
                            STotal = STotal - (tmpTotal / (12 * Convert.ToInt16(ContractYears.Text)));
                        }
                        else if (STotal > 0)
                        {
                            dr1[4] = (tmpTotal / (12 * Convert.ToInt16(ContractYears.Text))) - STotal;
                            STotal = 0;
                        }
                        else
                            dr1[4] = (tmpTotal / (12 * Convert.ToInt16(ContractYears.Text)));

                        dr1[5] = (TaxCheck.Checked == true) ? Convert.ToDouble(dr1[4]) * 0.05 : 0;
                        dt1.Rows.Add(dr1);
                    }

                }
                dt1.DefaultView.Sort = "ID";
            }
            Repeater4.DataSource = dt1.DefaultView;
            Repeater4.DataBind();

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            //儲存收款資料
            //如果曾經儲存過,則只要判斷是否有新增收款記錄,儲存並滙出即可,其餘表單不做

            string[] FileArray2 = { "2303", "2301", "2304", "2302", "2201", "2202", "2102", "2101", "2104", "2103", "2306" };
            int BID = 0;
            string tmpCNo;
            int tmpPayID = 0;
            string tmpTotalFee = (TotalFee.Text.Contains(",") ? TotalFee.Text.Remove(TotalFee.Text.IndexOf(','), 1) : TotalFee.Text).Trim();
            string tmpTotalTax = (TotalTax.Text.Contains(",") ? TotalTax.Text.Remove(TotalTax.Text.IndexOf(','), 1) : TotalTax.Text).Trim();
            ShareFunction SF = new ShareFunction();



            string INString = "Select * from Cust_Contract_Pay where Contract_ID = " + Contract_ID.Text.Trim();
            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Close();
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            //如果有找到資料
            if (SQLReader.Read() == true)
            {
                tmpPayID = Convert.ToInt32(SQLReader[0]);    //讀取ID
                tmpCNo = SQLReader[13].ToString();  //讀取結帳單編號,做為收款單使用
                INCommand.Connection.Close();

                //更新交易收款資料表
                foreach (Control item in Repeater4.Items)   //收款方式記錄
                {
                    Label tmpID = (Label)item.FindControl("ID");
                    TextBox tmpPayDate = (TextBox)item.FindControl("PayDate");
                    DropDownList tmpPayType = (DropDownList)item.FindControl("PayType");
                    TextBox tmpPayText = (TextBox)item.FindControl("PayText");
                    TextBox tmpPayAmt = (TextBox)item.FindControl("PayAmt");
                    CheckBox tmpPayDone = (CheckBox)item.FindControl("PayDone");
                    TextBox tmpPayTax = (TextBox)item.FindControl("PayTax");
                    Label tmpRPayDate = (Label)item.FindControl("R_PayDate");

                    string sPayAmt = (tmpPayAmt.Text.Contains(",") ? tmpPayAmt.Text.Remove(tmpPayAmt.Text.IndexOf(','), 1) : tmpPayAmt.Text).Trim();
                    string sPayTax = (tmpPayTax.Text.Contains(",") ? tmpPayTax.Text.Remove(tmpPayTax.Text.IndexOf(','), 1) : tmpPayTax.Text).Trim();



                    if (!SF.IsDate(tmpRPayDate.Text))     //如果已有收款記錄則不處理
                    {
                        INString = "Update Cust_Contract_Pay_Detail set" +
                                   " Payment_Type =" + tmpPayType.SelectedIndex.ToString() + "," +
                                   " Out_Acct_No = '" + SF.NSubString(tmpPayText.Text.Trim(), 0, 20) + "'," +
                                   " FulFill = " + ((tmpPayDone.Checked == true) ? "1" : "0") + "," +
                                   " R_Pay_Date = " + ((tmpPayDone.Checked == true) ? "'" + DateTime.Now.ToShortDateString() + "'" : "null") +
                                   " where Pay_ID = " + tmpPayID.ToString() + " and Pay_Number = " + tmpID.Text.Trim();

                        INCommand = new SqlCommand(INString, INConnection1);
                        INCommand.Connection.Open();
                        INCommand.ExecuteNonQuery();
                        INCommand.Connection.Close();
                    }
                }
            }
            else
            {
                //收款期數 單次及加次為1,其餘為年份 * 12
                string tmpPayNumber = (ContractTimes.Text == "0") ? "1" : (Convert.ToInt16(ContractYears.Text) * 12).ToString();

                INString = "Insert into Cust_Contract_Pay (Contract_ID,Cust_No,C_Pay_Numbers,Payment_Type,Out_Bank,Out_Acct_No,C_Pay_Date,C_Pay_Amt,Payment_memo," +
                    "       C_Pay_Tax,C_Tax_Flag) " +
                           " values ('" + Contract_ID.Text + "','" + Cust_No.Text + "'," + tmpPayNumber + "," + RadioButtonList3.SelectedIndex.ToString() + ",'" +
                           SF.NSubString(Bank_No.Text.Trim(), 0, 10) + "','" + SF.NSubString(Account_No.Text.Trim(), 0, 20) + "',getdate()," + tmpTotalFee + ",''," +
                           tmpTotalTax + "," + ((TaxCheck.Checked) ? "1" : "0") + ")";

                INCommand.Connection.Close();
                INCommand = new SqlCommand(INString, INConnection1);
                INCommand.Connection.Open();
                INCommand.ExecuteNonQuery();
                INCommand.Connection.Close();

                //讀取收款單號
                INString = "Select ID from Cust_Contract_Pay where Contract_ID = " + Contract_ID.Text.Trim();
                INCommand = new SqlCommand(INString, INConnection1);
                INCommand.Connection.Close();
                INCommand.Connection.Open();
                SQLReader = INCommand.ExecuteReader();
                //如果有找到資料
                if (SQLReader.Read() == true)
                    tmpPayID = Convert.ToInt32(SQLReader[0]);
                INCommand.Connection.Close();

                //寫入交易收款資料表

                foreach (Control item in Repeater4.Items)   //收款方式記錄
                {
                    Label tmpID = (Label)item.FindControl("ID");
                    TextBox tmpPayDate = (TextBox)item.FindControl("PayDate");
                    DropDownList tmpPayType = (DropDownList)item.FindControl("PayType");
                    TextBox tmpPayText = (TextBox)item.FindControl("PayText");
                    TextBox tmpPayAmt = (TextBox)item.FindControl("PayAmt");
                    CheckBox tmpPayDone = (CheckBox)item.FindControl("PayDone");
                    TextBox tmpPayTax = (TextBox)item.FindControl("PayTax");

                    string sPayAmt = (tmpPayAmt.Text.Contains(",") ? tmpPayAmt.Text.Remove(tmpPayAmt.Text.IndexOf(','), 1) : tmpPayAmt.Text).Trim();
                    string sPayTax = (tmpPayTax.Text.Contains(",") ? tmpPayTax.Text.Remove(tmpPayTax.Text.IndexOf(','), 1) : tmpPayTax.Text).Trim();

                    INString = "Insert into Cust_Contract_Pay_Detail (Pay_ID,Pay_Number,Payment_Type,Out_Acct_No,B_Pay_Date,Amount,Amt,Sales_Memo,FulFill," +
                               "R_Pay_Date,Tax) " +
                               " values (" + tmpPayID.ToString() + "," + tmpID.Text.Trim() + "," + tmpPayType.SelectedIndex.ToString() + ",'" + tmpPayText.Text.Trim()
                               + "','" + tmpPayDate.Text.Trim() + "',1," + sPayAmt + ",'" +
                               ChargeStaff.Text.Trim() + "'," + ((tmpPayDone.Checked == true) ? "1" : "0") + "," +
                               ((tmpPayDone.Checked == true) ? "'" + DateTime.Now.ToShortDateString() + "'" : "null") + "," + sPayTax + ")";

                    INCommand = new SqlCommand(INString, INConnection1);
                    INCommand.Connection.Open();
                    INCommand.ExecuteNonQuery();
                    INCommand.Connection.Close();
                }

                //讀取部門代號
                INString = "Select top 1 Branch_ID from  Cust_Contract_Data where Cust_No = '" + CustNo.Text.Trim() + "' order by ID DESC";
                INCommand = new SqlCommand(INString, INConnection1);
                INCommand.Connection.Close();
                INCommand.Connection.Open();
                SQLReader = INCommand.ExecuteReader();
                //如果有找到資料
                if (SQLReader.Read() == true)
                {
                    //INString = "Select ID from  Branch_Data where Branch_Name = '" + SQLReader[0].ToString().Trim() + "'";
                    //INCommand = new SqlCommand(INString, INConnection1);
                    //INCommand.Connection.Close();
                    //INCommand.Connection.Open();
                    //SQLReader = INCommand.ExecuteReader();
                    ////如果有找到資料
                    //if (SQLReader.Read() == true)
                    BID = Convert.ToInt32(SQLReader[0]) - 1;
                }
                INCommand.Connection.Close();

                //讀取記錄

                //MyCommand = new SqlDataAdapter("SELECT top 1 * FROM Cust_Contract_Pay where Contract_ID = '" + Contract_ID.Text.Trim() +
                //                                                "' order by Contract_ID DESC", INConnection1);
                //MyCommand.Fill(MyDataSet, "Contract_Pay");
                //if (MyDataSet.Tables["Contract_Pay"].DefaultView.Count > 0)  //表示有記錄
                //{
                //    MyCommand = new SqlDataAdapter("SELECT * FROM Cust_Contract_Pay_Detail where Pay_ID = " +
                //                                                                  MyDataSet.Tables["Contract_Pay"].Rows[0][0].ToString(), INConnection1);
                //    MyCommand.Fill(MyDataSet, "Contract_Pay_Detail");

                //    DataGrid1.DataSource = MyDataSet.Tables["Contract_Pay"].DefaultView;
                //    DataGrid1.DataBind();

                //    GridView1.DataSource = MyDataSet.Tables["Contract_Pay_Detail"].DefaultView;
                //    GridView1.DataBind();
                //}

                //找出最後一筆訂單記錄編號
                string tmpONo = DateTime.Now.ToString("yyyyMMdd");
                INString = "Select top 1 TC002 from COPTC where TC001 = '2201' and TC002 like '" + tmpONo + "%' Order by TC002 DESC ";
                INCommand = new SqlCommand(INString, INConnection2);
                INCommand.Connection.Close();
                INCommand.Connection.Open();
                SQLReader = INCommand.ExecuteReader();
                //如果有找到資料
                if (SQLReader.Read() == true)
                    tmpONo = SF.NSubString(SQLReader[0].ToString(), 0, 8) + (Convert.ToInt32(SF.NSubString(SQLReader[0].ToString(), 8, 3)) + 1).ToString("000");
                else
                    tmpONo = tmpONo + "001";
                INCommand.Connection.Close();
                string DN = (SF.NSubString(Cust_No.Text, 0, 2) == "PT") ? "KN" : SF.NSubString(Cust_No.Text, 0, 2);


                //訂單單頭
                INString = "Insert into COPTC (" +
                           "COMPANY,CREATOR,CREATE_DATE,FLAG,CREATE_TIME,TC001,TC002,TC003,TC004,TC005,TC006,TC007,TC008,TC009,TC012,TC015,TC016,TC026,TC027,TC028" +
                           ",TC029,TC030,TC031,TC039,TC041,TC043,TC044,TC045,TC046,TC048,TC050,TC052,TC053,TC056,TC060,TC068,TC069,TC070,TC077,TC078,TC091)" +
                           " Values ('MH','DS','" + DateTime.Now.ToString("yyyyMMdd") + "',1,'" + DateTime.Now.ToString("HH:mm:ss") + "','2201','" + tmpONo + "','" +
                           SF.NSubString(tmpONo, 0, 8) + "','" + Cust_No.Text.Trim() + "','" + FileArray2[BID] + "','" + ChargeStaff.Text.Trim() + "','0000','NT$','1','" +
                           Contract_ID.Text.Trim() + "','','" + (TaxCheck.Checked ? "2" : "9") + "',0,'Y',0," + tmpTotalFee + "," + tmpTotalTax + "," + tmpPayNumber + ",'" +
                           SF.NSubString(tmpONo, 0, 8) + "'," + (TaxCheck.Checked ? "0.05" : "0.00") + ",0,0,1,0,'N','N',0,'" + Cust_No.Text.Trim() +
                           "','1','N','1','0000','Y','N','" + (TaxCheck.Checked ? "S21" : "S17") + "','N')";

                INCommand = new SqlCommand(INString, INConnection2);
                INCommand.Connection.Open();
                INCommand.ExecuteNonQuery();
                INCommand.Connection.Close();

                foreach (Control item in Repeater4.Items)   //收款方式記錄
                {
                    Label tmpID = (Label)item.FindControl("ID");
                    TextBox tmpPayDate = (TextBox)item.FindControl("PayDate");
                    DropDownList tmpPayType = (DropDownList)item.FindControl("PayType");
                    TextBox tmpPayText = (TextBox)item.FindControl("PayText");
                    TextBox tmpPayAmt = (TextBox)item.FindControl("PayAmt");
                    CheckBox tmpPayDone = (CheckBox)item.FindControl("PayDone");
                    TextBox tmpPayTax = (TextBox)item.FindControl("PayTax");
                    Label tmpRPayDate = (Label)item.FindControl("R_PayDate");

                    string sPayAmt = (tmpPayAmt.Text.Contains(",") ? tmpPayAmt.Text.Remove(tmpPayAmt.Text.IndexOf(','), 1) : tmpPayAmt.Text).Trim();
                    string sPayTax = (tmpPayTax.Text.Contains(",") ? tmpPayTax.Text.Remove(tmpPayTax.Text.IndexOf(','), 1) : tmpPayTax.Text).Trim();

                    //訂單-訂金分批子單頭
                    string tmpNumber = "000" + tmpID.Text.Trim();
                    tmpNumber = SF.NSubString(tmpNumber, tmpNumber.Length - 4, 4);
                    INString = "Insert into COPUC (COMPANY,CREATOR,CREATE_DATE,FLAG,CREATE_TIME,UC001,UC002,UC003,UC004,UC005,UC006,UC007)" +
                               " Values ('MH','DS','" + DateTime.Now.ToString("yyyyMMdd") + "',1,'" + DateTime.Now.ToString("HH:mm:ss") + "','2201','" + tmpONo +
                               "','" + tmpNumber + "',0," + sPayAmt + ",'" + Convert.ToDateTime(tmpPayDate.Text).ToString("yyyyMMdd") + "','N')";
                    INCommand = new SqlCommand(INString, INConnection2);
                    INCommand.Connection.Open();
                    INCommand.ExecuteNonQuery();
                    INCommand.Connection.Close();

                    //訂單單身
                    INString = "Insert into COPTD (COMPANY,CREATOR,CREATE_DATE,FLAG,CREATE_TIME,TD001,TD002,TD003,TD004,TD007,TD008,TD009,TD010,TD011,TD012,TD013" +
                               ",TD016," + "TD020,TD021,TD024,TD025,TD026,TD030,TD031,TD032,TD033,TD034,TD035,TD045,TD047,TD048,TD049,TD050,TD051,TD052,TD053,TD069" +
                               ",TD070,TD076,TD077,TD078,TD079,TD080) Values ('MH','DS','" + DateTime.Now.ToString("yyyyMMdd") + "',1,'" +
                               DateTime.Now.ToString("HH:mm:ss") + "','2201','" + tmpONo + "','" + tmpNumber + "','AA0101001','" +
                               DN + "'," + "1" + ",0,'件'," + sPayAmt + "," + Convert.ToInt32("1") * Convert.ToInt32(sPayAmt) +
                               ",'" + Convert.ToDateTime(tmpPayDate.Text).ToString("yyyyMMdd") + "','N','','Y',0,0,1,0,0,0,0,0,0,'9','" +
                               Convert.ToDateTime(tmpPayDate.Text).ToString("yyyyMMdd") + "','" + Convert.ToDateTime(tmpPayDate.Text).ToString("yyyyMMdd") +
                               "','1',0,0,0,0,'N'," + (TaxCheck.Checked ? "0.05" : "0.00") + "," + "1,'件',0,'" + (TaxCheck.Checked ? "S21" : "S17") + "',0)";
                    INCommand = new SqlCommand(INString, INConnection2);
                    INCommand.Connection.Open();
                    INCommand.ExecuteNonQuery();
                    INCommand.Connection.Close();
                }

                //回寫訂單編號到LOCAL資料庫
                INString = "Update Cust_Contract_Pay set Export_ID ='" + tmpONo + "' where ID = " + tmpPayID;
                INCommand = new SqlCommand(INString, INConnection1);
                INCommand.Connection.Open();
                INCommand.ExecuteNonQuery();
                INCommand.Connection.Close();


                //找出最後一筆結帳單記錄編號
                tmpCNo = DateTime.Now.ToString("yyyyMMdd");
                INString = "Select top 1 TA002 from ACRTA where TA001 = '6401' and TA002 like '" + tmpCNo + "%' Order by TA002 DESC ";
                INCommand = new SqlCommand(INString, INConnection2);
                INCommand.Connection.Close();
                INCommand.Connection.Open();
                SQLReader = INCommand.ExecuteReader();
                //如果有找到資料
                if (SQLReader.Read() == true)
                    tmpCNo = SQLReader[0].ToString();
                else
                    tmpCNo = tmpCNo + "000";

                INCommand.Connection.Close();

                //結帳單頭 以下改寫為 一張單頭一筆單身 2018/10/18
                //INString = "Insert into ACRTA (COMPANY,CREATOR,CREATE_DATE,FLAG,CREATE_TIME,TA001,TA002,TA003,TA004,TA005,TA006,TA007,TA008,TA009,TA010,TA011,TA012," +
                //           "TA013,TA014,TA017,TA018,TA019,TA020,TA021,TA022,TA025,TA026,TA027,TA028,TA029,TA030,TA031,TA032,TA034,TA037,TA038,TA040,TA041,TA042,TA044," +
                //           "TA045,TA046,TA047,TA048,TA049,TA053,TA054,TA055,TA058,TA066,TA068,TA073,TA083,TA093,TA101)" +
                //           " Values ('MH','DS','" + DateTime.Now.ToString("yyyyMMdd") + "',1,'" + DateTime.Now.ToString("HH:mm:ss") + "','6401','" + tmpCNo + "','" +
                //           tmpCNo.Substring(0, 8) + "','" + Cust_No.Text.Trim() + "','" + ChargeStaff.Text.Trim() + "','0000','','','NT$',1,'7','2','N','1',0,0,'N','" +
                //           tmpONo.Substring(0, 8) + "','" + tmpONo.Substring(0, 8) + "','','N','N','N',0," + tmpTotalFee + "," +  tmpTotalTax + ",0,'" +
                //           tmpCNo.Substring(0, 6) + "',0,0,'" + tmpCNo.Substring(0, 8) + "',0.05," + tmpTotalFee + "," + tmpTotalTax + ",'" + 
                //           tmpONo.Substring(0, 8) + "','" + tmpONo.Substring(0, 8) +  "',0,0,'N','N','N',0,0,0,'" + (TaxCheck.Checked ? "S21": "S17") + "','N','N','0','N','N')";
                //INCommand = new SqlCommand(INString, INConnection2);
                //INCommand.Connection.Open();
                //INCommand.ExecuteNonQuery();
                //INCommand.Connection.Close();

                foreach (Control item in Repeater4.Items)   //收款方式記錄
                {
                    Label tmpID = (Label)item.FindControl("ID");
                    TextBox tmpPayDate = (TextBox)item.FindControl("PayDate");
                    DropDownList tmpPayType = (DropDownList)item.FindControl("PayType");
                    TextBox tmpPayText = (TextBox)item.FindControl("PayText");
                    TextBox tmpPayAmt = (TextBox)item.FindControl("PayAmt");
                    CheckBox tmpPayDone = (CheckBox)item.FindControl("PayDone");
                    TextBox tmpPayTax = (TextBox)item.FindControl("PayTax");
                    Label tmpRPayDate = (Label)item.FindControl("R_PayDate");

                    string sPayAmt = (tmpPayAmt.Text.Contains(",") ? tmpPayAmt.Text.Remove(tmpPayAmt.Text.IndexOf(','), 1) : tmpPayAmt.Text).Trim();
                    string sPayTax = (tmpPayTax.Text.Contains(",") ? tmpPayTax.Text.Remove(tmpPayTax.Text.IndexOf(','), 1) : tmpPayTax.Text).Trim();

                    if (tmpPayDone.Checked == true & !SF.IsDate(tmpRPayDate.Text))       //如果有收款而且沒有收款日期,表示是新收款資料才處理
                    {

                        //編號 + 1
                        tmpCNo = SF.NSubString(tmpCNo, 0, 8) + (Convert.ToInt32(SF.NSubString(tmpCNo, 8, 3)) + 1).ToString("000");

                        //結帳單頭
                        INString = "Insert into ACRTA (COMPANY,CREATOR,CREATE_DATE,FLAG,CREATE_TIME,TA001,TA002,TA003,TA004,TA005,TA006,TA007,TA008,TA009,TA010,TA011,TA012," +
                                   "TA013,TA014,TA017,TA018,TA019,TA020,TA021,TA022,TA025,TA026,TA027,TA028,TA029,TA030,TA031,TA032,TA034,TA037,TA038,TA040,TA041,TA042,TA044," +
                                   "TA045,TA046,TA047,TA048,TA049,TA053,TA054,TA055,TA058,TA066,TA068,TA073,TA083,TA093,TA101)" +
                                   " Values ('MH','DS','" + DateTime.Now.ToString("yyyyMMdd") + "',1,'" + DateTime.Now.ToString("HH:mm:ss") + "','6401','" +
                                   tmpCNo + "','" + SF.NSubString(tmpCNo, 0, 8) + "','" + Cust_No.Text.Trim() + "','" + ChargeStaff.Text.Trim() + "','0000','','','NT$',1,'" +
                                   (TaxCheck.Checked ? "7" : "6") + "','" + (TaxCheck.Checked ? "2" : "9") + "','N','1',0,0,'N','" + SF.NSubString(tmpONo, 0, 8) +
                                   "','" + SF.NSubString(tmpONo, 0, 8) + "','','N','N','N',0," + sPayAmt + "," + sPayTax + ",0,'" + SF.NSubString(tmpCNo, 0, 6) +
                                   "',0,0,'" + SF.NSubString(tmpCNo, 0, 8) + "'," + (TaxCheck.Checked ? "0.05" : "0.00") + "," + sPayAmt + "," + sPayTax + ",'" +
                                   SF.NSubString(tmpONo, 0, 8) + "','" + SF.NSubString(tmpONo, 0, 8) + "',0,0,'N','N','N',0,0,0,'" + (TaxCheck.Checked ? "S21" : "S17") +
                                   "','N','N','0','N','N')";
                        INCommand = new SqlCommand(INString, INConnection2);
                        INCommand.Connection.Open();
                        INCommand.ExecuteNonQuery();
                        INCommand.Connection.Close();

                        //結帳單身   
                        string tmpNumber = "000" + tmpID.Text.Trim();
                        tmpNumber = tmpNumber.Substring(tmpNumber.Length - 4, 4);
                        //string tmpNumber = "0001";

                        INString = "Insert into ACRTB (COMPANY,CREATOR,CREATE_DATE,FLAG,CREATE_TIME,TB001,TB002,TB003,TB004,TB005,TB006,TB008,TB009,TB010,TB011,TB012,TB013," +
                                   "TB014,TB015,TB017,TB018,TB019,TB020,TB021,TB023,TB029)" +
                                   " Values ('MH','DS','" + DateTime.Now.ToString("yyyyMMdd") + "',1,'" + DateTime.Now.ToString("HH:mm:ss") + "','6401','" + tmpCNo +
                                   "','" + tmpNumber + "','M','2201','" + tmpONo + "','" + Convert.ToDateTime(tmpPayDate.Text).ToString("yyyyMMdd") + "'," +
                                   sPayAmt + ",0,'','N','2131',0,1," + sPayAmt + "," + sPayTax + "," + sPayAmt + "," +
                                  sPayTax + ",'" + FileArray2[BID] + "','" + tmpNumber + "'," + (TaxCheck.Checked ? "0.05" : "0.00") + ")";
                        INCommand = new SqlCommand(INString, INConnection2);
                        INCommand.Connection.Open();
                        INCommand.ExecuteNonQuery();
                        INCommand.Connection.Close();
                    }


                }

                //回寫最後一筆結帳單編號到LOCAL資料庫
                INString = "Update Cust_Contract_Pay set ERP_No ='" + tmpCNo + "' where ID = " + tmpPayID;
                INCommand = new SqlCommand(INString, INConnection1);
                INCommand.Connection.Open();
                INCommand.ExecuteNonQuery();
                INCommand.Connection.Close();

            }

            //收款單存入
            foreach (Control item in Repeater4.Items)   //收款方式記錄
            {
                Label tmpID = (Label)item.FindControl("ID");
                TextBox tmpPayDate = (TextBox)item.FindControl("PayDate");
                DropDownList tmpPayType = (DropDownList)item.FindControl("PayType");
                TextBox tmpPayText = (TextBox)item.FindControl("PayText");
                TextBox tmpPayAmt = (TextBox)item.FindControl("PayAmt");
                CheckBox tmpPayDone = (CheckBox)item.FindControl("PayDone");
                TextBox tmpPayTax = (TextBox)item.FindControl("PayTax");
                Label tmpRPayDate = (Label)item.FindControl("R_PayDate");

                string sPayAmt = (tmpPayAmt.Text.Contains(",") ? tmpPayAmt.Text.Remove(tmpPayAmt.Text.IndexOf(','), 1) : tmpPayAmt.Text).Trim();
                string sPayTax = (tmpPayTax.Text.Contains(",") ? tmpPayTax.Text.Remove(tmpPayTax.Text.IndexOf(','), 1) : tmpPayTax.Text).Trim();


                if (tmpPayDone.Checked == true & !SF.IsDate(tmpRPayDate.Text))       //如果有收款而且沒有收款日期,表示是新收款資料才處理
                {
                    //此段應讀取ERP內資料檔,目前先寫定
                    string tmpCode = "";
                    string tmpBCode = "";
                    switch (tmpPayType.SelectedIndex.ToString().Trim())
                    {
                        case "7":   //eDDA
                        case "3":   //ACH
                            tmpCode = "6301";
                            tmpBCode = "110311";
                            break;
                        case "4":   //滙款
                        case "5":   //轉帳
                            tmpCode = "6302";
                            tmpBCode = "110311";
                            break;
                        case "2":   //支票
                            tmpCode = "6304";
                            tmpBCode = "1151";
                            break;
                        case "1":   //現金
                            tmpCode = "6303";
                            tmpBCode = "1101";
                            break;
                    }
                    //找出最後一筆收款單記錄編號
                    //string tmpPNo = Convert.ToDateTime(tmpPayDate.Text.Trim()).ToString("yyyyMMdd");    //利用每筆應收款日期來找尋
                    string tmpPNo = DateTime.Now.ToString("yyyyMMdd");    //改用今天日期來找尋
                    INString = "Select top 1 TC002 from ACRTC where TC001 = '" + tmpCode + "' and TC002 like '" + tmpPNo + "%' Order by TC002 DESC ";
                    INCommand = new SqlCommand(INString, INConnection2);
                    INCommand.Connection.Close();
                    INCommand.Connection.Open();
                    SQLReader = INCommand.ExecuteReader();
                    //如果有找到資料
                    if (SQLReader.Read() == true)
                        tmpPNo = SF.NSubString(SQLReader[0].ToString(), 0, 8) + (Convert.ToInt32(SF.NSubString(SQLReader[0].ToString(), 8, 3)) + 1).ToString("000");
                    else
                        tmpPNo = tmpPNo + "001";

                    INCommand.Connection.Close();

                    //收款單頭
                    INString = "Insert into ACRTC (COMPANY,CREATOR,CREATE_DATE,FLAG,CREATE_TIME,TC001,TC002,TC003,TC004,TC005,TC006,TC007,TC008,TC009,TC010,TC011,TC012," +
                               "TC013,TC014,TC015,TC016,TC017,TC019,TC020,TC028,TC033)" +
                               " Values ('MH','DS','" + DateTime.Now.ToString("yyyyMMdd") + "',1,'" + DateTime.Now.ToString("HH:mm:ss") + "','" + tmpCode + "','" +
                               tmpPNo + "','" + SF.NSubString(tmpPNo, 0, 8) + "','" + Cust_No.Text.Trim() + "','NT$','0','" + tmpPayDate.Text + "','N','N','0000'," +
                               sPayAmt + "," + sPayAmt + "," + sPayAmt + "," + sPayAmt + ",'" + ChargeStaff.Text.Trim() + "','N','" + SF.NSubString(tmpPNo, 0, 8) + "','N',0,'N','0')";
                    INCommand = new SqlCommand(INString, INConnection2);
                    INCommand.Connection.Open();
                    INCommand.ExecuteNonQuery();
                    INCommand.Connection.Close();



                    //收款單身
                    //借方
                    INString = "Insert into ACRTD (COMPANY,CREATOR,CREATE_DATE,FLAG,CREATE_TIME,TD001,TD002,TD003,TD004,TD005,TD006,TD007,TD008,TD009,TD010," +
                               "TD011,TD012,TD013,TD014,TD015,TD017,TD020,TD021,TD022)" +
                               " Values ('MH','DS','" + DateTime.Now.ToString("yyyyMMdd") + "',1,'" + DateTime.Now.ToString("HH:mm:ss") + "','" + tmpCode + "','" +
                               tmpPNo + "','0001',1,'" + ((tmpCode == "6304") ? "2" : "1") + "','','','" + tmpBCode + "','','NT$',1," + sPayAmt + "," +
                               sPayAmt + "," + sPayAmt + "," + sPayAmt + ",'','N','" + FileArray2[BID] + "','')";
                    INCommand = new SqlCommand(INString, INConnection2);
                    INCommand.Connection.Open();
                    INCommand.ExecuteNonQuery();
                    INCommand.Connection.Close();
                    //貸方
                    INString = "Insert into ACRTD (COMPANY,CREATOR,CREATE_DATE,FLAG,CREATE_TIME,TD001,TD002,TD003,TD004,TD005,TD006,TD007,TD008,TD009,TD010," +
                               "TD011,TD012,TD013,TD014,TD015,TD017,TD020,TD021,TD022)" +
                              " Values ('MH','DS','" + DateTime.Now.ToString("yyyyMMdd") + "',1,'" + DateTime.Now.ToString("HH:mm:ss") + "','" + tmpCode + "','" +
                              tmpPNo + "','0002',-1,'4','6401','" + tmpCNo + "','1172','" + Convert.ToDateTime(tmpPayDate.Text).ToString("yyyyMMdd") +
                              "','NT$',1," + sPayAmt + "," + sPayAmt + "," + sPayAmt + "," + sPayAmt + ",'','N','" + FileArray2[BID] + "','')";
                    INCommand = new SqlCommand(INString, INConnection2);
                    INCommand.Connection.Open();
                    INCommand.ExecuteNonQuery();
                    INCommand.Connection.Close();
                }
            }


        }

        protected void Repeater4_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            ListItemType objItemType = (ListItemType)e.Item.ItemType;
            DropDownList dropDownList;
            Label tmpLabel;
            TextBox tmpTextBox;
            CheckBox tmpCheckBox;

            if (objItemType == ListItemType.Item | objItemType == ListItemType.AlternatingItem)
            {

                tmpLabel = (Label)e.Item.FindControl("PayDoneFlag");
                tmpCheckBox = (CheckBox)e.Item.FindControl("PayDone");

                if (tmpLabel != null)
                {
                    if (tmpLabel.Text == "True")
                        tmpCheckBox.Checked = true;
                    else
                        tmpCheckBox.Checked = false;
                }

                dropDownList = (DropDownList)e.Item.FindControl("PayType");
                if (dropDownList != null)
                {
                    //讀取收款方式資料表
                    string INString = "Select * from Public_Payment_Type";
                    INCommand = new SqlCommand(INString, INConnection1);
                    INCommand.Connection.Open();
                    SQLReader = INCommand.ExecuteReader();
                    while (SQLReader.Read())
                    {
                        dropDownList.Items.Add(new ListItem(SQLReader[1].ToString()));
                    }
                    INCommand.Connection.Close();

                    //設定儲存的收款方式
                    tmpLabel = (Label)e.Item.FindControl("PaymentType");
                    if (tmpLabel != null)
                        dropDownList.Items[Convert.ToInt16(tmpLabel.Text)].Selected = true;

                    //如果己經收款則無法更改收款方式及資料
                    tmpCheckBox = (CheckBox)e.Item.FindControl("PayDone");
                    if (tmpCheckBox != null)
                        if (tmpCheckBox.Checked == true)
                        {
                            dropDownList.Enabled = false;
                            tmpTextBox = (TextBox)e.Item.FindControl("PayDate");
                            tmpTextBox.Enabled = false;
                            tmpTextBox = (TextBox)e.Item.FindControl("PayText");
                            tmpTextBox.Enabled = false;
                            tmpTextBox = (TextBox)e.Item.FindControl("PayAmt");
                            tmpTextBox.Enabled = false;
                            tmpTextBox = (TextBox)e.Item.FindControl("PayTax");
                            tmpTextBox.Enabled = false;
                            tmpCheckBox.Enabled = false;
                        }
                }
            }
        }

        protected void RadioButtonList3_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (DBNull.Value.Equals(dt1))
            {
                return;
            }

            //防止畫面重整造成錯誤
            if ((string)Session["CT"] == "1")
            {
                Session["CT"] = "0";
                foreach (Control item in Repeater4.Items)   //收款方式記錄
                {
                    DropDownList tmpDrop = (DropDownList)item.FindControl("PayType");
                    CheckBox checkBox = (CheckBox)item.FindControl("PayDone");
                    if (checkBox.Checked == false)
                        tmpDrop.SelectedIndex = RadioButtonList3.SelectedIndex;
                }
            }
            Session["CT"] = "1";
            Page_Load(sender, e);
        }

        protected void TaxCheck_CheckedChanged(object sender, EventArgs e)
        {
            foreach (Control item in Repeater4.Items)   //檢查是否收款方式記錄
            {
                CheckBox checkBox = (CheckBox)item.FindControl("PayDone");
                if (checkBox.Checked == true)
                {
                    Response.Write("<script language=JavaScript>alert('己有收款資料!!無法修改!!');</script>");
                    Page_Load(sender, e);
                }
            }
            double TaxRate = 0.05;

            if (TaxCheck.Checked == false)
                TaxRate = 0;

            foreach (Control item in Repeater4.Items)   //檢查是否收款方式記錄
            {
                TextBox tmpPayAmt = (TextBox)item.FindControl("PayAmt");
                TextBox tmpPayTax = (TextBox)item.FindControl("PayTax");
                tmpPayTax.Text = (Convert.ToDouble(tmpPayAmt.Text) * TaxRate).ToString("#,##0");
            }
            TotalTax.Text = (Convert.ToDouble(TotalFee.Text) * TaxRate).ToString("#,##0");

        }

        protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            ListItemType objItemType = e.Item.ItemType;
            Button TempButton = default(Button);
            ShareFunction SF = new ShareFunction();
            Label TempLabel = default(Label);

            if (objItemType == ListItemType.Item | objItemType == ListItemType.AlternatingItem)
            {
                TempButton = (Button)e.Item.FindControl("DJob");
                TempLabel = (Label)e.Item.FindControl("JFlag");
                //判斷是否是假日
                if (!SF.IsDate(TempButton.Text))
                {
                    TempButton.BackColor = Color.Red;
                    TempButton.ForeColor = Color.White;
                }
                else
                {
                    if (TempLabel != null)
                        switch (TempLabel.Text.Trim())
                        {
                            case "4":    //完成服務
                                TempButton.BackColor = Color.LightBlue;
                                TempButton.ForeColor = Color.Black;
                                break;
                            case "255":  //解約
                                TempButton.BackColor = Color.DarkGray;
                                TempButton.ForeColor = Color.White;
                                break;
                        }
                }
            }
        }

        protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Button TempButton = default(Button);
            Label TempLabel2 = default(Label);
            ShareFunction SF = new ShareFunction();

            TempButton = (Button)e.Item.FindControl("DJob");
            TempLabel2 = (Label)e.Item.FindControl("ID");

            //判斷是否是假日,若是才處理
            if (!SF.IsDate(TempButton.Text))
            {
                Panel1.Visible = true;
                Panel2.Visible = false;
            }
            else
            {
                Panel1.Visible = false;
                Panel2.Visible = true;
            }
            Session["JDate"] = TempButton.Text.Trim();
            Session["WID"] = Convert.ToInt32(TempLabel2.Text);
            Session["UP"] = "1";
            //Session("WID") = tmpButton.Text
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            int tmpContractID = 0;
            string LastDate = "";
            Panel1.Visible = false;
            if ((string)Session["UP"] == "1")     //避免回上頁造成錯誤
            {
                Session["UP"] = "0";


                if (Session["WID"] != null & CheckBox1.Checked == true)     //如果有勾選確認
                {
                    CheckBox1.Checked = false;
                    string INString = "Select Contract_ID,Job_Flag from Cust_Job_Schedule where ID = " + Session["WID"].ToString();
                    INCommand = new SqlCommand(INString, INConnection1);
                    INCommand.Connection.Close();
                    INCommand.Connection.Open();
                    SQLReader = INCommand.ExecuteReader();
                    //如果有找到資料
                    if (SQLReader.Read() == true)
                    {
                        string DC = SQLReader[1].ToString().Trim();
                        //判斷是否扣次   

                        //如果沒有扣次,則需要將最後一個日期次數刪除
                        //找出整份合約編號
                        tmpContractID = Convert.ToInt32(SQLReader[0].ToString());
                        INCommand.Connection.Close();

                        //找出最後一筆記錄
                        INString = "Select Top 1 ID from Cust_Job_Schedule where Contract_ID = " + tmpContractID + " order by Job_Date Desc";
                        INCommand = new SqlCommand(INString, INConnection1);
                        INCommand.Connection.Close();
                        INCommand.Connection.Open();
                        SQLReader = INCommand.ExecuteReader();
                        //如果有找到資料而且沒有扣次
                        if (SQLReader.Read() == true & DC != "5")
                        {
                            //刪除該筆記錄
                            INString = "Delete from Cust_Job_Schedule where ID = " + SQLReader[0].ToString();
                            INCommand = new SqlCommand(INString, INConnection1);
                            INCommand.Connection.Close();
                            INCommand.Connection.Open();
                            INCommand.ExecuteNonQuery();
                            INCommand.Connection.Close();
                        }
                        //如果剩下的最後一筆是假日,則必需要刪除到非假日
                        while (true)
                        {
                            //找出最後一筆記錄
                            INString = "Select Top 1 ID,Job_Result_ID,Job_Date from Cust_Job_Schedule where Contract_ID = " + tmpContractID + " order by Job_Date Desc";
                            INCommand = new SqlCommand(INString, INConnection1);
                            INCommand.Connection.Close();
                            INCommand.Connection.Open();
                            SQLReader = INCommand.ExecuteReader();
                            //如果有找到資料
                            if (SQLReader.Read() == true)
                            {
                                if (Convert.ToInt16(SQLReader[1]) == 0)    //如果是正常班則跳離
                                    break;
                                //刪除該筆記錄
                                INString = "Delete from Cust_Job_Schedule where ID = " + SQLReader[0].ToString();
                                INCommand = new SqlCommand(INString, INConnection1);
                                INCommand.Connection.Close();
                                INCommand.Connection.Open();
                                INCommand.ExecuteNonQuery();
                                INCommand.Connection.Close();
                            }
                        }
                        LastDate = Convert.ToDateTime(SQLReader[2].ToString()).ToShortDateString();    //最後一筆日期

                        //還原為正常班
                        INString = "Update Cust_Job_Schedule set Job_Result_ID = 0,Staff_No ='',Job_Flag = 2 where ID = " + Session["WID"].ToString();
                        //舊寫法
                        //INString = "Update Cust_Job_Schedule set Job_Result = '' where ID = " + Session["WID"].ToString();
                        INCommand = new SqlCommand(INString, INConnection1);
                        INCommand.Connection.Close();
                        INCommand.Connection.Open();
                        INCommand.ExecuteNonQuery();
                        INCommand.Connection.Close();

                        //更新合約最後日期
                        INString = "Update Cust_Contract_Data set Contract_E_Date ='" + LastDate + "' where ID = " + tmpContractID;
                        INCommand = new SqlCommand(INString, INConnection1);
                        INCommand.Connection.Close();
                        INCommand.Connection.Open();
                        INCommand.ExecuteNonQuery();
                        INCommand.Connection.Close();

                    }
                    INCommand.Connection.Close();
                    //重新讀取資料
                    Button1_Click(sender, e);
                }
            }
        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] ReCust = new string[] { };
            Logs SLogs = new Logs();     //記錄更改內容

            //讀取假況資料表
            string INString = "Select * from Public_JobResult";
            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            while (SQLReader.Read())
            {
                ReCust = ReCust.Concat(new string[] { SQLReader[1].ToString().Trim() }).ToArray();
            }
            INCommand.Connection.Close();

            Panel2.Visible = false;

            if ((string)Session["UP"] == "1")     //避免回上頁造成錯誤
            {
                Session["UP"] = "0";


                if (Session["WID"] != null)
                {
                    if (RadioButtonList1.SelectedValue != "取消修改")
                    {
                        // 先將舊客戶資料填入請假 
                        INString = "Update Cust_Job_Schedule set Job_Result_ID = " + (Array.IndexOf(ReCust, RadioButtonList1.SelectedValue)).ToString() +
                               " where ID =" + Session["WID"].ToString();
                        INCommand = new SqlCommand(INString, INConnection1);
                        INCommand.Connection.Open();
                        INCommand.ExecuteNonQuery();
                        INCommand.Connection.Close();
                        //Logs
                        SLogs.ScheduleLogs("Schedule.log", Session["Username"].ToString() + "|[變更]-[" + CustNo + CustName + "]的原[" +
                              Session["JDate"].ToString() + "]的次數,改為管家請[" + RadioButtonList1.SelectedValue + "]假,並延次(加次)");

                        //TextBox4.Text = TextBox4.Text + INString;
                        // 增加次數
                        Add_Job_Schedule(Session["WID"].ToString());
                    }
                    //重新讀取資料
                    RadioButtonList1.ClearSelection();
                    Button1_Click(sender, e);
                }

            }
        }
        protected void Add_Job_Schedule(string tmpData)  // 資料庫序號
        {
            string[] ReCust = new string[] { };

            //讀取假日資料庫
            MyCommand = new SqlDataAdapter("SELECT *  FROM Holiday_Date", INConnection1);
            MyCommand.Fill(MyDataSet, "Holiday_Date");

            //讀取假況資料表
            string INString = "Select * from Public_JobResult";
            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            while (SQLReader.Read())
            {
                ReCust = ReCust.Concat(new string[] { SQLReader[1].ToString().Trim() }).ToArray();
            }
            INCommand.Connection.Close();
            // 增加一筆日期在最後
            // 依服務次數回推找出日期
            int tmpID = 0;
            int tmpFlag = 0;
            double tmpTimes = 1.0;
            INString = "Select Contract_Times,Contract_ID,Job_Flag from Cust_Job_Schedule_View where ID = " + tmpData;

            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            if (SQLReader.Read() == true)
            {
                tmpTimes = Convert.ToDouble(SQLReader[0]);
                tmpID = Convert.ToInt32(SQLReader[1]);
                tmpFlag = Convert.ToInt32(SQLReader[2]);    //原次數 Job_Flag
            }
            INCommand.Connection.Close();
            // TextBox4.Text = TextBox4.Text & INString

            // 找出倒數第二筆日期
            DateTime tmpDate = DateTime.Now;
            int tmpPrice = 0;
            int tmpHKSerial = 0;
            int tmpJobTime = 0;
            string tmpCustN0 = "";
            INString = "Select top " + (tmpTimes * 2).ToString() + " Job_Date,Job_Price,HK_Serial,Job_Time,Cust_No from Cust_Job_Schedule where Contract_ID = " +
                       tmpID + " order by Job_Date Desc";

            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            while (SQLReader.Read() == true)
            {
                tmpDate = (DateTime)SQLReader[0];
                tmpPrice = Convert.ToInt32(SQLReader[1]);
                tmpHKSerial = Convert.ToInt32(SQLReader[2]);
                tmpJobTime = Convert.ToInt16(SQLReader[3]);
                tmpCustN0 = SQLReader[4].ToString().Trim();
            }

            INCommand.Connection.Close();
            // TextBox4.Text = TextBox4.Text & INString
            tmpDate = tmpDate.AddDays(14);

            while (true)
            {
                MyDataSet.Tables["Holiday_Date"].DefaultView.RowFilter = "H_Date = '" + tmpDate.ToShortDateString() + "'";
                if (MyDataSet.Tables["Holiday_Date"].DefaultView.Count == 1)
                {
                    INString = "Insert into  Cust_Job_Schedule (Job_Date,Job_time,Staff_No,Cust_No,Job_Result_ID,Job_Price,Job_Flag,Job_Status,HK_Serial," +
                               "Contract_ID) values('" + tmpDate.ToShortDateString() + "'," + tmpJobTime.ToString() + ",'','" +
                               tmpCustN0 + "'," + (Array.IndexOf(ReCust, "國") + 1).ToString() + ",0,0,null,null," + tmpID + ")";
                    INCommand = new SqlCommand(INString, INConnection1);
                    INCommand.Connection.Open();
                    INCommand.ExecuteNonQuery();
                    INCommand.Connection.Close();

                    tmpDate = tmpDate.AddDays(7);
                }
                else
                    break;
            }
            // 加入一筆日期
            INString = "Insert into  Cust_Job_Schedule (Job_Date,Job_time,Staff_No,Cust_No,Job_Result_ID,Job_Price,Job_Flag,Job_Status,HK_Serial,Contract_ID)" +
                       " values('" + tmpDate.ToShortDateString() + "'," + tmpJobTime.ToString() + ",'','" + tmpCustN0 +
                       "',0," + tmpPrice.ToString() + "," + tmpFlag + ",null," + tmpHKSerial.ToString() + "," + tmpID + ")";
            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            INCommand.ExecuteNonQuery();
            INCommand.Connection.Close();
            // TextBox4.Text = TextBox4.Text & INString

            // 更新合約結束日期
            INString = "Update Cust_Contract_Data set Contract_E_Date = '" + tmpDate.ToShortDateString() + "' where ID = " + tmpID;
            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            INCommand.ExecuteNonQuery();
            INCommand.Connection.Close();
        }
    }
}