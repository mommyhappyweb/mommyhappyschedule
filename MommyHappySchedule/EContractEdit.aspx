﻿<%@ Page Language="C#" MasterPageFile="~/Template/Layout.master" AutoEventWireup="true" CodeBehind="EContractEdit.aspx.cs" Inherits="MommyHappySchedule.EContractEdit" %>
<asp:Content ID="Style" ContentPlaceHolderID="Style" runat="server">
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="Content" runat="server">
    <div class="loading" id="Loading">
        <div class="loading-block">
            <div class="loading-dot"></div>
            <div class="loading-dot"></div>
            <div class="loading-dot"></div>
            <div class="loading-dot"></div>
            <div class="loading-dot"></div>
        </div>
    </div>
    <div class="container-fluid position-relative">
        <nav class="mt-3 mb-3" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/Index.aspx">首頁</a></li>
                <li class="breadcrumb-item"><a href="/EContractList.aspx">合約列表</a></li>
                <li class="breadcrumb-item active">合約編輯</li>
            </ol>
        </nav>
        <div class="features">
            <input class="btn btn-info" type="button" id="UpdateBtn" value="儲存本筆合約" />
            <input class="btn btn-outline-danger" type="button" id="DeleteBtn" value="刪除本筆合約" />
        </div>
        <fieldset id="ContractDataFieldset">
            <legend>合約資料</legend>
            <% if (ContractData != null && ContractData.Count > 0) { %>
                <% foreach(Dictionary<string, object> Item in ContractData) { %>
                <input type="hidden" name="Contract_ID" value="<%=Item["ID"] %>" />
                <div class="form-group row">
                    <div class="col-sm-2">
                        <label for="Contract_Nums" class="col-form-label">合約編號</label>
                        <input class="form-control" name="Contract_Nums" placeholder="合約編號" type="text" value="<%=Item["Contract_Nums"] %>" />
                    </div>
                    <div class="col-sm-2">
                        <label for="Branch_ID" class="col-form-label">駐點</label>
                        <select class="form-control" name="Branch_ID">
                            <% if(BranchData.Count > 0) { %>
                                <% foreach(Dictionary<string, object> Items in BranchData) { %>
                                    <% 
                                        string Selected = (Item["Branch_ID"].Equals(Items["ID"])) ? " selected=\"selected\"" : "";
                                    %>
                               <option value="<%=Items["ID"] %>"<%=Selected %>><%=Items["Branch_Name"] %></option>
                                <% } %>
                            <% } %>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <label for="Charge_Staff_No" class="col-form-label">簽約員工編號</label>
                        <input class="form-control" name="Charge_Staff_No" placeholder="簽約員工編號" type="text" value="<%=Item["Charge_Staff_No"] %>" />

                    </div>
                    <div class="col-sm-2">
                        <label for="Charge_Staff_Name" class="col-form-label">簽約員工名稱</label>
                        <input class="form-control" name="Charge_Staff_Name" placeholder="簽約員工名稱" type="text" value="<%=Item["Charge_Staff_Name"] %>" />
                    </div>
                    <div class="col-sm-2">
                        <label for="Cust_No" class="col-form-label">客戶編號</label>
                        <input class="form-control" name="Cust_No" readonly="readonly" placeholder="客戶編號" type="text" value="<%=Item["Cust_No"] %>" />
                    </div>
                    <div class="col-sm-2">
                        <label for="Contract_S_Date" class="col-form-label">合約起始時間</label>
                        <input class="form-control" name="Contract_S_Date" type="date" value="<%=Item["Contract_S_Date"] %>" />
                    </div>
                    <div class="col-sm-2">
                        <label for="Contract_E_Date" class="col-form-label">合約結束時間</label>
                        <input class="form-control" name="Contract_E_Date" type="date" value="<%=Item["Contract_E_Date"] %>" />
                    </div>
                    <div class="col-sm-2">
                        <label for="Contract_R_Date" class="col-form-label">實際結束時間</label>
                        <input class="form-control" name="Contract_R_Date" type="date" value="<%=Item["Contract_R_Date"] %>" />
                    </div>
                    <div class="col-sm-2">
                        <label for="Contract_Times" class="col-form-label">服務週次</label>
                        <input class="form-control" name="Contract_Times" placeholder="服務週次" type="text" value="<%=Item["Contract_Times"] %>" />
                    </div>
                    <div class="col-sm-2">
                        <label for="Contract_Status" class="col-form-label">合約狀態</label>
                        <select class="form-control" name="Contract_Status">
                            <% if(ContractStatusData.Count > 0) { %>
                                <% foreach(Dictionary<string, object> Items in ContractStatusData) { %>
                                    <% 
                                        string Selected = (Item["Contract_Status"].Equals(Items["ID"])) ? " selected=\"selected\"" : "";
                                    %>
                               <option value="<%=Items["ID"] %>"<%=Selected %>><%=Items["Name"] %></option>
                                <% } %>
                            <% } %>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <label for="Contract_U_Date" class="col-form-label">合約更新日期</label>
                        <input class="form-control" name="Contract_U_Date" type="date" value="<%=Item["Contract_U_Date"] %>" />
                    </div>
                    <div class="col-sm-2">
                        <label for="Service_Cycle" class="col-form-label">服務週期</label>
                        <input class="form-control" name="Service_Cycle" placeholder="服務週期" type="text" value="<%=Item["Service_Cycle"] %>" />
                    </div>
                    <div class="col-sm-2">
                        <label for="Contract_Years" class="col-form-label">合約年限</label>
                        <input class="form-control" name="Contract_Years" type="number" value="<%=Item["Contract_Years"] %>" />
                    </div>
                    <div class="col-sm-2">
                        <label for="HK_Nums" class="col-form-label">管家人數</label>
                        <input class="form-control" name="HK_Nums" type="number" value="<%=Item["HK_Nums"] %>" />
                    </div>
                    <div class="col-sm-12">
                        <label for="Note" class="col-form-label">備註</label>
                        <textarea class="form-control" name="Note" placeholder="備註" ><%=Item["Note"] %></textarea>
                    </div>
                </div>
                <% } %>
            <% } %>
        </fieldset>
        <fieldset id="JobScheduleFieldset">
            <legend>派班行程表</legend>
            <div class="alert"></div>
            <% if (JobScheduleData != null && JobScheduleData.Count != 0) { %>
            <table class="table table-light schedule-table">
                <thead class="text-center">
                    <tr>
                        <th class="table-btn">功能</th>
                        <th class="t-w-180">派班日期</th>
                        <th>時段</th>
                        <th>員工編號</th>
                        <th>派班項目</th>
                        <th>本次金額</th>
                        <th>異動狀態</th>
                        <th>備註</th>
                    </tr>
                    <tr class="thead-insert">
                        <td class="table-btn">
                            <input type="hidden" name="Cust_No" value="<%=JobScheduleData[0]["Cust_No"] %>" />
                            <input type="hidden" name="Contract_ID" value="<%=JobScheduleData[0]["Contract_ID"] %>" />
                            <input class="btn btn-sm btn-success" type="button" id="InsertJobScheduleBtn" value="新增" />
                        </td>
                        <td class="t-w-180"><input class="form-control" type="date" name="Job_Date" /></td>
                        <td>
                            <select class="form-control" name="Job_Time">
                            <% if (JobTimeData.Count != 0) { %>
                                <% foreach (KeyValuePair<string, string> Items in JobTimeData) { %>
                               <option value="<%=Items.Key %>"><%=Items.Value %></option>
                                <% } %>
                            <% } %>
                            </select>
                        </td>
                        <td><input class="form-control" name="Staff_No" type="text" placeholder="員工編號" /></td>
                        <td>
                            <select class="form-control" name="Job_Result">
                            <% if (JobResultData.Count != 0) { %>
                                <% foreach (Dictionary<string, object> Items in JobResultData) { %>
                                <option value="<%=Items["ID"] %>"><%=Items["Name"] %></option>
                                <% } %>
                            <% } %>
                            </select>
                        </td>
                        <td><input class="form-control" type="number" name="Job_Price" min="0" value="0" /></td>
                        <td>
                            <select class="form-control" name="Job_Flag">
                            <% if (JobFlagData.Count != 0) { %>
                                <% foreach (KeyValuePair<string, string> Items in JobFlagData) { %>
                               <option value="<%=Items.Key %>"><%=Items.Value%></option>
                                <% } %>
                            <% } %>
                            </select>
                        </td>
                        <td><input class="form-control" type="text" name="Note" placeholder="單次備註" /></td>
                    </tr>
                </thead>
                <tbody class="text-center">
                    <% foreach (Dictionary<string, object> Item in JobScheduleData) {%>
                    <tr>
                        <td class="table-btn">
                            <input type="hidden" name="Schedule_ID" value="<%=Item["ID"] %>" />
                            <input type="hidden" name="Cust_No" value="<%=Item["Cust_No"] %>" />
                            <input type="hidden" name="Contract_ID" value="<%=Item["Contract_ID"] %>" />
                            <input class="btn btn-sm btn-outline-danger" name="Delete" type="button" value="刪除" />
                            <input class="btn btn-sm btn-outline-info" name="Update" type="button" value="儲存" />
                        </td>
                        <td class="t-w-180"><input class="form-control" type="date" name="Job_Date" value="<%=Item["Job_Date"] %>" /></td>
                        <td>
                            <select class="form-control" name="Job_Time">
                            <% if (JobTimeData.Count != 0) { %>
                                <% foreach (KeyValuePair<string, string> Items in JobTimeData) { %>
                                    <% 
                                        string Selected = (Item["Job_Time"].ToString() == Items.Key) ? " selected=\"selected\"" : "";
                                    %>
                               <option value="<%=Items.Key %>"<%=Selected %>><%=Items.Value %></option>
                                <% } %>
                            <% } %>
                            </select>
                        </td>
                        <td><input class="form-control" type="text" name="Staff_No" placeholder="未派管家" value="<%=Item["Staff_No"] %>" /></td>
                        <td>
                            <select class="form-control" name="Job_Result">
                            <% if (JobResultData.Count != 0) { %>
                                <% foreach (Dictionary<string, object> Items in JobResultData) { %>
                                <% string Selected = (Item["Job_Result_ID"].Equals(Items["ID"])) ? " selected=\"selected\"" : ""; %>
                                <option value="<%=Items["ID"] %>"<%=Selected %>><%=Items["Name"] %></option>
                                <% } %>
                            <% } %>
                            </select>
                        </td>
                        <td><input class="form-control" type="number" name="Job_Price" min="0" value="<%=Item["Job_Price"] %>" /></td>
                        <td>
                            <select class="form-control" name="Job_Flag">
                            <% if (JobFlagData.Count != 0) { %>
                                <% foreach (KeyValuePair<string, string> Items in JobFlagData) { %>
                                    <% 
                                        string Selected = (Item["Job_Flag"].ToString() == Items.Key) ? " selected=\"selected\"" : "";
                                    %>
                               <option value="<%=Items.Key %>"<%=Selected %>><%=Items.Value%></option>
                                <% } %>
                            <% } %>
                            </select>
                        </td>
                        <td><input class="form-control" type="text" name="Note" placeholder="單次備註" value="<%=Item["Note"] %>" /></td>
                    </tr>
                    <% } %>
                </tbody>
                <tfoot class="text-right">
                    <tr>
                        <% foreach (Dictionary<string, object> Items in JobSchudleResultTotal){ %>
                        <td><%=Items["Name"] %>筆數：<label class="badge badge-info"><%=Items["Count"] %></label>筆</td>
                        <% } %>
                        <td>總派班筆數：<label class="badge badge-info"><%=JobScheduleData.Count %></label>筆</td>
                    </tr>
                </tfoot>
            </table>
            <% } %>
        </fieldset>
        <fieldset id="ContractPayFieldset">
            <legend>付款人資訊</legend>
            <% if (ContractPayData != null && ContractPayData.Count != 0) { %>
                <% foreach (Dictionary<string, object> Item in ContractPayData) { %>
                <div class="form-group row">
                    <input name="ContractPay_ID" type="hidden" value="<%=Item["ID"] %>" />
                    <input type="hidden" name="Contract_ID" value="<%=Item["Contract_ID"] %>" />
                    <input type="hidden" name="Cust_No" value="<%=Item["Cust_No"] %>" />
                    <div class="col-sm-2">
                        <label for="C_Pay_Numbers" class="col-form-label">交易總期數</label>
                        <input class="form-control" name="C_Pay_Numbers" type="text" value="<%=Item["C_Pay_Numbers"] %>" />
                    </div>
                    <div class="col-sm-2">
                        <label for="Payment_Type" class="col-form-label">交易方式</label>
                        <select class="form-control" name="Payment_Type">
                            <% if (PaymentTypeData.Count != 0) { %>
                                <% foreach (Dictionary<string, object> Items in PaymentTypeData) { %>
                                    <% 
                                        string Selected = (Item["Payment_Type"].Equals(Items["ID"])) ? " selected=\"selected\"" : "";
                                    %>
                                <option value="<%=Items["ID"] %>"<%=Selected %>><%=Items["Name"] %></option>
                                <% } %>
                            <% } %>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <label for="Out_Bank" class="col-form-label">銀行代號</label>
                        <input class="form-control" name="Out_Bank" type="text" maxlength="5" placeholder="例：00813" value="<%=Item["Out_Bank"] %>" />
                    </div>
                    <div class="col-sm-2">
                        <label for="Out_Acct_No" class="col-form-label">扣款帳號</label>
                        <input class="form-control" name="Out_Acct_No" type="text" maxlength="16" placeholder="例；0000123456965456" value="<%=Item["Out_Acct_No"] %>" />
                    </div>
                    <div class="col-sm-2">
                        <label for="C_Pay_Date" class="col-form-label">付款交易日期</label>
                        <input class="form-control" name="C_Pay_Date" type="text" value="<%=Item["C_Pay_Date"] %>" />
                    </div>
                    <div class="col-sm-2">
                        <label for="C_Pay_Amt" class="col-form-label">付款總金額</label>
                        <input class="form-control" name="C_Pay_Amt" type="number" value="<%=Item["C_Pay_Amt"] %>" />
                    </div>
                    <div class="col-sm-2">
                        <label for="C_Pay_Tax" class="col-form-label">外加稅總金額</label>
                        <input class="form-control" name="C_Pay_Tax" type="number" value="<%=Item["C_Pay_Tax"] %>" />
                    </div>
                    <div class="col-sm-2">
                        <label for="C_Tax_Flag" class="col-form-label">是否外加稅</label>
                        <select class="form-control" name="C_Tax_Flag">
                            <% if (YesNoData.Count != 0) { %>
                                <% foreach (KeyValuePair<string, string> Items in YesNoData) { %>
                                    <% 
                                        string Selected = (Item["C_Tax_Flag"].ToString() == Items.Key) ? " selected=\"selected\"" : "";
                                    %>
                               <option value="<%=Items.Key %>"<%=Selected %>><%=Items.Value %></option>
                                <% } %>
                            <% } %>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <label for="Contract_Type" class="col-form-label">合約狀態</label>
                        <select class="form-control" name="Contract_Type">
                            <% if (ContractTypeData.Count != 0) { %>
                                <% foreach (Dictionary<string, object> Items in ContractTypeData) { %>
                                    <% 
                                        string Selected = (Item["Contract_Type"].Equals(Items["ID"])) ? " selected=\"selected\"" : "";
                                    %>
                                <option value="<%=Items["ID"] %>"<%=Selected %>><%=Items["Name"] %></option>
                                <% } %>
                            <% } %>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <label for="Payment_Memo" class="col-form-label">備註</label>
                        <input class="form-control" name="Payment_Memo" type="text" placeholder="備註" value="<%=Item["Payment_Memo"] %>" />
                    </div>
                </div>
                <% } %>
            <% } %>
        </fieldset>
        <fieldset id="ContractPayDetailFieldset">
            <legend>付款明細</legend>
            <div class="alert"></div>
            <% if (ContractPayDetailData != null && ContractPayDetailData.Count != 0) { %>
            <table class="table table-light schedule-table">
                <thead class="text-center">
                    <tr>
                        <th class="table-btn">功能</th>
                        <th class="t-w-100">付款期數</th>
                        <th>付款方式</th>
                        <th>銀行代號</th>
                        <th class="t-w-180">扣款帳號</th>
                        <th class="t-w-100">數量</th>
                        <th>金額</th>
                        <th class="t-w-180">預計交易日期</th>
                        <th>是否付款</th>
                        <th class="t-w-180">付款日期</th>
                        <th>外加稅額</th>
                        <th>備註</th>
                    </tr>
                    <tr class="thead-insert">
                        <td class="table-btn">
                            <input type="hidden" name="Pay_ID" value="<%=ContractPayDetailData[0]["Pay_ID"] %>" />
                            <input class="btn btn-sm btn-success" type="button" id="InsertContractPayDetailBtn" value="新增" />
                        </td>
                        <td class="t-w-100"><input class="form-control" type="number" min="0" name="Pay_Number" value="<%=ContractPayDetailData.Count + 1 %>" /></td>
                        <td>
                            <select class="form-control" name="Payment_Type">
                            <% if (PaymentTypeData.Count != 0) { %>
                                <% foreach (Dictionary<string, object> Items in PaymentTypeData) { %>
                                <option value="<%=Items["ID"] %>"><%=Items["Name"] %></option>
                                <% } %>
                            <% } %>
                            </select>
                        </td>
                        <td><input class="form-control" type="text" name="Out_Bank" maxlength="5" placeholder="例：00813" /></td>
                        <td class="t-w-180"><input class="form-control" type="text" name="Out_Acct_No" maxlength="16" placeholder="例；0000123456965456" /></td>
                        <td class="t-w-100"><input class="form-control" type="number" name="Amount" min="0"  value="1" /></td>
                        <td><input class="form-control" type="number" name="Amt" min="0" value="0" /></td>
                        <td class="t-w-180"><input class="form-control" type="date" name="B_Pay_Date" /></td>
                        <td>
                            <select class="form-control" name="Fulfill">
                            <% if (YesNoData.Count != 0) { %>
                                <% 
                                    foreach (KeyValuePair<string, string> Items in YesNoData) {
                                %>
                                <option value="<%=Items.Key %>"><%=Items.Value %></option>
                                <% } %>
                            <% } %>
                            </select>
                        </td>
                        <td class="t-w-180"><input class="form-control" type="date" name="R_Pay_Date" /></td>
                        <td><input class="form-control" type="number" name="Tax" min="0" value="0" /></td>
                        <td><input class="form-control" type="text" name="Sales_Memo" placeholder="備註" /></td>
                    </tr>
                </thead>
                <tbody class="text-center">
                    <% foreach(Dictionary<string, object> Item in ContractPayDetailData) { %>
                    <tr>
                        <td class="table-btn">
                            <input type="hidden" name="ContractPayDetail_ID" value="<%=Item["ID"] %>" />
                            <input type="hidden" name="Pay_ID" value="<%=Item["Pay_ID"] %>" />
                            <input class="btn btn-sm btn-outline-danger" name="Delete" type="button" value="刪除" />
                            <input class="btn btn-sm btn-outline-info" name="Update" type="button" value="儲存" />
                        </td>
                        <td class="t-w-100"><input class="form-control" type="number" min="0" name="Pay_Number" value="<%=Item["Pay_Number"] %>" /></td>
                        <td>
                            <select class="form-control" name="Payment_Type">
                            <% if (PaymentTypeData.Count != 0) { %>
                                <% foreach (Dictionary<string, object> Items in PaymentTypeData) { %>
                                    <% 
                                        string Selected = (Item["Payment_Type"].Equals(Items["ID"])) ? " selected=\"selected\"" : "";
                                    %>
                                <option value="<%=Items["ID"] %>"<%=Selected %>><%=Items["Name"] %></option>
                                <% } %>
                            <% } %>
                            </select>
                        </td>
                        <td><input class="form-control" type="text" name="Out_Bank" placeholder="例：00813" maxlength="5" value="<%=Item["Out_Bank"] %>" /></td>
                        <td class="t-w-180"><input class="form-control" type="text" name="Out_Acct_No" maxlength="16" placeholder="例；0000123456965456" value="<%=Item["Out_Acct_No"] %>" /></td>
                        <td class="t-w-100"><input class="form-control" type="number" name="Amount" min="0" value="<%=Item["Amount"] %>" /></td>
                        <td><input class="form-control" type="number" name="Amt" min="0" value="<%=Item["Amt"] %>" /></td>
                        <td class="t-w-180"><input class="form-control" type="date" name="B_Pay_Date" value="<%=Item["B_Pay_Date"] %>" /></td>
                        <td>
                            <select class="form-control" name="Fulfill">
                            <% if (PaymentTypeData.Count != 0) { %>
                                <% foreach (KeyValuePair<string, string> Items in YesNoData) { %>
                                    <% 
                                        string Selected = (Item["Fulfill"].ToString() == Items.Key) ? " selected=\"selected\"" : "";
                                    %>
                                <option value="<%=Items.Key %>"<%=Selected %>><%=Items.Value %></option>
                                <% } %>
                            <% } %>
                            </select>
                        </td>
                        <td class="t-w-180"><input class="form-control" type="date" name="R_Pay_Date" value="<%=Item["R_Pay_Date"] %>" /></td>
                        <td><input class="form-control" type="number" name="Tax" min="0" value="<%=Item["Tax"] %>" /></td>
                        <td><input class="form-control" type="text" name="Sales_Memo" placeholder="備註" value="<%=Item["Sales_Memo"] %>" /></td>
                    </tr>
                    <% } %>
                </tbody>
                <tfoot class="text-right">
                    <tr>
                        <td>總付款筆數：<label class="badge badge-info"><%=ContractPayDetailData.Count %></label>筆</td>
                    </tr>
                </tfoot>
            </table>
            <% } %>
        </fieldset>
    </div>
</asp:Content>
<asp:Content ID="Script" ContentPlaceHolderID="Script" runat="server">
    <script src="Public/Scripts/Core/MethodApp.js"></script>
    <script src="Public/Scripts/App/LoadingApp.js"></script>
    <script src="Public/Scripts/App/EContractEditApp.js"></script>
</asp:Content>
