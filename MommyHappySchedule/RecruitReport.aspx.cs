﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;
using System.Configuration;
using Models.Library;

namespace MommyHappySchedule
{
    public partial class RecruitReport : Page
    {
        // Dim String1 As String = "server=(local);database=MommyHappy;uid=aspsa;pwd=12345678;language=english"
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        SqlConnection INConnection1 = new SqlConnection(ConnString);
        // Dim INConnection2 As Data.SqlClient.SqlConnection = New Data.SqlClient.SqlConnection(String2)
        private SqlCommand INCommand;
        private SqlDataReader SQLReader;
        private DataSet MyDataSet;
        private SqlDataAdapter MyCommand;
        ShareFunction SF = new ShareFunction();
        bool IsTheMonth = true;
        protected void Page_Load(object sender, System.EventArgs e)
        {

            // If Session("account") = "" Or Session("Username") <> "mommyhappy" Then
            // Session("msg") = "您沒有使用此程式的權限!!"
            // Response.Redirect("Index.aspx")
            // End If

            if (!Page.IsPostBack)
            {
                MyDataSet = new DataSet();
                MyCommand = new SqlDataAdapter("SELECT ID, TRIM(Branch_Name) AS Branch_Name FROM Branch_Data", INConnection1);
                MyCommand.Fill(MyDataSet, "Branch");

                Branch.DataSource = MyDataSet.Tables["Branch"].DefaultView;
                Branch.DataTextField = "Branch_Name";
                Branch.DataValueField = "ID";
                Branch.DataBind();

                Check_Year.SelectedValue = DateTime.Now.Year.ToString();
                Check_Month.SelectedValue = DateTime.Now.Month.ToString();

                Panel1.Visible = false;
                Panel2.Visible = false;
                Panel3.Visible = false;
                Panel4.Visible = false;
            }
        }


        protected void Check_Report_Click(object sender, EventArgs e)
        {
            int CToday = DateTime.Today.Year;
            DataTable dt;
            DataRow dr;
            string Select_Banch;
            string Select_Banch1;
            int Start_Banch, End_Banch;

            // Label1.Text = Branch.SelectedItem.Text

            dt = new DataTable();
            dt.Columns.Add(new DataColumn("ID", typeof(string)));
            dt.Columns.Add(new DataColumn("Recruit_Number", typeof(string)));
            dt.Columns.Add(new DataColumn("Hire_Number", typeof(string)));
            dt.Columns.Add(new DataColumn("Paper_Number", typeof(string)));
            dt.Columns.Add(new DataColumn("Oritation_Number", typeof(string)));
            dt.Columns.Add(new DataColumn("Experience_Number", typeof(string)));
            dt.Columns.Add(new DataColumn("Final_Number", typeof(string)));
            dt.Columns.Add(new DataColumn("R_H_P", typeof(string)));
            dt.Columns.Add(new DataColumn("H_P_P", typeof(string)));
            dt.Columns.Add(new DataColumn("P_O_P", typeof(string)));
            dt.Columns.Add(new DataColumn("O_E_P", typeof(string)));
            dt.Columns.Add(new DataColumn("E_W_P", typeof(string)));
            dt.Columns.Add(new DataColumn("R_W_P", typeof(string)));
            dt.Columns.Add(new DataColumn("S1", typeof(int)));  // 排序用
            dt.Columns.Add(new DataColumn("S2", typeof(int)));  // 排序用

            if (Branch.SelectedIndex == 0)
            {
                Start_Banch = 1;
                End_Banch = Branch.Items.Count - 1;
            }
            else
            {
                Start_Banch = Branch.SelectedIndex;
                End_Banch = Branch.SelectedIndex;
            }

            int jj;
            int Re_Number, Hi_Number, Pa_Number, Or_Number, Ex_Number, Fi_Number, TRe_Number, THi_Number, TPa_Number, TOr_Number, TEx_Number, TFi_Number;
            TRe_Number = 0;
            THi_Number = 0;
            TPa_Number = 0;
            TOr_Number = 0;
            TEx_Number = 0;
            TFi_Number = 0;
            Re_Number = 0;
            Hi_Number = 0;
            Pa_Number = 0;
            Or_Number = 0;
            Ex_Number = 0;
            Fi_Number = 0;

            for (jj = Start_Banch; jj <= End_Banch; jj++)
            {
                if (Branch.Items[jj].Text.Trim() == "南高雄")
                    Select_Banch = " (Branch_Name ='南高雄' or Branch_Name = '屏東') ";
                else
                    Select_Banch = " Branch_Name ='" + Branch.Items[jj].Text.Trim() + "'";// 營業所名稱

                MyDataSet = new DataSet();
                MyCommand = new SqlDataAdapter("SELECT * FROM Recruit_Process_View where " + Select_Banch, INConnection1);
                MyCommand.Fill(MyDataSet, "RNumber");


                dr = dt.NewRow();
                dr[0] = Branch.Items[jj].Text;

                // 應徵人數 即為合乎該面試日期的筆數
                Re_Number = Convert.ToInt16(MyDataSet.Tables["RNumber"].Compute("Count(Name)", "(Interview_Date >= #" + Check_Year.SelectedValue + 
                            "/1/1# and Interview_Date <=#" +  Check_Year.SelectedValue + "/12/31#)"));
                // 含跨月
                // Re_Number = MyDataSet.Tables("RNumber").Compute("Count(Name)", "(Interview_Date >= #" & Check_Year.SelectedValue & "/1/1# and Interview_Date <=#" & Check_Year.SelectedValue &
                // "/12/31#) or ( ((Hout_Info_Date >= #" & Check_Year.SelectedValue & "/1/1# and Hout_Info_Date <= #" & Check_Year.SelectedValue &
                // "/12/31#) or (Train_Date >= #" & Check_Year.SelectedValue & "/1/1# and Train_Date <= #" & Check_Year.SelectedValue &
                // "/12/31#) or (Experi_Date_1 >= #" & Check_Year.SelectedValue & "/1/1# and Experi_Date_1 <= #" & Check_Year.SelectedValue &
                // "/12/31#) or (Experi_Date_2 >= #" & Check_Year.SelectedValue & "/1/1# and Experi_Date_2 <= #" & Check_Year.SelectedValue &
                // "/12/31#) or (Experi_Date_3 >= #" & Check_Year.SelectedValue & "/1/1# and Experi_Date_3 <= #" & Check_Year.SelectedValue &
                // "/12/31#) or (Experi_Date_4 >= #" & Check_Year.SelectedValue & "/1/1# and Experi_Date_4 <= #" & Check_Year.SelectedValue &
                // "/12/31#) or (Sign_Contr_Date >= #" & Check_Year.SelectedValue & "/1/1# and Sign_Contr_Date <= #" & Check_Year.SelectedValue & "/12/31#))) ")
                dr[1] = Re_Number.ToString();
                TRe_Number = TRe_Number + Re_Number;

                // 錄取人數 為合乎該面試日期的筆數 且 應徵結果為  0
                Hi_Number = Convert.ToInt16(MyDataSet.Tables["RNumber"].Compute("Count(Name)", "Interview_Result = 0 and (Interview_Date >= #" +
                            Check_Year.SelectedValue + "/1/1# and Interview_Date <= #" + Check_Year.SelectedValue + "/12/31#)"));
                dr[2] = Hi_Number.ToString();
                THi_Number = THi_Number + Hi_Number;

                // 繳資料人數  繳資料日期 為 日期區間
                Pa_Number = Convert.ToInt16(MyDataSet.Tables["RNumber"].Compute("Count(Name)", "(Hout_Info_Date >= #" + Check_Year.SelectedValue + 
                            "/1/1# and Hout_Info_Date <= #" + Check_Year.SelectedValue + "/12/31#)"));
                dr[3] = Pa_Number.ToString();
                TPa_Number = TPa_Number + Pa_Number;

                // 說明人數 新訓日期 為 日期區間
                Or_Number = Convert.ToInt16(MyDataSet.Tables["RNumber"].Compute("Count(Name)", "(Train_Date >= #" + Check_Year.SelectedValue +
                            "/1/1# and Train_Date <= #" + Check_Year.SelectedValue + "/12/31#)"));
                dr[4] = Or_Number.ToString();
                TOr_Number = TOr_Number + Or_Number;

                // 體驗人數  體驗日期 為 日期區間
                Ex_Number = Convert.ToInt16(MyDataSet.Tables["RNumber"].Compute("Count(Name)", "(Experi_Date_1 >= #" + Check_Year.SelectedValue + 
                            "/1/1# and Experi_Date_1 <= #" + Check_Year.SelectedValue + "/12/31#)"));
                // 含跨月
                // Ex_Number = MyDataSet.Tables("RNumber").Compute("Count(Name)", "(Experi_Date_1 >= #" & Check_Year.SelectedValue & "/1/1# and Experi_Date_1 <= #" & Check_Year.SelectedValue &
                // "/12/31#) or (Experi_Date_2 >= #" & Check_Year.SelectedValue & "/1/1# and Experi_Date_2 <= #" & Check_Year.SelectedValue &
                // "/12/31#) or (Experi_Date_3 >= #" & Check_Year.SelectedValue & "/1/1# and Experi_Date_3 <= #" & Check_Year.SelectedValue &
                // "/12/31#) or (Experi_Date_4 >= #" & Check_Year.SelectedValue & "/1/1# and Experi_Date_4 <= #" & Check_Year.SelectedValue & "/12/31#)")
                dr[5] = Ex_Number.ToString();
                TEx_Number = TEx_Number + Ex_Number;

                // 入職人數  簽約日期 為 日期區間
                Fi_Number = Convert.ToInt16(MyDataSet.Tables["RNumber"].Compute("Count(Name)", "(Sign_Contr_Date >= #" + Check_Year.SelectedValue +
                            "/1/1# and Sign_Contr_Date <= #" + Check_Year.SelectedValue + "/12/31#)"));
                dr[6] = Fi_Number.ToString();
                TFi_Number = TFi_Number + Fi_Number;

                // 比例
                if (Re_Number == 0)
                    dr[7] = "0.00%";
                else
                    dr[7] = (Hi_Number / (double)Re_Number).ToString("0.00%");

                if (Hi_Number == 0)
                    dr[8] = "0.00%";
                else
                    dr[8] = (Pa_Number / (double)Hi_Number).ToString("0.00%");

                if (Pa_Number == 0)
                    dr[9] = "0.00%";
                else
                    dr[9] = (Or_Number / (double)Pa_Number).ToString("0.00%");

                if (Or_Number == 0)
                    dr[10] = "0.00%";
                else
                    dr[10] = (Ex_Number / (double)Or_Number).ToString("0.00%");

                if (Ex_Number == 0)
                    dr[11] = "0.00%";
                else
                    dr[11] = (Fi_Number / (double)Ex_Number).ToString("0.00%");

                if (Re_Number == 0)
                    dr[12] = "0.00%";
                else
                    dr[12] = (Fi_Number / (double)Re_Number).ToString("0.00%");

                dt.Rows.Add(dr);
            }
            if (Branch.SelectedIndex == 0)
            {
                dr = dt.NewRow();
                dr[0] = "總公司 ";

                dr[1] = TRe_Number.ToString();
                dr[2] = THi_Number.ToString();
                dr[3] = TPa_Number.ToString();
                dr[4] = TOr_Number.ToString();
                dr[5] = TEx_Number.ToString();
                dr[6] = TFi_Number.ToString();

                if (TRe_Number == 0)
                    dr[7] = "0.00%";
                else
                    dr[7] = (THi_Number / (double)TRe_Number).ToString("0.00%");

                if (THi_Number == 0)
                    dr[8] = "0.00%";
                else
                    dr[8] = (TPa_Number / (double)THi_Number).ToString("0.00%");

                if (TPa_Number == 0)
                    dr[9] = "0.00%";
                else
                    dr[9] = (TOr_Number / (double)TPa_Number).ToString("0.00%");

                if (TOr_Number == 0)
                    dr[10] = "0.00%";
                else
                    dr[10] = (TEx_Number / (double)TOr_Number).ToString("0.00%");

                if (TEx_Number == 0)
                    dr[11] = "0.00%";
                else
                    dr[11] = (TFi_Number / (double)TEx_Number).ToString("0.00%");

                if (TRe_Number == 0)
                    dr[12] = "0.00%";
                else
                    dr[12] = (TFi_Number / (double)TRe_Number).ToString("0.00%");

                dt.Rows.InsertAt(dr, 0);
            }

            int NR;


            if (Branch.SelectedIndex == 0 & Check_Year.SelectedValue == DateTime.Now.Year.ToString())
            {
                // 選擇總公司當年度則多顯示當月資料
                dr = dt.NewRow();
                dt.Rows.Add(dr);
                NR = dt.Rows.Count;

                // 找出目前月份的最後一天日期
                int LMD = 31;
                while (!SF.IsDate(DateTime.Now.Year.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + LMD.ToString()))
                    LMD = LMD - 1;
                string SDPeriod = "#" + DateTime.Now.Year.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + "1#";
                string EDPeriod = "#" + DateTime.Now.Year.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + LMD.ToString() + "#";

                TRe_Number = 0;
                THi_Number = 0;
                TPa_Number = 0;
                TOr_Number = 0;
                TEx_Number = 0;
                TFi_Number = 0;
                Re_Number = 0;
                Hi_Number = 0;
                Pa_Number = 0;
                Or_Number = 0;
                Ex_Number = 0;
                Fi_Number = 0;

                for (jj = Start_Banch; jj <= End_Banch; jj++)
                {
                    if (Branch.Items[jj].Text.Trim() == "南高雄")
                        Select_Banch = " (Branch_Name ='南高雄' or Branch_Name = '屏東') ";
                    else
                        Select_Banch = " Branch_Name ='" + Branch.Items[jj].Text.Trim() + "'";// 營業所名稱

                    MyDataSet = new DataSet();
                    MyCommand = new SqlDataAdapter("SELECT * FROM Recruit_Process_View where " + Select_Banch, INConnection1);
                    MyCommand.Fill(MyDataSet, "RNumber");

                    // MyCommand = New SqlDataAdapter("SELECT  Staff_Job_Data.Svr_Name FROM Staff_Job_Data LEFT OUTER JOIN Staff_Job_Data AS Staff_Job_Data_1 ON Staff_Job_Data.Svr_Name = Staff_Job_Data_1.Staff_Name WHERE  (Staff_Job_Data.Branch_Name ='" & Select_Banch & "') GROUP BY Staff_Job_Data.Svr_Name, Staff_Job_Data_1.Depart_Date, Staff_Job_Data_1.Staff_Name HAVING  (Staff_Job_Data_1.Depart_Date IS NULL) OR (YEAR(Staff_Job_Data_1.Depart_Date) = 2017) OR (Staff_Job_Data_1.Staff_Name IS NULL) ORDER BY Staff_Job_Data_1.Staff_Name", INConnection1)
                    // MyCommand.Fill(MyDataSet, "Svr_Name")

                    dr = dt.NewRow();
                    dr[0] = Branch.Items[jj].Text + "-當月";

                    // 應徵人數 即為合乎該面試日期的筆數
                    Re_Number = Convert.ToInt16(MyDataSet.Tables["RNumber"].Compute("Count(Name)", "(Interview_Date >= " + SDPeriod + " and Interview_Date <= " + 
                                EDPeriod + ")"));
                    // 含跨月
                    // Re_Number = MyDataSet.Tables("RNumber").Compute("Count(Name)", "(Interview_Date >= #" & Check_Year.SelectedValue & "/1/1# and Interview_Date <=#" & Check_Year.SelectedValue &
                    // "/12/31#) or ( ((Hout_Info_Date >= #" & Check_Year.SelectedValue & "/1/1# and Hout_Info_Date <= #" & Check_Year.SelectedValue &
                    // "/12/31#) or (Train_Date >= #" & Check_Year.SelectedValue & "/1/1# and Train_Date <= #" & Check_Year.SelectedValue &
                    // "/12/31#) or (Experi_Date_1 >= #" & Check_Year.SelectedValue & "/1/1# and Experi_Date_1 <= #" & Check_Year.SelectedValue &
                    // "/12/31#) or (Experi_Date_2 >= #" & Check_Year.SelectedValue & "/1/1# and Experi_Date_2 <= #" & Check_Year.SelectedValue &
                    // "/12/31#) or (Experi_Date_3 >= #" & Check_Year.SelectedValue & "/1/1# and Experi_Date_3 <= #" & Check_Year.SelectedValue &
                    // "/12/31#) or (Experi_Date_4 >= #" & Check_Year.SelectedValue & "/1/1# and Experi_Date_4 <= #" & Check_Year.SelectedValue &
                    // "/12/31#) or (Sign_Contr_Date >= #" & Check_Year.SelectedValue & "/1/1# and Sign_Contr_Date <= #" & Check_Year.SelectedValue & "/12/31#))) ")
                    dr[1] = Re_Number.ToString();
                    TRe_Number = TRe_Number + Re_Number;

                    // 錄取人數 為合乎該面試日期的筆數 且 應徵結果為  0
                    Hi_Number = Convert.ToInt16(MyDataSet.Tables["RNumber"].Compute("Count(Name)", "Interview_Result = 0 and (Interview_Date >= " + SDPeriod + 
                                " and Interview_Date <= " + EDPeriod + ")"));
                    dr[2] = Hi_Number.ToString();
                    THi_Number = THi_Number + Hi_Number;

                    // 繳資料人數  繳資料日期 為 日期區間
                    Pa_Number = Convert.ToInt16(MyDataSet.Tables["RNumber"].Compute("Count(Name)", "(Hout_Info_Date >= " + SDPeriod + " and Hout_Info_Date <= " +
                                EDPeriod + ")"));
                    dr[3] = Pa_Number.ToString();
                    TPa_Number = TPa_Number + Pa_Number;

                    // 說明人數 新訓日期 為 日期區間
                    Or_Number = Convert.ToInt16(MyDataSet.Tables["RNumber"].Compute("Count(Name)", "(Train_Date >= " + SDPeriod + " and Train_Date <= " +
                                EDPeriod + ")"));
                    dr[4] = Or_Number.ToString();
                    TOr_Number = TOr_Number + Or_Number;

                    // 體驗人數  體驗日期 為 日期區間
                    Ex_Number = Convert.ToInt16(MyDataSet.Tables["RNumber"].Compute("Count(Name)", "(Experi_Date_1 >= " + SDPeriod + " and Experi_Date_1 <= " +
                                EDPeriod + ")"));
                    // 含跨月
                    // Ex_Number = MyDataSet.Tables("RNumber").Compute("Count(Name)", "(Experi_Date_1 >= #" & Check_Year.SelectedValue & "/1/1# and Experi_Date_1 <= #" & Check_Year.SelectedValue &
                    // "/12/31#) or (Experi_Date_2 >= #" & Check_Year.SelectedValue & "/1/1# and Experi_Date_2 <= #" & Check_Year.SelectedValue &
                    // "/12/31#) or (Experi_Date_3 >= #" & Check_Year.SelectedValue & "/1/1# and Experi_Date_3 <= #" & Check_Year.SelectedValue &
                    // "/12/31#) or (Experi_Date_4 >= #" & Check_Year.SelectedValue & "/1/1# and Experi_Date_4 <= #" & Check_Year.SelectedValue & "/12/31#)")
                    dr[5] = Ex_Number.ToString();
                    TEx_Number = TEx_Number + Ex_Number;

                    // 入職人數  簽約日期 為 日期區間
                    Fi_Number = Convert.ToInt16(MyDataSet.Tables["RNumber"].Compute("Count(Name)", "(Sign_Contr_Date >= " + SDPeriod + " and Sign_Contr_Date <= " +
                                EDPeriod + ")"));
                    dr[6] = Fi_Number.ToString();
                    TFi_Number = TFi_Number + Fi_Number;

                    // 比例
                    if (Re_Number == 0)
                        dr[7] = "0.00%";
                    else
                        dr[7] = (Hi_Number / (double)Re_Number).ToString("0.00%");

                    if (Hi_Number == 0)
                        dr[8] = "0.00%";
                    else
                        dr[8] = (Pa_Number / (double)Hi_Number).ToString("0.00%");

                    if (Pa_Number == 0)
                        dr[9] = "0.00%";
                    else
                        dr[9] = (Or_Number / (double)Pa_Number).ToString("0.00%");

                    if (Or_Number == 0)
                        dr[10] = "0.00%";
                    else
                        dr[10] = (Ex_Number / (double)Or_Number).ToString("0.00%");

                    if (Ex_Number == 0)
                        dr[11] = "0.00%";
                    else
                        dr[11] = (Fi_Number / (double)Ex_Number).ToString("0.00%");

                    if (Re_Number == 0)
                        dr[12] = "0.00%";
                    else
                        dr[12] = (Fi_Number / (double)Re_Number).ToString("0.00%");

                    dt.Rows.Add(dr);
                }

                dr = dt.NewRow();
                dr[0] = "總公司";

                dr[1] = TRe_Number.ToString();
                dr[2] = THi_Number.ToString();
                dr[3] = TPa_Number.ToString();
                dr[4] = TOr_Number.ToString();
                dr[5] = TEx_Number.ToString();
                dr[6] = TFi_Number.ToString();

                if (TRe_Number == 0)
                    dr[7] = "0.00%";
                else
                    dr[7] = (THi_Number / (double)TRe_Number).ToString("0.00%");

                if (THi_Number == 0)
                    dr[8] = "0.00%";
                else
                    dr[8] = (TPa_Number / (double)THi_Number).ToString("0.00%");

                if (TPa_Number == 0)
                    dr[9] = "0.00%";
                else
                    dr[9] = (TOr_Number / (double)TPa_Number).ToString("0.00%");

                if (TOr_Number == 0)
                    dr[10] = "0.00%";
                else
                    dr[10] = (TEx_Number / (double)TOr_Number).ToString("0.00%");

                if (TEx_Number == 0)
                    dr[11] = "0.00%";
                else
                    dr[11] = (TFi_Number / (double)TEx_Number).ToString("0.00%");

                if (TRe_Number == 0)
                    dr[12] = "0.00%";
                else
                    dr[12] = (TFi_Number / (double)TRe_Number).ToString("0.00%");

                dt.Rows.InsertAt(dr, NR);
            }


            // Repeater1.DataSource = dt.DefaultView
            // Repeater1.DataBind()

            // 1-12月資料

            dr = dt.NewRow();
            dt.Rows.Add(dr);
            NR = dt.Rows.Count;


            TRe_Number = 0;
            THi_Number = 0;
            TPa_Number = 0;
            TOr_Number = 0;
            TEx_Number = 0;
            TFi_Number = 0;
            Re_Number = 0;
            Hi_Number = 0;
            Pa_Number = 0;
            Or_Number = 0;
            Ex_Number = 0;
            Fi_Number = 0;

            for (jj = 1; jj <= 12; jj++)
            {
                if (Branch.SelectedIndex == 0)
                    Select_Banch = "Branch_Name is not null";
                else if (Branch.Items[Branch.SelectedIndex].Text == "南高雄")
                    Select_Banch = " (Branch_Name = '" + Branch.Items[Branch.SelectedIndex].Text + "' or Branch_Name = '屏東') ";
                else
                    Select_Banch = "Branch_Name = '" + Branch.Items[Branch.SelectedIndex].Text + "'";// 營業所名稱


                MyDataSet = new DataSet();
                MyCommand = new SqlDataAdapter("SELECT * FROM Recruit_Process_View where " + Select_Banch, INConnection1);
                MyCommand.Fill(MyDataSet, "RNumber");

                // MyCommand = New SqlDataAdapter("SELECT  Staff_Job_Data.Svr_Name FROM Staff_Job_Data LEFT OUTER JOIN Staff_Job_Data AS Staff_Job_Data_1 ON Staff_Job_Data.Svr_Name = Staff_Job_Data_1.Staff_Name WHERE  (Staff_Job_Data.Branch_Name ='" & Select_Banch & "') GROUP BY Staff_Job_Data.Svr_Name, Staff_Job_Data_1.Depart_Date, Staff_Job_Data_1.Staff_Name HAVING  (Staff_Job_Data_1.Depart_Date IS NULL) OR (YEAR(Staff_Job_Data_1.Depart_Date) = 2017) OR (Staff_Job_Data_1.Staff_Name IS NULL) ORDER BY Staff_Job_Data_1.Staff_Name", INConnection1)
                // MyCommand.Fill(MyDataSet, "Svr_Name")




                dr = dt.NewRow();
                dr[0] = jj.ToString() + "月";
                string MString = "";

                // 判斷月份
                switch (jj)
                {
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                    case 10:
                    case 12:
                        {
                            MString = Check_Year.SelectedValue + "/" + jj.ToString() + "/31";
                            break;
                        }

                    case 4:
                    case 6:
                    case 9:
                    case 11:
                        {
                            MString = Check_Year.SelectedValue + "/" + jj.ToString() + "/30";
                            break;
                        }

                    case 2:
                        {
                            if (SF.IsDate(Check_Year.SelectedValue + "/" + jj.ToString() + "/29") == true)
                                MString = Check_Year.SelectedValue + "/" + jj.ToString() + "/29";
                            else
                                MString = Check_Year.SelectedValue + "/" + jj.ToString() + "/28";
                            break;
                        }
                }



                // 應徵人數 即為合乎該面試日期的筆數
                Re_Number = Convert.ToInt16(MyDataSet.Tables["RNumber"].Compute("Count(Name)", "(Interview_Date >= #" + Check_Year.SelectedValue + "/" +
                            jj.ToString() + "/1# and Interview_Date <=#" + MString + "#)"));
                dr[1] = Re_Number.ToString();
                TRe_Number = TRe_Number + Re_Number;

                // 錄取人數 為合乎該面試日期的筆數 且 應徵結果為  0
                Hi_Number = Convert.ToInt16(MyDataSet.Tables["RNumber"].Compute("Count(Name)", "Interview_Result = 0 and (Interview_Date >= #" + 
                            Check_Year.SelectedValue + "/" + jj.ToString() + "/1# and Interview_Date <= #" + MString + "#)"));
                dr[2] = Hi_Number.ToString();
                THi_Number = THi_Number + Hi_Number;

                // 繳資料人數  繳資料日期 為 日期區間
                Pa_Number = Convert.ToInt16(MyDataSet.Tables["RNumber"].Compute("Count(Name)", "(Hout_Info_Date >= #" + Check_Year.SelectedValue + "/" + 
                            jj.ToString() + "/1# and Hout_Info_Date <= #" + MString + "#)"));
                dr[3] = Pa_Number.ToString();
                TPa_Number = TPa_Number + Pa_Number;

                // 說明人數 新訓日期 為 日期區間
                Or_Number = Convert.ToInt16(MyDataSet.Tables["RNumber"].Compute("Count(Name)", "(Train_Date >= #" + Check_Year.SelectedValue + "/" +
                    jj.ToString() + "/1# and Train_Date <= #" + MString + "#)"));
                dr[4] = Or_Number.ToString();
                TOr_Number = TOr_Number + Or_Number;

                // 體驗人數  體驗日期 為 日期區間
                Ex_Number = Convert.ToInt16(MyDataSet.Tables["RNumber"].Compute("Count(Name)", "(Experi_Date_1 >= #" + Check_Year.SelectedValue + "/" + 
                            jj.ToString() + "/1# and Experi_Date_1 <= #" + MString + "#)"));
                dr[5] = Ex_Number.ToString();
                TEx_Number = TEx_Number + Ex_Number;

                // 入職人數  簽約日期 為 日期區間
                Fi_Number = Convert.ToInt16(MyDataSet.Tables["RNumber"].Compute("Count(Name)", "(Sign_Contr_Date >= #" + Check_Year.SelectedValue + "/" + 
                            jj.ToString() + "/1# and Sign_Contr_Date <= #" + MString + "#)"));
                dr[6] = Fi_Number.ToString();
                TFi_Number = TFi_Number + Fi_Number;

                // 比例
                if (Re_Number == 0)
                    dr[7] = "0.00%";
                else
                    dr[7] = (Hi_Number / (double)Re_Number).ToString("0.00%");

                if (Hi_Number == 0)
                    dr[8] = "0.00%";
                else
                    dr[8] = (Pa_Number / (double)Hi_Number).ToString("0.00%");

                if (Pa_Number == 0)
                    dr[9] = "0.00%";
                else
                    dr[9] = (Or_Number / (double)Pa_Number).ToString("0.00%");

                if (Or_Number == 0)
                    dr[10] = "0.00%";
                else
                    dr[10] = (Ex_Number / (double)Or_Number).ToString("0.00%");

                if (Ex_Number == 0)
                    dr[11] = "0.00%";
                else
                    dr[11] = (Fi_Number / (double)Ex_Number).ToString("0.00%");

                if (Re_Number == 0)
                    dr[12] = "0.00%";
                else
                    dr[12] = (Fi_Number / (double)Re_Number).ToString("0.00%");

                dt.Rows.Add(dr);
            }
            if (Branch.SelectedIndex == 0)
            {
                dr = dt.NewRow();
                dr[0] = "Total";

                dr[1] = TRe_Number.ToString();
                dr[2] = THi_Number.ToString();
                dr[3] = TPa_Number.ToString();
                dr[4] = TOr_Number.ToString();
                dr[5] = TEx_Number.ToString();
                dr[6] = TFi_Number.ToString();

                if (TRe_Number == 0)
                    dr[7] = "0.00%";
                else
                    dr[7] = (THi_Number / (double)TRe_Number).ToString("0.00%");

                if (THi_Number == 0)
                    dr[8] = "0.00%";
                else
                    dr[8] = (TPa_Number / (double)THi_Number).ToString("0.00%");

                if (TPa_Number == 0)
                    dr[9] = "0.00%";
                else
                    dr[9] = (TOr_Number / (double)TPa_Number).ToString("0.00%");

                if (TOr_Number == 0)
                    dr[10] = "0.00%";
                else
                    dr[10] = (TEx_Number / (double)TOr_Number).ToString("0.00%");

                if (TEx_Number == 0)
                    dr[11] = "0.00%";
                else
                    dr[11] = (TFi_Number / (double)TEx_Number).ToString("0.00%");

                if (TRe_Number == 0)
                    dr[12] = "0.00%";
                else
                    dr[12] = (TFi_Number / (double)TRe_Number).ToString("0.00%");

                dt.Rows.InsertAt(dr, NR);
            }
            // 排序
            Repeater1.DataSource = dt.DefaultView;
            Repeater1.DataBind();

            // --------------------------------------------------------------------------------------------
            // 異常報表
            dt = new DataTable();
            dt.Columns.Add(new DataColumn("ID", typeof(string)));
            dt.Columns.Add(new DataColumn("部門", typeof(string)));
            dt.Columns.Add(new DataColumn("本月入職人數", typeof(string)));
            dt.Columns.Add(new DataColumn("近二周入職人數", typeof(string)));
            dt.Columns.Add(new DataColumn("本月應徵人數", typeof(string)));
            dt.Columns.Add(new DataColumn("本周應徵人數", typeof(string)));
            dt.Columns.Add(new DataColumn("本日應徵人數", typeof(string)));
            dt.Columns.Add(new DataColumn("近二周說明會人數", typeof(string)));

            // 異常數字

            int Ti;
            for (Ti = 1; Ti <= Branch.Items.Count - 1; Ti++)
            {
                dr = dt.NewRow();
                dr[0] = Ti;
                dr[1] = Branch.Items[Ti].Text.Trim();
                dt.Rows.Add(dr);
            }

            string INString;
            INString = "SELECT   Branch_Name,COUNT(Branch_Name) AS 簽約 FROM Recruit_Process_View where Month(Sign_Contr_Date) = month( GETDATE()) and " +
                       "year(Sign_Contr_Date) = year( GETDATE()) GROUP BY Branch_Name";
            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();

            while (SQLReader.Read() == true)
            {
                for (Ti = 0; Ti <= dt.Rows.Count - 1; Ti++)
                {
                    if ((string)dt.Rows[Ti][1] == SQLReader[0].ToString().Trim())
                    {
                        dt.Rows[Ti][2] = SQLReader[1];    // 本月入職人員
                        break;
                    }
                }
            }
            INCommand.Connection.Close();

            INString = "SELECT  Branch_Name,COUNT(Branch_Name) AS 簽約 FROM Recruit_Process_View where datediff(WEEK,Sign_Contr_Date,GETDATE()) <= 1 " +
                       "GROUP BY Branch_Name";
            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();

            while (SQLReader.Read() == true)
            {
                for (Ti = 0; Ti <= dt.Rows.Count - 1; Ti++)
                {
                    if ((string)dt.Rows[Ti][1] == SQLReader[0].ToString().Trim())
                    {
                        dt.Rows[Ti][3] = SQLReader[1];    // 本二周入職人員
                        break;
                    }
                }
            }
            INCommand.Connection.Close();

            INString = "SELECT   Branch_Name,COUNT(Branch_Name) AS Interview_Date FROM Recruit_Process_View where Month(Interview_Date) = month( GETDATE()) " +
                       "and year(Interview_Date) = year( GETDATE()) GROUP BY Branch_Name";
            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();

            while (SQLReader.Read() == true)
            {
                for (Ti = 0; Ti <= dt.Rows.Count - 1; Ti++)
                {
                    if ((string)dt.Rows[Ti][1] == SQLReader[0].ToString().Trim())
                    {
                        dt.Rows[Ti][4] = SQLReader[1];    // 本月應徵人員
                        break;
                    }
                }
            }
            INCommand.Connection.Close();

            INString = "SELECT   Branch_Name,COUNT(Branch_Name) AS Interview_Date FROM Recruit_Process_View where datediff(WEEK,Interview_Date,GETDATE()) = 0 " +
                       "GROUP BY Branch_Name";
            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();

            while (SQLReader.Read() == true)
            {
                for (Ti = 0; Ti <= dt.Rows.Count - 1; Ti++)
                {
                    if ((string)dt.Rows[Ti][1] == SQLReader[0].ToString().Trim())
                    {
                        dt.Rows[Ti][5] = SQLReader[1];    // 本周應徵人員
                        break;
                    }
                }
            }
            INCommand.Connection.Close();


            INString = "SELECT   Branch_Name,COUNT(Branch_Name) AS Interview_Date FROM Recruit_Process_View where datediff(DY,Interview_Date,GETDATE()) = 1 " +
                       "GROUP BY Branch_Name";
            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();

            while (SQLReader.Read() == true)
            {
                for (Ti = 0; Ti <= dt.Rows.Count - 1; Ti++)
                {
                    if ((string)dt.Rows[Ti][1] == SQLReader[0].ToString().Trim())
                    {
                        dt.Rows[Ti][6] = SQLReader[1];    // 本日應徵人員
                        break;
                    }
                }
            }
            INCommand.Connection.Close();

            INString = "SELECT  Branch_Name,COUNT(Branch_Name) AS 新訓 FROM Recruit_Process_View where datediff(WEEK,Train_Date,GETDATE()) <= 1 " +
                       "GROUP BY Branch_Name";
            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();

            while (SQLReader.Read() == true)
            {
                for (Ti = 0; Ti <= dt.Rows.Count - 1; Ti++)
                {
                    if ((string)dt.Rows[Ti][1] == SQLReader[0].ToString().Trim())
                    {
                        dt.Rows[Ti][7] = SQLReader[1];    // 本二周說明人員
                        break;
                    }
                }
            }
            INCommand.Connection.Close();

            DataGrid5.DataSource = dt.DefaultView;
            DataGrid5.DataBind();


            if (Branch.SelectedIndex == 0)
                Select_Banch = "Branch_Name is not null";
            else if (Branch.Items[Branch.SelectedIndex].Text == "南高雄")
                Select_Banch = " (Branch_Name = '" + Branch.Items[Branch.SelectedIndex].Text + "' or Branch_Name = '屏東') ";
            else
                Select_Banch = "Branch_Name = '" + Branch.Items[Branch.SelectedIndex].Text + "'";// 營業所名稱



            // 繳交資料有問題 
            MyDataSet = new DataSet();
            MyCommand = new SqlDataAdapter(
                        "SELECT * FROM Recruit_Process_View where datediff(month,Interview_Date,getdate()) < 2 and  Interview_Result = 0 and" +
                        " ( DATEDIFF(day,Interview_Date,Hout_Info_Date) > 10  or  DateDiff(day,Interview_Date,getdate()) > 10 ) and  ( Hout_Info_Date is null" +
                        " and No_Info_Reason is null and No_Train_Reason is null)  and " + Select_Banch + " Order by Branch_Name,Interview_Date", INConnection1);
            MyCommand.Fill(MyDataSet, "RNumber");
            Label2.Text = MyDataSet.Tables["RNumber"].DefaultView.Count.ToString();
            DataGrid1.DataSource = MyDataSet.Tables["RNumber"].DefaultView;
            DataGrid1.DataBind();
            if (MyDataSet.Tables["RNumber"].DefaultView.Count > 0)
                Panel1.Visible = true;
            else
                Panel1.Visible = false;

            // 新訓有問題 
            MyDataSet = new DataSet();
            MyCommand = new SqlDataAdapter(
                        "SELECT * FROM Recruit_Process_View where datediff(month,Interview_Date,getdate()) < 2  and Interview_Result = 0 and " +
                        "(DATEDIFF(day,Hout_Info_Date,Train_Date) > 7  or  DateDiff(day,Hout_Info_Date,getdate()) > 7 ) and Train_Date is null and " +
                        " No_Train_Reason is null   and " + Select_Banch + " Order by Branch_Name,Interview_Date", INConnection1);
            MyCommand.Fill(MyDataSet, "RNumber");
            Label3.Text = MyDataSet.Tables["RNumber"].DefaultView.Count.ToString();
            DataGrid2.DataSource = MyDataSet.Tables["RNumber"].DefaultView;
            DataGrid2.DataBind();
            if (MyDataSet.Tables["RNumber"].DefaultView.Count > 0)
                Panel2.Visible = true;
            else
                Panel2.Visible = false;
            // 未安排體驗
            MyDataSet = new DataSet();
            MyCommand = new SqlDataAdapter(
                        "SELECT * FROM Recruit_Process_View where datediff(month,Interview_Date,getdate()) < 2  and Interview_Result = 0 and " +
                        " (DATEDIFF(day,Train_Date,Experi_Date_1) > 2  or DateDiff(day,Train_Date,getdate()) > 2 ) and Experi_Date_1 is null   and " +
                        Select_Banch + " Order by Branch_Name,Interview_Date", INConnection1);
            MyCommand.Fill(MyDataSet, "RNumber");
            Label4.Text = MyDataSet.Tables["RNumber"].DefaultView.Count.ToString();
            DataGrid3.DataSource = MyDataSet.Tables["RNumber"].DefaultView;
            DataGrid3.DataBind();
            if (MyDataSet.Tables["RNumber"].DefaultView.Count > 0)
                Panel3.Visible = true;
            else
                Panel3.Visible = false;
            // 未入職
            MyDataSet = new DataSet();
            MyCommand = new SqlDataAdapter(
                        "SELECT * FROM Recruit_Process_View where datediff(month,Interview_Date,getdate()) < 2  and Interview_Result = 0 and " +
                        "(DATEDIFF(day,Train_Date,Sign_Contr_Date) > 7  or DateDiff(day,Train_Date,getdate()) > 7 ) and Sign_Contr_Date is null and " +
                        "No_Sign_Reason is null     and " + Select_Banch + " Order by Branch_Name,Interview_Date", INConnection1);
            MyCommand.Fill(MyDataSet, "RNumber");
            Label5.Text = MyDataSet.Tables["RNumber"].DefaultView.Count.ToString();
            DataGrid4.DataSource = MyDataSet.Tables["RNumber"].DefaultView;
            DataGrid4.DataBind();
            if (MyDataSet.Tables["RNumber"].DefaultView.Count > 0)
                Panel4.Visible = true;
            else
                Panel4.Visible = false;

            // ---------------------------------------------------------------------------------------
            // 金牌績效表
            dt = new DataTable();
            dt.Columns.Add(new DataColumn("ID", typeof(string)));
            dt.Columns.Add(new DataColumn("EX_Name", typeof(string)));
            dt.Columns.Add(new DataColumn("EX_Number_T", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Number_T", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Ratio_T", typeof(string)));
            dt.Columns.Add(new DataColumn("EX_Number_1", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Number_1", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Ratio_1", typeof(string)));
            dt.Columns.Add(new DataColumn("EX_Number_2", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Number_2", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Ratio_2", typeof(string)));
            dt.Columns.Add(new DataColumn("EX_Number_3", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Number_3", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Ratio_3", typeof(string)));
            dt.Columns.Add(new DataColumn("EX_Number_4", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Number_4", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Ratio_4", typeof(string)));
            dt.Columns.Add(new DataColumn("EX_Number_5", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Number_5", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Ratio_5", typeof(string)));
            dt.Columns.Add(new DataColumn("EX_Number_6", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Number_6", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Ratio_6", typeof(string)));
            dt.Columns.Add(new DataColumn("EX_Number_7", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Number_7", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Ratio_7", typeof(string)));
            dt.Columns.Add(new DataColumn("EX_Number_8", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Number_8", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Ratio_8", typeof(string)));
            dt.Columns.Add(new DataColumn("EX_Number_9", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Number_9", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Ratio_9", typeof(string)));
            dt.Columns.Add(new DataColumn("EX_Number_A", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Number_A", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Ratio_A", typeof(string)));
            dt.Columns.Add(new DataColumn("EX_Number_B", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Number_B", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Ratio_B", typeof(string)));
            dt.Columns.Add(new DataColumn("EX_Number_C", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Number_C", typeof(string)));
            dt.Columns.Add(new DataColumn("Fi_Ratio_C", typeof(string)));
            dt.Columns.Add(new DataColumn("S1", typeof(int)));  // 排序用
            dt.Columns.Add(new DataColumn("S2", typeof(int)));  // 排序用

            if (Branch.SelectedIndex == 0)
            {
                Start_Banch = 0;
                End_Banch = Branch.Items.Count - 1;
            }
            else
            {
                Start_Banch = Branch.SelectedIndex;
                End_Banch = Branch.SelectedIndex;
            }

            Ex_Number = 0;
            Fi_Number = 0;

            for (jj = Start_Banch; jj <= End_Banch; jj++)
            {
                string Query_String = "(Experi_Date_1 >= #" + Check_Year.SelectedValue + "/1/1# And Experi_Date_1 <= #" + Check_Year.SelectedValue + "/12/31#) ";
                int End_Name;

                if (jj == 0)
                {
                    MyDataSet = new DataSet();
                    MyCommand = new SqlDataAdapter(
                                "SELECT ID,Charge_Staff_Name,year(Experi_Date_1) as 體驗年份,month(Experi_Date_1) as 體驗月份 ,year(Sign_Contr_Date) as 簽約年份," +
                                "month(Sign_Contr_Date) as 簽約月份 FROM Recruit_Process_View ", INConnection1);
                    MyCommand.Fill(MyDataSet, "RNumber");

                    MyCommand = new SqlDataAdapter(
                                "SELECT  Staff_Job_Data.Svr_Name FROM Staff_Job_Data LEFT OUTER JOIN " + " Staff_Job_Data [Staff_Job_Data_1] ON " +
                                "Staff_Job_Data.Svr_Name = [Staff_Job_Data_1].Staff_Name " + " WHERE   (Staff_Job_Data.Depart_Date Is NULL OR " +
                                " YEAR(Staff_Job_Data.Depart_Date) >= " + Check_Year.SelectedValue + ") " +
                                " GROUP BY Staff_Job_Data.Svr_Name, [Staff_Job_Data_1].Depart_Date, [Staff_Job_Data_1].Staff_Name, " +
                                " [Staff_Job_Data_1].Job_Status HAVING  ([Staff_Job_Data_1].Depart_Date Is NULL) And " +
                                " ([Staff_Job_Data_1].Job_Status <> '調職') OR " + " (YEAR([Staff_Job_Data_1].Depart_Date) >= " + 
                                Check_Year.SelectedValue + ") " + " OR ([Staff_Job_Data_1].Staff_Name IS NULL) ORDER BY Staff_Job_Data_1.Staff_Name", INConnection1);
                    MyCommand.Fill(MyDataSet, "金牌");
                    Select_Banch = "合計";
                    End_Name = 0;
                }
                else
                {
                    if (Branch.Items[jj].Text.Trim() == "南高雄")
                    {
                        Select_Banch = " (Branch_Name ='南高雄' or Branch_Name = '屏東') ";
                        Select_Banch1 = " (Staff_Job_Data.Branch_Name ='南高雄' or Staff_Job_Data.Branch_Name = '屏東') ";
                    }
                    else
                    {
                        Select_Banch = " Branch_Name ='" + Branch.Items[jj].Text.Trim() + "'";    // 營業所名稱
                        Select_Banch1 = " Staff_Job_Data.Branch_Name ='" + Branch.Items[jj].Text.Trim () + "'";
                    }


                    MyDataSet = new DataSet();
                    MyCommand = new SqlDataAdapter(
                                "SELECT ID,Charge_Staff_Name,year(Experi_Date_1) as 體驗年份,month(Experi_Date_1) as 體驗月份 ,year(Sign_Contr_Date) as 簽約年份," +
                                "month(Sign_Contr_Date) as 簽約月份 FROM Recruit_Process_View where " + Select_Banch, INConnection1);
                    MyCommand.Fill(MyDataSet, "RNumber");

                    MyCommand = new SqlDataAdapter(
                                "SELECT  Staff_Job_Data.Svr_Name FROM Staff_Job_Data LEFT OUTER JOIN " + " Staff_Job_Data [Staff_Job_Data_1] ON " +
                                "Staff_Job_Data.Svr_Name = [Staff_Job_Data_1].Staff_Name " + " WHERE   (" + Select_Banch1 + ")" + 
                                " AND (Staff_Job_Data.Depart_Date Is NULL OR " + " YEAR(Staff_Job_Data.Depart_Date) >= " + Check_Year.SelectedValue + ") " +
                                " GROUP BY Staff_Job_Data.Svr_Name, [Staff_Job_Data_1].Depart_Date, [Staff_Job_Data_1].Staff_Name, " + 
                                " [Staff_Job_Data_1].Job_Status HAVING  ([Staff_Job_Data_1].Depart_Date Is NULL) And " +
                                " ([Staff_Job_Data_1].Job_Status <> '調職') OR " + " (YEAR([Staff_Job_Data_1].Depart_Date) >= " + Check_Year.SelectedValue + ") " +
                                " OR ([Staff_Job_Data_1].Staff_Name IS NULL) ORDER BY Staff_Job_Data_1.Staff_Name", INConnection1);
                    MyCommand.Fill(MyDataSet, "金牌");
                    End_Name = MyDataSet.Tables["金牌"].DefaultView.Count;
                }




                int ii;

                for (ii = 0; ii <= End_Name; ii++)
                {
                    dr = dt.NewRow();

                    // 排序用
                    string Se_String;
                    if (ii == 0)
                    {
                        Se_String = "";
                        dr[0] = Branch.Items[jj].Text;
                    }
                    else
                    {
                        Se_String = " and  Charge_Staff_Name = '" + MyDataSet.Tables["金牌"].DefaultView[ii - 1][0] + "'";
                        dr[0] = Branch.Items[jj].Text;
                        dr[1] = MyDataSet.Tables["金牌"].DefaultView[ii - 1][0];
                    }
                    dr[41] = jj;                   // 各營業所 KEY 值
                    dr[42] = ii;                   // 各輔導組長 KEY 值


                    int kk;

                    for (kk = 0; kk <= 12; kk++)   // 1-12月
                    {
                        if (kk == 0)
                        {
                            Ex_Number = Convert.ToInt16(MyDataSet.Tables["RNumber"].Compute("Count(ID)", "體驗年份 = " + Check_Year.SelectedValue + Se_String));
                            Fi_Number = Convert.ToInt16(MyDataSet.Tables["RNumber"].Compute("Count(ID)", "簽約年份 = " + Check_Year.SelectedValue + Se_String));
                            dr[2] = Ex_Number.ToString("0");
                            dr[3] = Fi_Number.ToString("0");
                            dr[4] = Ex_Number != 0 ? (Fi_Number / (double)Ex_Number).ToString("0%") : "0";
                        }
                        else
                        {
                            Ex_Number = Convert.ToInt16(MyDataSet.Tables["RNumber"].Compute("Count(ID)", "體驗年份 = " + Check_Year.SelectedValue +
                                        " and 體驗月份 = " + kk + Se_String));
                            Fi_Number = Convert.ToInt16(MyDataSet.Tables["RNumber"].Compute("Count(ID)", "簽約年份 = " + Check_Year.SelectedValue +
                                        " and 簽約月份 = " + kk + Se_String));
                            dr[2 + kk * 3] = Ex_Number.ToString("0");
                            dr[3 + kk * 3] = Fi_Number.ToString("0");
                            dr[4 + kk * 3] = Ex_Number != 0 ? (Fi_Number / (double)Ex_Number).ToString("0%") : "0";
                        }
                    }
                    dt.Rows.Add(dr);
                }
            }

            dt.DefaultView.Sort = "S1 ASC, S2 ASC";     // 排序
            Repeater2.DataSource = dt.DefaultView;
            Repeater2.DataBind();


            Page_Load(sender, e);
        }
        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = RadioButtonList1.SelectedIndex;

            Page_Load(sender, e);
        }
        public void Show_Sc_Type(object sender, DataGridItemEventArgs e)
        {
            ListItemType objItemType = (ListItemType)e.Item.ItemType;
            Label TempLabel;

            if (objItemType == ListItemType.Item | objItemType == ListItemType.AlternatingItem)
            {
                if (((Control)sender).ID == "DataGrid1")
                {
                    TempLabel = (Label)e.Item.FindControl("Hire_FLag");
                    switch (e.Item.Cells[7].Text)
                    {
                        case "0":
                            {
                                TempLabel.Text = "錄取";
                                break;
                            }

                        case "1":
                            {
                                TempLabel.Text = "安排複試";
                                break;
                            }

                        case "2":
                            {
                                TempLabel.Text = "不錄取";
                                break;
                            }

                        case "3":
                            {
                                TempLabel.Text = "保留";
                                break;
                            }
                    }
                }
                if (((Control)sender).ID == "DataGrid5")
                {
                    e.Item.Cells[2].BackColor = Color.LightPink;
                    if (e.Item.Cells[2].Text != "&nbsp;")
                    {
                        if (Convert.ToInt16(e.Item.Cells[2].Text) >= 2)
                            e.Item.Cells[2].BackColor = Color.White;
                    }
                    e.Item.Cells[3].BackColor = Color.LightPink;
                    if (e.Item.Cells[3].Text != "&nbsp;")
                    {
                        if (Convert.ToInt16(e.Item.Cells[3].Text) >= 1)
                            e.Item.Cells[3].BackColor = Color.White;
                    }
                    e.Item.Cells[4].BackColor = Color.LightPink;
                    if (e.Item.Cells[4].Text != "&nbsp;")
                    {
                        if (Convert.ToInt16(e.Item.Cells[4].Text) >= 16)
                            e.Item.Cells[4].BackColor = Color.White;
                    }
                    e.Item.Cells[5].BackColor = Color.LightPink;
                    if (e.Item.Cells[5].Text != "&nbsp;")
                    {
                        if (Convert.ToInt16(e.Item.Cells[5].Text) >= 4)
                            e.Item.Cells[5].BackColor = Color.White;
                    }
                    e.Item.Cells[6].BackColor = Color.LightPink;
                    if (e.Item.Cells[6].Text != "&nbsp;")
                    {
                        if (Convert.ToInt16(e.Item.Cells[6].Text) >= 1)
                            e.Item.Cells[6].BackColor = Color.White;
                    }
                    e.Item.Cells[7].BackColor = Color.LightPink;
                    if (e.Item.Cells[7].Text != "&nbsp;")
                    {
                        if (Convert.ToInt16(e.Item.Cells[7].Text) >= 1)
                            e.Item.Cells[7].BackColor = Color.White;
                    }
                }
            }
        }
        public void Show_Job_Detail(object sender, DataGridCommandEventArgs e)
        {
        }

        protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            ListItemType objItemType = (ListItemType)e.Item.ItemType;
            Label tmpLabel1, tmpLabel2;
            int MPlus = 12;
            if (Check_Year.SelectedValue == DateTime.Now.Year.ToString())
                MPlus = DateTime.Now.Month;
            if (objItemType == ListItemType.Item | objItemType == ListItemType.AlternatingItem)
            {
                if (((Control)sender).ID == "Repeater1")
                {
                    int XPlus = 1;
                    if (Branch.SelectedIndex == 0)
                        XPlus = Branch.Items.Count - 1;

                    tmpLabel1 = (Label)e.Item.FindControl("Label1");
                    // 應徵人數
                    tmpLabel2 = (Label)e.Item.FindControl("Recruit_Number");
                    //if (SF.IsNumeric(SF.NSubString(tmpLabel1.Text, 0, 1)) == true)
                    if(tmpLabel1.Text.Trim() == "總公司")
                    {
                        tmpLabel2.ForeColor = Color.Black;
                        if (IsTheMonth)
                        {
                            if (Convert.ToInt16(tmpLabel2.Text) < ((XPlus * 16) * MPlus))
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                            IsTheMonth = false;
                        }
                        else
                        {
                            if (Convert.ToInt16(tmpLabel2.Text) < (XPlus * 16))
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                        }
                    }
                    else if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "")
                    {
                        //if (Convert.ToInt16(Recruit_Number.Text) < 16 * MPlus)
                        if (tmpLabel1.Text.Trim().IndexOf("當月") == -1)
                        {
                            if ((Convert.ToInt16(tmpLabel2.Text) / MPlus) < 16)
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                            else
                            {
                                tmpLabel2.ForeColor = Color.Black;
                            }
                        }
                        else
                        {
                            if (Convert.ToInt16(tmpLabel2.Text) < 16)
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                            else
                            {
                                tmpLabel2.ForeColor = Color.Black;
                            }
                        }
                    }

                    // 錄取人數
                    tmpLabel2 = (Label)e.Item.FindControl("Hire_Number");
                    //if (SF.IsNumeric(SF.NSubString(tmpLabel1.Text,0, 1)) == true)
                    if (tmpLabel1.Text.Trim() == "總公司")
                    {
                        tmpLabel2.ForeColor = Color.Black;
                        if (IsTheMonth)
                        {
                            if (Convert.ToInt16(tmpLabel2.Text) < ((XPlus * 8) * MPlus))
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                            IsTheMonth = false;
                        }
                        else
                        {
                            if (Convert.ToInt16(tmpLabel2.Text) < (XPlus * 8))
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                        }
                    }
                    else if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "")
                    {
                        //if (Convert.ToInt16(tmpLabel2.Text) < 8 * MPlus)
                        if (tmpLabel1.Text.Trim().IndexOf("當月") == -1)
                        {
                            if ((Convert.ToInt16(tmpLabel2.Text) / MPlus) < 8)
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                            else
                            {
                                tmpLabel2.ForeColor = Color.Black;
                            }
                        }
                        else
                        {
                            if (Convert.ToInt16(tmpLabel2.Text) < 8)
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                            else
                            {
                                tmpLabel2.ForeColor = Color.Black;
                            }
                        }
                    }

                    // 繳交人數
                    tmpLabel2 = (Label)e.Item.FindControl("Paper_Number");
                    //if (SF.IsNumeric(SF.NSubString(tmpLabel1.Text,0, 1)) == true)
                    if (tmpLabel1.Text.Trim() == "總公司")
                    {
                        tmpLabel2.ForeColor = Color.Black;
                        if (IsTheMonth)
                        {
                            if (Convert.ToInt16(tmpLabel2.Text) < ((XPlus * 5) * MPlus))
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                            IsTheMonth = false;
                        }
                        else
                        {
                            if (Convert.ToInt16(tmpLabel2.Text) < (XPlus * 5))
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                        }
                    }
                    else if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "")
                    {
                        //if (Convert.ToInt16(tmpLabel2.Text) < 5 * MPlus)
                        if (tmpLabel1.Text.Trim().IndexOf("當月") == -1)
                        {
                            if ((Convert.ToInt16(tmpLabel2.Text) / MPlus) < 5)
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                            else
                            {
                                tmpLabel2.ForeColor = Color.Black;
                            }
                        }
                        else
                        {
                            if (Convert.ToInt16(tmpLabel2.Text) < 5)
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                            else
                            {
                                tmpLabel2.ForeColor = Color.Black;
                            }
                        }
                    }

                    // 體驗人數
                    tmpLabel2 = (Label)e.Item.FindControl("Experience_Number");
                    //if (SF.IsNumeric(SF.NSubString(tmpLabel1.Text,0, 1)) == true)
                    if (tmpLabel1.Text.Trim() == "總公司")
                    {
                        tmpLabel2.ForeColor = Color.Black;
                        if (IsTheMonth)
                        {
                            if (Convert.ToInt16(tmpLabel2.Text) < ((XPlus * 4) * MPlus))
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                            IsTheMonth = false;
                        }
                        else
                        {
                            if (Convert.ToInt16(tmpLabel2.Text) < (XPlus * 4))
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                        }
                    }
                    else if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "")
                    {
                        //if (Convert.ToInt16(tmpLabel2.Text) < 4 * MPlus)
                        if (tmpLabel1.Text.Trim().IndexOf("當月") == -1)
                        {
                            if ((Convert.ToInt16(tmpLabel2.Text) / MPlus) < 4)
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                            else
                            {
                                tmpLabel2.ForeColor = Color.Black;
                            }
                        }
                        else
                        {
                            if (Convert.ToInt16(tmpLabel2.Text) < 4)
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                            else
                            {
                                tmpLabel2.ForeColor = Color.Black;
                            }
                        }
                    }
                    
                    // 入職人數
                    tmpLabel2 = (Label)e.Item.FindControl("Final_Number");
                    //if (SF.IsNumeric(SF.NSubString(tmpLabel1.Text,0, 1)) == true)
                    if (tmpLabel1.Text == "總公司")
                    {
                        tmpLabel2.ForeColor = Color.Black;
                        if (IsTheMonth)
                        {
                            if (Convert.ToInt16(tmpLabel2.Text) < ((XPlus * 2) * MPlus))
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                            IsTheMonth = false;
                        }
                        else
                        {
                            if (Convert.ToInt16(tmpLabel2.Text) < (XPlus * 2))
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                        }
                    }
                    else if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "")
                    {
                        //if (Convert.ToInt16(tmpLabel2.Text) < 3 * MPlus)
                        tmpLabel2.ForeColor = Color.Black;
                        if (tmpLabel1.Text.Trim().IndexOf("當月") == -1)
                        {
                            if ((Convert.ToInt16(tmpLabel2.Text) / MPlus) < 2)
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                        }
                        else
                        {
                            if (Convert.ToInt16(tmpLabel2.Text) < 2)
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                        }
                    }

                    // 錄取率
                    tmpLabel2 = (Label)e.Item.FindControl("R_H_P");
                    if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "")
                    {
                        if (SF.IsNumeric(SF.NSubString(tmpLabel2.Text,0,tmpLabel2.Text.Length  - 1)))
                        {
                            if (Convert.ToDouble(SF.NSubString(tmpLabel2.Text,0,tmpLabel2.Text.Length  - 1)) < 50.0)
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                            else
                            {
                                tmpLabel2.ForeColor = Color.Black;
                            }
                        }
                    }

                    // 繳交率
                    tmpLabel2 = (Label)e.Item.FindControl("H_P_P");
                    if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "")
                    {
                        if (SF.IsNumeric(SF.NSubString(tmpLabel2.Text,0,tmpLabel2.Text.Length  - 1)))
                        {
                            if (Convert.ToDouble(SF.NSubString(tmpLabel2.Text,0,tmpLabel2.Text.Length  - 1)) < 60.0)
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                            else
                            {
                                tmpLabel2.ForeColor = Color.Black;
                            }
                        }
                    }

                    // 體驗入職率
                    tmpLabel2 = (Label)e.Item.FindControl("E_W_P");
                    if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "")
                    {
                        if (SF.IsNumeric(SF.NSubString(tmpLabel2.Text,0,tmpLabel2.Text.Length  - 1)))
                        {
                            if (Convert.ToDouble(SF.NSubString(tmpLabel2.Text,0,tmpLabel2.Text.Length  - 1)) < 70.0)
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                            else
                            {
                                tmpLabel2.ForeColor = Color.Black;
                            }
                        }
                    }

                    // 應徵入職率
                    tmpLabel2 = (Label)e.Item.FindControl("R_W_P");
                    if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "")
                    {
                        if (SF.IsNumeric(SF.NSubString(tmpLabel2.Text,0,tmpLabel2.Text.Length  - 1)))
                        {
                            if (Convert.ToDouble(SF.NSubString(tmpLabel2.Text, 0, tmpLabel2.Text.Length - 1)) < 15.5)
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                            else
                            {
                                tmpLabel2.ForeColor = Color.Black;
                            }
                        }
                    }
                }
                else
                {
                    int ii;

                    tmpLabel1 = (Label)e.Item.FindControl("Label1");
                    tmpLabel2 = (Label)e.Item.FindControl("Label7");

                    if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "")
                    {
                        if (SF.IsNumeric(SF.NSubString(tmpLabel2.Text,0,tmpLabel2.Text.Length  - 1)))
                        {
                            if (Convert.ToInt32(SF.NSubString(tmpLabel2.Text,0,tmpLabel2.Text.Length  - 1)) < 70)
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                            else
                            {
                                tmpLabel2.ForeColor = Color.Black;
                            }
                        }
                    }

                    for (ii = 0; ii <= MPlus - 1; ii++)
                    {
                        string tmpName = "Label";

                        tmpName = tmpName + (ii * 3 + 10).ToString();
                        tmpLabel2 = (Label)e.Item.FindControl(tmpName);

                        if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "")
                        {
                            if (SF.IsNumeric(SF.NSubString(tmpLabel2.Text,0,tmpLabel2.Text.Length  - 1)))
                            {
                                if (Convert.ToInt32(SF.NSubString(tmpLabel2.Text,0,tmpLabel2.Text.Length  - 1)) < 70)
                                {
                                    tmpLabel2.ForeColor = Color.Red;
                                    tmpLabel2.Font.Bold = true;
                                }
                                else
                                {
                                    tmpLabel2.ForeColor = Color.Black;
                                }
                            }
                        }
                    }
                }
            }
        }


    }

}