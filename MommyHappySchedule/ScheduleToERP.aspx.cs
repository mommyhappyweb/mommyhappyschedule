﻿using Models.Library;
using Models.Library.Excel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MommyHappySchedule
{
    public partial class ScheduleToERP : System.Web.UI.Page

    {
        //private System.Configuration.Configuration rootWebConfig;
        //private System.Configuration.ConnectionStringSettings connString, connString1;

        private System.Data.DataTable dt;
        private System.Data.DataRow dr;
        //private bool NewData;
        private System.Data.DataSet MyDataSet = new System.Data.DataSet();
        private System.Data.SqlClient.SqlDataAdapter MyCommand;
        /// <summary>是否為測試機</summary>
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        System.Data.SqlClient.SqlConnection INConnection1 = new System.Data.SqlClient.SqlConnection(ConnString);
        private System.Data.SqlClient.SqlCommand INCommand;
        private System.Data.SqlClient.SqlDataReader SQLReader;
        private string INString;
        private   string BranchERPName, BranchSTime, BranchETime;
        ShareFunction SF = new ShareFunction();

        protected void Page_Load(object sender, System.EventArgs e)
        {

            // If Session("account") = "" Or Session("Username") <> "mommyhappy" Then
            // Session("msg") = "您沒有使用此程式的權限!!"
            // Response.Redirect("Index.aspx")
            // End If

            if (!IsPostBack)
            {
                MyDataSet = new System.Data.DataSet();
                MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Branch_Data where ID > 0 ", INConnection1);
                MyCommand.Fill(MyDataSet, "Branch");

                Branch.DataSource = MyDataSet.Tables["Branch"].DefaultView;
                Branch.DataTextField = "Branch_Name";
                Branch.DataValueField = "ID";
                Branch.DataBind();

                Check_Year.SelectedValue = DateTime.Now.Year.ToString();
                Check_Month.SelectedValue = DateTime.Now.Month.ToString();
            }
        }

        protected void Check_Report_Click(object sender, EventArgs e)
        {
            int Start_Branch, End_Branch;
            int Ki;

            Session["dt"] = null;
            Session["CS"] = null;
            Session["ds"] = null;
            Session["Pa"] = 0;

            TextBox1.Text = "0";
            //TextBox2.Text = "0";
            //TextBox3.Text = "0";
            //TextBox4.Text = "0";
            //Label3.Text = "";
            //Label4.Text = "";

            Label2.Text = DateTime.Now.ToLongTimeString();
            if (Branch.SelectedIndex == 0)
            {
                Start_Branch = 1;
                End_Branch = 10;
            }
            else
            {
                Start_Branch = Branch.SelectedIndex;
                End_Branch = Branch.SelectedIndex;
            }

            string Select_Banch;
            if (Branch.SelectedItem.Text.Trim() == "南高雄")
                Select_Banch = " (Branch_Name ='南高雄' or Branch_Name= '屏東')";
            else
                Select_Banch = " Branch_Name ='" + Branch.SelectedItem.Text.Trim() + "'";



            MyDataSet = new System.Data.DataSet();
            //找出可以相互支援的駐點名單 以  ,駐點編號, 方式判斷
            string Support_Branch = "";
            MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT ID,Branch_Name FROM Branch_Data where Support_Branch like '%," + Branch.SelectedValue.ToString().Trim() + ",%'", INConnection1);
            MyCommand.Fill(MyDataSet, "Support_Branch");

            foreach (DataRowView Si in MyDataSet.Tables["Support_Branch"].DefaultView)
                Support_Branch = Support_Branch + "'" + Si[1].ToString().Trim() + "',";
            Support_Branch = "Branch_Name in (" + Support_Branch.Substring(0, Support_Branch.Length - 1) + ")";

            string EndDate = Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/01";
            for (Ki = 1; Ki <= 31; Ki++)
            {
                ShareFunction SF = new ShareFunction();
                if (!SF.IsDate(Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/" + Ki.ToString()))
                {
                    EndDate = Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/" + (Ki - 1).ToString();
                    break;
                }
            }
            // 找出尚未離職的管家
            MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT Staff_No,Staff_Name,Job_Title,Join_Date FROM Staff_Job_Data_View where (Depart_Date is null or  Depart_Date >= '" + EndDate  + "') and " + Select_Banch + " and (not (Job_Title like '%專%' or Job_Title like '%備%' or Job_Title like '%理%' ))  Order by Staff_No", INConnection1);
            MyCommand.Fill(MyDataSet, "Staff_Nu");
            // 非客人請假的工作排程及沒有解
            MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Cust_Job_Schedule_View where  (Year(Job_Date) = " + Check_Year.SelectedValue + " and Month(Job_Date) = " + Check_Month.SelectedValue + ") and " + Support_Branch + " and Staff_No is not null and Staff_No <> '' and ( Rtrim(Job_Result) <> '客' or Job_Result is null) and Job_Flag <> 255 Order by Staff_No,Job_Date", INConnection1);
            MyCommand.Fill(MyDataSet, "Job_Schedule");
            // 管家請假的工作排程
            MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Cust_Job_Schedule_View where  (Year(Job_Date) = " + Check_Year.SelectedValue + " and Month(Job_Date) = " + Check_Month.SelectedValue + ")  and Staff_No is not null and Staff_No <> '' and  Rtrim(Job_Result) in ('管','事','病','特','公','訓','停') and ( Cust_No is null or Cust_No = '' ) Order by Staff_No,Job_Date", INConnection1);
            MyCommand.Fill(MyDataSet, "Day_Off_Schedule");
            // 未安排管家的工作排程
            MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Cust_Job_Schedule_View where  (Year(Job_Date) = " + Check_Year.SelectedValue + " and Month(Job_Date) = " + Check_Month.SelectedValue + ") and " + Select_Banch + " and (Staff_No is null or Staff_No = '') and Job_Flag <> 255 Order by Job_Date,Cust_No", INConnection1);
            MyCommand.Fill(MyDataSet, "No_Staff_Job_Schedule");
            // 客人請假的工作排程
            MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Cust_Job_Schedule_View where  (Year(Job_Date) = " + Check_Year.SelectedValue + " and Month(Job_Date) = " + Check_Month.SelectedValue + ") and " + Select_Banch + " and Staff_No is not null and Staff_No <> '' and Rtrim(Job_Result) = '客'  Order by Staff_No,Job_Date", INConnection1);
            MyCommand.Fill(MyDataSet, "Cust_Off_Job_Schedule");
            // 
            int RCount = 1;
            //string HK_Name = ".";
            //string HKU = ".";
            //string Sheet_Name = ".";
            //string Sheet_Name_1 = ".";

            dt = new System.Data.DataTable();
            dt.Columns.Add(new System.Data.DataColumn("ID", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("HK_Name", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("Day_ID", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B3", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B4", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B5", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B6", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B7", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B8", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B9", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B10", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B11", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B12", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B13", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B14", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B15", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B16", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B17", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B18", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B19", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B20", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B21", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B22", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B23", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B24", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B25", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B26", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B27", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B28", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B29", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B30", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B31", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("RN", typeof(int)));
            dt.Columns.Add(new System.Data.DataColumn("HKN", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("JoinDate", typeof(string)));


            //加班資料表
            System.Data.DataTable dt1 = new System.Data.DataTable();
            dt1.Columns.Add(new System.Data.DataColumn("T1", typeof(string)));    //客戶編號
            dt1.Columns.Add(new System.Data.DataColumn("T2", typeof(string)));    //日期
            dt1.Columns.Add(new System.Data.DataColumn("T3", typeof(string)));    //時段
            dt1.Columns.Add(new System.Data.DataColumn("T4", typeof(string)));    //員工編號
            dt1.Columns.Add(new System.Data.DataColumn("T5", typeof(string)));    //員工姓名


            //請假資料表
            System.Data.DataTable dt2 = new System.Data.DataTable();
            dt2.Columns.Add(new System.Data.DataColumn("T1", typeof(string)));    //假別
            dt2.Columns.Add(new System.Data.DataColumn("T2", typeof(string)));    //日期
            dt2.Columns.Add(new System.Data.DataColumn("T3", typeof(string)));    //時段
            dt2.Columns.Add(new System.Data.DataColumn("T4", typeof(string)));    //員工編號
            dt2.Columns.Add(new System.Data.DataColumn("T5", typeof(string)));    //員工姓名

            System.Data.DataRow dr1, dr2;

            //假日及星期六日等日期字串
            string HDString = "";
            INString = "SELECT  H_Date ,H_Memo FROM Holiday_Date where Year(H_Date) = " + Check_Year.SelectedValue.ToString() + " and Month(H_Date) = " +
                      Check_Month.SelectedValue.ToString();
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            while (SQLReader.Read() == true)
            {
                HDString = HDString + "[" + Convert.ToDateTime(SQLReader[0]).ToShortDateString() + "]";
            }
            INCommand.Connection.Close();

            for (var ii = 1; ii <= 31; ii++)
            {
                string TmpString = Check_Year.SelectedItem.Text + "/" + (Check_Month.SelectedIndex + 1).ToString() + "/" + (ii).ToString();
                if (SF.IsDate(TmpString))       //如果是周末日
                    if (System.Convert.ToDateTime(TmpString).DayOfWeek == DayOfWeek.Sunday | System.Convert.ToDateTime(TmpString).DayOfWeek == DayOfWeek.Saturday)
                        HDString = HDString + "[" + Convert.ToDateTime(TmpString).ToShortDateString() + "]";
            }


            for (var ii = 0; ii <= MyDataSet.Tables["Staff_Nu"].DefaultView.Count - 1; ii++)   // 每個管家
            {
                for (Ki = 0; Ki <= 1; Ki++)   // 上午及下午班別
                {
                    dr = dt.NewRow();
                    dr[34] = ii * 2 + Ki;   // 流水號記錄號
                    dr[0] = MyDataSet.Tables["Staff_Nu"].DefaultView[ii][2].ToString().Substring(0, 2);  // Job Title
                    dr[1] = MyDataSet.Tables["Staff_Nu"].DefaultView[ii][1].ToString() + " " + MyDataSet.Tables["Staff_Nu"].DefaultView[ii][0].ToString();  // Staff No & Staff Name
                    dr[35] = MyDataSet.Tables["Staff_Nu"].DefaultView[ii][0].ToString();
                    dr[36] = MyDataSet.Tables["Staff_Nu"].DefaultView[ii][3].ToString();
                    dr[2] = (Ki == 0) ? "上" : "下";

                    DataView dv = new DataView(MyDataSet.Tables["Job_Schedule"])
                    {
                        RowFilter = "Staff_No = '" + MyDataSet.Tables["Staff_Nu"].DefaultView[ii][0].ToString() + "' and Job_Time = " + Ki
                    };
                    for (RCount = 0; RCount <= dv.Count - 1; RCount++)
                    {

                        //找出假日加班的次數儲存在 dt1 中
                        string TmpString = "[" + Convert.ToDateTime(dv[RCount][1]).ToShortDateString() + "]";
                        if (HDString.Contains(TmpString) == true)
                        {
                            dr1 = dt1.NewRow();
                            dr1[0] = dv[RCount][4];   // 客戶編號
                            dr1[1] = TmpString;       // 日期
                            dr1[2] = Ki;              // 時段
                            dr1[3] = dv[RCount][1].ToString().Split(' ')[1];
                            dr1[4] = dv[RCount][1].ToString().Split(' ')[0];
                            dt1.Rows.Add(dr1);
                        }
                        else
                        {
                            int tmpDay = Convert.ToDateTime(dv[RCount][1]).Day; // 找出資料的日期
                            dr[2 + tmpDay] = dv[RCount][4];   // 客戶編號

                        }
                    }
                    // 管家請假排班記錄
                    dv = null;
                    dv = new DataView(MyDataSet.Tables["Day_Off_Schedule"])
                    {
                        RowFilter = "Staff_No = '" + MyDataSet.Tables["Staff_Nu"].DefaultView[ii][0].ToString() + "' and Job_Time = " + Ki
                    };
                    for (RCount = 0; RCount <= dv.Count - 1; RCount++)
                    {
                        int tmpDay = Convert.ToDateTime(dv[RCount][1]).Day; // 找出資料的日期
                        dr[2 + tmpDay] = DBNull.Value.Equals(dv[RCount][5]) ? "假" : dv[RCount][5].ToString().Trim();   // 假別
                    }
                    dt.Rows.Add(dr);
                }
                for (Ki = 0; Ki <= 1; Ki++)   // 將加班插入請假中
                {
                    dr = dt.Rows[dt.Rows.Count - 2 + Ki];    //移到最後二筆中
                    for (var Ai = 1; Ai <= 31; Ai++)
                    {
                        string TmpString = Check_Year.SelectedItem.Text + "/" + (Check_Month.SelectedIndex + 1).ToString() + "/" + (Ai).ToString();
                        if (SF.IsDate(TmpString))
                        {
                            if (HDString.Contains("[" + Convert.ToDateTime(TmpString).ToShortDateString() + "]") == false)    //如果不是周末日或假日
                            {
                                //如果是請假
                                if (dr[2 + Ai].ToString().Trim() == "事" | dr[2 + Ai].ToString().Trim() == "病" | dr[2 + Ai].ToString().Trim() == "家" | dr[2 + Ai].ToString().Trim() == "")
                                {
                                    if (dt1.Rows.Count > 0)
                                    {
                                        //用加班取代請假
                                        dr[2 + Ai] = dt1.Rows[0][0];
                                        dt1.Rows[0].Delete();
                                        dt1.AcceptChanges();
                                    }
                                    else   //加班不夠取代請假
                                    {
                                        if (dr[2 + Ai].ToString().Trim() != "")
                                        {
                                            dr2 = dt2.NewRow();                       // 加到請假資料表
                                            dr2[0] = dr[2 + Ai].ToString().Trim();    // 假別
                                            dr2[1] = TmpString;                       // 日期
                                            dr2[2] = Ki;                              // 時段
                                            dr2[3] = dr[1].ToString().Split(' ')[1];  // 員工編號
                                            dr2[4] = dr[1].ToString().Split(' ')[0];  // 員工姓名
                                            dt2.Rows.Add(dr2);
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }

            DataGrid3.DataSource = dt.DefaultView;
            DataGrid3.DataBind();

            // GridView1.DataSource = dt.DefaultView;
            // GridView1.DataBind();

            Session["dt"] = dt;
            Session["dt1"] = dt1;
            Session["dt2"] = dt2;

        }

        public void Check_Sc(object sender, System.EventArgs e)
        {

        }

        public void Show_Sc_Type(object sender, DataGridItemEventArgs e)
        {
            ListItemType objItemType = (ListItemType)e.Item.ItemType;
            Button TempButton;
            string TmpString;
            string INString;
            System.Data.SqlClient.SqlDataReader SQLReader;
            int iCount = 1;


            if (objItemType == ListItemType.Item | objItemType == ListItemType.AlternatingItem)
            {
                for (iCount = 1; iCount <= 31; iCount++)
                {
                    TempButton = (Button)e.Item.FindControl("B" + iCount.ToString());
                    if (TempButton != null)
                    {
                        // 判斷是否為管家請假
                        switch (TempButton.Text.Trim())
                        {
                            case "管":
                            case "病":
                            case "事":
                            case "公":
                            case "特":
                            case "訓":
                                {
                                    TempButton.BackColor = Color.LightGreen;
                                    break;
                                }

                            case "停":
                                {
                                    TempButton.BackColor = Color.LightGoldenrodYellow;
                                    break;
                                }
                        }

                        // 判斷是否為例假日或國定假日
                        TempButton.Width = 75;
                        // TmpString = "2018/01/" + Right(TempButton.ID, Len(TempButton.ID) - 1)
                        TmpString = Check_Year.SelectedItem.Text + "/" + (Check_Month.SelectedIndex + 1).ToString("00") + "/" + (iCount).ToString();
                        // TextBox1.Text = TextBox1.Text + TmpString
                        if (SF.IsDate(TmpString))
                        {
                            INString = "SELECT  H_Date ,H_Memo FROM Holiday_Date where H_Date = '" + TmpString + "'";
                            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                            INCommand.Connection.Open();
                            SQLReader = INCommand.ExecuteReader();

                            if (SQLReader.Read())
                            {
                                TempButton.BackColor = Color.LightPink;
                                TempButton.Width = 75;
                                e.Item.Cells[iCount + 2].BackColor = Color.LightBlue;
                            }
                            else if (System.Convert.ToDateTime(TmpString).DayOfWeek == DayOfWeek.Sunday | System.Convert.ToDateTime(TmpString).DayOfWeek == DayOfWeek.Saturday)
                            {
                                TempButton.BackColor = Color.LightPink;
                                TempButton.Width = 75;
                                e.Item.Cells[iCount + 2].BackColor = Color.LightBlue;
                            }
                            INCommand.Connection.Close();
                        }
                    }
                }
            }
        }

        public void Show_Job_Detail(object sender, DataGridCommandEventArgs e)
        {
        }


        protected void Button5_Click(object sender, EventArgs e)
        {
            System.Data.DataTable dt = (DataTable)Session["dt"];
            System.Data.DataTable dt1 = (DataTable)Session["dt1"];
            System.Data.DataTable dt2 = (DataTable)Session["dt2"];

            //找出EPR系統名稱,起始時間,終止時間
            INString = "SELECT * FROM Branch_Time_Schedule where Schedule_ID = 1 and  Branch_ID = " + Branch.SelectedValue.ToString();
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            if (SQLReader.Read() == true)
            {
                BranchERPName = SQLReader[4].ToString();
                BranchSTime = SQLReader[2].ToString();
                BranchETime = SQLReader[3].ToString();
            }
            INCommand.Connection.Close();



            //滙出班表
            List<string> Thead = new List<string>();
            List<string[]> Tbodys = new List<string[]>();

            Thead.Add("工號");
            Thead.Add("姓名");
            Thead.Add("公司");
            Thead.Add("部門");
            Thead.Add("到職日期");
            Thead.Add("員工狀態");
            Thead.Add("行事曆_所屬公司");
            Thead.Add("行事曆_輪班類型");

            for (var ii = 1; ii <= 31; ii++)
                if (SF.IsDate(Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/" + ii.ToString()))   //如果有該日期
                    Thead.Add(Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/" + ii.ToString());

            for (var ii = 0; ii < dt.Rows.Count; ii += 2)
            {
                string[] Tbody = new string[] {
                dt.Rows[ii][1].ToString().Split(' ')[1],   //員工編號
                dt.Rows[ii][1].ToString().Split(' ')[0],   //員工姓名
                "媽咪樂居家清潔行",
                BranchERPName.Trim (),
                 Convert.ToDateTime(dt.Rows[ii][36]).ToShortDateString (),   //到職日期
                 "正式員工",
                 "媽咪樂集團",
                 "一例一休"};


                for (var jj = 1; jj < 31; jj++)
                {
                    if (SF.IsDate(Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/" + jj.ToString()))   //如果有該日期
                    {
                        string tmpString = "";
                        if (dt.Rows[ii][2 + jj].ToString() != "" && dt.Rows[ii + 1][2 + jj].ToString() != "")  //如果早上下午都有班
                            tmpString = "管家正常班A(" + BranchETime.Trim() + ")";
                        else if (dt.Rows[ii][2 + jj].ToString() != "" && dt.Rows[ii + 1][2 + jj].ToString() == "")  //如果早上有班 下午沒班
                            tmpString = "管家兼職班(12:00)";
                        else if (dt.Rows[ii][2 + jj].ToString() == "" && dt.Rows[ii + 1][2 + jj].ToString() != "")  //如果下午有班 早上沒班
                            tmpString = "管家兼職班(" + BranchETime.Trim() + ")";

                        System.Array.Resize(ref Tbody, Tbody.Length + 1);
                        Tbody[Tbody.Length - 1] = tmpString;
                    }
                }
                Tbodys.Add(Tbody);

            }

            ReportsExcel ReportsExcel2 = new ReportsExcel();
            ReportsExcel2.GetExcel(Thead, Tbodys, "班表", 2003);

        }

        protected void Button6_Click(object sender, EventArgs e)
        {
            System.Data.DataTable dt2 = (DataTable)Session["dt2"];

            //找出EPR系統名稱,起始時間,終止時間
            INString = "SELECT * FROM Branch_Time_Schedule where Schedule_ID = 1 and  Branch_ID = " + Branch.SelectedValue.ToString();
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            if (SQLReader.Read() == true)
            {
                BranchERPName = SQLReader[4].ToString();
                BranchSTime = SQLReader[2].ToString();
                BranchETime = SQLReader[3].ToString();
            }
            INCommand.Connection.Close();


            string Stime = "08:00";
            string Etime = "12:00";

            List<string> Thead = new List<string>();
            List<string[]> Tbodys = new List<string[]>();

            //滙出請假
            if (dt2.Rows.Count > 0)
            {

                Thead.Add("序號");
                Thead.Add("工號");
                Thead.Add("姓名");
                Thead.Add("請假類型");
                Thead.Add("開始日期");
                Thead.Add("結束日期");
                Thead.Add("開始時間");
                Thead.Add("結束時間");
                Thead.Add("請假方式");
                Thead.Add("請假參數");
                Thead.Add("狀態");
                Thead.Add("審核結果");
                Thead.Add("審核人");
                Thead.Add("審核日期");
                Thead.Add("備註");

                for (var ii = 0; ii < dt2.Rows.Count; ii++)
                {
                    Tbodys.Add(
                        new string[]
                        {
                        ii+1.ToString (),
                        dt2.Rows[ii][3].ToString (),
                        dt2.Rows[ii][4].ToString (),
                        dt2.Rows[ii][0].ToString () + "假",
                        Convert.ToDateTime (dt2.Rows[ii][1]).ToShortDateString (),
                        Convert.ToDateTime (dt2.Rows[ii][1]).ToShortDateString (),
                        ((dt2.Rows[ii][2].ToString()=="0") ? Stime  : BranchSTime.Trim()).ToString(), // "08:00"
                        ((dt2.Rows[ii][2].ToString()=="0") ? Etime  : BranchETime.Trim()).ToString(),
                        "1",
                        "1",
                        "待審核",
                        "",
                        "",
                        ""
                        }
                     );
                }

                ReportsExcel ReportsExcel2 = new ReportsExcel();
                ReportsExcel2.GetExcel(Thead, Tbodys, "請假", 2003);

            }

        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            System.Data.DataTable dt1 = (DataTable)Session["dt1"];

            //找出EPR系統名稱,起始時間,終止時間
            INString = "SELECT * FROM Branch_Time_Schedule where Schedule_ID = 1 and  Branch_ID = " + Branch.SelectedValue.ToString();
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            if (SQLReader.Read() == true)
            {
                BranchERPName = SQLReader[4].ToString();
                BranchSTime = SQLReader[2].ToString();
                BranchETime = SQLReader[3].ToString();
            }
            INCommand.Connection.Close();


            List<string> Thead = new List<string>();
            List<string[]> Tbodys = new List<string[]>();

            //滙出加班
            if (dt1.Rows.Count > 0)
            {
                Thead.Add("工號");
                Thead.Add("姓名");
                Thead.Add("加班類型");
                Thead.Add("加班所屬日期");
                Thead.Add("開始日期");
                Thead.Add("結束日期");
                Thead.Add("開始時間");
                Thead.Add("結束時間");
                Thead.Add("加班時數");
                Thead.Add("審核狀態");
                Thead.Add("審核人ID");
                Thead.Add("審核結果ID");
                Thead.Add("審核日期");
                Thead.Add("是否計畫調休");
                Thead.Add("加班種類");
                Thead.Add("備註");

                for (var ii = 0; ii < dt1.Rows.Count; ii++)
                {
                    Tbodys.Add(
                        new string[]
                        {
                        dt1.Rows[ii][3].ToString (),
                        dt1.Rows[ii][4].ToString (),
                        "假日加班",
                        Convert.ToDateTime (dt1.Rows[ii][1]).ToShortDateString (),
                        Convert.ToDateTime (dt1.Rows[ii][1]).ToShortDateString (),
                        Convert.ToDateTime (dt1.Rows[ii][1]).ToShortDateString (),
                        ((dt1.Rows[ii][2].ToString()=="0") ? "08:00" : BranchSTime.Trim()).ToString(),
                        ((dt1.Rows[ii][2].ToString()=="0") ? "12:00" : BranchETime.Trim()).ToString(),
                        "4",
                        "已審核",
                        "00006",
                        "同意",
                        DateTime.Now.ToShortDateString (),
                        "0",
                        "加班參數",
                        "Excel導入"
                        }
                     );
                }

                ReportsExcel ReportsExcel3 = new ReportsExcel();
                ReportsExcel3.GetExcel(Thead, Tbodys, "加班", 2003);

            }

        }
    }

}