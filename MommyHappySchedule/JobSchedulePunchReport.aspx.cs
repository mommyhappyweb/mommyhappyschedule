﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Data;
using System.Drawing;
using System.Configuration;
using Models.Library;
using Models.Library.Excel;

namespace MommyHappySchedule
{
    public partial class JobSchedulePunchReport : System.Web.UI.Page
    {
        private System.Data.DataSet MyDataSet = new System.Data.DataSet();
        private System.Data.SqlClient.SqlDataAdapter MyCommand;
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        System.Data.SqlClient.SqlConnection INConnection1 = new System.Data.SqlClient.SqlConnection(ConnString);
        System.Data.SqlClient.SqlConnection INConnection2 = new System.Data.SqlClient.SqlConnection("server=60.248.240.187\\ERPDB,2899;database=MH;uid=sa;pwd=Mh@ht5357");
        System.Data.SqlClient.SqlCommand INCommand, INCommand1;
        System.Data.SqlClient.SqlDataReader SQLReader, SQLReader1;
        //System.Data.DataTable dt1;
        //System.Data.DataRow dr1;


        protected void Page_Load(object sender, System.EventArgs e)
        {

            if ((string)Session["Account"] == "" | (Session["Account"] == null))
            {
                Session["msg"] = "您沒有使用此程式的權限!!";
                Response.Redirect("Index.aspx");
            }


            if (!Page.IsPostBack)
            {
                MyDataSet = new System.Data.DataSet();
                MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Branch_Data where ID > 0 ", INConnection1);
                MyCommand.Fill(MyDataSet, "Branch");


                Branch.DataSource = MyDataSet.Tables["Branch"].DefaultView;
                Branch.DataTextField = "Branch_Name";
                Branch.DataValueField = "ID";
                Branch.DataBind();

                Show_Date.Text = DateTime.Now.ToShortDateString();
                End_Date.Text = DateTime.Now.ToShortDateString();
                RadioButtonList1.SelectedIndex = 0;
            }
        }

        protected void Check_Report_Click(object sender, EventArgs e)
        {
            ShareFunction SF = new ShareFunction();
            if (!SF.IsDate(Show_Date.Text) | !SF.IsDate(End_Date.Text))
                return;


            Label2.Text = DateTime.Now.ToLongTimeString();

            string Select_Banch;
            if (Branch.SelectedItem.Text.Trim() == "南高雄")
                Select_Banch = " and (Branch_Name ='南高雄' or Branch_Name = '屏東')";
            else
                Select_Banch = " and Branch_Name ='" + Branch.SelectedItem.Text.Trim() + "'";


            // 找出尚未離職的管家
            MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                "SELECT Staff_No,Staff_Name,Job_Title FROM Staff_Job_Data_View where (Depart_Date is null or Depart_Date >= '" +
                Show_Date.Text + "') " +
                Select_Banch + " and Staff_No <> '' and Staff_No is not null  Order by Staff_No", INConnection1);
            MyCommand.Fill(MyDataSet, "Staff_Nu");
            string Select_Staff = "";
            foreach (DataRowView Si in MyDataSet.Tables["Staff_Nu"].DefaultView)
                Select_Staff = Select_Staff + "'" + Si[0].ToString().Trim() + "',";
            if (Select_Staff.Length > 0)
                Select_Staff = " and Staff_No in (" + Select_Staff.Substring(0, Select_Staff.Length - 1) + ")";

            if (TextBox6.Text != "")
            {
                Select_Banch = " and (Staff_Name like '%" + TextBox6.Text + "%' or Staff_No like '%" + TextBox6.Text + "%') ";
                Select_Staff = " and (Staff_Name like '%" + TextBox6.Text + "%' or Staff_No like '%" + TextBox6.Text + "%') ";
                //以下是針對駐點內尋找
                //Select_Banch = Select_Banch + " and (Staff_Name like '%" + TextBox6.Text + "%' or Staff_No like '%" + TextBox6.Text + "%') ";
                //Select_Staff = Select_Staff + " and (Staff_Name like '%" + TextBox6.Text + "%' or Staff_No like '%" + TextBox6.Text + "%') ";

            }

            if (TextBox7.Text != "")
            {
                Select_Banch = " and ( Cust_Name like '%" + TextBox7.Text + "%' or Cust_No like '%" + TextBox7.Text + "%' )";
                Select_Staff = " and ( Cust_Name like '%" + TextBox7.Text + "%' or Cust_No like '%" + TextBox7.Text + "%' )";
                //以下是針對駐點內尋找
                //Select_Banch = Select_Banch + " and Cust_Name like '%" + TextBox7.Text + "%' or Cust_No like '%" + TextBox7.Text + "%' ";
                //Select_Staff = Select_Staff + " and Cust_Name like '%" + TextBox7.Text + "%' or Cust_No like '%" + TextBox7.Text + "%' ";

            }


            switch (RadioButtonList1.SelectedIndex)
            {
                default:
                case 0:
                    {
                        Select_Banch = Select_Banch + " and Job_Flag <> 255";
                        Select_Staff = Select_Staff + " and Job_Flag <> 255";
                        break;
                    }

                case 1:
                    {
                        Select_Banch = Select_Banch + " and Job_Flag = 4 ";
                        Select_Staff = Select_Staff + " and Job_Flag = 4 ";
                        break;
                    }
                case 2:
                    {
                        Select_Banch = Select_Banch + " and Job_Flag = 3 ";
                        Select_Staff = Select_Staff + " and Job_Flag = 3 ";
                        break;
                    }

                case 3:
                    {
                        Select_Banch = Select_Banch + " and Job_Flag = 2 ";
                        Select_Staff = Select_Staff + " and Job_Flag = 2 ";
                        break;
                    }
                case 4:
                    {
                        Select_Banch = Select_Banch + " and (R_In_ID <> 0 or R_Out_ID <> 0) ";
                        Select_Staff = Select_Staff + " and (R_In_ID <> 0 or R_Out_ID <> 0) ";
                        break;
                    }
            }


            MyDataSet = new System.Data.DataSet();
            //配合可以跨區支援,改判斷管家編號並顯示工作
            if (Select_Staff.Length > 0)
                MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                            "SELECT * FROM Job_Schedule_Punch_Detail where Job_Date >= '" + Show_Date.Text + "' and Job_Date <= '" + End_Date.Text + "' " +
                            Select_Staff + "  Order by Staff_No,Job_Date,Job_Time,Cust_No", INConnection1);
            else
                MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                            "SELECT * FROM Job_Schedule_Punch_Detail where Job_Date >= '" + Show_Date.Text + "' and Job_Date <= '" + End_Date.Text +
                            "' " + Select_Banch + "  Order by Staff_No,Job_Date,Job_Time,Cust_No", INConnection1);

            MyCommand.Fill(MyDataSet, "Job_Schedule_Punch_Detail");

            //加入公-督-訓的打卡記錄
            if (Select_Staff.Length > 0)
                MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                            "SELECT * FROM Cust_Job_Schedule_Supervision_View where Job_Date >= '" + Show_Date.Text + "' and Job_Date <= '" + End_Date.Text + "' " +
                            Select_Staff + "  Order by Staff_No,Job_Date,Job_Time,Cust_No", INConnection1);
            else
                MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                            "SELECT * FROM Cust_Job_Schedule_Supervision_View where Job_Date >= '" + Show_Date.Text + "' and Job_Date <= '" + End_Date.Text +
                            "' " + Select_Banch + "  Order by Staff_No,Job_Date,Job_Time,Cust_No", INConnection1);

            MyCommand.Fill(MyDataSet, "Job_Schedule_Punch_Detail");
            MyDataSet.Tables["Job_Schedule_Punch_Detail"].DefaultView.Sort = "Staff_No Asc,Job_Date Asc,Job_Time Asc,Cust_No Asc";

            DataGrid1.DataSource = MyDataSet.Tables["Job_Schedule_Punch_Detail"].DefaultView;
            DataGrid1.DataBind();

            Label2.Text = Label2.Text + "-" + DateTime.Now.ToLongTimeString();
        }

        public void Show_Sc_Type(object sender, DataGridItemEventArgs e)
        {
            ListItemType objItemType = (ListItemType)e.Item.ItemType;
            Label TmpLabel;
            RadioButtonList tmpRadioButtonList;


            if (objItemType == ListItemType.Item | objItemType == ListItemType.AlternatingItem)
            {
                TmpLabel = (Label)e.Item.FindControl("Label3");
                tmpRadioButtonList = (RadioButtonList)e.Item.FindControl("RadioButtonList3");

                switch (e.Item.Cells[18].Text)
                {
                    case "0":
                        {
                            TmpLabel.Text = "未繳費";
                            //tmpRadioButtonList.Enabled = false;    //先暫時不用
                            break;
                        }

                    case "1":
                        {
                            TmpLabel.Text = "可排班";
                            //tmpRadioButtonList.Enabled = true;     //先暫時不用
                            break;
                        }

                    case "2":
                        {
                            TmpLabel.Text = "已確認";
                            //tmpRadioButtonList.Enabled = true;     //先暫時不用
                            break;
                        }

                    case "3":
                        {
                            TmpLabel.Text = "服務中";
                            TmpLabel.BackColor = Color.LightGreen;
                            //tmpRadioButtonList.Enabled = false ;   //先暫時不用
                            break;
                        }
                    case "4":
                        {
                            TmpLabel.Text = "已服務";
                            TmpLabel.BackColor = Color.LightPink;
                            //tmpRadioButtonList.Enabled = false ;   //先暫時不用
                            break;
                        }
                }
            }
        }

        public void Show_Job_Detail(object sender, DataGridCommandEventArgs e)
        {
        }


        protected void Change_Screen(object sender, EventArgs e)
        {
            if (((Control)sender).ID == "Left_Screen")
            {
                Show_Date.Text = Convert.ToDateTime(Show_Date.Text).AddDays(-1).ToShortDateString();
                End_Date.Text = Convert.ToDateTime(End_Date.Text).AddDays(-1).ToShortDateString();
            }
            if (((Control)sender).ID == "Right_Screen")
            {
                Show_Date.Text = Convert.ToDateTime(Show_Date.Text).AddDays(1).ToShortDateString();
                End_Date.Text = Convert.ToDateTime(End_Date.Text).AddDays(1).ToShortDateString();
            }

            RadioButtonList2.ClearSelection();
            RadioButtonList1.ClearSelection();

            Check_Report_Click(sender, e);
        }


        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            RadioButtonList2.ClearSelection();
            Check_Report_Click(sender, e);
        }


        protected void Button6_Click(object sender, EventArgs e)
        {
            TextBox6.Text = "";
            TextBox7.Text = "";
            Show_Date.Text = DateTime.Now.ToShortDateString();
            End_Date.Text = DateTime.Now.ToShortDateString();
            DataGrid1.Dispose();
        }
        protected void RadioButtonList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            RadioButtonList tmpRadioButtonList;
            int Gi;

            for (Gi = 0; Gi <= DataGrid1.Items.Count - 1; Gi++)
            {
                tmpRadioButtonList = (RadioButtonList)DataGrid1.Items[Gi].FindControl("RadioButtonList3");
                if (tmpRadioButtonList != null)
                    if (DataGrid1.Items[Gi].Cells[13].Text == "1" | DataGrid1.Items[Gi].Cells[13].Text == "2" | DataGrid1.Items[Gi].Cells[13].Text == "0")
                        tmpRadioButtonList.SelectedIndex = RadioButtonList2.SelectedIndex;
            }

            Page_Load(sender, e);
        }

        protected void ExportToExcel_Click(object sender, EventArgs e)
        {
            if ((string)Session["Groups"] != "3" & (string)Session["Groups"] != "4" & (string)Session["Groups"] != "5")
            {
                Response.Write("<script language=JavaScript>alert('無此權限!請洽IT部門');</script>");
                return;
            }
            List<string> Thead = new List<string>();
            List<string[]> Tbodys = new List<string[]>();

            //滙出請假
            if (DataGrid1.Items.Count > 0)
            {
                Thead.Add("序號");
                Thead.Add("駐點");
                Thead.Add("工號");
                Thead.Add("姓名");
                Thead.Add("日期");
                Thead.Add("起始時間");
                Thead.Add("上班時間");
                Thead.Add("上班異常");
                Thead.Add("異常描述");
                Thead.Add("終止時間");
                Thead.Add("下班時間");
                Thead.Add("下班異常");
                Thead.Add("異常描述");
                Thead.Add("狀態");
                Thead.Add("服務情況");

                for (var ii = 0; ii < DataGrid1.Items.Count; ii++)
                {
                    Tbodys.Add(
                        new string[]
                        {
                        ii+1.ToString (),                                   //流水號
                        DataGrid1.Items[ii].Cells[1].Text.Trim(),           //駐點
                        DataGrid1.Items[ii].Cells[2].Text.Trim(),           //管家編號
                        DataGrid1.Items[ii].Cells[3].Text.Trim(),           //管家姓名
                        DataGrid1.Items[ii].Cells[7].Text.Trim(),           //日期
                        DataGrid1.Items[ii].Cells[8].Text.Trim(),           //起始時間
                        DataGrid1.Items[ii].Cells[9].Text.Trim() == "&nbsp;" ? "" : DataGrid1.Items[ii].Cells[9].Text.Trim() ,           //上班時間
                        DataGrid1.Items[ii].Cells[10].Text.Trim() == "&nbsp;" ? "" : DataGrid1.Items[ii].Cells[10].Text.Trim() ,          //上班異常
                        DataGrid1.Items[ii].Cells[11].Text.Trim() == "&nbsp;" ? "" : DataGrid1.Items[ii].Cells[11].Text.Trim() ,          //異常描述
                        DataGrid1.Items[ii].Cells[12].Text.Trim() == "&nbsp;" ? "" : DataGrid1.Items[ii].Cells[12].Text.Trim() ,          //終止時間
                        DataGrid1.Items[ii].Cells[13].Text.Trim() == "&nbsp;" ? "" : DataGrid1.Items[ii].Cells[13].Text.Trim() ,          //下班時間
                        DataGrid1.Items[ii].Cells[14].Text.Trim() == "&nbsp;" ? "" : DataGrid1.Items[ii].Cells[14].Text.Trim() ,          //下班異常
                        DataGrid1.Items[ii].Cells[15].Text.Trim() == "&nbsp;" ? "" : DataGrid1.Items[ii].Cells[15].Text.Trim() ,          //異常描述
                        DataGrid1.Items[ii].Cells[16].Text.Trim() == "&nbsp;" ? "" : DataGrid1.Items[ii].Cells[16].Text.Trim() ,          //狀態
                        DataGrid1.Items[ii].Cells[17].Text.Trim() == "&nbsp;" ? "" : DataGrid1.Items[ii].Cells[17].Text.Trim()            //服務情況
                       }
                    );
                }

                ReportsExcel ReportsExcel = new ReportsExcel();
                ReportsExcel.GetExcel(Thead, Tbodys, "Excel", 2003);

            }
        }

        protected void ExportToERP_Click(object sender, EventArgs e)
        {
            if ((string)Session["Groups"] != "3" & (string)Session["Groups"] != "4" & (string)Session["Groups"] != "5")
            {
                Response.Write("<script language=JavaScript>alert('無此權限!請洽IT部門');</script>");
                return;
            }
            string[] FileArray2 = { "2303", "2301", "2304", "2302", "2201", "2202", "2102", "2101", "2104", "2103", "2306" };
            ShareFunction SF = new ShareFunction();
            int BID = 0;

            string Select_Banch;
            if (Branch.SelectedItem.Text.Trim() == "南高雄")
                Select_Banch = " (Branch_Name ='南高雄' or Branch_Name = '屏東')";
            else
                Select_Banch = " Branch_Name ='" + Branch.SelectedItem.Text.Trim() + "'";

            Select_Banch = Select_Banch + " and Job_Flag = 4 ";

            MyDataSet = new System.Data.DataSet();
            //配合可以跨區支援,改判斷管家編號並顯示工作
            string INString = "SELECT ID,Job_Date,Job_Time,Staff_No,Cust_No,Job_Price,Job_Flag,Job_Status,HK_Serial,Contract_ID," +
                              "Job_Result_ID,Charege_Staff_No,Export_ID,ERP_No,Branch_Name,Branch_ID,C_Tax_Flag FROM Job_Schedule_To_ERP " +
                              " where Job_Date >= '" + Show_Date.Text + "' and Job_Date <= '" + End_Date.Text +
                              "' and " + Select_Banch + "  Order by Cust_No,Job_Date,Job_Time";
            INCommand1 = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
            INCommand1.Connection.Close();
            INCommand1.Connection.Open();
            SQLReader1 = INCommand1.ExecuteReader();

            while (SQLReader1.Read())
            {
                //先判斷是否有重覆匯入利用 Job_Schedule 的 ID欄位
                INString = "Select TG001 from COPTG where TG001 = '2301' and TG020 like 'ID:" + SQLReader1[0] + "%'";
                INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
                INCommand.Connection.Close();
                INCommand.Connection.Open();
                SQLReader = INCommand.ExecuteReader();
                //如果有找到資料則跳過
                if (SQLReader.Read() == true)
                {
                    INCommand.Connection.Close();
                    continue;
                }
                else
                {
                    INCommand.Connection.Close();

                    BID = Convert.ToInt16(SQLReader1[15]) - 1;
                    Boolean TaxCheck = false;
                    if (!DBNull.Value.Equals(SQLReader1[16]))
                        if (Convert.ToInt16(SQLReader1[16]) == 1)
                            TaxCheck = true;
                    //找出最後一筆銷貨單記錄編號
                    string tmpCNo = Convert.ToDateTime(SQLReader1[1]).ToString("yyyyMMdd");
                    INString = "Select top 1 TG002 from COPTG where TG001 = '2301' and TG002 like '" + tmpCNo + "%' Order by TG002 DESC ";
                    INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
                    INCommand.Connection.Close();
                    INCommand.Connection.Open();
                    SQLReader = INCommand.ExecuteReader();
                    //如果有找到資料
                    if (SQLReader.Read() == true)
                        tmpCNo = SF.NSubString(SQLReader[0].ToString(), 0, 8) + (Convert.ToInt32(SF.NSubString(SQLReader[0].ToString(), 8, 3)) + 1).ToString("000");
                    else
                        tmpCNo = tmpCNo + "001";

                    INCommand.Connection.Close();
                    //銷貨單頭
                    INString = "Insert into COPTG (COMPANY,CREATOR,CREATE_DATE,FLAG,CREATE_TIME,TG001,TG002,TG003,TG004,TG005,TG006,TG007," +
                               "TG010,TG011,TG012,TG013,TG015,TG016,TG017,TG020,TG022,TG023,TG024,TG025,TG026,TG027,TG028,TG029,TG030,TG031," +
                               "TG032,TG033,TG034,TG035,TG036,TG037,TG041,TG042,TG044,TG045,TG046,TG048,TG049,TG050,TG051,TG052,TG053,TG054," +
                               "TG055,TG056,TG059,TG061,TG062,TG063,TG068,TG069,TG070,TG071,TG072,TG082,TG089,TG094,TG097,TG099,TG100,TG101," +
                               "TG111) Values ('MH','DS','" + DateTime.Now.ToString("yyyyMMdd") + "',1,'" + DateTime.Now.ToString("HH:mm:ss") +
                               "','2301','" + tmpCNo + "','" + SF.NSubString(tmpCNo, 0, 8) + "','" + SQLReader1[4].ToString().Trim() +
                               "','" + FileArray2[BID] + "','" + SQLReader1[11].ToString().Trim() + "','','0000','NT$',1, " +
                               Convert.ToInt16(SQLReader1[5]) + ",'','','" + (TaxCheck ? "2" : "9") + "','ID:" + SQLReader1[0] +
                               "',0,'N','N'," + (TaxCheck ? Convert.ToInt16(SQLReader1[5]) * 0.05 : 0).ToString() +
                               ",'" + SQLReader1[11].ToString().Trim() + "','','','','N','1',0,1,'N','" + SQLReader1[3].ToString().Trim() +
                               "','N','N',0,'" +
                               SF.NSubString(tmpCNo, 0, 8) + "'," + (TaxCheck ? "0.05" : "0.00") + "," + Convert.ToInt16(SQLReader1[5]) +
                               "," + (TaxCheck ? Convert.ToInt16(SQLReader1[5]) * 0.05 : 0).ToString() + ",'2201','" +
                               SQLReader1[12].ToString() + "','6401','" + SQLReader1[13].ToString() + "'," + Convert.ToInt16(SQLReader1[5]) +
                               "," + (TaxCheck ? Convert.ToInt16(SQLReader1[5]) * 0.05 : 0).ToString() + ",0,'N','N','N','N',0,0,'1',0,'N'" +
                               ",0,'5','1','N','" + (TaxCheck ? "S21" : "S17") + "','N',1,'N',0,'0')";
                    INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
                    INCommand.Connection.Open();
                    INCommand.ExecuteNonQuery();
                    INCommand.Connection.Close();

                    //銷貨單身   
                    //string tmpNumber = "000" + tmpID.Text.Trim();  
                    //tmpNumber = tmpNumber.Substring(tmpNumber.Length - 4, 4);
                    string tmpNumber = "0001";

                    //如果是屏東,則入庫南高
                    string DN = (SF.NSubString(SQLReader1[4].ToString(), 0, 2) == "PT") ? "KN" : SF.NSubString(SQLReader1[4].ToString(), 0, 2);

                    INString = "Insert into COPTH (COMPANY,CREATOR,CREATE_DATE,FLAG,CREATE_TIME,TH001,TH002,TH003,TH004,TH007,TH008,TH009," +
                               "TH012,TH013,TH014,TH015,TH016,TH018,TH020,TH021,TH024,TH025,TH026,TH031,TH035,TH036,TH037,TH038,TH039,TH040," +
                               "TH042,TH043,TH044,TH045,TH046,TH047,TH048,TH049,TH050,TH052,TH053,TH054,TH057,TH061,TH062,TH068,TH069,TH071," +
                               "TH072,TH073,TH080,TH091)" +
                               " Values ('MH','DS','" + DateTime.Now.ToString("yyyyMMdd") + "',1,'" + DateTime.Now.ToString("HH:mm:ss") +
                               "','2301','" + tmpCNo + "','" + tmpNumber + "','AA0101001','" + DN + "',1,'件'," +
                               Convert.ToInt16(SQLReader1[5]) + "," + Convert.ToInt16(SQLReader1[5]) + ",'2201','" +
                               SQLReader1[12].ToString() + "','0001','ID:" + SQLReader1[0] + "','N','N',0,1,'N','1'," +
                               Convert.ToInt16(SQLReader1[5]) + "," + (TaxCheck ? Convert.ToInt16(SQLReader1[5]) * 0.05 : 0).ToString() +
                               "," + Convert.ToInt16(SQLReader1[5]) + "," + (TaxCheck ? Convert.ToInt16(SQLReader1[5]) * 0.05 : 0).ToString() +
                               ",0,0,'N',0,0,0,0,0,0,0,0,0,0,'N',0,1,'件','N',0,0,0," + (TaxCheck ? "0.05" : "0.00") + ",0,0)";

                    INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
                    INCommand.Connection.Open();
                    INCommand.ExecuteNonQuery();
                    INCommand.Connection.Close();
                }

            }
            Response.Write("<script language=JavaScript>alert('轉入ERP作業完成!!');</script>");
        }
    }
}