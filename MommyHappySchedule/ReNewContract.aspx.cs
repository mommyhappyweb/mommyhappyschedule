﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Drawing;
using System.Configuration;
using System.Data;
using Models.Library;

namespace MommyHappySchedule
{
    public partial class ReNewContract : System.Web.UI.Page
    {

        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        private static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        private System.Data.SqlClient.SqlConnection INConnection1 = new System.Data.SqlClient.SqlConnection(ConnString);
        private System.Data.SqlClient.SqlCommand INCommand;
        private System.Data.SqlClient.SqlDataReader SQLReader;
        private System.Data.DataSet MyDataSet;
        private System.Data.SqlClient.SqlDataAdapter MyCommand;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            //if ((string)Session["account"] == "" | (string)Session["Username"] != "mommyhappy")
            //{
            //    Session["msg"] = "您沒有使用此程式的權限!!";
            //    Response.Redirect("Index.aspx");
            //}

            if (!Page.IsPostBack)
            {
                MyDataSet = new System.Data.DataSet();
                MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Branch_Data", INConnection1);
                MyCommand.Fill(MyDataSet, "Branch");

                Branch.DataSource = MyDataSet.Tables["Branch"].DefaultView;
                Branch.DataTextField = "Branch_Name";
                Branch.DataValueField = "ID";
                Branch.DataBind();

                Check_Year.SelectedValue = DateTime.Now.Year.ToString();
                Check_Month.SelectedValue = DateTime.Now.Month.ToString();
            }
        }


        protected void Check_Report_Click(object sender, EventArgs e)
        {
            string Select_Banch;
            string Se_String = "";

            MyDataSet = new System.Data.DataSet();
            if (Branch.SelectedIndex == 0)
                Select_Banch = "Branch_Name is not null";
            else if ((Branch.Items[Branch.SelectedIndex].Text.Trim()) == "南高雄")
                Select_Banch = "(Branch_Name = '" + (Branch.Items[Branch.SelectedIndex].Text.Trim()) + "' or Branch_Name = '屏東')";
            else
                Select_Banch = "Branch_Name = '" + (Branch.Items[Branch.SelectedIndex].Text.Trim()) + "'";// 營業所名稱

            // Label1.Text = Branch.SelectedItem.Text
            if (TextBox3.Text != "")
            {
                MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                    "SELECT * FROM Cust_Contract_Data_View where Cust_No like '%" + TextBox3.Text + "%' or Cust_Name like '%" + TextBox3.Text + "%' and " +
                    Select_Banch + "  Order by Contract_E_Date", INConnection1);
                MyCommand.Fill(MyDataSet, "Cust_Contract");
            }
            else
            {
                // 顯示合約到期資料表
                MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                    "SELECT * FROM Cust_Contract_Expire_View where ( (year(Contract_E_Date) =" + Check_Year.SelectedValue + " and month(Contract_E_Date) = " +
                    Check_Month.SelectedValue + ") or (year(N_Contract_S_Date) =" + Check_Year.SelectedValue + " and month(N_Contract_S_Date) = " +
                    Check_Month.SelectedValue + ") ) and " + Select_Banch + " Order by Branch_Name,Cust_No,Contract_E_Date", INConnection1);
                MyCommand.Fill(MyDataSet, "ReNew_Contract");

                DataGrid2.DataSource = MyDataSet.Tables["ReNew_Contract"].DefaultView;
                DataGrid2.DataBind();

                Se_String = "('0',";
                foreach (DataRowView item in MyDataSet.Tables["ReNew_Contract"].DefaultView)
                    Se_String = Se_String + "'" + item[1].ToString() + "',";// 己經在合約到期資料表的記錄
                Se_String = Se_String.Substring(0, Se_String.Length - 1) + ")";

                MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                    "SELECT * FROM Cust_Contract_Data_View where year(Contract_E_Date) =" + Check_Year.SelectedValue + " and month(Contract_E_Date) = " +
                    Check_Month.SelectedValue + " and " + Select_Banch + " and ID not in " + Se_String + " Order by Branch_Name,Cust_No,Contract_E_Date",
                    INConnection1);
                MyCommand.Fill(MyDataSet, "Cust_Contract");
                //TextBox1.Text = "SELECT * FROM Cust_Contract_Data_View where year(Contract_E_Date) =" + Check_Year.SelectedValue +
                //                " and month(Contract_E_Date) = " + Check_Month.SelectedValue + " and " + Select_Banch + " and ID not in " +
                //                Se_String + " Order by Branch_Name,Cust_No,Contract_E_Date";
            }

            DataGrid1.DataSource = MyDataSet.Tables["Cust_Contract"].DefaultView;
            DataGrid1.DataBind();


            Page_Load(sender, e);
        }

        public void Show_Sc_Type(object sender, DataGridItemEventArgs e)
        {
            ListItemType objItemType = (ListItemType)e.Item.ItemType;
            Label TempLabel;
            string tmpResult;

            if (((Control)sender).ID == "DataGrid2")
                tmpResult = e.Item.Cells[12].Text.Trim();
            else
                tmpResult = e.Item.Cells[8].Text.Trim();

            if (objItemType == ListItemType.Item | objItemType == ListItemType.AlternatingItem)
            {
                TempLabel = (Label)e.Item.FindControl("Contract_FLag");
                switch (tmpResult)
                {
                    case "0":
                        {
                            TempLabel.Text = "";
                            break;
                        }

                    case "1":
                        {
                            TempLabel.Text = "解約";
                            TempLabel.ForeColor = Color.Red;
                            break;
                        }

                    case "2":
                        {
                            TempLabel.Text = "保留";
                            TempLabel.ForeColor = Color.Red;
                            break;
                        }

                    case "3":
                        {
                            TempLabel.ForeColor = Color.Blue;
                            if (Convert.ToDouble(e.Item.Cells[14].Text.Trim())-Convert.ToDouble(e.Item.Cells[7].Text.Trim()) == 0 )
                               TempLabel.Text = "續約";
                            else if(Convert.ToDouble(e.Item.Cells[14].Text.Trim()) - Convert.ToDouble(e.Item.Cells[7].Text.Trim()) > 0)
                                TempLabel.Text = "續約-加次";
                            else
                                TempLabel.Text = "續約-減次";
                            break;
                        }

                    case "4":
                        {
                            TempLabel.Text = "減次";
                            TempLabel.ForeColor = Color.Green;
                            break;
                        }

                    case "5":
                        {
                            TempLabel.Text = "新約";
                            TempLabel.ForeColor = Color.Goldenrod;
                            break;
                        }

                    case "6":
                        {
                            TempLabel.Text = "回流";
                            TempLabel.ForeColor = Color.Goldenrod;
                            break;
                        }
                }
            }
        }
        public void Show_Job_Detail(object sender, DataGridCommandEventArgs e)
        {
            LinkButton tmpLB;
            ID_Label.Text = "";
            CN_Number.Text = "";
            SF_Name.Text = "";
            CT_Number.Text = "";
            EC_Date.Text = "";
            CT_Year.Text = "";
            HK_Number.Text = "";

            tmpLB = (LinkButton)e.CommandSource;
            // Label3.Text = tmpLB.CommandName



            if (tmpLB.CommandName == "客戶編號")
            {
                MultiView1.ActiveViewIndex = 1;
                tmpLB = (LinkButton)e.Item.Cells[2].Controls[0];
                CN_Number.Text = tmpLB.Text;
                SF_Name.Text = e.Item.Cells[4].Text;

                if (((Control)sender).ID == "DataGrid1")
                {
                    CT_Number.Text = e.Item.Cells[6].Text;     //合約次數
                    ID_Label.Text = e.Item.Cells[0].Text;

                    if (e.Item.Cells[8].Text != "&nbsp;")
                    {
                        CT_Result.SelectedIndex = Convert.ToInt32(e.Item.Cells[8].Text);
                        Session["CT"] = e.Item.Cells[8].Text;    // 目前合約狀況
                    }
                    else
                    {
                        CT_Result.SelectedIndex = 0;
                        Session["CT"] = "0";
                    }
                    if (e.Item.Cells[5].Text != "&nbsp;")
                        EC_Date.Text = e.Item.Cells[5].Text;
                    if (e.Item.Cells[10].Text != "&nbsp;")
                        CT_Year.Text = e.Item.Cells[10].Text;
                    if (e.Item.Cells[11].Text != "&nbsp;")
                        HK_Number.Text = e.Item.Cells[11].Text;
                    if (e.Item.Cells[9].Text != "&nbsp;" & e.Item.Cells[9].Text.Trim() != "")
                        DropDownList2.SelectedIndex = DropDownList2.Items.IndexOf(DropDownList2.Items.FindByText( e.Item.Cells[9].Text.Trim()));
                }
                else
                {
                    CT_Number.Text = e.Item.Cells[7].Text;

                    if (e.Item.Cells[12].Text != "&nbsp;")
                    {
                        CT_Result.SelectedIndex = Convert.ToInt32(e.Item.Cells[12].Text);
                        Session["CT"] = e.Item.Cells[12].Text;    // 目前合約狀況
                    }
                    else
                    {
                        CT_Result.SelectedIndex = 0;
                        Session["CT"] = "0";
                    }
                    if (e.Item.Cells[5].Text != "&nbsp;")
                        EC_Date.Text = e.Item.Cells[5].Text;
                    if (e.Item.Cells[9].Text != "&nbsp;")
                        CT_Year.Text = e.Item.Cells[9].Text;
                    if (e.Item.Cells[10].Text != "&nbsp;")
                        HK_Number.Text = e.Item.Cells[10].Text;
                    if (e.Item.Cells[8].Text != "&nbsp;" & e.Item.Cells[8].Text.Trim() != "")
                        DropDownList2.SelectedItem.Text = e.Item.Cells[8].Text.Trim();
                    ID_Label.Text = e.Item.Cells[18].Text;

                }

                Panel1.Visible = false;
                Panel2.Visible = false;
            }
            else
            {
                if (((Control)sender).ID == "DataGrid1")
                    ID_Label.Text = e.Item.Cells[0].Text;
                else
                    ID_Label.Text = e.Item.Cells[18].Text;

                Response.Redirect("CP.aspx?ID=" + ID_Label.Text);

            }
            // tmpLB = e.Item.Cells(2).Controls(0)
            // CN_Number.Text = tmpLB.Text
            // Response.Redirect("Show_Contract_Detail.aspx?Cust_No=" & CN_Number.Text)
        }

        protected void Show_Calendar(object sender, System.EventArgs e)
        {
            Session["TB"] = ((Control)sender).ID.ToString();
            Panel1.Visible = true;
            Page_Load(sender, e);
        }
        protected void Show_DropDownlist(object sender, System.EventArgs e)
        {
            Session["DB"] = ((Control)sender).ID.ToString();
            Page_Load(sender, e);
        }
        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            Panel1.Visible = false;
            switch ((string)Session["TB"])
            {
                case "EC_Date_B":
                    {
                        EC_Date.Text = Calendar1.SelectedDate.ToShortDateString();
                        break;
                    }

                case "Start_Date_B":
                    {
                        Start_Date.Text = Calendar1.SelectedDate.ToShortDateString();
                        break;
                    }
            }
        }
        protected void Rest_Click(object sender, EventArgs e)
        {
            ID_Label.Text = "";
            ID_Label.Text = "";
            CN_Number.Text = "";
            SF_Name.Text = "";
            CT_Number.Text = "";
            EC_Date.Text = "";
            CT_Year.Text = "";
            HK_Number.Text = "";
            Panel1.Visible = false;
            MultiView1.ActiveViewIndex = 0;
        }

        protected void Save_Click(object sender, EventArgs e)
        {
            string INString;
            string EMG = "";

            // If CT_Result.SelectedIndex = 3 Or CT_Result.SelectedIndex = 4 Then

            // '新合約
            // If N_SF_Name.Text = "" Then
            // EMG = "【負責專員】沒有填入!!"
            // End If
            // If N_HK_Number.Text = "" Then
            // EMG = EMG & "\n" & "【管家人數】沒有填入!!"
            // End If
            // If N_EC_Date.Text = "" Or Not IsDate(N_EC_Date.Text) Then
            // EMG = EMG & "\n" & "【合約到期日期】沒有填入!!"
            // End If
            // If N_CT_Year.Text = "" Then
            // EMG = EMG & "\n" & "【合約年限】沒有填入!!"
            // End If
            // If N_CT_Number.Text = "" Then
            // If EMG = "" Then
            // EMG = EMG & "\n" & "【合約次數】沒有填入!!"
            // End If
            // If CDbl(N_CT_Number.Text) <> CDbl(N_HK_Number.Text) * CDbl(DropDownList3.SelectedValue) Then
            // EMG = EMG & "\n" & "【合約次數】計算有誤!!"
            // End If
            // End If
            // End If

            ShareFunction SF = new ShareFunction();
            if (CT_Result.SelectedIndex == 1 | CT_Result.SelectedIndex == 2)
            {
                if (Start_Date.Text == "" | !SF.IsDate(Start_Date.Text))
                    EMG = EMG + @"\n" + "【解約(保留)日期】沒有填入!!";
                if (SF.IsDate(Start_Date.Text))
                {
                    //暫時不檢查
                    //if (Convert.ToDateTime(Start_Date.Text) < DateTime.Now.Date)
                    //    EMG = EMG + @"\n" + "【解約(保留)日期】不能早於今日日期!!";
                }
            }
            if (CT_Result.SelectedIndex == 2)
            {
                if (DropDownList1.SelectedIndex == 0)
                    EMG = EMG + @"\n" + "【保留周數】沒有選擇!!";
            }

            // 目前合約
            if (SF_Name.Text == "")
                EMG = "【負責專員】沒有填入!!";
            if (HK_Number.Text == "")
                EMG = EMG + @"\n" + "【管家人數】沒有填入!!";
            if (EC_Date.Text == "" | !SF.IsDate(EC_Date.Text))
                EMG = EMG + @"\n" + "【合約到期日期】沒有填入!!";
            if (CT_Year.Text == "")
                EMG = EMG + @"\n" + "【合約年限】沒有填入!!";
            if (CT_Number.Text == "")
                EMG = EMG + @"\n" + "【合約次數】沒有填入!!";
            //if (EMG == "")
            //{
            //    if (System.Convert.ToDouble(CT_Number.Text) != System.Convert.ToDouble(HK_Number.Text) * System.Convert.ToDouble(DropDownList2.SelectedValue))
            //        EMG = EMG + @"\n" + "【合約次數】計算有誤!!";
            //}

            if (EMG != "")
                Response.Write("<script language=JavaScript>alert('" + EMG + "');</script>");
            else
            {
                INString = "Update Cust_Contract_Data set Charge_Staff_Name = '" + SF_Name.Text + "',Contract_E_Date = '" + EC_Date.Text +
                           "',Contract_Times = " + CT_Number.Text + ",Contract_Status = " + CT_Result.SelectedIndex + ",Service_Cycle = '" +
                           DropDownList2.SelectedItem.Text + "',Contract_Years = " + CT_Year.Text + ",HK_Nums = " + HK_Number.Text +
                           " where ID = " + ID_Label.Text;

                switch (CT_Result.SelectedIndex)
                {
                    case 0:   //進行中只更新資料
                        INString = "Update Cust_Contract_Data set Charge_Staff_Name = '" + SF_Name.Text + "',Contract_E_Date = '" + EC_Date.Text +
                                   "',Contract_Times = " + CT_Number.Text + ",Contract_Status = " + CT_Result.SelectedIndex + ",Service_Cycle = '" +
                                   DropDownList2.SelectedItem.Text + "',Contract_Years = " + CT_Year.Text + ",HK_Nums = " + HK_Number.Text +
                                   " where ID = " + ID_Label.Text;
                        break;
                    case 1:   //解約
                        INString = "Update Cust_Contract_Data set Charge_Staff_Name = '" + SF_Name.Text + "',Contract_E_Date = '" + EC_Date.Text +
                                   "',Contract_Times = " + CT_Number.Text + ",Contract_Status = " + CT_Result.SelectedIndex + ",Service_Cycle = '" +
                                   DropDownList2.SelectedItem.Text + "',Contract_Years = " + CT_Year.Text + ",HK_Nums = " + HK_Number.Text +
                                   ",Contract_R_Date ='" + Start_Date.Text + "' where ID = " + ID_Label.Text;
                        break;
                    case 2:     //保留
                        INString = "Update Cust_Contract_Data set Charge_Staff_Name = '" + SF_Name.Text + "',Contract_E_Date = '" + EC_Date.Text +
                                   "',Contract_Times = " + CT_Number.Text + ",Contract_Status = " + CT_Result.SelectedIndex + ",Service_Cycle = '" +
                                   DropDownList2.SelectedItem.Text + "',Contract_Years = " + CT_Year.Text + ",HK_Nums = " + HK_Number.Text +
                                   ",Contract_R_Date = Dateadd(d," + Convert.ToInt32(DropDownList1.SelectedValue) * 7 + ",'" + EC_Date.Text +
                                   "') where ID = " + ID_Label.Text;
                        break;
                }
                INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                INCommand.Connection.Open();
                INCommand.ExecuteNonQuery();
                INCommand.Connection.Close();
                //Label3.Text = INString;
                if (CT_Result.SelectedIndex == 1 | CT_Result.SelectedIndex == 2)
                {
                    switch (CT_Result.SelectedIndex)
                    {
                        case 1:     //解約
                            INString = "Update Cust_Job_Schedule set Job_Flag = 255,Staff_No ='' where Job_date >= '" + Start_Date.Text + "' and Contract_ID = " + ID_Label.Text;
                            break;
                        case 2:     //保留
                            INString = "Update Cust_Job_Schedule set Job_Date = Dateadd(d," + Convert.ToInt32(DropDownList1.SelectedValue) * 7 +
                                       ",Job_Date) where Job_date >= '" + Start_Date.Text + "' and Contract_ID = " + ID_Label.Text;
                            break;
                    }

                    INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                    INCommand.Connection.Open();
                    INCommand.ExecuteNonQuery();
                    INCommand.Connection.Close();
                }
                //儲存到合約到期資料表
                string tmpContractEDate = null;
                string tmpContractRDate = null;
                string tmpServiceCycle = null;
                string tmpHKNums = null;
                string tmpContractTimes = null;
                string tmpContractYears = null;
                string tmpContractResult = null;

                tmpContractEDate = "null";
                tmpContractRDate = "null";
                tmpServiceCycle = "null";
                tmpHKNums = "null";
                tmpContractTimes = "null";
                tmpContractYears = "null";
                tmpContractResult = "5";

                INString = "Select * from Cust_Contract_Data where ID = " + ID_Label.Text;
                INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                INCommand.Connection.Open();
                SQLReader = INCommand.ExecuteReader();
                //如果有找到資料
                if (SQLReader.Read() == true)
                {
                    tmpContractEDate = (DBNull.Value.Equals(SQLReader[4]) ? "null" : "'" + Convert.ToDateTime(SQLReader[4]).ToShortDateString() + "'");
                    //預計終止日期
                    tmpContractRDate = (DBNull.Value.Equals(SQLReader[5]) ? "null" : "'" + Convert.ToDateTime(SQLReader[5]).ToShortDateString() + "'");
                    //實際終止日期
                    tmpContractTimes = (DBNull.Value.Equals(SQLReader[7]) ? "0" : SQLReader[7].ToString());
                    //合約次數
                    tmpServiceCycle = (DBNull.Value.Equals(SQLReader[11]) ? "null" : "'" + SQLReader[11].ToString() + "'");
                    //合約周期
                    tmpContractYears = (DBNull.Value.Equals(SQLReader[12]) ? "0" : SQLReader[12].ToString());
                    //合約年限
                    tmpHKNums = (DBNull.Value.Equals(SQLReader[13]) ? "0" : SQLReader[13].ToString());
                    //管家數量
                    //如果新約小約舊約次數,則為減次

                    tmpContractResult = CT_Result.SelectedIndex.ToString();

                }
                INCommand.Connection.Close();

                //判斷是否存在資料庫中
                INString = "Select ID from Cust_Contract_Expire where Contract_ID = " + ID_Label.Text;
                INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                INCommand.Connection.Open();
                SQLReader = INCommand.ExecuteReader();
                //如果有找到資料
                if (SQLReader.Read() == true)
                {
                    INString = "Update Cust_Contract_Expire set Contract_Result = " + tmpContractResult + ",Contract_R_Date = " + tmpContractRDate +
                               " where ID = " + SQLReader[0].ToString();
                }
                else
                {
                    INString = "Insert into Cust_Contract_Expire (Contract_ID,Cust_No,Staff_Name,Contract_E_Date,Contract_R_Date,Service_Cycle,HK_Nums," +
                               "Contract_Times,Contract_Years,Contract_Result) Values (" + ID_Label.Text + ",'" + CN_Number.Text + "','" + SF_Name.Text +
                               "'," + tmpContractEDate + "," + tmpContractRDate + "," + tmpServiceCycle + "," + tmpHKNums + "," + tmpContractTimes + "," +
                               tmpContractYears + "," + tmpContractResult + ")";

                }
                INCommand.Connection.Close();
                INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                INCommand.Connection.Open();
                INCommand.ExecuteNonQuery();
                INCommand.Connection.Close();

                MultiView1.ActiveViewIndex = 0;
                Check_Report_Click(sender, e);
            }
        }
        protected void Branch_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Dim tmpHyperLink As HyperLink = CType(FindControl("New_Recruit"), HyperLink)

            // tmpHyperLink.NavigateUrl = "Recruit.aspx?Branch=" & Branch.SelectedValue

            Page_Load(sender, e);
        }
        protected void CT_Result_SelectedIndexChanged(object sender, EventArgs e)
        {
            Panel2.Visible = true;
        }
    }
}