﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.IO;
using System.Text;
using System.Web.Script.Serialization;
using System.Configuration;

namespace MommyHappySchedule
{
    public class Addr_Json
    {
        public int Type;
        public string No;
        public string Name;
        public string Addr;
        public string lat;
        public string lng;
        public string Geometry;
    }

    /// <summary>
    /// getSpot 的摘要描述
    /// </summary>
    public class getSpot : IHttpHandler
    {
        int zip_no = 2;//中正區的郵遞區號
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        SqlConnection INConnection = new SqlConnection(ConnString);

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            if (!string.IsNullOrEmpty(context.Request["zip_no"]))
            {
                int.TryParse(context.Request["zip_no"], out this.zip_no);//防SQL Injection，轉型別失敗就用預設值

            }

            //取得DataTable原始資料
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                SqlDataAdapter da = new SqlDataAdapter(@"SELECT  0 as Type  ,No,Name,Addr,LEFT(Geometry,CHARINDEX(',',Geometry)-1) as lat
                                                   ,right(rtrim(Geometry),len(Geometry)-CHARINDEX(',',Geometry)) as lng,Geometry
                                                   FROM Cust_Addr_View
                                                   Where Geometry is not null and Geometry <> '' and Branch_ID = " + (this.zip_no) + @"
                                                   Order by No ASC", conn);
                da.Fill(ds);
                da = new SqlDataAdapter(@"SELECT  1 as Type,No,Name,Addr,LEFT(Geometry,CHARINDEX(',',Geometry)-1) as lat
                                                   ,right(rtrim(Geometry),len(Geometry)-CHARINDEX(',',Geometry)) as lng,Geometry
                                                   FROM Staff_Addr_View
                                                   Where Geometry is not null and Geometry <> '' and Branch_ID = " + (this.zip_no) + @"
                                                   Order by No ASC", conn);
                da.Fill(ds);
            }
            DataTable dt = ds.Tables[0];

            using (SqlConnection conn = new SqlConnection(ConnString))
            {
                SqlCommand command = new SqlCommand(@"Select ID,Branch_Name,Email,Address,Geometry,LEFT(Geometry,CHARINDEX(',',Geometry)-1) as lat
                                                   ,right(Rtrim(Geometry),len(Geometry)-CHARINDEX(',',Geometry)) as lng from Branch_Data where ID = " + (this.zip_no), conn);
                conn.Open();
                SqlDataReader SqlReader = command.ExecuteReader();

                // Call Read before accessing data.
                if (SqlReader.Read())
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = 1;
                    dr[1] = SqlReader[1].ToString().Trim();
                    dr[2] = SqlReader[1].ToString().Trim();
                    dr[3] = SqlReader[3].ToString().Trim();
                    dr[4] = SqlReader[5].ToString().Trim();
                    dr[5] = SqlReader[6].ToString().Trim();
                    dr[6] = SqlReader[4].ToString().Trim();
                    dt.Rows.InsertAt(dr, 0);
                }

                // Call Close when done reading.
                SqlReader.Close();
            }

            //因為本範例的資料都沒有緯度和經度，所以把原始資料DataTable傳入取得一個新的DataTable(有緯度、經度的)
            //利用Json.NET將DataTable轉成JSON字串，請參考另一篇文章：http://www.dotblogs.com.tw/shadow/archive/2011/11/30/60083.aspx
            //string str_json = JsonConvert.SerializeObject(dt, Formatting.Indented);
            var addr_json = new List<Addr_Json>();

            int i;
            for (i = 0; i < dt.DefaultView.Count; i++)
            {
                Addr_Json tmpAddr = new Addr_Json();
                tmpAddr.Type = (int)dt.Rows[i][0];
                tmpAddr.No = (string)dt.Rows[i][1];
                tmpAddr.Name = (string)dt.Rows[i][2];
                tmpAddr.Addr = (string)dt.Rows[i][3];
                tmpAddr.lat = (string)dt.Rows[i][4];
                tmpAddr.lng = (string)dt.Rows[i][5];
                tmpAddr.Geometry = (string)dt.Rows[i][6];
                addr_json.Add(tmpAddr);
            }
            var serializer = new JavaScriptSerializer();
            string str_json = serializer.Serialize(addr_json);
            context.Response.Write(str_json);   
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}