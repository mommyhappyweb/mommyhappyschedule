﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.UI;
using Models.Library;
using Models.Library.Cyphertext;

namespace MommyHappySchedule
{
    public partial class Default : Page
    {
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];

        private static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];

        private readonly ConvertType ConvertType = new ConvertType();

        private readonly BaseMD5 BaseMD5 = new BaseMD5();

        public bool Status = false;

        public string Message = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = "登入-電子排班系統";
            if (!IsPostBack) {
                Status = ConvertType.Boolean(Request.QueryString["Status"]);
                Message = Request.QueryString["Message"] ?? "";

                // 帳號密碼串接
                if (Request.QueryString["Account"] != null && Request.QueryString["Password"] != null) {
                    Account.Text = Request.QueryString["Account"];
                    Password.Text = Request.QueryString["Password"];
                    LoginClick(sender, e);
                }
            }
        }

        protected void LoginClick(object sender, EventArgs e)
        {
            using (SqlConnection Conn = new SqlConnection(ConnString))
            {
                Conn.Open();
                string AccountText = Account.Text;
                string PasswordText = BaseMD5.Md5Encrypt(Password.Text);
                string Sql = "SELECT TOP (1) UserName, Password, IsAdmin, Groups FROM Staff_Right WHERE Account = @Account";
                using (SqlCommand Command = new SqlCommand(Sql, Conn))
                {
                    Command.Parameters.AddWithValue("@Account", AccountText);
                    using (SqlDataReader Reader = Command.ExecuteReader())
                    {
                        if (Reader != null && Reader.HasRows)
                        {
                            while (Reader.Read())
                            {
                                string Password = ConvertType.String(Reader["Password"]);
                                if (Password == PasswordText)
                                {
                                    Session["Account"] = AccountText;
                                    Session["Username"] = ConvertType.String(Reader["UserName"]);
                                    Session["IsAdmin"] = ConvertType.String(Reader["IsAdmin"]);
                                    Session["Groups"] = ConvertType.String(Reader["Groups"]);
                                    Response.Redirect("Index.aspx");
                                }
                                else
                                {
                                    Response.Redirect("/?Code=false&Message=登入失敗");
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}