﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ScheduleToERP.aspx.cs" Inherits="MommyHappySchedule.ScheduleToERP" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Arrange Job Schedule</title>
    <style type="text/css">
        .auto-style1 {
            height: 112px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <center>
                <table>
                    <tr>
                        <td align="center">
                            <table>

                                <tr>

                                    <td colspan="3" class="auto-style1">
                                        <table border="1" cellpadding="1" cellspacing="0">
                                            <tr style="font-size: 10pt; font-family: Arial;">
                                                <td>駐點：</td>
                                                <td>
                                                    <asp:DropDownList ID="Branch" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>年份</td>
                                                <td>
                                                    <asp:DropDownList ID="Check_Year" runat="server">
                                                        <asp:ListItem>2016</asp:ListItem>
                                                        <asp:ListItem>2017</asp:ListItem>
                                                        <asp:ListItem Selected="True">2018</asp:ListItem>
                                                        <asp:ListItem>2019</asp:ListItem>
                                                        <asp:ListItem>2020</asp:ListItem>
                                                        <asp:ListItem>2021</asp:ListItem>
                                                    </asp:DropDownList></td>
                                                <td>月份</td>
                                                <td>
                                                    <asp:DropDownList ID="Check_Month" runat="server">
                                                        <asp:ListItem Selected="True">1</asp:ListItem>
                                                        <asp:ListItem>2</asp:ListItem>
                                                        <asp:ListItem>3</asp:ListItem>
                                                        <asp:ListItem>4</asp:ListItem>
                                                        <asp:ListItem>5</asp:ListItem>
                                                        <asp:ListItem>6</asp:ListItem>
                                                        <asp:ListItem>7</asp:ListItem>
                                                        <asp:ListItem>8</asp:ListItem>
                                                        <asp:ListItem>9</asp:ListItem>
                                                        <asp:ListItem>10</asp:ListItem>
                                                        <asp:ListItem>11</asp:ListItem>
                                                        <asp:ListItem>12</asp:ListItem>
                                                    </asp:DropDownList></td>
                                                <td>
                                                    <asp:Button ID="Check_Report" runat="server" Text="產生報表" OnClick="Check_Report_Click" /></td>
                                                <td>
                                                    <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                                                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="Index.aspx">返回主選單</asp:HyperLink>
                                                </td>
                                                <td colspan="2">
                                                    <asp:TextBox ID="TextBox1" runat="server" Width="80px" Visible="false"></asp:TextBox>
                                                    <asp:Label ID="Label5" runat="server" Text="0.00"></asp:Label>

                                                </td>

                                            </tr>
                                            <tr style="font-size: 10pt; font-family: Arial;">
                                                <td colspan="4">
                                                    
                                                </td>
                                                
                                                <td style="text-align: center" colspan="6">
                                                    <asp:CheckBox ID="CheckBox3" runat="server" Text="若有重覆記錄則覆蓋" Visible="False" />
                                                    <asp:Button ID="Button5" runat="server" Text="滙出排班"   OnClick="Button5_Click" />
                                                    <asp:Button ID="Button6" runat="server" Text="滙出請假"   OnClick="Button6_Click" />
                                                    <asp:Button ID="Button7" runat="server" Text="滙出加班"   OnClick="Button7_Click" />

                                                </td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:DataGrid ID="DataGrid3" runat="server" Font-Names="Arial"
                                            BorderWidth="1" Font-Size="Smaller" CellPadding="2" AutoGenerateColumns="False"
                                            OnItemCommand="Show_Job_Detail" OnItemDataBound="Show_Sc_Type" >
                                            <SelectedItemStyle Font-Bold="True" ForeColor="#333333" BackColor="#C5BBAF"></SelectedItemStyle>
                                            <ItemStyle BackColor="#E3EAEB"></ItemStyle>
                                            <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#1C5E55"></HeaderStyle>
                                            <FooterStyle ForeColor="White" BackColor="#1C5E55" Font-Bold="True"></FooterStyle>
                                            <Columns>
                                                <asp:BoundColumn DataField="ID" HeaderText="職稱">
                                                    <ItemStyle Width="30px" />
                                                </asp:BoundColumn>
                                                <%--<asp:TemplateColumn HeaderText="管家" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="HK_Name" runat="server" OnClick="Check_Sc" Text='<%# Bind("HK_Name") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>--%>
                                                <asp:BoundColumn DataField="HK_Name" HeaderText="管家" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle Width="100px"></ItemStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Day_ID" HeaderText="" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle Width="20px"></ItemStyle>
                                                </asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="1" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B1" runat="server" OnClick="Check_Sc" Text='<%# Bind("B1") %>'  />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="2" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B2" runat="server" OnClick="Check_Sc" Text='<%# Bind("B2") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="3" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B3" runat="server" OnClick="Check_Sc" Text='<%# Bind("B3") %>'  />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="4" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B4" runat="server" OnClick="Check_Sc" Text='<%# Bind("B4") %>'  />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="5" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B5" runat="server" OnClick="Check_Sc" Text='<%# Bind("B5") %>'  />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="6" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B6" runat="server" OnClick="Check_Sc" Text='<%# Bind("B6") %>'  />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="7" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B7" runat="server" OnClick="Check_Sc" Text='<%# Bind("B7") %>'  />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="8" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B8" runat="server" OnClick="Check_Sc" Text='<%# Bind("B8") %>'  />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="9" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B9" runat="server" OnClick="Check_Sc" Text='<%# Bind("B9") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="10" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B10" runat="server" OnClick="Check_Sc" Text='<%# Bind("B10") %>'  />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="11" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B11" runat="server" OnClick="Check_Sc" Text='<%# Bind("B11") %>'  />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="12" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B12" runat="server" OnClick="Check_Sc" Text='<%# Bind("B12") %>'  />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="13" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B13" runat="server" OnClick="Check_Sc" Text='<%# Bind("B13") %>'  />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="14" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B14" runat="server" OnClick="Check_Sc" Text='<%# Bind("B14") %>'  />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="15" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B15" runat="server" OnClick="Check_Sc" Text='<%# Bind("B15") %>'  />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="16" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B16" runat="server" OnClick="Check_Sc" Text='<%# Bind("B16") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="17" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B17" runat="server" OnClick="Check_Sc" Text='<%# Bind("B17") %>'  />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="18" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B18" runat="server" OnClick="Check_Sc" Text='<%# Bind("B18") %>'  />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="19" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B19" runat="server" OnClick="Check_Sc" Text='<%# Bind("B19") %>'  />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="20" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B20" runat="server" OnClick="Check_Sc" Text='<%# Bind("B20") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                 <asp:TemplateColumn HeaderText="21" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B21" runat="server" OnClick="Check_Sc" Text='<%# Bind("B21") %>'  />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="22" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B22" runat="server" OnClick="Check_Sc" Text='<%# Bind("B22") %>'  />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="23" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B23" runat="server" OnClick="Check_Sc" Text='<%# Bind("B23") %>'  />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="24" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B24" runat="server" OnClick="Check_Sc" Text='<%# Bind("B24") %>'  />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="25" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B25" runat="server" OnClick="Check_Sc" Text='<%# Bind("B25") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="26" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B26" runat="server" OnClick="Check_Sc" Text='<%# Bind("B26") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="27" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B27" runat="server" OnClick="Check_Sc" Text='<%# Bind("B27") %>'  />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="28" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B28" runat="server" OnClick="Check_Sc" Text='<%# Bind("B28") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="29" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B29" runat="server" OnClick="Check_Sc" Text='<%# Bind("B29") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="30" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B30" runat="server" OnClick="Check_Sc" Text='<%# Bind("B30") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                 <asp:TemplateColumn HeaderText="31" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Left" Wrap="true"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Button ID="B31" runat="server" OnClick="Check_Sc" Text='<%# Bind("B31") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>

                                            <EditItemStyle BackColor="#7C6F57" />
                                            <AlternatingItemStyle BackColor="White" />
                                        </asp:DataGrid>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:GridView ID="GridView1" runat="server" AllowCustomPaging="true"></asp:GridView>
                            <asp:TextBox ID="TextBox5" runat="server" Width="100%"></asp:TextBox></td>
                    </tr>
                </table>
            </center>
        </div>
    </form>
</body>
</html>
