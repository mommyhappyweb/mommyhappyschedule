﻿using Models.Library;
using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Transactions;
using System.Data.SqlClient;

namespace MommyHappySchedule
{

    partial class EContract_1 : Page
    {
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        SqlConnection INConnection1 = new SqlConnection(ConnString);
        SqlCommand INCommand;
        SqlDataReader SQLReader;
        DataSet MyDataSet;
        SqlDataAdapter MyCommand;
        DataTable dt, dt1;
        DataRow dr;
        string[] ReCust = new string[] { };

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string Sql = "";
                MyDataSet = new DataSet();
                Sql = "SELECT ID, TRIM(Branch_Name) AS Branch_Name FROM Branch_Data WHERE ID > 0";
                MyCommand = new SqlDataAdapter(Sql, INConnection1);
                MyCommand.Fill(MyDataSet, "Branch");

                Branch.DataSource = MyDataSet.Tables["Branch"].DefaultView;
                Branch.DataTextField = "Branch_Name";
                Branch.DataValueField = "ID";
                Branch.DataBind();

                BranchSelectEvent();

                Sql = "SELECT City_Name FROM  Addr_Code GROUP BY City_Name ORDER BY MIN(Addr_Code)";
                MyCommand = new SqlDataAdapter(Sql, INConnection1);
                MyCommand.Fill(MyDataSet, "CityName");
                DataRow TR = default(DataRow);
                TR = MyDataSet.Tables["CityName"].NewRow();
                TR[0] = "請選擇";
                MyDataSet.Tables["CityName"].Rows.InsertAt(TR, 0);

                City_Name.DataSource = MyDataSet.Tables["CityName"].DefaultView;
                City_Name.DataTextField = "City_Name";
                City_Name.DataValueField = "City_Name";
                City_Name.DataBind();

                DataTable Work_Week = default(DataTable);
                DataTable HK_Count = default(DataTable);
                DataRow dr = default(DataRow);
                Work_Week = new DataTable();
                Work_Week.Columns.Add(new DataColumn("ID", typeof(int)));
                Work_Week.Columns.Add(new DataColumn("WTime", typeof(string)));

                dr = Work_Week.NewRow();
                dr[0] = -1;
                dr[1] = "請選擇";
                Work_Week.Rows.Add(dr);
                dr = Work_Week.NewRow();
                dr[0] = 0;
                dr[1] = "上午";
                Work_Week.Rows.Add(dr);
                dr = Work_Week.NewRow();
                dr[0] = 1;
                dr[1] = "下午";
                Work_Week.Rows.Add(dr);

                Week_Work1.DataSource = Work_Week.DefaultView;
                Week_Work1.DataTextField = "WTime";
                Week_Work1.DataValueField = "ID";
                Week_Work1.DataBind();

                Week_Work2.DataSource = Work_Week.DefaultView;
                Week_Work2.DataTextField = "WTime";
                Week_Work2.DataValueField = "ID";
                Week_Work2.DataBind();

                Week_Work3.DataSource = Work_Week.DefaultView;
                Week_Work3.DataTextField = "WTime";
                Week_Work3.DataValueField = "ID";
                Week_Work3.DataBind();

                Week_Work4.DataSource = Work_Week.DefaultView;
                Week_Work4.DataTextField = "WTime";
                Week_Work4.DataValueField = "ID";
                Week_Work4.DataBind();

                Week_Work5.DataSource = Work_Week.DefaultView;
                Week_Work5.DataTextField = "WTime";
                Week_Work5.DataValueField = "ID";
                Week_Work5.DataBind();

                Week_Work6.DataSource = Work_Week.DefaultView;
                Week_Work6.DataTextField = "WTime";
                Week_Work6.DataValueField = "ID";
                Week_Work6.DataBind();

                Week_Work7.DataSource = Work_Week.DefaultView;
                Week_Work7.DataTextField = "WTime";
                Week_Work7.DataValueField = "ID";
                Week_Work7.DataBind();

                Week_Work8.DataSource = Work_Week.DefaultView;
                Week_Work8.DataTextField = "WTime";
                Week_Work8.DataValueField = "ID";
                Week_Work8.DataBind();

                Week_Work9.DataSource = Work_Week.DefaultView;
                Week_Work9.DataTextField = "WTime";
                Week_Work9.DataValueField = "ID";
                Week_Work9.DataBind();

                Week_Work10.DataSource = Work_Week.DefaultView;
                Week_Work10.DataTextField = "WTime";
                Week_Work10.DataValueField = "ID";
                Week_Work10.DataBind();

                Week_Work11.DataSource = Work_Week.DefaultView;
                Week_Work11.DataTextField = "WTime";
                Week_Work11.DataValueField = "ID";
                Week_Work11.DataBind();

                Week_Work12.DataSource = Work_Week.DefaultView;
                Week_Work12.DataTextField = "WTime";
                Week_Work12.DataValueField = "ID";
                Week_Work12.DataBind();

                Week_Work13.DataSource = Work_Week.DefaultView;
                Week_Work13.DataTextField = "WTime";
                Week_Work13.DataValueField = "ID";
                Week_Work13.DataBind();

                Week_Work14.DataSource = Work_Week.DefaultView;
                Week_Work14.DataTextField = "WTime";
                Week_Work14.DataValueField = "ID";
                Week_Work14.DataBind();

                HK_Count = new DataTable();
                HK_Count.Columns.Add(new DataColumn("HKC", typeof(string)));
                int Hii = 0;
                for (Hii = 0; Hii <= 6; Hii++)
                {
                    dr = HK_Count.NewRow();
                    dr[0] = Hii.ToString();
                    HK_Count.Rows.Add(dr);
                }

                HK_Count1.DataSource = HK_Count.DefaultView;
                HK_Count1.DataTextField = "HKC";
                HK_Count1.DataValueField = "HKC";
                HK_Count1.DataBind();

                HK_Count2.DataSource = HK_Count.DefaultView;
                HK_Count2.DataTextField = "HKC";
                HK_Count2.DataValueField = "HKC";
                HK_Count2.DataBind();

                HK_Count3.DataSource = HK_Count.DefaultView;
                HK_Count3.DataTextField = "HKC";
                HK_Count3.DataValueField = "HKC";
                HK_Count3.DataBind();

                HK_Count4.DataSource = HK_Count.DefaultView;
                HK_Count4.DataTextField = "HKC";
                HK_Count4.DataValueField = "HKC";
                HK_Count4.DataBind();

                HK_Count5.DataSource = HK_Count.DefaultView;
                HK_Count5.DataTextField = "HKC";
                HK_Count5.DataValueField = "HKC";
                HK_Count5.DataBind();

                HK_Count6.DataSource = HK_Count.DefaultView;
                HK_Count6.DataTextField = "HKC";
                HK_Count6.DataValueField = "HKC";
                HK_Count6.DataBind();

                HK_Count7.DataSource = HK_Count.DefaultView;
                HK_Count7.DataTextField = "HKC";
                HK_Count7.DataValueField = "HKC";
                HK_Count7.DataBind();

                HK_Count8.DataSource = HK_Count.DefaultView;
                HK_Count8.DataTextField = "HKC";
                HK_Count8.DataValueField = "HKC";
                HK_Count8.DataBind();

                HK_Count9.DataSource = HK_Count.DefaultView;
                HK_Count9.DataTextField = "HKC";
                HK_Count9.DataValueField = "HKC";
                HK_Count9.DataBind();

                HK_Count10.DataSource = HK_Count.DefaultView;
                HK_Count10.DataTextField = "HKC";
                HK_Count10.DataValueField = "HKC";
                HK_Count10.DataBind();

                HK_Count11.DataSource = HK_Count.DefaultView;
                HK_Count11.DataTextField = "HKC";
                HK_Count11.DataValueField = "HKC";
                HK_Count11.DataBind();

                HK_Count12.DataSource = HK_Count.DefaultView;
                HK_Count12.DataTextField = "HKC";
                HK_Count12.DataValueField = "HKC";
                HK_Count12.DataBind();

                HK_Count13.DataSource = HK_Count.DefaultView;
                HK_Count13.DataTextField = "HKC";
                HK_Count13.DataValueField = "HKC";
                HK_Count13.DataBind();

                HK_Count14.DataSource = HK_Count.DefaultView;
                HK_Count14.DataTextField = "HKC";
                HK_Count14.DataValueField = "HKC";
                HK_Count14.DataBind();

                //讀取假況資料表
                string INString = "Select * from Public_JobResult";
                INCommand = new SqlCommand(INString, INConnection1);
                INCommand.Connection.Open();
                SQLReader = INCommand.ExecuteReader();
                while (SQLReader.Read())
                {
                    RadioButtonList1.Items.Add(new ListItem(SQLReader[1].ToString().Trim()));
                }
                INCommand.Connection.Close();

                Panel1.Visible = false;
            }
        }
        protected void City_Name_SelectedIndexChanged(object sender, EventArgs e)
        {
            MyDataSet = new DataSet();
            MyCommand = new SqlDataAdapter(
                "SELECT Country_Name  FROM Addr_Code  where City_Name = '" + City_Name.SelectedValue + "'  Group by Country_Name ORDER BY MIN(Addr_Code)", INConnection1);
            MyCommand.Fill(MyDataSet, "AreaName");
            DataRow TR = default(DataRow);
            TR = MyDataSet.Tables["AreaName"].NewRow();
            TR[0] = "請選擇";
            MyDataSet.Tables["AreaName"].Rows.InsertAt(TR, 0);

            Area_Name.DataSource = MyDataSet.Tables["AreaName"].DefaultView;
            Area_Name.DataTextField = "Country_Name";
            Area_Name.DataValueField = "Country_Name";

            Area_Name.DataBind();

            Page_Load(sender, e);
        }

        protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            ListItemType objItemType = e.Item.ItemType;
            Button TempButton = default(Button);
            Label TempLabel = default(Label);
            Label TempLabel1 = default(Label);
            int tmpInt = 0;


            if (objItemType == ListItemType.Item | objItemType == ListItemType.AlternatingItem)
            {
                TempButton = (Button)e.Item.FindControl("BDate");
                TempLabel = (Label)e.Item.FindControl("Btime");
                TempLabel1 = (Label)e.Item.FindControl("BPrice");

                switch (TempLabel.Text)
                {
                    case "0":
                        //上午班
                        TempButton.BackColor = Color.LightPink;
                        TempButton.ForeColor = Color.Black;
                        break;
                    case "1":
                        //下午班
                        TempButton.BackColor = Color.LightBlue;
                        TempButton.ForeColor = Color.Black;
                        break;
                }

                //判斷是否是假日
                if (!int.TryParse(TempLabel1.Text, out tmpInt))
                {
                    TempButton.BackColor = Color.Red;
                    TempButton.ForeColor = Color.White;
                    TempButton.Text = TempButton.Text + TempLabel1.Text.Trim();
                }

            }

        }

        protected void CDate_Click(object sender, EventArgs e)
        {
            MyDataSet = new DataSet();
            MyCommand = new SqlDataAdapter("SELECT *  FROM Holiday_Date", INConnection1);
            MyCommand.Fill(MyDataSet, "Holiday_Date");
            MyCommand = new SqlDataAdapter("SELECT *  FROM Branch_Charge_Data where Branch_ID=" + Branch.SelectedValue, INConnection1);
            MyCommand.Fill(MyDataSet, "Branch_Charge");
            ShareFunction SF = new ShareFunction();
            Calendar1.Visible = false;

            if (!SF.IsDate(Start_Date.Text))
            {
                Response.Write("<script language=JavaScript>alert('不是日期格式!!');</script>");
                Page_Load(sender, e);
                return;
            }

            OContract_ID.Text = "0";
            int i = 0;
            int j = 0;
            int k = 0;
            DateTime SDate = Convert.ToDateTime(Start_Date.Text);
            DateTime Fdate = SDate;
            int tmpPrice = 0;
            int tmpTotal = 0;
            double tmpHT = 0.0;
            dt = new DataTable();
            dr = default(DataRow);
            dt1 = new DataTable();
            //dr1 = default(DataRow);

            int[] WD = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };
            int[] HC = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            int SW = 0;
            int SA = 0;
            int HT = 0;

            //服務日期資料表
            dt.Columns.Add(new DataColumn("ID", typeof(int)));
            dt.Columns.Add(new DataColumn("WDate", typeof(DateTime)));
            dt.Columns.Add(new DataColumn("WTime", typeof(string)));
            dt.Columns.Add(new DataColumn("WPrice", typeof(string)));
            dt.Columns.Add(new DataColumn("WHK", typeof(int)));

            //付款資料表
            dt1.Columns.Add(new DataColumn("ID", typeof(int)));
            dt1.Columns.Add(new DataColumn("PayDate", typeof(DateTime)));
            dt1.Columns.Add(new DataColumn("PayType", typeof(int)));
            dt1.Columns.Add(new DataColumn("PayText", typeof(string)));
            dt1.Columns.Add(new DataColumn("PayMount", typeof(int)));


            //利用14個星期及管家來判斷工作周期及日期
            //若第一周及第二周的管家總數 / 2 就是 周期數
            //管家總數即 該星期若有選擇'下午'或'上午' 則該管家數相加起來,即為 管家總數

            SW = (int)Convert.ToDateTime(Start_Date.Text).DayOfWeek;
            //算出第一個日期是星期幾
            HT = 0;
            //管家總人數
            TextBox7.Text = "";
            Session["HK"] = 0;
            Boolean WorkWeek = false;
            Boolean FirstWeek = false;
            Boolean SecondWeek = false;

            //14個下拉式元件
            for (SA = 0; SA <= 13; SA++)
            {
                DropDownList tmpDL1 = default(DropDownList);
                DropDownList tmpDL2 = default(DropDownList);
                tmpDL1 = (DropDownList)FindControl("Week_Work" + (((SA + SW) % 14) + 1).ToString());
                tmpDL2 = (DropDownList)FindControl("HK_Count" + (((SA + SW) % 14) + 1).ToString());
                //TextBox7.Text = TextBox7.Text & "Week_Work" & (((SA + SW) Mod 14) + 1).ToString

                //表示有選擇工作星期
                if (Convert.ToInt32(tmpDL1.SelectedValue) != -1)
                {
                    WD[SA] = Convert.ToInt32(tmpDL1.SelectedValue);
                    HC[SA] = Convert.ToInt32(tmpDL2.SelectedValue);
                    //找到最多的管家人數
                    Session["HK"] = (Convert.ToInt32(Session["HK"]) < Convert.ToInt32(tmpDL2.SelectedValue)) ? Convert.ToInt32(tmpDL2.SelectedValue) :
                        Convert.ToInt32(Session["HK"]);
                    HT = HT + Convert.ToInt32(tmpDL2.SelectedValue);
                    //加總管家數
                    TextBox7.Text = TextBox7.Text + tmpDL1.ID;
                    if (SA >= 0 & SA <= 6)
                        FirstWeek = true;
                    if (SA >= 7 & SA <= 13)
                        SecondWeek = true;
                }
            }
            TextBox7.Text = TextBox7.Text + HT.ToString();
            int TotalService = 0;

            WorkWeek = FirstWeek && SecondWeek;


            //如果是單次轉合約,則先找尋原單次合約,計算之前費用
            int LC_Total_Price = 0;
            int LC_Total_Numbers = 0;
            if (RadioButtonList2.SelectedIndex == 3)
            {
                if (TextBox8.Text != "")
                {
                    string INString = "Select Top 1 ID,Contract_S_Date,Contract_E_Date from Cust_Contract_Data where Cust_No = '" + TextBox8.Text +
                        "' and Note not like '%已轉合約%' Order by Contract_S_Date DESC";

                    TextBox7.Text = TextBox7.Text + INString + "\n";
                    INCommand = new SqlCommand(INString, INConnection1);
                    INCommand.Connection.Open();
                    SQLReader = INCommand.ExecuteReader();
                    //如果有找到資料
                    if (SQLReader.Read() == true)
                    {
                        DateTime tmpSdate, tmpEdate;
                        tmpSdate = (DBNull.Value.Equals(SQLReader[1])) ? Convert.ToDateTime("2018/01/01") : Convert.ToDateTime(SQLReader[1]);
                        tmpEdate = (DBNull.Value.Equals(SQLReader[2])) ? DateTime.Now : Convert.ToDateTime(SQLReader[2]);

                        //判斷單次合約日期是否早於目前合約的起始日期
                        if (DateTime.Compare(tmpSdate, SDate) >= 0 | DateTime.Compare(tmpEdate, SDate) >= 0)
                        {
                            INCommand.Connection.Close();
                            Response.Write("<script language=JavaScript>alert('日期早於單次起始或終止日期!請重設!');</script>");
                            Page_Load(sender, e);
                            return;
                        }
                        INString = "Select Sum(Job_Price) as Totol_Price,Count(ID) as Total_Numbers from Cust_Job_Schedule where Contract_ID = " +
                                   SQLReader[0].ToString();
                        INCommand.Connection.Close();

                        TextBox7.Text = TextBox7.Text + INString + "\n";
                        INCommand = new SqlCommand(INString, INConnection1);
                        INCommand.Connection.Open();
                        SQLReader = INCommand.ExecuteReader();
                        //如果有找到資料
                        if (SQLReader.Read() == true)
                        {
                            LC_Total_Price = Convert.ToInt32(SQLReader[0]);
                            LC_Total_Numbers = Convert.ToInt32(SQLReader[1]);
                        }
                        INCommand.Connection.Close();
                    }
                    else
                    {
                        INCommand.Connection.Close();
                        Response.Write("<script language=JavaScript>alert('此筆單次客戶編號已經移轉合約!!');</script>");
                        Page_Load(sender, e);
                        return;
                    }
                }
                else
                {
                    Response.Write("<script language=JavaScript>alert('單次轉合約，需要填入單次客戶編號!!');</script>");
                    Page_Load(sender, e);
                    return;
                }
            }


            //續約、加次判斷是否有舊合約
            if (RadioButtonList2.SelectedIndex == 1 | RadioButtonList2.SelectedIndex == 2)
            {
                if (Cust_No.Text != "")
                {
                    //string INString = "Select Top 1 ID,Contract_S_Date,Contract_E_Date,Contract_Times,Branch_Name from Cust_Contract_Data where Cust_No = '" +
                    //    Cust_No.Text +  "' Order by Contract_E_Date DESC ";
                    string INString = "Select Top 1 ID,Contract_S_Date,Contract_E_Date,Contract_Times,Branch_ID from Cust_Contract_Data where Cust_No = '" +
                        Cust_No.Text + "' Order by Contract_E_Date DESC ";

                    TextBox7.Text = TextBox7.Text + INString + "\n";
                    INCommand = new SqlCommand(INString, INConnection1);
                    INCommand.Connection.Open();
                    SQLReader = INCommand.ExecuteReader();
                    //如果有找到資料
                    if (SQLReader.Read() == true)
                    {
                        DateTime tmpSdate, tmpEdate;
                        tmpSdate = (DBNull.Value.Equals(SQLReader[1])) ? Convert.ToDateTime("2018/01/01") : Convert.ToDateTime(SQLReader[1]);
                        tmpEdate = (DBNull.Value.Equals(SQLReader[2])) ? DateTime.Now : Convert.ToDateTime(SQLReader[2]);
                        if (DBNull.Value.Equals(SQLReader[4]))
                        {
                            Branch.SelectedValue = SQLReader[4].ToString().Trim();
                            OContract_ID.Text = SQLReader[0].ToString().Trim();
                        }


                        switch (RadioButtonList2.SelectedIndex)
                        {
                            case 1:
                                //續約判斷舊合約日期是否早於目前合約的起始日期
                                if (DateTime.Compare(tmpSdate, SDate) > 0 | DateTime.Compare(tmpEdate, SDate) > 0)
                                {
                                    INCommand.Connection.Close();
                                    Response.Write("<script language=JavaScript>alert('日期早於舊合約起始或終止日期!請重設!');</script>");
                                    Page_Load(sender, e);
                                    return;
                                }
                                break;
                            case 2:
                                //加次判斷舊合約日期是否早於目前合約的起始日期
                                if (!(DateTime.Compare(SDate, tmpSdate) > 0 & DateTime.Compare(SDate, tmpEdate) < 0))
                                {
                                    INCommand.Connection.Close();
                                    Response.Write("<script language=JavaScript>alert('日期未在舊合約起始或終止日期之間!請重設!');</script>");
                                    Page_Load(sender, e);
                                    return;
                                }
                                break;
                        }
                        tmpHT = Convert.ToDouble(SQLReader[3]);    //舊合約次數是判斷加次費用的依據
                        INCommand.Connection.Close();
                    }
                    else
                    {
                        INCommand.Connection.Close();
                        Response.Write("<script language=JavaScript>alert('續約及加次，找不到舊的客戶編號!!');</script>");
                        Page_Load(sender, e);
                        return;
                    }
                }
                else
                {
                    Response.Write("<script language=JavaScript>alert('續約及加次需要填入舊的客戶編號!!');</script>");
                    Page_Load(sender, e);
                    return;
                }
            }

            //計算每月應收款項
            int Monthly_Payment = 0;
            if (Contract_Year.SelectedIndex != 2)        //如果不是單次
            {
                for (SA = 0; SA <= 13; SA++)
                {
                    int tmpSA = (SA + SW) % 7;
                    if (WD[SA] > -1 & HC[SA] > 0)
                    {
                        tmpPrice = 0;
                        if (WorkWeek)  //HT >= 2  舊判斷式,會錯判,如果二周都有選擇
                        {
                            //星期六、日價格
                            if (tmpSA == 0 | tmpSA == 6)
                            {
                                tmpPrice = (int)MyDataSet.Tables["Branch_Charge"].DefaultView[0][2];
                            }
                            else
                            {
                                tmpPrice = (int)MyDataSet.Tables["Branch_Charge"].DefaultView[0][1];
                            }

                        }
                        else   //二周一次
                        {
                            //星期六、日價格
                            if (tmpSA == 0 | tmpSA == 6)
                            {
                                tmpPrice = (int)MyDataSet.Tables["Branch_Charge"].DefaultView[0][4];
                            }
                            else
                            {
                                tmpPrice = (int)MyDataSet.Tables["Branch_Charge"].DefaultView[0][3];
                            }
                        }
                        Monthly_Payment = Monthly_Payment + tmpPrice;
                    }
                }
                Monthly_Payment = Monthly_Payment * 2;    //算完只有半個月,需再乘以2才為一個月應收帳款
            }


            //應服務總次數 不含假日
            if (Contract_Year.SelectedIndex == 2)        //單次
                TotalService = HT;
            else
                TotalService = 24 * Convert.ToInt32(Contract_Year.SelectedValue) * HT - LC_Total_Numbers;     //減去單次轉合約的單次次數,若無則為0

            i = 0;
            tmpTotal = 0;
            int tmpSTotal = LC_Total_Price;    //單次轉合約的單次總金額
            //總價格
            k = 0;

            //如果還沒有到達合約次數
            while (dt.DefaultView.Count < TotalService)
            {
                if (Contract_Year.SelectedIndex == 2)   //單次價格
                {
                    if (RadioButtonList2.SelectedIndex == 2)   //加次
                    {
                        //一周一次以上
                        if (tmpHT >= 1.0)    //判斷舊合約次數 
                        {
                            //星期六、日價格
                            if (Fdate.DayOfWeek == DayOfWeek.Saturday | Fdate.DayOfWeek == DayOfWeek.Sunday)
                            {
                                tmpPrice = (int)MyDataSet.Tables["Branch_Charge"].DefaultView[0][2];
                            }
                            else
                            {
                                tmpPrice = (int)MyDataSet.Tables["Branch_Charge"].DefaultView[0][1];
                            }
                            //二周一次
                        }
                        else
                        {
                            //星期六、日價格
                            if (Fdate.DayOfWeek == DayOfWeek.Saturday | Fdate.DayOfWeek == DayOfWeek.Sunday)
                            {
                                tmpPrice = (int)MyDataSet.Tables["Branch_Charge"].DefaultView[0][4];
                            }
                            else
                            {
                                tmpPrice = (int)MyDataSet.Tables["Branch_Charge"].DefaultView[0][3];
                            }
                        }
                    }
                    else
                        tmpPrice = 2200;
                }
                else
                {
                    //一周一次以上
                    if (WorkWeek)   //HT >= 2 會判錯
                    {
                        //星期六、日價格
                        if (Fdate.DayOfWeek == DayOfWeek.Saturday | Fdate.DayOfWeek == DayOfWeek.Sunday)
                        {
                            tmpPrice = (int)MyDataSet.Tables["Branch_Charge"].DefaultView[0][2];
                        }
                        else
                        {
                            tmpPrice = (int)MyDataSet.Tables["Branch_Charge"].DefaultView[0][1];
                        }
                        //二周一次
                    }
                    else
                    {
                        //星期六、日價格
                        if (Fdate.DayOfWeek == DayOfWeek.Saturday | Fdate.DayOfWeek == DayOfWeek.Sunday)
                        {
                            tmpPrice = (int)MyDataSet.Tables["Branch_Charge"].DefaultView[0][4];
                        }
                        else
                        {
                            tmpPrice = (int)MyDataSet.Tables["Branch_Charge"].DefaultView[0][3];
                        }
                    }
                }
                if (i == 0)
                    tmpSTotal = tmpSTotal - tmpPrice * LC_Total_Numbers;   //需減去次數乘以每次金額



                //如果有選擇
                if (WD[i % 14] > -1)
                {
                    //判斷管家數
                    for (j = 1; j <= HC[i % 14]; j++)
                    {
                        int stmpPrice = tmpPrice;
                        if (tmpSTotal != 0)    //如果單次轉合約金額尚未扣完
                        {
                            if (tmpSTotal > tmpPrice)       //如果金額大於單筆金額
                            {
                                tmpSTotal = tmpSTotal - tmpPrice;
                                stmpPrice = 0;
                            }
                            else
                            {
                                stmpPrice = stmpPrice - tmpSTotal;      //等於單筆金額減去剩下金額
                                tmpSTotal = 0;
                            }
                        }

                        dr = dt.NewRow();
                        dr[0] = k;
                        dr[1] = Fdate;
                        dr[2] = WD[i % 14];
                        MyDataSet.Tables["Holiday_Date"].DefaultView.RowFilter = "H_Date = '" + Fdate.ToShortDateString() + "'";
                        //如果是假日
                        if (MyDataSet.Tables["Holiday_Date"].DefaultView.Count == 1)
                        {
                            dr[3] = "國" + MyDataSet.Tables["Holiday_Date"].DefaultView[0][1].ToString().Trim();
                            TotalService = TotalService + 1;
                            //服務總次數加假日次數
                        }
                        else
                        {
                            dr[3] = stmpPrice;
                            tmpTotal = tmpTotal + stmpPrice;
                        }
                        dr[4] = j;
                        //管家數
                        dt.Rows.Add(dr);
                        k = k + 1;
                    }
                }
                i = i + 1;
                Fdate = SDate.AddDays(i);
            }
            TotalFee.Text = tmpTotal.ToString("#,##0");
            if (Contract_Year.SelectedIndex == 2)   //單次
            {
                Contract_Times.Text = "";
                TotalC.Text = dt.DefaultView.Count.ToString() + "(" + dt.DefaultView.Count.ToString() + ")";
            }
            else
            {
                //Contract_Times.Text = ((double)(HT) / 2) == 0.5 ? "二周一次" : "一周" + ((double)(HT) / 2).ToString() + "次";
                Contract_Times.Text = (WorkWeek ? "一周" + ((double)(HT) / 2).ToString() + "次" : "二周一次" + HT.ToString() + "管家");
                TotalC.Text = (24 * Convert.ToInt32(Contract_Year.SelectedValue) * HT).ToString() + "(" + dt.DefaultView.Count.ToString() + ")";
                Label3.Text = LC_Total_Price.ToString();
            }

            //if (Contract_Year.SelectedIndex == 2)  //單次
            //{
            //    dr1 = dt1.NewRow();
            //    dr1[0] = 1;
            //    dr1[1] = DateTime.Today;
            //    dr1[2] = RadioButtonList3.SelectedIndex;
            //    dr1[3] = "";
            //    //第2期之後都是依照每月扣款金額,第一期則是依實際金額減去每月的扣款金額剩下的
            //    dr1[4] = tmpTotal;
            //    dt1.Rows.Add(dr1);
            //}
            //else
            //{
            //    //顯示每次付款方式及內容
            //    for (i = 1; i <= Convert.ToInt16(Contract_Year.SelectedValue) * 12; i++)
            //    {
            //        dr1 = dt1.NewRow();
            //        dr1[0] = i;
            //        if (RadioButtonList2.SelectedIndex == 1)
            //            dr1[1] = (i <= 1) ? DateTime.Today : Convert.ToDateTime((SDate.Day > 15) ? SDate.AddMonths(i - 1).ToString("yyyy/MM") + "/15" :
            //                SDate.AddMonths(i - 1).ToString("yyyy/MM") + "/01");
            //        else
            //            dr1[1] = (i <= 2) ? DateTime.Today : Convert.ToDateTime((SDate.Day > 15) ? SDate.AddMonths(i - 1).ToString("yyyy/MM") + "/15" :
            //                SDate.AddMonths(i - 1).ToString("yyyy/MM") + "/01");
            //        dr1[2] = RadioButtonList3.SelectedIndex;
            //        dr1[3] = "";
            //        //第2期之後都是依照每月扣款金額,第一期則是依實際金額減去每月的扣款金額剩下的
            //        dr1[4] = (i == 1) ? tmpTotal - Monthly_Payment * (Convert.ToInt16(Contract_Year.SelectedValue) * 12 - 1) : Monthly_Payment;
            //        dt1.Rows.Add(dr1);
            //    }

            //}

            //TextBox7.Text = tmpTotal.ToString
            dt.DefaultView.Sort = "WDate";
            Repeater1.DataSource = dt.DefaultView;
            Repeater1.DataBind();
            //dt1.DefaultView.Sort = "ID";
            //Repeater2.DataSource = dt1.DefaultView;
            //Repeater2.DataBind();

            Session["dt"] = dt;
            Session["HT"] = HT;
            Session["TC"] = tmpTotal;
            Session["WW"] = WorkWeek;

            Page_Load(sender, e);


        }
        protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            Label TempLabel1 = default(Label);
            Label TempLabel2 = default(Label);
            Button tmpButton = default(Button);

            Session["CT"] = "1";
            TempLabel1 = (Label)e.Item.FindControl("BTime");
            TempLabel2 = (Label)e.Item.FindControl("ID");
            tmpButton = (Button)e.Item.FindControl("BDate");

            if (TempLabel1.Text == "0" | TempLabel1.Text == "1")
            {
                Panel1.Visible = true;
                RadioButtonList1.ClearSelection();
                Session["WID"] = Convert.ToInt16(TempLabel2.Text);
                Label2.Text = tmpButton.Text;
            }

            //Session("WID") = tmpButton.Text


        }
        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)Session["dt"];
            int SR = Convert.ToInt16(Session["WID"]);
            int HT = Convert.ToInt16(Session["HT"]);
            Boolean WorkWeek = Convert.ToBoolean(Session["WW"]);

            //服務次數  2周總和
            DateTime FDate = default(DateTime);
            int ti = 0;
            int tmpPrice = 0;
            int tmpTotal = 0;
            int tmpHK = 0;
            int tk = 0;
            string WTS = "";

            if (DBNull.Value.Equals(dt))
            {
                return;
            }

            //防止畫面重整造成錯誤
            if ((string)Session["CT"] == "1")
            {
                Session["CT"] = "0";

                tmpTotal = Convert.ToInt32(Session["TC"]);
                //總金額

                Panel1.Visible = false;

                if (RadioButtonList1.SelectedValue != "取消修改")
                {
                    MyDataSet = new DataSet();
                    MyCommand = new SqlDataAdapter("SELECT *  FROM Holiday_Date", INConnection1);
                    MyCommand.Fill(MyDataSet, "Holiday_Date");
                    MyCommand = new SqlDataAdapter("SELECT *  FROM Branch_Charge_Data where Branch_ID=" + Branch.SelectedValue, INConnection1);
                    MyCommand.Fill(MyDataSet, "Branch_Charge");

                    //修改即有日期及原因
                    dr = dt.Rows[SR];
                    //Label2.Text = dr[1)
                    dr[3] = RadioButtonList1.SelectedValue;
                    FDate = (DateTime)dr[1];
                    tmpHK = Convert.ToInt16(dr[4]);
                    //一周一次以上
                    if (WorkWeek)  //HT >= 2
                    {
                        //星期六價格
                        if (FDate.DayOfWeek == DayOfWeek.Saturday | FDate.DayOfWeek == DayOfWeek.Sunday)
                        {
                            tmpPrice = (int)MyDataSet.Tables["Branch_Charge"].DefaultView[0][2];
                        }
                        else
                        {
                            tmpPrice = (int)MyDataSet.Tables["Branch_Charge"].DefaultView[0][1];
                        }

                    }
                    else   //二周一次
                    {
                        //星期六價格
                        if (FDate.DayOfWeek == DayOfWeek.Saturday | FDate.DayOfWeek == DayOfWeek.Sunday)
                        {
                            tmpPrice = (int)MyDataSet.Tables["Branch_Charge"].DefaultView[0][4];
                        }
                        else
                        {
                            tmpPrice = (int)MyDataSet.Tables["Branch_Charge"].DefaultView[0][3];
                        }
                    }
                    tmpTotal = tmpTotal - tmpPrice;
                    //減去修改的金額


                    ti = dt.Rows.Count - HT;
                    tk = dt.Rows.Count;
                    while (true)
                    {
                        dr = dt.Rows[ti];
                        FDate = (DateTime)dr[1];
                        WTS = (string)dr[2];
                        FDate = FDate.AddDays(14);
                        //找下一個工作日

                        MyDataSet.Tables["Holiday_Date"].DefaultView.RowFilter = "H_Date = '" + FDate.ToShortDateString() + "'";
                        //如果是假日
                        if (MyDataSet.Tables["Holiday_Date"].DefaultView.Count == 1)
                        {
                            dr = dt.NewRow();
                            dr[0] = tk;
                            dr[1] = FDate;
                            dr[2] = WTS;
                            dr[3] = MyDataSet.Tables["Holiday_Date"].DefaultView[0][1].ToString().Trim();
                            dr[4] = tmpHK;

                            tk = tk + 1;
                            ti = ti + 1;
                        }
                        else
                        {
                            break; // TODO: might not be correct. Was : Exit While
                        }
                    }
                    dr = dt.NewRow();
                    dr[0] = tk;
                    dr[1] = FDate;
                    dr[2] = WTS;
                    //一周一次以上
                    if (WorkWeek)  //HT >= 2
                    {
                        //星期六價格
                        if (FDate.DayOfWeek == DayOfWeek.Saturday | FDate.DayOfWeek == DayOfWeek.Sunday)
                        {
                            tmpPrice = (int)MyDataSet.Tables["Branch_Charge"].DefaultView[0][2];
                        }
                        else
                        {
                            tmpPrice = (int)MyDataSet.Tables["Branch_Charge"].DefaultView[0][1];
                        }

                    }
                    else  //二周一次
                    {
                        //星期六價格
                        if (FDate.DayOfWeek == DayOfWeek.Saturday | FDate.DayOfWeek == DayOfWeek.Sunday)
                        {
                            tmpPrice = (int)MyDataSet.Tables["Branch_Charge"].DefaultView[0][4];
                        }
                        else
                        {
                            tmpPrice = (int)MyDataSet.Tables["Branch_Charge"].DefaultView[0][3];
                        }
                    }
                    dr[3] = tmpPrice;
                    dr[4] = tmpHK;
                    dt.Rows.Add(dr);

                    tmpTotal = tmpTotal + tmpPrice;

                    TotalC.Text = (24 * Convert.ToInt32(Contract_Year.SelectedValue) * HT).ToString() + "(" + dt.DefaultView.Count.ToString() + ")";
                    TotalFee.Text = tmpTotal.ToString("#,##0");
                }
            }
            dt.DefaultView.Sort = "WDate";
            Repeater1.DataSource = dt.DefaultView;
            Repeater1.DataBind();
            Session["dt"] = dt;
            Session["HT"] = HT;
            Session["TC"] = tmpTotal;
            Session["CT"] = "1";
            Session["WW"] = WorkWeek;

            Page_Load(sender, e);
        }

        protected void CustContractSaveClick(object sender, EventArgs e)
        {
            using (TransactionScope Transactions = new TransactionScope())
            {
                using (SqlConnection Conn = new SqlConnection(ConnString))
                {
                    Conn.Open();

                    bool SaveData = false;
                    string INString = null;
                    string ContractNums = null;
                    DataTable dt = (DataTable)Session["dt"];
                    int HT = Convert.ToInt16(Session["HT"]);
                    bool WorkSeek = Convert.ToBoolean(Session["WW"]);
                    int tmpInt = 0;

                    if (dt == null | DBNull.Value.Equals(Session["HK"]) == true)
                    {
                        Response.Write("<script language=JavaScript>alert('請先按下產生日期!!');</script>");
                        return;
                    }
                    if (dt.DefaultView.Count == 0)
                    {
                        Response.Write("<script language=JavaScript>alert('未選擇服務星期!!');</script>");
                        return;
                    }

                    //foreach (Control item in Repeater2.Items)   //付款方式記錄
                    //{
                    //    DropDownList tmpDrop = (DropDownList)item.FindControl("PayType");
                    //    TextBox7.Text = TextBox7.Text + tmpDrop.SelectedValue;

                    //}

                    //讀取假況資料表
                    INString = "Select * from Public_JobResult";
                    using (SqlCommand Command = new SqlCommand(INString, Conn))
                    {
                        using (SqlDataReader Reader = Command.ExecuteReader())
                        {
                            if (Reader != null && Reader.HasRows)
                            {
                                while (Reader.Read())
                                {
                                    ReCust = ReCust.Concat(new string[] { Reader[1].ToString().Trim() }).ToArray();
                                    RadioButtonList1.Items.Add(new ListItem(Reader[1].ToString().Trim()));
                                }
                            }
                        }
                    }

                    //如果有選擇員工編號
                    string StaffDataName = "";
                    if (Staff_Data.SelectedIndex >= 0) StaffDataName = Staff_Data.SelectedItem.Text;

                    //服務次數  2周總和
                    //單次
                    if (Contract_Year.SelectedIndex == 2)
                    {
                        //找尋重複的合約內容
                        INString = "Select * from Cust_Contract_Data where Cust_No = '" + Cust_No.Text + "' and Contract_S_Date = '" + Convert.ToDateTime(dt.Rows[0][1]).ToShortDateString() + "' and Contract_E_Date = '" + Convert.ToDateTime(dt.Rows[dt.Rows.Count - 1][1]).ToShortDateString() + "' and Contract_Times = 0.0 ";
                    }
                    else
                    {
                        //找尋重複的合約內容
                        //續約、單次、加次則判斷所有資料不能重複
                        if (RadioButtonList2.SelectedIndex == 1)
                            INString = "Select COUNT(*) AS Count from Cust_Contract_Data where Cust_No = '" + Cust_No.Text + "' and Contract_S_Date = '" + Convert.ToDateTime(dt.Rows[0][1]).ToShortDateString() + "' and Contract_E_Date = '" + Convert.ToDateTime(dt.Rows[dt.Rows.Count - 1][1]).ToShortDateString() + "' and Contract_Times = " + ((WorkSeek) ? (Convert.ToDouble(HT) / 2).ToString() : "0.5");
                        else
                            //非續約則判斷不能有相同的合約編號
                            INString = "Select COUNT(ID) AS Count from Cust_Contract_Data where Cust_No = '" + Cust_No.Text + "'";
                    }
                    TextBox7.Text = TextBox7.Text + INString + "\n";
                    int ContractDataCount = 0;
                    using (SqlCommand INCommand = new SqlCommand(INString, Conn))
                    {
                        using (SqlDataReader Reader = INCommand.ExecuteReader())
                        {
                            if (Reader != null && Reader.HasRows)
                            {
                                while (Reader.Read())
                                {
                                    ContractDataCount = Convert.ToInt32(Reader["Count"]);
                                }
                            }
                        }
                    }
                    int SID = 0;
                    if (ContractDataCount < 0)
                    {
                        INCommand.Connection.Close();
                        Response.Write("<script language=JavaScript>alert('重複記錄無法存入!!');</script>");
                        return;
                    } else {
                        INString = "Select Key_SerNo from Key_Data where Key_Name = '合約編號'";
                        TextBox7.Text = TextBox7.Text + INString + "\n";
                        long KeySerNo = 0;
                        using (SqlCommand INCommand = new SqlCommand(INString, Conn))
                        {
                            using (SqlDataReader Reader = INCommand.ExecuteReader())
                            {
                                //合約編號
                                if (Reader != null && Reader.HasRows)
                                {
                                    while (Reader.Read())
                                    {
                                        KeySerNo = Convert.ToInt64(Reader[0]) + 1;
                                    }
                                    ContractNums = DateTime.Now.Year.ToString().Substring(2, 2) + string.Format("{0:000000}", KeySerNo);
                                } else {
                                    ContractNums = DateTime.Now.Year.ToString().Substring(2, 2) + string.Format("{0:000000}", 1);
                                }
                            }
                        }
                        if (KeySerNo > 0)
                        {
                            INString = "Update Key_Data set Key_SerNo = '" + string.Format("{0:000000}", KeySerNo) + "' where Key_Name = '合約編號'";
                            using (SqlCommand INCommand = new SqlCommand(INString, Conn))
                            {
                                INCommand.ExecuteNonQuery();
                            }
                        }

                        //存入合約資料檔中
                        if (Contract_Year.SelectedIndex == 2)
                        {
                            //單次
                            INString = "INSERT INTO Cust_Contract_Data (Contract_Nums, Charge_Staff_Name, Contract_S_Date, Contract_E_Date, Contract_R_Date, Cust_No, Contract_Times, Contract_Status, Contract_U_Date, Branch_ID, Service_Cycle, Contract_Years, HK_Nums,Note) Values ('" + ContractNums + "', '" + StaffDataName + "', '" + Convert.ToDateTime(dt.Rows[0][1]).ToShortDateString() + "', '" +  Convert.ToDateTime(dt.Rows[dt.Rows.Count - 1][1]).ToShortDateString() + "', null, '" + Cust_No.Text + "', 0, 0, getdate(), '" + Branch.SelectedValue.Trim() + "', '', 0, '" + Session["HK"].ToString() + "', '單次')";
                        } else {
                            //加次
                            string tmpNote = (RadioButtonList2.SelectedIndex == 3) ? RadioButtonList2.SelectedValue + TextBox8.Text : tmpNote = RadioButtonList2.SelectedValue;
                            INString = "INSERT INTO Cust_Contract_Data (Contract_Nums, Charge_Staff_Name, Contract_S_Date, Contract_E_Date, Contract_R_Date, Cust_No, Contract_Times, Contract_Status, Contract_U_Date, Branch_ID, Service_Cycle, Contract_Years, HK_Nums,Note) Values ('" + ContractNums + "', '" + StaffDataName + "', '" + Convert.ToDateTime(dt.Rows[0][1]).ToShortDateString() + "', '" + Convert.ToDateTime(dt.Rows[dt.Rows.Count - 1][1]).ToShortDateString() + "', null, '" + Cust_No.Text + "', " + ((WorkSeek) ? (Convert.ToDouble(HT) / 2).ToString() : "0.5") + ", 0, getdate(), '" + Branch.SelectedValue.Trim() + "', '" + Contract_Times.Text + "', '" + Contract_Year.SelectedValue + "', '" + Session["HK"].ToString() + "', '" + tmpNote + "')";
                        }
                        TextBox7.Text = TextBox7.Text + INString + "\n";
                        using (SqlCommand INCommand = new SqlCommand(INString, Conn))
                        {
                            INCommand.ExecuteNonQuery();
                        }

                        //讀取 Contract ID編號
                        INString = "Select ID from Cust_Contract_Data where Charge_Staff_Name = '" + StaffDataName + "' and Contract_S_Date = '" + Convert.ToDateTime(dt.Rows[0][1]).ToShortDateString() + "' and " + " Contract_E_Date = '" + Convert.ToDateTime(dt.Rows[dt.Rows.Count - 1][1]).ToShortDateString() + "' and" + " Cust_No = '" + Cust_No.Text + "' and Contract_Years = '" + Contract_Year.SelectedValue + "' and HK_Nums = '" + Session["HK"].ToString() + "'";
                        TextBox7.Text = TextBox7.Text + INString + "\n";
                        using (SqlCommand INCommand = new SqlCommand(INString, Conn))
                        {
                            using (SqlDataReader Reader = INCommand.ExecuteReader())
                            {
                                if (Reader != null && Reader.HasRows)
                                {
                                    while (Reader.Read())
                                    {
                                        SID = Convert.ToInt32(Reader[0]);
                                    }
                                }
                            }
                        }
                        //如果有找到資料
                        if (SID > 0)
                        {
                            //寫入 Job Schedule 中
                            for (int i = 0; i <= dt.Rows.Count - 1; i++)
                            {
                                //表示是有假況
                                string Job_Date = Convert.ToDateTime(dt.Rows[i][1]).ToShortDateString();
                                string Job_Time = dt.Rows[i][2].ToString();
                                string Job_Price = dt.Rows[i][3].ToString();
                                string Job_Result_ID = (Array.IndexOf(ReCust, dt.Rows[i][3].ToString().Substring(0, 1))).ToString();
                                string HK_Serial = dt.Rows[i][4].ToString();
                                if (int.TryParse((string)dt.Rows[i][3], out tmpInt))
                                {
                                    INString = "INSERT INTO Cust_Job_Schedule (Job_Date, Job_Time, Staff_No, Cust_No, Job_Result_ID, Job_Price, Job_Flag, Job_Status, HK_Serial, Contract_ID) Values ('" + Job_Date + "', '" + Job_Time + "', null, '" + Cust_No.Text + "', 0, '" + Job_Price + "', 0, null, '" + HK_Serial + "','" + SID + "')";
                                } else {
                                    INString = "INSERT INTO Cust_Job_Schedule (Job_Date, Job_Time, Staff_No, Cust_No, Job_Result_ID, Job_Price, Job_Flag, Job_Status, HK_Serial, Contract_ID) Values ('" + Job_Date + "', '" + Job_Time + "', null, '" + Cust_No.Text + "', '" + Job_Result_ID + "', 0, 0, null, '" + HK_Serial + "', '" + SID + "')";
                                }
                                TextBox7.Text = TextBox7.Text + INString + "\n";
                                using (SqlCommand INCommand = new SqlCommand(INString, Conn))
                                {
                                    INCommand.ExecuteNonQuery();
                                    SaveData = true;
                                }
                            }
                        }
                        //單次轉合約
                        if (RadioButtonList2.SelectedIndex == 3)
                        {
                            INString = "Update Cust_Contract_Data set Note = CONCAT(note,'已轉合約到[" + Cust_No.Text + "]') " + " where Cust_No = '" + TextBox8.Text + "'";
                            using (SqlCommand INCommand = new SqlCommand(INString, Conn))
                            {
                                INCommand.ExecuteNonQuery();
                            }
                        }
                    }

                    //找尋是否有客戶資料
                    if (CheckBox1.Checked == true | RadioButtonList2.SelectedIndex == 0)
                    {
                        //找出定位資訊
                        WebClient webClient = new WebClient
                        {
                            Encoding = Encoding.UTF8
                        };
                        JavaScriptSerializer Serializer = new JavaScriptSerializer();
                        Address ItemList;

                        // JsonString = webClient.DownloadString("https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCFAVGBNpJ4rdb5YP36TGGvVS0WEBEZMpc&address=" +
                        //                                        City_Name.SelectedValue.ToString() + Area_Name.SelectedValue.ToString() + Home_Addr.Text)   'Gmail
                        string JsonString = webClient.DownloadString("https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCg6PptSmUzNUUw-BZW5h5ZXSe4LI8SMyU&address=" + City_Name.SelectedValue.ToString() + Area_Name.SelectedValue.ToString() + Home_Addr.Text);   // Mommyhappy
                        ItemList = Serializer.Deserialize<Address>(JsonString);

                        string Cust_Geometry = "";
                        if (ItemList.status == "OK")
                        {
                            Cust_Geometry = ItemList.results[0].geometry.location.lat + "," + ItemList.results[0].geometry.location.lng;
                        }

                        INString = "SELECT COUNT(Cust_No) AS Count FROM Cust_Data WHERE Cust_No = '" + Cust_No.Text + "'";
                        int CustCount = 0;
                        using (SqlCommand INCommand = new SqlCommand(INString, Conn))
                        {
                            using (SqlDataReader Reader = INCommand.ExecuteReader())
                            {
                                if (Reader != null && Reader.HasRows)
                                {
                                    while (Reader.Read())
                                    {
                                        CustCount = Convert.ToInt32(Reader["Count"]);
                                    }
                                }
                            }
                        }
                        //如果有找到資料
                        if (CustCount > 0)
                        {
                            INString = "UPDATE Cust_Data SET Cust_Name = '" + Contract_Name.Text + "', Cell_Phone ='" + Cell_Phone.Text + "', House_Phone ='" + Home_Phone.Text + "', Other_Phone = '" + Business_Phone.Text + "', Cust_Addr ='" + City_Name.SelectedValue.ToString() + Area_Name.SelectedValue.ToString() + Home_Addr.Text + "', Cust_Geometry = '" + Cust_Geometry + "' WHERE Cust_No = '" + Cust_No.Text + "'";
                        } else {
                            INString = "INSERT INTO Cust_Data (Cust_No, Cust_Name, Cell_Phone, House_Phone, Other_Phone, Cust_Addr, IDCard, Cust_Geometry) VALUES ('" + Cust_No.Text + "', '" + Contract_Name.Text + "', '" + Cell_Phone.Text + "', '" + Home_Phone.Text + "', '" + Business_Phone.Text + "', '" + City_Name.SelectedValue.ToString() + Area_Name.SelectedValue.ToString() + Home_Addr.Text + "', '" + RID.Text + "', '" + Cust_Geometry + "')";
                        }
                        //TextBox7.Text = TextBox7.Text + INString + "\n";
                        using (SqlCommand INCommand = new SqlCommand(INString, Conn))
                        {
                            INCommand.ExecuteNonQuery();
                        }
                    }

                    //如果不是單次的記錄,則必需要記錄在 Cust_Contract_Expire 中
                    //單次
                    if (Contract_Year.SelectedIndex != 2)
                    {
                        string tmpContractEDate = "";
                        string tmpContractRDate = "";
                        string tmpServiceCycle = "";
                        string tmpHKNums = "";
                        string tmpContractTimes = "";
                        string tmpContractYears = "";
                        string tmpContractResult = "5";

                        //續約
                        if (RadioButtonList2.SelectedIndex == 1)
                        {
                            //判斷是否有舊合約編號
                            if (string.IsNullOrEmpty(OContract_ID.Text))
                            {
                                INString = "SELECT TOP 1 * FROM Cust_Contract_Data WHERE Cust_No = '" + Cust_No.Text + "' ORDER BY Contract_S_Date, Contract_E_Date";
                            } else {
                                INString = "SELECT * FROM Cust_Contract_Data WHERE ID = " + OContract_ID.Text;
                            }
                            TextBox7.Text = TextBox7.Text + INString + "\n";
                            using (SqlCommand INCommand = new SqlCommand(INString, Conn))
                            {
                                using (SqlDataReader Reader = INCommand.ExecuteReader())
                                {
                                    if (Reader != null && Reader.HasRows)
                                    {
                                        while (Reader.Read())
                                        {
                                            OContract_ID.Text = Reader[0].ToString();
                                            tmpContractEDate = (DBNull.Value.Equals(Reader[4]) ? "" : Convert.ToDateTime(Reader[4]).ToShortDateString());
                                            //預計終止日期
                                            tmpContractRDate = (DBNull.Value.Equals(Reader[5]) ? "" : Convert.ToDateTime(Reader[5]).ToShortDateString());
                                            //實際終止日期
                                            tmpContractTimes = (DBNull.Value.Equals(Reader[7]) ? "0" : Reader[7].ToString());
                                            //合約次數
                                            tmpServiceCycle = (DBNull.Value.Equals(Reader[11]) ? "" : Reader[11].ToString());
                                            //合約周期
                                            tmpContractYears = (DBNull.Value.Equals(Reader[12]) ? "0" : Reader[12].ToString());
                                            //合約年限
                                            tmpHKNums = (DBNull.Value.Equals(Reader[13]) ? "0" : Reader[13].ToString());
                                            //管家數量
                                            //如果新約小約舊約次數,則為減次
                                            tmpContractResult = (Convert.ToDouble(tmpContractTimes) < Convert.ToDouble(HT) / 2) ? "4" : "3";
                                        }
                                    }  else {
                                        OContract_ID.Text = "0";
                                    }
                                }
                            }
                        }
                        //判斷是否存在資料庫中
                        INString = "SELECT TOP 1 ID FROM Cust_Contract_Expire WHERE Contract_ID = '" + OContract_ID.Text + "' AND (Contract_E_Date = '" + tmpContractEDate + "' OR Contract_R_Date = '" + tmpContractEDate + "' ) AND Contract_Times = '" + tmpContractTimes + "'";
                        int CustContractExpire_ID = 0;
                        using (SqlCommand INCommand = new SqlCommand(INString, Conn))
                        {
                            using (SqlDataReader Reader = INCommand.ExecuteReader())
                            {
                                if (Reader != null && Reader.HasRows)
                                {
                                    while (Reader.Read())
                                    {
                                        CustContractExpire_ID = Convert.ToInt32(Reader["ID"]);
                                    }
                                }
                            }
                        }
                        //如果有找到資料
                        string N_Contract_Times = ((WorkSeek) ? (Convert.ToDouble(HT) / 2).ToString() : "0.5");
                        string N_Contract_Years = Contract_Year.SelectedValue;
                        string N_Contract_S_Date = Start_Date.Text;
                        if (CustContractExpire_ID > 0)
                        {
                            string N_Service_Cycle = Contract_Times.Text;
                            string N_HK_Nums = HT.ToString();
                            INString = "UPDATE Cust_Contract_Expire SET N_Service_Cycle = '" + N_Service_Cycle + "', N_HK_Nums = '" + N_HK_Nums + "', N_Contract_Times = '" + N_Contract_Times + "', N_Contract_Years = '" + N_Contract_Years + "', N_Contract_S_Date = '" + N_Contract_S_Date + "' WHERE ID = '" + CustContractExpire_ID + "'";
                        } else {
                            INString = "INSERT INTO Cust_Contract_Expire (Contract_ID, Cust_No, Staff_Name, Contract_E_Date, Contract_R_Date, Service_Cycle, HK_Nums, Contract_Times, Contract_Years, Contract_Result, N_Service_Cycle, N_HK_Nums, N_Contract_Times, N_Contract_Years, N_Contract_S_Date)" + " VALUES ('" + OContract_ID.Text + "', '" + Cust_No.Text + "', '" + StaffDataName + "', '" + tmpContractEDate + "', '" + tmpContractRDate + "', '" + tmpServiceCycle + "', '" + tmpHKNums + "', '" + tmpContractTimes + "', '" + tmpContractYears + "', '" + tmpContractResult + "', '" + Contract_Times.Text + "', '" + HT.ToString() + "', '" + N_Contract_Times + "', '" + N_Contract_Years + "', '" + N_Contract_S_Date + "')";
                        }

                        TextBox7.Text = TextBox7.Text + INString + "\n";
                        using (SqlCommand INCommand = new SqlCommand(INString, Conn))
                        {
                            INCommand.ExecuteNonQuery();
                        }
                    }

                    ////儲存付款資料
                    //// 找尋是否有重複的記錄,若有則無法儲存
                    //int tmpPayID = 0;

                    //INString = "Select * from Cust_Contract_Pay where Contract_ID = " + ContractNums.Trim();
                    //TextBox7.Text = TextBox7.Text + INString + "\n";
                    //INCommand = new SqlCommand(INString, INConnection1);
                    //INCommand.Connection.Close();
                    //INCommand.Connection.Open();
                    //SQLReader = INCommand.ExecuteReader();
                    ////如果有找到資料
                    //if (SQLReader.Read() == true)
                    //{
                    //    Response.Write("<script language=JavaScript>alert('付款方式重複儲存!!');</script>");
                    //}
                    //else
                    //{
                    //    //付款期數 單次及加次為1,其餘為年份 * 12
                    //    string tmpPayNumber = (Contract_Year.SelectedValue == "0") ? "1" : (Convert.ToInt16(Contract_Year.SelectedValue) * 12).ToString();

                    //    INString = "Insert into Cust_Contract_Pay (Contract_ID,Cust_No,C_Pay_Numbers,Payment_Type,Out_Bank,Out_Acct_No,C_Pay_Date,C_Pay_Amt,Payment_memo) " +
                    //               " values ('" + ContractNums + "','" + Cust_No.Text + "'," + tmpPayNumber + "," + RadioButtonList3.SelectedIndex.ToString() + ",'" +
                    //               Bank_No.Text + "','" + Account_No.Text + "',getdate()," + TotalFee.Text.Remove(TotalFee.Text.IndexOf(','), 1) + ",'" +
                    //               RadioButtonList2.SelectedItem.Text + "')";

                    //    INCommand.Connection.Close();
                    //    INCommand = new SqlCommand(INString, INConnection1);
                    //    INCommand.Connection.Open();
                    //    INCommand.ExecuteNonQuery();
                    //    INCommand.Connection.Close();

                    //    //讀取付款單號
                    //    INString = "Select ID from Cust_Contract_Pay where Contract_ID = " + ContractNums.Trim();
                    //    TextBox7.Text = TextBox7.Text + INString + "\n";
                    //    INCommand = new SqlCommand(INString, INConnection1);
                    //    INCommand.Connection.Close();
                    //    INCommand.Connection.Open();
                    //    SQLReader = INCommand.ExecuteReader();
                    //    //如果有找到資料
                    //    if (SQLReader.Read() == true)
                    //        tmpPayID = Convert.ToInt32(SQLReader[0]);
                    //    INCommand.Connection.Close();

                    //    //寫入交易付款資料表

                    //    foreach (Control item in Repeater2.Items)   //付款方式記錄
                    //    {
                    //        Label tmpID = (Label)item.FindControl("ID");
                    //        Label tmpPayDate = (Label)item.FindControl("PayDate");
                    //        DropDownList tmpPayType = (DropDownList)item.FindControl("PayType");
                    //        TextBox tmpPayText = (TextBox)item.FindControl("PayText");
                    //        Label tmpPayMount = (Label)item.FindControl("PayMount");
                    //        CheckBox tmpPayDone = (CheckBox)item.FindControl("PayDone");

                    //        INString = "Insert into Cust_Contract_Pay_Detail (Pay_ID,Pay_Number,Payment_Type,Out_Acct_No,R_Pay_Date,Amount,Amt,Sales_Memo,FulFill) " +
                    //                   " values (" + tmpPayID.ToString() + "," + tmpID.Text.Trim() + "," + tmpPayType.SelectedIndex.ToString() + ",'" + tmpPayText.Text.Trim()
                    //                   + "','" + tmpPayDate.Text.Trim() + "',1," + tmpPayMount.Text.Remove(tmpPayMount.Text.IndexOf(','), 1).Trim() + ",'" +
                    //                   Staff_Data.SelectedValue.Trim() + "'," + ((tmpPayDone.Checked == true) ? "1" : "0") + ")";

                    //        INCommand = new SqlCommand(INString, INConnection1);
                    //        INCommand.Connection.Open();
                    //        INCommand.ExecuteNonQuery();
                    //        INCommand.Connection.Close();
                    //    }

                    //}

                    if (SaveData == true)
                    {
                        Transactions.Complete();
                        Conn.Close();
                        Conn.Dispose();
                        Response.Write("<script language=JavaScript>alert('資料己存檔!!');</script>");
                        Response.Redirect("CP.aspx?ID=" + SID.ToString(), false);
                    }
                }
            }
        }

        protected void CustContractRemoveClick(object sender, EventArgs e)
        {
            RadioButtonList2.SelectedIndex = 0;
            Cust_No.Text = "";
            Contract_Name.Text = "";
            RID.Text = "";
            Home_Phone.Text = "";
            Business_Phone.Text = "";
            Cell_Phone.Text = "";
            City_Name.SelectedIndex = 0;
            Area_Name.SelectedIndex = 0;
            Home_Addr.Text = "";
            Contract_Year.SelectedIndex = 0;
            Start_Date.Text = "";
            Contract_Times.Text = "";
            TotalC.Text = "";
            TotalFee.Text = "";
            //14個下拉式元件
            for (int SA = 1; SA <= 14; SA++)
            {
                DropDownList tmpDL1 = default(DropDownList);
                DropDownList tmpDL2 = default(DropDownList);
                tmpDL1 = (DropDownList)FindControl("Week_Work" + SA.ToString());
                tmpDL2 = (DropDownList)FindControl("HK_Count" + SA.ToString());
                tmpDL1.SelectedIndex = 0;
                tmpDL2.SelectedIndex = 0;
            }
            RadioButtonList1.SelectedIndex = 0;
            TextBox7.Text = "";
            dt = new DataTable();
            Repeater1.DataSource = dt.DefaultView;
            Repeater1.DataBind();
            dt1 = new DataTable();
            Repeater1.DataSource = dt1.DefaultView;
            Repeater1.DataBind();
            Panel1.Visible = false;

            Page_Load(sender, e);
        }

        protected void Branch_SelectedIndexChanged(object sender, EventArgs e)
        {
            BranchSelectEvent();
        }

        protected void BranchSelectEvent()
        {
            MyDataSet = new DataSet();
            int Branch_ID = Convert.ToInt16(Branch.SelectedValue);
            string Sql = "SELECT Staff_No, Staff_Name FROM Staff_Job_Data_View WHERE Job_Title_ID IN (5, 6, 16, 17, 18, 19) AND Job_Status_ID = 2 AND Branch_ID = " + Branch_ID;
            //string Sql = "SELECT Staff_No, Staff_Name FROM Staff_Job_Data_View where (Job_Title like '%專%' or Job_Title like '%備%' or Job_Title like '%理%' ) and Job_Status = '在職' and  Branch_ID = '" + Branch.SelectedIndex + "'";
            MyCommand = new SqlDataAdapter(Sql, INConnection1);
            MyCommand.Fill(MyDataSet, "Staff_Data");

            Staff_Data.DataSource = MyDataSet.Tables["Staff_Data"].DefaultView;
            Staff_Data.DataTextField = "Staff_Name";
            Staff_Data.DataValueField = "Staff_No";
            Staff_Data.DataBind();
        }

        public EContract_1()
        {
            Load += Page_Load;
        }

        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            Start_Date.Text = Calendar1.SelectedDate.ToShortDateString();
            Calendar1.Visible = false;
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Calendar1.Visible = true;
        }

        protected void RadioButtonList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Contract_Year.Items[0].Enabled = true;
            Contract_Year.Items[1].Enabled = true;
            Contract_Year.Items[2].Enabled = true;

            switch (RadioButtonList2.SelectedIndex)
            {
                case 0:    //新約
                    break;
                case 1:    //續約
                    Contract_Year.Items[2].Enabled = false;   //續約無法選取單次
                    break;
                case 2:    //加次
                    Contract_Year.Items[0].Enabled = false;
                    Contract_Year.Items[1].Enabled = false;
                    Contract_Year.Items[2].Selected = true;   //加次僅能單次
                    break;
                case 3:    //單次轉合約
                    Contract_Year.Items[2].Enabled = false;   //轉合約無法選取單次
                    break;

            }



            if (RadioButtonList2.SelectedIndex == 3)   //單次轉合約
                Panel2.Visible = true;
            else
                Panel2.Visible = false;

        }
    }
}