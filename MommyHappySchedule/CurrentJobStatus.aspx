﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CurrentJobStatus.aspx.cs" Inherits="MommyHappySchedule.CurrentJobStatus" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Refresh" content="30" />
    <title></title>
    <!--<script type="text/javascript" src="Public/Scripts/jquery-3.3.1.min.js"></script>-->
    <!--要玩Google Map 就一定要引用此js-->
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCMyvr47tuN5E0bwEgemLMaT-fvbztedV4&callback=initMap">
    </script>
    <script type="text/javascript">
        
        //網頁上所有的DOM都載入後
        function initMap() {
            <%--，這裡可以抓Code-Behind的 protected string zip_no;來替代--%> 
            var zip_no = QueryString("ID");
            var time_no = QueryString("Time");

            //alert(zip_no);
            //取得標記點資料
            getMapData(zip_no,time_no);
        }

        function attachSecretMessage(marker, secretMessage)
        {
           var infowindow = new google.maps.InfoWindow({
                content: secretMessage
                });

           marker.addListener('click', function() {
            infowindow.open(marker.get('map'), marker);
                });
        }

        function QueryString(name) {
            var AllVars = window.location.search.substring(1);
            var Vars = AllVars.split("&");
            for (i = 0; i < Vars.length; i++) {
                var Var = Vars[i].split("=");
                if (Var[0] == name) return Var[1];
            }
            return "";
        }

        //取得標記點資料
        function getMapData(zip_no,time_no) {
            //$.ajax(
            //    {
            //        url: 'getSpot.ashx',
            //        type: 'post',
            //        async: false,
            //        data: { zip_no: zip_no },
            //        dataType: 'json',
            //        success: addMarker,//在某一區加入多個標記點
            //        error: function (e) {
            //            alert("Google地圖發生錯誤，無法載入");
            //        }
            //    });//End jQuery Ajax
            var url = 'GetJobSpot.ashx?zip_no=' + zip_no + '&time_no=' + time_no;
            var xhr = new XMLHttpRequest();
            xhr.open('get', url , true);
            xhr.send(null);
            xhr.onload = function () {
                var str_json = JSON.parse(xhr.responseText);
                addMarker(str_json);
            }
        } //End function getMapData(zip_no)
       
        //在某一區加入多個標記點
        function addMarker(str_json) {
            //是否為第一次執行迴圈
            var map;
            var first = true;

            for (var index in str_json) {

                //建立緯經度座標物件
                var latlng = new google.maps.LatLng(str_json[index].lat, str_json[index].lng);

                if (first) {
                    map = new google.maps.Map(document.getElementById('Map'), {
                        center: latlng,   // 22.6687945, 120.3058869 中高雄位置
                        zoom: 13
                    });
                    var marker = new google.maps.Marker({
                        position: latlng,
                        map: map,
                        color: '#ADFF2F',
                        title: str_json[index].No
                    });
                    first = false;
                }
                else {
                    //alert(str_json[index].lat);
                    //加入一個Marker到map中
                    switch (str_json[index].Type )
                    {
                        case 2:
                            var marker = new google.maps.Marker({
                                position: latlng,
                                map: map,
                                icon: {
                                    path: google.maps.SymbolPath.CIRCLE,
                                    strokeColor: '#FF0000',
                                    fillColor: '#FF0000',
                                    fillOpacity: 1,
                                    strokeOpacity: 1.0,
                                    strokeWeight: 3,
                                    zIndex : str_json[index].Addr,
                                    scale: 5
                                },
                                title: str_json[index].No + '\n' + str_json[index].Name
                            });
                            attachSecretMessage(marker, str_json[index].Geometry);
                            break;
                        case 3:
                            var marker = new google.maps.Marker({
                                position: latlng,
                                map: map,
                                animation: google.maps.Animation.BOUNCE,
                                icon: {
                                    //path: google.maps.SymbolPath.CIRCLE,
                                    //strokeColor: '#FF00FF',
                                    //fillColor: '#FF00FF',
                                    //fillOpacity: 1,
                                    //strokeOpacity: 1.0,
                                    //strokeWeight: 3,
                                    //scale: 5
                                    //url: 'http://60.248.240.187:8084/img/mhdoll-02.png',
                                    //scaledSize: new google.maps.Size(20, 20)
                                    url: 'https://schedule.mommyhappy.com/Public/Images/mhdoll-01.png',
                                    zIndex : str_json[index].Addr,
                                    scaledSize: new google.maps.Size(20, 30)
                                },
                                title: str_json[index].No + '\n' + str_json[index].Name
                            });
                            attachSecretMessage(marker, str_json[index].Geometry);
                            break;
                        case 4:
                            var marker = new google.maps.Marker({
                                position: latlng,
                                map: map,
                                icon: {
                                    path: google.maps.SymbolPath.CIRCLE,
                                    strokeColor: '#00FFFF',
                                    fillColor: '#00FFFF',
                                    fillOpacity: 1,
                                    strokeOpacity: 1.0,
                                    strokeWeight: 3,
                                    zIndex : str_json[index].Addr,
                                    scale: 5
                                },
                                title: str_json[index].No + '\n' + str_json[index].Name
                            });
                            attachSecretMessage(marker, str_json[index].Geometry);
                            break;
                    }
                }
            } //End for (var index in str_json) 
        }//End function addMarker()
    </script>
</head>
<body>
    <form id="form1" runat="server">

    <!--顯示Google Map的區塊-->
    <div id="Map" style="width:100%; height:560px;">
    </div>
    </form>
   
</body>
</html>