﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MommyHappySchedule
{
    public partial class ShowCurrentJob : System.Web.UI.Page
    {
        private System.Data.DataSet MyDataSet = new System.Data.DataSet();
        private System.Data.SqlClient.SqlDataAdapter MyCommand;
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        System.Data.SqlClient.SqlConnection INConnection1 = new System.Data.SqlClient.SqlConnection(ConnString);



        protected void Page_Load(object sender, System.EventArgs e)
        {
            if ((string)Session["account"] == null)
            {
                Session["msg"] = "您沒有使用此程式的權限!!";
                Response.Redirect("Index.aspx");
            }

            if (!Page.IsPostBack)
            {
                MyDataSet = new System.Data.DataSet();
                MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Branch_Data Order by ID ", INConnection1);
                MyCommand.Fill(MyDataSet, "Branch");

                Branch.DataSource = MyDataSet.Tables["Branch"].DefaultView;
                Branch.DataTextField = "Branch_Name";
                Branch.DataValueField = "ID";
                Branch.DataBind();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if(CheckBox1.Checked == false )
                Response.Redirect("CurrentJobStatus.aspx?ID=" + Branch.SelectedValue);
            else
                Response.Redirect("CurrentJobStatus.aspx?ID=" + Branch.SelectedValue + "&Time=2");
        }
    }

}