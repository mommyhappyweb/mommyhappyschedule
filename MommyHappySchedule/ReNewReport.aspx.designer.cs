﻿//------------------------------------------------------------------------------
// <自動產生的>
//     這段程式碼是由工具產生的。
//
//     變更這個檔案可能會導致不正確的行為，而且如果已重新產生
//     程式碼，則會遺失變更。
// </自動產生的>
//------------------------------------------------------------------------------

namespace MommyHappySchedule {
    
    
    public partial class ReNewReport {
        
        /// <summary>
        /// form1 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;
        
        /// <summary>
        /// Branch 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList Branch;
        
        /// <summary>
        /// Check_Year 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList Check_Year;
        
        /// <summary>
        /// Check_Month 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList Check_Month;
        
        /// <summary>
        /// Check_Report 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button Check_Report;
        
        /// <summary>
        /// HyperLink1 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.HyperLink HyperLink1;
        
        /// <summary>
        /// RadioButtonList1 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList RadioButtonList1;
        
        /// <summary>
        /// ImageMap1 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageMap ImageMap1;
        
        /// <summary>
        /// Label44 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label44;
        
        /// <summary>
        /// ViewT01 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel ViewT01;
        
        /// <summary>
        /// ViewT02 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel ViewT02;
        
        /// <summary>
        /// ViewT03 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel ViewT03;
        
        /// <summary>
        /// ViewT04 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel ViewT04;
        
        /// <summary>
        /// ViewT05 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel ViewT05;
        
        /// <summary>
        /// ViewT06 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel ViewT06;
        
        /// <summary>
        /// ViewT07 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel ViewT07;
        
        /// <summary>
        /// ViewT08 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel ViewT08;
        
        /// <summary>
        /// ViewT09 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel ViewT09;
        
        /// <summary>
        /// ViewT10 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel ViewT10;
        
        /// <summary>
        /// ViewT11 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel ViewT11;
        
        /// <summary>
        /// ViewT12 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel ViewT12;
        
        /// <summary>
        /// Repeater2 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 若要修改，請將欄位宣告從設計工具檔案移到程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Repeater Repeater2;
    }
}
