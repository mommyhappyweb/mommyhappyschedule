﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddJobSchedule.aspx.cs" Inherits="MommyHappySchedule.iframe.AddJobSchedule" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>電子合約</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <style type="text/css">
        .auto-style3 {
            width: 100%;
        }

        .auto-style7 {
            border-style: solid;
            border-width: 1px;
            padding: 1px;
        }

        .auto-style8 {
            border-style: solid;
            border-width: 1px;
            padding: 1px;
            text-align: center;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <table class="auto-style3">
            <tr>
                <td colspan="8" style="text-align: center; font-size: 30px;" class="auto-style7">清潔服務委任契約書</td>
            </tr>
            <tr>
                <td class="auto-style7">駐點：</td>
                <td class="auto-style7">
                    <asp:DropDownList ID="Branch" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Branch_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:DropDownList ID="Staff_Data" runat="server">
                    </asp:DropDownList>
                </td>
                <td colspan="2" class="auto-style7">
                    <asp:RadioButtonList ID="RadioButtonList2" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="RadioButtonList2_SelectedIndexChanged">
                        <asp:ListItem Selected="True">新約</asp:ListItem>
                        <asp:ListItem>續約</asp:ListItem>
                        <asp:ListItem>加次</asp:ListItem>
                        <asp:ListItem>單次轉合約</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td colspan="2" class="auto-style7">
                    <asp:Label ID="OContract_ID" runat="server" Visible="false"></asp:Label>

                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="RC.aspx">返回主選單</asp:HyperLink>
                </td>
                <td class="auto-style7">訂單編號：</td>
                <td class="auto-style7">
                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style7">客戶編號：</td>
                <td class="auto-style7">
                    <asp:TextBox ID="Cust_No" runat="server" Text=""></asp:TextBox>
                </td>
                <td class="auto-style7">訂約人姓名</td>
                <td class="auto-style7">
                    <asp:TextBox ID="Contract_Name" runat="server" AutoCompleteType="DisplayName"></asp:TextBox>
                </td>
                <td class="auto-style7">
                    <asp:RadioButtonList ID="Sexy" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                        <asp:ListItem Value="0">先生</asp:ListItem>
                        <asp:ListItem Value="1" Selected="True">小姐</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td class="auto-style7">營利事業或<br />
                    身份證號碼</td>
                <td colspan="2" class="auto-style7">
                    <asp:TextBox ID="RID" runat="server" AutoCompleteType="BusinessZipCode" EnableTheming="True" MaxLength="10"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style7">電話：</td>
                <td colspan="2" class="auto-style7">宅：<asp:TextBox ID="Home_Phone" runat="server" AutoCompleteType="HomePhone" TextMode="Phone"></asp:TextBox>
                </td>
                <td colspan="2" class="auto-style7">公：<asp:TextBox ID="Business_Phone" runat="server" AutoCompleteType="BusinessPhone" TextMode="Phone"></asp:TextBox>
                </td>
                <td class="auto-style7">行動電話：</td>
                <td colspan="2" class="auto-style7">
                    <asp:TextBox ID="Cell_Phone" runat="server" AutoCompleteType="Cellular" TextMode="Phone"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style7">清潔標的<br />
                    (地址)：</td>
                <td class="auto-style7">
                    <asp:DropDownList ID="City_Name" runat="server" AutoPostBack="true" OnSelectedIndexChanged="City_Name_SelectedIndexChanged">
                    </asp:DropDownList>

                    縣市</td>
                <td class="auto-style7">
                    <asp:DropDownList ID="Area_Name" runat="server" AutoPostBack="true">
                    </asp:DropDownList>

                    鄉鎮市區
                </td>

                <td colspan="5" class="auto-style7">
                    <asp:TextBox ID="Home_Addr" runat="server" Width="90%"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <asp:Panel ID="Panel2" runat="server">
                    <td class="auto-style7">單次編號</td>
                    <td class="auto-style7">
                        <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
                    </td>
                    <td class="auto-style7">單次費用</td>
                    <td class="auto-style7">
                        <asp:Label ID="Label3" runat="server" Text="0"></asp:Label>
                    </td>
                </asp:Panel>
            </tr>
            <tr>
                <td class="auto-style7" rowspan="2">
                    <asp:RadioButtonList ID="Contract_Year" runat="server">
                        <asp:ListItem Value="1" Selected="True">一年約</asp:ListItem>
                        <asp:ListItem Value="2">二年約</asp:ListItem>
                        <asp:ListItem Value="0">單次</asp:ListItem>
                    </asp:RadioButtonList></td>
                <td colspan="3" class="auto-style7">起始日期：
                        <asp:TextBox ID="Start_Date" runat="server" Width="90px"></asp:TextBox><asp:Button ID="Button3" runat="server" Text="..." OnClick="Button3_Click" />
                    <asp:Button ID="CDate" runat="server" Text="計算日期" OnClick="CDate_Click"></asp:Button>
                    <asp:Calendar ID="Calendar1" runat="server" Visible="false"  BackColor="White" BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" Height="180px" Width="200px" OnSelectionChanged ="Calendar1_SelectionChanged">
                                    <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                                    <NextPrevStyle VerticalAlign="Bottom" />
                                    <OtherMonthDayStyle ForeColor="#808080" />
                                    <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                    <SelectorStyle BackColor="#CCCCCC" />
                                    <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                                    <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                                    <WeekendDayStyle BackColor="#FFFFCC" />
                                </asp:Calendar>
                    合約次數:<asp:Label ID="Contract_Times" runat="server" Text=""></asp:Label>
                </td>
                <td class="auto-style7">總服務次數：(含假日)</td>
                <td class="auto-style7">

                    <asp:Label ID="TotalC" runat="server" Text="" BackColor="#FFCCFF"></asp:Label>次
                </td>
                <td class="auto-style7">總費用金額</td>
                <td class="auto-style7">

                    <asp:Label ID="TotalFee" runat="server" Text="" BackColor="#FFCCFF"></asp:Label>元

                </td>

            </tr>
            <tr>
                <td class="auto-style7">星期日</td>
                <td class="auto-style7">星期一</td>
                <td class="auto-style7">星期二</td>
                <td class="auto-style7">星期三</td>
                <td class="auto-style7">星期四</td>
                <td class="auto-style7">星期五</td>
                <td class="auto-style7">星期六</td>
            </tr>
            <tr style="background-color: #CCCCCC">
                <td class="auto-style7" style="width: 12%">第一周</td>
                <td class="auto-style7" style="width: 12%">
                    <asp:DropDownList ID="Week_Work1" runat="server"></asp:DropDownList></td>
                <td class="auto-style7" style="width: 12%">
                    <asp:DropDownList ID="Week_Work2" runat="server"></asp:DropDownList></td>
                <td class="auto-style7" style="width: 12%">
                    <asp:DropDownList ID="Week_Work3" runat="server"></asp:DropDownList></td>
                <td class="auto-style7" style="width: 12%">
                    <asp:DropDownList ID="Week_Work4" runat="server"></asp:DropDownList></td>
                <td class="auto-style7" style="width: 12%">
                    <asp:DropDownList ID="Week_Work5" runat="server"></asp:DropDownList></td>
                <td class="auto-style7" style="width: 12%">
                    <asp:DropDownList ID="Week_Work6" runat="server"></asp:DropDownList></td>
                <td class="auto-style7" style="width: 12%">
                    <asp:DropDownList ID="Week_Work7" runat="server"></asp:DropDownList></td>
            </tr>
            <tr>
                <td class="auto-style7">管家數</td>
                <td class="auto-style7">
                    <asp:DropDownList ID="HK_Count1" runat="server"></asp:DropDownList></td>
                <td class="auto-style7">
                    <asp:DropDownList ID="HK_Count2" runat="server"></asp:DropDownList></td>
                <td class="auto-style7">
                    <asp:DropDownList ID="HK_Count3" runat="server"></asp:DropDownList></td>
                <td class="auto-style7">
                    <asp:DropDownList ID="HK_Count4" runat="server"></asp:DropDownList></td>
                <td class="auto-style7">
                    <asp:DropDownList ID="HK_Count5" runat="server"></asp:DropDownList></td>
                <td class="auto-style7">
                    <asp:DropDownList ID="HK_Count6" runat="server"></asp:DropDownList></td>
                <td class="auto-style7">
                    <asp:DropDownList ID="HK_Count7" runat="server"></asp:DropDownList></td>
            </tr>
            <tr style="background-color: #CCCCCC">
                <td class="auto-style7">第二周</td>
                <td class="auto-style7">
                    <asp:DropDownList ID="Week_Work8" runat="server"></asp:DropDownList></td>
                <td class="auto-style7">
                    <asp:DropDownList ID="Week_Work9" runat="server"></asp:DropDownList></td>
                <td class="auto-style7">
                    <asp:DropDownList ID="Week_Work10" runat="server"></asp:DropDownList></td>
                <td class="auto-style7">
                    <asp:DropDownList ID="Week_Work11" runat="server"></asp:DropDownList></td>
                <td class="auto-style7">
                    <asp:DropDownList ID="Week_Work12" runat="server"></asp:DropDownList></td>
                <td class="auto-style7">
                    <asp:DropDownList ID="Week_Work13" runat="server"></asp:DropDownList></td>
                <td class="auto-style7">
                    <asp:DropDownList ID="Week_Work14" runat="server"></asp:DropDownList></td>
            </tr>
            <tr>
                <td class="auto-style7">管家數</td>
                <td class="auto-style7">
                    <asp:DropDownList ID="HK_Count8" runat="server"></asp:DropDownList></td>
                <td class="auto-style7">
                    <asp:DropDownList ID="HK_Count9" runat="server"></asp:DropDownList></td>
                <td class="auto-style7">
                    <asp:DropDownList ID="HK_Count10" runat="server"></asp:DropDownList></td>
                <td class="auto-style7">
                    <asp:DropDownList ID="HK_Count11" runat="server"></asp:DropDownList></td>
                <td class="auto-style7">
                    <asp:DropDownList ID="HK_Count12" runat="server"></asp:DropDownList></td>
                <td class="auto-style7">
                    <asp:DropDownList ID="HK_Count13" runat="server"></asp:DropDownList></td>
                <td class="auto-style7">
                    <asp:DropDownList ID="HK_Count14" runat="server"></asp:DropDownList></td>
            </tr>
            <asp:Panel ID="Panel1" runat="server" Visible="false">
                <tr>
                    <td colspan="8" class="auto-style7">請選擇原因：<asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged">
                            <asp:ListItem>取消修改</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </asp:Panel>
            <tr>
                <td colspan="8" class="auto-style7">
                    <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_ItemDataBound" OnItemCommand="Repeater1_ItemCommand">
                        <ItemTemplate>
                            <asp:Label ID="ID" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "ID") %>'></asp:Label>
                            <asp:Button ID="BDate" runat="server" Width="8%" Text='<%# DataBinder.Eval(Container.DataItem, "WDate", "{0:yyyy/MM/dd}") %>' Height="30px"></asp:Button>
                            <asp:Label ID="BTime" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "WTime") %>'></asp:Label>
                            <asp:Label ID="BPrice" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "WPrice") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:Repeater>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="auto-style7">
                    <asp:Button ID="Button1" runat="server" Text="存檔" OnClick="Button1_Click" />
                    <asp:Button ID="Button2" runat="server" Text="清除" OnClick="Button2_Click" />
                    <asp:CheckBox ID="CheckBox2" runat="server" Text="是否重覆存入!" Visible="False" />
                    <asp:CheckBox ID="CheckBox1" runat="server" Text="是否更新即有客戶資料" Visible="False" />

                </td>
                <td colspan="6" class="auto-style7">
                    <asp:TextBox ID="TextBox7" runat="server" TextMode="MultiLine"></asp:TextBox>
                </td>
            </tr>

        </table>
    </form>

</body>
</html>

