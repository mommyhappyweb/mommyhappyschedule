﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Configuration;
using Models.Library;

namespace MommyHappySchedule.iframe
{
    public partial class JC : Page
    {
        private DataSet MyDataSet = new DataSet();
        private SqlDataAdapter MyCommand;
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        SqlConnection INConnection1 = new SqlConnection(ConnString);
        private SqlCommand INCommand;
        private string INString;

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = "派班工作";
            if (!Page.IsPostBack)
            {
                MyDataSet = new DataSet();
                MyCommand = new SqlDataAdapter("SELECT * FROM Branch_Data Order by ID ", INConnection1);
                MyCommand.Fill(MyDataSet, "Branch");


                Branch.DataSource = MyDataSet.Tables["Branch"].DefaultView;
                Branch.DataTextField = "Branch_Name";
                Branch.DataValueField = "ID";
                Branch.DataBind();

                Show_Date.Text = DateTime.Now.AddDays(1).ToShortDateString();
                PaymentCheck.SelectedIndex = 0;
            }
        }

        protected void Check_Report_Click(object sender, EventArgs e)
        {
            ShareFunction SF = new ShareFunction();
            if (!SF.IsDate(Show_Date.Text))
                return;


            Label2.Text = DateTime.Now.ToLongTimeString();

            string Select_Banch;
            if (Branch.SelectedItem.Text.Trim() == "南高雄")
                Select_Banch = " (Branch_Name ='南高雄' or Branch_Name = '屏東')";
            else
            {
                if (Branch.SelectedIndex == 0)
                    Select_Banch = " 1=1 ";
                else
                    Select_Banch = " Branch_Name ='" + Branch.SelectedItem.Text.Trim() + "'";
            }


            // 找出尚未離職的管家
            MyCommand = new SqlDataAdapter(
                "SELECT Staff_No,Staff_Name,Job_Title FROM Staff_Job_Data_View where (Depart_Date is null or Depart_Date >= '" +
                Show_Date.Text + "') and " + Select_Banch + " and Job_Status = '在職' " + " and Staff_No <> '' and " +
                " Staff_No is not null  Order by Staff_No", INConnection1);
            MyCommand.Fill(MyDataSet, "Staff_Nu");
            string Select_Staff = "";
            foreach (DataRowView Si in MyDataSet.Tables["Staff_Nu"].DefaultView)
                Select_Staff = Select_Staff + "'" + Si[0].ToString().Trim() + "',";
            if (Select_Staff.Length > 0)
                Select_Staff = " Staff_No in (" + Select_Staff.Substring(0, Select_Staff.Length - 1) + ")";

            if (Staff_Name.Text != "")
            {
                Select_Banch = " (Staff_Name like '%" + Staff_Name.Text + "%' or Staff_No like '%" + Staff_Name.Text + "%') ";
                Select_Staff = " (Staff_Name like '%" + Staff_Name.Text + "%' or Staff_No like '%" + Staff_Name.Text + "%') ";
                //以下是針對駐點內尋找
                //Select_Banch = Select_Banch + " and (Staff_Name like '%" + Staff_Name.Text + "%' or Staff_No like '%" + Staff_Name.Text + "%') ";
                //Select_Staff = Select_Staff + " and (Staff_Name like '%" + Staff_Name.Text + "%' or Staff_No like '%" + Staff_Name.Text + "%') ";

            }

            if (Cust_Name.Text != "")
            {
                Select_Banch = "( Cust_Name like '%" + Cust_Name.Text + "%' or Cust_No like '%" + Cust_Name.Text + "%' )";
                Select_Staff = "( Cust_Name like '%" + Cust_Name.Text + "%' or Cust_No like '%" + Cust_Name.Text + "%' )";
                //以下是針對駐點內尋找
                //Select_Banch = Select_Banch + " and Cust_Name like '%" + Cust_Name.Text + "%' or Cust_No like '%" + Cust_Name.Text + "%' ";
                //Select_Staff = Select_Staff + " and Cust_Name like '%" + Cust_Name.Text + "%' or Cust_No like '%" + Cust_Name.Text + "%' ";

            }


            switch (PaymentCheck.SelectedIndex)
            {
                default:
                case 0:
                    {
                        Select_Banch = Select_Banch + " and Job_Flag <> 255";
                        Select_Staff = Select_Staff + " and Job_Flag <> 255";
                        break;
                    }

                case 1:
                    {
                        Select_Banch = Select_Banch + " and Job_Flag >= 2 ";
                        Select_Staff = Select_Staff + " and Job_Flag >= 2 ";
                        break;
                    }

                case 2:
                    {
                        Select_Banch = Select_Banch + " and ((Job_Flag < 2 or Job_Flag is null ) and Job_Flag <> 255)  ";
                        Select_Staff = Select_Staff + " and ((Job_Flag < 2 or Job_Flag is null ) and Job_Flag <> 255)  ";
                        break;
                    }
            }



            MyDataSet = new DataSet();
            //配合可以跨區支援,改判斷管家編號並顯示工作
            if (Select_Staff.Length > 0)
                MyCommand = new SqlDataAdapter(
                    "SELECT *,0 as SUID FROM Job_Schedule_Confirm_View where Job_Date = '" + Show_Date.Text + "' and " + Select_Staff +
                    "  Order by Job_Time,Staff_No,Cust_No", INConnection1);
            else
                MyCommand = new SqlDataAdapter(
                    "SELECT *,0 as SUID FROM Job_Schedule_Confirm_View where Job_Date = '" + Show_Date.Text + "' and " + Select_Banch +
                    "  Order by Job_Time,Staff_No,Cust_No", INConnection1);

            MyCommand.Fill(MyDataSet, "Cust_Job_Schedule");

            //公督訓
            //配合可以跨區支援,改判斷管家編號並顯示工作
            if (Select_Staff.Length > 0)
                MyCommand = new SqlDataAdapter(
                    "SELECT *,1 as SUID FROM Job_Schedule_Supervision_Confirm_View where Job_Date = '" + Show_Date.Text + "' and " + Select_Staff +
                    "  Order by Job_Time,Staff_No,Cust_No", INConnection1);
            else
                MyCommand = new SqlDataAdapter(
                    "SELECT ,1 as SUID FROM Job_Schedule_Supervision_Confirm_View where Job_Date = '" + Show_Date.Text + "' and " + Select_Banch +
                    "  Order by Job_Time,Staff_No,Cust_No", INConnection1);

            MyCommand.Fill(MyDataSet, "Cust_Job_Schedule");

            MyDataSet.Tables["Cust_Job_Schedule"].DefaultView.Sort = "Branch_Name Asc,Job_Time Asc,Staff_No Asc,Cust_No Asc";
            DataGrid1.DataSource = MyDataSet.Tables["Cust_Job_Schedule"].DefaultView;
            DataGrid1.DataBind();

            Label2.Text = Label2.Text + "-" + DateTime.Now.ToLongTimeString();
        }

        public void Show_Sc_Type(object sender, DataGridItemEventArgs e)
        {
            DataGrid1.UseAccessibleHeader = true;
            ListItemType objItemType = e.Item.ItemType;
            Label TmpLabel;
            RadioButtonList tmpRadioButtonList;


            if (objItemType == ListItemType.Item | objItemType == ListItemType.AlternatingItem)
            {
                TmpLabel = (Label)e.Item.FindControl("Label3");
                tmpRadioButtonList = (RadioButtonList)e.Item.FindControl("RadioButtonList3");
                if (e.Item.Cells[16].Text != "0") //公督訓
                {
                    e.Item.BackColor = Color.LightGray;
                    tmpRadioButtonList.Enabled = false;
                }

                switch (e.Item.Cells[14].Text)
                {
                    case "0":
                        {
                            TmpLabel.Text = "未繳費";
                            //tmpRadioButtonList.Enabled = false;    //先暫時不用
                            break;
                        }

                    case "1":
                        {
                            TmpLabel.Text = "可排班";
                            break;
                        }

                    case "2":
                        {
                            TmpLabel.Text = "已確認";
                            break;
                        }

                    case "3":
                        {
                            TmpLabel.Text = "服務中";
                            TmpLabel.BackColor = Color.LightGreen;
                            tmpRadioButtonList.Enabled = false;
                            break;
                        }
                    case "4":
                        {
                            TmpLabel.Text = "已服務";
                            TmpLabel.BackColor = Color.LightPink;
                            tmpRadioButtonList.Enabled = false;
                            break;
                        }
                }
            }
        }

        public void Show_Job_Detail(object sender, DataGridCommandEventArgs e)
        {
        }


        protected void Change_Screen(object sender, EventArgs e)
        {
            if (((Control)sender).ID == "Left_Screen")
                Show_Date.Text = Convert.ToDateTime(Show_Date.Text).AddDays(-1).ToShortDateString();
            if (((Control)sender).ID == "Right_Screen")
                Show_Date.Text = Convert.ToDateTime(Show_Date.Text).AddDays(1).ToShortDateString();
            if ((Convert.ToDateTime(Show_Date.Text) - Convert.ToDateTime(DateTime.Now.ToShortDateString())).TotalDays < 0)
                SaveBtn.Enabled = false;
            else
                SaveBtn.Enabled = true;
            DetermineCheck.ClearSelection();
            PaymentCheck.ClearSelection();

            Check_Report_Click(sender, e);
        }


        protected void PaymentCheckEvent(object sender, EventArgs e)
        {
            DetermineCheck.ClearSelection();
            Check_Report_Click(sender, e);
        }


        protected void Remove_Report_Click(object sender, EventArgs e)
        {
            Staff_Name.Text = "";
            Cust_Name.Text = "";
        }
        protected void DetermineCheckEvent(object sender, EventArgs e)
        {
            RadioButtonList tmpRadioButtonList;
            int Gi;

            for (Gi = 0; Gi <= DataGrid1.Items.Count - 1; Gi++)
            {
                tmpRadioButtonList = (RadioButtonList)DataGrid1.Items[Gi].FindControl("RadioButtonList3");
                if (tmpRadioButtonList != null)
                    if (DetermineCheck.SelectedValue == "1" || DetermineCheck.SelectedValue == "0")
                    {
                        tmpRadioButtonList.SelectedIndex = Convert.ToInt32(DetermineCheck.SelectedValue);
                    }
                    else
                    {
                        Response.Redirect(Request.Url.ToString());
                    }
            }

            Page_Load(sender, e);
        }
        protected void SaveBtnClick(object sender, EventArgs e)
        {
            RadioButtonList tmpRadioButtonList;
            int tmpID;
            int Gi;

            Label4.Text = "";
            for (Gi = 0; Gi <= DataGrid1.Items.Count - 1; Gi++)
            {
                tmpID = Convert.ToInt32(DataGrid1.Items[Gi].Cells[0].Text);
                tmpRadioButtonList = (RadioButtonList)DataGrid1.Items[Gi].FindControl("RadioButtonList3");
                if (tmpRadioButtonList != null)
                {
                    //不是公督訓的記錄
                    if (tmpRadioButtonList.SelectedIndex >= 0 & DataGrid1.Items[Gi].Cells[16].Text == "0")
                    {
                        if (tmpRadioButtonList.SelectedIndex == 0)
                            INString = "Update Cust_Job_Schedule set Job_Flag = 2 where Job_Flag <=1 and  ID = " + tmpID;
                        else
                            INString = "Update Cust_Job_Schedule set Job_Flag = 1 where Job_Flag <=2 and  ID = " + tmpID;
                        INCommand = new SqlCommand(INString, INConnection1);
                        INCommand.Connection.Open();
                        INCommand.ExecuteNonQuery();
                        INCommand.Connection.Close();
                    }
                }
            }
            DetermineCheck.ClearSelection();
            PaymentCheck.ClearSelection();
            Check_Report_Click(sender, e);
        }

    }

}