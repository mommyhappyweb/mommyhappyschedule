﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Configuration;
using Models.Library;

namespace MommyHappySchedule.iframe
{
    public partial class QuitJobReport : Page
    {
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];

        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Title = "離職率報表";
        

            if (!Page.IsPostBack)
            {

            }
        }

        protected void SearchClick(object sender, EventArgs e)
        {

        }
    }

}