﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Text.RegularExpressions;

namespace MommyHappySchedule.iframe
{
    public partial class Recruit : System.Web.UI.Page
    {

        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        System.Data.SqlClient.SqlConnection INConnection1 = new System.Data.SqlClient.SqlConnection(ConnString);
        System.Data.SqlClient.SqlConnection INConnection2 = new System.Data.SqlClient.SqlConnection(ConnString);
        private System.Data.SqlClient.SqlCommand INCommand, INCommand1;
        private System.Data.SqlClient.SqlDataReader SQLReader, SQLReader1;
        private System.Data.DataSet MyDataSet;
        private System.Data.SqlClient.SqlDataAdapter MyCommand;
        private System.Data.DataTable dt;
        private System.Data.DataRow dr;
        

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // http://211.20.204.77/cdrnew.php?page=2&numItems=1000&PageSize=500&i=1
            if ((string)Session["account"] == "")
            {
                Session["msg"] = "您沒有使用此程式的權限!!";
                Response.Redirect("Index.aspx");
            }
            if (!Page.IsPostBack)
            {
                MyDataSet = new System.Data.DataSet();
                MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Branch_Data", INConnection1);
                MyCommand.Fill(MyDataSet, "Branch");

                Branch.DataSource = MyDataSet.Tables["Branch"].DefaultView;
                Branch.DataTextField = "Branch_Name";
                Branch.DataValueField = "ID";
                Branch.DataBind();

                // Join Media
                MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT Recruit_Source  FROM Recruit_Source_Data Group by Recruit_Source", INConnection1);
                MyCommand.Fill(MyDataSet, "Recruit_Source");

                Join_Media_Type.DataSource = MyDataSet.Tables["Recruit_Source"].DefaultView;
                Join_Media_Type.DataTextField = "Recruit_Source";
                Join_Media_Type.DataValueField = "Recruit_Source";
                Join_Media_Type.DataBind();


                MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Recruit_Source_Data ", INConnection1);
                MyCommand.Fill(MyDataSet, "Recruit_Source_Data");
                MyDataSet.Tables["Recruit_Source_Data"].DefaultView.RowFilter = "Recruit_Source='人力銀行'";
                Join_Media_Select.DataSource = MyDataSet.Tables["Recruit_Source_Data"].DefaultView;
                Join_Media_Select.DataTextField = "Source_Detail";
                Join_Media_Select.DataValueField = "Source_Detail";
                Join_Media_Select.DataBind();

                // City Name
                MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                            "SELECT City_Name FROM Addr_Code GROUP BY LEFT (Addr_Code, 1), City_Name ORDER BY LEFT (Addr_Code, 1)", INConnection1);
                MyCommand.Fill(MyDataSet, "CityName");
                System.Data.DataRow TR;
                TR = MyDataSet.Tables["CityName"].NewRow();
                TR[0] = "請選擇";
                MyDataSet.Tables["CityName"].Rows.InsertAt(TR, 0);

                City_Name.DataSource = MyDataSet.Tables["CityName"].DefaultView;
                City_Name.DataTextField = "City_Name";
                City_Name.DataValueField = "City_Name";
                City_Name.DataBind();
                City_Name1.DataSource = MyDataSet.Tables["CityName"].DefaultView;
                City_Name1.DataTextField = "City_Name";
                City_Name1.DataValueField = "City_Name";
                City_Name1.DataBind();
                Work_City_Name.DataSource = MyDataSet.Tables["CityName"].DefaultView;
                Work_City_Name.DataTextField = "City_Name";
                Work_City_Name.DataValueField = "City_Name";
                Work_City_Name.DataBind();
                Birth_City.DataSource = MyDataSet.Tables["CityName"].DefaultView;
                Birth_City.DataTextField = "City_Name";
                Birth_City.DataValueField = "City_Name";
                Birth_City.DataBind();

                // AreaName
                MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                            "SELECT Country_Name  FROM Addr_Code  where City_Name = '" + City_Name1.SelectedValue + "'  Group by Country_Name ", INConnection1);
                MyCommand.Fill(MyDataSet, "AreaName");
                TR = MyDataSet.Tables["AreaName"].NewRow();
                TR[0] = "請選擇";
                MyDataSet.Tables["AreaName"].Rows.InsertAt(TR, 0);

                Area_Name1.DataSource = MyDataSet.Tables["AreaName"].DefaultView;
                Area_Name1.DataTextField = "Country_Name";
                Area_Name1.DataValueField = "Country_Name";
                Area_Name1.DataBind();

                Area_Name.DataSource = MyDataSet.Tables["AreaName"].DefaultView;
                Area_Name.DataTextField = "Country_Name";
                Area_Name.DataValueField = "Country_Name";
                Area_Name.DataBind();


                dt = new System.Data.DataTable();
                dt.Columns.Add(new System.Data.DataColumn("Year", typeof(string)));

                int ii;

                for (ii = 1; ii <= 300; ii++)
                {
                    dr = dt.NewRow();
                    dr[0] = ii;
                    dt.Rows.Add(dr);
                }
                Birth_Year.DataSource = dt.DefaultView;
                Birth_Year.DataTextField = "Year";
                Birth_Year.DataValueField = "Year";
                Birth_Year.DataBind();
                Birth_Year.SelectedValue = (DateTime.Now.Year - 1911).ToString();

                dt = new System.Data.DataTable();
                dt.Columns.Add(new System.Data.DataColumn("Year", typeof(string)));

                for (ii = 1900; ii <= 2099; ii++)
                {
                    dr = dt.NewRow();
                    dr[0] = ii;
                    dt.Rows.Add(dr);
                }
                Train_Year.DataSource = dt.DefaultView;
                Train_Year.DataTextField = "Year";
                Train_Year.DataValueField = "Year";
                Train_Year.DataBind();
                Train_Year.SelectedValue = DateTime.Now.Year.ToString();

                PJ_SY1.DataSource = dt.DefaultView;
                PJ_SY1.DataTextField = "Year";
                PJ_SY1.DataValueField = "Year";
                PJ_SY1.DataBind();
                PJ_SY1.SelectedValue = DateTime.Now.Year.ToString();

                PJ_SY2.DataSource = dt.DefaultView;
                PJ_SY2.DataTextField = "Year";
                PJ_SY2.DataValueField = "Year";
                PJ_SY2.DataBind();
                PJ_SY2.SelectedValue = DateTime.Now.Year.ToString();

                PJ_SY3.DataSource = dt.DefaultView;
                PJ_SY3.DataTextField = "Year";
                PJ_SY3.DataValueField = "Year";
                PJ_SY3.DataBind();
                PJ_SY3.SelectedValue = DateTime.Now.Year.ToString();

                PJ_EY1.DataSource = dt.DefaultView;
                PJ_EY1.DataTextField = "Year";
                PJ_EY1.DataValueField = "Year";
                PJ_EY1.DataBind();
                PJ_EY1.SelectedValue = DateTime.Now.Year.ToString();

                PJ_EY2.DataSource = dt.DefaultView;
                PJ_EY2.DataTextField = "Year";
                PJ_EY2.DataValueField = "Year";
                PJ_EY2.DataBind();
                PJ_EY2.SelectedValue = DateTime.Now.Year.ToString();

                PJ_EY3.DataSource = dt.DefaultView;
                PJ_EY3.DataTextField = "Year";
                PJ_EY3.DataValueField = "Year";
                PJ_EY3.DataBind();
                PJ_EY3.SelectedValue = DateTime.Now.Year.ToString();

                Recruit_Date.Text = DateTime.Now.ToShortDateString(); // Interview_Date期
                Recruit_Date.ReadOnly = false;

                if (Request.QueryString.Count > 0)
                {
                    string RID = Request.QueryString["ID"];
                    string RBranch = Request.QueryString["Branch"];
                    Recruit_Date.ReadOnly = true;

                    if (!(RBranch == null))
                        Branch.SelectedValue = Convert.ToInt32(RBranch).ToString();
                    else
                    {
                        string INString;

                        INString = "Select * from Recruit_Data where ID = " + Request.QueryString[0].ToString();

                        INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                        INCommand.Connection.Open();
                        SQLReader = INCommand.ExecuteReader();
                        if (SQLReader.Read() == true)
                        {
                            Branch.SelectedItem.Text = SQLReader[25].ToString().Trim();

                            EM_NO.Text = SQLReader[1].ToString();
                            EM_Name.Text = SQLReader[2].ToString();
                            if (!DBNull.Value.Equals(SQLReader[3]))
                                EM_SEX.SelectedValue = SQLReader[3].ToString();
                            Birth_Year.SelectedValue = (Convert.ToDateTime(SQLReader[4]).Year - 1911).ToString();
                            Birth_Month.SelectedValue = Convert.ToDateTime(SQLReader[4]).Month.ToString();
                            Birth_Day.SelectedValue = Convert.ToDateTime(SQLReader[4]).Day.ToString();
                            if (!DBNull.Value.Equals(SQLReader[5]))
                                Birth_City.SelectedValue = SQLReader[5].ToString();
                            EM_Height.Text = SQLReader[7].ToString();
                            EM_Weight.Text = SQLReader[8].ToString();
                            if (!DBNull.Value.Equals(SQLReader[9]))
                                Marrige_Status.SelectedValue = SQLReader[9].ToString();
                            Child_Number.Text = SQLReader[10].ToString();

                            if (!DBNull.Value.Equals(SQLReader[11]))
                            {
                                int kk = 0;
                                ListItem tmpItem;
                                string[] tmpArray = SQLReader[11].ToString().Split('-');
                                for (kk = 0; kk <= tmpArray.Length - 2; kk++)
                                {
                                    tmpItem = Child_TakeCare.Items.FindByText(tmpArray[kk]);
                                    if (!(tmpItem == null))
                                        tmpItem.Selected = true;
                                }
                                TakeCare_Text.Text = tmpArray[kk];
                            }
                            EM_ID.Text = SQLReader[12].ToString();
                            // 通訊地址 和 戶籍地址
                            if (!(DBNull.Value.Equals(SQLReader[13]) | DBNull.Value.Equals(SQLReader[14]) | DBNull.Value.Equals(SQLReader[15])))
                            {
                                // 如果通訊地址 和 戶籍地址 相同
                                if (SQLReader[13] == SQLReader[16] & SQLReader[14] == SQLReader[17] & SQLReader[15] == SQLReader[18])
                                {
                                    Book_Addr.SelectedIndex = 0;
                                    City_Name1.Enabled = false;
                                    Area_Name1.Enabled = false;
                                    Road_Name1.Enabled = false;
                                }
                                else
                                {
                                    City_Name1.SelectedValue = SQLReader[16].ToString();
                                    Area_Name1.SelectedValue = SQLReader[17].ToString();
                                    Addr_Text1.Text = SQLReader[18].ToString();
                                }
                                City_Name.SelectedValue = SQLReader[13].ToString();
                                Area_Name.SelectedValue = SQLReader[14].ToString();
                                Addr_Text.Text = SQLReader[15].ToString();
                            }

                            Cell_Number.Text = SQLReader[19].ToString();
                            Home_Tele_Number.Text = SQLReader[21].ToString();
                            Recruit_Date.Text = Convert.ToDateTime(SQLReader[22].ToString()).ToShortDateString();


                            if (!DBNull.Value.Equals(SQLReader[26]))
                            {
                                Recruit_Job_Type.SelectedValue = SQLReader[26].ToString();
                                if (Recruit_Job_Type.SelectedIndex == 2)
                                    Recruit_Job_Other.Text = SQLReader[26].ToString().Substring(SQLReader[26].ToString().IndexOf('-') + 1);
                            }

                            EM_Contact_Name.Text = SQLReader[27].ToString();
                            EM_Contact_Re.Text = SQLReader[28].ToString();
                            EM_Contact_Number.Text = SQLReader[29].ToString();

                            // Join Media
                            // If Not IsDBNull(SQLReader(30)) Then
                            // Join_Media.SelectedValue = SQLReader(30).ToString
                            // If Join_Media.SelectedIndex > 1 Then
                            // Join_Media_Text.Text = Right(SQLReader(30).ToString, SQLReader(30).ToString.Length - InStr("-", SQLReader(30).ToString))
                            // End If
                            // End If
                            if (!DBNull.Value.Equals(SQLReader[30]))
                            {
                                string[] tmpArray = SQLReader[30].ToString().Split('-');
                                MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Recruit_Source_Data ", INConnection2);
                                MyCommand.Fill(MyDataSet, "Recruit_Source_Data");
                                if (tmpArray.Length > 2)
                                {
                                    Join_Media.SelectedValue = tmpArray[0];

                                    MyDataSet.Tables["Recruit_Source_Data"].DefaultView.RowFilter = "Recruit_Source='" + tmpArray[0] + "'";
                                    Join_Media_Select.DataSource = MyDataSet.Tables["Recruit_Source_Data"].DefaultView;
                                    Join_Media_Select.DataTextField = "Source_Detail";
                                    Join_Media_Select.DataValueField = "Source_Detail";
                                    Join_Media_Select.DataBind();
                                    Join_Media_Select.SelectedValue = tmpArray[1];
                                    Join_Media_Text.Text = tmpArray[2];
                                }
                                else if (tmpArray.Length > 1)
                                {
                                    Join_Media.SelectedValue = tmpArray[0];
                                    MyDataSet.Tables["Recruit_Source_Data"].DefaultView.RowFilter = "Recruit_Source='" + tmpArray[0] + "'";
                                    Join_Media_Select.DataSource = MyDataSet.Tables["Recruit_Source_Data"].DefaultView;
                                    Join_Media_Select.DataTextField = "Source_Detail";
                                    Join_Media_Select.DataValueField = "Source_Detail";
                                    Join_Media_Select.DataBind();
                                    Join_Media_Select.SelectedValue = tmpArray[1];
                                }
                                else
                                {
                                    Join_Media.SelectedValue = "其它";
                                    MyDataSet.Tables["Recruit_Source_Data"].DefaultView.RowFilter = "Recruit_Source='其它'";
                                    Join_Media_Select.DataSource = MyDataSet.Tables["Recruit_Source_Data"].DefaultView;
                                    Join_Media_Select.DataTextField = "Source_Detail";
                                    Join_Media_Select.DataValueField = "Source_Detail";
                                    Join_Media_Select.DataBind();
                                    Join_Media_Select.SelectedValue = "其它";
                                    Join_Media_Text.Text = tmpArray[0];
                                }
                            }


                            if (!DBNull.Value.Equals(SQLReader[31]))
                            {
                                int kk = 0;
                                ListItem tmpItem;
                                string[] tmpArray = SQLReader[31].ToString().Split('-');
                                for (kk = 0; kk <= tmpArray.Length - 2; kk++)
                                {
                                    tmpItem = Trans_Type.Items.FindByText(tmpArray[kk]);
                                    if (!(tmpItem == null))
                                        tmpItem.Selected = true;
                                }
                                Trans_Type_Text.Text = tmpArray[kk];
                            }

                            if (!DBNull.Value.Equals(SQLReader[32]))
                            {
                                int kk = 0;
                                ListItem tmpItem;
                                string[] tmpArray = SQLReader[32].ToString().Split('-');
                                for (kk = 0; kk <= tmpArray.Length - 1; kk++)
                                {
                                    tmpItem = Driver_License.Items.FindByText(tmpArray[kk]);
                                    if (!(tmpItem == null))
                                        tmpItem.Selected = true;
                                }
                            }

                            if (!DBNull.Value.Equals(SQLReader[33]))
                            {
                                int kk = 0;
                                ListItem tmpItem;
                                string[] tmpArray = SQLReader[33].ToString().Split('-');
                                for (kk = 0; kk <= tmpArray.Length - 1; kk++)
                                {
                                    tmpItem = Language_Type.Items.FindByText(tmpArray[kk]);
                                    if (!(tmpItem == null))
                                        tmpItem.Selected = true;
                                }
                            }

                            if (!DBNull.Value.Equals(SQLReader[34]))
                            {
                                int kk = 0;
                                ListItem tmpItem;
                                string[] tmpArray = SQLReader[34].ToString().Split('-');
                                for (kk = 0; kk <= tmpArray.Length - 2; kk++)
                                {
                                    tmpItem = Car_Surgery.Items.FindByText(tmpArray[kk]);
                                    if (!(tmpItem == null))
                                        tmpItem.Selected = true;
                                }
                                Car_Surgery_Reason.Text = tmpArray[kk];
                            }

                            if (!DBNull.Value.Equals(SQLReader[35]))
                            {
                                int kk = 0;
                                ListItem tmpItem;
                                string[] tmpArray = SQLReader[35].ToString().Split('-');
                                for (kk = 0; kk <= tmpArray.Length - 3; kk++)
                                {
                                    tmpItem = Disable_Status.Items.FindByText(tmpArray[kk]);
                                    if (!(tmpItem == null))
                                        tmpItem.Selected = true;
                                }
                                Disable_Type.Text = tmpArray[kk];
                                Disable_Level.Text = tmpArray[kk + 1];
                            }

                            if (!DBNull.Value.Equals(SQLReader[36]))
                            {
                                int kk = 0;
                                ListItem tmpItem;
                                string[] tmpArray = SQLReader[36].ToString().Split('-');
                                for (kk = 0; kk <= tmpArray.Length - 2; kk++)
                                {
                                    tmpItem = Health_Status.Items.FindByText(tmpArray[kk]);
                                    if (!(tmpItem == null))
                                        tmpItem.Selected = true;
                                }
                                Health_Text.Text = tmpArray[kk];
                            }

                            if (!DBNull.Value.Equals(SQLReader[37]))
                                Internet_status.SelectedIndex = Convert.ToInt16(SQLReader[37]);

                            if (!DBNull.Value.Equals(SQLReader[38]))
                                Family_support.SelectedIndex = Convert.ToInt16(SQLReader[38]);

                            if (!DBNull.Value.Equals(SQLReader[39]))
                                loan_Status.SelectedIndex = Convert.ToInt16(SQLReader[39]);

                            if (!DBNull.Value.Equals(SQLReader[40]))
                                Internet_status.SelectedIndex = Convert.ToInt16(SQLReader[40]);

                            if (!DBNull.Value.Equals(SQLReader[41]))
                            {
                                int kk = 0;
                                ListItem tmpItem;
                                string[] tmpArray = SQLReader[41].ToString().Split('-');
                                for (kk = 0; kk <= tmpArray.Length - 2; kk++)
                                {
                                    tmpItem = Free_Insurance_Status.Items.FindByText(tmpArray[kk]);
                                    if (!(tmpItem == null))
                                        tmpItem.Selected = true;
                                }
                                Free_Insurance_Text.Text = tmpArray[kk];
                            }

                            if (!DBNull.Value.Equals(SQLReader[42]))
                                Family_IN_Number.Text = SQLReader[42].ToString();

                            if (!DBNull.Value.Equals(SQLReader[43]))
                                Raise_Number.Text = SQLReader[43].ToString();

                            if (!DBNull.Value.Equals(SQLReader[44]))
                            {
                                int kk = 0;
                                ListItem tmpItem;
                                string[] tmpArray = SQLReader[44].ToString().Split('-');
                                for (kk = 0; kk <= tmpArray.Length - 2; kk++)
                                {
                                    tmpItem = Insurance_Status.Items.FindByText(tmpArray[kk]);
                                    if (!(tmpItem == null))
                                        tmpItem.Selected = true;
                                }
                                Insurance_Text.Text = tmpArray[kk];
                            }

                            if (!DBNull.Value.Equals(SQLReader[45]))
                            {
                                Train_Year.SelectedValue = Convert.ToDateTime(SQLReader[45]).Year.ToString();
                                Train_Month.SelectedValue = Convert.ToDateTime(SQLReader[45]).Month.ToString();
                                Train_Day.SelectedValue = Convert.ToDateTime(SQLReader[45]).Day.ToString();
                            }

                            if (!DBNull.Value.Equals(SQLReader[46]))
                            {
                                if (Convert.ToInt16(SQLReader[46]) == 1)
                                    Agree_O_County.Checked = true;
                            }

                            if (!DBNull.Value.Equals(SQLReader[47]))
                                Work_City_Name.SelectedValue = SQLReader[47].ToString();

                            if (!DBNull.Value.Equals(SQLReader[48]))
                            {
                                if (Convert.ToInt16(SQLReader[48]) == 1)
                                    Agree_Private_Data.Checked = true;
                            }

                            if (!DBNull.Value.Equals(SQLReader[49]))
                                Interview_Result.SelectedIndex = Convert.ToInt16(SQLReader[49]);


                            if (!DBNull.Value.Equals(SQLReader[50]))
                                Duty_Date.Text = SQLReader[50].ToString();

                            Interview_Text.Text = SQLReader[52].ToString();

                            if (!DBNull.Value.Equals(SQLReader[53]))
                                Interview_Salary.Text = SQLReader[53].ToString();

                            // Graduate School
                            INString = "Select * from Recruit_School_Data where ID = " + Request.QueryString[0].ToString();

                            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
                            INCommand.Connection.Open();
                            SQLReader1 = INCommand.ExecuteReader();
                            while (SQLReader1.Read())
                            {
                                School_Name.Text = SQLReader1[1].ToString();
                                Class_Name.Text = SQLReader1[2].ToString();
                                School_Period.Text = SQLReader1[3].ToString();
                                Gradu_Status.SelectedIndex = Convert.ToInt16(SQLReader1[4]);
                            }
                            INCommand.Connection.Close();

                            // Job Experience
                            INString = "Select * from Recruit_Job_Experi where ID = " + Request.QueryString[0].ToString();

                            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
                            INCommand.Connection.Open();
                            SQLReader1 = INCommand.ExecuteReader();
                            int iii = 1;
                            TextBox PreJobName, PreJobTitle, PJSA;
                            DropDownList PJSY, PJSM, PJEY, PJEM;

                            while (SQLReader1.Read())
                            {
                                PreJobName = (TextBox)FindControl("Pre_Job_Name" + iii.ToString("0"));
                                PreJobTitle = (TextBox)FindControl("Pre_Job_Title" + iii.ToString("0"));
                                PJSA = (TextBox)FindControl("PJ_SA" + iii.ToString("0"));
                                PJSY = (DropDownList)FindControl("PJ_SY" + iii.ToString("0"));
                                PJSM = (DropDownList)FindControl("PJ_SM" + iii.ToString("0"));
                                PJEY = (DropDownList)FindControl("PJ_EY" + iii.ToString("0"));
                                PJEM = (DropDownList)FindControl("PJ_EM" + iii.ToString("0"));

                                PreJobName.Text = SQLReader1[1].ToString();
                                PreJobTitle.Text = SQLReader1[2].ToString();
                                PJSA.Text = SQLReader1[5].ToString();
                                PJSY.SelectedValue = SQLReader1[3].ToString().Substring(0, 4);
                                PJSM.SelectedValue = SQLReader1[3].ToString().Substring(SQLReader1[3].ToString().Length - 2, 2);
                                PJEY.SelectedValue = SQLReader1[4].ToString().Substring(0, 4);
                                PJEM.SelectedValue = SQLReader1[4].ToString().Substring(SQLReader1[4].ToString().Length - 2, 2);

                                iii = iii + 1;
                                if (iii > 3)
                                    break;
                            }
                            INCommand.Connection.Close();

                            // Family 
                            INString = "Select * from Recruit_Family_Data where ID = " + Request.QueryString[0].ToString();

                            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
                            INCommand.Connection.Open();
                            SQLReader1 = INCommand.ExecuteReader();
                            iii = 1;
                            TextBox FGC, FGNAME, FGCOM, FGAGE;

                            while (SQLReader1.Read())
                            {
                                FGC = (TextBox)FindControl("F_G_C" + iii.ToString("0"));
                                FGNAME = (TextBox)FindControl("F_G_Name" + iii.ToString("0"));
                                FGCOM = (TextBox)FindControl("F_G_Company" + iii.ToString("0"));
                                FGAGE = (TextBox)FindControl("F_G_Age" + iii.ToString("0"));

                                FGC.Text = SQLReader1[1].ToString();
                                FGNAME.Text = SQLReader1[2].ToString();
                                FGCOM.Text = SQLReader1[3].ToString();
                                FGAGE.Text = (DateTime.Now.Year - Convert.ToInt32(SQLReader1[4])).ToString();

                                iii = iii + 1;
                                if (iii > 4)
                                    break;
                            }
                            INCommand.Connection.Close();
                        }
                        INCommand.Connection.Close();
                    }
                }
            }
        }

        protected void City_Name_SelectedIndexChanged(object sender, EventArgs e)
        {
            MyDataSet = new System.Data.DataSet();
            MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                        "SELECT Country_Name  FROM Addr_Code  where City_Name = '" + City_Name.SelectedValue + "'  Group by Country_Name ", INConnection1);
            MyCommand.Fill(MyDataSet, "AreaName");
            System.Data.DataRow TR;
            TR = MyDataSet.Tables["AreaName"].NewRow();
            TR[0] = "請選擇";
            MyDataSet.Tables["AreaName"].Rows.InsertAt(TR, 0);

            switch (((Control)sender).ID) 
            {
                case "City_Name":
                    {
                        Area_Name.DataSource = MyDataSet.Tables["AreaName"].DefaultView;
                        Area_Name.DataTextField = "Country_Name";
                        Area_Name.DataValueField = "Country_Name";
                        Area_Name.DataBind();
                        break;
                    }

                case "City_Name1":
                    {
                        Area_Name.DataSource = MyDataSet.Tables["AreaName"].DefaultView;
                        Area_Name.DataTextField = "Country_Name";
                        Area_Name.DataValueField = "Country_Name";
                        Area_Name.DataBind();
                        break;
                    }
            }



            Page_Load(sender, e);
        }

        protected void Area_Name_SelectedIndexChanged(object sender, EventArgs e)
        {
            MyDataSet = new System.Data.DataSet();
            MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                        "SELECT Road_Name  FROM Addr_Code where City_Name = '" + City_Name.SelectedValue + "' and Country_Name = '" + Area_Name.SelectedValue +
                        "'", INConnection1);
            MyCommand.Fill(MyDataSet, "RoadName");
            System.Data.DataRow TR;
            TR = MyDataSet.Tables["RoadName"].NewRow();
            TR[0] = "請選擇";
            MyDataSet.Tables["RoadName"].Rows.InsertAt(TR, 0);

            Road_Name.DataSource = MyDataSet.Tables["RoadName"].DefaultView;
            Road_Name.DataTextField = "Road_Name";
            Road_Name.DataValueField = "Road_Name";

            Road_Name.DataBind();
            Page_Load(sender, e);
        }
        protected void Book_Addr_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Book_Addr.SelectedValue == "同上")
            {
                City_Name1.Enabled = false;
                Area_Name1.Enabled = false;
                Road_Name1.Enabled = false;
            }
            else
            {
                City_Name1.Enabled = true;
                Area_Name1.Enabled = true;
                Road_Name1.Enabled = true;
            }
            Page_Load(sender, e);
        }
        protected void Save_Data_Click(object sender, EventArgs e)
        {
            string INString;
            System.Data.SqlClient.SqlConnection INConnection = new System.Data.SqlClient.SqlConnection(ConnString);
            System.Data.SqlClient.SqlDataReader SQLReader;
            string ErrorMsg = "";
            string Health_Status_String = "";
            string Trans_String = "";
            string Driver_License_String = "";
            string Language_Type_String = "";
            string Child_Care_String = "";
            string Duty_Date_String = "";

            if (Recruit_Job_Type.SelectedIndex < 0)
                // ErrorMsg = ErrorMsg & "\n" & "【應徵職務】未選擇"
                Recruit_Job_Type.SelectedIndex = 0;
            if (Recruit_Job_Type.SelectedValue == "其它" & Recruit_Job_Other.Text == "")
            {
            }
            if (EM_NO.Text == "")
                ErrorMsg = ErrorMsg + @"\n" + "【面試者】未輸入";
            if (EM_Name.Text == "")
                ErrorMsg = ErrorMsg + @"\n" + "【Name】未輸入";
            if (EM_ID.Text == "" | EM_ID.Text.Trim().Length != 10)
                ErrorMsg = ErrorMsg + @"\n" + "【身份証字號】輸入錯誤";
            else
            {
                string arg_Identify = EM_ID.Text.Trim().ToUpper();

                if (Regex.IsMatch(arg_Identify, @"^[A-Z][A-D]\d{8}$"))   //統一證號
                {
                    var cityCode = new[] { 10, 11, 12, 13, 14, 15, 16, 17, 34, 18, 19, 20, 21, 22, 35, 23, 24, 25, 26, 27, 28, 29, 32, 30, 31, 33 };
                    // 計算時使用的容器，最後一個位置拿來放檢查碼，所以有11個位置(縣市區碼佔2個位置)
                    var valueContainer = new int[11];
                    valueContainer[0] = cityCode[arg_Identify[0] - 65] / 10; // 區域碼十位數
                    valueContainer[1] = cityCode[arg_Identify[0] - 65] % 10; // 區域碼個位數
                    valueContainer[2] = cityCode[arg_Identify[1] - 65] % 10; // 性別碼個位數
                                                                              // 證號執行特定數規則所產生的結果值的加總，這裡把初始值訂為區域碼的十位數數字(特定數為1，所以不用乘)
                    var sumVal = valueContainer[0];

                    // 迴圈執行特定數規則
                    for (var i = 1; i <= 9; i++)
                    {
                        // 跳過性別碼，如果是一般身分證字號則不用跳過
                        if (i > 1)
                            // 將當前證號於索引位置的數字放到容器的下一個索引的位置
                            valueContainer[i + 1] = arg_Identify[i] - 48;

                        // 特定數為: 1987654321 ，因為首個數字1已經在sumVal初始值算過了，所以這裡從9開始
                        sumVal += valueContainer[i] * (10 - i);
                    }

                    // 此為「檢查碼 = 10 - 總和值的個位數數字 ; 若個位數為0則取0為檢查碼」的反推
                    if ((sumVal + valueContainer[10]) % 10 != 0)
                        ErrorMsg = ErrorMsg + @"\n" + "【身份証字號】輸入錯誤";

                }
                else   //身分証號
                {
                    var a = new[] { 10, 11, 12, 13, 14, 15, 16, 17, 34, 18, 19, 20, 21, 22, 35, 23, 24, 25, 26, 27, 28, 29, 32, 30, 31, 33 };
                    var b = new int[11];
                    b[1] = a[(arg_Identify[0]) - 65] % 10;
                    var c = b[0] = a[(arg_Identify[0]) - 65] / 10;
                    for (var i = 1; i <= 9; i++)
                    {
                        b[i + 1] = arg_Identify[i] - 48;
                        c += b[i] * (10 - i);
                    }
                    if (((c % 10) + b[10]) % 10 != 0 )
                        ErrorMsg = ErrorMsg + @"\n" + "【身份証字號】輸入錯誤";
                }

            }
            if (Home_Tele_Number.Text == "")
            {
            }
            if (Cell_Number.Text == "")
                ErrorMsg = ErrorMsg + @"\n" + "【行動電話】輸入錯誤";
            if (EM_Height.Text == "")
                // ErrorMsg = ErrorMsg & "\n" & "【Height】輸入錯誤"
                EM_Height.Text = "0";
            if (EM_Weight.Text == "")
                // ErrorMsg = ErrorMsg & "\n" & "【Weight】輸入錯誤"
                EM_Weight.Text = "0";

            if (Addr_Text.Text == "")
            {
            }
            if (Book_Addr.SelectedIndex == 1 & Addr_Text1.Text == "")
                // ErrorMsg = ErrorMsg & "\n" & "【戶籍地址】輸入錯誤"
                Book_Addr.SelectedIndex = 0;
            if (EM_Contact_Name.Text == "")
            {
            }
            if (EM_Contact_Re.Text == "")
            {
            }
            if (EM_Contact_Number.Text == "")
            {
            }
            if (School_Name.Text == "")
            {
            }
            if (Class_Name.Text == "")
            {
            }
            if (School_Period.Text == "")
            {
            }
            if (Gradu_Status.SelectedIndex < 0)
                // ErrorMsg = ErrorMsg & "\n" & "【畢業】輸入錯誤"
                Gradu_Status.SelectedIndex = 0;
            // If Join_Media.SelectedIndex < 0 Then
            // ErrorMsg = ErrorMsg & "\n" & "【加入途徑】輸入錯誤"
            // End If
            if (Join_Media_Type.SelectedValue == "其它" & Join_Media_Text.Text == "")
                ErrorMsg = ErrorMsg + @"\n" + "【加入途徑說】輸入錯誤";
            if (Hope_Salary.Text == "")
                // ErrorMsg = ErrorMsg & "\n" & "【Wish_Salary】輸入錯誤"
                Hope_Salary.Text = "0";
            if (Trans_Type.SelectedIndex < 0 | (Trans_Type.Items[2].Selected == true & Trans_Type_Text.Text == ""))
                // ErrorMsg = ErrorMsg & "\n" & "【Traff_Type】輸入錯誤"
                Trans_Type.SelectedIndex = 0;
            else
            {
                int ii;
                Trans_String = "";
                for (ii = 0; ii <= Trans_Type.Items.Count - 1; ii++)
                {
                    if (Trans_Type.Items[ii].Selected == true)
                        Trans_String = Trans_String + Trans_Type.Items[ii].Text + "-";
                }
                if (Trans_Type.Items[2].Selected)
                    Trans_String = Trans_String + Trans_Type_Text.Text;
            }

            if (Child_TakeCare.SelectedIndex < 0 | (Child_TakeCare.Items[2].Selected == true & TakeCare_Text.Text == ""))
                // ErrorMsg = ErrorMsg & "\n" & "【小孩照顧】輸入錯誤"
                Child_TakeCare.SelectedIndex = 0;
            else
            {
                int ii;
                Child_Care_String = "";
                for (ii = 0; ii <= Child_TakeCare.Items.Count - 1; ii++)
                {
                    if (Child_TakeCare.Items[ii].Selected)
                        Child_Care_String = Child_Care_String + Child_TakeCare.Items[ii].Text + "-";
                }
                if (Child_TakeCare.Items[2].Selected)
                    Child_Care_String = Child_Care_String + TakeCare_Text.Text;
            }

            if (Driver_License.SelectedIndex < 0)
                // ErrorMsg = ErrorMsg & "\n" & "【Drive_Licence】輸入錯誤"
                Driver_License.SelectedIndex = 0;
            else
            {
                int ii;

                for (ii = 0; ii <= Driver_License.Items.Count - 1; ii++)
                {
                    if (Driver_License.Items[ii].Selected)
                        Driver_License_String = Driver_License_String + Driver_License.Items[ii].Text + "-";
                }
            }
            if (Language_Type.SelectedIndex < 0)
                // ErrorMsg = ErrorMsg & "\n" & "【Lang_Ability】輸入錯誤"
                Language_Type.SelectedIndex = 0;
            else
            {
                int ii;

                for (ii = 0; ii <= Language_Type.Items.Count - 1; ii++)
                {
                    if (Language_Type.Items[ii].Selected)
                        Language_Type_String = Language_Type_String + Language_Type.Items[ii].Text + "-";
                }
            }
            if (Internet_status.SelectedIndex < 0)
                // ErrorMsg = ErrorMsg & "\n" & "【網路情況】輸入錯誤"
                Internet_status.SelectedIndex = 0;
            if (Family_support.SelectedIndex < 0)
                // ErrorMsg = ErrorMsg & "\n" & "【Family_Support】輸入錯誤"
                Family_support.SelectedIndex = 0;
            if (loan_Status.SelectedIndex < 0)
                // ErrorMsg = ErrorMsg & "\n" & "【貸款情況】輸入錯誤"
                loan_Status.SelectedIndex = 0;
            if (Car_Surgery.SelectedIndex < 0 | (Car_Surgery.SelectedIndex == 1 & Car_Surgery_Reason.Text == ""))
                // ErrorMsg = ErrorMsg & "\n" & "【車禍/手術】輸入錯誤"
                Car_Surgery.SelectedIndex = 0;
            if (Disable_Status.SelectedIndex < 0 | (Disable_Status.SelectedIndex == 1 & (Disable_Type.Text == "" | Disable_Level.Text == "")))
                // ErrorMsg = ErrorMsg & "\n" & "【障礙手冊】輸入錯誤"
                Disable_Status.SelectedIndex = 0;
            if (Health_Status.SelectedIndex < 0 | (Health_Status.Items[14].Selected == true & Health_Text.Text == ""))
                // ErrorMsg = ErrorMsg & "\n" & "【Health_Status】輸入錯誤"
                Health_Status.SelectedIndex = 0;
            else
            {
                int ii;
                for (ii = 0; ii <= Health_Status.Items.Count - 1; ii++)
                {
                    if (Health_Status.Items[ii].Selected)
                        Health_Status_String = Health_Status_String + Health_Status.Items[ii].Text + "-";
                }
                if (Health_Status.Items[14].Selected)
                    Health_Status_String = Health_Status_String + Health_Text.Text;
            }
            if (Marrige_Status.SelectedIndex < 0 | (Marrige_Status.SelectedIndex == 2 & Marrige_Text.Text == ""))
                // ErrorMsg = ErrorMsg & "\n" & "【Marrige狀況】輸入錯誤"
                Marrige_Status.SelectedIndex = 0;
            if (Free_Insurance_Status.SelectedIndex < 0 | (Free_Insurance_Status.SelectedIndex == 2 & Free_Insurance_Text.Text == ""))
                // ErrorMsg = ErrorMsg & "\n" & "【健保抵免】輸入錯誤"
                Free_Insurance_Status.SelectedIndex = 0;
            if (Insurance_Status.SelectedIndex < 0 | (Insurance_Status.SelectedIndex == 2 & Insurance_Text.Text == ""))
                // ErrorMsg = ErrorMsg & "\n" & "【Insurance_Status】輸入錯誤"
                Insurance_Status.SelectedIndex = 0;

            if (Duty_Date.Text == "")
                // ErrorMsg = ErrorMsg & "\n" & "【Duty_Date】輸入錯誤"
                Duty_Date_String = "null";
            else
                Duty_Date_String = "'" + Duty_Date.Text + "'";

            if (ErrorMsg.Length != 0)
            {
                ErrorMsg = "以下欄位輸入錯誤" + ErrorMsg;
                Response.Write("<script language=JavaScript>alert('" + ErrorMsg + "');</script>");
                return;
            }

            // 90天內曾有應徵者
            INString = "Select * from Recruit_Data where RID = '" + EM_ID.Text + "' and DateDiff(day,Interview_Date,getdate()) <= 90 ";

            string tmpAddr = (Book_Addr.SelectedIndex == 0 ?
                           (",H_City_Name = '" + City_Name.SelectedValue + "',H_Country_Name = '" + Area_Name.SelectedValue + "',H_Road_Name = '" +
                           Addr_Text.Text + "'") :
                           (",H_City_Name = '" + City_Name1.SelectedValue + "',H_Country_Name = '" + Area_Name1.SelectedValue + "',H_Road_Name = '" +
                           Addr_Text1.Text + "'"));
            string ItmpAddr = (Book_Addr.SelectedIndex == 0 ?
                           (",'" + City_Name.SelectedValue + "','" + Area_Name.SelectedValue + "','" + Addr_Text.Text + "'") :
                           (",'" + City_Name1.SelectedValue + "','" + Area_Name1.SelectedValue + "','" + Addr_Text1.Text + "'"));
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            if (SQLReader.Read() == true)
                INString = "Update Recruit_Data  SET Name = '" + EM_Name.Text + "',Interview_Staff_Name = '" + EM_NO.Text + "',Sex = " + EM_SEX.SelectedIndex +
                           ",Birth = '" + (Convert.ToInt16(Birth_Year.SelectedValue) + 1911).ToString() + "/" + Birth_Month.Text + "/" + Birth_Day.Text + "'" + ",Birth_Host = '" +
                           Birth_City.Text + "', Height = " + EM_Height.Text + ", Weight = " + EM_Weight.Text + ",Marrige = '" + Marrige_Status.SelectedValue +
                           "-" + Marrige_Text.Text + "'" + ",Childs = " + (Child_Number.Text == "" ? "0" : Child_Number.Text) + ",Take_Care_Style = '" +
                           Child_Care_String + "'" + ",L_City_Name = '" + City_Name.SelectedValue + "',L_Country_Name = '" + Area_Name.SelectedValue +
                           "',L_Road_Name = '" + Addr_Text.Text + "'" + tmpAddr + ",Cell_Phone = '" + Cell_Number.Text + "',House_Phone = '" +
                           Home_Tele_Number.Text + "',Branch_Compnay = '" + Branch.SelectedItem.ToString().Trim() + "'" + ",Branch_Name = '" +
                           Branch.SelectedItem.ToString().Trim() + "',R_Job = '" + Recruit_Job_Type.SelectedValue + "-" + Recruit_Job_Other.Text + "'" +
                           ",Emg_Name = '" + EM_Contact_Name.Text.Trim() + "',Emg_Relation = '" + EM_Contact_Re.Text.Trim() + "',Emg_Cell_Phone = '" +
                           EM_Contact_Number.Text.Trim() + "'" + ",Recurit_Source = '" + Join_Media_Type.SelectedValue.Trim() + "-" +
                           Join_Media_Select.SelectedValue + "-" + Join_Media_Text.Text + "', Traff_Type = '" + Trans_String + "', Drive_Licence = '" +
                           Driver_License_String + "'" + ",Lang_Ability = '" + Language_Type_String + "',Car_Surg_Status = '" + Car_Surgery.SelectedValue + "-" +
                           Car_Surgery_Reason.Text + "'" + ",Disable_Status = '" + Disable_Status.SelectedValue + "-" + Disable_Type.Text + "-" +
                           Disable_Level.Text + "'" + ",Health_Status = '" + Health_Status_String + "',Internet_Flag = " + Internet_status.SelectedIndex.ToString() +
                           ",Family_Support = " + Family_support.SelectedIndex.ToString() + ",Loan_Status = " + loan_Status.SelectedIndex.ToString() +
                           ",Wish_Salary = " + Hope_Salary.Text + ",Free_Insurance = '" + Free_Insurance_Status.SelectedValue + "-" + Free_Insurance_Text.Text +
                           "'" + ",Insur_Nums = " + (Family_IN_Number.Text == "" ? "0" : Family_IN_Number.Text) + ",Raise_Nums = " +
                           (Raise_Number.Text == "" ? "0" : Raise_Number.Text) + ",Insurance_Status = '" + Insurance_Status.SelectedValue + "-" + Insurance_Text.Text +
                           "'" + ",Train_Date = '" + Train_Year.Text + "/" + Train_Month.Text + "/" + Train_Day.Text + "', Out_Country_Flag = " +
                           (Agree_O_County.Checked == true ? "1" : "0") + ",Work_City_Name = '" + Work_City_Name.SelectedValue + "',Private_Data = " +
                           (Agree_Private_Data.Checked == true ? "1" : "0") + ",Interview_Result = " + Interview_Result.SelectedIndex.ToString() + ",Duty_Date = " +
                           Duty_Date_String + ",No_Regis_Reason = '" + No_Register_Text.Text + "',Interview_Comm = '" + Interview_Text.Text +
                           "',Interview_Salary = " + (Interview_Salary.Text == "" ? "0" : Interview_Salary.Text) + " where RID = '" + EM_ID.Text + "'";
            else
                INString = "Insert into  Recruit_Data (Interview_Staff_Name,Name,Sex,Birth,Birth_Host,Blood_Type,Height,Weight,Marrige,Childs,Take_Care_Style," +
                           "RID,L_City_Name,L_Country_Name,L_Road_Name,H_City_Name,H_Country_Name,H_Road_Name,Cell_Phone,Tele_Code,House_Phone,Interview_Date," +
                           "R_Picture,Branch_Compnay,Branch_Name,R_Job,Emg_Name,Emg_Relation,Emg_Cell_Phone,Recurit_Source,Traff_Type,Drive_Licence,Lang_Ability," +
                           "Car_Surg_Status,Disable_Status,Health_Status,Internet_Flag,Family_Support,Loan_Status,Wish_Salary,Free_Insurance,Insur_Nums,Raise_Nums," +
                           "Insurance_Status,Train_Date,Out_Country_Flag,Work_City_Name,Private_Data,Interview_Result,Duty_Date,No_Regis_Reason,Interview_Comm," +
                           "Interview_Salary) values ('" + EM_NO.Text + "','" + EM_Name.Text + "'," + EM_SEX.SelectedIndex + ",'" +
                           (Convert.ToInt16(Birth_Year.SelectedValue) + 1911).ToString() + "/" + Birth_Month.Text + "/" + Birth_Day.Text + "','" + Birth_City.Text + "',''" + "," +
                           EM_Height.Text + "," + EM_Weight.Text + ",'" + Marrige_Status.SelectedValue + "-" + Marrige_Text.Text + "'" + "," +
                          (Child_Number.Text == "" ? "0" : Child_Number.Text) + ",'" + Child_Care_String + "','" + EM_ID.Text + "'" + ",'" + City_Name.SelectedValue +
                           "','" + Area_Name.SelectedValue + "','" + Addr_Text.Text + "'" + ItmpAddr + ",'" + Cell_Number.Text + "','','" + Home_Tele_Number.Text +
                           "','" + Recruit_Date.Text.Trim() + "',null,'" + Branch.SelectedItem.ToString().Trim() + "'" + ",'" + Branch.SelectedItem.ToString().Trim() +
                           "','" + Recruit_Job_Type.SelectedValue + "-" + Recruit_Job_Other.Text + "'" + ",'" + EM_Contact_Name.Text.Trim() + "','" +
                           EM_Contact_Re.Text.Trim() + "','" + EM_Contact_Number.Text.Trim() + "'" + ",'" + Join_Media_Type.SelectedValue.Trim() + "-" +
                           Join_Media_Select.SelectedValue.Trim() + "-" + Join_Media_Text.Text.Trim() + "','" + Trans_String + "','" + Driver_License_String + "'" +
                           ",'" + Language_Type_String + "','" + Car_Surgery.SelectedValue + "-" + Car_Surgery_Reason.Text + "'" + ",'" +
                           Disable_Status.SelectedValue + "-" + Disable_Type.Text + "-" + Disable_Level.Text + "'" + ",'" + Health_Status_String + "'," +
                           Internet_status.SelectedIndex.ToString() + ", " + Family_support.SelectedIndex.ToString() + "," + loan_Status.SelectedIndex.ToString() +
                           ", " + Hope_Salary.Text + ",'" + Free_Insurance_Status.SelectedValue + "-" + Free_Insurance_Text.Text + "'" + ", " +
                           (Family_IN_Number.Text == "" ? "0" : Family_IN_Number.Text) + "," + (Raise_Number.Text == "" ? "0" : Raise_Number.Text) + ",'" +
                           Insurance_Status.SelectedValue + "-" + Insurance_Text.Text + "'" + ",'" + Train_Year.Text + "/" + Train_Month.Text + "/" +
                           Train_Day.Text + "'," + (Agree_O_County.Checked == true ? "1" : "0") + ",'" + Work_City_Name.SelectedValue + "'," +
                           (Agree_Private_Data.Checked == true ? "1" : "0") + ", " + Interview_Result.SelectedIndex.ToString() + "," + Duty_Date_String + "" + ",'" +
                           No_Register_Text.Text + "','" + Interview_Text.Text + "'," + (Interview_Salary.Text == "" ? "0" : Interview_Salary.Text) + ")";
            INCommand.Connection.Close();
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection);
            INCommand.Connection.Open();
            INCommand.ExecuteNonQuery();
            INCommand.Connection.Close();

            INString = "Select Top 1 ID from Recruit_Data where Name = '" + EM_Name.Text + "' and RID = '" + EM_ID.Text + "' order by Interview_Date desc";

            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();

            // 畢業學校
            if (SQLReader.Read())
            {
                INString = "Delete  from Recruit_School_Data where ID = " + SQLReader[0];
                INCommand1 = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
                INCommand1.Connection.Open();
                INCommand1.ExecuteNonQuery();
                INCommand1.Connection.Close();
                INString = "Delete  from Recruit_Job_Experi where ID = " + SQLReader[0];
                INCommand1 = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
                INCommand1.Connection.Open();
                INCommand1.ExecuteNonQuery();
                INCommand1.Connection.Close();
                INString = "Delete  from Recruit_Family_Data where ID = " + SQLReader[0];
                INCommand1 = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
                INCommand1.Connection.Open();
                INCommand1.ExecuteNonQuery();
                INCommand1.Connection.Close();
            }

            INString = "Insert into  Recruit_School_Data (ID,School_Name,Class_Name,FromToDate,Graduate) values (" + SQLReader[0] + ",'" + School_Name.Text + 
                       "','" + Class_Name.Text + "','" + School_Period.Text + "'," + Convert.ToInt32(Gradu_Status.SelectedIndex) + ")";
            INCommand1 = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
            INCommand1.Connection.Open();
            INCommand1.ExecuteNonQuery();
            INCommand1.Connection.Close();

            // Job Experience
            int iii = 1;
            TextBox PreJobName, PreJobTitle, PJSA;
            DropDownList PJSY, PJSM, PJEY, PJEM;
            string tmpSYM, tmpEYM;

            for (iii = 1; iii <= 3; iii++)
            {
                PreJobName = (TextBox)FindControl("Pre_Job_Name" + iii.ToString("0"));
                PreJobTitle = (TextBox)FindControl("Pre_Job_Title" + iii.ToString("0"));
                PJSA = (TextBox)FindControl("PJ_SA" + iii.ToString("0"));
                PJSY = (DropDownList)FindControl("PJ_SY" + iii.ToString("0"));
                PJSM = (DropDownList)FindControl("PJ_SM" + iii.ToString("0"));
                PJEY = (DropDownList)FindControl("PJ_EY" + iii.ToString("0"));
                PJEM = (DropDownList)FindControl("PJ_EM" + iii.ToString("0"));
                tmpSYM = string.Format("{0:0000}", PJSY.SelectedValue) + string.Format("{0:00}", PJSM.SelectedValue);
                tmpEYM = string.Format("{0:0000}", PJEY.SelectedValue) + string.Format("{0:00}", PJEM.SelectedValue);

                if (PreJobName.Text != "")
                {
                    INString = "Insert into  Recruit_Job_Experi (ID,Company_Name,Title_Name,StartYM,EndYm,Salary) values (" + SQLReader[0] + ",'" +
                               PreJobName.Text + "','" + PreJobTitle.Text + "','" + tmpSYM + "','" + tmpEYM + "','" + PJSA.Text + "')";
                    INCommand1 = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
                    INCommand1.Connection.Open();
                    INCommand1.ExecuteNonQuery();
                    INCommand1.Connection.Close();
                }
            }

            // Family 
            TextBox FGC, FGNAME, FGCOM, FGAGE;
            for (iii = 1; iii <= 4; iii++)
            {
                FGC = (TextBox)FindControl("F_G_C" + iii.ToString("0"));
                FGNAME = (TextBox)FindControl("F_G_Name" + iii.ToString("0"));
                FGCOM = (TextBox)FindControl("F_G_Company" + iii.ToString("0"));
                FGAGE = (TextBox)FindControl("F_G_Age" + iii.ToString("0"));

                if (FGC.Text != "")
                {
                    INString = "Insert into  Recruit_Family_Data (ID,Title,Name,Company,Birth_Year) values (" + SQLReader[0] + ",'" + FGC.Text + "','" +
                               FGNAME.Text + "','" + FGCOM.Text + "'," +  string.Format("{0:##0}", (DateTime.Now.Year - Convert.ToInt32(FGAGE.Text))) + ")";
                    INCommand1 = new System.Data.SqlClient.SqlCommand(INString, INConnection2);
                    INCommand1.Connection.Open();
                    INCommand1.ExecuteNonQuery();
                    INCommand1.Connection.Close();
                }
            }

            INCommand.Connection.Close();

            TextBox1.Text = INString;
            //Response.Redirect("RecruitList.aspx");
            Response.Redirect("RecruitList.aspx");
            //Page_Load(sender, e);
        }
        protected void Join_Media_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            MyDataSet = new System.Data.DataSet();

            MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                        "SELECT * FROM Recruit_Source_Data  where Recruit_Source='" + Join_Media_Type.SelectedValue + "'", INConnection1);
            MyCommand.Fill(MyDataSet, "Recruit_Source_Data");

            Join_Media_Select.DataSource = MyDataSet.Tables["Recruit_Source_Data"].DefaultView;
            Join_Media_Select.DataTextField = "Source_Detail";
            Join_Media_Select.DataValueField = "Source_Detail";
            Join_Media_Select.DataBind();

            Page_Load(sender, e);
        }
    }

}