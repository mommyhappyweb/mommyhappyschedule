﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JobScheduleModify.aspx.cs" Inherits="MommyHappySchedule.iframe.JobScheduleModify" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>確認班表</title>
    <style type="text/css">
        .auto-style1 {
            height: 112px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <center>
                <table>
                    <tr>
                        <td align="center">
                            <table>

                                <tr>

                                    <td colspan="3" class="auto-style1">
                                        <table border="1" cellpadding="1" cellspacing="0">
                                            <tr style="font-size: 10pt; font-family: Arial;">
                                                <td>駐點：</td>
                                                <td>
                                                    <asp:DropDownList ID="Branch" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>員工名稱</td>
                                                <td>
                                                    <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                                                </td>
                                                <td>客戶名稱</td>
                                                <td>
                                                    <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Button ID="Check_Report" runat="server" Text="產生報表" OnClick="Check_Report_Click" />
                                                    <asp:Button ID="Button6" runat="server" Text="取消" OnClick="Button6_Click" />
                                                </td>
                                                <td>
                                                    <%--<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="Index.aspx">返回主選單</asp:HyperLink>--%>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                                                    <asp:Label ID="Label1" runat="server" Text="0" Visible="false"></asp:Label>
                                                    <asp:Label ID="Label4" runat="server" Text="0.00"></asp:Label>
                                                    <asp:Label ID="Label5" runat="server" Text="0.00"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr style="font-size: 10pt; font-family: Arial;">
                                                <td colspan="4">顯示：
                                                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged" RepeatLayout="Flow">
                                                        <asp:ListItem>全部</asp:ListItem>
                                                        <asp:ListItem>已服務</asp:ListItem>
                                                        <asp:ListItem>服務中</asp:ListItem>
                                                        <asp:ListItem>未打卡</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td colspan="3">

                                                    <asp:Button ID="Left_Screen" runat="server" Text="前一天" OnClick="Change_Screen" />
                                                    <asp:TextBox ID="Show_Date" runat="server" Width="70px"></asp:TextBox>
                                                    <asp:TextBox ID="End_Date" runat="server" Width="70px"></asp:TextBox>
                                                    <asp:Button ID="Right_Screen" runat="server" Text="後一天" OnClick="Change_Screen" />
                                                </td>
                                                <td colspan="3">
                                                    <asp:RadioButtonList ID="RadioButtonList2" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" AutoPostBack="true"  OnSelectedIndexChanged="RadioButtonList2_SelectedIndexChanged" >
                                                        <asp:ListItem>全部確定</asp:ListItem>
                                                        <asp:ListItem>全部取消</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:Button ID="SaveFile" runat="server" Text="存檔" OnClick="SaveFile_Click" />
                                                </td>
                                        </table>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" BackColor="White" OnItemDataBound="Show_Sc_Type"
                                            BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" Font-Names="Arial" Font-Size="Smaller" BorderStyle="None">
                                            <FooterStyle BackColor="White" ForeColor="#000066" />
                                            <SelectedItemStyle BackColor="#669999" ForeColor="White" Font-Bold="True" />
                                            <Columns>

                                                <asp:BoundColumn DataField="ID" HeaderText="Type"  >
                                                    <ItemStyle Width="10px" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Branch_Name" HeaderText="駐點">
                                                    <ItemStyle Width="60px" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Staff_No" HeaderText="管家編號">
                                                    <ItemStyle Width="60px" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Staff_Name" HeaderText="管家姓名">
                                                    <ItemStyle Width="60px" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Cust_No" HeaderText="客戶編號">
                                                    <ItemStyle Width="70px" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Cust_Name" HeaderText="客戶姓名">
                                                    <ItemStyle Width="70px" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Cust_Addr" HeaderText="客戶地址">
                                                    <ItemStyle Width="250px" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Job_Date" HeaderText="日期" DataFormatString="{0:yyyy/MM/dd}">
                                                    <ItemStyle Width="40px" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Job_Result" HeaderText="狀態">
                                                    <ItemStyle Width="10px" />
                                                </asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="出勤狀態">
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label3" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="Job_Flag" Visible="false">
                                                    <ItemStyle Width="120px" />
                                                </asp:BoundColumn>
                                                <asp:TemplateColumn HeaderText="取消">
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CheckBox1" runat="server"  Text="取消班表"/>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle HorizontalAlign="Left" Mode="NumericPages" BackColor="White" ForeColor="#000066" />
                                            <ItemStyle ForeColor="#000066" />
                                        </asp:DataGrid>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:TextBox ID="TextBox5" runat="server" Width="100%"></asp:TextBox></td>
                    </tr>
                </table>
            </center>
        </div>
    </form>
</body>
</html>


