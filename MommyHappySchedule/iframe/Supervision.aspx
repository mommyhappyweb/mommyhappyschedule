﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Supervision.aspx.cs" Inherits="MommyHappySchedule.iframe.Supervision" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="zh-tw">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>督導選擇時間</title>
    <link href="/Public/Style/bootstrap.min.css" rel="stylesheet" />
    <link href="/Public/Style/Layout.css" rel="stylesheet" />
</head>
<body class="body">
    <div class="loading" id="Loading">
        <div class="loading-block">
            <div class="loading-dot"></div>
            <div class="loading-dot"></div>
            <div class="loading-dot"></div>
            <div class="loading-dot"></div>
            <div class="loading-dot"></div>
        </div>
    </div>
    <div class="container">
        <div class="main">
            <h1 class="main-title">督導管理</h1>
            <hr />
            <div class="btn-block text-right">
                <div class="row" id="Search">
                    <div class="col-sm-2">
                        <select class="form-control" id="KeySelect">
                            <option value="Schedule_ID">合約編號</option>
                        </select>
                    </div>
                    <div class="col-sm-8">
                        <input class="form-control" type="text" id="Keyword" />
                    </div>
                    <div class="col-sm-2">
                        <input class="btn btn-primary" type="button" id="SearchBtn" value="搜尋" />
                    </div>
                </div>
                <input type="button" class="btn btn-success" id="ListAddBtn" value="新增督導時間" />
            </div>
            <div class="list">
                <div class="list-block text-center" id="ListBlock">
                    <table class="table table-striped" id="Table">
                        <thead></thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <hr />
            <div class="btn-block text-center">
                <input type="button" class="btn btn-primary" id="ListSave" value="儲存" />
            </div>
        </div>
        <div id="Prompt"></div>
    </div>
    <script src="/Public/Scripts/jquery/jquery-3.3.1.min.js"></script>
    <script src="/Public/Scripts/App/LoadingApp.js"></script>
    <script src="/Public/Scripts/App/SupervisionApp.js"></script>
</body>
</html>
