﻿using System;
using System.Web;
using System.Web.UI;

namespace MommyHappySchedule.iframe
{
    public partial class Supervision : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string Schedule_ID = Request["Schedule_ID"];
            string Job_Date = Request["Job_Date"];
            string Staff_No = Request["Staff_No"];
            string Job_Result_ID = Request["Job_Result_ID"];
            //int Schedule_ID = 26;
            //string Job_Date = "2018-10-16";
            //string Staff_No = "A123456";
            //byte Job_Result_ID = 0;
            HttpContext HttpContext = HttpContext.Current;
            HttpContext.Session["Schedule_ID"] = Schedule_ID;
            HttpContext.Session["Job_Date"] = Job_Date;
            HttpContext.Session["Staff_No"] = Staff_No;
            HttpContext.Session["Job_Result_ID"] = Job_Result_ID;
        }
    }
}