﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Template/IframeLayout.master" AutoEventWireup="true" CodeBehind="AS.aspx.cs" Inherits="MommyHappySchedule.iframe.AS" %>


<asp:Content ID="Style" ContentPlaceHolderID="Style" runat="server">
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="Content" runat="server">
    <div class="container-fluid">
      
        <fieldset class="col-sm-10 offset-sm-1">
            <legend>資料搜尋</legend>
            <div class="form-group row">
                <div class="col-sm-2">
                    <label>駐點：</label>
                    <asp:DropDownList CssClass="form-control" ID="Branch" runat="server"></asp:DropDownList>
                </div>
                <div class="col-sm-2">
                    <label>年份：</label>
                    <asp:DropDownList CssClass="form-control" ID="Check_Year" runat="server">
                        <asp:ListItem>2016</asp:ListItem>
                        <asp:ListItem>2017</asp:ListItem>
                        <asp:ListItem Selected="True">2018</asp:ListItem>
                        <asp:ListItem>2019</asp:ListItem>
                        <asp:ListItem>2020</asp:ListItem>
                        <asp:ListItem>2021</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-sm-2">
                    <label>月份：</label>
                    <asp:DropDownList CssClass="form-control" ID="Check_Month" runat="server">
                        <asp:ListItem Selected="True">1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-sm-2">
                    <asp:Button ID="Check_Report" CssClass="btn btn-primary mt-4" runat="server" Text="產生報表" OnClick="Check_Report_Click" />
                </div>
                <div class="col-sm-2">
                    <asp:Label ID="Label2" runat="server" Text="" Visible="false"></asp:Label>
                    <asp:TextBox ID="TextBox1" runat="server" Visible="false"></asp:TextBox>
                    <asp:Label ID="Label5" runat="server" Text="0.00" Visible="false"></asp:Label>
                    <label>客戶編號/姓名：</label>
                    <asp:TextBox ID="SearchText" CssClass="form-control" runat="server"></asp:TextBox>
                </div>
                <div class="col-sm-2">
                    <asp:Button ID="Search" CssClass="btn btn-primary mt-4" runat="server" Text="搜尋" OnClick="Search_Click" />
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-4">
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" Visible="False" AutoPostBack="true" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged">
                        <asp:ListItem>換客戶(單)</asp:ListItem>
                        <asp:ListItem>換管家(單)</asp:ListItem>
                        <asp:ListItem>換管家(整)</asp:ListItem>
                        <asp:ListItem>輸入單次備註</asp:ListItem>
                        <asp:ListItem>換客戶(整)</asp:ListItem>
                        <asp:ListItem>設定顏色</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div class="col-sm-3">
                    <asp:DropDownList ID="SelectMonth" CssClass="form-control" runat="server" Visible="False" AutoPostBack="true" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:DropDownList ID="DropDownList1" CssClass="form-control" runat="server" Visible="False" AutoPostBack="true" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="DropDownList4" CssClass="form-control" runat="server" Visible="False" AutoPostBack="true" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:DropDownList ID="DropDownList2" CssClass="form-control" runat="server" Visible="False">
                    </asp:DropDownList>
                    <asp:HyperLink ID="HyperLink2" runat="server" Visible="false" Target="_blank"></asp:HyperLink>
                    <asp:DropDownList ID="JobResultSelect" CssClass="form-control" runat="server" RepeatDirection="Horizontal" Visible="false">
                        <asp:ListItem>先不排</asp:ListItem>
                    </asp:DropDownList>
                    <asp:TextBox ID="JobNote" placeholder="備註" CssClass="form-control" runat="server" Visible="false" MaxLength="50" TextMode="MultiLine"></asp:TextBox>
                    <asp:DropDownList ID="DropDownList5" CssClass="form-control" Visible="false" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownList5_SelectedIndexChanged">
                        <asp:ListItem>請選顏色</asp:ListItem>
                        <asp:ListItem Value="Orange">橙色</asp:ListItem>
                        <asp:ListItem Value="PaleGreen">淺綠</asp:ListItem>
                        <asp:ListItem Value="BlanchedAlmond">淺黃</asp:ListItem>
                        <asp:ListItem Value="LightPink">淺粉紅</asp:ListItem>
                        <asp:ListItem Value="LightSkyBlue">淺天藍</asp:ListItem>
                        <asp:ListItem Value="Red">紅色</asp:ListItem>
                        <asp:ListItem Value="Chocolate">深橙色</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="col-sm-5">
                    <asp:CheckBox ID="CheckBox1" CssClass="form-check" runat="server" Text="是否扣次" Visible="False" />
                    <asp:CheckBox ID="CheckBox2" CssClass="form-check" runat="server" Text="是否順延" Visible="False" />
                    <asp:CheckBox ID="CheckBox4" CssClass="form-check" runat="server" Visible="False" Text="上午" AutoPostBack="true" OnCheckedChanged="DropDownList1_SelectedIndexChanged" />
                    <asp:CheckBox ID="CheckBox5" CssClass="form-check" runat="server" Visible="False" Text="隔周換" />
                    <asp:CheckBox ID="CheckBox3" CssClass="form-check" runat="server" Text="若有重覆記錄則覆蓋" Visible="False" />
                    <asp:Button ID="Button5" CssClass="btn btn-success" runat="server" Text="確定修改" Visible="False" OnClick="Button5_Click" />
                    <asp:Button ID="Button6" CssClass="btn btn-dark" runat="server" Text="取消" Visible="False" OnClick="Button6_Click" />
                </div>
            </div>
        </fieldset>
        <div class="btn-block text-right">
            <asp:Button ID="Left_Screen" CssClass="btn btn-info" runat="server" Text="上一頁" OnClick="Change_Screen" />
            <asp:Button ID="Right_Screen" CssClass="btn btn-info" runat="server" Text="下一頁" OnClick="Change_Screen" />
        </div>
        <asp:DataGrid ID="ScheduleTable" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-hover ScheduleTable schedule-table" ShowHeader="True"
            OnItemCommand="Show_Job_Detail" OnItemDataBound="Show_Sc_Type" OnPageIndexChanged="DataGrid_PageIndexChanged" GridLines="None" AllowCustomPaging="False" AllowPaging="False" PagerStyle-NextPageText="下一頁" PagerStyle-PrevPageText="前一頁" PagerStyle-Mode="NumericPages">
            <ItemStyle CssClass="text-center" />
            <Columns>
                <asp:BoundColumn DataField="ID" HeaderText="職稱"></asp:BoundColumn>
                <asp:TemplateColumn HeaderText="管家">
                    <ItemTemplate>
                        <asp:Label ID="HK_Name" runat="server" Text='<%# Bind("HK_Name") %>' ToolTip='<%# Bind("HK_Data") %>' />
                    </ItemTemplate>
                </asp:TemplateColumn>
<%--                <asp:BoundColumn DataField="HK_Name" HeaderText="管家"></asp:BoundColumn>--%>
                <asp:BoundColumn DataField="Day_ID" HeaderText="班制"></asp:BoundColumn>
                <asp:TemplateColumn HeaderText="1">
                    <ItemTemplate>
                        <asp:Button ID="B1" CssClass="btn btn-light btn-sm btn-block" runat="server" OnClick="Check_Sc" Text='<%# Bind("B1") %>' ToolTip='<%# Eval("B1_1") + "\n" + Eval("B1_2") %>' />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="2">
                    <ItemTemplate>
                        <asp:Button ID="B2" CssClass="btn btn-light btn-sm btn-block" runat="server" OnClick="Check_Sc" Text='<%# Bind("B2") %>' ToolTip='<%# Eval("B2_1") + "\n" + Eval("B2_2") %>' />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="3">
                    <ItemTemplate>
                        <asp:Button ID="B3" CssClass="btn btn-light btn-sm btn-block" runat="server" OnClick="Check_Sc" Text='<%# Bind("B3") %>' ToolTip='<%# Eval("B3_1") + "\n" + Eval("B3_2") %>' />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="4">
                    <ItemTemplate>
                        <asp:Button ID="B4" CssClass="btn btn-light btn-sm btn-block" runat="server" OnClick="Check_Sc" Text='<%# Bind("B4") %>' ToolTip='<%# Eval("B4_1") + "\n" + Eval("B4_2") %>' />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="5">
                    <ItemTemplate>
                        <asp:Button ID="B5" CssClass="btn btn-light btn-sm btn-block" runat="server" OnClick="Check_Sc" Text='<%# Bind("B5") %>' ToolTip='<%# Eval("B5_1") + "\n" + Eval("B5_2") %>' />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="6">
                    <ItemTemplate>
                        <asp:Button ID="B6" CssClass="btn btn-light btn-sm btn-block" runat="server" OnClick="Check_Sc" Text='<%# Bind("B6") %>' ToolTip='<%# Eval("B6_1") + "\n" + Eval("B6_2") %>' />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="7">
                    <ItemTemplate>
                        <asp:Button ID="B7" CssClass="btn btn-light btn-sm btn-block" runat="server" OnClick="Check_Sc" Text='<%# Bind("B7") %>' ToolTip='<%# Eval("B7_1") + "\n" + Eval("B7_2") %>' />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="8">
                    <ItemTemplate>
                        <asp:Button ID="B8" CssClass="btn btn-light btn-sm btn-block" runat="server" OnClick="Check_Sc" Text='<%# Bind("B8") %>' ToolTip='<%# Eval("B8_1") + "\n" + Eval("B8_2") %>' />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="9">
                    <ItemTemplate>
                        <asp:Button ID="B9" CssClass="btn btn-light btn-sm btn-block" runat="server" OnClick="Check_Sc" Text='<%# Bind("B9") %>' ToolTip='<%# Eval("B9_1") + "\n" + Eval("B9_2") %>' />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="10">
                    <ItemTemplate>
                        <asp:Button ID="B10" CssClass="btn btn-light btn-sm btn-block" runat="server" OnClick="Check_Sc" Text='<%# Bind("B10") %>' ToolTip='<%# Eval("B10_1") + "\n" + Eval("B10_2") %>' />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="11">
                    <ItemTemplate>
                        <asp:Button ID="B11" CssClass="btn btn-light btn-sm btn-block" runat="server" OnClick="Check_Sc" Text='<%# Bind("B11") %>' ToolTip='<%# Eval("B11_1") + "\n" + Eval("B11_2") %>' />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="12">
                    <ItemTemplate>
                        <asp:Button ID="B12" CssClass="btn btn-light btn-sm btn-block" runat="server" OnClick="Check_Sc" Text='<%# Bind("B12") %>' ToolTip='<%# Eval("B12_1") + "\n" + Eval("B12_2") %>' />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="13">
                    <ItemTemplate>
                        <asp:Button ID="B13" CssClass="btn btn-light btn-sm btn-block" runat="server" OnClick="Check_Sc" Text='<%# Bind("B13") %>' ToolTip='<%# Eval("B13_1") + "\n" + Eval("B13_2") %>' />
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="14">
                    <ItemTemplate>
                        <asp:Button ID="B14" CssClass="btn btn-light btn-sm btn-block" runat="server" OnClick="Check_Sc" Text='<%# Bind("B14") %>' ToolTip='<%# Eval("B14_1") + "\n" + Eval("B14_2") %>' />
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
    </div>
</asp:Content>
<asp:Content ID="Script" ContentPlaceHolderID="Script" runat="server">
    <script src="/Public/Scripts/App/AsApp.js"></script>
    <script>
        document.domain = 'mommyhappy.com';
    </script>
</asp:Content>
