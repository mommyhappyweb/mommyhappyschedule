﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Drawing;
using System.Configuration;

namespace MommyHappySchedule.iframe
{
    public partial class RecruitList : System.Web.UI.Page
    {
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        System.Data.SqlClient.SqlConnection INConnection1 = new System.Data.SqlClient.SqlConnection(ConnString);
        System.Data.SqlClient.SqlConnection INConnection2 = new System.Data.SqlClient.SqlConnection(ConnString);
        private System.Data.SqlClient.SqlCommand INCommand;
        private System.Data.SqlClient.SqlDataReader SQLReader;
        private System.Data.DataSet MyDataSet;
        private System.Data.SqlClient.SqlDataAdapter MyCommand;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // If Session("account") = "" Or Session("Username") <> "mommyhappy" Then
            // Session("msg") = "您沒有使用此程式的權限!!"
            // Response.Redirect("Index.aspx")
            // End If

            if (!Page.IsPostBack)
            {
                MyDataSet = new System.Data.DataSet();
                MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Branch_Data", INConnection1);
                MyCommand.Fill(MyDataSet, "Branch");

                Branch.DataSource = MyDataSet.Tables["Branch"].DefaultView;
                Branch.DataTextField = "Branch_Name";
                Branch.DataValueField = "ID";
                Branch.DataBind();

                Check_Year.SelectedValue = DateTime.Now.Year.ToString();
                Check_Month.SelectedValue = DateTime.Now.Month.ToString();
            }
        }


        protected void Check_Report_Click(object sender, EventArgs e)
        {
            string Select_Banch;
            string Se_String = "";

            // Label1.Text = Branch.SelectedItem.Text

            if (TextBox3.Text != "")
            {
                MyDataSet = new System.Data.DataSet();
                MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                            "SELECT * FROM Recruit_Process_View where Name like '%" + TextBox3.Text + "%' Order by Branch_Name,Interview_Date", INConnection1);
                MyCommand.Fill(MyDataSet, "RNumber");
            }
            else
            {
                if (Branch.SelectedIndex == 0)
                    Select_Banch = "Branch_Name is not null";
                else if (Branch.Items[Branch.SelectedIndex].Text == "南高雄")
                    Select_Banch = "Branch_Name = '" + Branch.Items[Branch.SelectedIndex].Text + "' or Branch_Name = '屏東'";
                else
                    Select_Banch = "Branch_Name = '" + Branch.Items[Branch.SelectedIndex].Text + "'";// 營業所名稱

                if (RadioButtonList1.SelectedValue != "全部")
                    Se_String = " and Interview_Result= " + RadioButtonList1.SelectedIndex.ToString();
                if (RadioButtonList2.SelectedValue != "全部")
                    Se_String = Se_String + (RadioButtonList2.SelectedIndex == 0 ? " and Hout_Info_Date is not null " : " and Hout_Info_Date is null ");
                if (RadioButtonList3.SelectedValue != "全部")
                    Se_String = Se_String + (RadioButtonList3.SelectedIndex == 0 ? " and Train_Date is not null " : " and Train_Date is null ");
                if (RadioButtonList5.SelectedValue != "全部")
                    Se_String = Se_String + (RadioButtonList5.SelectedIndex == 0 ? " and Sign_Contr_Date is not null " : " and Sign_Contr_Date is null ");
                if (RadioButtonList4.SelectedValue != "全部")
                {
                    if (RadioButtonList4.SelectedIndex == 0)
                        Se_String = Se_String + " and not ( (Experi_Date_1 is null or Experi_Date_1 = '') and  (Experi_Date_2 is null or Experi_Date_2 = '') and" +
                                    " (Experi_Date_3 is null or Experi_Date_3 = '') and (Experi_Date_4 is null or Experi_Date_4 = '') )  ";
                    else
                        Se_String = Se_String + " and ((Experi_Date_1 is null or Experi_Date_1 = '') and  (Experi_Date_2 is null or Experi_Date_2 = '') and " +
                                    "(Experi_Date_3 is null or Experi_Date_3 = '') and (Experi_Date_4 is null or Experi_Date_4 = '') ) ";
                }

                string tmpCheckDay = "1=1";
                if (Check_Day.Text.Trim() != "")
                {
                    switch (Check_Day.Text.Trim().Split(',').Length)
                    {
                        case 0:
                            {
                                tmpCheckDay = "1=1";
                                break;
                            }

                        case 1:
                            {
                                tmpCheckDay = "Day(Interview_Date) = " + Check_Day.Text.Trim().Split(',')[0];
                                break;
                            }

                        case 2:
                            {
                                tmpCheckDay = "Day(Interview_Date) >= " + Check_Day.Text.Trim().Split(',')[0] + " and Day(Interview_Date) <= " +
                                              Check_Day.Text.Trim().Split(',')[1];
                                break;
                            }
                    }
                }

                MyDataSet = new System.Data.DataSet();
                MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                            "SELECT * FROM Recruit_Process_View where year(Interview_Date) =" + Check_Year.SelectedValue + " and month(Interview_Date) = " +
                            Check_Month.SelectedValue + " and " + tmpCheckDay + " and " + Select_Banch + Se_String + " Order by Branch_Name,Interview_Date",
                            INConnection1);
                MyCommand.Fill(MyDataSet, "RNumber");
            }
            DataGrid1.DataSource = MyDataSet.Tables["RNumber"].DefaultView;
            DataGrid1.DataBind();

            Page_Load(sender, e);
        }

        public void Show_Sc_Type(object sender, DataGridItemEventArgs e)
        {
            ListItemType objItemType = (ListItemType)e.Item.ItemType;
            Label TempLabel;

            if (objItemType == ListItemType.Item | objItemType == ListItemType.AlternatingItem)
            {
                TempLabel = (Label)e.Item.FindControl("Hire_FLag");
                switch (e.Item.Cells[7].Text)
                {
                    case "0":
                        {
                            TempLabel.Text = "錄取";
                            break;
                        }

                    case "1":
                        {
                            TempLabel.Text = "安排複試";
                            break;
                        }

                    case "2":
                        {
                            TempLabel.Text = "不錄取";
                            break;
                        }

                    case "3":
                        {
                            TempLabel.Text = "保留";
                            break;
                        }
                }
                TempLabel = (Label)e.Item.FindControl("Trace_FLag");
                switch (e.Item.Cells[24].Text)
                {
                    case "1":
                        {
                            TempLabel.Text = "不交結案";
                            break;
                        }

                    case "2":
                        {
                            TempLabel.Text = "己交結案";
                            break;
                        }

                    default:
                        {
                            TempLabel.Text = "持續追踨";
                            break;
                        }
                }
                if (e.Item.Cells[21].Text != "&nbsp;")
                    e.Item.BackColor = Color.LightCoral;
                if (e.Item.Cells[22].Text != "&nbsp;")
                    e.Item.BackColor = Color.LightGray;
            }
        }
        public void Show_Job_Detail(object sender, DataGridCommandEventArgs e)
        {
            LinkButton tmpLB;
            ID_Label.Text = "";
            Label1.Text = "";
            Label2.Text = "";
            TextBox1.Text = "";
            TextBox2.Text = "";
            Pa_Date.Text = "";
            No_Pa_Re.Text = "";
            Tr_Date.Text = "";
            No_Tr_Re.Text = "";
            Ex_Date_1.Text = "";
            Ex_Name_1.Text = "";
            Ex_Date_2.Text = "";
            Ex_Name_2.Text = "";
            Ex_Date_3.Text = "";
            Ex_Name_3.Text = "";
            Ex_Date_4.Text = "";
            Ex_Name_4.Text = "";
            Fi_Date.Text = "";
            TextBox12.Text = "";

            tmpLB = (LinkButton)e.CommandSource;
            // Label3.Text = tmpLB.CommandName

            if (tmpLB.CommandName == "部門")
                Response.Redirect("Recruit.aspx?ID=" + e.Item.Cells[0].Text);
            else
            {
                tmpLB = (LinkButton)e.Item.Cells[1].Controls[0];

                MyDataSet = new System.Data.DataSet();
                MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                            "Select Staff_Name from Staff_Job_Data where Branch_Name ='" + tmpLB.Text + "' and Job_Status='在職' and Depart_Date is null and" +
                            " (Job_Title='金牌' or Job_Title='組長')", INConnection1);
                MyCommand.Fill(MyDataSet, "Staff_Name");
                System.Data.DataRow TR;
                TR = MyDataSet.Tables["Staff_Name"].NewRow();
                TR[0] = "請選擇";
                MyDataSet.Tables["Staff_Name"].Rows.InsertAt(TR, 0);
                DropDownList1.DataSource = MyDataSet.Tables["Staff_Name"].DefaultView;
                DropDownList1.DataTextField = "Staff_Name";
                DropDownList1.DataValueField = "Staff_Name";
                DropDownList1.DataBind();
                if (e.Item.Cells[24].Text != "&nbsp;")
                    RadioButtonList6.SelectedIndex = Convert.ToInt16(e.Item.Cells[24].Text);
                else
                    RadioButtonList6.SelectedIndex = 0;

                MultiView1.ActiveViewIndex = 1;

                ID_Label.Text = e.Item.Cells[0].Text;
                tmpLB = (LinkButton)e.Item.Cells[2].Controls[0];
                Label1.Text = tmpLB.Text;
                Label2.Text = e.Item.Cells[5].Text;
                TextBox1.Text = e.Item.Cells[4].Text;
                Interview_Result.SelectedIndex = Convert.ToInt16(e.Item.Cells[7].Text);
                if (e.Item.Cells[8].Text != "&nbsp;")
                    Pa_Date.Text = e.Item.Cells[8].Text;
                if (e.Item.Cells[21].Text != "&nbsp;")
                    No_Pa_Re.Text = e.Item.Cells[21].Text;
                if (e.Item.Cells[9].Text != "&nbsp;")
                    Tr_Date.Text = e.Item.Cells[9].Text;
                if (e.Item.Cells[22].Text != "&nbsp;")
                    No_Tr_Re.Text = e.Item.Cells[22].Text;
                if (e.Item.Cells[11].Text != "&nbsp;")
                    Ex_Date_1.Text = e.Item.Cells[11].Text;
                if (e.Item.Cells[12].Text != "&nbsp;")
                    Ex_Name_1.Text = e.Item.Cells[12].Text;
                if (e.Item.Cells[13].Text != "&nbsp;")
                    Ex_Date_2.Text = e.Item.Cells[13].Text;
                if (e.Item.Cells[14].Text != "&nbsp;")
                    Ex_Name_2.Text = e.Item.Cells[14].Text;
                if (e.Item.Cells[15].Text != "&nbsp;")
                    Ex_Date_3.Text = e.Item.Cells[15].Text;
                if (e.Item.Cells[16].Text != "&nbsp;")
                    Ex_Name_3.Text = e.Item.Cells[16].Text;
                if (e.Item.Cells[17].Text != "&nbsp;")
                    Ex_Date_4.Text = e.Item.Cells[17].Text;
                if (e.Item.Cells[18].Text != "&nbsp;")
                    Ex_Name_4.Text = e.Item.Cells[18].Text;
                if (e.Item.Cells[19].Text != "&nbsp;")
                    Fi_Date.Text = e.Item.Cells[19].Text;
                if (e.Item.Cells[20].Text != "&nbsp;")
                    TextBox12.Text = e.Item.Cells[20].Text;
                if (e.Item.Cells[10].Text != "&nbsp;")
                    TextBox2.Text = e.Item.Cells[10].Text;
            }
        }

        protected void Show_Calendar(object sender, System.EventArgs e)
        {
            Session["TB"] = ((Control)sender).ID.ToString();
            Calendar1.SelectedDates.Clear();
            Panel1.Visible = true;

            Page_Load(sender, e);
        }
        protected void Show_DropDownlist(object sender, System.EventArgs e)
        {
            Session["DB"] = ((Control)sender).ID.ToString();
            DropDownList1.Visible = true;
            DropDownList1.SelectedIndex = 0;

            Page_Load(sender, e);
        }
        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            Panel1.Visible = false;
            switch (Session["TB"])
            {
                case "Pa_Date_B":
                    {
                        Pa_Date.Text = Calendar1.SelectedDate.ToShortDateString();
                        break;
                    }

                case "Tr_Date_B":
                    {
                        Tr_Date.Text = Calendar1.SelectedDate.ToShortDateString();
                        break;
                    }

                case "Ex_Date_1_B":
                    {
                        Ex_Date_1.Text = Calendar1.SelectedDate.ToShortDateString();
                        break;
                    }

                case "Ex_Date_2_B":
                    {
                        Ex_Date_2.Text = Calendar1.SelectedDate.ToShortDateString();
                        break;
                    }

                case "Ex_Date_3_B":
                    {
                        Ex_Date_3.Text = Calendar1.SelectedDate.ToShortDateString();
                        break;
                    }

                case "Ex_Date_4_B":
                    {
                        Ex_Date_4.Text = Calendar1.SelectedDate.ToShortDateString();
                        break;
                    }

                case "Fi_Date_B":
                    {
                        Fi_Date.Text = Calendar1.SelectedDate.ToShortDateString();
                        break;
                    }
            }
        }
        protected void Rest_Click(object sender, EventArgs e)
        {
            ID_Label.Text = "";
            Label1.Text = "";
            Label2.Text = "";
            TextBox1.Text = "";
            TextBox2.Text = "";
            Pa_Date.Text = "";
            No_Pa_Re.Text = "";
            Tr_Date.Text = "";
            No_Tr_Re.Text = "";
            Ex_Date_1.Text = "";
            Ex_Name_1.Text = "";
            Ex_Date_2.Text = "";
            Ex_Name_2.Text = "";
            Ex_Date_3.Text = "";
            Ex_Name_3.Text = "";
            Ex_Date_4.Text = "";
            Ex_Name_4.Text = "";
            Fi_Date.Text = "";
            TextBox12.Text = "";
            Panel1.Visible = false;
            MultiView1.ActiveViewIndex = 0;
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList1.Visible = false;
            switch (Session["DB"])
            {
                case "Ex_Name_1_B":
                    {
                        Ex_Name_1.Text = DropDownList1.SelectedValue;
                        break;
                    }

                case "Ex_Name_2_B":
                    {
                        Ex_Name_2.Text = DropDownList1.SelectedValue;
                        break;
                    }

                case "Ex_Name_3_B":
                    {
                        Ex_Name_3.Text = DropDownList1.SelectedValue;
                        break;
                    }

                case "Ex_Name_4_B":
                    {
                        Ex_Name_4.Text = DropDownList1.SelectedValue;
                        break;
                    }

                case "Button1":
                    {
                        TextBox2.Text = DropDownList1.SelectedValue;
                        break;
                    }
            }
        }
        protected void Save_Click(object sender, EventArgs e)
        {
            string INString, INString1;
            string TPa_Date, TTr_Date, TEX_Date_1, TEX_Date_2, TEX_Date_3, TEX_Date_4, TFi_Date;

            if (Pa_Date.Text == "")
                TPa_Date = "null";
            else
                TPa_Date = "'" + Pa_Date.Text + "'";
            if (Tr_Date.Text == "")
                TTr_Date = "null";
            else
                TTr_Date = "'" + Tr_Date.Text + "'";
            if (Ex_Date_1.Text == "")
                TEX_Date_1 = "null";
            else
                TEX_Date_1 = "'" + Ex_Date_1.Text + "'";
            if (Ex_Date_2.Text == "")
                TEX_Date_2 = "null";
            else
                TEX_Date_2 = "'" + Ex_Date_2.Text + "'";
            if (Ex_Date_3.Text == "")
                TEX_Date_3 = "null";
            else
                TEX_Date_3 = "'" + Ex_Date_3.Text + "'";
            if (Ex_Date_4.Text == "")
                TEX_Date_4 = "null";
            else
                TEX_Date_4 = "'" + Ex_Date_4.Text + "'";
            if (Fi_Date.Text == "")
                TFi_Date = "null";
            else
                TFi_Date = "'" + Fi_Date.Text + "'";

            INString = "Update Recruit_Data set Cell_Phone = '" + TextBox1.Text + "',Interview_Result = " + Interview_Result.SelectedIndex.ToString() + " where ID = " + ID_Label.Text;
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            INCommand.ExecuteNonQuery();
            INCommand.Connection.Close();

            INString = "Select * from Recruit_Process_Data where ID = " + ID_Label.Text;

            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();


            if (SQLReader.Read() == true)
                INString1 = "Update Recruit_Process_Data set Hout_Info_Date = " + TPa_Date + ",Train_Date = " + TTr_Date + ",Charge_Staff_Name = '" +
                            TextBox2.Text.Trim() + "',Experi_Date_1 = " + TEX_Date_1 + ",Experi_Staff_1 = '" + Ex_Name_1.Text.Trim() + "',Experi_Date_2 = " +
                            TEX_Date_2 + ",Experi_Staff_2 = '" + Ex_Name_2.Text.Trim() + "',Experi_Date_3 = " + TEX_Date_3 + ",Experi_Staff_3 = '" +
                            Ex_Name_3.Text.Trim() + "',Experi_Date_4 = " + TEX_Date_4 + ",Experi_Staff_4 = '" + Ex_Name_4.Text.Trim() + "',Sign_Contr_Date = " +
                            TFi_Date + ",No_Info_Reason = '" + No_Pa_Re.Text.Trim() + "',No_Train_Reason = '" + No_Tr_Re.Text.Trim() + "',No_Sign_Reason = '" +
                            TextBox12.Text.Trim() + "',Recruit_Status=" + RadioButtonList6.SelectedValue + " where ID = " + ID_Label.Text;
            else
                INString1 = "insert into  Recruit_Process_Data (ID,Hout_Info_Date,No_Info_Reason,Train_Date,No_Train_Reason,Charge_Staff_Name,Experi_Date_1," +
                            "Experi_Staff_1,Experi_Date_2,Experi_Staff_2,Experi_Date_3,Experi_Staff_3,Experi_Date_4,Experi_Staff_4,Recruit_Status,Sign_Contr_Date" +
                            ",No_Sign_Reason) Values ( " + ID_Label.Text + "," + TPa_Date + ",null," + TTr_Date + ",'" + No_Tr_Re.Text.Trim() +
                            "','" + TextBox2.Text.Trim() + "'," + TEX_Date_1 + ",'" + Ex_Name_1.Text.Trim() + "'," + TEX_Date_2 + ",'" + Ex_Name_2.Text.Trim() +
                            "'," + TEX_Date_3 + ",'" + Ex_Name_3.Text.Trim() + "'," + TEX_Date_4 + ",'" + Ex_Name_4.Text.Trim() + "'," +
                            RadioButtonList6.SelectedValue + "," + TFi_Date + ",'" + TextBox12.Text.Trim() + "')";

            INCommand.Connection.Close();
            INCommand = new System.Data.SqlClient.SqlCommand(INString1, INConnection1);
            INCommand.Connection.Open();
            INCommand.ExecuteNonQuery();
            INCommand.Connection.Close();

            MultiView1.ActiveViewIndex = 0;
            Check_Report_Click(sender, e);
        }
        protected void Branch_SelectedIndexChanged(object sender, EventArgs e)
        {
            HyperLink tmpHyperLink = (HyperLink)FindControl("New_Recruit");

            if (tmpHyperLink != null)
            {
                tmpHyperLink.NavigateUrl = "Recruit.aspx?Branch=" + Branch.SelectedValue;
            }
            Page_Load(sender, e);
        }
    }

}