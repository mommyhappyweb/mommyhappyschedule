﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace MommyHappySchedule.iframe.DataRebuilt
{
    public partial class CheckContractDetail : Page
    {

        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        SqlConnection INConnection1 = new SqlConnection(ConnString);
        SqlConnection INConnection2 = new SqlConnection(ConnString);
        private SqlCommand INCommand, INCommand1;
        private SqlDataReader SQLReader, SQLReader1;
        private DataSet MyDataSet;
        private SqlDataAdapter MyCommand;

        protected void Page_Load(object sender, System.EventArgs e)
        {
            // If Session("dept") <> "Finance Dept." And Session("dept") <> "IT Dept." Then
            // Session("msg") = "您沒有使用此程式的權限!!"
            // Response.Redirect("Index.aspx")
            // End If

            if (!Page.IsPostBack)
            {
                // If Not IsNothing(Request("Cust_NO")) Then
                MyDataSet = new DataSet();
                MyCommand = new SqlDataAdapter("SELECT * FROM Branch_Data", INConnection1);
                MyCommand.Fill(MyDataSet, "Branch");

                Branch.DataSource = MyDataSet.Tables["Branch"].DefaultView;
                Branch.DataTextField = "Branch_Name";
                Branch.DataValueField = "ID";
                Branch.DataBind();
            }
        }

        protected void Branch_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Branch.SelectedIndex != 0)
                Show_Data(Branch.SelectedItem.Text);
            Page_Load(sender, e);
        }
        private void Show_Data(string BranchSelect)
        {
            string INString = "Select Cust_Name,Cust_No,Charge_Staff_Name,HK_Nums,Contract_S_Date,Contract_E_Date,Contract_Times," +
                              "Contract_Years,ID from Cust_Contract_Data_View where Branch_Name = '" + BranchSelect.Trim() +
                              "' order by Cust_No";
            DataTable dt;
            DataRow dr;

            dt = new DataTable();
            dt.Columns.Add(new DataColumn("ID", typeof(int)));
            dt.Columns.Add(new DataColumn("CustNo", typeof(string)));
            dt.Columns.Add(new DataColumn("CustName", typeof(string)));
            dt.Columns.Add(new DataColumn("DBCount", typeof(int)));
            dt.Columns.Add(new DataColumn("CTCount", typeof(string)));
            dt.Columns.Add(new DataColumn("NotMatch", typeof(string)));
            dt.Columns.Add(new DataColumn("NotContract", typeof(string)));


            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            while (SQLReader.Read() == true)
            {
                Label1.Text = DBNull.Value.Equals(SQLReader[0]) ? "" : SQLReader[0].ToString();
                Label2.Text = DBNull.Value.Equals(SQLReader[1]) ? "" : SQLReader[1].ToString();
                Label3.Text = DBNull.Value.Equals(SQLReader[2]) ? "" : SQLReader[2].ToString();
                Label4.Text = DBNull.Value.Equals(SQLReader[3]) ? "1" : SQLReader[3].ToString();
                Label5.Text = DBNull.Value.Equals(SQLReader[4]) ? "" : SQLReader[4].ToString();
                Label6.Text = DBNull.Value.Equals(SQLReader[5]) ? "" : SQLReader[5].ToString();
                Label7.Text = DBNull.Value.Equals(SQLReader[6]) ? "1" : SQLReader[6].ToString();
                Label8.Text = DBNull.Value.Equals(SQLReader[7]) ? "1" : SQLReader[7].ToString();

                if (Label4.Text == "0" | Label4.Text == "")
                    Label4.Text = "1";

                if (Label7.Text == "0" | Label7.Text == "")
                    Label7.Text = "1";

                if (Label8.Text == "0" | Label8.Text == "")
                    Label8.Text = "1";


                // 計算合約次數-扣除有假況
                int DBCount = 0;
                string SumString = "Select Count(ID) from Cust_Job_Schedule where Job_Result_ID = 0 Group by Contract_ID having Contract_ID = " + SQLReader[8];
                INCommand1 = new SqlCommand(SumString, INConnection2);
                INCommand1.Connection.Open();
                SQLReader1 = INCommand1.ExecuteReader();
                if (SQLReader1.Read() == true)
                    DBCount = Convert.ToInt16(SQLReader1[0]);
                INCommand1.Connection.Close();

                // 合約次數或合約不為24倍數不合
                if (Convert.ToDouble(Label4.Text) * Convert.ToDouble(Label7.Text) * Convert.ToDouble(Label8.Text) * 48 != DBCount | (DBCount % 24 != 0))
                {
                    dr = dt.NewRow();
                    dr[0] = SQLReader[8];
                    dr[1] = Label2.Text.Trim();
                    dr[2] = Label1.Text.Trim();
                    dr[3] = DBCount;
                    dr[4] = Convert.ToDouble(Label4.Text) * Convert.ToDouble(Label7.Text) * Convert.ToDouble(Label8.Text) * 48;
                    dr[5] = (Convert.ToDouble(Label4.Text) * Convert.ToDouble(Label7.Text) * Convert.ToDouble(Label8.Text) * 48 != DBCount ? "次數不符" : "");
                    dr[6] = (DBCount % 24 != 0 ? "非正常次數" : "");
                    dt.Rows.Add(dr);
                }
            }
            INCommand.Connection.Close();

            DataGrid1.DataSource = dt.DefaultView;
            DataGrid1.UseAccessibleHeader = true;
            DataGrid1.DataBind();
        }

        public void Show_Job_Detail(object sender, DataGridCommandEventArgs e)
        {
            LinkButton tmpLB;
            tmpLB = (LinkButton)e.Item.Cells[0].Controls[0];

            Response.Redirect("CP.aspx?ID=" + tmpLB.Text);
        }
    }

}