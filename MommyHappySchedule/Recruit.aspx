﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Recruit.aspx.cs" Inherits="MommyHappySchedule.Recruit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        p.MsoNormal {
            margin-bottom: .0001pt;
            font-size: 12.0pt;
            margin-left: 0cm;
            margin-right: 0cm;
            margin-top: 0cm;
        }

        table.MsoNormalTable {
            font-size: 10.0pt;
        }

        .shape {
            behavior: url(#default#VML);
        }

        .auto-style1 {
            /*width: 1177px;*/
        }

        .auto-style3 {
            font-size: 12.0pt;
        }

        .auto-style4 {
            height: 24.15pt;
        }

        .auto-style5 {
            width: 130px;
            height: 15.2pt;
        }

        .auto-style6 {
            width: 72.7pt;
            height: 20pt;
        }

        .auto-style7 {
            width: 198.95pt;
            height: 20pt;
        }
        .auto-style9 {
            font-size: 12.0pt;
            color: #FF0000;
        }
        .auto-style10 {
            color: #FF0000;
        }
        .auto-style11 {
            font-size: 12.0pt;
            color: #FF0000;
        }
        body {
            width:100%;box-sizing:border-box;font-size:1rem;font-family: 'Microsoft JhengHei';padding:0; margin:0;
        }
        .form {
            width:100%;
        }
        .form table{width:100%;}
    </style>
</head>
<body>
    <form class="form" id="form1" runat="server">
        <div>
            <table>
                <tr>
                    <td class="auto-style1">
                        <table>
                            <tr>
                                <td>
                                    <span class="auto-style11">面試人員：</span>
                                </td>
                                <td><span class="auto-style3">
                                    <asp:TextBox ID="EM_NO" runat="server" Width="117px"></asp:TextBox></span> </td>
                                <td class="auto-style9">區別：</td>
                                <td>
                                    <asp:DropDownList ID="Branch" runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td><span class="auto-style3">應徵職務：</span></td>
                                <td><span class="auto-style3">
                                    <asp:RadioButtonList ID="Recruit_Job_Type" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem>居家清潔管家</asp:ListItem>
                                        <asp:ListItem>客服行政專員</asp:ListItem>
                                        <asp:ListItem>其他</asp:ListItem>
                                    </asp:RadioButtonList></span></td>
                                <td><span class="auto-style3">
                                    <asp:TextBox ID="Recruit_Job_Other" runat="server" Width="71px"></asp:TextBox></span></td>
                                <td><span class="auto-style3">填表日期：</span></td>
                                <td><span class="auto-style3">
                                    <asp:TextBox ID="Recruit_Date" runat="server" Text="Label"></asp:TextBox>
                                </span></td>
                                <td><span class="auto-style3">
                                    <asp:Button ID="Save_Data" runat="server" Text="存檔" OnClick="Save_Data_Click"  />
                                </span></td>
                                <td><span class="auto-style3">
                                    <asp:Button ID="Rest_Data" runat="server" Text="清除" />
                                </span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <table border="0" class="MsoNormalTable">
                            <tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes; height: 24.15pt">
                                <td colspan="2" nowrap style="width: 72.7pt; border-top: double 3.0pt; border-left: double 3.0pt; border-bottom: solid 1.0pt; border-right: solid 1.0pt; border-color: windowtext; mso-border-top-alt: thin-thick-small-gap 3.0pt; mso-border-left-alt: thin-thick-small-gap 3.0pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .75pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 24.15pt"
                                    width="97">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3"><span class="auto-style10">姓</span><span lang="EN-US"><span style="mso-spacerun: yes" class="auto-style10">&nbsp;&nbsp;&nbsp; </span></span><span class="auto-style10">名</span><span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="4" nowrap style="width: 67.95pt; border-top: double windowtext 3.0pt; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-top-alt: thin-thick-small-gap windowtext 3.0pt; padding: 0cm 0cm 0cm 0cm; height: 24.15pt"
                                    width="91">

                                    <asp:TextBox ID="EM_Name" runat="server"></asp:TextBox>

                                </td>
                                <td colspan="5" nowrap style="width: 89.35pt; border-top: double windowtext 3.0pt; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-top-alt: thin-thick-small-gap windowtext 3.0pt; padding: 0cm 0cm 0cm 0cm; height: 24.15pt"
                                    width="119">
                                    <p align="center" class="MsoNormal" style="margin-left: -.1pt; mso-para-margin-left: -.11gd; text-align: center; text-indent: -1.2pt; mso-char-indent-count: -.11; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3"><span class="auto-style10">身分證字號</span><span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td nowrap style="border-top: double windowtext 3.0pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-top-alt: thin-thick-small-gap windowtext 3.0pt; padding: 0cm 0cm 0cm 0cm; border-left-style: none; border-left-color: inherit; border-left-width: medium;" class="auto-style4" colspan="16">
                                    <asp:TextBox ID="EM_ID" runat="server"></asp:TextBox>
                                </td>
                                <td colspan="7" rowspan="3" style="width: 97.8pt; border-top: double windowtext 3.0pt; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 3.0pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: thin-thick-small-gap 3.0pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: thin-thick-small-gap 3.0pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 24.15pt"
                                    width="130">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly; height: 143px;">
                                        <span style="font-size: 11.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; color: silver; mso-font-kerning: 0pt">照片黏貼處<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 1; height: 26.05pt">
                                <td colspan="2" nowrap style="width: 72.7pt; border-top: none; border-left: double windowtext 3.0pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-left-alt: thin-thick-small-gap windowtext 3.0pt; padding: 0cm 0cm 0cm 0cm; height: 26.05pt"
                                    width="97">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">性<span lang="EN-US"><span style="mso-spacerun: yes">&nbsp;&nbsp;&nbsp; </span></span>別<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="4" nowrap style="width: 67.95pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 26.05pt"
                                    width="91">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:RadioButtonList ID="EM_SEX" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem>男</asp:ListItem>
                                                <asp:ListItem Selected="True">女</asp:ListItem>
                                            </asp:RadioButtonList></span>
                                    </p>
                                </td>
                                <td colspan="5" nowrap style="width: 89.35pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 26.05pt"
                                    width="119">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3"><span class="auto-style10">出生日期</span><span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="4" nowrap style="width: 95.95pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-right-alt: solid windowtext .5pt; padding: 0cm 0cm 0cm 0cm; height: 26.05pt"
                                    width="128">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <asp:DropDownList ID="Birth_Year" runat="server">
                                        </asp:DropDownList>
                                        <span lang="EN-US" style="font-size: 11.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-kerning: 0pt"><span style="mso-spacerun: yes">&nbsp;</span></span><span class="auto-style3">年
                                            <asp:DropDownList ID="Birth_Month" runat="server">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>11</asp:ListItem>
                                                <asp:ListItem>12</asp:ListItem>
                                            </asp:DropDownList>
                                            &nbsp;月
                                            <asp:DropDownList ID="Birth_Day" runat="server">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>11</asp:ListItem>
                                                <asp:ListItem>12</asp:ListItem>
                                                <asp:ListItem>13</asp:ListItem>
                                                <asp:ListItem>14</asp:ListItem>
                                                <asp:ListItem>15</asp:ListItem>
                                                <asp:ListItem>16</asp:ListItem>
                                                <asp:ListItem>17</asp:ListItem>
                                                <asp:ListItem>18</asp:ListItem>
                                                <asp:ListItem>19</asp:ListItem>
                                                <asp:ListItem>20</asp:ListItem>
                                                <asp:ListItem>21</asp:ListItem>
                                                <asp:ListItem>22</asp:ListItem>
                                                <asp:ListItem>23</asp:ListItem>
                                                <asp:ListItem>24</asp:ListItem>
                                                <asp:ListItem>25</asp:ListItem>
                                                <asp:ListItem>26</asp:ListItem>
                                                <asp:ListItem>27</asp:ListItem>
                                                <asp:ListItem>28</asp:ListItem>
                                                <asp:ListItem>29</asp:ListItem>
                                                <asp:ListItem>30</asp:ListItem>
                                                <asp:ListItem>31</asp:ListItem>
                                            </asp:DropDownList>
                                            &nbsp;日<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="6" style="width: 68.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: .75pt; mso-border-left-alt: .5pt; mso-border-bottom-alt: .75pt; mso-border-right-alt: .5pt; mso-border-color-alt: windowtext; mso-border-style-alt: solid; padding: 0cm 0cm 0cm 0cm; height: 26.05pt"
                                    width="92">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">出 生 地<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="6" style="width: 75.2pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: .75pt; mso-border-left-alt: .5pt; mso-border-bottom-alt: .75pt; mso-border-right-alt: .5pt; mso-border-color-alt: windowtext; mso-border-style-alt: solid; padding: 0cm 0cm 0cm 0cm; height: 26.05pt"
                                    width="100">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:DropDownList ID="Birth_City" runat="server">
                                            </asp:DropDownList></span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 2; height: 25.7pt">
                                <td colspan="2" nowrap style="width: 72.7pt; border-top: none; border-left: double windowtext 3.0pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-left-alt: thin-thick-small-gap windowtext 3.0pt; padding: 0cm 0cm 0cm 0cm; height: 25.7pt"
                                    width="97">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">電<span lang="EN-US"><span style="mso-spacerun: yes">&nbsp;&nbsp;&nbsp; </span></span>話<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="4" nowrap style="width: 67.95pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 25.7pt"
                                    width="91">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3"><span lang="EN-US">
                                            <o:p></o:p>
                                            <asp:TextBox ID="Home_Tele_Number" runat="server"></asp:TextBox>
                                        </span></span>
                                    </p>
                                </td>
                                <td colspan="5" nowrap style="width: 89.35pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 25.7pt"
                                    width="119">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3"><span class="auto-style10">行動電話</span><span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="4" nowrap style="width: 95.95pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-right-alt: solid windowtext .5pt; padding: 0cm 0cm 0cm 0cm; height: 25.7pt"
                                    width="128">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span lang="EN-US" style="font-size: 11.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-kerning: 0pt">
                                            <o:p>&nbsp;</o:p>
                                        </span>
                                        <asp:TextBox ID="Cell_Number" runat="server"></asp:TextBox>
                                    </p>
                                </td>
                                <td colspan="6" style="width: 68.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: .75pt; mso-border-left-alt: .5pt; mso-border-bottom-alt: .75pt; mso-border-right-alt: .5pt; mso-border-color-alt: windowtext; mso-border-style-alt: solid; padding: 0cm 0cm 0cm 0cm; height: 25.7pt"
                                    width="92">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">身高<span lang="EN-US">/</span>體重<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="6" style="width: 75.2pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: .75pt; mso-border-left-alt: .5pt; mso-border-bottom-alt: .75pt; mso-border-right-alt: .5pt; mso-border-color-alt: windowtext; mso-border-style-alt: solid; padding: 0cm 0cm 0cm 0cm; height: 25.7pt"
                                    width="100">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span lang="EN-US" style="font-size: 11.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-kerning: 0pt"><span style="mso-spacerun: yes">&nbsp;</span><asp:TextBox ID="EM_Height" runat="server" Width="46px"></asp:TextBox>
                                            cm/<asp:TextBox ID="EM_Weight" runat="server" Width="45px"></asp:TextBox>
                                            kg<o:p></o:p></span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 3; height: 4.35pt">
                                <td colspan="2" nowrap rowspan="2" style="width: 72.7pt; border-top: none; border-left: double windowtext 3.0pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-left-alt: thin-thick-small-gap windowtext 3.0pt; padding: 0cm 0cm 0cm 0cm; height: 4.35pt"
                                    width="97">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">通訊地址<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="25" nowrap rowspan="2" style="width: 397.25pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 4.35pt"
                                    width="530">
                                    <asp:DropDownList ID="City_Name" runat="server" AutoPostBack="true" OnSelectedIndexChanged="City_Name_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="Area_Name" runat="server" AutoPostBack="true" OnSelectedIndexChanged="Area_Name_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="Road_Name" runat="server" Visible="False">
                                    </asp:DropDownList>
                                    <asp:TextBox ID="Addr_Text" runat="server" Width="314px"></asp:TextBox>
                                </td>
                                <td colspan="6" nowrap style="width: 63.0pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-right-alt: solid windowtext .5pt; padding: 0cm 0cm 0cm 0cm; height: 4.35pt"
                                    width="84">
                                    <p align="center" class="MsoNormal" style="text-align: center; line-height: 12.0pt; mso-line-height-rule: exactly; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">緊急聯絡人</span>
                                    </p>
                                </td>
                                <td style="width: 34.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 3.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: solid .75pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: thin-thick-small-gap 3.0pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 4.35pt"
                                    width="46">
                                    <p align="center" class="MsoNormal" style="text-align: center; line-height: 12.0pt; mso-line-height-rule: exactly; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">關係</span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 4; height: 15.25pt">
                                <td colspan="6" nowrap style="width: 63.0pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-right-alt: solid windowtext .5pt; padding: 0cm 0cm 0cm 0cm; height: 15.25pt"
                                    width="84">
                                    <p class="MsoNormal" style="margin-top: 1.8pt; mso-para-margin-top: .1gd; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:TextBox ID="EM_Contact_Name" runat="server" Width="63pt"></asp:TextBox>
                                        </span>
                                    </p>
                                </td>
                                <td style="width: 34.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 3.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: solid .75pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: thin-thick-small-gap 3.0pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 15.25pt"
                                    width="46">
                                    <p class="MsoNormal" style="margin-top: 1.8pt; mso-para-margin-top: .1gd; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:TextBox ID="EM_Contact_Re" runat="server" Width="34pt"></asp:TextBox></span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 5; height: 5.3pt">
                                <td colspan="2" nowrap rowspan="2" style="width: 72.7pt; border-top: none; border-left: double windowtext 3.0pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-left-alt: thin-thick-small-gap windowtext 3.0pt; padding: 0cm 0cm 0cm 0cm; height: 5.3pt"
                                    width="97">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">戶籍地址<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="3" nowrap rowspan="2" style="width: 397.25pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 5.3pt"
                                    width="65">
                                    <p class="MsoNormal" style="margin-top: 3.6pt; mso-para-margin-top: .2gd; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:RadioButtonList ID="Book_Addr" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                                <asp:ListItem>同上</asp:ListItem>
                                                <asp:ListItem>其它</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </span>

                                    </p>
                                </td>
                                <td colspan="22" nowrap rowspan="2" style="width: 397.25pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 5.3pt"
                                    width="465">
                                    <p class="MsoNormal" style="margin-top: 3.6pt; mso-para-margin-top: .2gd; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:DropDownList ID="City_Name1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="City_Name_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="Area_Name1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="Area_Name_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="Road_Name1" runat="server" Visible="False">
                                            </asp:DropDownList>
                                            <asp:TextBox ID="Addr_Text1" runat="server" Width="314px"></asp:TextBox>
                                        </span>
                                    </p>
                                </td>
                                <td colspan="7" nowrap style="width: 97.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 3.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-right-alt: thin-thick-small-gap windowtext 3.0pt; padding: 0cm 0cm 0cm 0cm; height: 5.3pt"
                                    width="130">
                                    <p align="center" class="MsoNormal" style="text-align: center; line-height: 12.0pt; mso-line-height-rule: exactly; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">緊急聯絡人電話<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 6; height: 11.45pt">
                                <td colspan="7" nowrap style="width: 97.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 3.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-right-alt: thin-thick-small-gap windowtext 3.0pt; padding: 0cm 0cm 0cm 0cm; height: 11.45pt"
                                    width="130">
                                    <p class="MsoNormal" style="margin-top: 1.8pt; mso-para-margin-top: .1gd; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3"><span lang="EN-US">
                                            <o:p></o:p>
                                            <asp:TextBox ID="EM_Contact_Number" runat="server"></asp:TextBox>
                                        </span></span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 7; height: 14.2pt">
                                <td colspan="2" nowrap rowspan="2" style="width: 72.7pt; border-top: none; border-left: double windowtext 3.0pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-left-alt: thin-thick-small-gap windowtext 3.0pt; padding: 0cm 0cm 0cm 0cm; height: 14.2pt"
                                    width="97">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">最高學歷<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="6" nowrap style="width: 115.85pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 14.2pt"
                                    width="154">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">學校名稱<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="10" nowrap style="width: 155.3pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 14.2pt"
                                    width="207">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">科　　系<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="9" nowrap style="width: 126.1pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 14.2pt"
                                    width="168">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">起訖日期<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="4" nowrap style="width: 48.9pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 14.2pt"
                                    width="65">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">畢<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="3" nowrap style="width: 48.9pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 3.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-right-alt: thin-thick-small-gap windowtext 3.0pt; padding: 0cm 0cm 0cm 0cm; height: 14.2pt"
                                    width="65">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">肄<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 8; height: 15.2pt">
                                <td colspan="6" nowrap style="width: 115.85pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 15.2pt"
                                    width="154">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3"><span lang="EN-US">
                                            <o:p></o:p>
                                            <asp:TextBox ID="School_Name" runat="server"></asp:TextBox>
                                        </span></span>
                                    </p>
                                </td>
                                <td colspan="10" nowrap style="width: 155.3pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 15.2pt"
                                    width="207">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3"><span lang="EN-US">
                                            <o:p></o:p>
                                            <asp:TextBox ID="Class_Name" runat="server"></asp:TextBox>
                                        </span></span>
                                    </p>
                                </td>
                                <td colspan="9" nowrap style="width: 126.1pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 15.2pt"
                                    width="168">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3"><span lang="EN-US">
                                            <o:p></o:p>
                                            <asp:TextBox ID="School_Period" runat="server"></asp:TextBox>
                                        </span></span>
                                    </p>
                                </td>
                                <td colspan="7" nowrap style="border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; border-left-style: none; border-left-color: inherit; border-left-width: medium; border-top-style: none; border-top-color: inherit; border-top-width: medium;" class="auto-style5">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:RadioButtonList ID="Gradu_Status" runat="server" RepeatDirection="Horizontal" Width="199px">
                                                <asp:ListItem Selected="True">畢業</asp:ListItem>
                                                <asp:ListItem>肄業</asp:ListItem>
                                            </asp:RadioButtonList></span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 9; height: 16.2pt">
                                <td colspan="2" rowspan="4" style="width: 72.7pt; border-top: none; border-left: double windowtext 3.0pt; border-bottom: double windowtext 1.5pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-top-alt: solid .75pt; mso-border-left-alt: thin-thick-small-gap 3.0pt; mso-border-bottom-alt: double 1.5pt; mso-border-right-alt: solid .75pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 16.2pt"
                                    width="97">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <b style="mso-bidi-font-weight: normal"><span class="auto-style3">＊工作經歷<span lang="EN-US"><o:p></o:p></span></span></b>
                                    </p>
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <b style="mso-bidi-font-weight: normal"><span lang="EN-US" style="font-size: 11.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-kerning: 0pt">(</span><span class="auto-style3">前三個工作<span lang="EN-US">)</span></span></b><span lang="EN-US" style="font-size: 11.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-kerning: 0pt"><o:p></o:p></span>
                                    </p>
                                </td>
                                <td colspan="6" nowrap style="width: 115.85pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 16.2pt"
                                    width="154">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">公司名稱<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="10" nowrap style="width: 155.3pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 16.2pt"
                                    width="207">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">職　　稱<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="16" nowrap style="width: 223.9pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 3.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-right-alt: thin-thick-small-gap windowtext 3.0pt; padding: 0cm 0cm 0cm 0cm; height: 16.2pt"
                                    width="299">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">起訖日期</span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 10; height: 19.2pt">
                                <td colspan="6" nowrap style="width: 115.85pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 19.2pt"
                                    width="154">
                                    <p class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3"><span lang="EN-US">
                                            <o:p></o:p>
                                            <asp:TextBox ID="Pre_Job_Name1" runat="server"></asp:TextBox>
                                        </span></span>
                                    </p>
                                </td>
                                <td colspan="10" nowrap style="width: 155.3pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 19.2pt"
                                    width="207">
                                    <p class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3"><span lang="EN-US">
                                            <o:p></o:p>
                                            <asp:TextBox ID="Pre_Job_Title1" runat="server"></asp:TextBox>
                                        </span></span>
                                    </p>
                                </td>
                                <td colspan="16" nowrap style="width: 223.9pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 3.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-right-alt: thin-thick-small-gap windowtext 3.0pt; padding: 0cm 0cm 0cm 0cm; height: 19.2pt"
                                    width="299">
                                    <p class="MsoNormal" style="mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">自<asp:DropDownList ID="PJ_SY1" runat="server">
                                        </asp:DropDownList>
                                            年<asp:DropDownList ID="PJ_SM1" runat="server">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>11</asp:ListItem>
                                                <asp:ListItem>12</asp:ListItem>
                                            </asp:DropDownList>
                                            月至<asp:DropDownList ID="PJ_EY1" runat="server">
                                            </asp:DropDownList>
                                            年
                                            <asp:DropDownList ID="PJ_EM1" runat="server">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>11</asp:ListItem>
                                                <asp:ListItem>12</asp:ListItem>
                                            </asp:DropDownList>
                                            月止待遇
                                                <asp:TextBox ID="PJ_SA1" runat="server" Width="50"></asp:TextBox>元</span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 10; height: 19.2pt">
                                <td colspan="6" nowrap style="width: 115.85pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 19.2pt"
                                    width="154">
                                    <p class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3"><span lang="EN-US">
                                            <o:p></o:p>
                                            <asp:TextBox ID="Pre_Job_Name2" runat="server"></asp:TextBox>
                                        </span></span>
                                    </p>
                                </td>
                                <td colspan="10" nowrap style="width: 155.3pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 19.2pt"
                                    width="207">
                                    <p class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3"><span lang="EN-US">
                                            <o:p></o:p>
                                            <asp:TextBox ID="Pre_Job_Title2" runat="server"></asp:TextBox>
                                        </span></span>
                                    </p>
                                </td>
                                <td colspan="16" nowrap style="width: 223.9pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 3.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-right-alt: thin-thick-small-gap windowtext 3.0pt; padding: 0cm 0cm 0cm 0cm; height: 19.2pt"
                                    width="299">
                                    <p class="MsoNormal" style="mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">自<asp:DropDownList ID="PJ_SY2" runat="server">
                                        </asp:DropDownList>
                                            年<asp:DropDownList ID="PJ_SM2" runat="server">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>11</asp:ListItem>
                                                <asp:ListItem>12</asp:ListItem>
                                            </asp:DropDownList>
                                            月至<asp:DropDownList ID="PJ_EY2" runat="server">
                                            </asp:DropDownList>
                                            年
                                            <asp:DropDownList ID="PJ_EM2" runat="server">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>11</asp:ListItem>
                                                <asp:ListItem>12</asp:ListItem>
                                            </asp:DropDownList>
                                            月止待遇
                                                <asp:TextBox ID="PJ_SA2" runat="server" Width="50"></asp:TextBox>元</span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 10; height: 19.2pt">
                                <td colspan="6" nowrap style="width: 115.85pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 19.2pt"
                                    width="154">
                                    <p class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3"><span lang="EN-US">
                                            <o:p></o:p>
                                            <asp:TextBox ID="Pre_Job_Name3" runat="server"></asp:TextBox>
                                        </span></span>
                                    </p>
                                </td>
                                <td colspan="10" nowrap style="width: 155.3pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 19.2pt"
                                    width="207">
                                    <p class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3"><span lang="EN-US">
                                            <o:p></o:p>
                                            <asp:TextBox ID="Pre_Job_Title3" runat="server"></asp:TextBox>
                                        </span></span>
                                    </p>
                                </td>
                                <td colspan="16" nowrap style="width: 223.9pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 3.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-right-alt: thin-thick-small-gap windowtext 3.0pt; padding: 0cm 0cm 0cm 0cm; height: 19.2pt"
                                    width="299">
                                    <p class="MsoNormal" style="mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">自<asp:DropDownList ID="PJ_SY3" runat="server">
                                        </asp:DropDownList>
                                            年<asp:DropDownList ID="PJ_SM3" runat="server">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>11</asp:ListItem>
                                                <asp:ListItem>12</asp:ListItem>
                                            </asp:DropDownList>
                                            月至<asp:DropDownList ID="PJ_EY3" runat="server">
                                            </asp:DropDownList>
                                            年
                                            <asp:DropDownList ID="PJ_EM3" runat="server">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>11</asp:ListItem>
                                                <asp:ListItem>12</asp:ListItem>
                                            </asp:DropDownList>
                                            月止待遇
                                                <asp:TextBox ID="PJ_SA3" runat="server" Width="50"></asp:TextBox>元</span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 13; height: 15.0pt">
                                <td colspan="2" nowrap style="width: 72.7pt; border-top: none; border-left: double windowtext 3.0pt; border-bottom: double windowtext 1.5pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: thin-thick-small-gap 3.0pt; mso-border-bottom-alt: double 1.5pt; mso-border-right-alt: solid .75pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 15.0pt"
                                    width="97">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3"><span class="auto-style10">加入途逕</span><span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="20" nowrap style="width: 375.6pt; border-top: none; border-left: none; border-bottom: double windowtext 1.5pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .75pt; mso-border-bottom-alt: double 1.5pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 15.0pt"
                                    width="501">
                                    <p class="MsoNormal" style="margin-left: 2.4pt; mso-para-margin-left: .1gd; text-indent: -1.2pt; mso-char-indent-count: -.1; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:DropDownList ID="Join_Media_Type" runat="server" AutoPostBack="true"  OnSelectedIndexChanged="Join_Media_Type_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:DropDownList ID="Join_Media_Select" runat="server">
                                        </asp:DropDownList>
                                        
                                            <asp:RadioButtonList ID="Join_Media" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" Visible="false">
                                                <asp:ListItem>網路</asp:ListItem>
                                                <asp:ListItem>就業站(介紹卡)</asp:ListItem>
                                                <asp:ListItem>介紹</asp:ListItem>
                                                <asp:ListItem>報紙</asp:ListItem>
                                                <asp:ListItem>其它</asp:ListItem>
                                            </asp:RadioButtonList>
                                            <asp:TextBox ID="Join_Media_Text" runat="server" Width="122px"></asp:TextBox>
                                        </span>
                                    </p>
                                </td>

                                <td colspan="4" style="width: 58.95pt; border-top: none; border-left: none; border-bottom: double windowtext 1.5pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: double 1.5pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 15.0pt"
                                    width="79">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <b style="mso-bidi-font-weight: normal"><span class="auto-style3">希望待遇　<span lang="EN-US">
                                            <o:p></o:p>
                                        </span></span></b>
                                    </p>
                                </td>
                                <td colspan="8" style="width: 60.5pt; border-top: none; border-left: none; border-bottom: double windowtext 1.5pt; border-right: double windowtext 3.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: double 1.5pt; mso-border-right-alt: thin-thick-small-gap 3.0pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 15.0pt"
                                    width="81">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:TextBox ID="Hope_Salary" runat="server"></asp:TextBox>
                                        </span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 14; height: 12.75pt">
                                <td colspan="2" nowrap rowspan="3" style="width: 72.7pt; border-top: none; border-left: double windowtext 3.0pt; border-bottom: none; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: thin-thick-small-gap windowtext 3.0pt; mso-border-right-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="97">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <b style="mso-bidi-font-weight: normal"><span class="auto-style3">＊家庭成員<span lang="EN-US"><o:p></o:p></span></span></b>
                                    </p>
                                </td>
                                <td nowrap style="width: 17.45pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .75pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="23">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span lang="EN-US" style="font-size: 11.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-kerning: 0pt">NO<o:p></o:p></span>
                                    </p>
                                </td>
                                <td colspan="3" style="width: 39.1pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .75pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="59">
                                    <p align="center" class="MsoNormal" style="text-align: center; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">稱謂<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="4" nowrap style="width: 58.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .75pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="72">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">姓名<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="6" style="width: 67.6pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="90">
                                    <p align="center" class="MsoNormal" style="text-align: center; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">服務單位<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="2" style="width: 41.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 1.5pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: double 1.5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="58">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">年齡<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="1" nowrap style="width: 18.0pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: double windowtext 1.5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: double 1.5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="23">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span lang="EN-US" style="font-size: 11.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-kerning: 0pt">NO<o:p></o:p></span>
                                    </p>
                                </td>
                                <td colspan="3" style="width: 44.1pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="59">
                                    <p align="center" class="MsoNormal" style="text-align: center; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">稱謂<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="4" style="width: 54.2pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="72">
                                    <p align="center" class="MsoNormal" style="text-align: center; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">姓名<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="6" style="width: 67.6pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="90">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">服務單位<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="2" style="width: 43.75pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 3.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: thin-thick-small-gap 3.0pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="58">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">年齡<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 15; height: 16.15pt">
                                <td nowrap style="width: 17.45pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-right-alt: solid windowtext .5pt; padding: 0cm 0cm 0cm 0cm; height: 16.15pt"
                                    width="23">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span lang="EN-US" style="font-size: 11.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-kerning: 0pt">1<o:p></o:p></span>
                                    </p>
                                </td>
                                <td colspan="3" style="width: 39.1pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .75pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="59">
                                    <p align="center" class="MsoNormal" style="text-align: center; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:TextBox ID="F_G_C1" runat="server" Width="100%"></asp:TextBox></span>
                                    </p>
                                </td>
                                <td colspan="4" nowrap style="width: 58.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .75pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="72">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:TextBox ID="F_G_Name1" runat="server" Width="100%"></asp:TextBox></span>
                                    </p>
                                </td>
                                <td colspan="6" style="width: 67.6pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="90">
                                    <p align="center" class="MsoNormal" style="text-align: center; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:TextBox ID="F_G_Company1" runat="server" Width="100%"></asp:TextBox></span>
                                    </p>
                                </td>
                                <td colspan="2" style="width: 41.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 1.5pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: double 1.5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="58">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:TextBox ID="F_G_Age1" runat="server" Width="100%"></asp:TextBox></span>
                                    </p>
                                </td>
                                <td colspan="1" nowrap style="width: 18.0pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: double windowtext 1.5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: double 1.5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="23">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span lang="EN-US" style="font-size: 11.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-kerning: 0pt">3<o:p></o:p></span>
                                    </p>
                                </td>
                                <td colspan="3" style="width: 44.1pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="59">
                                    <p align="center" class="MsoNormal" style="text-align: center; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:TextBox ID="F_G_C3" runat="server" Width="100%"></asp:TextBox></span>
                                    </p>
                                </td>
                                <td colspan="4" style="width: 54.2pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="72">
                                    <p align="center" class="MsoNormal" style="text-align: center; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:TextBox ID="F_G_Name3" runat="server" Width="100%"></asp:TextBox></span>
                                    </p>
                                </td>
                                <td colspan="6" style="width: 67.6pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="90">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:TextBox ID="F_G_Company3" runat="server" Width="100%"></asp:TextBox></span>
                                    </p>
                                </td>
                                <td colspan="2" style="width: 43.75pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 3.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: thin-thick-small-gap 3.0pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="58">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:TextBox ID="F_G_Age3" runat="server" Width="100%"></asp:TextBox></span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 16; height: 14.9pt">
                                <td nowrap style="width: 17.45pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-right-alt: solid windowtext .5pt; padding: 0cm 0cm 0cm 0cm; height: 14.9pt"
                                    width="23">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span lang="EN-US" style="font-size: 11.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-kerning: 0pt">2<o:p></o:p></span>
                                    </p>
                                </td>
                                <td colspan="3" style="width: 39.1pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .75pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="59">
                                    <p align="center" class="MsoNormal" style="text-align: center; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:TextBox ID="F_G_C2" runat="server" Width="100%"></asp:TextBox></span>
                                    </p>
                                </td>
                                <td colspan="4" nowrap style="width: 58.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .75pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="72">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:TextBox ID="F_G_Name2" runat="server" Width="100%"></asp:TextBox></span>
                                    </p>
                                </td>
                                <td colspan="6" style="width: 67.6pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="90">
                                    <p align="center" class="MsoNormal" style="text-align: center; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:TextBox ID="F_G_Company2" runat="server" Width="100%"></asp:TextBox></span>
                                    </p>
                                </td>
                                <td colspan="2" style="width: 41.15pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 1.5pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: double 1.5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="58">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:TextBox ID="F_G_Age2" runat="server" Width="100%"></asp:TextBox></span>
                                    </p>
                                </td>
                                <td colspan="1" nowrap style="width: 18.0pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: double windowtext 1.5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: double 1.5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="23">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span lang="EN-US" style="font-size: 11.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-kerning: 0pt">4<o:p></o:p></span>
                                    </p>
                                </td>
                                <td colspan="3" style="width: 44.1pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="59">
                                    <p align="center" class="MsoNormal" style="text-align: center; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:TextBox ID="F_G_C4" runat="server" Width="100%"></asp:TextBox></span>
                                    </p>
                                </td>
                                <td colspan="4" style="width: 54.2pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="72">
                                    <p align="center" class="MsoNormal" style="text-align: center; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:TextBox ID="F_G_Name4" runat="server" Width="100%"></asp:TextBox></span>
                                    </p>
                                </td>
                                <td colspan="6" style="width: 67.6pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="90">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:TextBox ID="F_G_Company4" runat="server" Width="100%"></asp:TextBox></span>
                                    </p>
                                </td>
                                <td colspan="2" style="width: 43.75pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 3.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: thin-thick-small-gap 3.0pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.75pt"
                                    width="58">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:TextBox ID="F_G_Age4" runat="server" Width="100%"></asp:TextBox></span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 17;">
                                <td colspan="2" nowrap style="border-top: 1.5pt double windowtext; border-left: 3.0pt double windowtext; border-bottom: 1.0pt solid windowtext; border-right: 1.0pt solid windowtext; mso-border-top-alt: double 1.5pt; mso-border-left-alt: thin-thick-small-gap 3.0pt; mso-border-bottom-alt: solid .5pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm;"
                                    width="97" class="auto-style6">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">交通工具<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="10" nowrap style="border-bottom: solid windowtext 1.0pt; border-right: double windowtext 1.5pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .5pt; mso-border-right-alt: double 1.5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; border-left-style: none; border-left-color: inherit; border-left-width: medium; border-top-style: none; border-top-color: inherit; border-top-width: medium;"
                                    width="265" class="auto-style7">
                                    <p class="MsoNormal" style="margin-left: 2.3pt; mso-para-margin-left: .1gd; text-align: justify; text-justify: inter-ideograph; text-indent: -1.1pt; mso-char-indent-count: -.1; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:CheckBoxList ID="Trans_Type" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                <asp:ListItem>汽車</asp:ListItem>
                                                <asp:ListItem>機車</asp:ListItem>
                                                <asp:ListItem>其它</asp:ListItem>
                                            </asp:CheckBoxList>
                                            <asp:TextBox ID="Trans_Type_Text" runat="server" Width="100"></asp:TextBox>

                                        </span>

                                    </p>
                                </td>

                                <td nowrap rowspan="5" style="width: 18.0pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: double windowtext 1.5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: double 1.5pt; mso-border-bottom-alt: solid .5pt; mso-border-right-alt: solid .75pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 19.4pt"
                                    valign="top" width="24">
                                    <p align="center" class="MsoNormal" style="text-align: center; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly; height: 220px; width: 46px;">
                                        <b style="mso-bidi-font-weight: normal"><span class="auto-style3">＊<br />
                                            健<br />
                                            康<br />
                                            狀<br />
                                            況<br />
                                            請<br />
                                            註<br />
                                            明</span></b>
                                    </p>
                                </td>
                                <td colspan="21" style="width: 43.75pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 3.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: solid .75pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: thin-thick-small-gap 3.0pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 14.9pt"
                                    width="58">
                                    <p class="MsoNormal" style="mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly; width: 597px;">
                                        <span class="auto-style3">是否曾發生車禍或動過手術
                                            <asp:RadioButtonList ID="Car_Surgery" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                <asp:ListItem>否</asp:ListItem>
                                                <asp:ListItem>是</asp:ListItem>
                                            </asp:RadioButtonList>&nbsp;&nbsp; 原因
                                            <asp:TextBox ID="Car_Surgery_Reason" runat="server" Width="22%"></asp:TextBox></span>
                                    </p>
                                </td>


                            </tr>
                            <tr style="mso-yfti-irow: 18; height: 7.8pt">
                                <td colspan="2" nowrap style="width: 72.7pt; border-top: none; border-left: double windowtext 3.0pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; mso-border-left-alt: thin-thick-small-gap windowtext 3.0pt; padding: 0cm 0cm 0cm 0cm; height: 7.8pt"
                                    width="97">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">駕<span lang="EN-US"><span style="mso-spacerun: yes">&nbsp;&nbsp;&nbsp; </span></span>照<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="10" nowrap style="width: 198.95pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 1.5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; mso-border-right-alt: double windowtext 1.5pt; padding: 0cm 0cm 0cm 0cm; height: 7.8pt"
                                    width="265">
                                    <p class="MsoNormal" style="margin-top: 0cm; margin-right: -10.3pt; margin-bottom: 0cm; margin-left: 2.3pt; margin-bottom: .0001pt; mso-para-margin-top: 0cm; mso-para-margin-right: -.86gd; mso-para-margin-bottom: 0cm; mso-para-margin-left: .1gd; mso-para-margin-bottom: .0001pt; text-indent: -1.1pt; mso-char-indent-count: -.1; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:CheckBoxList ID="Driver_License" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem>輕機車</asp:ListItem>
                                                <asp:ListItem>重機車</asp:ListItem>
                                                <asp:ListItem>小客車</asp:ListItem>
                                                <asp:ListItem>小貨車</asp:ListItem>
                                            </asp:CheckBoxList></span>
                                    </p>
                                </td>
                                <td colspan="21" style="width: 43.75pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 3.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: solid .75pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: thin-thick-small-gap 3.0pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 14.9pt"
                                    width="58">
                                    <p class="MsoNormal" style="mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly; width: 597px;">
                                        <span style="font-size: 11.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-width: 90%; mso-ansi-language: EN-US; mso-fareast-language: ZH-TW; mso-bidi-language: AR-SA">是否領有身心障礙手冊</span><span class="auto-style3">
                                            <asp:RadioButtonList ID="Disable_Status" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                <asp:ListItem>否</asp:ListItem>
                                                <asp:ListItem>是</asp:ListItem>
                                            </asp:RadioButtonList>&nbsp;<span style="font-size: 11.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-width: 90%; mso-ansi-language: EN-US; mso-fareast-language: ZH-TW; mso-bidi-language: AR-SA">障礙類別</span>
                                            <asp:TextBox ID="Disable_Type" runat="server" Width="11%"></asp:TextBox>等級
                                            <asp:TextBox ID="Disable_Level" runat="server" Width="7%"></asp:TextBox></span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 19; height: 7.8pt">
                                <td colspan="2" nowrap style="width: 72.7pt; border-top: none; border-left: double windowtext 3.0pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; mso-border-left-alt: thin-thick-small-gap windowtext 3.0pt; padding: 0cm 0cm 0cm 0cm; height: 4.75pt"
                                    width="97">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">語文能力<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="10" nowrap style="width: 198.95pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 1.5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; mso-border-right-alt: double windowtext 1.5pt; padding: 0cm 0cm 0cm 0cm; height: 4.75pt"
                                    width="265">
                                    <p class="MsoNormal" style="margin-top: 0cm; margin-right: -1.3pt; margin-bottom: 0cm; margin-left: 2.3pt; margin-bottom: .0001pt; mso-para-margin-top: 0cm; mso-para-margin-right: -.11gd; mso-para-margin-bottom: 0cm; mso-para-margin-left: .1gd; mso-para-margin-bottom: .0001pt; text-indent: -1.1pt; mso-char-indent-count: -.1; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly; width: 352px;">
                                        <span class="auto-style3">
                                            <asp:CheckBoxList ID="Language_Type" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem>英語</asp:ListItem>
                                                <asp:ListItem>日語</asp:ListItem>
                                                <asp:ListItem>台語</asp:ListItem>
                                                <asp:ListItem>國語</asp:ListItem>
                                                <asp:ListItem>客語</asp:ListItem>
                                            </asp:CheckBoxList>
                                        </span>
                                    </p>
                                </td>
                                <td colspan="21" rowspan="3" style="width: 278.1pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 3.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: solid .5pt; mso-border-left-alt: solid .75pt; mso-border-bottom-alt: solid .5pt; mso-border-right-alt: thin-thick-small-gap 3.0pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 4.75pt"
                                    valign="top" width="371">

                                    <p class="MsoNormal" style="margin-top: 0cm; margin-right: -1.3pt; margin-bottom: 0cm; margin-left: 2.3pt; margin-bottom: .0001pt; mso-para-margin-top: 0cm; mso-para-margin-right: -.11gd; mso-para-margin-bottom: 0cm; mso-para-margin-left: .1gd; mso-para-margin-bottom: .0001pt; text-align: justify; text-justify: inter-ideograph; text-indent: -1.1pt; mso-char-indent-count: -.1; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:CheckBoxList ID="Health_Status" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" RepeatColumns="5">
                                                <asp:ListItem>癌症</asp:ListItem>
                                                <asp:ListItem>氣喘</asp:ListItem>
                                                <asp:ListItem>風溼</asp:ListItem>
                                                <asp:ListItem>中風</asp:ListItem>
                                                <asp:ListItem>高血壓</asp:ListItem>
                                                <asp:ListItem>心臟病</asp:ListItem>
                                                <asp:ListItem>手腳肌腱(韌帶)受傷</asp:ListItem>
                                                <asp:ListItem>富貴手</asp:ListItem>
                                                <asp:ListItem>脊椎受傷</asp:ListItem>
                                                <asp:ListItem>肺結核</asp:ListItem>
                                                <asp:ListItem>肝炎</asp:ListItem>
                                                <asp:ListItem>聽力受損</asp:ListItem>
                                                <asp:ListItem>先天性/慢性疾病</asp:ListItem>
                                                <asp:ListItem>鬱/燥鬱症或其他精神疾病</asp:ListItem>
                                                <asp:ListItem>其他</asp:ListItem>
                                                <asp:ListItem>無問題</asp:ListItem>
                                            </asp:CheckBoxList>
                                            <asp:TextBox ID="Health_Text" runat="server"></asp:TextBox></span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 20; height: 1.6pt">
                                <td colspan="2" nowrap style="width: 72.7pt; border-top: none; border-left: double windowtext 3.0pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-top-alt: solid .5pt; mso-border-left-alt: thin-thick-small-gap 3.0pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 1.6pt"
                                    width="97">
                                    <p align="center" class="MsoNormal" style="text-align: center; line-height: 13.0pt; mso-line-height-rule: exactly; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">家中可否上網<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="10" nowrap style="width: 198.95pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 1.5pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: solid .5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: double 1.5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 1.6pt"
                                    width="265">
                                    <p class="MsoNormal" style="margin-top: 0cm; margin-right: -1.3pt; margin-bottom: 0cm; margin-left: 2.3pt; margin-bottom: .0001pt; mso-para-margin-top: 0cm; mso-para-margin-right: -.11gd; mso-para-margin-bottom: 0cm; mso-para-margin-left: .1gd; mso-para-margin-bottom: .0001pt; text-indent: -1.1pt; mso-char-indent-count: -.1; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:RadioButtonList ID="Internet_status" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem>可</asp:ListItem>
                                                <asp:ListItem>否</asp:ListItem>
                                                <asp:ListItem>入職後安裝</asp:ListItem>
                                                <asp:ListItem>手機可上網</asp:ListItem>
                                            </asp:RadioButtonList>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 21; height: 34.1pt">
                                <td colspan="2" nowrap style="width: 72.7pt; border-top: none; border-left: double windowtext 3.0pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-top-alt: solid .75pt; mso-border-left-alt: thin-thick-small-gap 3.0pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 34.1pt"
                                    width="97">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">家人是否瞭解<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">從事清潔產業<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="2" style="width: 54.85pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: .75pt; mso-border-left-alt: .5pt; mso-border-bottom-alt: .75pt; mso-border-right-alt: .5pt; mso-border-color-alt: windowtext; mso-border-style-alt: solid; padding: 0cm 0cm 0cm 0cm; height: 34.1pt"
                                    width="73">
                                    <p align="center" class="MsoNormal" style="margin-top: 0cm; margin-right: -1.3pt; margin-bottom: 0cm; margin-left: 2.3pt; margin-bottom: .0001pt; mso-para-margin-top: 0cm; mso-para-margin-right: -.11gd; mso-para-margin-bottom: 0cm; mso-para-margin-left: .1gd; mso-para-margin-bottom: .0001pt; text-align: center; text-indent: -1.1pt; mso-char-indent-count: -.1; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:RadioButtonList ID="Family_support" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem>是</asp:ListItem>
                                                <asp:ListItem>否</asp:ListItem>
                                            </asp:RadioButtonList></span>
                                    </p>
                                </td>
                                <td colspan="4" style="width: 90.95pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: .75pt; mso-border-left-alt: .5pt; mso-border-bottom-alt: .75pt; mso-border-right-alt: .5pt; mso-border-color-alt: windowtext; mso-border-style-alt: solid; padding: 0cm 0cm 0cm 0cm; height: 34.1pt"
                                    width="90">
                                    <p align="center" class="MsoNormal" style="margin-right: -2.15pt; mso-para-margin-right: -.18gd; text-align: center; text-indent: -1.3pt; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">有無負債/貸款</span>
                                    </p>
                                </td>
                                <td colspan="4" style="width: 90pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 1.5pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: solid .75pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: double 1.5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 34.1pt"
                                    width="58">
                                    <p align="center" class="MsoNormal" style="margin-top: 0cm; margin-right: -1.3pt; margin-bottom: 0cm; margin-left: 2.3pt; margin-bottom: .0001pt; mso-para-margin-top: 0cm; mso-para-margin-right: -.11gd; mso-para-margin-bottom: 0cm; mso-para-margin-left: .1gd; mso-para-margin-bottom: .0001pt; text-align: center; text-indent: -1.1pt; mso-char-indent-count: -.1; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:RadioButtonList ID="loan_Status" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem>有</asp:ListItem>
                                                <asp:ListItem>無</asp:ListItem>
                                            </asp:RadioButtonList></span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 22; height: 12.95pt">
                                <td colspan="2" nowrap style="width: 72.7pt; border-top: none; border-left: double windowtext 3.0pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: thin-thick-small-gap 3.0pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: solid .75pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.95pt"
                                    width="97">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">目前家庭概況<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="32" nowrap style="width: 495.05pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 3.0pt; mso-border-top-alt: double windowtext 1.5pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: double 1.5pt; mso-border-left-alt: solid .75pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: thin-thick-small-gap 3.0pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 12.95pt"
                                    width="660">
                                    <p class="MsoNormal" style="margin-top: 0cm; margin-right: -1.3pt; margin-bottom: 0cm; margin-left: 2.3pt; margin-bottom: .0001pt; mso-para-margin-top: 0cm; mso-para-margin-right: -.11gd; mso-para-margin-bottom: 0cm; mso-para-margin-left: .1gd; mso-para-margin-bottom: .0001pt; text-indent: -1.1pt; mso-char-indent-count: -.1; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:RadioButtonList ID="Marrige_Status" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                <asp:ListItem>己婚</asp:ListItem>
                                                <asp:ListItem>未婚</asp:ListItem>
                                                <asp:ListItem>其它</asp:ListItem>
                                            </asp:RadioButtonList>

                                            <asp:TextBox ID="Marrige_Text" runat="server" Width="80"></asp:TextBox>
                                            有<asp:TextBox ID="Child_Number" runat="server" Width="50"></asp:TextBox>
                                            個 3歲以下的子女,由<asp:CheckboxList ID="Child_TakeCare" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                <asp:ListItem>安親班</asp:ListItem>
                                                <asp:ListItem>家人</asp:ListItem>
                                                <asp:ListItem>其它</asp:ListItem>
                                            </asp:CheckboxList>
                                            <asp:TextBox ID="TakeCare_Text" runat="server" Width="100"></asp:TextBox>照顧</span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 23; height: 20.8pt">
                                <td colspan="2" nowrap rowspan="2" style="width: 72.7pt; border-top: none; border-left: double windowtext 3.0pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-left-alt: thin-thick-small-gap windowtext 3.0pt; padding: 0cm 0cm 0cm 0cm; height: 20.8pt"
                                    width="97">
                                    <p align="center" class="MsoNormal" style="margin-left: .05pt; text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">有無健保費<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                    <p align="center" class="MsoNormal" style="margin-left: .05pt; text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">抵免資格<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="12" nowrap style="width: 226.25pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: .75pt; mso-border-left-alt: .75pt; mso-border-bottom-alt: .5pt; mso-border-right-alt: .5pt; mso-border-color-alt: windowtext; mso-border-style-alt: solid; padding: 0cm 0cm 0cm 0cm; height: 20.8pt"
                                    width="302">
                                    <p class="MsoNormal" style="margin-right: -1.3pt; mso-para-margin-right: -.11gd; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:RadioButtonList ID="Free_Insurance_Status" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                <asp:ListItem>無</asp:ListItem>
                                                <asp:ListItem>中低收入</asp:ListItem>
                                                <asp:ListItem>菸捐</asp:ListItem>
                                                <asp:ListItem>其它</asp:ListItem>
                                            </asp:RadioButtonList>


                                            <asp:TextBox ID="Free_Insurance_Text" runat="server" Width="80"></asp:TextBox>
                                        </span>
                                    </p>
                                </td>
                                <td colspan="6" style="width: 72.05pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; mso-border-top-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 20.8pt"
                                    width="96">
                                    <p align="center" class="MsoNormal" style="margin-right: -1.3pt; mso-para-margin-right: -.11gd; text-align: center; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">眷屬健保人數<u><span lang="EN-US"><o:p></o:p></span></u></span>
                                    </p>
                                </td>
                                <td colspan="4" style="width: 58.45pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; mso-border-top-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 20.8pt"
                                    width="78">
                                    <p align="center" class="MsoNormal" style="text-align: center; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span lang="EN-US" style="font-size: 11.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-kerning: 0pt"><span style="mso-spacerun: yes"><span class="auto-style3">


                                            <asp:TextBox ID="Family_IN_Number" runat="server" Width="80"></asp:TextBox>
                                        </span></span></span><span class="auto-style3">人<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="5" style="width: 65.25pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; mso-border-top-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 20.8pt"
                                    width="87">
                                    <p align="center" class="MsoNormal" style="margin-right: -1.3pt; mso-para-margin-right: -.11gd; text-align: center; mso-pagination: widow-orphan; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">扶養人數<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="5" style="width: 73.05pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 3.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: solid .75pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .5pt; mso-border-right-alt: thin-thick-small-gap 3.0pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 20.8pt"
                                    width="97">
                                    <span class="auto-style3">


                                        <asp:TextBox ID="Raise_Number" runat="server" Width="80"></asp:TextBox>
                                        人</span></td>
                            </tr>
                            <tr style="mso-yfti-irow: 24; height: 15.2pt">
                                <td colspan="12" nowrap style="width: 226.25pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .75pt; mso-border-top-alt: .5pt; mso-border-left-alt: .75pt; mso-border-bottom-alt: .75pt; mso-border-right-alt: .5pt; mso-border-color-alt: windowtext; mso-border-style-alt: solid; padding: 0cm 0cm 0cm 0cm; height: 15.2pt"
                                    width="302">
                                    <p class="MsoNormal" style="margin-right: -1.3pt; mso-para-margin-right: -.11gd; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <b style="mso-bidi-font-weight: normal"><span class="auto-style3">＊有健保費抵免資格者，請檢附相關證明影本<span lang="EN-US"><o:p></o:p></span></span></b>
                                    </p>
                                </td>
                                <td colspan="20" style="width: 268.8pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 3.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: solid .5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: thin-thick-small-gap 3.0pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 15.2pt"
                                    width="358">
                                    <p class="MsoNormal" style="margin-right: -1.3pt; mso-para-margin-right: -.11gd; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly; width: 608px;">
                                        <b style="mso-bidi-font-weight: normal"><span style="font-size: 11.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-width: 90%; mso-font-kerning: 0pt">眷屬如需加保請提供：姓名<span lang="EN-US">/</span>關係<span lang="EN-US">/</span>身分證字號<span lang="EN-US">/</span>出生年月日<span lang="EN-US"><o:p></o:p></span></span></b>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 25; height: 16.2pt">
                                <td colspan="2" nowrap style="width: 72.7pt; border-top: none; border-left: double windowtext 3.0pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-left-alt: thin-thick-small-gap windowtext 3.0pt; padding: 0cm 0cm 0cm 0cm; height: 16.2pt"
                                    width="97">
                                    <p align="center" class="MsoNormal" style="text-align: center; line-height: 12.0pt; mso-line-height-rule: exactly; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">個人投保狀況<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="32" nowrap style="width: 495.05pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 3.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-right-alt: thin-thick-small-gap windowtext 3.0pt; padding: 0cm 0cm 0cm 0cm; height: 16.2pt"
                                    width="660">
                                    <p class="MsoNormal" style="margin-right: -1.45pt; mso-para-margin-right: -.12gd; text-align: justify; text-justify: inter-ideograph; line-height: 12.0pt; mso-line-height-rule: exactly; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3"><span lang="EN-US"><span style="mso-spacerun: yes">
                                            <asp:RadioButtonList ID="Insurance_Status" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                <asp:ListItem>公司加保</asp:ListItem>
                                                <asp:ListItem>公(工)會</asp:ListItem>
                                                <asp:ListItem>漁保</asp:ListItem>
                                                <asp:ListItem>農保</asp:ListItem>
                                                <asp:ListItem>其它</asp:ListItem>
                                            </asp:RadioButtonList>


                                            &nbsp;<asp:TextBox ID="Insurance_Text" runat="server" Width="80"></asp:TextBox>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><o:p></o:p></span></span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 26; height: 20.8pt">
                                <td colspan="2" style="width: 72.7pt; border-top: none; border-left: double windowtext 3.0pt; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-left-alt: thin-thick-small-gap windowtext 3.0pt; padding: 0cm 0cm 0cm 0cm; height: 20.8pt"
                                    width="97">
                                    <p align="center" class="MsoNormal" style="margin-left: .05pt; mso-para-margin-left: -.07gd; text-align: center; text-indent: -.9pt; mso-char-indent-count: -.08; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">可受訓日期<span lang="EN-US"><o:p></o:p></span></span>
                                    </p>
                                </td>
                                <td colspan="9" style="width: 154.1pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; padding: 0cm 0cm 0cm 0cm; height: 20.8pt"
                                    width="205">
                                    <p align="center" class="MsoNormal" style="margin-left: -.5pt; mso-para-margin-left: -.04gd; text-align: center; text-indent: .45pt; mso-char-indent-count: .04; mso-pagination: widow-orphan; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:DropDownList ID="Train_Year" runat="server">
                                            </asp:DropDownList>
                                            年
                                            <asp:DropDownList ID="Train_Month" runat="server">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>11</asp:ListItem>
                                                <asp:ListItem>12</asp:ListItem>
                                            </asp:DropDownList>
                                            月
                                            <asp:DropDownList ID="Train_Day" runat="server">
                                                <asp:ListItem>1</asp:ListItem>
                                                <asp:ListItem>2</asp:ListItem>
                                                <asp:ListItem>3</asp:ListItem>
                                                <asp:ListItem>4</asp:ListItem>
                                                <asp:ListItem>5</asp:ListItem>
                                                <asp:ListItem>6</asp:ListItem>
                                                <asp:ListItem>7</asp:ListItem>
                                                <asp:ListItem>8</asp:ListItem>
                                                <asp:ListItem>9</asp:ListItem>
                                                <asp:ListItem>10</asp:ListItem>
                                                <asp:ListItem>11</asp:ListItem>
                                                <asp:ListItem>12</asp:ListItem>
                                                <asp:ListItem>13</asp:ListItem>
                                                <asp:ListItem>14</asp:ListItem>
                                                <asp:ListItem>15</asp:ListItem>
                                                <asp:ListItem>16</asp:ListItem>
                                                <asp:ListItem>17</asp:ListItem>
                                                <asp:ListItem>18</asp:ListItem>
                                                <asp:ListItem>19</asp:ListItem>
                                                <asp:ListItem>20</asp:ListItem>
                                                <asp:ListItem>21</asp:ListItem>
                                                <asp:ListItem>22</asp:ListItem>
                                                <asp:ListItem>23</asp:ListItem>
                                                <asp:ListItem>24</asp:ListItem>
                                                <asp:ListItem>25</asp:ListItem>
                                                <asp:ListItem>26</asp:ListItem>
                                                <asp:ListItem>27</asp:ListItem>
                                                <asp:ListItem>28</asp:ListItem>
                                                <asp:ListItem>29</asp:ListItem>
                                                <asp:ListItem>30</asp:ListItem>
                                                <asp:ListItem>31</asp:ListItem>
                                            </asp:DropDownList>
                                            日<span lang="EN-US"></span>
                                    </p>
                                </td>
                                <td colspan="5" style="width: 75.35pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .75pt; mso-border-alt: solid windowtext .75pt; mso-border-right-alt: solid windowtext .5pt; padding: 0cm 0cm 0cm 0cm; height: 20.8pt"
                                    width="143">
                                    <p align="center" class="MsoNormal" style="margin-right: -.25pt; mso-para-margin-right: -.02gd; text-align: center; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly; width: 237px;">
                                        <b style="mso-bidi-font-weight: normal"><span class="auto-style3">＊<asp:CheckBox ID="Agree_O_County" runat="server" Text="我同意派駐外縣市" />
                                        </span></b>

                                    </p>
                                </td>

                                <td colspan="12" nowrap style="width: 106.95pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: .75pt; mso-border-left-alt: .5pt; mso-border-bottom-alt: .75pt; mso-border-right-alt: .5pt; mso-border-color-alt: windowtext; mso-border-style-alt: solid; padding: 0cm 0cm 0cm 0cm; height: 20.8pt"
                                    width="100">
                                    <p align="center" class="MsoNormal" style="text-align: center; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <b style="mso-bidi-font-weight: normal"><span class="auto-style3">＊請填入可派駐外縣市區域</span></b><span lang="EN-US" style="font-size: 11.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-kerning: 0pt"><o:p></o:p></span>
                                    </p>
                                </td>
                                <td colspan="6" style="width: 77.2pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 3.0pt; mso-border-top-alt: solid windowtext .75pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: solid .75pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .75pt; mso-border-right-alt: thin-thick-small-gap 3.0pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 20.8pt"
                                    width="103">
                                    <p align="center" class="MsoNormal" style="margin-left: 7.55pt; mso-para-margin-left: .63gd; text-align: center; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">
                                            <asp:DropDownList ID="Work_City_Name" runat="server">
                                            </asp:DropDownList>
                                        </span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 27; height: 52.1pt">
                                <td colspan="34" style="width: 567.75pt; border: double windowtext 3.0pt; border-top: none; mso-border-top-alt: solid windowtext .75pt; mso-border-top-alt: solid .75pt; mso-border-left-alt: thin-thick-small-gap 3.0pt; mso-border-bottom-alt: thick-thin-small-gap 3.0pt; mso-border-right-alt: thin-thick-small-gap 3.0pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 52.1pt"
                                    width="757">
                                    <p align="center" class="MsoNormal" style="text-align: center; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <b style="mso-bidi-font-weight: normal"><span style="font-size: 11.0pt; font-family: 標楷體">個人資料使用同意聲明<span lang="EN-US"><o:p></o:p></span></span></b>
                                    </p>
                                    <p class="MsoNormal" style="margin-right: 9.0pt; mso-para-margin-right: .75gd; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span style="font-size: 10.5pt; font-family: 標楷體">本人同意媽咪樂居家服務集團內所屬之關係企業<span lang="EN-US">(</span>下稱「媽咪樂公司」<span lang="EN-US">)</span>在運作管理、業務經營範疇內得依個人資料保護法而正當合理使用本人之個人資料；媽咪樂公司保留本人個人資料的期限，以媽咪樂公司內部作業完成為準，但若法律另有規定者，則不在此限。本人完全明瞭本同意書內容，並勾選以示同意。<span lang="EN-US"><span style="mso-spacerun: yes">&nbsp; </span>
                                            <o:p></o:p>
                                        </span></span>
                                    </p>
                                    <p align="center" class="MsoNormal" style="text-align: center; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <b style="mso-bidi-font-weight: normal"><span lang="EN-US" style="font-size: 11.0pt; font-family: 標楷體"><span style="mso-spacerun: yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;</span></span><span style="font-size: 11.0pt; font-family: 標楷體"><u><span lang="EN-US"><span style="mso-spacerun: yes"><span class="auto-style3"><asp:CheckBox ID="Agree_Private_Data" runat="server" Text="我同意以上個人資料使用聲明" />
                                        </span>&nbsp; </span></span></u></span></b><b style="mso-bidi-font-weight: normal"><u><span lang="EN-US" style="font-size: 10.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-kerning: 0pt">
                                            <o:p></o:p>
                                        </span></u></b>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 28; page-break-inside: avoid; height: 17.8pt">
                                <td nowrap rowspan="3" style="width: 25.3pt; border-top: none; border-left: double windowtext 3.0pt; border-bottom: double windowtext 3.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: thick-thin-small-gap windowtext 3.0pt; mso-border-top-alt: thick-thin-small-gap 3.0pt; mso-border-left-alt: thin-thick-small-gap 3.0pt; mso-border-bottom-alt: thin-thick-small-gap 3.0pt; mso-border-right-alt: solid .5pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; layout-flow: vertical-ideographic; height: 17.8pt"
                                    width="34">
                                    <p class="MsoNormal" style="margin-top: 0cm; margin-right: 5.65pt; margin-bottom: 0cm; margin-left: 5.65pt; margin-bottom: .0001pt; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <b><span class="auto-style3">公司填寫</span></b>
                                    </p>
                                </td>
                                <td colspan="11" style="width: 240pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: solid windowtext 1.0pt; mso-border-top-alt: thick-thin-small-gap windowtext 3.0pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; mso-border-top-alt: thick-thin-small-gap windowtext 3.0pt; padding: 0cm 0cm 0cm 0cm; height: 17.8pt"
                                    width="244">
                                    <p class="MsoNormal" style="layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly; font-size: 12.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-kerning: 0pt;">
                                            <asp:RadioButtonList ID="Interview_Result" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" CssClass="auto-style10">
                                                <asp:ListItem>錄取</asp:ListItem>
                                                <asp:ListItem>安排複試</asp:ListItem>
                                                <asp:ListItem>不錄取</asp:ListItem>
                                                <asp:ListItem Selected="True">保留</asp:ListItem>
                                            </asp:RadioButtonList>


                                    </p>
                                </td>
                                <td colspan="22" style="width: 280pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 3.0pt; mso-border-top-alt: thick-thin-small-gap windowtext 3.0pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: thick-thin-small-gap 3.0pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: solid .5pt; mso-border-right-alt: thin-thick-small-gap 3.0pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 17.8pt"
                                    width="479">
                                    <p class="MsoNormal" style="layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly; width: 733px;">
                                        <span class="auto-style3">預計報到日：<asp:TextBox ID="Duty_Date" runat="server" Width="80"></asp:TextBox><asp:CheckBox ID="No_Register" runat="server" Text="不報到，原因" />
                                            <asp:TextBox ID="No_Register_Text" runat="server" Width="80"></asp:TextBox></span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 29; page-break-inside: avoid; height: 5.85pt">
                                <td colspan="33" style="width: 542.45pt; border-top: none; border-left: none; border-bottom: solid windowtext 1.0pt; border-right: double windowtext 3.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-alt: solid windowtext .5pt; mso-border-right-alt: thin-thick-small-gap windowtext 3.0pt; padding: 0cm 0cm 0cm 0cm; height: 5.85pt"
                                    width="723">
                                    <p class="MsoNormal" style="layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly">
                                        <span class="auto-style3">□報到：資料預計繳交日<span lang="EN-US">:<u><span style="mso-spacerun: yes">&nbsp;&nbsp;&nbsp; &nbsp;</span></u><span style="mso-spacerun: yes">&nbsp;</span></span>追踨日期：<span lang="EN-US"><span style="mso-spacerun: yes">&nbsp; &nbsp;&nbsp;&nbsp;</span></span>追踨人員：<span lang="EN-US"><span style="mso-spacerun: yes">&nbsp;&nbsp;&nbsp;&nbsp; </span></span>資料繳交日期<span lang="EN-US">:<span style="mso-spacerun: yes">&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span>收件人員：　<span lang="EN-US">
                                            <o:p></o:p>
                                        </span></span>
                                    </p>
                                </td>
                            </tr>
                            <tr style="mso-yfti-irow: 30; mso-yfti-lastrow: yes; page-break-inside: avoid; height: 9.55pt">
                                <td colspan="33" style="width: 542.45pt; border-top: none; border-left: none; border-bottom: double windowtext 3.0pt; border-right: double windowtext 3.0pt; mso-border-top-alt: solid windowtext .5pt; mso-border-left-alt: solid windowtext .5pt; mso-border-top-alt: solid .5pt; mso-border-left-alt: solid .5pt; mso-border-bottom-alt: thin-thick-small-gap 3.0pt; mso-border-right-alt: thin-thick-small-gap 3.0pt; mso-border-color-alt: windowtext; padding: 0cm 0cm 0cm 0cm; height: 9.55pt"
                                    width="723">
                                    <p class="MsoNormal" style="margin-top: 1.8pt; mso-para-margin-top: .1gd; layout-grid-mode: char; mso-element: frame; mso-element-frame-hspace: 9.0pt; mso-element-wrap: around; mso-element-anchor-vertical: paragraph; mso-element-anchor-horizontal: column; mso-element-left: center; mso-element-top: .05pt; mso-height-rule: exactly; width: 1148px;">
                                        <b><span class="auto-style3"><span class="auto-style10">評語：</span><asp:TextBox ID="Interview_Text" runat="server" Width="629px"></asp:TextBox>
                                            ＊核定薪資：
                                        <asp:TextBox ID="Interview_Salary" runat="server"></asp:TextBox>
                                            元</span></b>
                                    </p>
                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <p class="MsoNormal" style="margin-left: 9.0pt; text-indent: -9.0pt; mso-char-indent-count: -.9; mso-pagination: widow-orphan; layout-grid-mode: char">
                            <span lang="EN-US" style="font-size: 10.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-kerning: 0pt">1.</span><span style="font-size: 10.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-kerning: 0pt">以上資料若有虛偽不實，本人願無條件接受開除且不要求任何補償。如因而造成公司損失，願付一切法律賠償責任。<span lang="EN-US"><o:p></o:p></span></span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <p class="MsoNormal" style="margin-top: 0cm; margin-right: 4.2pt; margin-bottom: 0cm; margin-left: 9.0pt; margin-bottom: .0001pt; mso-para-margin-top: 0cm; mso-para-margin-right: .35gd; mso-para-margin-bottom: 0cm; mso-para-margin-left: 9.0pt; mso-para-margin-bottom: .0001pt; text-indent: -9.0pt; mso-char-indent-count: -.9; mso-pagination: widow-orphan; layout-grid-mode: char">
                            <span lang="EN-US" style="font-size: 10.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-kerning: 0pt">2.</span><span style="font-size: 10.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-kerning: 0pt">應徵居家清潔管家者，須取得「良民證」並證實無<span lang="EN-US">A</span>、<span lang="EN-US">B</span>、<span lang="EN-US">C</span>型肝炎、肺結核<span lang="EN-US">..</span>等任何法定傳染病始得辦理培訓報到手續<span lang="EN-US">,</span>違者視同未到且不得支領薪資或求任何補償，經任用滿三個月，請直接向駐點分公司申請費用補助。<span lang="EN-US"><o:p></o:p></span></span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <p class="MsoNormal" style="margin-top: 0cm; margin-right: 24.0pt; margin-bottom: 0cm; margin-left: 9.0pt; margin-bottom: .0001pt; mso-para-margin-top: 0cm; mso-para-margin-right: 2.0gd; mso-para-margin-bottom: 0cm; mso-para-margin-left: 9.0pt; mso-para-margin-bottom: .0001pt; text-indent: -9.0pt; mso-char-indent-count: -.9; mso-pagination: widow-orphan; layout-grid-mode: char">
                            <span lang="EN-US" style="font-size: 10.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-kerning: 0pt">3.</span><span style="font-size: 10.0pt; font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-kerning: 0pt">應徵人員資料卡中各欄位，請確實填寫完整；<b style="mso-bidi-font-weight: normal"><span style="border: solid windowtext 1.0pt; mso-border-alt: solid windowtext .5pt; padding: 0cm">此表單為公司機密文件，請勿擅自拷貝或列印</span>。<span lang="EN-US"><o:p></o:p></span></b></span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:TextBox ID="TextBox1" runat="server" Width="1221px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <div class="shape" style="padding: 0pt 0pt 0pt 0pt" v:shape="_x0000_s1026">
                            <p class="MsoNormal" style="margin-right: -.2pt; mso-pagination: widow-orphan; tab-stops: 566.8pt; layout-grid-mode: char">
                                <span style="font-family: 標楷體; mso-bidi-font-family: 新細明體; mso-font-kerning: 0pt">人資承辦人<span lang="EN-US">: <u><span style="mso-spacerun: yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></u> </span>單位主管：<u><span lang="EN-US"><span style="mso-spacerun: yes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span></u><b style="mso-bidi-font-weight: normal">＊面試人員：<u><span lang="EN-US"><span style="mso-spacerun: yes">&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span></u></b><span lang="EN-US"> </span><b style="mso-bidi-font-weight: normal">＊應徵人員親簽：</b> <u><span style="mso-spacerun: yes">&nbsp;</span>　　 　<span lang="EN-US"><span style="mso-spacerun: yes">&nbsp;&nbsp;&nbsp;&nbsp;</span></span></u><span lang="EN-US"><o:p></o:p></span></span>
                            </p>
                        </div>
                        <![if !mso]></td>
                </tr>
            </table>
            </span><![endif]><![if !mso & !vml]>&nbsp;<![endif]><![if !vml]></td>
                                    </tr>
                                </table>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1"></span><![endif]>
                    </td>
                </tr>
            </table>

            

        </div>
    </form>
</body>
</html>
