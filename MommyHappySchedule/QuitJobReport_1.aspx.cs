﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Configuration;
using Models.Library;

namespace MommyHappySchedule
{
    public partial class QuitJobReport_1 : Page
    {
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        SqlConnection INConnection1 = new SqlConnection(ConnString);
        SqlConnection INConnection2 = new SqlConnection(ConnString);
        private SqlCommand INCommand;
        private SqlDataReader SQLReader;
        private DataSet MyDataSet;
        private SqlDataAdapter MyCommand;
        private DataTable  dt1;
        ShareFunction SF = new ShareFunction();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["account"] == null)
            {
                Session["msg"] = "您沒有使用此程式的權限!!";
                Response.Redirect("Index.aspx");
            }

            if (!Page.IsPostBack)
            {
                MyDataSet = new DataSet();
                MyCommand = new SqlDataAdapter("SELECT * FROM Branch_Data", INConnection1);
                MyCommand.Fill(MyDataSet, "Branch");

                Branch.DataSource = MyDataSet.Tables["Branch"].DefaultView;
                Branch.DataTextField = "Branch_Name";
                Branch.DataValueField = "ID";
                Branch.DataBind();

                Check_Year.SelectedValue = DateTime.Now.Year.ToString();
                Check_Month.SelectedValue = DateTime.Now.Month.ToString();
            }
        }


        protected void Check_Report_Click(object sender, EventArgs e)
        {
            // Dim CToday As Integer = Year(Today)
            DataTable dt;
            DataRow dr;
            string Select_Banch;
            int Start_Banch, End_Banch;

            // Label1.Text = Branch.SelectedItem.Text

            dt = new DataTable();
            dt.Columns.Add(new DataColumn("ID", typeof(string)));
            dt.Columns.Add(new DataColumn("LBY_Number", typeof(string)));
            dt.Columns.Add(new DataColumn("LBN_Number", typeof(string)));
            dt.Columns.Add(new DataColumn("LBT_Number", typeof(string)));
            dt.Columns.Add(new DataColumn("LBI_Number", typeof(string)));
            dt.Columns.Add(new DataColumn("LT_Number", typeof(string)));
            dt.Columns.Add(new DataColumn("LLY1_Number", typeof(string)));
            dt.Columns.Add(new DataColumn("LLY2_Number", typeof(string)));
            dt.Columns.Add(new DataColumn("LLY3_Number", typeof(string)));
            dt.Columns.Add(new DataColumn("LLYT_Number", typeof(string)));
            dt.Columns.Add(new DataColumn("LLY_P", typeof(string)));
            dt.Columns.Add(new DataColumn("LLN1_Number", typeof(string)));
            dt.Columns.Add(new DataColumn("LLN2_Number", typeof(string)));
            dt.Columns.Add(new DataColumn("LLN3_Number", typeof(string)));
            dt.Columns.Add(new DataColumn("LLNT_Number", typeof(string)));
            dt.Columns.Add(new DataColumn("LLN_P", typeof(string)));
            dt.Columns.Add(new DataColumn("LLT_Number", typeof(string)));
            dt.Columns.Add(new DataColumn("LLT_P", typeof(string)));
            dt.Columns.Add(new DataColumn("LC_Number", typeof(string)));
            dt.Columns.Add(new DataColumn("S1", typeof(int)));  // 排序用
            dt.Columns.Add(new DataColumn("S2", typeof(int)));  // 排序用


            if (Branch.SelectedIndex == 0)
            {
                Start_Banch = 1;
                End_Banch = Branch.Items.Count - 1;
            }
            else
            {
                Start_Banch = Branch.SelectedIndex;
                End_Banch = Branch.SelectedIndex;
            }

            int jj;
            int [] TB = {0,0,0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 0, 0 };
            int CM = 12;
            if (RadioButtonList1.SelectedIndex != 2)
            {
                if (Check_Year.SelectedValue == DateTime.Now.Year.ToString())
                    CM = DateTime.Now.Month;
                // 總公司或分駐點 MTD
                for (jj = Start_Banch; jj <= End_Banch; jj++)
                {
                    string Sql = "";
                    if (Branch.Items[jj].Text.Trim() == "南高雄")
                        Select_Banch = " (Staff_Job_Data_View.Branch_Name ='南高雄' or Staff_Job_Data_View.Branch_Name = '屏東') ";
                    else
                        Select_Banch = " Staff_Job_Data_View.Branch_Name ='" + Branch.Items[jj].Text + "'";// 營業所名稱

                    MyDataSet = new DataSet();
                    Sql = "SELECT *, DateDiff(month, Join_Date,DATEFROMPARTS(" + Check_Year.SelectedValue + "," + CM.ToString() + ",01)) as Check_Date, DateDiff(month, Join_Date,Depart_Date) as In_Job_Months, DATEPART(year, Depart_Date ) as " + "Depart_Year, DATEPART(Month, Depart_Date ) as Depart_Month, DATEPART(year, Join_Date ) as Join_Year , " + "DATEPART(Month, Join_Date ) as Join_Month FROM Staff_Job_Data_View where " + Select_Banch;
                    MyCommand = new SqlDataAdapter(Sql, INConnection1);
                    MyCommand.Fill(MyDataSet, "BNumber");

                    dr = dt.NewRow();

                    // 排序用
                    string Se_String;
                    Se_String = "";
                    dr[0] = Branch.Items[jj].Text;
                    dr[19] = 0;                   // 總公司 KEY 值為 0
                    dr[20] = jj;                  // 各營業所 KEY 值為累加

                    int BY_Number, BN_Number, BI_Number, LY1_Number, LY2_Number, LY3_Number, LN1_Number, LN2_Number, LN3_Number;
                    // In_Job_Months=  DateDiff(month, Join_Date,Depart_Date) 

                    // 期初總人數 調查的年份 1/1以前入職, 未離職 或 調查的年份 離職的
                    BY_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Staff_No)", "Job_Status_ID = 2 AND Job_Title_ID IN (7, 8, 20, 21) AND (Depart_Date is NULL or Depart_Year >= " + Check_Year.SelectedValue + ") and Join_Year  < " + Check_Year.SelectedValue + Se_String));
                    //BY_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", "(Depart_Date is NULL or Depart_Year >= " + Check_Year.SelectedValue + ") and Join_Year  < " + Check_Year.SelectedValue + Se_String));

                    //期初總人數
                    dr[3] = BY_Number;
                    //dr[3] = (BY_Number + BN_Number).ToString();

                    // 期初未滿一年  調查的年份 1/1以前入職, In_Job_Months <= 12
                    BN_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", "(In_Job_Months <= 12 or Check_Date <= 12 ) and (Depart_Date is NULL or (Depart_Year >= " + Check_Year.SelectedValue + " and Depart_Month >= " + CM.ToString() + ")) and Join_Year = " + (Convert.ToInt16(Check_Year.SelectedValue) - 1).ToString() + Se_String));
                    dr[2] = BN_Number.ToString();


                    // 期初滿一年人數 = 期初總人數 - 期初未滿一年
                    // BY_Number = BY_Number - BN_Number;
                    //dr[1] = BY_Number.ToString();
                    dr[1] = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Staff_No)", "Check_Date > 12"));

                    // 新進人數 調查的年份 1/1以後入職, 未離職 或 調查的年份 離職的
                    BI_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", " Join_Year = " + Check_Year.SelectedValue +
                                "and Join_Month <= " + CM.ToString() + Se_String));
                    dr[4] = BI_Number.ToString();

                    // 總員工數
                    var StaffCount = MyDataSet.Tables["BNumber"].Compute("Count(Staff_No)", "Job_Status_ID = 2 AND Job_Title_ID IN (7, 8, 20, 21)");
                    dr[5] = Convert.ToInt16(StaffCount);
                    //dr[5] = (BY_Number + BN_Number + BI_Number).ToString();

                    // 1年-2年舊員工離職數
                    LY1_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", "Depart_Year = " + Check_Year.SelectedValue + 
                                 " and In_Job_Months > 12 and In_Job_Months < 24 and Depart_Month <= " + CM.ToString() + Se_String));
                    dr[6] = LY1_Number.ToString();

                    // 2年-3年舊員工離職數
                    LY2_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", "Depart_Year = " + Check_Year.SelectedValue + 
                                 " and In_Job_Months >= 24 and In_Job_Months < 48 and Depart_Month <= " + CM.ToString() + Se_String));
                    dr[7] = LY2_Number.ToString();
                    // 3年舊員工離職數
                    LY3_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", "Depart_Year = " + Check_Year.SelectedValue + 
                                 " and In_Job_Months >= 48  and Depart_Month <= " + CM.ToString() + Se_String));
                    dr[8] = LY3_Number.ToString();
                    // 舊員工離職總數
                    dr[9] = (LY1_Number + LY2_Number + LY3_Number).ToString();
                    // 比例
                    if (BY_Number == 0)
                        dr[10] = "0.00%";
                    else
                        dr[10] = (1 - (((LY1_Number + LY2_Number + LY3_Number) * 12 / (double)DateTime.Now.Month) / BY_Number)).ToString("0.00%");


                    // 1-3月新員工離職數
                    LN1_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", "Depart_Year = " + Check_Year.SelectedValue +
                                 " and In_Job_Months <= 3  and Depart_Month <= " + CM.ToString() + Se_String));
                    dr[11] = LN1_Number.ToString();
                    // 3-6月新員工離職數
                    LN2_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", "Depart_Year = " + Check_Year.SelectedValue +
                                 " and In_Job_Months >= 4 and In_Job_Months < 7  and Depart_Month <= " + CM.ToString() + Se_String));
                    dr[12] = LN2_Number.ToString();
                    // 6-12月新員工離職數
                    LN3_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", "Depart_Year = " + Check_Year.SelectedValue +
                                 " and In_Job_Months >= 7 and In_Job_Months <=12  and Depart_Month <= " + CM.ToString() + Se_String));
                    dr[13] = LN3_Number.ToString();
                    // 新進員工離職總數
                    dr[14] = (LN1_Number + LN2_Number + LN3_Number).ToString();
                    // 比例
                    if ((BN_Number + BI_Number) == 0)
                        dr[15] = "0.00%";
                    else
                        dr[15] = (1 - (((LN1_Number + LN2_Number + LN3_Number) * 12 / (double)DateTime.Now.Month) / 
                                 (BN_Number + BI_Number * 12 / (double)DateTime.Now.Month))).ToString("0.00%");


                    // 總離職人數
                    dr[16] = ((LY1_Number + LY2_Number + LY3_Number) + (LN1_Number + LN2_Number + LN3_Number)).ToString();
                    if ((BY_Number + BN_Number + BI_Number) == 0)
                        dr[17] = "0.00%";
                    else
                    {
                        double tmpi, tmpj;
                        tmpi = (LY1_Number + LY2_Number + LY3_Number) + (LN1_Number + LN2_Number + LN3_Number);   // 總離職人數
                        tmpj = Convert.ToDouble(BY_Number + BN_Number);   // 期初人數
                        dr[17] = (1 - (tmpi * 12 / DateTime.Now.Month) / (tmpj + BI_Number * 12 / (double)DateTime.Now.Month)).ToString("0.00%");
                    }

                    dr[18] = (BY_Number + BN_Number + BI_Number) - ((LY1_Number + LY2_Number + LY3_Number) + (LN1_Number + LN2_Number + LN3_Number));
                    dt.Rows.Add(dr);

                    if (Branch.SelectedIndex == 0)
                    {
                        TB[2] = TB[2] + BN_Number;
                        TB[1] = TB[1] + BY_Number;
                        TB[3] = TB[3] + (BY_Number + BN_Number);
                        TB[4] = TB[4] + BI_Number;
                        TB[5] = TB[5] + (BY_Number + BN_Number + BI_Number);
                        TB[6] = TB[6] + LY1_Number;
                        TB[7] = TB[7] + LY2_Number;
                        TB[8] = TB[8] + LY3_Number;
                        TB[9] = TB[9] + (LY1_Number + LY2_Number + LY3_Number);
                        TB[11] = TB[11] + LN1_Number;
                        TB[12] = TB[12] + LN2_Number;
                        TB[13] = TB[13] + LN3_Number;
                        TB[14] = TB[14] + (LN1_Number + LN2_Number + LN3_Number);
                        TB[16] = TB[16] + (LY1_Number + LY2_Number + LY3_Number) + (LN1_Number + LN2_Number + LN3_Number);
                        TB[18] = TB[18] + (BY_Number + BN_Number + BI_Number) - ((LY1_Number + LY2_Number + LY3_Number) + (LN1_Number + LN2_Number + LN3_Number));
                    }
                }
                dr = dt.NewRow();     // 加入空白行
                dt.Rows.Add(dr);

                if (Branch.SelectedIndex == 0)
                {
                    dr = dt.NewRow();
                    if (TB[1] == 0)
                        dr[10] = "0.00%";
                    else
                        dr[10] = (1 - (TB[9] * 12 / (double)DateTime.Now.Month) / (double)TB[1]).ToString("0.00%");
                    if (TB[2] + TB[4] == 0)
                        dr[15] = "0.00%";
                    else
                        dr[15] = (1 - (TB[14] * 12 / (double)DateTime.Now.Month) / (double)(TB[2]+ TB[4] * 12 / (double)DateTime.Now.Month)).ToString("0.00%");
                    if (TB[5] == 0)
                        dr[17] = "0.00%";
                    else
                        dr[17] = (1 - (TB[16] * 12 / (double)DateTime.Now.Month) / (double)(TB[3] + TB[4] * 12 / (double)DateTime.Now.Month)).ToString("0.00%");


                    for (var ii = 0; ii <= 18; ii++)
                    {
                        if (ii == 10 | ii == 15 | ii == 17)
                        {
                        }
                        else
                            dr[ii] = TB[ii].ToString();
                    }
                    dr[19] = 0;
                    dr[20] = 0;
                    dt.Rows.InsertAt(dr, 0);
                }


                // 1-12月

                for (jj = 1; jj <= 12; jj++)
                {
                    string BranchName = Branch.SelectedItem.Text.Trim();
                    string Year = Check_Year.SelectedValue;
                    if (BranchName == "南高雄")
                        Select_Banch = "WHERE (Staff_Job_Data_View.Branch_Name ='南高雄' OR Staff_Job_Data_View.Branch_Name = '屏東')";
                    else
                        Select_Banch = "WHERE Staff_Job_Data_View.Branch_Name ='" + BranchName + "'";// 營業所名稱

                    MyDataSet = new DataSet();
                    string Sql = "SELECT *, DATEDIFF(MONTH, Join_Date, DATEFROMPARTS(" + Year + "," + jj.ToString() +
                                ", 01)) as Check_Date, DATEDIFF(MONTH, Join_Date,Depart_Date) as In_Job_Months, DATEPART(YEAR, Depart_Date ) as Depart_Year,  DATEPART(MONTH, Depart_Date ) as Depart_Month, DATEPART(YEAR, Join_Date ) as Join_Year, DATEPART(MONTH, Join_Date ) as Join_Month FROM Staff_Job_Data_View " + Select_Banch;
                    MyCommand = new SqlDataAdapter(Sql, INConnection1);
                    MyCommand.Fill(MyDataSet, "BNumber");

                    dr = dt.NewRow();

                    // 排序用
                    string Se_String;

                    Se_String = "";
                    dr[0] = jj.ToString() + "月";
                    dr[19] = 0;                   // 總公司 KEY 值為 0
                    dr[20] = jj;                  // 各營業所 KEY 值為累加


                    int BY_Number, BN_Number, BI_Number, LY1_Number, LY2_Number, LY3_Number, LN1_Number, LN2_Number, LN3_Number;
                    // In_Job_Months=  DateDiff(month, Join_Date,Depart_Date) 
                    // 期初總人數 調查的年份 1/1以前入職, 未離職 或 調查的年份 離職的

                    // 初期總人數
                    BY_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Staff_No)", "Job_Status_ID = 2 AND Job_Title_ID IN (7, 8, 20, 21) AND (Depart_Date is NULL or Depart_Year >= " + Check_Year.SelectedValue + ") and Join_Year  < " + Check_Year.SelectedValue + Se_String));
                    //BY_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", "(Depart_Date is NULL or Depart_Year >= " +  Check_Year.SelectedValue + ") and Join_Year  < " + Check_Year.SelectedValue + Se_String));

                    //期初總人數 
                    dr[3] = BY_Number;
                    //dr[3] = (BY_Number + BN_Number).ToString();

                    // 期初未滿一年  調查的年份 1/1以前入職, In_Job_Months <= 12
                    Sql = "(In_Job_Months <= 12 or Check_Date <= 12 ) and (Depart_Date is NULL or (Depart_Year >= " + Check_Year.SelectedValue + " and Depart_Month >= " + jj.ToString() + ") ) and Join_Year = " + (Convert.ToInt16(Check_Year.SelectedValue) - 1).ToString() + Se_String;
                    BN_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", Sql));
                    dr[2] = BN_Number.ToString();

                    //  期初滿一年人數 = 期初總人數-期初未滿一年
                    //BY_Number = BY_Number - BN_Number;
                    //dr[1] = BY_Number.ToString();
                    dr[1] = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Staff_No)", "Check_Date > 12"));

                    // 新進人數 調查的年份 1/1以後入職, 未離職 或 調查的年份 離職的
                    BI_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", " Join_Year = " + Check_Year.SelectedValue +  " and Join_Month = " + jj.ToString() + Se_String));
                    dr[4] = BI_Number.ToString();

                    // 總員工數
                    var StaffCount = MyDataSet.Tables["BNumber"].Compute("Count(Staff_No)", "Job_Status_ID = 2 AND Job_Title_ID IN (7, 8, 20, 21)");
                    dr[5] = Convert.ToInt16(StaffCount);
                    //dr[5] = (BY_Number + BN_Number + BI_Number).ToString();

                    // 1年-2年舊員工離職數
                    LY1_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", "Depart_Year = " + Check_Year.SelectedValue +
                                 " and In_Job_Months > 12 and In_Job_Months < 24 and Depart_Month = " + jj.ToString() + Se_String));
                    dr[6] = LY1_Number.ToString();

                    // 2年-3年舊員工離職數
                    LY2_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", "Depart_Year = " + Check_Year.SelectedValue + 
                                 " and In_Job_Months >= 24 and In_Job_Months < 48 and Depart_Month = " + jj.ToString() + Se_String));
                    dr[7] = LY2_Number.ToString();
                    // 3年舊員工離職數
                    LY3_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", "Depart_Year = " + Check_Year.SelectedValue + 
                                 " and In_Job_Months >= 48  and Depart_Month = " + jj.ToString() + Se_String));
                    dr[8] = LY3_Number.ToString();
                    // 舊員工離職總數
                    dr[9] = (LY1_Number + LY2_Number + LY3_Number).ToString();
                    // 比例
                    if (BY_Number == 0)
                        dr[10] = "";
                    else
                        dr[10] = "";// dr(10) = (1 - ((LY1_Number + LY2_Number + LY3_Number) / BY_Number)).ToString("0.00%")


                    // 1-3月新員工離職數
                    LN1_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", "Depart_Year = " + Check_Year.SelectedValue + 
                                 " and In_Job_Months <= 3  and Depart_Month = " + jj.ToString() + Se_String));
                    dr[11] = LN1_Number.ToString();
                    // 3-6月新員工離職數
                    LN2_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", "Depart_Year = " + Check_Year.SelectedValue + 
                                 " and In_Job_Months >= 4 and In_Job_Months < 7  and Depart_Month = " + jj.ToString() + Se_String));
                    dr[12] = LN2_Number.ToString();
                    // 6-12月新員工離職數
                    LN3_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", "Depart_Year = " + Check_Year.SelectedValue + 
                                 " and In_Job_Months >= 7 and In_Job_Months <=12  and Depart_Month = " + jj.ToString() + Se_String));
                    dr[13] = LN3_Number.ToString();
                    // 新進員工離職總數
                    dr[14] = (LN1_Number + LN2_Number + LN3_Number).ToString();
                    // 比例
                    if ((BN_Number + BI_Number) == 0)
                        dr[15] = "";
                    else
                        dr[15] = "";// dr(15) = (1 - ((LN1_Number + LN2_Number + LN3_Number) / (BN_Number + BI_Number))).ToString("0.00%")


                    // 總離職人數
                    dr[16] = ((LY1_Number + LY2_Number + LY3_Number) + (LN1_Number + LN2_Number + LN3_Number)).ToString();
                    if ((BY_Number + BN_Number + BI_Number) == 0)
                        dr[17] = "";
                    else
                        dr[17] = "";// dr(17) = (1 - (((LY1_Number + LY2_Number + LY3_Number) + (LN1_Number + LN2_Number + LN3_Number)) /
                                    //(BY_Number + BN_Number + BI_Number))).ToString("0.00%")

                    dr[18] = (BY_Number + BN_Number + BI_Number) - ((LY1_Number + LY2_Number + LY3_Number) + (LN1_Number + LN2_Number + LN3_Number));

                    dt.Rows.Add(dr);
                }

                // dt.DefaultView.Sort = "S1 ASC, S2 ASC"   '排序
                Repeater1.DataSource = dt.DefaultView;
                Repeater1.DataBind();


                // 未晉升明細表
                int kk = 0;
                dt1 = new DataTable();
                dt1.Columns.Add(new DataColumn("ID", typeof(string)));
                dt1.Columns.Add(new DataColumn("Staff_Name", typeof(string)));
                dt1.Columns.Add(new DataColumn("Join_Date", typeof(DateTime)));
                dt1.Columns.Add(new DataColumn("No_P_Re", typeof(string)));
                dt1.Columns.Add(new DataColumn("Branch", typeof(string)));


                for (jj = Start_Banch; jj <= End_Banch; jj++)
                {
                    if (Branch.Items[jj].Text == "南高雄")
                        Select_Banch = Branch.Items[jj].Text + " or Branch_Name = '屏東'";
                    else
                        Select_Banch = Branch.Items[jj].Text;// 營業所名稱

                    int CT_Number;
                    INCommand = new SqlCommand(
                                "SELECT Count(Job_Status)  FROM Staff_Job_Data_View where Depart_Date is null and Job_Status = '在職' and" +
                                " DATEDIFF (dy,Join_Date,getdate()) >60 and Job_Title='一般服務員' and Branch_Name = '" + Select_Banch + "'", INConnection1);
                    INCommand.Connection.Open();
                    SQLReader = INCommand.ExecuteReader();
                    if (SQLReader.Read() == true)
                        CT_Number = Convert.ToInt16(SQLReader[0]);
                    else
                        CT_Number = 0;
                    INCommand.Connection.Close();

                    MyDataSet = new DataSet();

                    MyCommand = new SqlDataAdapter(
                                "SELECT *   FROM Staff_Job_Data_View where Depart_Date is null and Job_Status = '在職' and DATEDIFF (dy,Join_Date,getdate()) >60 and" +
                                " Job_Title='一般服務員' and Branch_Name = '" + Select_Banch + "'", INConnection1);
                    MyCommand.Fill(MyDataSet, "No_Prom_List");

                    int ii;


                    for (ii = 0; ii <= MyDataSet.Tables["No_Prom_List"].DefaultView.Count - 1; ii++)
                    {
                        dr = dt1.NewRow();

                        dr[0] = CT_Number;  // MyDataSet.Tables("No_Prom_List").DefaultView.Item(ii)(1)
                        dr[1] = MyDataSet.Tables["No_Prom_List"].DefaultView[ii][1];
                        dr[2] = MyDataSet.Tables["No_Prom_List"].DefaultView[ii][8];
                        dr[3] = MyDataSet.Tables["No_Prom_List"].DefaultView[ii][10];
                        dr[4] = MyDataSet.Tables["No_Prom_List"].DefaultView[ii][3];
                        dt1.Rows.Add(dr);
                        kk = kk + 1;
                    }
                }



                dt1.DefaultView.Sort = "Branch ASC,ID ASC";   // 排序
                DataGrid1.DataSource = dt1.DefaultView;
                DataGrid1.DataBind();

                TextBox1.Text = kk.ToString();
            }
            else
            {
                for (jj = Start_Banch; jj <= End_Banch; jj++)
                {
                    string Sql = "";
                    if (Branch.Items[jj].Text.Trim() == "南高雄")
                        Select_Banch = " (Staff_Job_Data_View.Branch_Name ='南高雄' or Staff_Job_Data_View.Branch_Name = '屏東') ";
                    else
                        Select_Banch = " Staff_Job_Data_View.Branch_Name ='" + Branch.Items[jj].Text + "'";// 營業所名稱

                    MyDataSet = new DataSet();
                    Sql = "SELECT *, DateDiff(month, Join_Date,DATEFROMPARTS(" + Check_Year.SelectedValue + "," +
                                Check_Month.SelectedValue + ",01)) as Check_Date, DateDiff(month, Join_Date,Depart_Date) as In_Job_Months, DATEPART(year, Depart_Date ) as Depart_Year, DATEPART(Month, Depart_Date ) as Depart_Month, DATEPART(year, Join_Date ) as Join_Year , DATEPART(Month, Join_Date ) as Join_Month FROM Staff_Job_Data_View where " + Select_Banch;
                    MyCommand = new SqlDataAdapter(Sql, INConnection1);
                    MyCommand.Fill(MyDataSet, "BNumber");

                    Sql = "SELECT Staff_Job_Data_View.Svr_Name FROM Staff_Job_Data_View LEFT OUTER JOIN " + " Staff_Job_Data_View [Staff_Job_Data_View_1] ON Staff_Job_Data_View.Svr_Name = [Staff_Job_Data_View_1].Staff_Name " + " WHERE   (" + Select_Banch + ") AND (Staff_Job_Data_View.Depart_Date Is NULL OR " + " YEAR(Staff_Job_Data_View.Depart_Date) >= " + Check_Year.SelectedValue + ") GROUP BY Staff_Job_Data_View.Svr_Name, [Staff_Job_Data_View_1].Depart_Date, [Staff_Job_Data_View_1].Staff_Name,  [Staff_Job_Data_View_1].Job_Status HAVING  ([Staff_Job_Data_View_1].Depart_Date Is NULL) And ([Staff_Job_Data_View_1].Job_Status <> '調職') OR " + " (YEAR([Staff_Job_Data_View_1].Depart_Date) >= " + Check_Year.SelectedValue + " And Month(Staff_Job_Data_View_1.Depart_Date) >= " + Check_Month.SelectedValue + ") Or ([Staff_Job_Data_View_1].Staff_Name IS NULL) ORDER BY Staff_Job_Data_View_1.Staff_Name";
                    MyCommand = new SqlDataAdapter(Sql, INConnection1);
                    MyCommand.Fill(MyDataSet, "Svr_Name");

                    int ii;

                    for (ii = 0; ii <= MyDataSet.Tables["Svr_Name"].DefaultView.Count; ii++)
                    {
                        if (ii == 0)
                        {
                            dr = dt.NewRow();     // 加入空白行
                            dt.Rows.Add(dr);
                        }

                        dr = dt.NewRow();

                        // 排序用
                        string Se_String;
                        if (ii == 0)
                        {
                            Se_String = "";
                            dr[0] = Branch.Items[jj].Text;
                        }
                        else
                        {
                            Se_String = " and  Svr_Name= '" + MyDataSet.Tables["Svr_Name"].DefaultView[ii - 1][0] + "'";
                            dr[0] = MyDataSet.Tables["Svr_Name"].DefaultView[ii - 1][0];
                        }

                        int BY_Number, BN_Number, BI_Number, LY1_Number, LY2_Number, LY3_Number, LN1_Number, LN2_Number, LN3_Number;
                        // In_Job_Months=  DateDiff(month, Join_Date,Depart_Date) 
                        // 期初總人數 調查的年份 1/1以前入職, 未離職 或 調查的年份 離職的
                        //BY_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", "(Depart_Date is NULL or Depart_Year >= " + Check_Year.SelectedValue + ") and Join_Year < " + Check_Year.SelectedValue + Se_String));
                        BY_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Staff_No)", "Job_Status_ID = 2 AND Job_Title_ID IN (7, 8, 20, 21) AND (Depart_Date is NULL or Depart_Year >= " + Check_Year.SelectedValue + ") and Join_Year  < " + Check_Year.SelectedValue + Se_String));

                        // 期初總人數
                        dr[3] = BY_Number;
                        //dr[3] = (BY_Number + BN_Number).ToString();

                        // 期初未滿一年  調查的年份 1/1以前入職, In_Job_Months <= 12
                        BN_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", "(In_Job_Months <= 12 or Check_Date <= 12 ) and" +
                                    " (Depart_Date is NULL or (Depart_Year >= " + Check_Year.SelectedValue + " and Depart_Month >= " +
                                    Check_Month.SelectedValue + ")) and Join_Year = " + (Convert.ToInt16(Check_Year.SelectedValue) - 1).ToString() + Se_String));
                        dr[2] = BN_Number.ToString();


                        // 期初滿一年人數 = 期初總人數-期初未滿一年
                        //BY_Number = BY_Number - BN_Number;
                        //dr[1] = BY_Number.ToString();
                        dr[1] = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Staff_No)", "Check_Date > 12"));

                        // 新進人數 調查的年份 1/1以後入職, 未離職 或 調查的年份 離職的
                        BI_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", " Join_Year = " + Check_Year.SelectedValue + 
                                    "and Join_Month <= " + Check_Month.SelectedValue + Se_String));
                        dr[4] = BI_Number.ToString();

                        // 總員工數
                        var StaffCount = MyDataSet.Tables["BNumber"].Compute("Count(Staff_No)", "Job_Status_ID = 2 AND Job_Title_ID IN (7, 8, 20, 21)");
                        dr[5] = Convert.ToInt16(StaffCount);
                        //dr[5] = (BY_Number + BN_Number + BI_Number).ToString();

                        // 1年-2年舊員工離職數
                        LY1_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", "Depart_Year = " + Check_Year.SelectedValue + 
                                     " and In_Job_Months > 12 and In_Job_Months < 24 and Depart_Month <= " + Check_Month.SelectedValue + Se_String));
                        dr[6] = LY1_Number.ToString();

                        // 2年-3年舊員工離職數
                        LY2_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", "Depart_Year = " + Check_Year.SelectedValue + 
                                     " and In_Job_Months >= 24 and In_Job_Months < 48 and Depart_Month <= " + Check_Month.SelectedValue + Se_String));
                        dr[7] = LY2_Number.ToString();
                        // 3年舊員工離職數
                        LY3_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", "Depart_Year = " + Check_Year.SelectedValue + 
                                     " and In_Job_Months >= 48  and Depart_Month <= " + Check_Month.SelectedValue + Se_String));
                        dr[8] = LY3_Number.ToString();
                        // 舊員工離職總數
                        dr[9] = (LY1_Number + LY2_Number + LY3_Number).ToString();
                        // 比例
                        if (BY_Number == 0)
                            dr[10] = "0.00%";
                        else
                            dr[10] = (1 - (((LY1_Number + LY2_Number + LY3_Number) * 12 / (double)DateTime.Now.Month) / BY_Number)).ToString("0.00%");


                        // 1-3月新員工離職數
                        LN1_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", "Depart_Year = " + Check_Year.SelectedValue + 
                                     " and In_Job_Months <= 3  and Depart_Month <= " + Check_Month.SelectedValue + Se_String));
                        dr[11] = LN1_Number.ToString();
                        // 3-6月新員工離職數
                        LN2_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", "Depart_Year = " + Check_Year.SelectedValue + 
                                     " and In_Job_Months >= 4 and In_Job_Months < 7  and Depart_Month <= " + Check_Month.SelectedValue + Se_String));
                        dr[12] = LN2_Number.ToString();
                        // 6-12月新員工離職數
                        LN3_Number = Convert.ToInt16(MyDataSet.Tables["BNumber"].Compute("Count(Job_Status)", "Depart_Year = " + Check_Year.SelectedValue + 
                                     " and In_Job_Months >= 7 and In_Job_Months <=12  and Depart_Month <= " + Check_Month.SelectedValue + Se_String));
                        dr[13] = LN3_Number.ToString();
                        // 新進員工離職總數
                        dr[14] = (LN1_Number + LN2_Number + LN3_Number).ToString();
                        // 比例
                        if ((BN_Number + BI_Number) == 0)
                            dr[15] = "0.00%";
                        else
                            // dr(15) = (1 - ((LN1_Number + LN2_Number + LN3_Number) / (BN_Number + BI_Number))).ToString("0.00%")
                            dr[15] = (1 - (((LN1_Number + LN2_Number + LN3_Number) * 12 / (double)DateTime.Now.Month) / 
                                     (BN_Number + BI_Number * 12 / (double)DateTime.Now.Month))).ToString("0.00%");


                        // 總離職人數
                        dr[16] = ((LY1_Number + LY2_Number + LY3_Number) + (LN1_Number + LN2_Number + LN3_Number)).ToString();
                        if ((BY_Number + BN_Number + BI_Number) == 0)
                            dr[17] = "0.00%";
                        else
                        {
                            // dr(17) = (1 - (((LY1_Number + LY2_Number + LY3_Number) + (LN1_Number + LN2_Number + LN3_Number)) /
                            // (BY_Number + BN_Number + BI_Number))).ToString("0.00%")
                            double tmpi, tmpj;
                            tmpi = (LY1_Number + LY2_Number + LY3_Number) + (LN1_Number + LN2_Number + LN3_Number);   // 總離職人數
                            tmpj = Convert.ToDouble(BY_Number + BN_Number);   // 期初人數
                            dr[17] = (1 - (tmpi * 12 / DateTime.Now.Month) / (tmpj + BI_Number * 12 / (double)DateTime.Now.Month)).ToString("0.00%");
                        }

                        dr[18] = (BY_Number + BN_Number + BI_Number) - ((LY1_Number + LY2_Number + LY3_Number) + (LN1_Number + LN2_Number + LN3_Number));
                        dt.Rows.Add(dr);
                    }
                }
                // dt.DefaultView.Sort = "S1 ASC, S2 ASC"   '排序
                Repeater2.DataSource = dt.DefaultView;
                Repeater2.DataBind();
            }
            Page_Load(sender, e);
        }

        public void ItemsGrid_Edit(object sender, DataGridCommandEventArgs e)
        {

            // Set the EditItemIndex property to the index of the item clicked 
            // in the DataGrid control to enable editing for that item. Be sure
            // to rebind the DateGrid to the data source to refresh the control.
            DataGrid1.EditItemIndex = e.Item.ItemIndex;
            Check_Report_Click(sender, e);
        }

        public void ItemsGrid_Cancel(object sender, DataGridCommandEventArgs e)
        {

            // Set the EditItemIndex property to -1 to exit editing mode.
            // Be sure to rebind the DateGrid to the data source to refresh
            // the control.
            DataGrid1.EditItemIndex = -1;
            Check_Report_Click(sender, e);
        }

        public void ItemsGrid_Update(object sender, DataGridCommandEventArgs e)
        {
            string INString;
            TextBox ReText = (TextBox)e.Item.Cells[5].Controls[0];
            string ReNum = e.Item.Cells[2].Text;
            string ReNam = e.Item.Cells[3].Text;
            string ReDate = e.Item.Cells[4].Text;


            INString = "Update Staff_Job_Data set NoPrm_Reason = '" + ReText.Text + "' where Staff_Name = '" + ReNam + "' and Branch_Name = '" + ReNum +
                       "' and Join_Date ='" + ReDate + "'";
            INCommand = new SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            INCommand.ExecuteNonQuery();
            INCommand.Connection.Close();
            // Label2.Text = INString
            DataGrid1.EditItemIndex = -1;
            Check_Report_Click(sender, e);
        }
        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = RadioButtonList1.SelectedIndex;
            if (RadioButtonList1.SelectedIndex == 2)
                Check_Month.Visible = true;
            else
                Check_Month.Visible = false;
            Page_Load(sender, e);
        }
        protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            ListItemType objItemType = (ListItemType)e.Item.ItemType;
            Label tmpLabel1, tmpLabel2;

            if (objItemType == ListItemType.Item | objItemType == ListItemType.AlternatingItem)
            {
                tmpLabel1 = (Label)e.Item.FindControl("Label1");

                // 留任率
                tmpLabel2 = (Label)e.Item.FindControl("LLT_P");
                if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "")
                {
                    if (tmpLabel2.Text.Trim() != "&nbsp;" & tmpLabel2.Text.Trim() != "")
                    {
                        if (SF.IsNumeric(tmpLabel2.Text.Substring(0, tmpLabel2.Text.Length - 1)))
                        {
                            if (Convert.ToDouble(tmpLabel2.Text.Substring(0, tmpLabel2.Text.Length - 1)) < 80.0)
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                        }
                    }
                }
                // 舊管家留任率
                tmpLabel2 = (Label)e.Item.FindControl("LLY_P");
                if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "")
                {
                    if (tmpLabel2.Text.Trim() != "&nbsp;" & tmpLabel2.Text.Trim() != "")
                    {
                        if (SF.IsNumeric(tmpLabel2.Text.Substring(0, tmpLabel2.Text.Length - 1)))
                        {
                            if (Convert.ToDouble(tmpLabel2.Text.Substring(0, tmpLabel2.Text.Length - 1)) < 80.0)
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                        }
                    }
                }
                // 新管家留任率
                tmpLabel2 = (Label)e.Item.FindControl("LLN_P");
                if (tmpLabel1.Text.Trim() != "總公司" & tmpLabel1.Text.Trim() != "Total" & tmpLabel1.Text.Trim() != "" & tmpLabel1.Text.Trim() != "&nbsp;")
                {
                    if (tmpLabel2.Text.Trim() != "&nbsp;" & tmpLabel2.Text.Trim() != "")
                    {
                        if (SF.IsNumeric(tmpLabel2.Text.Substring(0, tmpLabel2.Text.Length - 1)))
                        {
                            if (Convert.ToDouble(tmpLabel2.Text.Substring(0, tmpLabel2.Text.Length - 1)) < 80.0)
                            {
                                tmpLabel2.ForeColor = Color.Red;
                                tmpLabel2.Font.Bold = true;
                            }
                        }
                    }
                }
            }
        }
        protected void DataGrid1_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            ListItemType objItemType = (ListItemType)e.Item.ItemType;

            if (objItemType == ListItemType.Item | objItemType == ListItemType.AlternatingItem)
            {
                if (Convert.ToInt32(e.Item.Cells[1].Text) >= 5)
                {
                    e.Item.ForeColor = Color.Red;
                    e.Item.Font.Bold = true;
                }
            }
        }
    }

}