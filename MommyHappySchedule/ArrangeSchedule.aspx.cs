﻿using Models.Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MommyHappySchedule
{

    public partial class ArrangeSchedule : System.Web.UI.Page
    {
        //private System.Configuration.Configuration rootWebConfig;
        //private System.Configuration.ConnectionStringSettings connString, connString1;

        private System.Data.DataTable dt;
        private System.Data.DataRow dr;
        //private bool NewData;
        private System.Data.DataSet MyDataSet = new System.Data.DataSet();
        private System.Data.SqlClient.SqlDataAdapter MyCommand;
        private static string DBMode = ConfigurationManager.AppSettings["DBMode"];
        static string ConnString = (DBMode == "Local") ? ConfigurationManager.AppSettings["DBLocal"] : ConfigurationManager.AppSettings["DBServer"];
        System.Data.SqlClient.SqlConnection INConnection1 = new System.Data.SqlClient.SqlConnection(ConnString);
        private System.Data.SqlClient.SqlCommand INCommand;
        private System.Data.SqlClient.SqlDataReader SQLReader;
        private string INString;
        private string[] ReCust = { };


        protected void Page_Load(object sender, System.EventArgs e)
        {

            // If Session("account") = "" Or Session("Username") <> "mommyhappy" Then
            // Session("msg") = "您沒有使用此程式的權限!!"
            // Response.Redirect("Index.aspx")
            // End If
            if ((string)Session["Account"] == "" | (Session["Account"] == null))
            {
                Session["msg"] = "您沒有使用此程式的權限!!";
                Response.Redirect("Index.aspx");
            }

            if (!((string)Session["Groups"] == "109" | Convert.ToInt16(Session["Groups"]) <= 9))
            {
                Session["msg"] = "您沒有使用此程式的權限!!";
                Response.Redirect("Index.aspx");
            }

            if (!IsPostBack)
            {
                MyDataSet = new System.Data.DataSet();
                MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT * FROM Branch_Data where ID > 0 ", INConnection1);
                MyCommand.Fill(MyDataSet, "Branch");

                Branch.DataSource = MyDataSet.Tables["Branch"].DefaultView;
                Branch.DataTextField = "Branch_Name";
                Branch.DataValueField = "ID";
                Branch.DataBind();

                Check_Year.SelectedValue = DateTime.Now.Year.ToString();
                Check_Month.SelectedValue = DateTime.Now.Month.ToString();

                ReCust = new string[] { };

                //讀取假況資料表
                INString = "Select * from Public_JobResult";
                INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                INCommand.Connection.Open();
                SQLReader = INCommand.ExecuteReader();
                while (SQLReader.Read())
                {
                    ReCust = ReCust.Concat(new string[] { SQLReader[1].ToString().Trim() }).ToArray();
                    DropDownList3.Items.Add(new ListItem(SQLReader[1].ToString().Trim()));
                }
                INCommand.Connection.Close();

                for (int ti = 0; ti < DropDownList5.Items.Count; ti++)
                    DropDownList5.Items[ti].Attributes.Add("style", "background-color:" + DropDownList5.Items[ti].Value);


            }
        }

        protected void Check_Report_Click(object sender, EventArgs e)
        {
            int Start_Branch, End_Branch;
            int Ki;

            Session["dt"] = null;
            Session["CS"] = null;
            Session["ds"] = null;
            Session["Pa"] = 0;

            TextBox1.Text = "0";
            //TextBox2.Text = "0";
            //TextBox3.Text = "0";
            //TextBox4.Text = "0";
            //Label3.Text = "";
            //Label4.Text = "";

            Label2.Text = DateTime.Now.ToLongTimeString();
            if (Branch.SelectedIndex == 0)
            {
                Start_Branch = 1;
                End_Branch = 10;
            }
            else
            {
                Start_Branch = Branch.SelectedIndex;
                End_Branch = Branch.SelectedIndex;
            }

            string Select_Banch;
            if (Branch.SelectedItem.Text.Trim() == "南高雄")
                Select_Banch = " (Branch_Name ='南高雄' or Branch_Name = '屏東')";
            else
                Select_Banch = " Branch_Name ='" + Branch.SelectedItem.Text.Trim() + "'";


            MyDataSet = new System.Data.DataSet();

            //找出可以相互支援的駐點名單 以  ,駐點編號, 方式判斷
            string Support_Branch = "";
            MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT ID,Branch_Name FROM Branch_Data where Support_Branch like '%," +
                Branch.SelectedValue.ToString().Trim() + ",%'", INConnection1);
            MyCommand.Fill(MyDataSet, "Support_Branch");

            foreach (DataRowView Si in MyDataSet.Tables["Support_Branch"].DefaultView)
                Support_Branch = Support_Branch + "'" + Si[1].ToString().Trim() + "',";
            Support_Branch = "Branch_Name in (" + Support_Branch.Substring(0, Support_Branch.Length - 1) + ")";

            DropDownList4.DataSource = MyDataSet.Tables["Support_Branch"].DefaultView;
            DropDownList4.DataTextField = "Branch_Name";
            DropDownList4.DataValueField = "ID";
            DropDownList4.DataBind();
            DropDownList4.SelectedValue = Branch.SelectedValue;

            //設定可以選擇客戶的月份,以本月及上個月為主
            SelectMonth.Items.Clear();
            if (Check_Month.SelectedValue == "1")
                SelectMonth.Items.Add(new ListItem("12"));
            else
                SelectMonth.Items.Add(new ListItem((Convert.ToInt16(Check_Month.SelectedValue) - 1).ToString()));
            SelectMonth.Items.Add(new ListItem(Check_Month.SelectedValue));
            SelectMonth.Items[1].Selected = true;
            SelectMonth.BackColor = Color.LightGray;

            string EndDate = Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/01";
            for (Ki = 1; Ki <= 31; Ki++)
            {
                ShareFunction SF = new ShareFunction();
                if (!SF.IsDate(Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/" + Ki.ToString()))
                {
                    EndDate = Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/" + (Ki - 1).ToString();
                    break;
                }
            }


            // 找出尚未離職的管家
            MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT Staff_No,Staff_Name,Job_Title,Job_TS,Cell_Phone,Addr FROM Staff_Job_Data_View where (Depart_Date is null or " +
                " Depart_Date >= '" + EndDate + "') and " + Select_Banch +
                " and (not (Job_Title like '%專%' or Job_Title like '%備%' or Job_Title like '%理%' ))  Order by Staff_No", INConnection1);
            MyCommand.Fill(MyDataSet, "Staff_Nu");
            // 非客人請假的工作排程及沒有解
            MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                "SELECT ID,Job_Date,Cust_No,Job_Result,Cust_Name,Cust_Sp_Request,Staff_No,Job_Time,Note,Job_Status FROM Cust_Job_Schedule_View where  (Year(Job_Date) = " +
                Check_Year.SelectedValue +
                " and Month(Job_Date) = " + Check_Month.SelectedValue + ") and " + Support_Branch + " and Staff_No is not null and Staff_No <> ''" +
                " and ( Rtrim(Job_Result) = '' or Job_Result is null) and Job_Flag <> 255 Order by Staff_No,Job_Date", INConnection1);
            MyCommand.Fill(MyDataSet, "Job_Schedule");
            // 管家請假的工作排程
            MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                "SELECT ID,Job_Date,Cust_No,Job_Result,Cust_Name,Cust_Sp_Request,Staff_No,Job_Time,Note,Job_Status FROM Cust_Job_Schedule_View where  (Year(Job_Date) = " +
                Check_Year.SelectedValue +
                " and Month(Job_Date) = " + Check_Month.SelectedValue + ")  and Staff_No is not null and Staff_No <> '' and " +
                " Rtrim(Job_Result) <> '客' and ( Cust_No is null or Cust_No = '' or Cust_No like '%00-000%' ) Order by Staff_No,Job_Date", INConnection1);
            MyCommand.Fill(MyDataSet, "Day_Off_Schedule");
            // 未安排管家的工作排程
            MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                "SELECT ID,Job_Date,Cust_No,Job_Result,Cust_Name,Cust_Sp_Request,Staff_No,Job_Time,Note,Job_Status FROM Cust_Job_Schedule_View where  (Year(Job_Date) = " +
                Check_Year.SelectedValue +
                " and Month(Job_Date) = " + Check_Month.SelectedValue + ") and " + Select_Banch + " and (Staff_No is null or Staff_No = '') and " +
                " ( Rtrim(Job_Result) = '' or Job_Result is null) and Job_Flag <> 255 Order by Job_Date,Cust_No", INConnection1);
            MyCommand.Fill(MyDataSet, "No_Staff_Job_Schedule");
            // 客人請假的工作排程
            //MyCommand = new System.Data.SqlClient.SqlDataAdapter(
            //    "SELECT ID,Job_Date,Cust_No,Job_Result,Cust_Name,Cust_Sp_Request,Staff_No,Job_Time,Note FROM Cust_Job_Schedule_View where  (Year(Job_Date) = " +
            //    Check_Year.SelectedValue +
            //    " and Month(Job_Date) = " + Check_Month.SelectedValue + ") and " + Select_Banch + " and Staff_No is not null and Staff_No <> '' and " +
            //    " Rtrim(Job_Result) = '客'  Order by Staff_No,Job_Date", INConnection1);
            MyCommand = new System.Data.SqlClient.SqlDataAdapter(
                "SELECT ID,Job_Date,Cust_No,Job_Result,Cust_Name,Cust_Sp_Request,Staff_No,Job_Time,Note,Job_Status FROM Cust_Job_Schedule_View where  (Year(Job_Date) = " +
                Check_Year.SelectedValue +
                " and Month(Job_Date) = " + Check_Month.SelectedValue + ") and " + Select_Banch + " and " +
                " Rtrim(Job_Result) = '客'  Order by Staff_No,Job_Date", INConnection1);

            MyCommand.Fill(MyDataSet, "Cust_Off_Job_Schedule");
            // 
            int RCount = 1;
            //string HK_Name = ".";
            //string HKU = ".";
            //string Sheet_Name = ".";
            //string Sheet_Name_1 = ".";

            dt = new System.Data.DataTable();
            dt.Columns.Add(new System.Data.DataColumn("ID", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("HK_Name", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("Day_ID", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B3", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B4", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B5", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B6", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B7", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B8", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B9", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B10", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B11", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B12", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B13", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B14", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B15", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B16", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B17", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B18", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B19", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B20", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B21", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B22", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B23", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B24", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B25", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B26", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B27", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B28", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B29", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B30", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B31", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("RN", typeof(int)));
            dt.Columns.Add(new System.Data.DataColumn("HKN", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B1_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B2_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B3_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B4_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B5_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B6_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B7_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B8_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B9_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B10_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B11_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B12_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B13_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B14_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B15_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B16_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B17_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B18_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B19_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B20_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B21_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B22_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B23_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B24_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B25_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B26_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B27_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B28_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B29_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B30_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B31_1", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B1_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B2_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B3_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B4_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B5_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B6_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B7_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B8_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B9_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B10_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B11_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B12_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B13_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B14_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B15_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B16_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B17_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B18_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B19_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B20_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B21_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B22_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B23_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B24_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B25_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B26_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B27_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B28_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B29_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B30_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("B31_2", typeof(string)));
            dt.Columns.Add(new System.Data.DataColumn("HK_Data", typeof(string)));


            for (var ii = 0; ii <= MyDataSet.Tables["Staff_Nu"].DefaultView.Count - 1; ii++)   // 每個管家
            {
                for (Ki = 0; Ki <= 1; Ki++)   // 上午及下午班別
                {
                    dr = dt.NewRow();
                    dr[34] = ii * 2 + Ki;   // 流水號記錄號
                    dr[0] = MyDataSet.Tables["Staff_Nu"].DefaultView[ii][2].ToString().Substring(0, 2);  // Job Title
                    dr[1] = MyDataSet.Tables["Staff_Nu"].DefaultView[ii][1].ToString() + " " + MyDataSet.Tables["Staff_Nu"].DefaultView[ii][0].ToString();
                    // Staff No & Staff Name
                    dr[35] = MyDataSet.Tables["Staff_Nu"].DefaultView[ii][0].ToString();
                    dr[98] = MyDataSet.Tables["Staff_Nu"].DefaultView[ii][3].ToString() + "\n" + MyDataSet.Tables["Staff_Nu"].DefaultView[ii][4].ToString() +
                             "\n" + MyDataSet.Tables["Staff_Nu"].DefaultView[ii][5].ToString();
                    dr[2] = (Ki == 0) ? "上" : "下";


                    DataView dv = new DataView(MyDataSet.Tables["Job_Schedule"])
                    {
                        RowFilter = "Staff_No = '" + MyDataSet.Tables["Staff_Nu"].DefaultView[ii][0].ToString() + "' and Job_Time = " + Ki
                    };
                    for (RCount = 0; RCount <= dv.Count - 1; RCount++)
                    {
                        int tmpDay = Convert.ToDateTime(dv[RCount][1]).Day; // 找出資料的日期
                        dr[2 + tmpDay] = dv[RCount][2];   // 客戶編號
                        dr[35 + tmpDay] = dv[RCount][2] + "-" + (DBNull.Value.Equals(dv[RCount][4]) ? "" : dv[RCount][4].ToString().Trim()) + "\n";
                        // 客戶編號 & 客戶姓名 
                        dr[66 + tmpDay] = "," + dv[RCount][0] + "," + dv[RCount][9] + "," + "\n" +
                                         (DBNull.Value.Equals(dv[RCount][8]) ? "" : dv[RCount][8].ToString().Trim()) +
                                         "\n" + (DBNull.Value.Equals(dv[RCount][5]) ? "" : dv[RCount][5].ToString().Trim()) +
                                         (DBNull.Value.Equals(dv[RCount][3]) ? " " : dv[RCount][3].ToString().Trim());    // 資料庫序號、客戶需求 & 服務結果
                    }

                    // 管家請假排班記錄
                    dv = null;
                    dv = new DataView(MyDataSet.Tables["Day_Off_Schedule"])
                    {
                        RowFilter = "Staff_No = '" + MyDataSet.Tables["Staff_Nu"].DefaultView[ii][0].ToString() + "' and Job_Time = " + Ki
                    };
                    for (RCount = 0; RCount <= dv.Count - 1; RCount++)
                    {
                        int tmpDay = Convert.ToDateTime(dv[RCount][1]).Day; // 找出資料的日期
                        dr[2 + tmpDay] = DBNull.Value.Equals(dv[RCount][3]) ? "假" : dv[RCount][3].ToString().Trim();   // 假別
                        dr[35 + tmpDay] = "-"; // 客戶編號 & 客戶姓名 
                        dr[66 + tmpDay] = "," + dv[RCount][0] + "," + dv[RCount][9] + "," + (DBNull.Value.Equals(dv[RCount][3]) ? " " : dv[RCount][3].ToString().Trim());
                        // 資料庫序號、客戶需求 & 服務結果
                    }
                    dt.Rows.Add(dr);
                }
            }

            // 未排班工作
            int Sdr, Edr, Fdr;
            Sdr = dt.DefaultView.Count;
            Edr = Sdr;
            dr = dt.NewRow();
            dr[1] = "未排班";
            dt.Rows.Add(dr);
            // TextBox1.Text = MyDataSet.Tables("No_Staff_Job_Schedule").DefaultView.Count

            for (Ki = 1; Ki <= 31; Ki++)
            {
                Fdr = Sdr;   // 指標歸 0
                ShareFunction SF = new ShareFunction();
                if (SF.IsDate(Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/" + Ki.ToString()))
                {
                    DataView dv = new DataView(MyDataSet.Tables["No_Staff_Job_Schedule"])
                    {
                        RowFilter = " Job_Date = '" + Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/" + Ki.ToString() + "'"
                    };
                    //TextBox2.Text = TextBox2.Text + Ki.ToString() + "->" + dv.Count + ".";
                    for (RCount = 0; RCount <= dv.Count - 1; RCount++)
                    {
                        if (Fdr > Edr)
                        {
                            dr = dt.NewRow();
                            dr[1] = "未排班";
                            dt.Rows.Add(dr);
                            Edr = Edr + 1;
                        }
                        dt.Rows[Fdr][Ki + 2] = dv[RCount][2];  // 客戶編號 
                        dt.Rows[Fdr][Ki + 35] = dv[RCount][2] + "." + (DBNull.Value.Equals(dv[RCount][7]) ? "上" : (dv[RCount][7].ToString().Trim() == "0") ?
                            "上" : "下") + "." + (DBNull.Value.Equals(dv[RCount][4]) ? "" : dv[RCount][4].ToString().Trim()) + "\n";
                        // 客戶編號  & 服務時段 & 客戶姓名
                        dt.Rows[Fdr][Ki + 66] = "," + dv[RCount][0] + "," + dv[RCount][9] + "," + "\n" + (DBNull.Value.Equals(dv[RCount][8]) ? "" : dv[RCount][8].ToString().Trim()) +
                                                "\n" + (DBNull.Value.Equals(dv[RCount][5]) ? "" : dv[RCount][5].ToString().Trim()) +
                                                (DBNull.Value.Equals(dv[RCount][3]) ? " " : dv[RCount][3].ToString().Trim());    // 資料庫序號、客戶需求 & 服務結果
                        Fdr = Fdr + 1;
                    }
                }

            }

            // 客人請假的工作
            Sdr = dt.DefaultView.Count;
            Edr = Sdr;
            dr = dt.NewRow();
            dr[1] = "客請假";
            dt.Rows.Add(dr);
            // TextBox1.Text = MyDataSet.Tables("Cust_Off_Job_Schedule").DefaultView.Count

            for (Ki = 1; Ki <= 31; Ki++)
            {
                Fdr = Sdr;   // 指標歸 0
                ShareFunction SF = new ShareFunction();
                if (SF.IsDate(Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/" + Ki.ToString()))
                {
                    DataView dv = new DataView(MyDataSet.Tables["Cust_Off_Job_Schedule"])
                    {
                        RowFilter = " Job_Date = '" + Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/" + Ki.ToString() + "'"
                    };
                    //TextBox2.Text = TextBox2.Text + Ki.ToString() + "->" + dv.Count + ".";
                    for (RCount = 0; RCount <= dv.Count - 1; RCount++)
                    {
                        if (Fdr > Edr)
                        {
                            dr = dt.NewRow();
                            dr[1] = "客請假";
                            dt.Rows.Add(dr);
                            Edr = Edr + 1;
                        }
                        dt.Rows[Fdr][Ki + 2] = dv[RCount][2];  // 客戶編號 
                        dt.Rows[Fdr][Ki + 35] = dv[RCount][2] + "-" + (DBNull.Value.Equals(dv[RCount][4]) ? "" : dv[RCount][4].ToString().Trim()) + "\n";
                        // 客戶編號 & 客戶姓名
                        dt.Rows[Fdr][Ki + 66] = "," + dv[RCount][0] + "," + dv[RCount][9] + "," + "\n" + (DBNull.Value.Equals(dv[RCount][8]) ? "" : dv[RCount][8].ToString().Trim()) +
                                                "\n" + (DBNull.Value.Equals(dv[RCount][5]) ? "" : dv[RCount][5].ToString().Trim()) +
                                                (DBNull.Value.Equals(dv[RCount][3]) ? " " : dv[RCount][3].ToString().Trim());    // 資料庫序號、客戶需求 & 服務結果
                        Fdr = Fdr + 1;
                    }
                }

            }

            // GridView1.DataSource = dt.DefaultView
            // GridView1.DataBind()

            Session["dt"] = dt;
            Session["CS"] = null;
            Session["ds"] = null;
            Session["Pa"] = 0;
            Session["RC"] = null;

            DataGrid3.CurrentPageIndex = 0;
            Change_Screen(sender, e);

            Label2.Text = Label2.Text + "-" + DateTime.Now.ToLongTimeString();
        }

        public void Check_Sc(object sender, System.EventArgs e)
        {
            DataGridItem Dtemp;
            Button tmpButton;
            Label tmpLabel;
            string tmpHKN;
            string tmpBS;
            string[] tmpData = TextBox1.Text.Split(',');   // 客戶編號、時段、日期、管家編號及資料庫序號
            ReCust = new string[] { };

            //讀取假況資料表
            INString = "Select * from Public_JobResult";
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            while (SQLReader.Read())
            {
                ReCust = ReCust.Concat(new string[] { SQLReader[1].ToString().Trim() }).ToArray();
            }
            INCommand.Connection.Close();

            DropDownList1.Visible = false;
            DropDownList2.Visible = false;
            DropDownList3.Visible = false;
            DropDownList4.Visible = false;
            CheckBox4.Visible = false;
            CheckBox1.Visible = false;
            CheckBox2.Visible = false;
            Button5.Visible = false;
            Button6.Visible = false;
            HyperLink2.Visible = false;
            RadioButtonList1.ClearSelection();
            CheckBox1.Visible = false;
            JobNote.Visible = false;
            SearchText.Text = "";
            CheckBox5.Visible = false;
            DropDownList5.Visible = false;


            Dtemp = (DataGridItem)((Control)sender).Parent.Parent;
            tmpLabel = (Label)Dtemp.Cells[1].FindControl("HK_Name");

            if (tmpLabel.Text != "客請假")
            {
                //tmpHKN = (Dtemp.Cells[1].Text.Split(' ').Length > 1) ? Dtemp.Cells[1].Text.Split(' ')[1] : "";
                tmpHKN = (tmpLabel.Text.Split(' ').Length > 1) ? tmpLabel.Text.Split(' ')[1] : "";
                tmpButton = (Button)Dtemp.FindControl(((Control)sender).ID);
                tmpButton.BackColor = Color.LightBlue;
                if (tmpButton.Text == "")
                    tmpBS = "0";
                else
                    tmpBS = tmpButton.ToolTip.Split(',')[1];


                //公-督-訓都要出現設定畫面
                if (tmpButton.Text == "督" | tmpButton.Text == "公" | tmpButton.Text == "訓")
                {

                    string rw = "Supervision.aspx?Schedule_ID=" + tmpBS + "&Job_Date=" + Check_Year.SelectedValue.ToString() +
                                "/" + Check_Month.SelectedValue.ToString() + "/" + (System.Convert.ToInt32(((Control)sender).ID.Substring(1)) +
                                System.Convert.ToInt32(Session["CS"]) - 1).ToString() + "&Job_Result_ID=" +
                                (Array.IndexOf(ReCust, tmpButton.Text) + 1).ToString() + "&Staff_No=" + tmpHKN;

                    Response.Write("<script>window.open('" + rw + " ','_blank');</script>");
                }

                if (!(tmpLabel.Text == "未排班" & tmpButton.Text.Trim() == ""))
                {
                    // 客戶編號、時段、日期、管家編號及資料庫序號 資料存入
                    if (tmpLabel.Text == "未排班")
                    {
                        //RadioButtonList1.Items[0].Enabled = false;
                        TextBox1.Text = tmpButton.Text + "," + tmpButton.ToolTip.Split('.')[1] + "," + (System.Convert.ToInt32(((Control)sender).ID.Substring(1)) +
                            System.Convert.ToInt32(Session["CS"]) - 1).ToString() + "," + tmpHKN + "," + tmpBS;
                    }
                    else
                    {
                        //RadioButtonList1.Items[0].Enabled = true;
                        TextBox1.Text = tmpButton.Text + "," + Dtemp.Cells[2].Text + "," + (System.Convert.ToInt32(((Control)sender).ID.Substring(1)) +
                            System.Convert.ToInt32(Session["CS"]) - 1).ToString() + "," + tmpHKN + "," + tmpBS;
                    }
                    DropDownList1.SelectedValue = (System.Convert.ToInt32(((Control)sender).ID.Substring(1)) + System.Convert.ToInt32(Session["CS"]) - 1).ToString();
                    RadioButtonList1.ClearSelection();
                    RadioButtonList1.Visible = true;
                }
            }
        }

        public void Show_Sc_Type(object sender, DataGridItemEventArgs e)
        {
            ListItemType objItemType = (ListItemType)e.Item.ItemType;
            Button TempButton;
            string TmpString;
            string INString;
            System.Data.SqlClient.SqlDataReader SQLReader;
            int CS = 0;
            int iCount = 1;

            if (Session["CS"] == null)
                CS = 0;
            else
                CS = Convert.ToInt32(Session["CS"]);
            if (objItemType == ListItemType.Header)
            {
                for (iCount = 1; iCount <= 14; iCount++)
                    e.Item.Cells[iCount + 2].Text = (iCount + CS - 1).ToString();
            }

            if (objItemType == ListItemType.Item | objItemType == ListItemType.AlternatingItem)
            {
                if (Session["RC"] == null)
                    Session["RC"] = 0;
                else
                    Session["RC"] = (Convert.ToInt16(Session["RC"]) + 1) % 10;

                if (Convert.ToInt16(Session["RC"]) == 9)
                {
                    e.Item.Cells[0].BorderStyle = BorderStyle.Groove;
                    e.Item.Cells[1].BorderStyle = BorderStyle.Groove;
                    e.Item.Cells[2].BorderStyle = BorderStyle.Groove;
                    e.Item.Cells[2].BorderStyle = BorderStyle.Groove;
                    //e.Item.Cells[0].Font.Underline  = true;
                    //e.Item.Cells[1].Font.Underline = true;
                    //e.Item.Cells[2].Font.Underline = true;
                }
                for (iCount = 1; iCount <= 14; iCount++)
                {
                    TempButton = (Button)e.Item.FindControl("B" + iCount.ToString());
                    if (TempButton != null)
                    {
                        TempButton.ForeColor = Color.Black;
                        if (Convert.ToInt16(Session["RC"]) == 9)
                        {
                            TempButton.BorderStyle = BorderStyle.Groove;
                            //TempButton.Font.Underline = true;
                        }
                        //判斷是否有找尋資料
                        if (TempButton.ToolTip.Contains(SearchText.Text.Trim()) & SearchText.Text.Trim() != "")
                        {
                            TempButton.BackColor = Color.Red;
                            TempButton.ForeColor = Color.White;
                        }
                        else
                        {

                            if (TempButton.ToolTip.Split(',').Length > 1)
                                if (TempButton.ToolTip.Split(',')[2] != "0" & TempButton.ToolTip.Split(',')[2] != "")
                                    TempButton.BackColor = Color.FromName(DropDownList5.Items[Convert.ToInt16(TempButton.ToolTip.Split(',')[2])].Value);


                            // 判斷是否為管家請假
                            switch (TempButton.ToolTip.Substring(TempButton.ToolTip.Length - 1, 1))
                            {
                                case "督":
                                    {
                                        TempButton.BackColor = Color.AntiqueWhite;
                                        break;
                                    }
                                case "公":
                                    {
                                        TempButton.BackColor = Color.PaleGreen;
                                        break;
                                    }
                                case "訓":
                                    {
                                        TempButton.BackColor = Color.LightGreen;
                                        break;
                                    }
                                case "空":
                                    {
                                        TempButton.BackColor = Color.LemonChiffon;
                                        break;
                                    }
                                case "停":
                                    {
                                        TempButton.BackColor = Color.LightGoldenrodYellow;
                                        break;
                                    }
                                case "病":
                                case "事":
                                case "特":
                                    {
                                        TempButton.BackColor = Color.LightBlue;
                                        break;
                                    }
                            }

                            // 判斷是否為例假日或國定假日
                            TempButton.Width = 75;
                            // TmpString = "2018/01/" + Right(TempButton.ID, Len(TempButton.ID) - 1)
                            TmpString = Check_Year.SelectedItem.Text + "/" + (Check_Month.SelectedIndex + 1).ToString("00") + "/" + (iCount + CS - 1).ToString();
                            // TextBox1.Text = TextBox1.Text + TmpString
                            ShareFunction SF = new ShareFunction();
                            if (SF.IsDate(TmpString))
                            {
                                INString = "SELECT  H_Date ,H_Memo FROM Holiday_Date where H_Date = '" + TmpString + "'";
                                INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                                INCommand.Connection.Open();
                                SQLReader = INCommand.ExecuteReader();

                                if (SQLReader.Read())
                                {
                                    TempButton.BackColor = Color.LightPink;
                                    TempButton.Width = 75;
                                    e.Item.Cells[iCount + 2].BackColor = Color.LightBlue;
                                }
                                else if (System.Convert.ToDateTime(TmpString).DayOfWeek == DayOfWeek.Sunday |
                                    System.Convert.ToDateTime(TmpString).DayOfWeek == DayOfWeek.Saturday)
                                {
                                    TempButton.BackColor = Color.LightPink;
                                    TempButton.Width = 75;
                                    e.Item.Cells[iCount + 2].BackColor = Color.LightBlue;
                                }
                                INCommand.Connection.Close();
                            }


                        }
                    }
                }
            }
        }

        public void Show_Job_Detail(object sender, DataGridCommandEventArgs e)
        {
        }

        protected void DataGrid_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
        {
            if (Session["ds"] == null)
                Check_Report_Click(sender, e);

            System.Data.DataTable ds = (DataTable)Session["ds"];
            DataGrid3.DataSource = ds.DefaultView;
            DataGrid3.CurrentPageIndex = e.NewPageIndex;
            DataGrid3.DataBind();
            RadioButtonList1.Visible = false;
            DropDownList1.Visible = false;
            DropDownList2.Visible = false;
            DropDownList3.Visible = false;
            DropDownList4.Visible = false;
            Button5.Visible = false;
            Button6.Visible = false;
            HyperLink2.Visible = false;
            RadioButtonList1.ClearSelection();
            CheckBox1.Visible = false;
            CheckBox2.Visible = false;
            CheckBox4.Visible = false;
            JobNote.Visible = false;
            SearchText.Text = "";
            SelectMonth.Visible = false;
            CheckBox5.Visible = false;
            DropDownList5.Visible = false;



            Session["Pa"] = e.NewPageIndex;
        }

        protected void Change_Screen(object sender, EventArgs e)
        {
            int CS, Ci;
            System.Data.DataTable dt = (DataTable)Session["dt"];
            System.Data.DataTable ds;

            RadioButtonList1.Visible = false;
            DropDownList1.Visible = false;
            DropDownList2.Visible = false;
            DropDownList3.Visible = false;
            DropDownList4.Visible = false;
            Button5.Visible = false;
            Button6.Visible = false;
            HyperLink2.Visible = false;
            RadioButtonList1.ClearSelection();
            CheckBox1.Visible = false;
            JobNote.Visible = false;
            SelectMonth.Visible = false;
            CheckBox4.Visible = false;
            CheckBox5.Visible = false;
            Session["RC"] = null;
            DropDownList5.Visible = false;


            if (Session["dt"] == null)
                Check_Report_Click(sender, e);

            if (Session["CS"] == null)
            {
                if (Check_Year.SelectedValue == DateTime.Now.Year.ToString() & Check_Month.SelectedValue == DateTime.Now.Month.ToString())
                    CS = DateTime.Now.Day - 2;
                else
                    CS = 1;
            }
            else
                CS = Convert.ToInt32(Session["CS"]);

            if (((Control)sender).ID == "Left_Screen")
            {
                CS = CS - 7;
                SearchText.Text = "";
            }
            if (((Control)sender).ID == "Right_Screen")
            {
                CS = CS + 7;
                SearchText.Text = "";
            }

            int Mi = 31;
            // 找出該月的最後一天
            ShareFunction SF = new ShareFunction();
            while (!SF.IsDate(Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/" + Mi))
                Mi = Mi - 1;
            // 如果顯示 14 欄位 超出最後一天
            if (CS + 13 >= Mi)
                CS = Mi - 13;
            if (CS < 1)
                CS = 1;
            Session["CS"] = CS;

            ds = new System.Data.DataTable();
            ds.Columns.Add(new System.Data.DataColumn("ID", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("HK_Name", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("Day_ID", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B1", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B2", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B3", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B4", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B5", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B6", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B7", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B8", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B9", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B10", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B11", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B12", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B13", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B14", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("HKN", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B1_1", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B2_1", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B3_1", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B4_1", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B5_1", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B6_1", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B7_1", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B8_1", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B9_1", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B10_1", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B11_1", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B12_1", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B13_1", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B14_1", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B1_2", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B2_2", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B3_2", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B4_2", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B5_2", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B6_2", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B7_2", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B8_2", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B9_2", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B10_2", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B11_2", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B12_2", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B13_2", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("B14_2", typeof(string)));
            ds.Columns.Add(new System.Data.DataColumn("HK_Data", typeof(string)));


            // 拷貝14欄位到 ds
            for (Mi = 0; Mi <= dt.DefaultView.Count - 1; Mi++)
            {
                dr = ds.NewRow();
                dr[0] = dt.Rows[Mi][0];
                dr[1] = dt.Rows[Mi][1];
                dr[2] = dt.Rows[Mi][2];
                dr[17] = dt.Rows[Mi][35];
                dr[46] = dt.Rows[Mi][98];

                for (Ci = 0; Ci <= 13; Ci++)
                {
                    dr[Ci + 3] = dt.Rows[Mi][CS + Ci + 2];
                    dr[Ci + 18] = dt.Rows[Mi][CS + Ci + 35];
                    dr[Ci + 32] = dt.Rows[Mi][CS + Ci + 66];
                }
                ds.Rows.Add(dr);
            }

            DataGrid3.DataSource = ds.DefaultView;
            DataGrid3.DataBind();
            Label5.Text = CS.ToString();
            Session["ds"] = ds;
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            string[] tmpData = TextBox1.Text.Split(',');   // 客戶編號、時段、日期、管家編號及資料庫序號
            ReCust = new string[] { };
            Logs SLogs = new Logs();     //記錄更改內容

            //讀取假況資料表
            INString = "Select * from Public_JobResult";
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            while (SQLReader.Read())
            {
                ReCust = ReCust.Concat(new string[] { SQLReader[1].ToString().Trim() }).ToArray();
            }
            INCommand.Connection.Close();

            MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT *  FROM Holiday_Date", INConnection1);
            MyCommand.Fill(MyDataSet, "Holiday_Date");

            switch (RadioButtonList1.SelectedIndex)
            {
                case 0: // 客人請假
                    {
                        if (tmpData[4] != "0")
                        {
                            if (tmpData[0].Contains("00-000") == true | ReCust.Contains(tmpData[0].Trim()) == true)
                            {
                                //代表回駐點或原先是有請假的,則要先刪除此工作,再派入新工作
                                INString = "Delete Cust_Job_Schedule where ID = " + tmpData[4];
                                //如果是公-督-訓則要先把子資料表的資料刪除
                                if (tmpData[0].Trim() == "公" | tmpData[0].Trim() == "督" | tmpData[0].Trim() == "訓")
                                {
                                    INCommand = new System.Data.SqlClient.SqlCommand(
                                        "Delete from Cust_Job_Schedule_Supervision where Schedule_ID = " + tmpData[4], INConnection1);
                                    INCommand.Connection.Open();
                                    INCommand.ExecuteNonQuery();
                                    INCommand.Connection.Close();

                                }
                                //Logs
                                SLogs.ScheduleLogs("Schedule.log", Session["Username"].ToString() + "|[刪除]-[" + tmpData[3].Trim() +
                                                   "]的原[" + Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/" +
                                                   tmpData[2].ToString() + "]-[" + (tmpData[1] == "0" ? "上" : "下") + "]-[" + tmpData[0].Trim() + "]的排程,");
                            }
                            else
                            {
                                // 先將舊客戶資料填入請假,並清除管家資料   Job_Flag = 5 表示扣次不加次,其它皆有加次
                                //INString = "Update Cust_Job_Schedule set Job_Result = '客' ,Staff_No = ''  where ID =" + tmpData[4];
                                if (CheckBox1.Checked)
                                    INString = "Update Cust_Job_Schedule set Job_Result_ID = " + (Array.IndexOf(ReCust, "客") + 1).ToString() +
                                           ",Job_Flag = 5 ,Staff_No = ''  where ID =" + tmpData[4];
                                else
                                    INString = "Update Cust_Job_Schedule set Job_Result_ID = " + (Array.IndexOf(ReCust, "客") + 1).ToString() +
                                           " ,Staff_No = ''  where ID =" + tmpData[4]; ;

                                //Logs
                                SLogs.ScheduleLogs("Schedule.log", Session["Username"].ToString() + "|[變更]-[" + tmpData[3].Trim() + "]的原[" +
                                                   Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/" + tmpData[2].ToString() +
                                                   "]-[" + (tmpData[1] == "0" ? "上" : "下") + "]-[" + tmpData[0].Trim() + "]的排程,填入[客]請假" +
                                                   (CheckBox1.Checked == true ? "並扣次" : "不扣次"));


                                INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                                INCommand.Connection.Open();
                                INCommand.ExecuteNonQuery();
                                INCommand.Connection.Close();

                                // TextBox4.Text = INString
                                // 加入一筆服務日期到最後
                                if (CheckBox1.Checked == false)
                                    Add_Job_Schedule(tmpData);
                            }
                        }

                        // 更改管家服務客戶
                        if (DropDownList2.SelectedItem.Text != "先不排")
                        {
                            if (DropDownList2.SelectedItem.Text == "回駐點")        //如果是回駐點,則要新增一筆記錄
                            {
                                INString = "Select Cust_No,ID from Cust_Contract_Data where cust_No like '%00-000' and Branch_Name = '" +
                                    Branch.SelectedItem.Text.Trim() + "'";
                                INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                                INCommand.Connection.Open();
                                SQLReader = INCommand.ExecuteReader();
                                if (SQLReader.Read())
                                {
                                    //INString = "Insert into Cust_Job_Schedule (Job_Date,Job_Time,Staff_No,Cust_No,Job_Result,Job_Price,Job_Flag,Job_Status," +
                                    //           "HK_Serial," + "Contract_ID) Values ('" + Check_Year.SelectedValue.ToString() + "/" + 
                                    //           Check_Month.SelectedValue.ToString() + "/" +
                                    //            tmpData[2].ToString() + "'," + (tmpData[1] == "上" ? 0 : 1).ToString() + ",'" + tmpData[3].ToString() + "','" +
                                    //            SQLReader[0].ToString().Trim() + "','',0,1,null,1," + SQLReader[1].ToString().Trim() + ")";
                                    INString = "Insert into Cust_Job_Schedule (Job_Date,Job_Time,Staff_No,Cust_No,Job_Result_ID,Job_Price,Job_Flag,Job_Status," +
                                               "HK_Serial," + "Contract_ID) Values ('" + Check_Year.SelectedValue.ToString() + "/" +
                                               Check_Month.SelectedValue.ToString() + "/" +
                                               tmpData[2].ToString() + "'," + (tmpData[1] == "上" ? 0 : 1).ToString() + ",'" + tmpData[3].ToString() + "','" +
                                               SQLReader[0].ToString().Trim() + "',0,0,1,null,1," +
                                               SQLReader[1].ToString().Trim() + ")";
                                    INCommand.Connection.Close();
                                    //Logs
                                    SLogs.ScheduleLogs("Schedule.log", Session["Username"].ToString() + "|[變更]-[" + tmpData[3].Trim() + "]的原[" +
                                                       Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/" + tmpData[2].ToString() +
                                                       "]-[" + (tmpData[1] == "0" ? "上" : "下") + "]-[" + tmpData[0].Trim() + "]的排程,改為[回駐點]");
                                }
                                else
                                {
                                    INCommand.Connection.Close();
                                    Response.Write("<script language=JavaScript>alert('找不到回駐地的記錄,請連絡工程師!!');</script>");
                                    break;
                                }
                            }
                            else
                            {
                                INString = "Update Cust_Job_Schedule set  Staff_No = '" + tmpData[3].ToString() + "',Job_Date = '" +
                                           Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/" +
                                           tmpData[2].ToString() + "', Job_Time = " + (tmpData[1] == "上" ? 0 : 1).ToString() + " where ID = " +
                                           DropDownList2.SelectedValue.ToString();
                                //Logs
                                SLogs.ScheduleLogs("Schedule.log", Session["Username"].ToString() + "|[變更]-[" + tmpData[3].Trim() + "]的原[" + Check_Year.SelectedValue.ToString() +
                                                   "/" + Check_Month.SelectedValue.ToString() + "/" + tmpData[2].ToString() + "]-[" + (tmpData[1] == "0" ? "上" : "下") +
                                                   "]-[" + tmpData[0].Trim() + "]的排程,改為[" + Check_Year.SelectedValue.ToString() + "/" +
                                                   Check_Month.SelectedValue.ToString() + "/" + tmpData[2].ToString() + "]-[" + DropDownList2.SelectedItem.Text +
                                                   "]排程");
                            }

                            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                            INCommand.Connection.Open();
                            INCommand.ExecuteNonQuery();
                            INCommand.Connection.Close();
                        }

                        break;
                    }

                case 1: // 管家請假

                    {
                        if (CheckBox2.Checked == true)
                        {
                            if (tmpData[4] != "0" & tmpData[0].Contains("00-000") == false & ReCust.Contains(tmpData[0].Trim()) == false)
                            //順延不能用在回公司及有假況上
                            {
                                if (DropDownList3.SelectedValue.Trim() != "先不排")
                                {
                                    // 先將舊客戶資料填入管家請假 , Job_Flag = 5 表示扣次不加次,其它皆有加次
                                    //INString = "Update Cust_Job_Schedule set Job_Result = '" + DropDownList3.SelectedValue + " '  where ID =" + tmpData[4];
                                    INString = "Update Cust_Job_Schedule set Job_Result_ID = " + (Array.IndexOf(ReCust, DropDownList3.SelectedValue) + 1).ToString() +
                                           " where ID =" + tmpData[4];
                                    INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                                    INCommand.Connection.Open();
                                    INCommand.ExecuteNonQuery();
                                    INCommand.Connection.Close();
                                    //Logs
                                    SLogs.ScheduleLogs("Schedule.log", Session["Username"].ToString() + "|[變更]-[" + tmpData[3].Trim() + "]的原[" + Check_Year.SelectedValue.ToString() +
                                                       "/" + Check_Month.SelectedValue.ToString() + "/" + tmpData[2].ToString() + "]-[" + (tmpData[1] == "0" ? "上" : "下") +
                                                       "]-[" + tmpData[0].Trim() + "]的排程,改為管家請[" + DropDownList3.SelectedValue + "]假,並延次(加次)");

                                    //TextBox4.Text = TextBox4.Text + INString;
                                    // 增加次數
                                    Add_Job_Schedule(tmpData);
                                }
                            }
                        }
                        else
                        {
                            // 更改管家服務客戶
                            if (tmpData[4] != "0")
                            {
                                if (tmpData[0].Contains("00-000") == true | ReCust.Contains(tmpData[0].Trim()) == true)
                                //代表回駐點或假況,則要先刪除此工作,再派入新工作
                                {
                                    INString = "Delete Cust_Job_Schedule where ID = " + tmpData[4];
                                    INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                                    INCommand.Connection.Open();
                                    INCommand.ExecuteNonQuery();
                                    INCommand.Connection.Close();
                                    //Logs
                                    SLogs.ScheduleLogs("Schedule.log", Session["Username"].ToString() + "|[刪除]-[" + tmpData[3].Trim() + "]的原[" + Check_Year.SelectedValue.ToString() +
                                                       "/" + Check_Month.SelectedValue.ToString() + "/" + tmpData[2].ToString() + "]-[" +
                                                       (tmpData[1] == "0" ? "上" : "下") + "]-[" + tmpData[0].Trim() + "]的排程,");
                                    //如果是公-督-訓則要先把子資料表的資料刪除
                                    if (tmpData[0].Trim() == "公" | tmpData[0].Trim() == "督" | tmpData[0].Trim() == "訓")
                                    {
                                        INCommand = new System.Data.SqlClient.SqlCommand(
                                            "Delete from Cust_Job_Schedule_Supervision where Schedule_ID = " + tmpData[4], INConnection1);
                                        INCommand.Connection.Open();
                                        INCommand.ExecuteNonQuery();
                                        INCommand.Connection.Close();

                                    }
                                    //Logs
                                    SLogs.ScheduleLogs("Schedule.log", Session["Username"].ToString() + "|[刪除]-[" + tmpData[3].Trim() +
                                                       "]的原[" + Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/" +
                                                       tmpData[2].ToString() + "]-[" + (tmpData[1] == "0" ? "上" : "下") + "]-[" + tmpData[0].Trim() + "]的排程,");
                                }

                                else if (DropDownList2.SelectedItem.Text != "先不排")
                                {

                                    //INString = "Update Cust_Job_Schedule set  Staff_No = '" + DropDownList2.SelectedValue.ToString() + "',Job_Date = '" +
                                    //    Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/" +
                                    //    DropDownList1.SelectedValue.ToString() +
                                    //    "' where ID = " + tmpData[4];

                                    //新增可以選擇上午及下午的管家
                                    INString = "Update Cust_Job_Schedule set  Staff_No = '" + DropDownList2.SelectedValue.ToString() + "',Job_Date = '" +
                                               Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/" +
                                               DropDownList1.SelectedValue.ToString() + "' , Job_Time = " + (CheckBox4.Checked ? 0 : 1) +
                                               " where ID = " + tmpData[4];

                                    INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                                    INCommand.Connection.Open();
                                    INCommand.ExecuteNonQuery();
                                    INCommand.Connection.Close();
                                    //Logs
                                    SLogs.ScheduleLogs("Schedule.log", Session["Username"].ToString() + "|[變更]-[" + tmpData[3].Trim() + "]的原[" + Check_Year.SelectedValue.ToString() +
                                                       "/" + Check_Month.SelectedValue.ToString() + "/" + tmpData[2].ToString() +
                                                       "]-[" + (tmpData[1] == "0" ? "上" : "下") + "]-[" + tmpData[0].Trim() + "]的排程,改為[" +
                                                       Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() + "/" +
                                                       DropDownList1.SelectedValue.ToString() + "]-[" + (CheckBox4.Checked ? "上" : "下") + "]-[" +
                                                       DropDownList2.SelectedItem.Text + "]排程");
                                    //TextBox4.Text = TextBox4.Text + INString;
                                }
                                else
                                {
                                    // 將舊客戶資料中,清除服務管家資料
                                    INString = "Update Cust_Job_Schedule set Staff_No = ''  where ID =" + tmpData[4];
                                    INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                                    INCommand.Connection.Open();
                                    INCommand.ExecuteNonQuery();
                                    INCommand.Connection.Close();
                                    //TextBox4.Text = TextBox4.Text + INString;
                                    //Logs
                                    SLogs.ScheduleLogs("Schedule.log", Session["Username"].ToString() + "|[清除]-[" + tmpData[3].Trim() + "]的原[" + Check_Year.SelectedValue.ToString() +
                                                       "/" + Check_Month.SelectedValue.ToString() + "/" + tmpData[2].ToString() + "]-[" +
                                                      (tmpData[1] == "0" ? "上" : "下") + "]-[" + tmpData[0].Trim() + "]的管家,為[先不排管家]");
                                }
                            }
                            // 將舊管家資料填入管家請假
                            if (DropDownList3.SelectedIndex != 0)    //確定有選擇請假原因
                            {
                                //INString = "Insert into  Cust_Job_Schedule (Job_Date,Job_Time,Staff_No,Cust_No,Job_Result,Job_Price,Job_Flag,Job_Status," +
                                //           "HK_Serial,Contract_ID) values('" + Check_Year.SelectedValue.ToString() + "/" +
                                //           Check_Month.SelectedValue.ToString() + "/" + tmpData[2].ToString() + "'," + (tmpData[1] == "上" ? 0 : 1).ToString() +
                                //           ",'" + tmpData[3].ToString() + "','','" +  DropDownList3.SelectedValue.ToString() + "',0,0,null,null,0)";
                                INString = "Insert into  Cust_Job_Schedule (Job_Date,Job_Time,Staff_No,Cust_No,Job_Result_ID,Job_Price,Job_Flag,Job_Status," +
                                           "HK_Serial,Contract_ID) values('" + Check_Year.SelectedValue.ToString() + "/" +
                                           Check_Month.SelectedValue.ToString() + "/" + tmpData[2].ToString() + "'," + (tmpData[1] == "上" ? 0 : 1).ToString() +
                                           ",'" + tmpData[3].ToString() + "',''," + (Array.IndexOf(ReCust, DropDownList3.SelectedValue) + 1).ToString() +
                                           ",0,1,null,null,0)";
                                INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                                INCommand.Connection.Open();
                                INCommand.ExecuteNonQuery();
                                INCommand.Connection.Close();
                                //Logs
                                SLogs.ScheduleLogs("Schedule.log", Session["Username"].ToString() + "|[變更]-[" + tmpData[3].Trim() + "]的原[" + Check_Year.SelectedValue.ToString() +
                                                   "/" + Check_Month.SelectedValue.ToString() + "/" + tmpData[2].ToString() + "]-[" +
                                                  (tmpData[1] == "0" ? "上" : "下") + "]-[" + tmpData[0].Trim() + "]的排程,改為管家請[" +
                                                  DropDownList3.SelectedValue + "]假");

                                //公-督-訓都要出現設定畫面
                                if (DropDownList3.SelectedItem.Text == "督" | DropDownList3.SelectedItem.Text == "公" | DropDownList3.SelectedItem.Text == "訓")
                                {
                                    // 找出新增的記錄
                                    INString = "Select ID from  Cust_Job_Schedule where Job_Date='" + Check_Year.SelectedValue.ToString() + "/" +
                                           Check_Month.SelectedValue.ToString() + "/" + tmpData[2].ToString() + "' and Job_Time = " +
                                           (tmpData[1] == "上" ? 0 : 1).ToString() + " and Staff_No = '" + tmpData[3].ToString() + "' and Job_Result_ID = " +
                                           (Array.IndexOf(ReCust, DropDownList3.SelectedValue) + 1).ToString() + " and Contract_ID = 0";

                                    INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                                    INCommand.Connection.Open();
                                    SQLReader = INCommand.ExecuteReader();
                                    if (SQLReader.Read())
                                    {
                                        string rw = "Supervision.aspx?Schedule_ID=" + SQLReader[0].ToString() + "&Job_Date=" + Check_Year.SelectedValue.ToString() +
                                                    "/" + Check_Month.SelectedValue.ToString() + "/" + DropDownList1.SelectedValue.ToString() + "&Job_Result_ID=" +
                                                    (Array.IndexOf(ReCust, DropDownList3.SelectedValue) + 1).ToString() + "&Staff_No=" + tmpData[3].ToString();

                                        INCommand.Connection.Close();
                                        Response.Write("<script>window.open('" + rw + " ','_blank');</script>");
                                        //System.Diagnostics.Process.Start("Chrome", rw);
                                    }
                                    INCommand.Connection.Close();

                                }
                            }

                            //TextBox4.Text = TextBox4.Text + INString;
                        }

                        break;
                    }

                case 2: // 換管家或客人

                    {
                        if (tmpData[4] != "0" & tmpData[0].Contains("00-000") == false & ReCust.Contains(tmpData[0].Trim()) == false)
                        //不能是回公司或有假況狀態處理此功能
                        {
                            // 計算客戶日期的清單
                            var JDate = new List<DateTime>();
                            var JID = new List<int>();
                            // 改寫找出同一客戶同一管家序號的日期清單
                            int tmpHKSerial = 0;
                            int tmpContractID = 0;
                            INString = "Select HK_Serial,Contract_ID from Cust_Job_Schedule where ID = " + tmpData[4].ToString();

                            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                            INCommand.Connection.Open();
                            SQLReader = INCommand.ExecuteReader();
                            if (SQLReader.Read() == true)
                            {
                                tmpHKSerial = Convert.ToInt16(SQLReader[0].ToString());
                                tmpContractID = Convert.ToInt32(SQLReader[1].ToString());
                            }
                            else
                            {
                                INCommand.Connection.Close();
                                return;
                            }
                            INCommand.Connection.Close();

                            //新增跳週整批更改,設定日期條件
                            string DW = "";
                            if (CheckBox5.Checked == true)
                                DW = " and (DateDiff(d,'" + Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() +
                                       "/" + tmpData[2].ToString() + "',Job_Date) % 14 = 0) ";

                            INString = "Select Job_Date,ID from Cust_Job_Schedule where HK_Serial = " + tmpHKSerial + " and Job_Time = " +
                                       (tmpData[1] == "上" ? 0 : 1).ToString() +
                                       " and  Contract_ID = " + tmpContractID + " and Job_Date >= '" + Check_Year.SelectedValue.ToString() + "/" +
                                       Check_Month.SelectedValue.ToString() +
                                       "/" + tmpData[2].ToString() + "'" + DW + " order by Job_Date";

                            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                            INCommand.Connection.Open();
                            SQLReader = INCommand.ExecuteReader();
                            while (SQLReader.Read() == true)
                            {
                                JDate.Add((DateTime)SQLReader[0]);   // 客戶工作日期清單
                                JID.Add(Convert.ToInt32(SQLReader[1]));     // 客戶工作日ID
                            }
                            INCommand.Connection.Close();

                            if (DropDownList2.SelectedItem.Text == "先不排")
                            {
                                // 將舊客戶資料中,清除服務管家資料
                                string JIString = "(";
                                foreach (var item in JID)
                                    JIString = JIString + item.ToString() + ",";
                                JIString = JIString.Substring(0, JIString.Length - 1) + ")";

                                if (JIString.Length > 2)
                                {
                                    INString = "Update Cust_Job_Schedule set Staff_No = ''  where ID  in " + JIString;
                                    INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                                    INCommand.Connection.Open();
                                    INCommand.ExecuteNonQuery();
                                    INCommand.Connection.Close();
                                    //Logs
                                    SLogs.ScheduleLogs("Schedule.log", Session["Username"].ToString() + "|[變更]-[" + tmpData[3].Trim() + "]的原[" + Check_Year.SelectedValue.ToString() +
                                                       "/" + Check_Month.SelectedValue.ToString() + "/" + tmpData[2].ToString() + "]-[" +
                                                       (tmpData[1] == "0" ? "上" : "下") + "]-[" + tmpData[0].Trim() + "]的排程,全部都不排管家,變成[橘子]");
                                }
                                //TextBox4.Text = TextBox4.Text + INString;
                            }
                            else
                            {
                                // 搜尋新管家工作日是否有空
                                string JDString = "(";
                                foreach (var item in JDate)
                                    JDString = JDString + "'" + item.AddDays(System.Convert.ToInt32(DropDownList1.SelectedValue) -
                                        System.Convert.ToInt32(tmpData[2])).ToShortDateString() + "',";
                                JDString = JDString.Substring(0, JDString.Length - 1) + ")";
                                if (JDString.Length <= 2)
                                    JDString = "";
                                else
                                    JDString = " and  Job_Date  in " + JDString;
                                // TextBox4.Text = JDString

                                var tmpID = new List<int>();

                                //INString = "Select ID from Cust_Job_Schedule where Staff_No = '" + DropDownList2.SelectedValue.ToString() + "' and Job_Time = " +
                                //     (tmpData[1] == "上" ? 0 : 1).ToString() + JDString + " Order by ID ";
                                //改新寫法,配合可以選擇上/下的管家(CheckBox4.Checked ? 0 : 1)
                                INString = "Select ID from Cust_Job_Schedule where Staff_No = '" + DropDownList2.SelectedValue.ToString() + "' and Job_Time = " +
                                     (CheckBox4.Checked ? 0 : 1).ToString() + JDString + " Order by ID ";
                                INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                                INCommand.Connection.Open();
                                SQLReader = INCommand.ExecuteReader();
                                while (SQLReader.Read())
                                    tmpID.Add(Convert.ToInt32(SQLReader[0]));
                                INCommand.Connection.Close();

                                //舊管家原先服務的次數ID列
                                string JIString = "(";
                                foreach (var item in tmpID)
                                    JIString = JIString + item.ToString() + ",";
                                JIString = JIString.Substring(0, JIString.Length - 1) + ")";

                                if (tmpID.Count > 0 & CheckBox3.Checked == false)
                                {
                                    Response.Write("<script language=JavaScript>alert('此管家己有安排工作,若要覆蓋,請勾選[若有重覆記錄則覆蓋]!!');</script>");
                                    // TextBox4.Text = "此管家己有安排工作,若要覆蓋,請勾選【若有重覆記錄則覆蓋】!!"
                                    break;
                                }

                                if (tmpID.Count > 0)
                                {
                                    // 準備覆蓋新管家,即有的服務資料
                                    INString = "Update Cust_Job_Schedule set  Staff_No = ''  where ID in " + JIString;
                                    INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                                    INCommand.Connection.Open();
                                    INCommand.ExecuteNonQuery();
                                    INCommand.Connection.Close();
                                    //TextBox4.Text = TextBox4.Text + INString;
                                }
                                // 將客戶的服務日期,填入新的管家編號
                                JIString = "(";
                                foreach (var item in JID)     // 客戶服務日期所在ID
                                    JIString = JIString + item.ToString() + ",";
                                JIString = JIString.Substring(0, JIString.Length - 1) + ")";
                                if (JIString.Length > 2)
                                {
                                    //INString = "Update Cust_Job_Schedule set  Staff_No = '" + DropDownList2.SelectedValue.ToString() + "'," +
                                    //           " Job_Date = DateAdd(DAY," + (System.Convert.ToInt32(DropDownList1.SelectedValue) -
                                    //           System.Convert.ToInt32(tmpData[2])).ToString() + ",Job_Date) where ID in " + JIString;

                                    //新修改配合可以選上下午的管家功能Job_Time = " + (CheckBox4.Checked ? 0 : 1).ToString()
                                    INString = "Update Cust_Job_Schedule set  Staff_No = '" + DropDownList2.SelectedValue.ToString() + "'" +
                                               ",Job_Time = " + (CheckBox4.Checked ? 0 : 1).ToString() +
                                               ",Job_Date = DateAdd(DAY," + (System.Convert.ToInt32(DropDownList1.SelectedValue) -
                                               System.Convert.ToInt32(tmpData[2])).ToString() + ",Job_Date) where ID in " + JIString;
                                    INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                                    INCommand.Connection.Open();
                                    INCommand.ExecuteNonQuery();
                                    INCommand.Connection.Close();
                                    //Logs
                                    SLogs.ScheduleLogs("Schedule.log", Session["Username"].ToString() + "|[變更]-[" + tmpData[3].Trim() + "]的原[" + Check_Year.SelectedValue.ToString() +
                                                       "/" + Check_Month.SelectedValue.ToString() + "/" + tmpData[2].ToString() + "]-[" +
                                                      (tmpData[1] == "0" ? "上" : "下") + "]-[" + tmpData[0].Trim() + "]的後續排程,全部變成移動[" +
                                                      (System.Convert.ToInt32(DropDownList1.SelectedValue) - System.Convert.ToInt32(tmpData[2])).ToString() +
                                                      "]天，由[" + DropDownList2.SelectedValue + "]-[" + (CheckBox4.Checked ? "上" : "下") + "]來服務");
                                }
                                //TextBox4.Text = TextBox4.Text + INString;
                            }
                        }
                        else
                        {
                        }

                        break;
                    }
                case 3:
                    {
                        if (tmpData[4] != "0")     //如果有工作
                        {
                            if (JobNote.Text != "")    //如果輸入備註
                            {
                                ShareFunction SF = new ShareFunction();
                                INString = "Update Cust_Job_Schedule set Note = '" + SF.NSubString(JobNote.Text, 0, 50) + "'  where ID =" + tmpData[4];
                                INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                                INCommand.Connection.Open();
                                INCommand.ExecuteNonQuery();
                                INCommand.Connection.Close();
                                //Logs
                                SLogs.ScheduleLogs("Schedule.log", Session["Username"].ToString() + "|[變更]-[" + tmpData[3].Trim() + "]的原[" + Check_Year.SelectedValue.ToString() +
                                                   "/" + Check_Month.SelectedValue.ToString() + "/" + tmpData[2].ToString() + "]-[" + (tmpData[1] == "0" ? "上" : "下") +
                                                   "]-[" + tmpData[0].Trim() + "]的備註");
                            }
                        }
                        break;
                    }
                case 4:    //跨區整批換客戶
                    {
                        if (tmpData[0].Contains("00-000") == false & ReCust.Contains(tmpData[0].Trim()) == false)
                        //不能是回公司或有假況狀態處理此功能
                        {
                            // 計算客戶日期的清單
                            var JDate = new List<DateTime>();
                            var JID = new List<int>();
                            // 改寫找出同一客戶同一管家序號的日期清單
                            int tmpHKSerial = 0;
                            int tmpContractID = 0;
                            int tmpJobTime = 0;
                            INString = "Select HK_Serial,Contract_ID,Job_Time from Cust_Job_Schedule where ID = " + DropDownList2.SelectedValue.ToString();

                            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                            INCommand.Connection.Open();
                            SQLReader = INCommand.ExecuteReader();
                            if (SQLReader.Read() == true)
                            {
                                tmpHKSerial = Convert.ToInt16(SQLReader[0].ToString());
                                tmpContractID = Convert.ToInt32(SQLReader[1].ToString());
                                tmpJobTime = Convert.ToInt16(SQLReader[2].ToString());
                            }
                            else
                            {
                                INCommand.Connection.Close();
                                return;
                            }
                            INCommand.Connection.Close();

                            //新增跳週整批更改,設定日期條件
                            string DW = "";
                            if (CheckBox5.Checked == true)
                                DW = " and (DateDiff(d,'" + Check_Year.SelectedValue.ToString() + "/" + Check_Month.SelectedValue.ToString() +
                                       "/" + tmpData[2].ToString() + "',Job_Date) % 14 = 0) ";

                            INString = "Select Job_Date,ID from Cust_Job_Schedule where HK_Serial = " + tmpHKSerial + " and Job_Time = " +
                                       tmpJobTime.ToString() +
                                       " and  Contract_ID = " + tmpContractID + " and Job_Date >= '" + Check_Year.SelectedValue.ToString() +
                                       "/" + Check_Month.SelectedValue.ToString() + "/" + DropDownList1.SelectedValue.ToString() + "'" + DW + " order by Job_Date";

                            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                            INCommand.Connection.Open();
                            SQLReader = INCommand.ExecuteReader();
                            while (SQLReader.Read() == true)
                            {
                                JDate.Add((DateTime)SQLReader[0]);   // 客戶工作日期清單
                                JID.Add(Convert.ToInt32(SQLReader[1]));     // 客戶工作日ID
                            }
                            INCommand.Connection.Close();

                            if (DropDownList2.SelectedItem.Text != "先不排" & DropDownList2.SelectedItem.Text != "回駐點")
                            {
                                // 搜尋新管家工作日是否有空
                                string JDString = "(";
                                foreach (var item in JDate)
                                    JDString = JDString + "'" + item.AddDays(System.Convert.ToInt32(tmpData[2]) -
                                               System.Convert.ToInt32(DropDownList1.SelectedValue)).ToShortDateString() + "',";
                                JDString = JDString.Substring(0, JDString.Length - 1) + ")";
                                if (JDString.Length <= 2)
                                    JDString = "";
                                else
                                    JDString = " and  Job_Date  in " + JDString;
                                // TextBox4.Text = JDString

                                var tmpID = new List<int>();

                                INString = "Select ID from Cust_Job_Schedule where Staff_No = '" + tmpData[3].ToString() + "' and Job_Time = " +
                                     (tmpData[1] == "上" ? 0 : 1).ToString() + JDString + " Order by ID ";
                                INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                                INCommand.Connection.Open();
                                SQLReader = INCommand.ExecuteReader();
                                while (SQLReader.Read())
                                    tmpID.Add(Convert.ToInt32(SQLReader[0]));
                                INCommand.Connection.Close();

                                //舊管家原先服務的次數ID列
                                string JIString = "(";
                                foreach (var item in tmpID)
                                    JIString = JIString + item.ToString() + ",";
                                JIString = JIString.Substring(0, JIString.Length - 1) + ")";

                                if (tmpID.Count > 0 & CheckBox3.Checked == false)
                                {
                                    Response.Write("<script language=JavaScript>alert('此管家己有安排工作,若要覆蓋,請勾選[若有重覆記錄則覆蓋]!!');</script>");
                                    // TextBox4.Text = "此管家己有安排工作,若要覆蓋,請勾選【若有重覆記錄則覆蓋】!!"
                                    break;
                                }

                                if (tmpID.Count > 0)
                                {
                                    // 準備覆蓋新管家,即有的服務資料
                                    INString = "Update Cust_Job_Schedule set  Staff_No = ''  where ID in " + JIString;
                                    INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                                    INCommand.Connection.Open();
                                    INCommand.ExecuteNonQuery();
                                    INCommand.Connection.Close();
                                    //TextBox4.Text = TextBox4.Text + INString;
                                }
                                // 將客戶的服務日期,填入新的管家編號
                                JIString = "(";
                                foreach (var item in JID)     // 客戶服務日期所在ID
                                    JIString = JIString + item.ToString() + ",";
                                JIString = JIString.Substring(0, JIString.Length - 1) + ")";
                                if (JIString.Length > 2)
                                {
                                    INString = "Update Cust_Job_Schedule set  Staff_No = '" + tmpData[3].ToString() + "'" +
                                               ",Job_Time = " + (tmpData[1] == "上" ? 0 : 1).ToString() +
                                               ",Job_Date = DateAdd(DAY," + (System.Convert.ToInt32(tmpData[2]) -
                                               System.Convert.ToInt32(DropDownList1.SelectedValue)).ToString() + ",Job_Date) where ID in " + JIString;
                                    INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                                    INCommand.Connection.Open();
                                    INCommand.ExecuteNonQuery();
                                    INCommand.Connection.Close();
                                    //Logs
                                    SLogs.ScheduleLogs("Schedule.log", Session["Username"].ToString() + "|[變更]-[" + DropDownList2.SelectedValue + "]的原[" + Check_Year.SelectedValue.ToString() +
                                                       "/" + Check_Month.SelectedValue.ToString() + "/" + DropDownList1.SelectedValue + "]-[" +
                                                      (tmpJobTime == 0 ? "上" : "下") + "]-[" + tmpData[0].Trim() + "]的後續排程,全部變成移動[" +
                                                      (System.Convert.ToInt32(DropDownList1.SelectedValue) - System.Convert.ToInt32(tmpData[2])).ToString() +
                                                      "]天，由[" + tmpData[3].Trim() + "]-[" + (tmpData[1] == "0" ? "上" : "下") + "]來服務");
                                }
                                //TextBox4.Text = TextBox4.Text + INString;
                            }
                        }
                        else
                        {
                        }

                        break;
                    }
                case 5:   //變更顏色
                    {
                        if (tmpData[4] != "0")     //如果有工作
                        {

                            ShareFunction SF = new ShareFunction();
                            INString = "Update Cust_Job_Schedule set Job_Status = " + DropDownList5.SelectedIndex + "  where ID =" + tmpData[4];
                            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                            INCommand.Connection.Open();
                            INCommand.ExecuteNonQuery();
                            INCommand.Connection.Close();
                            //Logs
                            SLogs.ScheduleLogs("Schedule.log", Session["Username"].ToString() + "|[變更]-[" + tmpData[3].Trim() + "]的原[" + Check_Year.SelectedValue.ToString() +
                                               "/" + Check_Month.SelectedValue.ToString() + "/" + tmpData[2].ToString() + "]-[" + (tmpData[1] == "0" ? "上" : "下") +
                                               "]-[" + tmpData[0].Trim() + "]的顏色為" + DropDownList5.SelectedValue);

                        }
                        break;
                    }
            }

            RadioButtonList1.Visible = false;
            DropDownList1.Visible = false;
            DropDownList2.Visible = false;
            DropDownList3.Visible = false;
            DropDownList4.Visible = false;
            DropDownList5.Visible = false;
            Button5.Visible = false;
            Button6.Visible = false;
            HyperLink2.Visible = false;
            CheckBox1.Checked = false;
            CheckBox2.Checked = false;
            CheckBox1.Visible = false;
            CheckBox2.Visible = false;
            CheckBox3.Visible = false;
            RadioButtonList1.ClearSelection();
            JobNote.Visible = false;
            SearchText.Text = "";
            SelectMonth.Visible = false;
            CheckBox5.Visible = false;

            Check_Report_Click(sender, e);
        }
        protected void Button6_Click(object sender, EventArgs e)
        {
            RadioButtonList1.Visible = false;
            DropDownList1.Visible = false;
            DropDownList2.Visible = false;
            DropDownList3.Visible = false;
            DropDownList4.Visible = false;
            Button5.Visible = false;
            Button6.Visible = false;
            HyperLink2.Visible = false;
            CheckBox1.Checked = false;
            CheckBox2.Checked = false;
            CheckBox1.Visible = false;
            CheckBox2.Visible = false;
            CheckBox3.Visible = false;
            RadioButtonList1.ClearSelection();
            JobNote.Visible = false;
            SearchText.Text = "";
            SelectMonth.Visible = false;
            CheckBox5.Visible = false;
            DropDownList5.Visible = false;


        }
        protected void Add_Job_Schedule(string[] tmpData)  // 客戶編號、時段、日期、管家編號及資料庫序號
        {
            ReCust = new string[] { };

            //讀取假況資料表
            INString = "Select * from Public_JobResult";
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            while (SQLReader.Read())
            {
                ReCust = ReCust.Concat(new string[] { SQLReader[1].ToString().Trim() }).ToArray();
                DropDownList3.Items.Add(new ListItem(SQLReader[1].ToString().Trim()));
            }
            INCommand.Connection.Close();
            int tmpID = 0;
            // 增加一筆日期在最後
            // 依服務次數回推找出日期
            double tmpTimes = 1.0;
            //INString = "Select Contract_Times,ID from Cust_Contract_Data where Cust_No = '" + tmpData[0] + "'";
            INString = "Select Contract_Times,Contract_ID from Cust_Job_Schedule_View where ID = " + tmpData[4];

            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            if (SQLReader.Read() == true)
            {
                tmpTimes = System.Convert.ToDouble(SQLReader[0]);
                tmpID = Convert.ToInt32(SQLReader[1]);
            }
            INCommand.Connection.Close();
            // TextBox4.Text = TextBox4.Text & INString

            // 找出倒數第二筆日期
            DateTime tmpDate = DateTime.Now;
            int tmpPrice = 0;
            int tmpHKSerial = 0;
            //INString = "Select top " + (tmpTimes * 2).ToString() + " Job_Date,Job_Price,HK_Serial from Cust_Job_Schedule where Cust_No = '" +
            //    tmpData[0] + "' order by Job_Date Desc";
            INString = "Select top " + (tmpTimes * 2).ToString() + " Job_Date,Job_Price,HK_Serial from Cust_Job_Schedule where Contract_ID = " +
                       tmpID + " order by Job_Date Desc";

            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            SQLReader = INCommand.ExecuteReader();
            while (SQLReader.Read() == true)
            {
                tmpDate = (DateTime)SQLReader[0];
                tmpPrice = Convert.ToInt32(SQLReader[1]);
                tmpHKSerial = Convert.ToInt32(SQLReader[2]);
            }

            INCommand.Connection.Close();
            // TextBox4.Text = TextBox4.Text & INString

            while (true)
            {
                tmpDate = (DateTime)tmpDate.AddDays(14);
                MyDataSet.Tables["Holiday_Date"].DefaultView.RowFilter = "H_Date = '" + tmpDate.ToShortDateString() + "'";
                if (MyDataSet.Tables["Holiday_Date"].DefaultView.Count == 1)
                {
                    //INString = "Insert into  Cust_Job_Schedule (Job_Date,Job_time,Staff_No,Cust_No,Job_Result,Job_Price,Job_Flag,Job_Status,HK_Serial," +
                    //           "Contract_ID) values('" + tmpDate.ToShortDateString() + "'," + (tmpData[1] == "上" ? 0 : 1).ToString() + ",'','" +
                    //           tmpData[0] + "','國',0,0,null,null," + tmpID + ")";
                    INString = "Insert into  Cust_Job_Schedule (Job_Date,Job_time,Staff_No,Cust_No,Job_Result_ID,Job_Price,Job_Flag,Job_Status,HK_Serial," +
                               "Contract_ID) values('" + tmpDate.ToShortDateString() + "'," + (tmpData[1] == "上" ? 0 : 1).ToString() + ",'','" +
                               tmpData[0] + "'," + (Array.IndexOf(ReCust, "國") + 1).ToString() + ",0,0,null,null," + tmpID + ")";
                    INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
                    INCommand.Connection.Open();
                    INCommand.ExecuteNonQuery();
                    INCommand.Connection.Close();
                }
                else
                    break;
            }
            // 加入一筆日期
            //INString = "Insert into  Cust_Job_Schedule (Job_Date,Job_time,Staff_No,Cust_No,Job_Result,Job_Price,Job_Flag,Job_Status,HK_Serial,Contract_ID)" +
            //           " values('" +  tmpDate.ToShortDateString() + "'," + (tmpData[1] == "上" ? 0 : 1).ToString() + ",'" + tmpData[3] + "','" + tmpData[0] + 
            //           "',''," + tmpPrice.ToString() + ",0,null," + tmpHKSerial.ToString() + "," + tmpID + ")";
            INString = "Insert into  Cust_Job_Schedule (Job_Date,Job_time,Staff_No,Cust_No,Job_Result_ID,Job_Price,Job_Flag,Job_Status,HK_Serial,Contract_ID)" +
                       " values('" + tmpDate.ToShortDateString() + "'," + (tmpData[1] == "上" ? 0 : 1).ToString() + ",'" + tmpData[3] + "','" + tmpData[0] +
                       "',0," + tmpPrice.ToString() + ",0,null," + tmpHKSerial.ToString() + "," + tmpID + ")";
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            INCommand.ExecuteNonQuery();
            INCommand.Connection.Close();
            // TextBox4.Text = TextBox4.Text & INString

            // 更新合約結束日期
            INString = "Update Cust_Contract_Data set Contract_E_Date = '" + tmpDate.ToShortDateString() + "' where ID = " + tmpID;
            INCommand = new System.Data.SqlClient.SqlCommand(INString, INConnection1);
            INCommand.Connection.Open();
            INCommand.ExecuteNonQuery();
            INCommand.Connection.Close();
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string tmpField, tmpField1, tmpField2;
            string[] tmpData; // 客戶編號、時段、日期、管家編號及資料庫序號

            tmpData = TextBox1.Text.Split(',');

            if (DBNull.Value.Equals(Session["dt"]))
                Check_Report_Click(sender, e);

            tmpField = "B" + DropDownList1.SelectedValue.ToString();
            tmpField1 = "B" + DropDownList1.SelectedValue.ToString() + "_1";
            tmpField2 = "B" + DropDownList1.SelectedValue.ToString() + "_2";

            System.Data.DataTable dt = (DataTable)Session["dt"];
            //設定排序
            if (dt.Columns.Contains("Cust_NN"))
                dt.Columns.Remove("Cust_NN");
            System.Data.DataColumn dc = new DataColumn();
            dc.ColumnName = "Cust_NN";
            dc.Expression = "Substring(" + tmpField2 + ",1,10)";
            dt.Columns.Add(dc);


            if (RadioButtonList1.SelectedIndex == 0 | RadioButtonList1.SelectedIndex == 4)
            {
                // 顯示未排班的客戶
                //System.Data.DataView dv = new System.Data.DataView(dt) { RowFilter = "(HK_Name = '未排班')  and ( " + tmpField + " <> '' or " + tmpField +
                //" is not null)" };

                //dv.AddNew();
                //dv[dv.Count - 1][System.Convert.ToInt32(DropDownList1.SelectedValue) + 35]) = "先不排";
                //dv[dv.Count - 1][System.Convert.ToInt32(DropDownList1.SelectedValue) + 2]) = "00000000";
                //DropDownList2.DataSource = dv;
                //DropDownList2.DataTextField = tmpField1;
                //DropDownList2.DataValueField = "Cust_NN";
                //DropDownList2.DataBind();

                //可以選擇相互支援的駐點該日期未排班的客戶,可以選擇前一個月未排班的客戶
                MyDataSet = new System.Data.DataSet();
                if (SelectMonth.SelectedIndex == 0 & SelectMonth.SelectedValue == "12")      //如果選擇的月份是12月,又是第一筆,表示是前一年12月
                    MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT ID,Cust_No+Cust_Name as Cust_Data FROM Cust_Job_Schedule_View where  Job_Date = '" +
                          (Convert.ToInt32(Check_Year.SelectedValue) - 1).ToString() + "/12/" + DropDownList1.SelectedValue.ToString() +
                           "' and Branch_Name = '" + DropDownList4.SelectedItem.Text.Trim() + "' and (Job_Result ='' or Job_Result is null) and" +
                           " (Staff_No is null or Staff_No = '') and Job_Flag <> 255 " + " Order by Job_Date,Cust_No", INConnection1);
                else
                    MyCommand = new System.Data.SqlClient.SqlDataAdapter("SELECT ID,Cust_No+Cust_Name as Cust_Data FROM Cust_Job_Schedule_View where  Job_Date = '" +
                                Check_Year.SelectedValue.ToString() + "/" + SelectMonth.SelectedValue.ToString() + "/" + DropDownList1.SelectedValue.ToString() +
                                "' and Branch_Name = '" + DropDownList4.SelectedItem.Text.Trim() + "' and (Job_Result ='' or Job_Result is null) and " +
                                "(Staff_No is null or Staff_No = '') and Job_Flag <> 255 " +
                                " Order by Job_Date,Cust_No", INConnection1);
                MyCommand.Fill(MyDataSet, "NoStaff");

                System.Data.DataView dv = MyDataSet.Tables["NoStaff"].DefaultView;
                dv.AddNew();
                dv[dv.Count - 1][1] = "先不排";
                dv[dv.Count - 1][0] = "00000000";
                dv.AddNew();
                dv[dv.Count - 1][1] = "回駐點";
                dv[dv.Count - 1][0] = "00000001";
                DropDownList2.DataSource = dv;
                DropDownList2.DataTextField = "Cust_Data";
                DropDownList2.DataValueField = "ID";
                DropDownList2.DataBind();

            }
            else
            {
                string DayID;
                if (CheckBox4.Checked == true)   // 上午
                    DayID = "上";
                else
                    DayID = "下";
                // 顯示未排班的管家
                //System.Data.DataView dv = new System.Data.DataView(dt) { RowFilter = "(HK_Name <> '未排班') and  Day_ID = '" + tmpData[1] + "' and ( " + 
                //                          tmpField + " = '空' or " + tmpField + " is null)" };
                System.Data.DataView dv = new System.Data.DataView(dt)
                {
                    RowFilter = "(HK_Name <> '未排班') and  Day_ID = '" + DayID + "' and ( " +
                                          tmpField + " = '空' or " + tmpField + " is null)"
                };
                dv.AddNew();
                dv[dv.Count - 1][1] = "先不排";
                dv[dv.Count - 1][35] = "000000";
                DropDownList2.DataSource = dv;
                DropDownList2.DataTextField = "HK_Name";
                DropDownList2.DataValueField = "HKN";
                DropDownList2.DataBind();
            }
            DropDownList2.Visible = true;
            Button5.Visible = true;
            Button6.Visible = true;
        }
        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] tmpData = TextBox1.Text.Split(',');
            DropDownList1.Visible = true;
            SearchText.Text = "";
            switch (RadioButtonList1.SelectedIndex)
            {
                case 0:
                    {
                        CheckBox1.Visible = true;
                        CheckBox2.Visible = false;
                        CheckBox4.Visible = false;
                        DropDownList3.Visible = false;
                        DropDownList4.Visible = true;
                        SelectMonth.Visible = true;
                        break;
                    }

                case 1:
                    {
                        CheckBox1.Visible = false;
                        CheckBox2.Visible = true;
                        CheckBox4.Visible = true;
                        DropDownList3.Visible = true;
                        DropDownList4.Visible = false;
                        DropDownList4.SelectedValue = Branch.SelectedValue;
                        SelectMonth.Visible = false;
                        break;
                    }

                case 2:
                    {
                        CheckBox3.Visible = true;
                        CheckBox3.Checked = false;
                        CheckBox4.Visible = true;
                        DropDownList4.Visible = true;
                        SelectMonth.Visible = false;
                        CheckBox5.Visible = true;
                        CheckBox5.Checked = false;
                        break;
                    }

                case 3:
                    {
                        JobNote.Text = "";
                        JobNote.Visible = true;
                        DropDownList2.Visible = false;
                        CheckBox4.Visible = false;
                        //HyperLink2.NavigateUrl = "Show_Contract_Detail.aspx?Cust_No=" + tmpData[0];
                        //HyperLink2.Text = tmpData[0].ToString();
                        //HyperLink2.Visible = true;
                        DropDownList4.Visible = false;
                        Button5.Visible = true;
                        Button6.Visible = true;
                        SelectMonth.Visible = false;
                        break;
                    }
                case 4:
                    {
                        CheckBox3.Visible = true;      //是否覆蓋
                        CheckBox3.Checked = false;
                        CheckBox1.Visible = false;     //是否扣次
                        CheckBox2.Visible = false;     //是否順延
                        CheckBox4.Visible = false;     //上午/下午
                        DropDownList3.Visible = false; //管家
                        DropDownList4.Visible = true;  //客戶
                        SelectMonth.Visible = false;  //選擇前面月份
                        CheckBox5.Visible = true;
                        CheckBox5.Checked = false;
                        break;
                    }
                case 5:
                    {
                        CheckBox3.Visible = false;      //是否覆蓋
                        CheckBox3.Checked = false;
                        CheckBox1.Visible = false;     //是否扣次
                        CheckBox2.Visible = false;     //是否順延
                        CheckBox4.Visible = false;     //上午/下午
                        DropDownList3.Visible = false; //管家
                        DropDownList4.Visible = false;  //客戶
                        SelectMonth.Visible = false;  //選擇前面月份
                        CheckBox5.Visible = false;
                        CheckBox5.Checked = false;
                        DropDownList5.Visible = true;  //顏色
                        break;
                    }
            }

            if (tmpData[4] == "0")
            {
                CheckBox1.Visible = false;
                CheckBox2.Visible = false;
                CheckBox1.Checked = false;
                CheckBox2.Checked = false;
                CheckBox4.Visible = false;
            }

            if (RadioButtonList1.SelectedIndex != 3)
            {
                DropDownList1_SelectedIndexChanged(sender, e);
                HyperLink2.Visible = false;
            }
        }


        protected void Search_Click(object sender, EventArgs e)
        {
            System.Data.DataTable dt = (DataTable)Session["dt"];
            Boolean FD = false;

            if (Session["dt"] == null)
                Check_Report_Click(sender, e);

            if (SearchText.Text != "")
            {
                for (int si = 0; si < dt.Rows.Count; si++)
                    for (int sj = 0; sj < 31; sj++)
                        if (dt.Rows[si][35 + sj].ToString().Contains(SearchText.Text.Trim()))
                        {
                            DataGrid3.CurrentPageIndex = (si / DataGrid3.PageSize);
                            if (sj > 20)
                                Session["CS"] = 20;
                            else
                                Session["CS"] = sj;
                            FD = true;
                            si = dt.Rows.Count;
                            sj = 31;
                        }
                if (FD)
                    Change_Screen(sender, e);
            }
        }

        protected void DropDownList5_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList5.BackColor = Color.FromName(DropDownList5.SelectedValue);
            for (int ti = 0; ti < DropDownList5.Items.Count; ti++)
                DropDownList5.Items[ti].Attributes.Add("style", "background-color:" + DropDownList5.Items[ti].Value);
        }
    }
}