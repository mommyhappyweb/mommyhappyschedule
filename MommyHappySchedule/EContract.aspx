﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EContract.aspx.cs" Inherits="MommyHappySchedule.EContract" MasterPageFile="~/Template/Layout.Master" %>

<asp:Content ID="Style" ContentPlaceHolderID="Style" runat="server">

</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="Content" runat="server">
    <div class="loading action" id="Loading">
        <div class="loading-block">
            <div class="loading-dot"></div>
            <div class="loading-dot"></div>
            <div class="loading-dot"></div>
            <div class="loading-dot"></div>
            <div class="loading-dot"></div>
        </div>
    </div>
    <div class="container-fluid" id="App">
        <nav class="mt-3 mb-3" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="Index.aspx">首頁</a></li>
                <li class="breadcrumb-item active">新增合約</li>
            </ol>
        </nav>
        <div class="features" id="Features">
            <input class="btn btn-info" type="button" value="儲存合約資料" @click="ContractSaveBtn" />
        </div>
        <fieldset>
            <legend>合約基本資料</legend>
            <div class="form-group row">
                <div class="col-sm-2">
                    <label class="col-form-label">駐點</label>
                    <select class="form-control" v-model="Branch_ID" @change="BranchChangeEvent">
                        <option v-for="Item in BranchData" :value="Item.ID">{{ Item.Branch_Name }}</option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <label class="col-form-label">簽約專員</label>
                    <select class="form-control" v-model="BranchStaff_No" @change="BranchChargeStaffEvent">
                        <option v-if="BranchStaff_No === ''" :value="-1">請選擇</option>
                        <option v-else v-for="Item in BranchStaffData" :value="Item.Staff_No">{{ Item.Staff_Name }}</option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <label class="col-form-label">合約項目</label>
                    <select class="form-control" v-model="ContractType_ID" @change="ContractTypeChargeEvent">
                        <option v-for="Item in ContractTypeData" :value="Item.ID">{{ Item.Name }}</option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <label class="col-form-label">合約編號</label>
                    <input class="form-control" type="text" placeholder="19001234" v-model="Contract_Nums" />
                </div>
            </div>
            <div class="form-group row" v-if="ContractType_ID == 0">
                <div class="col-sm-2">
                    <label class="col-form-label">客戶編號<b class="badge badge-pill badge-danger">必填</b></label>
                    <input class="form-control" type="text" placeholder="KC00-000" required="required" v-model="Cust_No" />
                </div>
                <div class="col-sm-2">
                    <label class="col-form-label">客戶姓名<b class="badge badge-pill badge-danger">必填</b></label>
                    <input class="form-control" type="text" placeholder="客戶姓名" required="required" v-model="Cust_Name" />
                </div>
                <div class="col-sm-2">
                    <label class="col-form-label">統一編號</label>
                    <input class="form-control" type="text" placeholder="A123456789 或 21345678" v-model="IDCard" />
                </div>
                <div class="col-sm-2">
                    <label class="col-form-label">性別</label>
                    <select class="form-control" v-model="Sex">
                        <option value="1">男</option>
                        <option value="2">女</option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <label class="col-form-label">住家或公司電話</label>
                    <input class="form-control" type="text" placeholder="(07)78123456" required="required" v-model="House_Phone" />
                </div>
                <div class="col-sm-2">
                    <label class="col-form-label">手機號碼<b class="badge badge-pill badge-danger">必填</b></label>
                    <input class="form-control" type="text" placeholder="0987087087" required="required" v-model="Cell_Phone" />
                </div>
                <div class="col-sm-2">
                    <label class="col-form-label">其他手機號碼</label>
                    <input class="form-control" type="text" placeholder="0987087087" v-model="Other_Phone" />
                </div>
                <div class="col-sm-2">
                    <label class="col-form-label">服務縣市</label>
                    <select class="form-control" v-model="City_Name" @change="CityChangeEvent">
                        <option v-for="Item in CityData" :value="Item.City_Name">{{ Item.City_Name }}</option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <label class="col-form-label">服務行政區</label>
                    <select class="form-control" v-model="Area_Name">
                        <option v-for="Item in AreaData" :value="Item.Country_Name">{{ Item.Country_Name }}</option>
                    </select>
                </div>
                <div class="col-sm-6">
                    <label class="col-form-label">服務地址<b class="badge badge-pill badge-danger">必填</b></label>
                    <input class="form-control" type="text" placeholder="富民路1號" required="required" v-model="Address" />
                </div>
            </div>
            <div class="form-group row" v-if="ContractType_ID == 1">
                <div class="col-sm-2">
                    <label class="col-form-label">續約客戶編號<b class="badge badge-pill badge-danger">必填</b></label>
                    <input class="form-control" type="text" placeholder="KC00-000" required="required" v-model="Cust_No" />
                </div>
            </div>
            <div class="form-group row" v-else-if="ContractType_ID == 2">
                <div class="col-sm-2">
                    <label class="col-form-label">合約ID<b class="badge badge-pill badge-danger">必填</b></label>
                    <input class="form-control" type="text" placeholder="123456" v-model="Contract_ID" />
                </div>
                <div class="col-sm-2">
                    <label class="col-form-label">加次客戶編號<b class="badge badge-pill badge-danger">必填</b></label>
                    <input class="form-control" type="text" placeholder="KC00-000" required="required" v-model="Cust_No" />
                </div>
                <div class="col-sm-2">
                    <label class="col-form-label">加次金額</label>
                    <input class="form-control" type="number" required="required" v-model="AddNumber_Amt" />
                </div>
            </div>
            <div class="form-group row" v-else-if="ContractType_ID == 3">
                <div class="col-sm-2">
                    <label class="col-form-label">單次客戶編號<b class="badge badge-pill badge-danger">必填</b></label>
                    <input class="form-control" type="text" placeholder="KC00-000" required="required" v-model="Single_Cust_No" />
                </div>
                <div class="col-sm-2">
                    <label class="col-form-label">轉合約客戶編號<b class="badge badge-pill badge-danger">必填</b></label>
                    <input class="form-control" type="text" placeholder="KC00-000" required="required" v-model="Cust_No" />
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend>服務週期試算</legend>
            <div class="form-group row">
                <div class="col-sm-2">
                    <label class="col-form-label">合約年限</label>
                    <select class="form-control" v-model="Contract_Years">
                        <option v-for="Item in ContractYearData" :value="Item.ID">{{ Item.Name }}</option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <label class="col-form-label">合約開始時間</label>
                    <input class="form-control" type="date" v-model="Contract_S_Date" />
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-2">
                    <label class="col-form-label">預計付款日期</label>
                    <input class="form-control" type="date" v-model="C_Pay_Date" />
                </div>
                <div class="col-sm-2">
                    <label class="col-form-label">付款方式</label>
                    <select class="form-control" v-model="Payment_Type" @change="PaymentTypeChangeEvent">
                        <option v-for="Item in PaymentTypeData" :value="Item.ID">{{ Item.Name }}</option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <label class="col-form-label">付款期數</label>
                    <input class="form-control" type="number" min="0" v-model="C_Pay_Numbers" />
                </div>
                <div class="col-sm-2">
                    <label class="col-form-label">含5%營業稅</label>
                    <select class="form-control" v-model="C_Tax_Flag">
                        <option value="0">否</option>
                        <option value="1">是</option>
                    </select>
                </div>
                <div class="col-sm-4">
                    <label class="col-form-label">付款備註</label>
                    <input class="form-control" type="text" placeholder="付款備註" v-model="Payment_Memo" />
                </div>
            </div>
            <div class="form-group row" v-if="IsPaymentCode()">
                <div class="col-sm-2">
                    <label class="col-form-label">銀行名稱</label>
                    <select class="form-control" v-model="Out_Bank">
                        <option value="">請選擇</option>
                        <option v-for="Item in BankData" :value="Item.Bank_Code">{{ Item.Bank_Name }}</option>
                    </select>
                </div>
                <div class="col-sm-2">
                    <label class="col-form-label">銀行帳號</label>
                    <input class="form-control" type="text" placeholder="0000123456789" v-model="Out_Acct_No" />
                </div>
            </div>
            <div class="form-group weekdate" v-for="Item in 2">
                <div class="weekdate-item holiday">
                    <span class="weekdate-title">星期日</span>
                    <hr />
                    <div class="weekdate-block">
                        <label>上午班管家數</label>
                        <select class="form-control">
                            <option v-for="Item in HousekeeperData" :value="Item.ID">{{ Item.Name }}</option>
                        </select>
                    </div>
                    <div class="weekdate-block">
                        <label>下午班管家數</label>
                        <select class="form-control">
                            <option v-for="Item in HousekeeperData" :value="Item.ID">{{ Item.Name }}</option>
                        </select>
                    </div>
                </div>
                <div class="weekdate-item">
                    <span class="weekdate-title">星期一</span>
                    <hr />
                    <div class="weekdate-block">
                        <label>上午班管家數</label>
                        <select class="form-control">
                            <option v-for="Item in HousekeeperData" :value="Item.ID">{{ Item.Name }}</option>
                        </select>
                    </div>
                    <div class="weekdate-block">
                        <label>下午班管家數</label>
                        <select class="form-control">
                            <option v-for="Item in HousekeeperData" :value="Item.ID">{{ Item.Name }}</option>
                        </select>
                    </div>
                </div>
                <div class="weekdate-item">
                    <span class="weekdate-title">星期二</span>
                    <hr />
                    <div class="weekdate-block">
                        <label>上午班管家數</label>
                        <select class="form-control">
                            <option v-for="Item in HousekeeperData" :value="Item.ID">{{ Item.Name }}</option>
                        </select>
                    </div>
                    <div class="weekdate-block">
                        <label>下午班管家數</label>
                        <select class="form-control">
                            <option v-for="Item in HousekeeperData" :value="Item.ID">{{ Item.Name }}</option>
                        </select>
                    </div>
                </div>
                <div class="weekdate-item">
                    <span class="weekdate-title">星期三</span>
                    <hr />
                    <div class="weekdate-block">
                        <label>上午班管家數</label>
                        <select class="form-control">
                            <option v-for="Item in HousekeeperData" :value="Item.ID">{{ Item.Name }}</option>
                        </select>
                    </div>
                    <div class="weekdate-block">
                        <label>下午班管家數</label>
                        <select class="form-control">
                            <option v-for="Item in HousekeeperData" :value="Item.ID">{{ Item.Name }}</option>
                        </select>
                    </div>
                </div>
                <div class="weekdate-item">
                    <span class="weekdate-title">星期四</span>
                    <hr />
                    <div class="weekdate-block">
                        <label>上午班管家數</label>
                        <select class="form-control">
                            <option v-for="Item in HousekeeperData" :value="Item.ID">{{ Item.Name }}</option>
                        </select>
                    </div>
                    <div class="weekdate-block">
                        <label>下午班管家數</label>
                        <select class="form-control">
                            <option v-for="Item in HousekeeperData" :value="Item.ID">{{ Item.Name }}</option>
                        </select>
                    </div>
                </div>
                <div class="weekdate-item">
                    <span class="weekdate-title">星期五</span>
                    <hr />
                    <div class="weekdate-block">
                        <label>上午班管家數</label>
                        <select class="form-control">
                            <option v-for="Item in HousekeeperData" :value="Item.ID">{{ Item.Name }}</option>
                        </select>
                    </div>
                    <div class="weekdate-block">
                        <label>下午班管家數</label>
                        <select class="form-control">
                            <option v-for="Item in HousekeeperData" :value="Item.ID">{{ Item.Name }}</option>
                        </select>
                    </div>
                </div>
                <div class="weekdate-item holiday">
                    <span class="weekdate-title">星期六</span>
                    <hr />
                    <div class="weekdate-block">
                        <label>上午班管家數</label>
                        <select class="form-control">
                            <option v-for="Item in HousekeeperData" :value="Item.ID">{{ Item.Name }}</option>
                        </select>
                    </div>
                    <div class="weekdate-block">
                        <label>下午班管家數</label>
                        <select class="form-control">
                            <option v-for="Item in HousekeeperData" :value="Item.ID">{{ Item.Name }}</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="btn-block text-center">
                <input class="btn btn-info" type="button" value="合約試算" @click="ContractExpectedEvent" />
            </div>
        </fieldset>
        <fieldset v-show="IsExpectedShow">
            <legend>預計服務週期結果</legend>
            <div class="form-group row text-center">
                <div class="col-sm-2">
                    <label class="col-form-label">合約結束時間</label>
                    <span class="form-value text-secondary" v-model="Contract_E_Date">{{ Contract_E_Date }}</span>
                </div>
                <div class="col-sm-2">
                    <label class="col-form-label">合約期數</label>
                    <span class="form-value text-warning" v-model="Contract_Week">{{ Contract_Week }}</span>
                </div>
                <div class="col-sm-2">
                    <label class="col-form-label">服務週期</label>
                    <span class="form-value text-info" v-model="Service_Cycle">{{ Service_Cycle }}</span>
                </div>
                <div class="col-sm-2">
                    <label class="col-form-label">總服務次數</label>
                    <span class="form-value text-primary" v-model="Contract_Total">{{ Contract_Total }}</span>
                </div>
                <div class="col-sm-2">
                    <label class="col-form-label">國定假日</label>
                    <span class="form-value text-danger" v-model="Contract_HolidayNumber">{{ Contract_HolidayNumber }}</span>
                </div>
                <div class="col-sm-2">
                    <label class="col-form-label">總金額</label>
                    <span class="form-value text-success" v-model="Contract_Pay_Amt">{{ Contract_Pay_Amt }}</span>
                </div>
            </div>
            <hr />
            <div class="contractdate form-group row" id="ContractDate">
                <div class="col-sm-1" v-for="Item in ScheduleData">
                    <span class="contractdate-item pm" 
                        :data-index="Item.Index"
                        :data-job_result_id="Item.Job_Result_ID"
                        :data-job_price="Item.Job_Price"
                        :data-job_time="Item.Job_Time"
                        :data-job_time_name="Item.Job_Time_Name"
                        :data-holiday_name="Item.HolidayName"
                        :title="Item.HolidayName + Item.Job_Time_Name + '-金額:' + Item.Job_Price" 
                        v-if="IsScheduleCode(Item) === 1" 
                        @contextmenu.prevent="ScheduleDateRightClick">{{ Item.Job_Date }}({{ Item.WeekName }})</span>
                    <span class="contractdate-item att"
                        :data-index="Item.Index"
                        :data-job_result_id="Item.Job_Result_ID"
                        :data-job_price="Item.Job_Price" 
                        :data-job_time="Item.Job_Time"
                        :data-job_time_name="Item.Job_Time_Name"
                        :data-holiday_name="Item.HolidayName"
                        :title="Item.HolidayName + '-金額:' + Item.Job_Price" 
                        v-else-if="IsScheduleCode(Item) === 2" 
                        @contextmenu.prevent="ScheduleDateRightClick">{{ Item.Job_Date }}({{ Item.WeekName }})</span>
                    <span class="contractdate-item"
                        :data-index="Item.Index"
                        :data-job_result_id="Item.Job_Result_ID"
                        :data-job_price="Item.Job_Price" 
                        :data-job_time="Item.Job_Time"
                        :data-job_time_name="Item.Job_Time_Name"
                        :data-holiday_name="Item.HolidayName"
                        :title="Item.HolidayName + Item.Job_Time_Name + '-金額:' + Item.Job_Price" 
                        v-else="IsScheduleCode(Item) === 0" 
                        @contextmenu.prevent="ScheduleDateRightClick">{{ Item.Job_Date }}({{ Item.WeekName }})</span>
                </div>
            </div>
        </fieldset>
        <fieldset v-show="IsPaymentShow">
            <legend>預計付款資訊</legend>
            <table class="table table-bordered text-center">
                <thead class="thead-light">
                    <tr>
                        <th>期數</th>
                        <th>付款日期</th>
                        <th>付款方式</th>
                        <th>付款金額</th>
                        <th>稅額</th>
                        <th>實付金額</th>
                    </tr>
                </thead>
                <tbody class="table-striped table-hover">
                    <tr v-for="Item in ContractPayDetailData">
                        <td>{{ Item.Pay_Number }}</td>
                        <td>{{ Item.B_Pay_Date }}</td>
                        <td>{{ Item.Pay_Payment_Name }}</td>
                        <td>{{ Item.Pay_Amt }}</td>
                        <td>{{ Item.Tax }}</td>
                        <td>{{ Item.Amt }}</td>
                    </tr>
                </tbody>
            </table>
        </fieldset>
        <div class="contractdate-menu" id="ContractDateMenu" v-show="IsContractdateMenuShow">
            <span class="contractdate-title title">{{ ContractDateMenu_Title }}</span>
            <input class="form-control" type="number" min="0" title="金額" v-model="ContractDateMenu_Job_Price" />
            <%--<span class="contractdate-title">{{ ContractDateMenu_Job_TimeName }}</span>--%>
            <select class="form-control" v-model="ContractDateMenu_Job_Time" title="班制">
                <option v-for="Item in JobTimeData" :value="Item.ID">{{ Item.Name }}</option>
            </select>
            <span class="contractdate-title holiday" v-if="ContractDateMenu_Holiday_Name !== ''">{{ ContractDateMenu_Holiday_Name }}</span>
            <select class="form-control" v-model="ContractDateMenu_Job_Result_ID" title="派班狀況">
                <option v-for="Item in JobResultData" :value="Item.ID">{{ Item.Name }}</option>
            </select>
            <textarea class="form-control" placeholder="備註" v-model="ContractDateMenu_Job_Note"></textarea>
            <input class="btn btn-sm btn-success" type="button" value="儲存" @click="ContractDateMenuSave" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Script" ContentPlaceHolderID="Script" runat="server">
    <script src="Public/Scripts/Vue/vue.min.js"></script>
    <script src="Public/Scripts/App/LoadingApp.js"></script>    
    <script src="Public/Scripts/App/EContractApp.js"></script>
</asp:Content>
