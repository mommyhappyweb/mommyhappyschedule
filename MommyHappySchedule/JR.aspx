﻿<%@ Page Language="C#" MasterPageFile="~/Template/Layout.master" AutoEventWireup="true" CodeBehind="JR.aspx.cs" Inherits="MommyHappySchedule.JR" MaintainScrollPositionOnPostback="true"  %>
<asp:Content ID="Style" ContentPlaceHolderID="Style" runat="server">
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="Content" runat="server">
    <div class="container-fluid">
        <nav class="mt-3 mb-3" aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="Index.aspx">首頁</a></li>
                <li class="breadcrumb-item active">修改管家打卡紀錄</li>
            </ol>
        </nav>
        <table class="table">
            <tr>
                <td>駐點：</td>
                <td>
                    <asp:DropDownList ID="Branch" runat="server">
                    </asp:DropDownList>
                </td>
                <td>員工名稱</td>
                <td>
                    <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                </td>
                <td>客戶名稱</td>
                <td>
                    <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                </td>
                <td>
                    <asp:Button ID="Check_Report" runat="server" Text="產生報表" OnClick="Check_Report_Click" />
                    <asp:Button ID="ExportToExcel" runat="server" Text="滙出EXCEL" OnClick="ExportToExcel_Click" />
                    <asp:Button ID="ExportForFIN" runat="server" Text="營收" OnClick="ExportForFIN_Click" />
                    <asp:Button ID="ExportToERP" runat="server" Text="滙到EPR" OnClick="ExportToERP_Click" />
                    <asp:Button ID="Button6" runat="server" Text="取消" OnClick="Button6_Click" />
                </td>
                <td>
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="Index.aspx">返回主選單</asp:HyperLink>
                </td>
                <td>
                    <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                    <asp:Label ID="Label1" runat="server" Text="0" Visible="false"></asp:Label>
                    <asp:Label ID="Label4" runat="server" Text="0.00"></asp:Label>
                    <asp:Label ID="Label5" runat="server" Text="0.00"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="4">顯示：
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged" RepeatLayout="Flow">
                        <asp:ListItem>全部</asp:ListItem>
                        <asp:ListItem>已服務</asp:ListItem>
                        <asp:ListItem>服務中</asp:ListItem>
                        <asp:ListItem>未打卡</asp:ListItem>
                        <asp:ListItem>打卡異常</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td colspan="3">
                    <asp:RadioButtonList ID="RadioButtonList2" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged" RepeatLayout="Flow">
                        <asp:ListItem>全部</asp:ListItem>
                        <asp:ListItem>上午</asp:ListItem>
                        <asp:ListItem>下午</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:Button ID="Left_Screen" runat="server" Text="前一天" OnClick="Change_Screen" />
                    <asp:TextBox ID="Show_Date" runat="server" Width="70px"></asp:TextBox>
                    <asp:TextBox ID="End_Date" runat="server" Width="70px"></asp:TextBox>
                    <asp:Button ID="Right_Screen" runat="server" Text="後一天" OnClick="Change_Screen" />
                </td>
                <td colspan="3"></td>
            </tr>
        </table>
        <asp:DataGrid ID="DataGrid1" runat="server" CssClass="table" AutoGenerateColumns="False" BackColor="White" OnItemDataBound="Show_Sc_Type"
            OnEditCommand="ItemsGrid_Edit" OnCancelCommand="ItemsGrid_Cancel" OnUpdateCommand="ItemsGrid_Update" OnItemCommand="Show_Job_Detail"
            BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" Font-Names="Arial" Font-Size="Smaller" BorderStyle="None">
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <SelectedItemStyle BackColor="#669999" ForeColor="White" Font-Bold="True" />
            <Columns>
                <asp:BoundColumn DataField="Schedule_ID" HeaderText="ID" Visible="false" ReadOnly="true">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Branch_Name" HeaderText="駐點" Visible="false" ReadOnly="true">
                </asp:BoundColumn>
                <asp:ButtonColumn DataTextField="Staff_No" HeaderText="管家編號" CommandName="照片">
                </asp:ButtonColumn>
                <asp:BoundColumn DataField="Staff_Name" HeaderText="管家姓名" ReadOnly="true">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Cust_No" HeaderText="客戶編號" ReadOnly="true">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Cust_Name" HeaderText="客戶姓名" ReadOnly="true">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Cust_Addr" HeaderText="客戶地址" Visible="false" ReadOnly="true">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Job_Date" HeaderText="日期" DataFormatString="{0:yyyy/MM/dd}" ReadOnly="true">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Start_Time" HeaderText="起始時間" ReadOnly="true">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Punch_In_Time" HeaderText="上班時間" DataFormatString="{0:HH:mm:ss}">
                </asp:BoundColumn>
                <asp:TemplateColumn HeaderText="異常原因">
                    <ItemTemplate>
                        <asp:DropDownList ID="DropDownList1" CssClass="form-control" runat="server" Enabled="false">
                            <asp:ListItem Value="0">正常打卡</asp:ListItem>
                            <asp:ListItem Value="1001">遲到</asp:ListItem>
                            <asp:ListItem Value="1002">客戶晚開門</asp:ListItem>
                            <asp:ListItem Value="1003">客戶不在家</asp:ListItem>
                            <asp:ListItem Value="1004">臨時指派客戶</asp:ListItem>
                            <asp:ListItem Value="1005">請假</asp:ListItem>
                            <asp:ListItem Value="1006">公司臨時任務</asp:ListItem>
                            <asp:ListItem Value="1007">督導</asp:ListItem>
                            <asp:ListItem Value="1000">其它原因</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="R_In_Des" HeaderText="異常描述">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="End_Time" HeaderText="終止時間" ReadOnly="true">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Punch_Out_Time" HeaderText="下班時間" DataFormatString="{0:HH:mm:ss}">
                </asp:BoundColumn>
                <asp:TemplateColumn HeaderText="異常原因">
                    <ItemTemplate>
                        <asp:DropDownList ID="DropDownList2" CssClass="form-control" runat="server" Enabled="false">
                            <asp:ListItem Value="0">正常打卡</asp:ListItem>
                            <asp:ListItem Value="1001">早退</asp:ListItem>
                            <asp:ListItem Value="1002">客戶指示提前離開</asp:ListItem>
                            <asp:ListItem Value="1003">客戶不在家</asp:ListItem>
                            <asp:ListItem Value="1004">公司指示離開</asp:ListItem>
                            <asp:ListItem Value="1005">請假</asp:ListItem>
                            <asp:ListItem Value="1006">公司臨時任務</asp:ListItem>
                            <asp:ListItem Value="1007">督導</asp:ListItem>
                            <asp:ListItem Value="1000">其它原因</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="R_Out_Des" HeaderText="異常描述">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Job_Result" HeaderText="狀態" ReadOnly="true">
                </asp:BoundColumn>
                <asp:TemplateColumn HeaderText="出勤狀態">
                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" ReadOnly="true"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="Job_Flag" Visible="false" ReadOnly="true">
                </asp:BoundColumn>
                <asp:EditCommandColumn EditText="編輯" CancelText="取消" UpdateText="存檔" HeaderText="編輯">
                    <ItemStyle Wrap="true"></ItemStyle>
                    <HeaderStyle Wrap="False"></HeaderStyle>
                </asp:EditCommandColumn>
                <asp:BoundColumn DataField="R_In_ID" Visible="false" ReadOnly="true">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="R_Out_ID" Visible="false" ReadOnly="true">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="SU_ID" Visible="false" ReadOnly="true">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Job_Price" Visible="false" ReadOnly="true">
                </asp:BoundColumn>
                <asp:BoundColumn DataField="Note" Visible="false" ReadOnly="true">
                </asp:BoundColumn>
            </Columns>
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <PagerStyle HorizontalAlign="Left" Mode="NumericPages" BackColor="White" ForeColor="#000066" />
            <ItemStyle ForeColor="#000066" />
        </asp:DataGrid>
        <asp:TextBox ID="TextBox5" runat="server" Width="100%"></asp:TextBox>
    </div>
</asp:Content>
<asp:Content ID="Script" ContentPlaceHolderID="Script" runat="server">
</asp:Content>